<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model {
 
   public function login($condition)
   {
	   $this->db->where($condition);
	   return $this->db->get('account');
   }
   public function signUp($data)
	{
	  	
		return  $this->db->insert('account',$data);

	} 

	public function insert_data($table, $data)
	{
	  	
		return  $this->db->insert($table,$data);

	} 
	public function delete_data($table, $condition)
	{
	  	if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return  $this->db->delete($table);

	} 
	
	public function getCustomers($table, $condition="",$order_by="",$search)
	{
	  	if(!empty($condition))
		{
			$this->db->where($condition); 
		} 
		if($search !="")
		{
				$this->db->like('company', $search);
				//$this->db->or_like('company', $search);  
		} 
		if(!empty($order_by))
		{
			$this->db->order_by($order_by,"DESC");
		} 
		$query=$this->db->get($table);
		if($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
			$row_set[] = htmlentities(stripslashes($row['company'])); //build an array
			}
			echo json_encode($row_set); //format the array into json data
		}
	}
	
	
public function get_data($table, $condition="",$order_by="",$search="")
	{
	  	 
	  	if(!empty($condition))
		{
			$this->db->where($condition); 
		} 
		if($search !="")
		{
				$this->db->like('company', $search);
				//$this->db->or_like('company', $search);  
		} 
		if(!empty($order_by))
		{
			$this->db->order_by($order_by,"DESC");
		} 
		return  $this->db->get($table);

	} 
	

	public function update_data($table,$condition="",$data)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return $this->db->update($table,$data);
	}
		
  public function get_subscribers()
	{
	  	$this->db->distinct();
	  	$this->db->select("invoice_no,token_no,user_info_id,package_id,date_paid,paid_amount");
		 $this->db->group_by('id'); 
		return  $this->db->get('package_payment');

	}
	
	public function getMessages($condition="",$start,$end)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
			
		}
		else if(!empty($search))
		{
				$this->db->like('name', $search);
				$this->db->or_like('subject', $search); 				

		} 
		
		$this->db->order_by('id','desc');
		$this->db->limit($end,$start); 
		return $this->db->get('messages');
		
	}
	
	public function getData($table,$condition="",$search="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
			
			$this->db->order_by('id','desc');
		}
		else if(!empty($search))
		{
				$this->db->like('name', $search);
				$this->db->or_like('subject', $search); 				

		}
		 
		return $this->db->get($table);
	}
	
	public function updateUser($table,$condition="",$data)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return $this->db->update($table,$data);
	}

	
	public function updateMessageInfo($id="",$data)
	{
		if(!empty($id))
		{
			$this->db->where('id',$id);
		}
		return $this->db->update('messages',$data);
	}
 
 
 	public function uploadPhoto($file_name,$condition,$table)
	{
		$data=array('photo'=>$file_name);
		$this->db->where($condition);
		$this->db->update($table,$data);		
		return true;
	} 
	public function update_sms($condition="",$count)
	{
		$remaining_sms=0; $sent_sms=0;
		if(!empty($condition))
		{
			$this->db->where($condition); 
			$q=$this->db->get('sms_status');
			foreach($q->result() as $row){$sent_sms=$row->sent_sms; $remaining_sms=$row->remaining_sms;}
		}
		if(!empty($condition))
		{
			$this->db->where($condition); 
			return $this->db->update('sms_status',$data=array('sent_sms'=>($sent_sms+$count),'remaining_sms'=>($remaining_sms-$count)));
		}
	}
	public function sendMail($id="",$data)
	{ 
		if(!empty($id))
		{
			$this->db->insert('messages',$data);
			$this->db->where(array('id'=>$id));
			return $this->db->delete('messages');
		} 
		else
		{
			return $this->db->insert('messages',$data);		
		}	  
		 
	}

public function proof_user($condition)
	{
		$this->db->where($condition);
		return $this->db->get("account");	
	} 

public function addUser($data)
		{
		$this->db->where('email',$data['email']); 
		$q = $this->db->get('account'); 
	
		if ($q->num_rows() >0) 
		{ 
			
			$this->db->where('email',$email); 
			return 	$this->db->update('user_info',$data);
		} 
		else
		{
			//insert into admin table
			return $this->db->insert('user_info',$data); 
		}
	}
	
	public function addPartner($data)
	{
		$this->db->where($data);
		$this->db->get('partners');
		if($this->db->affected_rows()>0)
		{
			return $this->db->update('partners');
		}
		else
		{
			return $this->db->insert('partners',$data);
		}
		
	}
	
	public function getReceipient($condition="")
	{
		if(!empty($condition)){
			$this->db->where($condition);	
		}
	 return $this->db->get('account');
	}
}