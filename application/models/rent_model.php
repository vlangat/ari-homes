<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rent_model extends CI_Model {
 
 	public function insert_data($table, $data)
	{
		return  $this->db->insert($table,$data); 
	}
	
	public function get_data($table="",$condition="", $order="",$limit="",$asc_desc="DESC")
	{  
		 if(!empty($condition))
		 {
			 $this->db->where($condition);
		 }
		if(!empty($order))
		 {
			 $this->db->order_by($order, $asc_desc);
		 }
		 if(!empty($limit))
		 {
			 $this->db->limit($limit,0);
		 }
		return $this->db->get($table); 
	} 

	public function update_data($table, $data, $condition)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return  $this->db->update($table,$data);
	}
	 
 public function get_aging_report($table="tenant_current_account", $condition)
	{ 
		$this->db->select("tenant_current_account.id,tenant_current_account.tenant_id,tenant_property_units.first_name,tenant_property_units.middle_name,tenant_property_units.last_name,tenant_property_units.tenant_email,tenant_property_units.company_email,tenant_property_units.company_name,tenant_property_units.company_code,tenant_property_units.tenant_type,tenant_property_units.property_name,tenant_property_units.category_name,tenant_property_units.house_no,,tenant_property_units.id_passport_number,tenant_current_account.last_pay_date,tenant_current_account.total_amount");
		$this->db->from($table);
		$this->db->join('tenant_property_units',$table.'.tenant_id=tenant_property_units.id','left'); 
		return  $this->db->get();
	}
	
  public function get_paid_rent($table="", $condition)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		$this->db->order_by("id","DESC");
		$this->db->select("tenant_transaction.id,tenant_transaction.tenant_id,tenant_transaction.date_paid,tenant_transaction.payment_mode,tenant_transaction.amount,tenant_transaction.type,tenant_property_units.company_code");
		$this->db->from($table);
		$this->db->join('tenant_property_units',$table.'.tenant_id=tenant_property_units.id','left'); 
		return  $this->db->get();
	}
 	
	public function get_statement($condition="",$from="",$to="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		if(!empty($to))
		{
			$this->db->where("date_paid BETWEEN '".$from."' AND '".$to."'");
		}
		$this->db->join('unit_property_details','unit_property_details.unit_id=tenant_transaction_view.property_unit_id','left');
		
		$this->db->order_by("transaction_id","ASC");
		return  $this->db->get('tenant_transaction_view');
	}

	public function get_expense_statement($condition="",$from="",$to="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		if(!empty($to))
		{
			$this->db->where("expenses_view.date_paid BETWEEN '".$from."' AND '".$to."'");
		} 
		return  $this->db->get('expenses_view ');
	}

	public function getExpenses($condition="")
	{
		$this->db->select("suppliers.company_name, suppliers.product_description, suppliers.company_code, expenses.id, expenses.property_id,expenses.supplier_invoice_no,expenses.invoice_no, expenses.expense_type_id,expenses.amount, expenses.payment_mode, expenses.date_paid, expenses.description");
		$this->db->from("expenses");
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		$this->db->order_by("expenses.id","desc");
		$this->db->join('suppliers','expenses.supplier_id=suppliers.id','left');
		return  $this->db->get();
	}

	public function unpaid_rent($condition="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		$this->db->select("*");
		$this->db->from("tenant_property_units");
		$this->db->where("tenant_current_account.total_amount >",0);
		$this->db->where("tenant_current_account.audit_number ",1); 
		$this->db->join("tenant_current_account","tenant_current_account.tenant_id=tenant_property_units.id","left");
		return  $this->db->get();  
	} 

	public function getInvoiceItems($table, $condition="",$order_by="priority")
	{
        if(!empty($condition))
		{
			$this->db->select('*'); 
			$this->db->where($condition); 
			$this->db->where("(invoice_no = '' OR  invoice_no IS  NULL)"); 	
		} 
		return $this->db->get($table); 
	}  
 
}
?>