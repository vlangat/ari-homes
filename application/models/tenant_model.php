<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tenant_model extends CI_Model { 

public function insert_data($table, $data)
	{  
		return $this->db->insert($table, $data);
	}
	
public function get_data($table,$condition="",$order_by="",$asc_desc="DESC",$search="",$search_field='company_name')
{ 
	if(!empty($condition))
	{
		$this->db->where($condition);
	} 
	if($search !="")
	{
			$this->db->like($search_field, $search);  
	} 
	if(!empty($order_by))
	{
		$this->db->order_by($order_by,$asc_desc);
	} 
	return $this->db->get($table);
}

public function update_data($table="",$condition="",$data)
{	
	if(!empty($condition))
	{
		$this->db->where($condition);
	}
	return $this->db->update($table,$data); 
}
   
}
?>