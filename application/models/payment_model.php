<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment_model extends CI_Model {
	
	public function insert_data($data, $table)
	{ 
		   return $this->db->insert($table,$data); 
	}
	
    public function get_data($condition="", $table="",$order="",$limit="")
	{ 
		  
		 if(!empty($condition))
		 {
			 $this->db->where($condition);
		 }
		 
		if(!empty($order))
		 {
			 $this->db->order_by($order, 'DESC');
		 }
		 if(!empty($limit))
		 {
			 $this->db->limit($limit,0);
		 }
		return $this->db->get($table); 
	}
	
	public function update_data($condition, $table, $data)
	{ 
	  if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->update($table, $data);
	}
 
	
}
?>