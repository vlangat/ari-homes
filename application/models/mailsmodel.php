<?php
class Mailsmodel extends CI_Model {
    
	function __construct()
    {
        parent::__construct();

        $this->load->library('email');
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
    }

    function insert_comm($to, $from, $message, $subject)
    {
		print_r($this->email);
        $this->email->to($to);
		$this->email->subject($subject);
        $this->email->from($from);
		$this->email->message( $this->emailcampain( $message ) );
        $this->email->send();

    }

     function emailcampain($message){
        return '<html>
                <head>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
            		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet" type="text/css">
					<style>
            			body{ background-color:#FAFAFA; width: 50%; margin: 0 auto; padding: 0;  border: solid 1px #f7931d; border-bottom: solid 1px #000; border-radius: 5px }
						header{ height: 60px; background:#f7931d; opacity: 1}
						header img{ padding: 10px}
						footer{ height: 40px; background: #000; color: #fff; text-align center border-bottom: solid 1px #000; }
						footer p{ padding: 10px; text-align: center }
            		</style>
            	</head>
            	<body>
					<header>
						<img src="http://localhost:90/matumizi/images/logo.png">
					</header>
            		<div class="container"><br>
						<div style="text-align: left; padding-left: 30px; padding-top:2px; padding-bottom:10px; line-height:2em;">
						   '. $message .'
						</div><br>
            		</div>
					<footer>
						<p>&copy; 2017 Health Systems Innovations Ke</p>
					</footer>
            	</body>
            </html>';
    }
}