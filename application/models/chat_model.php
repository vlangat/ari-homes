<?php
class Chat_model extends CI_Model {  
  
	function add_message($data="")
	{ 
		$this->db->insert('chat_messages', $data);
	}

 public function update_data($condition="",$data)
 {
	 if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->update($table="chat_messages", $data);
 }
 
 public function get_data($table, $condition,$order_by="",$asc_desc="DESC",$limit="")
	{
	  	if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		if(!empty($order_by))
		{
			$this->db->order_by($order_by,$asc_desc);
		} 
    if(!empty($limit))
		{
			$this->db->limit($limit); 
		} 
 
		return  $this->db->get($table); 
	} 
	
	
	public function get_new_message($table, $condition,$order_by="",$asc_desc="DESC",$limit="", $receiver="")
	{
	  	if(!empty($condition))
		{
			$this->db->where($condition); 
		} 
		if(!empty($receiver))
		{ 
			$this->db->or_where('receiver',$receiver); 
		} 
		if(!empty($order_by))
		{
			$this->db->order_by($order_by,$asc_desc);
		} 
    if(!empty($limit))
		{
			$this->db->limit($limit); 
		} 
 
		return  $this->db->get($table); 
	} 
	
	
	function get_messages($timestamp,$tokenId,$receiver="")
	{ 
		$this->db->where(array('tokenId'=>$tokenId,'timestamp >'=>$timestamp));
		//$this->db->where(array('email'=>$email));
		if(!empty($receiver))
		{ 
			$this->db->or_where_in(array('timestamp >'=>$timestamp,'chatType'=>2,'receiver'=>$receiver));  
		 } 
		$this->db->order_by('timestamp', 'DESC');
		$this->db->limit(10); 
		  $query = $this->db->get('messages_view'); 
		 return array_reverse($query->result_array());
	}
	
	function fetch_recent_messages($condition="")
	{ 
		$this->db->where($condition); 
		$this->db->order_by('id', 'DESC');
		$this->db->limit(5); 
		$query = $this->db->get('messages_view'); 
		return array_reverse($query->result_array());
	}

}
?>