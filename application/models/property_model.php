<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Property_model extends CI_Model { 

	public function insert_data($table,$data)
	{ 
		$this->db->where($data);
		$this->db->get($table);
		 if($this->db->affected_rows()>0)
		 {
			$this->db->where($data);
			return $this->db->update($table,$data);
		 }
		else{
				return $this->db->insert($table,$data);
			}  
	}   
	 
	public function update_data($table, $condition,$data)
	{
			if(!empty($condition))
			{
				$this->db->where($condition);
				return  $this->db->update($table,$data);
			} 
	} 
	 
	public function get_data($table,$condition="", $order_by="",$asc_desc="DESC",$limit="")
	{  
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
        if(!empty($order_by))
		{
			$this->db->order_by($order_by,$asc_desc);
        } 
		if($table=="unit_property_details" || $table=="property_category")
		{ 
			$this->db->distinct(); 
		}
		if(!empty($limit)){ $this->db->limit($limit); }
		return $this->db->get($table);
	}
	  
	public function delete_data($table,$condition="")
	{ 
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return  $this->db->update($table,array('audit_number'=>2));  
	} 
	
}

?>