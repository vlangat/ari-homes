<?php
class Ipnmodel extends CI_Model {

	var $payment_mode="";
	var $receipt="";
	var $time;
	var $phonenumber="";
	var $name="";
	var $account="";
	var $status;
	var $amount;
	var $note="";
 
	public function get($condition="")
	{ 
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('payment_tbl');
	}
 
	public function insert()
	{
		$this->status=0;
		$this->payment_mode = $this->input->post('mode');
		$this->receipt = $this->input->post('ref');
		$this->time = $this->input->post('timestamp');
		$this->phonenumber = $this->input->post('phone');
		$this->name = $this->input->post('name');
		$this->account = $this->input->post('account');
		$this->amount = $this->input->post('amount');
		$this->note = $this->input->post('sms');
		
		
		if(!empty($this->name) && !empty($this->amount))
			return $this->db->insert('payment_tbl',$this);
		else
			return -1;
	}
}