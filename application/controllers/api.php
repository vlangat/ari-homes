<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	var $tokenId=null;
	public function __construct()
	{
		parent::__construct(); 
		$this->load->library('email');
		$this->load->helper('email');
		$this->load->helper('security'); 
		 
		$this->load->library('session');  
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Africa/Nairobi');
		$this->load->model('Chat_model');
		$this->load->library('sendSms'); 
	}
	
	
	public function send_message()
	{
		$message = $this->input->get('message', null); 
		$token_id = $this->input->get('token', null); 
		//$nickname =$this->session->userdata('first_name');  
		$email =$this->session->userdata('email');  
		$guid = $this->input->get('guid', '');
		$i=0;
		if($token_id==""){   
		$q=$this->Chat_model->get_data($table="chat_messages",array('email'=>$email,'chatType'=>1,'status'=>'open'));
		if($q->num_rows()<=0){ $token_id=rand(1000,20000); }else{ foreach($q->result() as $r){$token_id=$r->tokenId; $i++;}}
		$this->tokenId=$token_id; $token_id=$this->tokenId; $this->session->set_userdata('tokenId',$token_id);
		}
		$data = array(
			'message'=> (string) $message, 
			'email'	=> (string) $email,
			'guid'=> (string)	$guid,
			'timestamp'	=> time(),
			'chatType'=>1,
			'status'=>'open',
			'tokenId'=>$token_id
		);
		if($i==0){
		$obj=new sendSms();
		//$obj->send_sms($receipients="+254789732828","You have a new chat message from ".$this->session->userdata('first_name')." Email(".$email."). Login to Admin to reply back",$from="ARI");
		$obj->send_sms($receipients="+254736822568","You have a new chat message from ".$this->session->userdata('first_name')." Email(".$email."). Login to Admin to reply back",$from="ARI");
		}
		$this->Chat_model->add_message($data);
		
		$this->_setOutput($message);
	}
	
	public function send_admin_message()
	{
	    $token_id="";
		$receiver = $this->input->get('receiver', null); 
		$message = $this->input->get('message', null); 
		$token_id = $this->input->get('token', null); 
		$chatType = 2; 
		//$nickname =$this->session->userdata('first_name');  
		$email =$this->session->userdata('email');  
		$guid = $this->input->get('guid', '');
		if($token_id==""){
		$q=$this->Chat_model->get_data($table="chat_messages",array('email'=>$email,'chatType'=>1,'status'=>'open'));
		if($q->num_rows()<=0){ $token_id=rand(1000,20000); }else{ foreach($q->result() as $r){$token_id=$r->tokenId;}}
		$this->tokenId=$token_id;$this->session->set_userdata('tokenId',$token_id);
		}
		$data = array(
			'message'	=> (string) $message, 
			'email'	=> (string) $email,
			'guid'		=> (string)	$guid,
			'timestamp'	=> time(),
			'chatType'=>$chatType,
			'status'=>'open',
			'receiver'=>$receiver,
			'tokenId'=>$token_id
		);
		$this->Chat_model->add_message($data);		
		$this->_setOutput($message);
	}
	
	
	public function get_messages()
	{
		$timestamp = $this->input->get('timestamp', null);
		$tokenId=$this->input->get('token', null); 
		if($tokenId==""){ $tokenId=$this->session->userdata('tokenId'); }		
		$receiver=$this->session->userdata('email');		
		if($tokenId !="")
		{  
			$messages = $this->Chat_model->get_messages($timestamp,$tokenId,$receiver=""); 
			$this->_setOutput($messages);
		}
	}
	
	public function get_prev_messages()
	{    	$tokenId=""; $receiver=$this->session->userdata('email');
		    $q=$this->Chat_model->get_new_message($table="chat_messages",array('email'=>$this->session->userdata('email'),'chatType'=>1,'status'=>'open'),$order_by="id",$asc_desc="ASC",$limit="1", $receiver);
			if($q->num_rows()>0){  foreach($q->result() as $r){  $tokenId=$r->tokenId; } }
			 
			$messages = $this->Chat_model->fetch_recent_messages(array('status'=>'open','tokenId'=>$tokenId));
		  
		  $this->_setOutput($messages);
		
	}
	
	public function get_customer_messages()
	{
	
		$timestamp = $this->input->get('timestamp', null);
		$tokenId=$this->input->get('tokenId', null);		
		if($tokenId !="")
		{  
			$messages = $this->Chat_model->get_messages($timestamp,$tokenId); 
		}
		else{   
			$q=$this->Chat_model->get_data($table="chat_messages",array('email'=>$this->session->userdata('email'),'chatType'=>1,'status'=>'open'),$order_by="id",$asc_desc="DESC",$limit="1");
			if($q->num_rows()>0){  foreach($q->result() as $r){  $tokenId=$r->tokenId; } }
			 
			$messages = $this->Chat_model->fetch_recent_messages(array('status'=>'open','tokenId'=>$tokenId));
		    } 
		 
		  $this->_setOutput($messages);
    }
    
    
	public function get_admin_messages()
	{
		$timestamp = $this->input->get('timestamp', null);
		$tokenId=$this->input->get('tokenId');		
		if($timestamp !=""){
			 $messages = $this->Chat_model->get_messages($timestamp,$tokenId); 
		   }else
		   {
			$messages = $this->Chat_model->fetch_recent_messages(array('tokenId'=>$tokenId));    
		   }
		//$messages = $this->Chat_model->fetch_recent_messages(); 
		$this->_setOutput($messages);
	}
	
	public function getNewChats()
	{  
		$count=0; $tokenId="";
		$email=$this->session->userdata('email');
		$r=$this->Chat_model->get_data($table="chat_messages", $condition=array('email'=>$email,'status'=>'open'));
		$c=$this->Chat_model->get_data($table="chat_messages", $condition=array('receiver'=>$email,'chatType'=>2,'seen'=>0));
		$b=$this->Chat_model->get_new_message($table="chat_messages", $condition=array('email'=>$email,'chatType'=>2,'seen'=>0), $ordey_by="id", $asc_desc="DESC", $limit="1",$email);
		foreach($b->result() as $u){  $tokenId=$u->tokenId; }
		foreach($c->result() as $z){ $count++; }
		if($r->num_rows()>0)
		{
			//foreach($r->result() as $s){ $tokenId=$s->tokenId;}
			$q=$this->Chat_model->get_data($table="chat_messages", $condition=array('tokenId'=>$tokenId,'chatType'=>2,'seen'=>0));
			foreach($q->result() as $v){ $count++;  }
			echo json_encode(array('result'=>"ok",'count'=>$count,'tokenId'=>$tokenId));
		}
		else{  echo json_encode(array('result'=>"ok",'count'=>$count,'tokenId'=>$tokenId)); } 
	}
	
	public function markChat($id="")
	{
		if($id==""){ echo json_encode(array('result'=>"false")); return; }
		if($this->Chat_model->update_data($condition=array('seen'=>0,'chatType'=>2,'tokenId'=>$id), $data=array('seen'=>1)))
		if($this->Chat_model->update_data($condition=array('seen'=>0,'chatType'=>1,'tokenId'=>$id), $data=array('seen'=>1)))
		{
			 echo json_encode(array('result'=>"ok"));
		}
		else{ echo json_encode(array('result'=>"false"));}
	}
	
public function getNewAdminMessages()
{    	
	if($this->session->userdata('admin_id') ==""){  show_404(); }
	$q=$this->Chat_model->get_data($table="chat_messages",array('chatType'=>1,'status'=>'open','seen'=>0),$order_by="id");
	if($q->num_rows()>0)
	{
	 echo json_encode(array('result'=>"ok",'data'=> $q->result_array()));
	}
	else
	{
	 echo json_encode(array('result'=>"false"));
	}

}
	
	private function _setOutput($data)
	{
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		
		echo json_encode($data);
	}
}

?>