<?php
class Tables extends CI_Controller{

    public function __construct()
	{
		parent::__construct();
        $this->load->library('Datatables');
		$this->load->model("Rent_model"); 
		//if($this->session->userdata('admin_id') ==""){redirect(base_url().'admin');}
	} 
     
	/*function login_history()
	{
        $this->datatables->select('login_ip_history.id,login_ip_history.login_time, CONCAT(user_info.first_name," ", user_info.last_name) AS name,user_info.email,login_ip_history.ip_address')
                    ->from('login_ip_history')
                    ->join('user_info','login_ip_history.user_id=user_info.id','left') 
                    ->order_by('login_ip_history.id','DESC');
        echo $this->datatables->generate();
    } 
	*/
	function audit_trail()
	{
			$this->db->select('id,create_date,user_id,ip_address,message,status')
			->from('logs')
			->order_by('id','DESC');					
			$q=$this->db->get();
		 echo json_encode(array('data'=>$q->result_array()));
    }
	
	function login_history()
	{
				$this->db->select('login_ip_history.login_time,  user_info.first_name, user_info.last_name,user_info.email,login_ip_history.ip_address')
                    ->from('login_ip_history')
                    ->join('user_info','login_ip_history.user_id=user_info.id','left') 
                    ->order_by('login_ip_history.id','DESC');
         $q=$this->db->get();
		 echo json_encode(array('data'=>$q->result_array()));
    } 
	
	public function properties()
	{
			$this->db->select('*')
			->from('property_units_details')
			->where(array('company_code'=>$this->session->userdata('company_code')))
			->order_by('id','DESC');
			$q=$this->db->get();
			$items = array();  
		foreach($q->result_array() as $u)
			 {  
				$encrypted_string = $this->encrypt->encode($u['id']);		
	            $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
				array_push($items,array(
				"id"=>$u['id'],
				"audit_number"=>$u['audit_number'],
				"property_deleted"=>$u['property_deleted'],
				"total_units"=>$u['total_units'],
				"property_name"=>$u['property_name'],
				"property_type"=>$u['property_type'],
				"landlord"=>$u['landlord'],
				"lr_no"=>$u['lr_no'],
				"floors"=>$u['floors'],
				"car_spaces"=>$u['car_spaces'],
				"location"=>$u['location'],
				"street"=>$u['street'],
				"town"=>$u['town'],  
				"enct"=>$enct
				));
			 }
		 if($q->num_rows() >0){ echo json_encode(array('result'=>"ok",'data'=>$items)); }
		else{ echo json_encode(array('result'=>"false",'msg'=>'Bad Request','data'=>'' )); }
	}
	 
function tenants()
	{
			$this->db->select('tenant_property_units.id,tenant_property_units.audit_number,tenant_property_units.first_name,tenant_property_units.tenant_type,tenant_property_units.property_name,tenant_property_units.category_name,tenant_property_units.house_no,tenant_property_units.floor_no,tenant_property_units.mobile_number,tenant_property_units.company_name,tenant_current_account.total_amount AS rent_due')
			->from('tenant_property_units')
			->where(array('tenant_property_units.audit_number'=>1,'tenant_property_units.tenant_status'=>1,'tenant_property_units.company_code'=>$this->session->userdata('company_code')))
			->join('tenant_current_account','tenant_property_units.id=tenant_current_account.tenant_id','left') 
			->order_by('tenant_property_units.id','DESC');
			$q=$this->db->get();
			$items = array();  
		foreach($q->result_array() as $u)
			 {  
				$encrypted_string = $this->encrypt->encode($u['id']);		
	            $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
				array_push($items,array(
				"id"=>$u['id'],
				"audit_number"=>$u['audit_number'],
				"first_name"=>$u['first_name'],
				"tenant_type"=>$u['tenant_type'],
				"property_name"=>$u['property_name'],
				"category_name"=>$u['category_name'],
				"house_no"=>$u['house_no'],
				"floor_no"=>$u['floor_no'],
				"mobile_number"=>$u['mobile_number'],
				"company_name"=>$u['company_name'],
				"rent_due"=>$u['rent_due'],
				"enct"=>$enct,
				));
			 }
			 if($q->num_rows() >0){ echo json_encode(array('result'=>"ok",'data'=>$items)); }
		else{ echo json_encode(array('result'=>"false",'msg'=>'Bad Request','data'=>'' )); }
					
	}
	
	function receipts($status="")
	{
		    $condition=array('audit_number'=>1,'status'=>0,'type'=>'c','company_code'=>$this->session->userdata('company_code'));
			if($status == "deleted"){$condition=array('audit_number'=>2,'status'=>1,'status'=>1,'type'=>'c','company_code'=>$this->session->userdata('company_code'));}
					$this->db->select('*')
				   ->from('tenant_transaction_view')
                    ->where($condition)
                    ->order_by('transaction_id','DESC');
					$q=$this->db->get();
					$items = array();  
		foreach($q->result_array() as $u)
			 {  
				$encrypted_string = $this->encrypt->encode($u['transaction_id']);		
	            $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
				array_push($items,array(
				"transaction_id"=>$u['transaction_id'],
				"audit_number"=>$u['audit_number'],
				"is_deleted"=>$u['is_deleted'],
				"delete_reason"=>$u['delete_reason'],
				"date_paid"=>$u['date_paid'],
				"amount"=>$u['amount'],
				"tenant_id"=>$u['tenant_id'],
				"payment_mode"=>$u['payment_mode'],
				"payment_mode_code"=>$u['payment_mode_code'],
				"receipt_no"=>$u['receipt_no'],
				"description"=>$u['description'],
				"first_name"=>$u['first_name'],
				"type"=>$u['type'], 
				"receipt"=>$u['receipt'], 
				"house_no"=>$u['house_no'], 
				"property_name"=>$u['property_name'], 
				"floor_no"=>$u['floor_no'], 
				"enct"=>$enct
				));
			 }
			echo json_encode(array('data'=>$items));
		}
	 
	public function invoice()
	{
			$condition=array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'));
			$this->db->select('*')
			->from('tenant_invoice_statement')
			->where($condition)
			->order_by('id','DESC');
			$q=$this->db->get();
			$items = array();  
			foreach($q->result_array() as $u)
			 {  
				$encrypted_string = $this->encrypt->encode($u['tenant_id']);		
	            $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
				$encr = $this->encrypt->encode($u['invoice_no']);		
	            $enct_invoice=str_replace(array('+', '/', '='), array('-', '_', ''), $encr);
				array_push($items,array(
				"id"=>$u['id'],
				"invoice_no"=>$u['invoice_no'],
				"audit_number"=>1,
				"date_generated"=>$u['date_generated'],
				"first_name"=> $u['first_name'],
				"property_name" => $u['property_name'],
				"house_no" => $u['house_no'],
				"floor_no" => $u['floor_no'],
				"amount" => $u['amount'],
				"enct_invoice" => $enct_invoice,
				"enct" => $enct));
			 }
		echo json_encode(array('data'=>$items));
	}
	
	
	function paid_rent($page="unpaid_rent")
	{
			$condition=array('type'=>'c','audit_number'=>1,'company_code'=>$this->session->userdata('company_code'));
			$this->db->select('*')
			->from('tenant_transaction_view')
			->where($condition)
			->order_by('transaction_id','DESC');
			$q=$this->db->get();
			$items = array();  
			 
			foreach($q->result_array() as $u)
			 {  $amount=0;
				$encrypted_string = $this->encrypt->encode($u['tenant_id']);		
	            $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
			    $q=$this->Rent_model->unpaid_rent(array('tenant_property_units.audit_number'=>1,'company_code'=>$this->session->userdata('company_code')));  
			    foreach($q->result() as $row){ if($u['tenant_id']==$row->tenant_id){ $amount=$row->total_amount;} }
				if($page=="paid"){  $amount=$u['amount']; }
				array_push($items,
				array(
				"transaction_id"=>$u['transaction_id'], 
				"tenant_type"=>$u['tenant_type'], 
				"first_name"=>$u['first_name'],
				"mobile_number"=>$u['mobile_number'],
				"email"=>$u['tenant_email'],
				"company_name"=>$u['company_name'],
				"payment_mode"=>$u['payment_mode'],
				"property_name"=>$u['property_name'], 
				"house_no"=>$u['house_no'],
				"floor_no"=>$u['floor_no'],
				"amount"=>$amount,
				"enct"=>$enct,
				"date_paid"=>$u['date_paid']
				));
			 }
			 if($q->num_rows() >0){ echo json_encode(array('result'=>"ok",'data'=>$items)); }
		else{ echo json_encode(array('result'=>"false",'msg'=>'Bad Request','data'=>'')); }
	}
	
	function suppliers()
	{
			$condition=array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'));
			$this->db->select('*')
			->from('suppliers')
			->where($condition)
			->order_by('id','DESC');
			$q=$this->db->get();
			if($q->num_rows() >0){ echo json_encode(array('result'=>"ok",'data'=>$q->result_array())); }
		else{ echo json_encode(array('result'=>"false",'msg'=>'Bad Request','data'=>'')); }
	}
	
}
?>