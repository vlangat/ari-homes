<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TenantSignUp extends CI_Controller {
	var $disabled="";
	 var $user_ip=null; 
	public function __construct()
	{
		parent::__construct();  
		$this->load->model("Authentication_model");
		$this->load->model("Property_model"); 
		$this->load->model("Tenant_model"); 
		$this->load->model("Rent_model"); 
		$this->load->model("Payment_model"); 
		$this->load->library('upload');
		$this->load->library('Expired');
		$this->load->helper('security');
		$this->load->library('rent_class');
		$this->load->library('email');
		$this->load->library('Encrypte');
		date_default_timezone_set('Africa/Nairobi');
		$this->user_ip=$this->input->ip_address();
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry();
	}
	public function index()
	{
		$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id");
		$data['current_acc']=$this->Property_model->get_data($table="current_account", array('audit_number'=>1));
		$this->load->view('accounts/header');
		$this->load->view('accounts/viewTenants',$data);
		$this->load->view('accounts/footer');
	}
	
  public function getId()
	{
		$id=$this->input->post('id');
		if($this->Authentication_model->update_data("user_info", array('id'=>$this->session->userdata('id')), array('national_id'=>$id)))
		{
			echo json_encode(array('result'=>'ok'));
		}
		else
		{
			echo json_encode(array('result'=>'false'));
		} 
	}
	
public function statement($enct="")
	{ 
		$hashed=new Encrypte(); $national_id=""; $id="";
		if($enct !="")
		{
			$id=$hashed->hashId($enct, $table="tenant",  $condition=array('audit_number'=>1),$searchItem="id");		
		}
		
		$q=$this->Rent_model->getData($table="user_info", array('id'=>$this->session->userdata('id')));
		if($q->num_rows()>0)
		{
			foreach($q->result() as $r){  $national_id=$r->national_id;	}
		} 		
		if($national_id !="")
		{  
			$p=$this->Rent_model->getData($table="tenant", array('id_passport_number'=>$national_id));
			foreach($p->result() as $r){	$id=$r->id;	}   
		}
		
		$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
		$enct =base64_encode(do_hash($id . $salt,'sha512') . $salt);
		$data['id']=$enct;  
		$data['national_id']=$national_id;  
		$data['units_details']= $this->Rent_model->getData($table="unit_property_details"); 
		$data['statement']="";
		if($id !=""){
			$data['statement']= $this->Rent_model->get_statement($condition=array('tenant_id'=>$id)); 
		}
		if($this->input->post('from') !="" && $this->input->post('to') !="")
		{ 
			$from=$this->input->post('from'); 
			$to=$this->input->post('to');
			$data['statement']=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1),$from,$to);
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1),$from,$to);
			if($q->num_rows()<=0){  $data['statement']= $this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1));  }
		}  
		$bal=0;$curr_bal=0; $credit=0; $debit=0; 
		 
		$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id));
		 
			if($q->num_rows()<=0)
			{
				foreach($q->result() as $row)
				{
					if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
					if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
				}
			}
			$bal=$debit-$credit;
			$this->Rent_model->update_data($table="current_account", array('balance'=>$bal), array('tenant_id'=>$id));
			$data['rent_due']=$this->Rent_model->getData($table="current_account",  array('tenant_id'=>$id));
			$data['users']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'user_added_by'=>0), $table="user_info");
			$data['company_info']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')), $table="company_info");
			$this->load->view('tenant/header');
			$this->load->view('tenant/statement',$data);
			$this->load->view('accounts/footer'); 
	}

public function tenant_receipts()
	{
		$national_id=$this->session->userdata('national_id'); $tenant_id="";
		$p=$this->Rent_model->getData($table="tenant", array('id_passport_number'=>$national_id));
		foreach($p->result() as $r){  $tenant_id=$r->id;  }  
	//	$data['tenants']=$this->Tenant_model->showTenants(array('unit_propnerty_details.audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant.id'=>$tenant_id,'tenant.tenant_status'=>1));
$data['tenants']=$this->Tenant_model->get_data($table="tenant", array('audit_number'=>1,'tenant.id'=>$tenant_id,'tenant.tenant_status'=>1));
	//	$data['received_rent']=$this->Rent_model->received_pay(array('tenant_tmransaction.audit_number'=>1,'tenant_id'=>$tenant_id,'tenant_transaction.type'=>'c','tenant_property_units.company_code'=>$this->session->userdata('company_code')));
	$data['received_rent']=$this->Rent_model->getData($table="tenant_transaction",array('audit_number'=>1,'tenant_id'=>$tenant_id,'tenant_transaction.type'=>'c'));
		$data['disabled']=$this->disabled;
		$this->load->view('tenant/header');
		$this->load->view('tenant/rent_receipts',$data);
		$this->load->view('accounts/footer');
	}
	
	public function printTenantReceipt($id="",$tenant_id="",$receipt="")
	{
		$hashed=new Encrypte();
		$id=$hashed->hashId($id, $table="tenant_transaction",  $condition=array('audit_number'=>1),$searchItem="id");		
		$current_reading=0; $previous_reading=0; $water_cost=0; $balance=0; 
	    if($tenant_id=="")
		{  
			$q=$this->Rent_model->getData($table="tenant_transaction", array('id'=>$id));
		}
		else if($id=="")
		{ 
			$tenant_id=$hashed->hashId($tenant_id, $table="tenant",  $condition=array('audit_number'=>1),$searchItem="id");		
			$receipt_no=$hashed->hashId($receipt, $table="tenant_transaction",  $condition=array('audit_number'=>1),$searchItem="receipt_no");		
		    
			$q=$this->Rent_model->getData($table="tenant_transaction", array('tenant_id'=>$tenant_id,'receipt_no'=>$receipt_no));
		}
		
	 if($q->num_rows()>0)
		{
		 	foreach($q->result() as $row){  $tenant_transaction_id=$row->id; $balance=$row->balance; $payment_mode=$row->payment_mode;$amount=$row->amount; $receipt_no=$row->receipt_no;$payment_mode_code=$row->payment_mode_code; $tenant_id=$row->tenant_id; $date=$row->date_paid;$pay_for='internet';}
		    $this->session->set_userdata(array('amount'=>$amount,'date_paid'=>$date,'pay_for'=>$pay_for,'payment_mode'=>$payment_mode,'receipt_no'=>$receipt_no));
			$data['amount']=$amount; $data['date_paid']=$date; $data['pay_for']=$pay_for;$data['balance']=$balance; $data['payment_mode']=$payment_mode; $data['receipt_no']=$receipt_no; $data['payment_mode_code']=$payment_mode_code; 
			$data['tenant_details']=$this->Rent_model->getData($table="tenant", array('id'=>$tenant_id));
		    $data['company_details']=$this->Rent_model->getData($table="company_info");
		    $data['agent_details']=$this->Rent_model->getData($table="user_info");
		    $data['receipt_items']=$this->Rent_model->get_receipt($tenant_transaction_id);
			$data['statement']= $this->Rent_model->get_statement($condition=array('tenant_id'=>$tenant_id)); 
			$data['current_balance']=$this->get_balance($tenant_id);
            $month=date("m",strtotime($date));			
            $year=date("Y",strtotime($date));			
			$water_cost=$this->getWaterBill($tenant_id,$month,$year);
			$query=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$tenant_id,'month'=>$month,'year'=>$year));
		   if($query->num_rows()>0)
			  {
				foreach($query->result() as $row)
				{
					 $current_reading=$row->current_reading; $previous_reading=$row->previous_reading; 
				}
			  }
			$data['water_cost']=$water_cost; 
			$data['current_reading']=$current_reading; $data['previous_reading']=$previous_reading; 
			$data['receipt_no']=$receipt_no;  
			$this->load->view('tenant/header');
			$this->load->view('tenant/receipt', $data);
			$this->load->view('accounts/footer'); 
		 
		}
		else{
				redirect(base_url().'tenantSignUp/tenant_receipts');
			}    
		 
		}

 
  public function referredAgents()
  {
		$national_id="";
		$p=$this->Authentication_model->get_data($table="user_info", array('id'=>$this->session->userdata('id')));
		if($p->num_rows()>0){ foreach($p->result() as $s){ $national_id=$s->national_id;}}
		$data['agents']=$this->Tenant_model->get_data($table="referred_agents", array('added_by'=>$national_id)); 
		$data['agents_details']=$this->Tenant_model->get_data($table="company_info");
		$data['tenants']=$this->Tenant_model->get_data($table="tenant",array('audit_number'=>1));
		$data['tenant_details']=$this->Rent_model->getData($table="tenant_property_units", array('id_passport_number'=>$national_id));
		$data['company_details']=$this->Rent_model->getData($table="company_info");
		$data['agency_details']=$this->Rent_model->getData($table="user_info");
		$this->load->view('tenant/header');
		$this->load->view('tenant/referred_agents',$data);
		$this->load->view('accounts/footer'); 
  }
  
  
  public function getAgent($national_id="")
  { 
		$p=$this->Tenant_model->get_data($table="tenant", array('id_passport_number'=>$national_id));
		 
		if($p->num_rows()>0)
		{ 
			foreach($p->result() as $r){  $tenant_id=$r->id; }
			$data=$this->Tenant_model->get_data($table="tenant_property_units", array('id_passport_number'=>$national_id));
			echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
		/*$this->load->view('tenant/header');
		$this->load->view('tenant/agent',$data);
		$this->load->view('accounts/footer'); */
   } 
 
public function profile()
	{
		$data['company']=$this->Authentication_model->get_data($table="company_info", array('company_code'=>$this->session->userdata('company_code')));
		$data['data']=$this->Authentication_model->get(array('email'=>$this->session->userdata('email'),'id'=>$this->session->userdata('id'),'company_code'=>$this->session->userdata('company_code')));
		$this->load->view('tenant/header');
		$this->load->view('tenant/singleUserAccount',$data);
		$this->load->view('accounts/footer');
	}
	
public function changePhoto($table="")
	{
		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
 
		//$this->load->library('upload', $config);
		$this->upload->initialize($config); //use initialize  when upload library is set to autoload
		if (!$this->upload->do_upload('photo'))
		{
			$error = array('error' => $this->upload->display_errors());
			$status=$this->session->set_flashdata('temp',' <font color="red">There was an error in saving Image </font> ');	 
		}
		else
		{
		$condition=array('id'=>$this->session->userdata('id'));
		$upload_data = $this->upload->data(); 
		$file_name =   $upload_data['file_name'];
		if($table==""){$table="user_info";}
		$this->Authentication_model->update_data($table ,$condition,$data=array('photo'=>$file_name));
		$data = array('upload_data' => $this->upload->data());
		$this->session->set_userdata('photo', $file_name);
		$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="change profile photo",$status=1);
		  
		$status=$this->session->set_flashdata('temp','Photo uploaded successfully');
		}
            
		 redirect(base_url() ."TenantSignUp/profile#tab_1_2");
             
	}
////end of upload function


	public function do_upload($id,$national_id=""){  
		$config['upload_path'] = 'media/'; 
		$config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx';
		$config['max_size']	= '5000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
        $this->upload->initialize($config);
		$targetDir='./media/';   //directory name
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);
		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i]; 
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			//$this->upload->initialize($this->set_upload_options());
			$this->upload->do_upload();
			//get file name
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
			$targetFile = $targetDir.$national_id."_".$fileName; 
			if(move_uploaded_file($_FILES['userfile']['tmp_name'],$targetFile))
			{
				//check if the file exist 
				$check=$this->Property_model->check_file(array('media'=>$national_id."_".$fileName,'tenant_id'=>$id));
				if($check->num_rows()<=0){   
					// upload files to images directiry and also  to the database
					$data=array('media'=>$national_id."_".$fileName,'tenant_id'=>$id,'date_added'=>date("Y-m-d H:i:s"));
					//pass parameter with data to model
					$this->Tenant_model->insert_documents($data); 
				}
				
			}
			}
			$fileName = implode(',',$images); 
				  
		}
 
		public function get_balance($id)
	{
		 $credit=0; $debit=0; 
		 $check_bal=$this->Rent_model->getData($table="tenant_transaction" , array('tenant_id'=>$id));
		 if($check_bal->num_rows()>0){ foreach($check_bal->result() as $balance){ if($balance->type=="c"){$credit=$credit+$balance->amount;}else if($balance->type=="d"){ $debit=$debit+$balance->amount;} } } 
		 //if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit){$curr_bal= $debit-$credit;}
		 $curr_bal= $debit-$credit;
		 return  $curr_bal;
	}

public function getWaterBill($tenant_id="", $month="", $year="")
	{ 
		  if($month==""){ $month=date("m");}  if($year==""){ $year=date("Y"); }
		  $cost=0; $current_reading=0; $previous_reading=0; $water_unit_cost=0;
		  $query=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$tenant_id,'month'=>$month,'year'=>$year));
		  $q=$this->Tenant_model->get_data($table="tenant_property_units", array('id'=>$tenant_id));
		  
		  if($query->num_rows()>0)
		  {
			foreach($query->result() as $row)
			{
				$tenant_id=$row->tenant_id; $current_reading=$row->current_reading; $previous_reading=$row->previous_reading;
			foreach($q->result() as $w) { $water_unit_cost=$w->water_unit_cost; }
			}
		  }
		  $cost=($current_reading-$previous_reading)*$water_unit_cost;
		  
		  return  $cost;  
	}		
		
	
}