<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice extends CI_Controller {
	public function __construct()
	{
		parent::__construct();   
		$this->load->model("Rent_model");
		$this->load->model("Tenant_model"); 		
		$this->load->model("Payment_model"); 		
		$this->load->model("Property_model");
		$this->load->library('Expired');
		$this->load->library('Encrypte');
		$this->load->helper('file');
		$this->load->helper('security');
		$this->load->library('rent_class');  
		$this->load->library('send_mails');  
		$this->load->library('pdf'); 
		$this->load->library('pdf_converter/FPDF');
        date_default_timezone_set('Africa/Nairobi'); 
		$obj=new Expired();
		ini_set("memory_limit","512M");
		$this->disabled=$obj->check_subscription_expiry(); 		
	}
  
public function create_invoice()
{
	$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id");
		
	$this->load->view('accounts/header');
	$this->load->view('accounts/create_invoice',$data);
	$this->load->view('accounts/footer');
	
}

public function get_invoice_details($id="", $tenant_id="")
	{
		$data=$this->Property_model->get_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'invoice_no'=>$id,'audit_number'=>1));	
		if($data->num_rows()>0)
		{ 
			 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
public function remove_invoice_item($id="")
	{ 
		
		$data=$this->Property_model->update_data($table="tenant_pricing", array('id'=>$id), array('audit_number'=>2));	
		$data=$this->Property_model->get_data($table="tenant_pricing", array('id'=>$id,'audit_number'=>1));	
		if($data->num_rows()==0)
		{ 
			 echo json_encode(array('result'=>"ok"));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
public function getNewInvoiceNo()
{
	$invoice_no=rand(12381,1238498);
	$c= $this->Rent_model->get_data("invoice", $condition=array('invoice_no'=>$invoice_no)); 
   while($c->num_rows()>0)
   {
	   
	   $invoice_no=rand(12381,1238498);
   }
	echo json_encode(array('invoice_no'=>$invoice_no));
}

public function generateInvoice()
{
		$company_code=$this->session->userdata('company_code');
		$q= $this->Rent_model->get_data("tenant_property_units",$condition=array('company_code'=>$company_code)); 
		
		$month=date('m'); $year=date('Y'); $date_created=date('d-m-Y'); $x=0;
		foreach($q->result() as $u) 
		{  $amount=0; $id=$u->id; 
		   $items=$this->listInvoiceItems($id);
		   /// check balance from previous table
			$tenant_id=$id;
            $t=$this->Property_model->get_data($table="tenant_current_account", array('tenant_id'=>$tenant_id));	
			 if($t->num_rows() ==0)
			 {  
           $data=$this->Property_model->get_data($table="current_account", array('tenant_id'=>$tenant_id));	
			 
				$y = $data->row();  
				$expected_pay_date=$y->expected_pay_date;
				$balance=$y->balance;  
				$dt=array('tenant_id'=>$tenant_id,
							'rent_balance'=>$balance,
							'total_amount'=>$balance,
							//`last_pay_date` DATE NULL DEFAULT NULL,
							'expected_pay_date'=>$expected_pay_date
						);
				 
			 
				 $this->Tenant_model->insert_data($table="tenant_current_account", $dt); 
			 
			 }

///	end	   
		   $z=$this->Tenant_model->get_data($table="tenant_current_account", array('tenant_id'=>$id));
		   if($z->num_rows()>0){$e = $z->row();	  $expected_pay_date=$e->expected_pay_date;}
		   $q= $this->Rent_model->get_data("tenant_property_units", $condition=array('company_code'=>$this->session->userdata('company_code'))); 
		   $c= $this->Rent_model->get_data("invoice", $condition=array('tenant_id'=>$id,'company_code'=>$this->session->userdata('company_code'),'month'=>$month,'status'=>0,'year'=>$year)); 
		   if($c->num_rows()==0 && !empty($items))
		   {  
				foreach($items as $row){  $amount=$amount+$row['payment_type_value'];  }
				$invoice_no=rand(12381,1238498);
				if($this->Rent_model->insert_data("invoice", array('tenant_id'=>$u->id,'company_code'=>$company_code,'invoice_no'=>$invoice_no,'amount'=>$amount,'date_generated'=>$date_created,'due_date'=>$expected_pay_date,'month'=>$month,'year'=>$year,'status'=>0))>0)
					{ 
                       $x++;
				    } 
		  }else{ continue; }
		   
		 }
		 if($x>0)
		 {
			  echo json_encode(array('result'=>'ok'));
		 }
		 else{ echo json_encode(array('result'=>'false')); }
	    	 
} 
  
public function new_paymentItem()
{
	$name=$this->input->post('name');
	$description=$this->input->post('description');
	$amount=$this->input->post('amount'); 
	$category_id=$this->input->post('category_id');
	$priority=$this->input->post('priority');
	$invoice_no_post=$this->input->post('invoice_no');
	$tenant_id=$this->input->post('tenant_id');
	$is_created=$this->input->post('is_created');
	$invoice_no=""; $company_code=$this->session->userdata('company_code');
	if($is_created==0){ $invoice_no=""; }else{ $invoice_no=$invoice_no_post; }
	if($category_id==""){ $category_id=NULL;}
	$q=$this->Payment_model->get_data(array('audit_number'=>1,'unit_id'=>$category_id,'payment_type'=>$name,'tenant_id'=>$tenant_id), $table="tenant_pricing");
	if($q->num_rows()>0){ echo json_encode(array('result'=>"false",'msg'=>'Item already exist')); return; }	 	
	if($this->Payment_model->insert_data($data=array('unit_id'=>$category_id,'payment_type'=>$name,'tenant_id'=>$tenant_id,'payment_type_value'=>$amount,'is_deposit'=>0,'priority'=>$priority,'is_one_time'=>1,'invoice_no'=>$invoice_no), $table="tenant_pricing")>0)
	{
		//$bl=$this->Rent_model->getData($table="tenant_current_account" , array('tenant_id'=>$tenant_id));
		//if($bl->num_rows()>0){  foreach($bl->result() as $b){ $rent_bal=$b->rent_balance+$amount; $deposit_bal=$b->deposit_balance; } }	
		//$this->Rent_model->update_data($table="tenant_current_account",array('rent_balance'=>$rent_bal,'deposit_balance'=>($deposit_bal),'total_amount'=>(($deposit_bal+$rent_bal))), array('tenant_id'=>$tenant_id));
		//update invoice data
		if($invoice_no_post !="")
		{
			    $amnt=0; $balance=0;$bal=0;
				$inv= $this->Rent_model->get_data("invoice", $condition=array('invoice_no'=>$invoice_no_post,'company_code'=>$company_code)); 
				$rw = $inv->row();  $balance=$rw->balance;  $amnt=$rw->amount;
				//$diff=($amount-$amnt); 
			//	$bal=($balance+$diff);
				$bal=($balance+$amount); 
				//$this->Rent_model->update_data("invoice", array('amount'=>($amnt + $diff),'balance'=>$balance), array('tenant_id'=>$tenant_id,'company_code'=>$company_code,'invoice_no'=>$invoice_no_post));
				$this->Rent_model->update_data("invoice", array('balance'=>$bal), array('tenant_id'=>$tenant_id,'company_code'=>$company_code,'invoice_no'=>$invoice_no_post));
		} 
		//end 
		echo json_encode(array('result'=>"ok"));
	}
	else
	{
		echo json_encode(array('result'=>"false",'msg'=>'Item Not added. Try again later'));
	} 
} 
	
	   
public function insert_invoice_item()
{
	$item=$this->input->post('item');
	$tenant_id=$this->input->post('tenant_id');
	$description=$this->input->post('description');
	$amount=$this->input->post('amount');  
	$priority=$this->input->post('priority');
	$invoice_no=$this->input->post('invoice_no');
	$company_code=$this->session->userdata('company_code'); 
	
	$e_invoice_no = $this->encrypt->encode($invoice_no);		
	$en_invoice_no=str_replace(array('+', '/', '='), array('-', '_', ''), $e_invoice_no);
	
	$data=array( 
					'tenant_id'=>$tenant_id,
					'payment_type'=>$item,
					'payment_type_value'=>$amount,
					'description'=>$description,
					'priority'=>$priority, 
					'invoice_no'=>$invoice_no,
					'is_deposit'=>0, 
					'is_one_time'=>1
				);
	$rent_bal=0; $deposit_bal=0;
	$q=$this->Payment_model->get_data(array('audit_number'=>1,'invoice_no'=>$invoice_no,'payment_type'=>$item,'tenant_id'=>$tenant_id), $table="tenant_pricing");
	if($q->num_rows()>0){ echo json_encode(array('result'=>"false",'msg'=>'Item already exist')); return; }	 	
	if($this->Payment_model->insert_data($data, $table="tenant_pricing")>0)
	{
		//$bl=$this->Rent_model->getData($table="tenant_current_account" , array('tenant_id'=>$tenant_id));
		//if($bl->num_rows()>0){  foreach($bl->result() as $b){ $rent_bal=$b->rent_balance+$amount; $deposit_bal=$b->deposit_balance; } }	
		//$this->Rent_model->update_data($table="tenant_current_account",array('rent_balance'=>$rent_bal,'deposit_balance'=>($deposit_bal),'total_amount'=>(($deposit_bal+$rent_bal))), array('tenant_id'=>$tenant_id));
		 $inv= $this->Rent_model->get_data("invoice", $condition=array('tenant_id'=>$tenant_id,'invoice_no'=>$invoice_no,'company_code'=>$company_code)); 
		   if($inv->num_rows()==0)
		   { 
				$u=$this->Tenant_model->get_data($table="tenant_current_account", array('tenant_id'=>$tenant_id));
				$e = $u->row();	  $expected_pay_date=$e->expected_pay_date;
				$balance=$amount; 
				$this->Rent_model->insert_data("invoice", array('tenant_id'=>$tenant_id,'company_code'=>$company_code,'invoice_no'=>$invoice_no,'amount'=>$amount,'balance'=>$balance,'date_generated'=>date('d-m-Y'),'due_date'=>$expected_pay_date,'month'=>date('m'),'year'=>date('Y'),'status'=>1));
		   }
		   else	{
			      $rw = $inv->row(); $balance=$rw->balance; $amnt=$rw->amount;
				  if($amnt == $balance){
						$balance=$balance+$amount; $amnt=$balance; 
						$this->Rent_model->update_data("invoice", array('balance'=>$balance,'amount'=>$amnt), array('tenant_id'=>$tenant_id,'company_code'=>$company_code,'invoice_no'=>$invoice_no));
					}
				}
		echo json_encode(array('result'=>"ok",'en_invoice_no'=>$en_invoice_no));
	}
	else
	{
		echo json_encode(array('result'=>"false",'msg'=>'Item Not added. Try again later'));
	} 
} 
	
 
public function update_schedule_settings()
{
	$day=$this->input->post('day');
	$email=$this->input->post('email');
	$automatic=$this->input->post('automatic');
	$company_code=$this->session->userdata('company_code');	
	$data=array('automatic'=>$automatic,'day_to_sent'=>$day,'email'=>$email);
	if($this->Property_model->update_data($table="invoice_settings", array('company_code'=>$company_code), $data))
	{			
		echo json_encode(array('result'=>"ok"));
	}
	else
	{
		echo json_encode(array('result'=>"false"));
	}
}

public function get_settings()
{   
		$day=""; $automatic=""; $email="";
		$q=$this->Rent_model->get_data($table="invoice_settings", array('company_code'=>$this->session->userdata('company_code')));
		foreach($q->result() as $r){ $day=$r->day_to_sent; $automatic=$r->automatic; $email=$r->email; }			
		echo json_encode(array('result'=>'ok', 'day'=>$day,'automatic'=>$automatic,'email'=>$email));  
	 
}
	
public function update_invoice_item()
{
	$id=$this->input->post('id');
	$item=$this->input->post('item'); 
	$initial_amount=$this->input->post('initial_amount'); 
	$description=$this->input->post('description');
	$amount=$this->input->post('amount');
	$invoice_no_post=$this->input->post('invoice_no');	
	$invoice_no=""; $company_code=$this->session->userdata('company_code');	
	$data=array('payment_type'=>$item,'payment_type_value'=>$amount,'description'=>$description);
	if($this->Property_model->update_data($table="tenant_pricing", array('id'=>$id), $data))
	{	
		//update invoice data
		$tp= $this->Rent_model->get_data("tenant_pricing", $condition=array('id'=>$id)); 
		$r = $tp->row(); $invoice_no=$r->invoice_no; $tenant_id=$r->tenant_id;
		$inv= $this->Rent_model->get_data("invoice", $condition=array('tenant_id'=>$tenant_id,'invoice_no'=>$invoice_no_post,'company_code'=>$company_code)); 
		if($inv->num_rows()>0)
		{  
			if($invoice_no_post !="")
			{  
				$inv= $this->Rent_model->get_data("invoice", $condition=array('tenant_id'=>$tenant_id,'invoice_no'=>$invoice_no_post)); 
				$rw = $inv->row(); $amnt=$rw->amount; $balance=$rw->balance;
				$diff=$amount-$initial_amount;
				$amnt=($amnt+ $diff);  $balance=($balance+$diff);
			    $this->Rent_model->update_data("invoice", array('amount'=>$amnt,'balance'=>$balance), array('tenant_id'=>$tenant_id,'company_code'=>$company_code,'invoice_no'=>$invoice_no_post));
			
			} 
		}
		//end	
		echo json_encode(array('result'=>"ok"));
	}
	else
	{
		echo json_encode(array('result'=>"false"));
	}
}
	
	
public function removePaymentItem($table="tenant_pricing")
{
	$id=$this->input->post('id');
	$tenant_id=$this->input->post('tenant_id');
	$amount=$this->input->post('amount');
	$invoice_no_post=$this->input->post('invoice_no');
	$company_code=$this->session->userdata('company_code'); 
	//if($this->session->userdata('company_code')=="" || $id==""){ echo json_encode(array('result'=>"false"));  return;}
	if($this->Rent_model->update_data($table="tenant_pricing", array('audit_number'=>2),array('id'=>$id)))
	{
		//$bl=$this->Rent_model->getData($table="tenant_current_account" , array('tenant_id'=>$tenant_id));
		//if($bl->num_rows()>0){  foreach($bl->result() as $b){ $rent_bal=$b->rent_balance-$amount; $deposit_bal=$b->deposit_balance; } }	
		//$this->Rent_model->update_data($table="tenant_current_account",array('rent_balance'=>$rent_bal,'total_amount'=>(($deposit_bal+$rent_bal))), array('tenant_id'=>$tenant_id));
		$tp= $this->Rent_model->get_data("tenant_pricing", $condition=array('id'=>$id,'tenant_id'=>$tenant_id)); 
		$r = $tp->row(); $invoice_no=$r->invoice_no;
		if($invoice_no ==""){ $invoice_no=$invoice_no_post;}
		if($invoice_no !="")
		{
			$inv= $this->Rent_model->get_data("invoice", $condition=array('tenant_id'=>$tenant_id,'invoice_no'=>$invoice_no)); 
			$rw = $inv->row(); $balance=$rw->balance;  $amnt=$rw->amount;
			$amnt=($amnt- $amount); $balance=($balance-$amount);
			$this->Rent_model->update_data("invoice", array('amount'=>$amnt,'balance'=>$balance), array('tenant_id'=>$tenant_id,'company_code'=>$company_code,'invoice_no'=>$invoice_no));
					 
		}     
		echo json_encode(array('result'=>"ok"));
	}
	else
	{
		echo json_encode(array('result'=>"false"));
	}
}
 
	public function remove_invoice($id="",$status=0)
	{
		$company_code=$this->session->userdata('company_code');
		if($company_code !="")
		{
			$q=$this->Rent_model->get_data($table="invoice", array('id'=>$id));
			if($q->num_rows()>0)
			{ 
				$r = $q->row(); 
				$invoice_no=$r->invoice_no; $tenant_id=$r->tenant_id; 
				$this->Rent_model->update_data($table="invoice", array('audit_number'=>2,'is_deleted'=>1,'delete_date'=>date('d-m-Y')),array('id'=>$id,'invoice_no'=>$invoice_no,'company_code'=>$company_code));
				if($status==1)
				{
					$this->Rent_model->update_data($table="tenant_pricing", array('audit_number'=>2),array('tenant_id'=>$tenant_id,'invoice_no'=>$invoice_no));
				}
				echo json_encode(array('result'=>"ok"));
			}else{ echo json_encode(array('result'=>"false")); } 
		}
		else 
		{
			echo json_encode(array('result'=>"false")); 
		} 
	}
	 
public function tenantInvoice()
{
		//$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id");
		//$data['invoice_details']=$this->Rent_model->get_data($table="tenant_invoice_statement", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id", $limit="", $asc_desc="DESC");
		$this->load->view('accounts/header');
		$this->load->view('accounts/tenant_invoice');
		$this->load->view('accounts/footer');
}

public function viewTenantInvoice($id)
{   
		$hashed=new Encrypte(); $invoice_amount=0; $status=0; $deposit_collected=0;
		$id=$hashed->hashId($id); $invoice_month=""; $invoice_year="";	
		$q=$this->Rent_model->get_data($table="invoice", array('audit_number'=>1,'tenant_id'=>$id,'company_code'=>$this->session->userdata('company_code')),$order="id",$limit="1",$asc_desc="ASC");
		if($q->num_rows()>0){ $r=$q->row(); $status=$r->is_first_invoice; $invoice_no=$r->invoice_no; $invoice_month=$r->month; 
		$invoice_year=$r->year; $invoice_balance=$r->balance; $invoice_amount=$r->amount; $deposit_collected=$r->deposit_collected;}
		 
		$query=$this->Tenant_model->get_data("tenant_property_units", array('audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		$data['tenants']=$this->Tenant_model->get_data("tenant_property_units", array('audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		foreach($query->result() as $f){ $unit_id=$f->property_unit_id;}
		if($id=="")
		{
			$data['received_rent']="";
			$data['rent_items']="";
		}
		else
		{ 
		$data['received_rent']=$this->Rent_model->get_data("tenant_transaction_view", array('audit_number'=>1,'type'=>'c','company_code'=>$this->session->userdata('company_code')));
		$data['rent_items']= $this->listInvoiceItems($id,$status,$deposit_collected);
		 
		}
		 
		$data['company_details']=$this->Rent_model->get_data($table="company_info", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')));
		//$data['payment_for']=$this->Rent_model->payment_for();
		$data['disabled']=$this->disabled;	$data['invoice_balance']=$invoice_balance;
		$data['is_created']=0; $obj=new rent_class();		
		//$data['invoice_date']=$obj->get_balance($id);
		$balance=$obj->get_balance($id); 
        if($balance==""){ $balance=0;}		
		$data['balance']=$balance;
		$data['invoice_amount']=$invoice_amount; 
		$data['aging_report']=$this->agingReport($id); 
		 
		$data['transactions']=$this->Rent_model->get_data("tenant_transaction_view",array('tenant_id'=>$id,'type'=>'c','month_paid'=>$invoice_month,'year_paid'=>$invoice_year)); 
		$data['selected_tenant']=$this->Tenant_model->get_data("tenant_property_units", array('id'=>$id,'audit_number'=>1)); 
		$data['tenant_details']=$this->Rent_model->get_data("tenant_invoice_statement", array('tenant_id'=>$id,'audit_number'=>1),$order_by="id", $limit="", $asc_desc="DESC"); 
		$this->load->view('accounts/header');
		$this->load->view('accounts/invoice',$data);
		$this->load->view('accounts/footer'); 
}
 
 
 
public function viewNewInvoice($invoice_no)
{
		$hashed=new Encrypte(); 
		$invoice_no=$hashed->hashId($invoice_no);		
	    $q=$this->Rent_model->get_data($table="invoice", array('audit_number'=>1,'invoice_no'=>$invoice_no));
		$id=""; $invoice_no=""; $invoice_month=""; $invoice_year=""; $invoice_balance="";
		if($q->num_rows() >0)
		{ 
			$row = $q->row();
			$id=$row->tenant_id;  $invoice_no=$row->invoice_no; $invoice_month=$row->month; $invoice_year=$row->year; $invoice_balance=$row->balance; 
		}
		$query=$this->Tenant_model->get_data("tenant_property_units", array('id'=>$id,'audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		$data['tenants']=$this->Tenant_model->get_data("tenant_property_units", array('audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		foreach($query->result() as $f){ $unit_id=$f->property_unit_id;}
		if($invoice_no=="")
		{
			$data['received_rent']="";
			$data['rent_items']="";
		}
		else
		{ 
		 
		$invoiced_items = $this->Rent_model->get_data($table="tenant_pricing", array('audit_number'=>1,'invoice_no'=>$invoice_no,'is_one_time'=>1),$order_by="priority",$limit="",$asc_desc="ASC");
		 
		$it_exist=0; $amount=0;
		$items = array();  
		 
		foreach($invoiced_items->result_array() as $u)
			 {  
				 $amount=$amount+$u['payment_type_value'];
								array_push($items,array(
								"id"=>$u['id'],"audit_number"=>1,"tenant_id"=>$u['tenant_id'],
								"payment_type"=> $u['payment_type'],
								"payment_type_value" => $u['payment_type_value'],
								"description" =>$u['description'] ,
								"priority"=> $u['priority'],
								"is_deposit"=>0,
								"is_one_time"=>$u['is_one_time'],
								"invoice_no"=>$u['invoice_no']
								));
							
						//}
			}
		  
		$data['rent_items']=$items;
        }
		 if($this->Property_model->update_data($table="invoice", array('invoice_no'=>$invoice_no,'tenant_id'=>$id,'company_code'=>$this->session->userdata('company_code')), array('amount'=>$amount)));
		$data['company_details']=$this->Rent_model->get_data($table="company_info", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')));
		//$data['payment_for']=$this->Rent_model->payment_for();
		$data['disabled']=$this->disabled; 	$data['invoice_balance']=$invoice_balance;
		$data['is_created']=1;
        $obj=new rent_class();		
		$data['balance']=$obj->get_balance($id);
		$data['aging_report']=$this->agingReport($id);		
		$data['selected_tenant']=$this->Tenant_model->get_data("tenant_property_units", array('id'=>$id,'audit_number'=>1)); 
		//$data['tenant_details']=$this->Tenant_model->showTenants(array('tenant.id'=>$id,'tenant.audit_number'=>1)); 
		$data['tenant_details']=$this->Rent_model->get_data("tenant_invoice_statement", array('tenant_id'=>$id,'audit_number'=>1)); 
		$data['transactions']=$this->Rent_model->get_data("tenant_transaction_view",array('tenant_id'=>$id,'type'=>'c','month_paid'=>$invoice_month,'year_paid'=>$invoice_year)); 
		$this->load->view('accounts/header');
		$this->load->view('accounts/invoice',$data);
		$this->load->view('accounts/footer');
	
}

 
public function auto_generate_invoice()
{
		$user=$this->Tenant_model->get_data($table="user_info", array('user_added_by'=>0));
		foreach($user->result() as $user_info)
		{
			$company_code=$user_info->company_code; $subscriber_mail=$user_info->email;
			$cc=$this->Tenant_model->get_data("invoice_settings", array('company_code'=>$company_code));
			if($cc->num_rows()==0){ $this->Rent_model->insert_data("invoice_settings",array('company_code'=>$company_code));}
		}
		$q=$this->Tenant_model->get_data($table="invoice_settings", array('audit_number'=>1,'day_to_sent'=>date("d"))); 
		
		$email_invoice="no"; $automatic="yes";   $subscriber_mail="";
		if($q->num_rows()>0){
			
		foreach($q->result() as $row){
		$company_code=$row->company_code; $day=$row->day_to_sent; $email_invoice=$row->email; $automatic=$row->automatic;
		if($automatic=="yes"){
		$user=$this->Tenant_model->get_data($table="user_info", array('user_added_by'=>0,'company_code'=>$company_code));
		$user_info = $user->row();	 $subscriber_mail=$user_info->email;
		$query=$this->Tenant_model->get_data(array('tenant_property_units'),array('company_code'=>$company_code,'audit_number'=>1,'tenant_status'=>1), $order_by="id");
		foreach($query->result() as $d){
	    $id=$d->id;  $unit_id=$d->property_unit_id; $rent_frequency=$d->rent_frequency; 
		$expected_pay_date= date("Y-m-d", strtotime(date('d-m-Y')." +1 month"));
		// check if rent_frequency is Quarterly or yearly...if not proceed
		$this_month=date("m");
		if($rent_frequency=="Quarterly" || $rent_frequency=="Yearly"){  
		 
		$u=$this->Tenant_model->get_data($table="tenant_current_account", array('tenant_id'=>$id));
		$e = $u->row();	  $expected_pay_date=$e->expected_pay_date;
        $invoice_month = date("Y-m-d", strtotime("$expected_pay_date -1 month")); 
		$invoice_month=substr($invoice_month,5,2);	 
		 
		if($invoice_month !=$this_month){  continue;  }
		
		}
		//end 
		
		$rent_items= $this->listInvoiceItems($id);
		 
		$company_details=$this->Rent_model->get_data($table="company_info", array('audit_number'=>1,'company_code'=>$company_code));
		//$data['payment_for']=$this->Rent_model->payment_for();
		$data['disabled']=$this->disabled;
        $obj=new rent_class();		
		$balance=$obj->get_balance($id); 
		$data['selected_tenant']=$this->Tenant_model->get_data("tenant_property_units", array('id'=>$id,'audit_number'=>1)); 
		///
		$month=date('m'); $year=date('Y');
		$items=$this->listInvoiceItems($id);
		$c= $this->Rent_model->get_data("invoice", $condition=array('tenant_id'=>$id,'status'=>0,'month'=>$month,'year'=>$year)); 
	   if($c->num_rows()==0 && !empty($items))
	   {
		   $amount=0;
			foreach($items as $row){  $amount=$amount+$row['payment_type_value'];  }
			$invoice_no=rand(12381,1238498); $date_created=date('d-m-Y');
			$this->Rent_model->insert_data("invoice", array('tenant_id'=>$id,'company_code'=>$company_code,'invoice_no'=>$invoice_no,'amount'=>$amount,'balance'=>$amount,'date_generated'=>$date_created,'due_date'=>$expected_pay_date,'month'=>$month,'year'=>$year,'status'=>0));
	   }
		// 
		$tenant_details =$this->Rent_model->get_data("tenant_invoice_statement", array('tenant_id'=>$id,'audit_number'=>1)); 
		$content="";
		$content ='<style>table, th, td {  
		text-align: left; font-size:12px;
		border-top: 1px solid #000;
		} ul{list-style-type:none;}</style>';
		$content .= '  
		<div  style="font-size:12px;border:1px solid grey;font-family:Verdana, Geneva, sans-serif,times new roman;width:100%;">';

		$logo=""; $tenant_company_name="";$company_web=""; $mobile_number1=""; $slogan=""; $mobile_number2=""; $description=""; $company_name=""; $address=""; $country="";$company_email=""; $town="";$postal_code=""; $mobile_number=""; $company_location=""; 
 
		$c = $company_details->row();
		 if($c->company_code==$company_code)
		 { 
			$company_name=$c->company_name; $company_web=$c->company_web; $company_location=$c->location;
			$logo=$c->logo; $description=$c->description; $company_email=$c->company_email; $postal_code=$c->postal_code;
			$address=$c->address; $mobile_number=$c->mobile_number; $town=$c->town; $country=$c->country;
			$mobile_number1=$c->mobile_one; $mobile_number2=$c->mobile_two;$slogan=$c->slogan;
		 } 
		 
	$t = $tenant_details->row();  $tenant_id=$t->id;
	if($logo ==""){ $logo="ari_company_logo.png"; }  
	$content .=' 
	<table border="0" class="table table-striped table-hover" width="100%" cellpadding="8" cellspacing="2">
	<tr>
	<td>
		<img id="my_file" src="'. base_url().'media/'.$logo.'" height="75" width="150" style="display:block;max-width:230px;max-height:95px;width: auto;height: auto;" alt="'.strtoupper($company_name).'">
	</td>'; 
	if($company_name !=""){ 
	$content .='<td>
				<h4><strong>'.$company_name.'</strong></h4>
				<p>'.$description.'</p>  
				<p>'.$company_location.'</p>  	 
				<p>'.$company_web.'</p>   
				<p>&nbsp;</p> 
			</td> 
<td> 
<p><strong> Cell:</strong>	'.$mobile_number.'</p> 
<p> <strong> Email:</strong>'.$company_email.'</p>
<p><strong> '.$address."-".$postal_code.",".'</strong></p>
	<p>';
	if($town !=""){ $content .=ucfirst(strtolower($town)).", ".ucfirst(strtolower($country)); }
	$content .='</p>';
 if($mobile_number1 !="" || $mobile_number2 !=""){ 
$content .='<p><strong> Tel: </strong>	'.$mobile_number1.','.$mobile_number2.'</p>';
}
$content .='</td>';
}else{ $content .='<td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td>'; }
$content .='</tr>
</table>  

<p>  &nbsp; </p> 
<table border="0"  class="table table-striped table-hover" style=" padding:1px;border:1px solid lightgrey" cellpadding="0" cellspacing="0" width="100%"> 
	 <tr> 
		<td align="left">
			<font>  <strong> Invoice No: </strong>'. $t->invoice_no .'</font> 
		</td> 
		 <td>   &nbsp; </td> 
		<td align="right">
			<strong>   Date: </strong>'. date('d-m-Y').' &nbsp;
		</td>
	 </tr>
<tr>
<td align="left">  	
	<p> <strong>Being invoice for </strong><br/>'. $t->property_name .'
	 <br/>'. $t->category_name .'
	 <br/>  House No '.$t->house_no .'
	</p> 

</td>
<td>   &nbsp; &nbsp; </td> 
<td  >';

$tenant_type=$t->tenant_type; 
if($tenant_type=="residential")
{ 
	$middle_name=""; $last_name=""; 
	$phone=$t->mobile_number; $email=$t->tenant_email;
	$other_names="";
	$tenant_name=($t->first_name ." ".$other_names); 
	$tenant_company_name=$t->company_name; 
}
else{
	$phone=$t->company_mobile; $tenant_name=$t->company_name; $company_name="";
	$email=$t->company_email;
 }
 
	$content .= '<ul style="font-size:12px; list-style-type:none">
		<li>&nbsp; </li>  
		<li><h4> Invoice To: </h4></li>   
		<li> <p>'.$tenant_name.'</p> </li>
		<li> <p>'. $email.'</p> </li>
		<li><p> '. $phone.'</p></li> 
		<li> &nbsp;   </li> 
	</ul> 
 
</td>
</tr>	

<tr> <td> &nbsp;  </td><td> &nbsp;  </td><td> &nbsp;  </td>  </tr> 
<tr  bgcolor="#DFDAD9"> 
<td>  <strong> Date   </strong>  </td><td>   <strong>  Particulars </strong>    </td>  <td align="right">  <strong>   Amount &nbsp;&nbsp;  </strong>  </td>
</tr>';

$i=0; $total=0;

foreach($rent_items as $row)
{
	$content .='<tr>
	<td> '. date('d/m/Y').' </td> 
	<td> '. $row['payment_type'].' ' . $row['description'].'  
	</td> 
	<td align="right">
	'. 	$row['payment_type_value']; 
		$priority=$row['priority']; 
		$total=$total+$row['payment_type_value']; 
		$content .='&nbsp;&nbsp;
	</td> 
	</tr>'; 
	$i++;
	$priority=$priority+1;	
	$total_items=$i;

}

if($total<=0){ 
$content .=' <tr>  <td>  &nbsp; </td> <td align="center"> <font color="red"> No Items to pay for </font></td> <td>  &nbsp;</td> </tr>';
} 
$content .='<tr> <td>  </td><td> &nbsp;  </td>  <td id="total" align="right"><strong> Total &nbsp;&nbsp;</strong>'.$total.' &nbsp;</td> </tr>

</table>  
<p>   </p>
<p> 
<h4><i><strong> **** This is automatically generated invoice **** </strong></i></h4> 
</p>
<p> <center> <i> '. $slogan.' </i></center> </p>   
</div>';  

$fileName="";
 
$pdf = new PDF(); 
$pdf->SetFont('','U'); 
$pdf->SetFont('helvetica','',12); 
$pdf->AddPage(); 
$pdf->SetFontSize(12);
 
$pdf->WriteHTML($content);
$fileName='document_'.date('Y-m-d').'_'.date('H-i-s').'.pdf';
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'media/'.$fileName, 'F');  
 
$obj=new send_mails(); 
if($email_invoice =="yes")
{  
	 $obj->sendMail($email,"Dear ".$tenant_name.", <br/> Attached to this email is your Invoice for the Month of ".date('M').", Thank you. <br/><br/>Regards ".$company_name ,$subject="Invoice",$company_name,$fileName); 
	// $obj->sendMail($subscriber_mail,$content="Dear ".$company_name.",<br/> Invoices was Automatically sent to your Tenants on ".date('d-m-Y H:i').". You can login to the system to view the invoices.<br/> Thank you",$subject="Automatic Invoicing");	
} 

unlink($_SERVER['DOCUMENT_ROOT'].'media/'.$fileName);
  
}

$obj->sendMail($subscriber_mail,$content="Dear ".$company_name.",<br/> Invoices was Automatically sent to your Tenants on ".date('d-m-Y H:i').". You can login to the system to view the invoices.<br/> Thank you",$subject="Automatic Invoicing");	

echo json_encode(array("result"=>"ok"));

}

}

}
else{ echo json_encode(array('result'=>'false','msg'=>'No Invoice set to send today '.date('d-m-Y')));}
}
		
function listInvoiceItems($tenant_id,$status="",$deposits_collected="0")
{  
		$balances = $this->Rent_model->get_data($table="tenant_current_account", array('audit_number'=>1,'tenant_id'=>$tenant_id));
		//$paymentsNoDepo = $this->Rent_model->get_data($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id,'is_deposit'=>0),$order_by="priority",$limit="",$asc_desc="ASC");
		$q = $this->Rent_model->getInvoiceItems($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id,'is_deposit'=>0),$order_by="priority",$limit="",$asc_desc="ASC");
		if($status==1)
		{
			$q = $this->Rent_model->getInvoiceItems($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id),$order_by="priority",$limit="",$asc_desc="ASC");
		}
		if($deposits_collected==1)
		{
			$q = $this->Rent_model->getInvoiceItems($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id,'is_deposit'=>0),$order_by="priority",$limit="",$asc_desc="ASC");
		}
		$row = $balances->row(); $it_exist=0;
		$items = array();  
		 
		foreach($q->result_array() as $u)
			 {  
				 
				array_push($items,array(
				"id"=>$u['id'],"audit_number"=>1,"tenant_id"=>$u['tenant_id'],
				"payment_type"=> $u['payment_type'],
				"payment_type_value" => $u['payment_type_value'],
				"description" => $u['description'],
				"priority"=> $u['priority'],
				"is_deposit"=>0,
				"is_one_time"=>$u['is_one_time'],
				"invoice_no"=>$u['invoice_no']
				)); 
			//}
			} 
			
return $items;

}

public function agingReport($tenant_id="")
{
	$items = array(); $amount1=0;	$amount2=0;	$amount3=0;	$amount4=0;
	$q=$this->Rent_model->get_data($table="invoice", array('audit_number'=>1,'balance >'=>0,'status'=>0,'is_deleted'=>0,'tenant_id'=>$tenant_id,'company_code'=>$this->session->userdata('company_code')));
	$now=time();  	  
	foreach($q->result_array() as $u)
		{   
		        $due_date=$u['due_date'];  $amount=$u['balance']; 
				$diff_time=$now-strtotime($due_date); 
				$days=floor($diff_time/(60*60*24)); 
				if($days <=30)
				{
					$amount1=$amount1+$amount;
				}else
				if($days >30 && $days <=60){
					$amount2=$amount2+$amount;
				}else
				if($days >60 && $days <=90){
					$amount3=$amount3+$amount;
				}else if($days >90)
				{
					$amount4=$amount4+$amount;
				}
				else{ continue;}
		}
		array_push($items,array(
			"thirty_and_less"=>$amount1,
			"more_than_thirty"=> $amount2,
			"more_than_sixty" => $amount3,
			"more_than_ninety" => $amount4  
			)); 
		 return $items;
}
 
 
public function tenant_aging_report()
{
	$searchId=$this->input->post('search_id');
	if($searchId ==""){ $startday ="1"; $endday="30";  }
	else if($searchId=="30"){ $startday ="1"; $endday="30";   }
	else if($searchId=="60"){ $startday ="31"; $endday="60";   }
	else if($searchId=="90"){ $startday ="61"; $endday="90";   }
	else{ 	$startday ="91"; $endday="10000000";	}
	$tenants = array(); $amount=0; 
	 
	$q=$this->Rent_model->get_aging_report($table="tenant_current_account", array('tenant_current_account.audit_number'=>1,'company_code'=>$this->session->userdata('company_code')));
	$now=time();  	  
	foreach($q->result_array() as $u)
		{   
		        $last_pay_date=$u['last_pay_date'];   
				$diff_time=$now-strtotime($last_pay_date); 
				$days=floor($diff_time/(60*60*24)); 
				if($days >=$startday && $days<=$endday) 
				{ 
						$name=$u['first_name']; $other_names="";
						if($u['middle_name']==0 || $u['middle_name']=="" ){$other_names="";}else{$other_names=$u['middle_name'];}
						if($u['last_name']==0 || $u['last_name']==""){  } else{ $other_names=$u['last_name'];  }
						array_push($tenants,
						array(
						"tenant_id"=>$u['tenant_id'],
						"tenant_name"=>$name." ".$other_names,
						"tenant_email"=>$u['tenant_email'],
						"company_code"=>$u['company_code'],
						"company_name"=>$u['company_name'],
						"company_email"=>$u['company_email'],
						"id_passport_number"=>$u['id_passport_number'],
						"property_name"=>$u['property_name'],
						"tenant_type"=>$u['tenant_type'],
						"house_no"=>$u['house_no'],
						"category_name"=>$u['category_name'],
						"total_amount"=>$u['total_amount'],
						"last_pay_date"=>$u['last_pay_date']
							)
						);
				}
				else{ continue;}
		} 
		$data['tenants']=$tenants;
		$data['selected_range']=$endday;
		$this->load->view('accounts/header');
		$this->load->view('accounts/aging_reports',$data);
		$this->load->view('accounts/footer');
		//print_r($tenants);
}

function get_enct()
{
	       $id=$this->input->post('id'); 		
	        $id = $this->encrypt->encode($id);		
			$enct=str_replace(array('+', '/', '='), array('-', '_', ''), $id);
		  echo json_encode(array('enct'=>$enct));
}

function get_invoice_data()
{
	$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id");
	$q=$this->Rent_model->get_data($table="tenant_invoice_statement", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id", $limit="", $asc_desc="DESC");
        if($q->num_rows()>0){
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
			}
		else{ echo json_encode(array('result'=>"false")); } 
} 
 
}	
		
?>