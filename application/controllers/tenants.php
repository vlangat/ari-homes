<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tenants extends CI_Controller {
	var $disabled="";
	 var $user_ip=null; 
	public function __construct()
	{
		parent::__construct();   
		$this->load->model("Authentication_model");
		$this->load->model("Property_model"); 
		$this->load->model("Tenant_model"); 
		$this->load->model("Rent_model"); 
		$this->load->model("Payment_model"); 
		$this->load->library('upload');
		$this->load->library('Expired');
		$this->load->helper('security');
		$this->load->library('rent_class');
		$this->load->library('Encrypte');
		date_default_timezone_set('Africa/Nairobi');
		$this->user_ip=$this->input->ip_address();
		ini_set("memory_limit","1024M");
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry();
	}
	public function index()
	{ 
		//$data['tenants']=$this->Tenant_model->get_data('tenant_property_units',array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id");
		//$data['current_acc']=$this->Property_model->get_data($table="tenant_current_account", array('audit_number'=>1));
		//$this->load->view('accounts/viewTenants',$data);
		$this->load->view('accounts/header');
		$this->load->view('accounts/viewTenants');
		$this->load->view('accounts/footer'); 
	}
	
 
  public function viewTenant($tenant_id="", $msg="")
	{
		 $id="";  
		 $hashed=new Encrypte();
	     $tenant_id=$hashed->hashId($tenant_id);	
		 $data['disabled']=$this->disabled;
		 if($tenant_id=="" && !empty($this->input->post('checkbox'))){ foreach($this->input->post('checkbox') as $value) {  $id=$value; } }else{  $id=$tenant_id;}
		 if($id==""){ $id=$this->input->post('id'); }
		 if($id==""){ $this->index(); return; }
		 $data['msg']=$msg; 
		 $data['tenant_details']=$q=$this->Tenant_model->get_data("tenant_property_units",array('id'=>$id,'audit_number'=>1));
         foreach($q->result() as $row){$type=$row->tenant_type; $unit_id=$row->property_unit_id;} 
		 //$data['property']=$this->Tenant_model->allocatedProperty($condition=array('id'=>$id));
		 $data['property']=$this->Tenant_model->get_data("property", $condition=array('id'=>$id));
		 $data['properties']=$this->Tenant_model->get_data("property", array('company_code'=>$this->session->userdata('company_code')));
		 $data['categories']=$this->Property_model->get_data($table="unit_property_details", array('company_code'=>$this->session->userdata('company_code')));
		 $data['payment_details']=$this->Property_model->get_data($table="tenant_pricing", array('tenant_id'=>$id));
		 $data['paid_rent']=$this->Rent_model->get_data($table="tenant_transaction",  array('tenant_id'=>$id,'type'=>'c')); 	 
		 $data['pricing_details']=$this->Tenant_model->get_data($table="pricing_details",array('unit_id'=>$unit_id)); 
		 $data['current_acc']=$this->Property_model->get_data($table="tenant_current_account", array('audit_number'=>1));
		 $data['documents']=$this->Tenant_model->get_data($table="media", array('tenant_id'=>$id,'deleted'=>1));
         if($type=="residential"){ $page="viewSingleTenant";} else{ $page="viewCommercialTenant";	}
		 $this->load->view('accounts/header');
		 $this->load->view('accounts/'.$page,$data);
		 $this->load->view('accounts/footer');  
	}
			 
	public function viewCommercialTenant($tenant_id="", $msg="")
	{
		if($tenant_id=="" && !empty($this->input->post('checkbox'))){ foreach($this->input->post('checkbox') as $value) {  $id=$value; } }else{  $id=$tenant_id;}
		if($id==""){ $id=$this->input->post('id'); }
		if($id==""){ $this->index(); return; } 
		$data['msg']=$msg; 
		$data['disabled']=$this->disabled;
		$data['tenant_details']=$q=$this->Tenant_model->get_data("tenant",array('id'=>$id,'tenant.audit_number'=>1)); 
		$data['property']=$this->Tenant_model->get_data($table="property"); 
		$data['properties']=$this->Tenant_model->get_data("property");
		$data['categories']=$this->Property_model->get_data($table="unit_property_details", array('company_code'=>1000));
		$data['payment_details']=$this->Property_model->get_data($table="tenant_pricing", array('tenant_id'=>$id));	
		$data['documents']=$this->Tenant_model->get_data($table="media", array('tenant_id'=>$id,'deleted'=>1));
		$this->load->view('accounts/header');
		$this->load->view('accounts/viewCommercialTenant',$data);
		$this->load->view('accounts/footer');  
	}
 
	public function getTennantDetails($id="")
	{  
		$data=$this->Tenant_model->get_data($table="lease", array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1),$order_by="",$asc_desc="");
		if($data->num_rows()<1)
		{
			//$data=$this->Tenant_model->get_data($table="tenant_property_units", array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'tenant_deleted'=>1),$order_by="",$asc_desc="");
		    $this->db->select("*");
		    $this->db->from("tenant_property_units");
			$this->db->where(array('tenant_property_units.id'=>$id,'tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_property_units.tenant_deleted'=>1));
		    $this->db->join("landlord","tenant_property_units.landlord=landlord.id","LEFT");
		    $data=$this->db->get();
		}
		
		if($data->num_rows()>0)
		{
		   echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}         
	  else
		 {
			echo json_encode(array('result'=>"false"));
		 }
                 
	}

	public function getCompanyDetails($id)
	{ 
		$data=$this->Tenant_model->get_data("tenant", array('id'=>$id));
		if($data->num_rows()>0)
		{
		  echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	public function getData($id="")
	{ 
	   if($id==""){ redirect(base_url().'tenants/'); }
		$data=$this->Tenant_model->get_data("tenant",array('id'=>$id));
		if($data->num_rows()>0)
		{ 
			 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			$data=$this->Tenant_model->get_data("tenant", array('no'=>$id,'tenant.audit_number'=>1));
			if($data->num_rows()>0)
			{
			  echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
			}
			else { echo json_encode(array('result'=>"false",'data'=>0)); }
		}
	}
	
	public function individual($id="",$msg="",$tenant_id="", $status="0")
	{
	    $hashed=new Encrypte();
	    $id=$hashed->hashId($id);
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['package_account']=$this->Property_model->get_data($table="package_current_account",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['property']=$this->Tenant_model->get_data("property", array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if(!empty($id))
		{
			$data['property']=$this->Tenant_model->get_data("property", array('id'=>$id));
		}
		$data['category']=$this->Property_model->get_data("property_category");
        $data['msg']=$msg;  $data['status']=$status;  $data['tenant_id']=$tenant_id;
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/single_tenants',$data);
		$this->load->view('accounts/footer');
	}
	public function commercial($id="", $msg="",$tenant_id="", $status="0")
	{
		
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['package_account']=$this->Property_model->get_data($table="package_current_account",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['property']=$this->Tenant_model->get_data("property", array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['msg']=$msg; $data['status']=$status;  $data['tenant_id']=$tenant_id;
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/commercialTenant',$data);
		$this->load->view('accounts/footer');
	}
	
	
	public function landlord($msg="")
	{
		$data['disabled']=$this->disabled;
		$data['msg']=$msg;
		$data['property']=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['landlords']=$this->Property_model->get_data($table="landlord",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1),$order_by="id");
		$this->load->view('accounts/header');
		$this->load->view('accounts/landlord',$data);
		$this->load->view('accounts/footer');  
	}
	

	public function assign_landlord_property()
	{
		$msg=""; 
		$property_allocated=$this->input->post('property_id');  
		$landlord_id=$this->input->post('landlord_id'); 
		if($property_allocated=="")
		{
			$msg="Data not updated successfully"; 
			echo json_encode(array('result'=>"false",'msg'=>$msg));
		}
		else
		{
			$this->Tenant_model->update_data($table="property", $condition=array('id'=>$property_allocated), array('landlord'=>$landlord_id));
			$msg="Data   updated successfully"; 
			echo json_encode(array('result'=>"ok",'msg'=>$msg));
		} 	
	}
	
	public function add_landlord()
	{ 
	     $landlord_id=$this->input->post('landlord_id');
		 $email=$this->input->post('landlord_email');
		 $data=array(
			'company_code'=>$this->session->userdata('company_code'),
			'full_name'=>$this->input->post('landlord_fname'),
			//'middle_name'=>$this->input->post('landlord_mname'),
			//'last_name'=>$this->input->post('landlord_lname'),
			'phone'=>$this->input->post('landlord_phone'),
			'email'=>$this->input->post('landlord_email'),
			'bank_name'=>$this->input->post('bank_name'),
			'bank_acc'=>$this->input->post('bank_acc'),
			'bank_branch'=>$this->input->post('bank_branch'),
			'id_no'=>$this->input->post('landlord_id_no')
			);
		
       if($landlord_id==""){	
       	$c=$this->Tenant_model->get_data($table="landlord", array('id_no'=>$this->input->post('landlord_id_no'),'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1)); 
		if($c->num_rows()>0)
	    { 
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new landlord ",$status=2);
			
	        echo json_encode(array('result'=>"false",'msg'=>'Landlord records already exist!'));
		}
		else{  
	    if($this->Tenant_model->insert_data($table="landlord", $data)>0)
	    { 
			$msg="Landlord details saved successfully";
			$data=array(
			'company_code'=>$this->session->userdata('company_code'),
			'acc_name'=>$this->input->post('landlord_fname'),
			'acc_no'=>$this->input->post('bank_acc'),
			'owner'=>$this->input->post('landlord_fname'), 
			'bank_name'=>$this->input->post('bank_name'),
			'branch'=>$this->input->post('bank_branch'), 
			'date_added'=>date("Y-m-d H:i:s")
			);
			//$this->Tenant_model->insert_data($table="bank", $data);
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="added new landlord ",$status=1);
			
			echo json_encode(array('result'=>"ok",'msg'=>$msg)); 		
		}
		else{
			  $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new landlord ",$status=1);
			
				$msg="Landlord details not saved ";	
                echo json_encode(array('result'=>"false",'msg'=>$msg));				
			}
		}
	   }
		else
		{
			$this->Tenant_model->update_data($table="landlord", $condition=array('id'=>$landlord_id), $data);
		 
			 $msg="Landlord details updated successfully";
			 echo json_encode(array('result'=>"ok",'msg'=>$msg));
		}
		//$this->landlord($msg);
    }
 
	public function dealocate_landlord($property_id="", $landlord_id="")
	{
		if($property_id==""){  echo json_encode(array('result'=>"false",'data'=>0)); }  
		if($this->Property_model->update_data($table="property", array('id'=>$property_id,'landlord'=>$landlord_id,'audit_number'=>1), array('landlord'=>'')))
		{
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	 
	public function removeLandlord($id="")
	{
		if($id==""){  echo json_encode(array('result'=>"false",'data'=>0)); }
		 
		if($this->Property_model->update_data($table="landlord", array('id'=>$id,'audit_number'=>1), array('audit_number'=>2)))
		{
			$this->Property_model->update_data($table="property", array('landlord'=>$id,'audit_number'=>1), array('landlord'=>''));	
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
public function landlord_property($id="")
{
		$enct=$id;
		$data['msg']="";
		$hashed=new Encrypte();
	    $id=$hashed->hashId($enct);		
		if($id==""){ redirect(base_url() ."tenants/landlord"); }
        $q=$this->Tenant_model->get_data($table="landlord", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		if($q->num_rows()<=0)
		 {
			redirect(base_url() ."tenants/landlord");
		 } 
		$data['disabled']=$this->disabled;
		$data['landlords']=$this->Tenant_model->get_data($table="landlord", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		$data['properties']=$this->Tenant_model->get_data($table="property", $condition=array('landlord'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		$this->load->view('accounts/header');
		$this->load->view('accounts/landlord_property',$data);
		$this->load->view('accounts/footer'); 	
}
	
public function singleTenant()
	{ 
		$property_allocated=$this->input->post('property'); 
		$unit_id=$this->input->post('unit_id'); 
		$national_id=$this->input->post('id_number');  
		$total_payment=$this->input->post('total_payment');  
		$lease_period=$this->input->post('lease_period');  
		$deposits=$this->input->post('total_deposits');  
		$deposit_status=$this->input->post('deposit_status');  
		if($lease_period==""){ $lease_period=0;}
		$payCode=$this->getPayCode();
		$data=array(
		'first_name'=>$this->input->post('fname'),
		'middle_name'=>$this->input->post('mname'),
		'last_name'=>$this->input->post('lname'),
		'mobile_number'=>$this->input->post('mobile_no'),
		'nok_fullname'=>$this->input->post('nok_fullname'),
		'nok_mobile'=>$this->input->post('nok_mobile'),
		'nok_relationship'=>$this->input->post('nok_relationship'),
		'nok_address'=>$this->input->post('nok_address'),
		'tenant_email'=>$this->input->post('email'),
		'id_passport_number'=>$this->input->post('id_number'), 
        'tenant_type'=>'residential',   
		'property_unit_id'=>$unit_id, 
		'house_no'=>$this->input->post('houseno'),
		'floor_no'=>$this->input->post('floor'),
		'parking_allocated'=>$this->input->post('car_spaces'),    
		'lease_period'=>$lease_period,
		'rent_frequency'=>$this->input->post('rent_frequency'),
		'expected_pay_day'=>$this->input->post('pay_day'),
		'date_added'=>$this->input->post('date_registered'),
		'added_by'=>$this->session->userdata('company_code'),		
		'delete_date'=>'',		
		'tenant_payId'=>$payCode		
		);
		$deposits=$this->input->post('total_deposits');
		$rent_frequency=$this->input->post('rent_frequency');
		if($deposit_status=="yes"){ $deposits=0; }
		 $files=$this->input->post('no_of_files');   
		//$this->Tenant_model->insert_data($table="tenant", $data);
		 $s=$this->Tenant_model->get_data($table="tenant",
			array(
				'id_passport_number'=>$this->input->post('id_number'),'tenant_type'=>'residential',
				'property_unit_id'=>$unit_id,'house_no'=>$this->input->post('houseno'),
				'floor_no'=>$this->input->post('floor'),'added_by'=>$this->session->userdata('company_code')
				)
		);
		if($s->num_rows()>0){
			//$this->individual($property_allocated,$msg="Tenant details already exist!",$tenant_id="", $status="0");
			$msg="Tenant details already exist!";
			$this->session->set_flashdata('temp',$msg);
			redirect(base_url().'tenants/individual'); 
			return; }
		if($this->Tenant_model->insert_data($table="tenant", $data)>0){ 
		$q=$this->Tenant_model->get_data($table="tenant_property_units", $condition=array('tenant_email'=>$this->input->post('email'),'id_passport_number'=>$this->input->post('id_number'),'company_code'=>$this->session->userdata('company_code')));
			foreach($q->result() as $row){ $id=$row->id;}  
			$this->Tenant_model->insert_data($table="tenant_current_account", array('tenant_id'=>$id,'rent_balance'=>0));
			$this->update_payment_info($id,$unit_id,$deposits,$rent_frequency);
			 
			//create invoice
			$this->listInvoiceItems($id,$deposits); 
			//end
			if ($files==0){  }else{ $this->do_upload($id,$national_id); }
		  $encrypted_string = $this->encrypt->encode($id);	 $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
		  $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add single tenant ",$status=1);
		  $msg="Tenant details saved successfully";
		  $this->session->set_flashdata('temp',$msg);
		  redirect(base_url().'tenants/viewTenant/'.$enct); 
		}
		else
		{
		$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new tenant ",$status=2);
		$this->session->set_userdata('tenant_status','Tenant details Not saved successfully. Try again');
		$msg="Tenant details not saved successfully!";
		$this->session->set_flashdata('temp',$msg);
		redirect(base_url().'tenants/individual');   
		}   
	}
	
	public function commercialTenant()
	{
		$property_allocated=$this->input->post('property');  
		$unit_id=$this->input->post('unit_id');  
		$business_no=$this->input->post('business_no');
		$lease_period=$this->input->post('lease_period');  
		$deposit_status=$this->input->post('deposit_status');  
		if($lease_period==""){ $lease_period=0;}
		$payCode=$this->getPayCode();
		$data=array(
		'company_name'=>$this->input->post('company'),
		//'company_code'=>$this->session->userdata('company_id'),
		'business_number'=>$this->input->post('business_no'),
		'tenant_email'=>$this->input->post('email'),
		'company_email'=>$this->input->post('email'),
		'company_mobile'=>$this->input->post('mobile'),
        'tenant_type'=>'commercial', 
		'business_activity'=>$this->input->post('activity'),
		'first_name'=>$this->input->post('contact_person'),
		'mobile_number'=>$this->input->post('phone_no'),  
		'nok_designation'=>$this->input->post('nok_designation'),  
		'nok_address'=>$this->input->post('nok_address'),  
		'property_unit_id'=>$unit_id,
		'house_no'=>$this->input->post('houseno'),
		'floor_no'=>$this->input->post('floor'),
		'parking_allocated'=>$this->input->post('car_spaces'), 
		'lease_period'=>$lease_period,
		'rent_frequency'=>$this->input->post('rent_frequency'),
		'expected_pay_day'=>$this->input->post('pay_day'),
		'added_by'=>$this->session->userdata('company_code'), 
		'date_added'=>$this->input->post('date_registered'),		
		'id_passport_number'=>'',		
		'delete_date'=>'',
		'tenant_payId'=>$payCode
		);
		 
		$deposits=$this->input->post('total_deposits');
		$rent_frequency=$this->input->post('rent_frequency');
		if($deposit_status=="yes"){ $deposits=0; } 
		$business_no=$this->input->post('business_no'); 
		$files=0;
		$files=$this->input->post('no_of_files'); 
		
		$s=$this->Tenant_model->get_data($table="tenant", $data);
		if($s->num_rows()>0){
		$msg="Tenant details already exist!";
		$this->session->set_flashdata('temp',$msg);
		redirect(base_url().'tenants/commercial'); 
		return; }
		
		if($this->Tenant_model->insert_data($table="tenant", $data)){
            $q=$this->Tenant_model->get_data($table="tenant_property_units",$condition=array('business_number'=>$this->input->post('business_no'),'company_email'=>$this->input->post('email'),'company_name'=>$this->input->post('company'),'company_code'=>$this->session->userdata('company_code')));
			foreach($q->result() as $row){ $id=$row->id; }
			$this->Tenant_model->insert_data($table="tenant_current_account", array('tenant_id'=>$id,'rent_balance'=>0));
			$this->update_payment_info($id,$unit_id,$deposits,$rent_frequency); 
			//create invoice
			$this->listInvoiceItems($id,$deposits);
			//end
			
			if ($files==0){    } else{ $this->do_upload($id,$business_no); }
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add commercial tenant ",$status=1);
			$encrypted_string = $this->encrypt->encode($id);	 $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
			$msg="Tenant details saved successfully";
			  $this->session->set_flashdata('temp',$msg);
			  redirect(base_url().'tenants/viewTenant/'.$enct); 
		}
		else
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new tenant ",$status=2);
		$msg="Tenant details Not saved successfully. Try again";
		$this->session->set_flashdata('temp',$msg);
		redirect(base_url().'tenants/commercial'); 
		}  
	}
	
	
	public function update_tenant()
	{
		$property_allocated=$this->input->post('property');
		//$unit_id=$this->input->post('category');
		$unit_id=$this->input->post('unit_id'); 
		//$q=$this->Property_model->get_data($table="property", array('id'=>$property_allocated));
		//foreach($q->result() as $row){ $name=$row->property_name; } 
		$id=$this->input->post('id_no'); 
		$national_id=$this->input->post('national_id'); 
		$userfile=$this->input->post('userfile');
		$data=array(
		'first_name'=>$this->input->post('fname'),
		'middle_name'=>$this->input->post('mname'),
		'last_name'=>$this->input->post('lname'),
		'mobile_number'=>$this->input->post('mobile_no'), 
		'nok_fullname'=>$this->input->post('nok_fullname'),
		'nok_mobile'=>$this->input->post('nok_mobile'),
		'nok_relationship'=>$this->input->post('nok_relationship'),
		'nok_address'=>$this->input->post('nok_address'),
		'tenant_email'=>$this->input->post('email'),
		'id_passport_number'=>$this->input->post('national_id'), 
		'property_unit_id'=>$unit_id,
		//'property_name'=>$name,
		//'unit_category'=>$this->input->post('category'),
		'house_no'=>$this->input->post('houseno'),
		'floor_no'=>$this->input->post('floor'),
		'parking_allocated'=>$this->input->post('car_spaces'), 
		'lease_period'=>$this->input->post('lease_period'),
		'rent_frequency'=>$this->input->post('rent_frequency'),
		'expected_pay_day'=>$this->input->post('pay_day'),
		'date_added'=>$this->input->post('date_registered') 
		);
		 
		$encrypted_string = $this->encrypt->encode($id);	 $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
		 
		$files=0; $files=$this->input->post('no_of_files');
		if($this->Tenant_model->update_data("tenant", array('id'=>$id),$data))
		{     
			if ($files==0) { 	}else{ $this->do_upload($id,$national_id); }
			//echo json_encode(array('result'=>"ok",'data'=>1));
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="edit  tenant id ".$id,$status=1);
			$msg="Tenant details successfully updated ";
			$this->session->set_flashdata('temp',$msg); 
			redirect(base_url().'tenants/viewTenant/'.$enct); 			
		}
		else
		{  
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to edit tenant id ".$id,$status=2);
			$msg="Tenant details not updated . Try again later";
			$this->session->set_flashdata('temp',$msg);
			redirect(base_url().'tenants/viewTenant/'.$enct); 
		}  
		 
	}
	
	public function update_Commercial_tenant()
	{
		$property_allocated=$this->input->post('property');
	//	$unit_id=$this->input->post('category'); 
        $unit_id=$this->input->post('unit_id'); 		
		$id=$this->input->post('id_no'); 
		$national_id=$this->input->post('national_id');  
		if($national_id==""){$national_id=$this->input->post('mobile_no');}
		$userfile=$this->input->post('userfile');
		$data=array(
		'company_name'=>$this->input->post('company_name'), 
		'company_mobile'=>$this->input->post('mobile_no'),
		'first_name'=>$this->input->post('contact_person'),
		'mobile_number'=>$this->input->post('phone_no'), 
		'nok_designation'=>$this->input->post('nok_designation'),  
		'nok_address'=>$this->input->post('nok_address'),
		'company_email'=>$this->input->post('email'),
		'business_number'=>$this->input->post('business_no'),
		'business_activity'=>$this->input->post('business_activity'),
		'property_unit_id'=>$unit_id, 
		'house_no'=>$this->input->post('houseno'),
		'floor_no'=>$this->input->post('floor'),
		'parking_allocated'=>$this->input->post('car_spaces'), 
		'lease_period'=>$this->input->post('lease_period'),
		'rent_frequency'=>$this->input->post('rent_frequency'),
		'expected_pay_day'=>$this->input->post('pay_day'),
		'date_added'=>$this->input->post('date_registered') 
		);  
		$files=0; 
		$encrypted_string = $this->encrypt->encode($id);	 $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
		 
		 $files=$this->input->post('no_of_files'); 
		if($this->Tenant_model->update_data("tenant", array('id'=>$id),$data))
		{  
			if ($files==0){  }else { $this->do_upload($id,$national_id); }
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="edit tenant id ".$id,$status=1);
			$msg="Tenant details successfully updated";
			$this->session->set_flashdata('temp',$msg);
			redirect(base_url().'tenants/viewTenant/'.$enct);  		
		}
		else
		{
		$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed edit tenant id ".$id,$status=2);
		$msg="Tenant details not  updated. Try again later";
		$this->session->set_flashdata('temp',$msg);
		redirect(base_url().'tenants/viewTenant/'.$enct);  		
		} 
		 
	}
	

	public function removeTenant($id="")
	{
		if($id==""){  echo json_encode(array('result'=>"false",'data'=>0)); }
		$this->Tenant_model->update_data("tenant", array('id'=>$id), array('tenant_deleted'=>2,'audit_number'=>2,'tenant_status'=>2));
		$q=$this->Tenant_model->get_data('tenant',array('id'=>$id,'audit_number'=>2,'tenant_deleted'=>2));
		if($q->num_rows()>0)
		{
			$tenant_id=$id;
			$this->Property_model->update_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2));	
			$this->Property_model->update_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2)); 
			$this->Property_model->update_data($table="tenant_payment", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2)); 	
			$this->Property_model->update_data($table="tenant_current_account", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2));	
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="removed tenant id ".$tenant_id,$status=1);
			
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to remove tenant id ".$tenant_id,$status=2);
			
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	public function add_supplier()
	{
		$edit_id=$this->input->post('id');
		$company_name=$this->input->post('company_name');
		$company_email=$this->input->post('email');
		
		$data=array(
				'company_name'=>$this->input->post('company_name'),  
				'contact_person_name'=>$this->input->post('contact_person'),  
				'product_description'=>$this->input->post('description'),
				'mobile_no'=>$this->input->post('phone'),
				'company_code'=>$this->session->userdata('company_code'),
				'supplier_added_by'=>$this->session->userdata('id'),
				'email'=>$this->input->post('email'),
				'supplier_delete_date'=>null,
				'supplier_deleted_by'=>null
			  ); 
		 
		if($edit_id=="")
		{ 
			  $count=$this->Tenant_model->get_data($table="suppliers",array('company_name'=>$this->input->post('company_name'),'company_code'=>$this->session->userdata('company_code')));
				if($count->num_rows()>0)
				{ 
					echo json_encode(array('result'=>"false",'data'=>0,'msg'=>'Supplier with same Company Name already exist'));
				 return; 
				} 
				if($this->Tenant_model->insert_data($table="suppliers", $data))
					{
					$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add new supplier ",$status=1);

					echo json_encode(array('result'=>"ok",'data'=>1));
					}
					else
					{
						$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new supplier ",$status=2);
						echo json_encode(array('result'=>"false",'data'=>0,'msg'=>''));
					}
				 
		}
         else
		{
			$q=$this->Tenant_model->get_data($table="suppliers", array('company_code'=>$this->session->userdata('company_code'),'id'=>$edit_id,'audit_number'=>1));
			$company=""; $company_mail="";
			if($q->num_rows()>0){ foreach($q->result() as $c){ $company=$c->company_name; $company_mail=$c->email;}}
			if($company_name !=$company)
			{ 
				$s=$this->Tenant_model->get_data($table="suppliers", array('company_code'=>$this->session->userdata('company_code'),'company_name'=>$company_name,'audit_number'=>1));
				if($s->num_rows()>0){ echo json_encode(array('result'=>"false",'data'=>0,'msg'=>'Company with same name exist')); return; }
			}
			if($company_mail != $company_email)
			{ 
				$z=$this->Tenant_model->get_data($table="suppliers", array('company_code'=>$this->session->userdata('company_code'),'email'=>$company_email,'audit_number'=>1));
				if($z->num_rows()>0){ echo json_encode(array('result'=>"false",'data'=>0,'msg'=>'Company with the same email address exist')); return; }
			}
			 
		    if($this->Tenant_model->update_data($table="suppliers",array('id'=>$edit_id),$data))
		     {
				 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="update details for supplier id ".$edit_id,$status=1);
				 echo json_encode(array('result'=>"ok",'data'=>1));
		     }
           else
		    {
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to update details for supplier id ".$edit_id,$status=2);
				echo json_encode(array('result'=>"false",'data'=>0,'msg'=>''));
		    }
        }
		
	}
	
	public function getLandlordDetails($id="")
	{
		$q=$this->Tenant_model->get_data($table="landlord", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
	public function showSupplier($id="")
	{
		$q=$this->Rent_model->get_data($table="expenses", $condition=array('no'=>$id,'company_id'=>$this->session->userdata('company_id'),'supplier_deleted'=>1));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
	public function remove_supplier($id="")
	{
		if($id==""){  echo json_encode(array('result'=>"false",'data'=>0)); return; }
		$q=$this->Tenant_model->update_data($table="supplier", $condition=array('id'=>$id,'supplier_deleted'=>1,'company_code'=>$this->session->userdata('company_code')),$data=array('supplier_deleted'=>2,'audit_number'=>2,'supplier_delete_date'=>date('Y-m-d')));
		$s=$this->Rent_model->get_data( $table="suppliers",$condition=array('id'=>$id,'supplier_deleted'=>2,'audit_number'=>2,'supplier_delete_date'=>date('Y-m-d')));
	
		if($s->num_rows()>0)
		{
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="remove supplier id ".$id,$status=1);
			
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to remove supplier id ".$id,$status=1);
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
public function search_supplier()
	{
		 $search=$this->input->get('term'); 
		if($search !="")
		{
		  $query = $this->Tenant_model->get_data($table="suppliers", array('company_code'=>$this->session->userdata('company_code'),'supplier_deleted'=>1),$order_by="",$asc_desc="",$search);
			foreach($query->result() as $r)
			{
				$arr_result[]=$r->company_name;
			}
		   echo json_encode($arr_result);  
		}
	else
	{
		echo json_encode(array('result'=>"false",'data'=>0));
	}  
		
  }
  
  public function search_tenant()
	{
		 $search=$this->input->get('term'); 
		if($search !="")
		{
		  $query = $this->Tenant_model->get_data($table="tenant_property_units", array('company_code'=>$this->session->userdata('company_code'),'tenant_deleted'=>1),$order_by="",$asc_desc="",$search,"first_name");
			foreach($query->result() as $r)
			{
				$arr_result[]= $r->first_name;
			}
		   echo json_encode($arr_result);  
		}
	else
	{
		echo json_encode(array('result'=>"false",'data'=>0));
	}  
		
  }
  
//modified function
public function update_payment_info($tenant_id, $unit_id,$deposits=0,$rent_frequency="")
	{
		$total_pay=0;
		$data=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id));	
		if($data->num_rows()>0)
		{
			foreach($data->result() as $r)
			{ 
			    $payment_type_value=0; $is_deposit=0; $priority=0;
				$q=$this->Property_model->get_data($table="tenant_pricing", array('unit_id'=>$unit_id,'tenant_id'=>$tenant_id,'payment_type'=>$r->payment_type));
				if($q->num_rows()>0)
				{
				 
				}
				else
				{
					$payment_type=$r->payment_type; $payment_type_value=$r->payment_type_value; $is_deposit=$r->is_deposit; $priority=$r->priority; 
					//	
					if($rent_frequency=="Quarterly" && $r->is_deposit==0){  $payment_type_value=$payment_type_value*3;} 
					if($rent_frequency=="Yearly" && $r->is_deposit==0){ $payment_type_value=$payment_type_value*12; }
					//
					if($r->is_deposit==1){$priority=0;}
					if($r->is_deposit==0){$total_pay=($total_pay+$payment_type_value);}
				}
				if($payment_type_value>0)
				{
					$this->Property_model->insert_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'unit_id'=>$unit_id,'payment_type'=>$payment_type,'payment_type_value'=>$payment_type_value,'priority'=>$priority,'is_deposit'=>$is_deposit));
				}
			
			}
		}
		if($deposits>0){
		$this->Property_model->insert_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'date_paid'=>date("m/d/Y"),'amount'=>$deposits,'description'=>'Deposit','payment_mode'=>'','payment_mode_code'=>'','receipt_no'=>'','type'=>'d'));
		}
		$data=array('tenant_id'=>$tenant_id,'date_paid'=>date("m/d/Y"),'amount'=>$total_pay,'payment_mode'=>'','description'=>'Rent', 'payment_mode_code'=>'','receipt_no'=>'','type'=>'d');
		$q=$this->Property_model->get_data($table="tenant_transaction", $data);
		if($q->num_rows() ==0)
		{
			$this->Property_model->insert_data($table="tenant_transaction", $data);
		}
		$obj=new rent_class;  
		$expected_pay_day=$obj->expected_pay_day($tenant_id);
		$this->Rent_model->update_data($table="tenant_current_account", array('rent_balance'=>$total_pay,'deposit_balance'=>$deposits,'total_amount'=>($total_pay+$deposits),'expected_pay_date'=>$expected_pay_day),  array('tenant_id'=>$tenant_id));
	} 
	
public function getPayCode()
{ 
	$paycode=1001;
	$q=$this->Tenant_model->get_data($table="tenant", array('tenant_payId'=>$paycode));
	while($q->num_rows() >0)
	{
		$paycode=$paycode+1;
		$q=$this->Tenant_model->get_data($table="tenant",array('tenant_payId'=>$paycode));
	}
	return $paycode;
} 

public function bank_details()
{
	$edit_id=$this->input->post('edit_id');
	$data=array(
			'company_code'=>$this->session->userdata('company_code'),
			'acc_name'=>$this->input->post('acc_name'),
			'acc_no'=>$this->input->post('acc_no'),
			'owner'=>$this->input->post('owner'), 
			'bank_name'=>$this->input->post('bank_name'),
			'branch'=>$this->input->post('bank_branch'),
			'description'=>$this->input->post('description'),
			'date_added'=>date("Y-m-d H:i:s")
			);
	if($edit_id !="")
	{ 
		$this->Tenant_model->update_data($table="bank",array('id'=>$edit_id), $data);
		echo json_encode(array('result'=>'ok'));	
	}else{
	if($this->Tenant_model->insert_data($table="bank", $data)>0)
	{
		echo json_encode(array('result'=>'ok'));
	}
	else
	{
		echo json_encode(array('result'=>'false'));
	}		
	}	
}
	
public function get_data($table="")
{ 
	$q=$this->Tenant_model->get_data($table, $condition=array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
	if($q->num_rows()>0)
	{
		echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
	}
	else
	{
		echo json_encode(array('result'=>"false",'data'=>0));
	} 
}

public function getBankDetails($id="")
{ 
	$q=$this->Tenant_model->get_data($table="bank", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
	if($q->num_rows()>0)
	{
		echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
	}
	else
	{
		echo json_encode(array('result'=>"false",'data'=>0));
	} 
}
	 

public function do_upload($id="",$national_id=""){  
	$config['upload_path'] = './media/'; 
	$config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx';
	$config['max_size']	= '5000';
	$config['max_width']  = '1024'; 
	$config['max_height']  = '768';
	$this->load->library('upload', $config);
	$this->upload->initialize($config);
	 
	$targetDir='./media/';   //directory name
	$files = $_FILES; 
	 $cpt = count($_FILES['userfile']['name']);
	  
for($i=0; $i<$cpt; $i++)
{
	$_FILES['userfile']['name']= $files['userfile']['name'][$i];
	$_FILES['userfile']['type']= $files['userfile']['type'][$i];
	$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	$_FILES['userfile']['error']= $files['userfile']['error'][$i]; 
	$_FILES['userfile']['size']= $files['userfile']['size'][$i];
	//$this->upload->initialize($this->set_upload_options());
	$this->upload->do_upload();
	//get file name
    $fileName = $_FILES['userfile']['name'];
	$images[] = $fileName;
	$targetFile = $targetDir.$national_id."_".$fileName; 
	if(move_uploaded_file($_FILES['userfile']['tmp_name'],$targetFile))
	{   
		//check if the file exist 
		$check=$this->Tenant_model->get_data("media", array('media'=>$national_id."_".$fileName,'tenant_id'=>$id));
		if($check->num_rows()<=0){   
			// upload files to images directiry and also  to the database
			$data=array('media'=>$national_id."_".$fileName,'tenant_id'=>$id,'date_added'=>date("Y-m-d H:i:s"));
			//pass parameter with data to model
			$this->Tenant_model->insert_data("media", $data); 
		  
		} 	
	}
	
	}
 
 $fileName = implode(',',$images); 
	 	 	  
}

	  
//modified function
public function update_payment_info2()
	{
		$d=$this->Tenant_model->get_data("tenant_property_units",array('company_code'=>54845,'audit_number'=>1 ));
		foreach($d->result() as $row){
		$tenant_id=$row->id; $unit_id=$row->property_unit_id; $rent_frequency=$row->rent_frequency; 
	    $total_pay=0; $deposits=0;  $payment_type=""; 
		$data=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id));	
		 
		if($data->num_rows()>0)
		{
			foreach($data->result() as $r)
			{   
			$payment_type_value=0; $is_deposit=0; $priority=0;
				$q=$this->Property_model->get_data($table="tenant_pricing", array('unit_id'=>$unit_id,'tenant_id'=>$tenant_id,'payment_type'=>$r->payment_type));
				if($q->num_rows()==0)
				{
				 $payment_type=$r->payment_type;  $payment_type_value=$r->payment_type_value; $is_deposit=$r->is_deposit; $priority=$r->priority; 
					if($payment_type_value>0)
					{   
				     echo "<br/>Tenant Id: ".$tenant_id ." Unit ".$unit_id." ". $payment_type=$r->payment_type ." ".$payment_type_value=$r->payment_type_value;
			         $this->Property_model->insert_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'unit_id'=>$unit_id,'payment_type'=>$payment_type,'payment_type_value'=>$payment_type_value,'priority'=>$priority,'is_deposit'=>$is_deposit));
					}
				}
			 }
		} 
			
		if($deposits>0)
		{
			//$this->Property_model->insert_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'date_paid'=>date("m/d/Y"),'amount'=>$deposits,'description'=>'Deposit','payment_mode'=>'','payment_mode_code'=>'','receipt_no'=>'','type'=>'d'));
		}
		//$this->Property_model->insert_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'date_paid'=>date("m/d/Y"),'amount'=>$total_pay,'payment_mode'=>'','description'=>'Rent', 'payment_mode_code'=>'','receipt_no'=>'','type'=>'d'));
		$obj=new rent_class; 
	 
		$expected_pay_day=$obj->expected_pay_day($tenant_id);
		//$this->Rent_model->update_data($table="tenant_current_account", array('rent_balance'=>$total_pay,'deposit_balance'=>$deposits,'total_amount'=>($total_pay+$deposits),'expected_pay_date'=>$expected_pay_day),  array('tenant_id'=>$tenant_id));
		} 
	}
	
	public function updateBal()
	{ 
		$d=$this->Tenant_model->get_data("tenant_property_units",array('audit_number'=>1)); 
		foreach($d->result() as $row)
		{
			 $tenant_id=$row->id; $unit_id=$row->property_unit_id; $rent_frequency=$row->rent_frequency; 
			$total_pay=0; $deposits=0;  
			$data=$this->Property_model->get_data($table="current_account", array('tenant_id'=>$tenant_id));	
			if($data->num_rows()>0)
			{
				$y = $data->row();  
				$expected_pay_date=$y->expected_pay_date;
				$balance=$y->balance;  
				$dt=array('tenant_id'=>$tenant_id,
							'rent_balance'=>$balance,
							'total_amount'=>$balance,
							//`last_pay_date` DATE NULL DEFAULT NULL,
							'expected_pay_date'=>$expected_pay_date
						);
				 
			  $t=$this->Property_model->get_data($table="tenant_current_account", array('tenant_id'=>$tenant_id));	
			 if($t->num_rows() ==0)
			 {  
				 $this->Tenant_model->insert_data($table="tenant_current_account", $dt); 
			 } 
			 
			}  
		}  
	}	
	
	public function update_deposit()
	{
		$data=$this->Property_model->get_data($table="pricing_details",array('is_deposit'=>0));	
		 
		if($data->num_rows()>0)
		{
			foreach($data->result() as $r)
			{   
			    $payment_type_value=0; $amount=0; $is_deposit=0; $priority=0;
				$unit_id=$r->unit_id; $payment_type=$r->payment_type; $id=$r->id; $is_deposit=$r->is_deposit;$priority=$r->priority;
			    $q=$this->Property_model->get_data($table="tenant_deposit", array('pricing_details_id'=>$id));
				if($q->num_rows()>0)
				{ $row = $q->row(); $amount=$row->amount;
				if($amount>0) 
				{ 
			    $c=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id,'payment_type'=>$payment_type,'is_deposit'=>1));
				if($c->num_rows()==0)
				{
				   echo "<br/>Not Existing! Pricing Id".$id." Unit ".$unit_id." ". $payment_type=$r->payment_type ."Amount ".$amount ." ".$priority;
				}
				 
				}
				
				}
			 }
		}
			 
	} 
	public function update_tenant_dep()
	{
		$data=$this->Property_model->get_data($table="pricing_details");	
		if($data->num_rows()>0)
		{
			foreach($data->result() as $r)
			{ 
			    
			    $payment_type_value=0; $is_deposit=0; $priority=0; $unit_id=$r->unit_id;   $is_deposit=$r->is_deposit;
				$d=$this->Tenant_model->get_data("tenant_property_units",array('property_unit_id'=>$unit_id,'audit_number'=>1)); 
				foreach($d->result() as $row)
				{
					 $tenant_id=$row->id; 
						$q=$this->Property_model->get_data($table="tenant_pricing", array('unit_id'=>$unit_id,'tenant_id'=>$tenant_id,'payment_type'=>$r->payment_type));
						if($q->num_rows()>0)
						{
						  $payment_type=$r->payment_type; $payment_type_value=$r->payment_type_value; $is_deposit=$r->is_deposit; $priority=$r->priority; 
						$q=$this->Property_model->update_data($table="tenant_pricing", array('unit_id'=>$unit_id,'tenant_id'=>$tenant_id,'payment_type'=>$payment_type), array('is_deposit'=>$is_deposit));
						
						}	 
				}
				
			}
		}
		} 
		
public function invoice_settings()
{
	
	$d=$this->Tenant_model->get_data("user_info",array('audit_number'=>1,'user_added_by'=>0)); 
	foreach($d->result() as $row)
	{
		   $company_code=$row->company_code; 
		$total_pay=0; $deposits=0;  
		$data=$this->Tenant_model->get_data($table="invoice_settings", array('company_code'=>$company_code));	
		if($data->num_rows()==0)
		{ 
			 $this->Tenant_model->insert_data($table="invoice_settings", array('company_code'=>$company_code)); 
		
		}  
	}  
}
 

public function listInvoiceItems($tenant_id="",$deposits="")
{  
        $deposit_collected=0;
        if($tenant_id ==""){    }else{
		$q = $this->Rent_model->get_data($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id),$order_by="priority",$limit="",$asc_desc="ASC");
		if($deposits==0)
		{
			$deposit_collected=1;
			$q = $this->Rent_model->get_data($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id,'is_deposit'=>0),$order_by="priority",$limit="",$asc_desc="ASC");
		}
		$it_exist=0; $items = array();   
		foreach($q->result_array() as $u)
			 {  
				  array_push($items,array(
					"id"=>$u['id'],"audit_number"=>1,"tenant_id"=>$u['tenant_id'],
					"payment_type"=> $u['payment_type'],
					"payment_type_value" => $u['payment_type_value'],
					"description" => $u['description'],
					"priority"=> $u['priority'],
					"is_deposit"=>$u['is_deposit'],
					"is_one_time"=>$u['is_one_time'],
					"invoice_no"=>$u['invoice_no']
					)); 			 
			} 
		$company_code=$this->session->userdata('company_code'); 
		$query=$this->Tenant_model->get_data(array('tenant_property_units'),array('company_code'=>$company_code,'audit_number'=>1,'tenant_status'=>1), $order_by="id");
		$d=$query->row(); $rent_frequency=$d->rent_frequency; 
		
		$t=$this->Property_model->get_data($table="tenant_current_account", array('tenant_id'=>$tenant_id));	
		$y = $t->row();  $expected_pay_date=$y->expected_pay_date;
				
	    $month=date('m'); $year=date('Y'); $date_created=date('d-m-Y');
		$c=$this->Rent_model->get_data("invoice", $condition=array('tenant_id'=>$tenant_id,'status'=>0,'month'=>$month,'year'=>$year)); 
		if($c->num_rows()==0 && !empty($items))
		{
		    $amount=0;
			foreach($items as $row){ $amount=$amount+$row['payment_type_value']; }
			$invoice_no=rand(12381,1238498);
			$this->Rent_model->insert_data("invoice", array('tenant_id'=>$tenant_id,'company_code'=>$company_code,'invoice_no'=>$invoice_no,'amount'=>$amount,'balance'=>$amount,'date_generated'=>$date_created,'due_date'=>$expected_pay_date,'month'=>$month,'year'=>$year,'status'=>0,'is_first_invoice'=>1,'deposit_collected'=>$deposit_collected));
		}
	}

}

public function tenant_login()
{
		$this->load->view('accounts/header');
		$this->load->view('accounts/tenant_login');
		$this->load->view('accounts/footer'); 
}
 
}
?>