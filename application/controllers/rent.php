<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rent extends CI_Controller {
	public function __construct()
	{
		parent::__construct();   
		$this->load->model("Rent_model");
		$this->load->model("Tenant_model"); 		
		$this->load->model("Payment_model"); 		
		$this->load->model("Property_model");
		$this->load->library('Expired');
		$this->load->library('Encrypte');
		$this->load->helper('security');
		$this->load->library('rent_class');  
		$this->load->library('send_mails');  
		$this->load->library('pdf');
		$this->load->library('encrypt');
        date_default_timezone_set('Africa/Nairobi'); 
		$obj=new Expired();
		ini_set("memory_limit","512M");
		 
		$this->disabled=$obj->check_subscription_expiry(); 		
	}
 
	public function paid($page='paid_rent')
	{
		$data['paid_rent']=$this->Rent_model->get_paid_rent($table="tenant_transaction",  array('tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_transaction.type'=>'c','tenant_transaction.audit_number'=>1)); 
		$month=date("m"); $year=date('Y');
		$this->db->select('*')
		 ->from("tenant_transaction_view")
		->where(array('company_code'=>$this->session->userdata('company_code'),'type'=>'d','audit_number'=>1))
		->where("SUBSTRING(date_paid,1,2)",$month)
		->where("SUBSTRING(date_paid,-4,4)",$year)
		->group_by('tenant_transaction_view.transaction_id');
		$expected_rent=$this->db->get();
		
		$this->db->select('transaction_id,SUM(amount) AS amount')
		 ->from("tenant_transaction_view")
		->where(array('company_code'=>$this->session->userdata('company_code'),'type'=>'c','audit_number'=>1)) 
		->where("SUBSTRING(date_paid,1,2)",$month)
		->where("SUBSTRING(date_paid,-4,4)",$year)
		->group_by('tenant_transaction_view.transaction_id');
		$paid_rent=$this->db->get();
		
		$paid=0; $expected_pay=0; $rent_not_paid=0;
		foreach($paid_rent->result()  as $row){  $paid=$paid+$row->amount; } 
		foreach($expected_rent->result()  as $r) { $expected_pay=$expected_pay+$r->amount; }  
		$data['expected_pay']=$expected_pay; $data['paid']=$paid; $data['rent_not_paid']=$expected_pay-$paid; 
		$this->load->view('accounts/header');
		$this->load->view('accounts/'.$page,$data);  
		$this->load->view('accounts/footer'); 
	}
 
	//original function
	public function receiveRent($id="")
	{
		if($id==""){ $id=$this->input->post('from'); }
		$query=$this->Tenant_model->get_data("tenant_property_units", array('audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		$data['tenants']=$this->Tenant_model->get_data("tenant_property_units", array('audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		foreach($query->result() as $f){ $unit_id=$f->property_unit_id;}
		if($id=="")
		{
			$data['received_rent']="";
			$data['rent_items']="";
		}
		else
		{ 
		$data['received_rent']=$this->Rent_model->get_data("tenant_transaction_view", array('audit_number'=>1,'type'=>'c','company_code'=>$this->session->userdata('company_code')));
		$data['rent_items']= $this->listItemsToPay($id);
		 $r=$this->get_balance($id);
		}
		
		$data['company_details']=$this->Rent_model->get_data($table="company_info", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')));
		$data['disabled']=$this->disabled; 
		$data['selected_tenant']=$this->Tenant_model->get_data("tenant_property_units",array('id'=>$id,'audit_number'=>1)); 
		$this->load->view('accounts/header');
		$this->load->view('accounts/receivePay',$data);
		$this->load->view('accounts/footer');
	}
	 
 public function statement($enct="")
	{ 
	     if($enct !=""){   $id=$this->input->post('id'); } 
		$data['id']=$enct;
		$hashed=new Encrypte();
		$id=$hashed->hashId($enct);
		$data['tenant_details']= $this->Rent_model->get_data("tenant_property_units",$condition=array('id'=>$id)); 
		$data['units_details']= $this->Rent_model->get_data($table="unit_property_details"); 
		$data['statement']= $this->Rent_model->get_statement($condition=array('tenant_id'=>$id)); 
		
		if($this->input->post('from') !="" && $this->input->post('to') !="" )
		{ 
			$from=$this->input->post('from'); 
			$to=$this->input->post('to');
			$data['statement']=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id),$from,$to);
			//$data['statement']=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1),$from,$to);
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id),$from,$to);
			//$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1),$from,$to);
			if($q->num_rows()<=0){  $data['tenant_details']= $this->Rent_model->get_data("tenant_property_units",$condition=array('id'=>$id));  }
		    
		}  
		
	 $bal=0;$curr_bal=0; $credit=0; $debit=0; 
	   if($id==""){ redirect(base_url() ."rent/paid"); }else{
		$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id));
		//$q=$this->Rent_model->get_statement($condition=array('tenant_transaction_view.audit_number'=>1,'tenant_id'=>$id));
		if($q->num_rows()>0){
			foreach($q->result() as $row)
			{
				if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
				if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
			}
			$bal=$debit-$credit;
			$this->Rent_model->update_data($table="tenant_current_account", array('rent_balance'=>$bal), array('tenant_id'=>$id));
			$data['rent_due']=$this->Rent_model->get_data($table="tenant_current_account",  array('tenant_id'=>$id));
			$data['users']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'user_added_by'=>0), $table="user_info");
			$data['company_info']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')), $table="company_info");
			$this->load->view('accounts/header');
			$this->load->view('accounts/statement',$data);
			$this->load->view('accounts/footer');
			}
		else{
				redirect(base_url() ."rent/paid"); 
			}			 
		 }   
	}

public function rent_statement($id="")
	{ 
		$bal=0;$curr_bal=0; $credit=0; $debit=0; 
		if($this->input->post('from') !="" && $this->input->post('to') !="")
		{   
			$from=$this->input->post('from'); 
			$to=$this->input->post('to');
			$data['statement']=$this->Rent_model->get_statement($condition=array('company_code'=>$this->session->userdata('company_code'),'tenant_transaction_view.audit_number'=>1),$from,$to); 
		}
		else{  
		if($id=="")
		{
			$data['statement']= $this->Rent_model->get_statement($condition=array('unit_property_details.company_code'=>$this->session->userdata('company_code'),'tenant_transaction_view.audit_number'=>1));
		}else{
			$data['id']=$id;
			$data['statement']= $this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1));  
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1));
				if($q->num_rows()>0)
				{
					foreach($q->result() as $row)
					{
						if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
						if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
							
					}
				}
		 
			$bal=$debit-$credit;
			$this->Rent_model->update_data($table="tenant_current_account", array('rent_balance'=>$bal), array('tenant_id'=>$id));
			$data['rent_due']=$this->Rent_model->get_data($table="tenant_current_account",  array('tenant_id'=>$id));   
			}
		}	 
		$this->load->view('accounts/header');
		$this->load->view('accounts/rent_statement',$data);
		$this->load->view('accounts/footer');		
	}
	 
 public function editRentDebit()
	{
		$bal=0; $curr_bal=0; $edit_amount=0;  $payment_type_bal=0;  $temp_val=0; $item_bal=0; $credit=0; $debit=0; $tenant_transaction_id=0;
		$edit_id=$this->input->post('edit_id');
		$tenant_id=$this->input->post('tenant_id');
		$amount=$this->input->post('amount'); 
		$pay_from=$this->input->post('from'); 
	  
		$data=array('payment_mode'=>$this->input->post('pay_mode'), 
		'payment_mode_code'=>$this->input->post('receipt_no'),
		'date_paid'=>$this->input->post('date'),
		//'balance'=>$edit_bal,
		'amount'=>$this->input->post('amount')  
		);		
		$edit_amount=$this->input->post('amount');
		$this->Rent_model->update_data($table="tenant_transaction",$data, $condition=array('id'=>$edit_id,'tenant_id'=>$tenant_id));
		
		$check_bal=$this->Rent_model->get_data($table="tenant_transaction" , array('tenant_id'=>$tenant_id));
		if($check_bal->num_rows()>0){ foreach($check_bal->result() as $balance){ if($balance->type=="c"){$credit=$credit+$balance->amount;}else if($balance->type=="d"){ $debit=$debit+$balance->amount;} } } 
		if($credit>=$debit){ $curr_bal=(0-$credit-$debit);} else if($credit<$debit){$curr_bal= $debit-$credit;}
 		 
		if($this->Rent_model->update_data($table="tenant_current_account",array('rent_balance'=>$curr_bal), $condition=array('tenant_id'=>$tenant_id)))
		{
			echo json_encode(array('result'=>"ok"));
		}
		else 
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		} 
	
}

public function printReceipt($id="",$tenant_id="",$receipt="")
	{
	$hashed=new Encrypte(); $id=$hashed->hashId($id);
	if(!empty($this->input->post('checkbox'))){ foreach($this->input->post('checkbox') as $value) {  $id=$value; } }
	$current_reading=0; $previous_reading=0; $water_cost=0; $balance=0;
	$receipt_no="";
	 if($tenant_id=="")
		{   
			$q=$this->Rent_model->get_data($table="tenant_transaction", array('id'=>$id));
		}
		else if($id=="")
		{ 
			 $tenant_id=$hashed->hashId($tenant_id); 
			 $receipt_no=$id=$hashed->hashId($receipt);
			  $q=$this->Rent_model->get_data($table="tenant_transaction_view", array('tenant_id'=>$tenant_id,'company_code'=>$this->session->userdata('company_code'),'receipt_no'=>$receipt_no));
		}
		  
		if($q->num_rows()>0)
		{
			$row=$q->row();  $tenant_transaction_id=$row->id; $balance=$row->balance; $payment_mode=$row->payment_mode;$amount=$row->amount; $receipt_no=$row->receipt_no;$payment_mode_code=$row->payment_mode_code;
			$tenant_id=$row->tenant_id; $date=$row->date_paid;$pay_for='internet';
			
			$this->session->set_userdata(array('amount'=>$amount,'date_paid'=>$date,'pay_for'=>$pay_for,'payment_mode'=>$payment_mode,'receipt_no'=>$receipt_no));
			$data['amount']=$amount; $data['date_paid']=$date; $data['pay_for']=$pay_for;$data['balance']=$balance; $data['payment_mode']=$payment_mode; $data['receipt_no']=$receipt_no; $data['payment_mode_code']=$payment_mode_code; 
			$data['tenant_details']=$this->Rent_model->get_data($table="tenant_property_units", array('id'=>$tenant_id));
			$data['company_details']=$this->Rent_model->get_data($table="company_info",array('company_code'=>$this->session->userdata('company_code')));
			$data['user_info']=$this->Rent_model->get_data($table="user_info",array('company_code'=>$this->session->userdata('company_code'),'user_added_by'=>0));
			$data['receipt_items']=$this->Rent_model->get_data("tenant_payment", array('tenant_payment.transaction_id'=>$tenant_transaction_id,'tenant_payment.paid_amount >'=>0));
			 $data['statement']="";// $this->Rent_model->get_statement($condition=array('tenant_id'=>$tenant_id)); 
			$data['current_balance']=$balance;//$this->get_balance($tenant_id);
			$data['balance']=$balance;
            $month=date("m",strtotime($date));			
            $year=date("Y",strtotime($date));			
			$water_cost=0; 
			$data['receipt_no']=$receipt_no;  
			$this->load->view('accounts/header');
			$this->load->view('accounts/receipt', $data);
			$this->load->view('accounts/footer'); 
		 
		} 
		else{
				 redirect(base_url().'rent/receiveRent');
			}    
	}
	
 
	public function edit_tenant_transact()
	{
		$data=array('tenant_id'=>$this->input->post('tenant'),
		'payment_mode'=>$this->input->post('pay_mode'), 
		'payment_mode_code'=>$this->input->post('receipt_no'),
		'date_paid'=>date('d-m-Y'),
		'amount'=>$this->input->post('amount') ,
		'type'=>$this->input->post('type'),
		'description'=>$this->input->post('description') 
		); 
		$this->Rent_model->insert_data($table="tenant_transaction", $data);
		if($this->db->affected_rows()>0)
		{ 
			$bal=$this->get_balance($this->input->post('tenant'));
			$this->Rent_model->update_data($table="tenant_current_account",array('rent_balance'=>$bal), $condition=array('tenant_id'=>$this->input->post('tenant')));
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else 
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	
  public function save_expenses($page="property_expenses")
	{
		$edit_id=$this->input->post('edit_id');
		$id=$this->input->post('from');
		$property_id=$this->input->post('property');
		$expense_type=$this->input->post('expense_type');
		$invoice=$this->input->post('invoice');
		$receipt=$this->input->post('receipt_no'); $system_invoice='A'.date("m").date("d").rand(123,1200).date("y")."R";
		if($this->input->post('receipt_no')=="" || $receipt==""){ $receipt="";} 
		if($this->input->post('pay_mode')=="Cash"){ $receipt="";}
		$data=array('supplier_id'=>$id,
		'payment_mode'=>$this->input->post('pay_mode'),
		'expense_type'=>$expense_type,
		'description'=>$this->input->post('pay_for'),
		'supplier_invoice_no'=>$invoice,
		'invoice_no'=>$system_invoice,
		'property_id'=>$property_id,
		'company_code'=>$this->session->userdata('company_code'),
		'payment_method_code'=>$receipt,
		'date_paid'=>date('Y-m-d'),
		'amount'=>$this->input->post('amount')  
		); 
		if($edit_id==""){ 
			if($this->Rent_model->insert_data($table="expenses",$data))
			{ 
				$msg='Payment information saved successfully';
				//redirect(base_url().'rent/'.$page);
				 echo json_encode(array('result'=>"ok",'msg'=>$msg));
			}
			else
			{
				$msg='Payment was not saved successfully. Try again later';
				 echo json_encode(array('result'=>"false",'msg'=>$msg));
			 //redirect(base_url().'rent/'.$page);
			} 
		}
	else
	{//expense_type_id'=>$this->input->post('pay_for'),
	 $this->Rent_model->update_data($table="expenses",
		$data=array('payment_mode'=>$this->input->post('pay_mode'),'payment_method_code'=>$receipt, 'date_paid'=>date('m/d/Y'), 'amount'=>$this->input->post('amount') ,
		'description'=>$this->input->post('pay_for'),'supplier_invoice_no'=>$invoice),
		array('id'=>$edit_id));
		$msg='Data Updated successfully';
	  echo json_encode(array('result'=>"ok",'msg'=>$msg));
	 //redirect(base_url().'rent/'.$page);
	}		
	}
	
	
	public function payment_details($id)
	{
		$q=$this->Rent_model->get_data($table="tenant_transaction_view", array('transaction_id'=>$id));
		if($q->num_rows()>0)
		{ 
		 
			 echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else 
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
 
	 
	
	public function deposits()
	{
		$id=$this->input->post('tenant_id');
		$data['deposits']=""; 
		$data['selected_tenant']=$id; 
		$data['pay_details']=$this->Property_model->get_data($table="tenant_pricing",array('audit_number'=>1));
		$data['tenant_deposits']=$this->Property_model->get_data($table="tenant_pricing",array('audit_number'=>1,'is_deposit'=>1));
		$data['price_details']=$this->Tenant_model->get_data($table="pricing_details");
		//$data['tenant_deposits']="";//$this->Tenant_model->get_data($table="tenant_deposit");
		$data['tenants']=$this->Tenant_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code')));
		if(empty($id))
		{
				
		}
		else
		{ 
		//$data['rent']=$this->Rent_model->paidRent($condition=array('company_code'=>$this->session->userdata('company_code'),'month'=>date("m")));
		 $data['tenant_details']=$q=$this->Tenant_model->get_data("tenant_property_units", array('id'=>$id));
         foreach($q->result() as $row){ $type=$row->tenant_type;} 
		 $data['property']=$this->Tenant_model->get_data("property", $condition=array('id'=>$id));
		 $data['properties']=$this->Tenant_model->get_data("property",array('company_code'=>$this->session->userdata('company_code')));
		 $data['payment_details']=$this->Property_model->get_data($table="tenant_pricing", array('tenant_id'=>$id));
		 $data['categories']=$this->Property_model->get_data($table="unit_property_details", array('company_code'=>$this->session->userdata('company_code')));
		 $data['deposits']=$this->Tenant_model->get_data($table="tenant_payment", array('tenant_id'=>$id,'paid_amount >'=>0, 'is_deposit'=>1));
		}
		$data['refunded_deposits']=$this->Tenant_model->get_data($table="refunded_deposits_view", $condition=array('company_code'=>$this->session->userdata('company_code')), $order_by="no");
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/deposits',$data);
		$this->load->view('accounts/footer');
	}
  	
  	
  	public function refund_deposits()
	{  
		$id=$this->input->post('tenant_id');
		$data=array(
		'tenant_id'=>$id,
		'property_id'=>$this->input->post('property_id'),
		'amount'=>$this->input->post('amount'),
		'date_refunded'=>date('Y-m-d')
		);
		$d=$this->Rent_model->get_data($table="refunded_deposits",array('tenant_id'=>$id));
		if($d->num_rows()>0)
		{
			//$this->session->set_flashdata('temp','Deposit not refunded  Ensure that Tenant has not  been previously refunded');
			echo json_encode(array('result'=>"false",'msg'=>'Deposit cannot be refunded more than once'));
		}
		else{
				if($this->Rent_model->insert_data($table="refunded_deposits",$data))
				{ 
					//$this->session->set_flashdata('temp','Deposit refunded successfully');
					echo json_encode(array('result'=>"ok",'msg'=>'Deposit refunded successfully'));
					//redirect(base_url().'rent/'); 
				}
			}	 
		//$this->deposits(); 
	}
	
	
	public function receipts()
	{
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/rent_receipts',$data);
		$this->load->view('accounts/footer');
	}
	
	public function deleted_receipts()
	{
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/deleted_receipts',$data);
		$this->load->view('accounts/footer');
	}
	
 	public function removed_receipts()
	{
		$data['tenants']=$this->Tenant_model->get_data("tenant_property_units",array('audit_number'=>1, 'company_code'=>$this->session->userdata('company_code')));
		$data['received_rent']=$this->Rent_model->get_data("tenant_transaction_view",array('audit_number'=>2,'is_deleted'=>1,'type'=>'c','company_code'=>$this->session->userdata('company_code')));
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/deleted_receipts',$data);
		$this->load->view('accounts/footer');
	}
 
 public function rentDebit($id="")
	{
		if($id==""){$id=$this->input->post('id');}
		$data['tenants']=$this->Tenant_model->get_data("tenant_property_units", array('audit_number'=>1, 'company_code'=>$this->session->userdata('company_code')));
		if(!$id)
		{
			$data['rent_statement']="";
		}
		else{
				$data['rent_statement']=$this->Rent_model->get_data("tenant_transaction_view",array('tenant_id'=>$id,'type'=>'d','company_code'=>$this->session->userdata('company_code')));
			}
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/rentDebit',$data);
		$this->load->view('accounts/footer');
	}
 
  	public function banks()
	{
		$data['bank_details']=$this->Tenant_model->get_data($table="bank", array('company_code'=>$this->session->userdata('company_code')),$order_by="id",$asc_desc="DESC");
		$data['tenant_transaction']=$this->Tenant_model->get_data($table="tenant_transaction_view", array('company_code'=>$this->session->userdata('company_code')),$order_by="transaction_id",$limit="", $asc_desc="DESC");
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/bank_details',$data);
		$this->load->view('accounts/footer');
	} 

	public function bank_transactions()
	{
		$id=$this->input->post('account_id');
		$data['bank_details']=$this->Tenant_model->get_data($table="bank", array('id'=>$id,'company_code'=>$this->session->userdata('company_code')),$order_by="id",$asc_desc="DESC");
		$data['tenant_transaction']=$this->Tenant_model->get_data($table="tenant_transaction_view", array('bank_account_id'=>$id,'company_code'=>$this->session->userdata('company_code')),$order_by="transaction_id", $limit="",$asc_desc="DESC");
		$this->load->view('accounts/header');
		$this->load->view('accounts/bank_transaction',$data);
		$this->load->view('accounts/footer');
	}
	
	public function edit_tenant_statement()
	{
		$data['tenants']=$this->Tenant_model->get_data('tenant_property_units',array('company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		//$data['rent']=$this->Rent_model->paidRent($condition=array('company_code'=>$this->session->userdata('company_code'),'month'=>date("m")));
		$data['received_rent']=$this->Rent_model->get_data("tenant_transaction_view", array('type'=>'c','company_code'=>$this->session->userdata('company_code')));
		//$data['payment_for']=$this->Rent_model->payment_for();
		$this->load->view('accounts/header');
		$this->load->view('accounts/edit_tenant_statement',$data);
		$this->load->view('accounts/footer');
	}
	
	
	public function property_expenses()
	{
		$data['disabled']=$this->disabled;
		$data['property']=$this->Rent_model->get_data($table="property", $condition=array('company_code'=>$this->session->userdata('company_code')), $order_by="id", $limit="", $asc_desc="DESC");
		$data['company_details']=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['suppliers']=$this->Rent_model->get_data($table="suppliers", $condition=array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id", $limit="", $asc_desc="DESC");
		$data['expenses']=$this->Rent_model->get_data($table="expense_type",$condition=array('company_code'=>$this->session->userdata('company_code')));
		$data['paid_expenses']=$this->Rent_model->getExpenses(array('expense_type'=>1,'expenses.audit_number'=>1,'suppliers.company_code'=>$this->session->userdata('company_code')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/property_expenses',$data);
		$this->load->view('accounts/footer');
	}
	
	public function business_expenses()
	{
		$data['disabled']=$this->disabled;
		$data['property']=$this->Rent_model->get_data($table="property", $condition=array('company_code'=>$this->session->userdata('company_code')), $order_by="id",$asc_desc="DESC");
		$data['suppliers']=$this->Rent_model->get_data($table="suppliers", $condition=array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id", $limit="", $asc_desc="DESC");
		$data['expenses']=$this->Rent_model->get_data($table="expense_type",$condition=array('company_code'=>$this->session->userdata('company_code')));
		$data['paid_expenses']=$this->Rent_model->getExpenses(array('expense_type'=>2,'expenses.audit_number'=>1,'suppliers.company_code'=>$this->session->userdata('company_code')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/business_expense',$data);
		$this->load->view('accounts/footer');
	}
	
  
  public function suppliers()
	{
		$data['disabled']=$this->disabled;
		$data['suppliers']=$this->Rent_model->get_data($table="suppliers", $condition=array('supplier_deleted'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id");
		$this->load->view('accounts/header');
		$this->load->view('accounts/suppliers',$data);
		$this->load->view('accounts/footer');
	}
	
	public function showSupplier($id)
	{
		$q=$this->Rent_model->get_data($table="suppliers", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code')));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		} 
	}

public function expense_statement($id="")
	{ 
		 $data['suppliers']=$this->Rent_model->get_data($table="suppliers", $condition=array('company_code'=>$this->session->userdata('company_code')), $order_by="id");
		  
		if($this->input->post('from') !="" &&  $this->input->post('to') !="")
		{   
			$from=$this->input->post('from'); 
			$to=$this->input->post('to');
			$data['statement']=$this->Rent_model->get_expense_statement($condition=array('company_code'=>$this->session->userdata('company_code')),$from,$to); 
		 }
		else{  
				if($id=="")
				{
					$data['statement']= $this->Rent_model->get_expense_statement(array('company_code'=>$this->session->userdata('company_code')));
				}
				else
				{
					$data['id']=$id;
					$data['statement']= $this->Rent_model->get_expense_statement($condition=array('company_code'=>$this->session->userdata('company_code'),'supplier_id'=>$id));  
					$q=$this->Rent_model->get_expense_statement($condition=array('company_code'=>$this->session->userdata('company_code'),'supplier_id'=>$id));
				}
			}	
			$this->load->view('accounts/header');
			$this->load->view('accounts/expense_statement',$data);
			$this->load->view('accounts/footer');		
	}
	
	public function paid_expenses()
	{
		$q=$this->Rent_model->get_data($table="expense_type",$condition=array('company_code'=>$this->session->userdata('company_code')), $order="id", $asc_desc="asc");
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
	
	
	public function getPrevBalance($id="", $par2="")
	{
		    $credit=0; $debit=0; $prev_bal=0; $rent=0;
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id));
			if($id=="")
			{
				echo json_encode(array('result'=>"false"));
			}
			else
			{
			if($q->num_rows()>0)
				{
					$count=$q->num_rows();
					foreach($q->result() as $row)
					{
						if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
						if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
						//if($count==2){  $prev_bal=$debit-$credit; }	
						if($row->type=='d'){  $rent=$row->amount; }
						$count--;
					} 
				}
			}
		
		//$return_val= $prev_bal;
		$return_val= $rent;
		if($par2==""){ } else{  $return_val=$rent; }
		
		 
		return $return_val;
	}
	
	
	public function checkPaidDeposit($id="")
	{
		$amount="";
		$q=$this->Rent_model->get_data($table="tenant_payment", array('tenant_id'=>$id,'is_deposit'=>1));
		if($q->num_rows()>0)
		{
				echo json_encode(array('result'=>'ok'));
		}
		else{
        echo json_encode(array('result'=>'false'));
		}
	}

	public function getRentFrequency($id="")
	{
		$rent_frequency="";
		$q=$this->Rent_model->get_data($table="tenant", array('id'=>$id));
		foreach($q->result() as $r){ $rent_frequency=$r->rent_frequency;} 
        echo json_encode(array('result'=>'ok','rent_frequency'=>$rent_frequency));
	}
	
	public function getTenantBalance($id="")
	{
		$bal=0; $bf=0; $rb=0; $balance=0;
		if($id=="")
		{
			echo json_encode(array('result'=>"false"));
		}
		else
		{
			$if_bal_exists = $this->Rent_model->get_data($table="tenant_payment", array('audit_number'=>1,'balance >'=>0,'tenant_id'=>$id,'status'=>0));
			foreach($if_bal_exists->result() as $r){ $bf=$bf+$r->balance; }
			$bal=$this->getPrevBalance($id);
			$bl=$this->Rent_model->get_data($table="tenant_current_account" , array('tenant_id'=>$id));
			if($bl->num_rows()>0){  foreach($bl->result() as $b){ $balance=$b->total_amount;$rb=$b->rent_balance; } }	
		     	
		   echo json_encode(array('result'=>"ok",'balance'=>$balance,'balance_forwarded'=>$bf,'rent'=>$bal,'expected_pay'=>$balance));
		}
	}
	
	
	public function get_paid_expenses($id="")
	{
		$q=$this->Rent_model->get_data($table="expenses",$condition=array('id'=>$id));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
	public function get_balance($id)
	{
		 $credit=0; $debit=0; 
		 $check_bal=$this->Rent_model->get_data($table="tenant_transaction" , array('tenant_id'=>$id));
		 if($check_bal->num_rows()>0){ foreach($check_bal->result() as $balance){ if($balance->type=="c"){$credit=$credit+$balance->amount;}else if($balance->type=="d"){ $debit=$debit+$balance->amount;} } } 
		 //if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit){$curr_bal= $debit-$credit;}
		 $curr_bal= $debit-$credit;
		 $this->Rent_model->update_data($table="tenant_current_account", array('total_amount'=>($curr_bal)),  array('tenant_id'=>$id));
		 return  $curr_bal;
	}

	public function update_receipt()
	{ 
		  $curr_bal=0; $water_cost=0; $rent_frequency=""; $expiry_date="";
		  $query=$this->Tenant_model->get_data("tenant_property_units",array('tenant_deleted'=>1,'tenant_status'=>1));
		  if($query->num_rows()>0)
		  {
			foreach($query->result() as $row)
			{ 
			    $total_pay=0; $deposits=0;
				$tenant_id=$row->id; $unit_id=$row->property_unit_id; $rent_frequency=$row->rent_frequency;
				$q=$this->Tenant_model->get_data($table="tenant_current_account", array('tenant_id'=>$tenant_id));
				foreach($q->result() as $e){ $expiry_date=$e->expected_pay_date; $deposits=$e->deposit_balance; $curr_bal=$e->rent_balance;}
				$now=time();  
				$water_cost=0;//$this->getWaterBill($tenant_id);
				$diff_time=strtotime($expiry_date)-$now; 
				$days=floor($diff_time/(60*60*24));  
					if($days<0)
					{  
						$data=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id));	
						if($data->num_rows()>0)
						{ 
							$t=$this->Property_model->get_data($table="tenant_pricing", array('unit_id'=>$unit_id,'tenant_id'=>$tenant_id,'is_deposit'=>0 ));
							foreach($t->result() as $d)
							{
								$total_pay=($total_pay+$d->payment_type_value);
								$id=$d->id; $payment_type_value=$d->payment_type_value; 						
							}
						} 
					 //if rent frequency is Quarterly multiply total_pay by 3 and if its Yearly multiply by 12
					 if($rent_frequency=="Quarterly"){ $total_pay=$total_pay*3;} if($rent_frequency=="Yearly"){ $total_pay=$total_pay*12;}
					 //end
					 
					$total_pay=($total_pay+$water_cost);//update total pay= rent+water_cost
					$invoice='A'.date("m").date("d").rand(10,1210).date("y")."L";     
					$this->Rent_model->insert_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'date_paid'=>date("m/d/Y"),'amount'=>$total_pay,'payment_mode'=>'','payment_mode_code'=>$invoice,'description'=>'Rent', 'payment_mode_code'=>'','type'=>'d'));
					$expected_pay_date=$this->get_next_pay($tenant_id); 
					if($expected_pay_date==0 || $expected_pay_date==""){		continue;	}else{		 
					$this->Rent_model->update_data($table="tenant_current_account", array('rent_balance'=>($total_pay+$curr_bal),'total_amount'=>($deposits+$total_pay+$curr_bal),'expected_pay_date'=>$expected_pay_date),  array('tenant_id'=>$tenant_id));
					}
						//send email to tenant if required 
					
					}  
				}
			  
			 echo json_encode(array('result'=>"ok",'data'=>1));
		  }
		  else
		  {
				echo json_encode(array('result'=>"false",'data'=>0));  
		  } 
	}
	
	
	public function get_next_pay($tenant_id){
		$expiry_date=date_create(date("Y-m-d"));//$day=""; $rent_frequency="Monthly";
		$day=0;		
		$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
		if($q->num_rows()>0)
		{
			$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
			foreach($q->result() as $r){ $rent_frequency=$r->rent_frequency;}
			$s=$this->Tenant_model->get_data($table="tenant_current_account", array('tenant_id'=>$tenant_id));
			foreach($s->result() as $i){ $expiry_date=$i->expected_pay_date;}		
			if($rent_frequency=="Monthly")
			{ 
				$date=date_create($expiry_date); 
		        date_add($date,date_interval_create_from_date_string(30 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			 else if($rent_frequency=="Daily"){
				  $date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(1 ."days"));
		       $day=date_format($date,"Y-m-d"); 
			} 
			else if($rent_frequency=="Quarterly"){
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(90 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Yearly"){
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(1 ."years"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Weekly"){
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(7 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
		}
		 return  $day;
		
	}
	
	public function expected_pay_day($tenant_id)
	{
		$day=""; $days_of_year=date("z", mktime(0,0,0,12,31,date('Y'))) + 1;
		$month = date("m");	 $year = date("Y"); $days_of_month=date("t",mktime(0,0,0,$month,1,$year)); 
		$expiry_date=date_create(date("Y-m-d"));
		//$day=""; $rent_frequency="Monthly"; 
		$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
		if($q->num_rows()>0)
		{
			$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
			foreach($q->result() as $r){ $date_added=$r->date_added; $rent_frequency=$r->rent_frequency; $expiry_date=$r->expected_pay_day;}
			$now=date("d");
			if($rent_frequency=="Monthly")
			{
				
				$date_added=date_create($date_added);
		        date_add($date_added,date_interval_create_from_date_string($days_of_month-$now+$expiry_date  ."days"));
		        $day=date_format($date_added,"Y-m-d");  
			}
			else if($rent_frequency=="Daily"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(1 ."days"));
				$day=date_format($date,"Y-m-d"); 
			} 
			else if($rent_frequency=="Quarterly"){
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(90-$now+$expiry_date ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Yearly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string($days_of_year-$now+$expiry_date."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Weekly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(7 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
		}
		 return  $day; 
		
}
	
public function getWaterBill($tenant_id="", $month="", $year="")
	{ 
		  if($month==""){ $month=date("m");}  if($year==""){ $year=date("Y"); }
		  $cost=0; $current_reading=0; $previous_reading=0; $water_unit_cost=0;
		  $query=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$tenant_id,'month'=>$month,'year'=>$year));
		  $q=$this->Tenant_model->get_data($table="tenant_property_units", array('id'=>$tenant_id));
		  
		  if($query->num_rows()>0)
		  {
			foreach($query->result() as $row)
			{
				$tenant_id=$row->tenant_id; $current_reading=$row->current_reading; $previous_reading=$row->previous_reading;
			foreach($q->result() as $w) { $water_unit_cost=$w->water_unit_cost; }
			}
		  }
		  $cost=($current_reading-$previous_reading)*$water_unit_cost;
		  
		  return  $cost;  
	}

public function getPrintHeader()
{
		$logo="";$msg=""; $tenant_company_name=""; $description=""; $company_name=""; $address=""; $country="";$company_email=""; $town="";$postal_code=""; $mobile_number=""; $company_location=""; 
		$company_details=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$user_info=$this->Property_model->get_data($table="user_info" ,array('company_code'=>$this->session->userdata('company_code'),'user_added_by'=>0)); 
		
		foreach($company_details->result() as $c)
		 {
			 if($c->company_code==$this->session->userdata('company_code'))
			 { 
				$company_name=$c->company_name; $company_location=$c->location;
				$logo=$c->logo; $description=$c->description; $company_email=$c->company_email; $postal_code=$c->postal_code;
				$address=$c->address; $mobile_number=$c->mobile_number; $town=$c->town; $country=$c->country;
			 } 
		} 
	if($logo==""){ $logo="ari_company_logo.png"; }  
	 
	$msg=$msg.'<table border="0" class="table table-striped table-hover" width="100%" cellpadding="8" cellspacing="2">
	<tr>
		<td>
			<img id="my_file" src="'.base_url().'media/'.$logo.'" height="75" width="150" style="display:block;max-width:230px;max-height:95px;width: auto;height: auto;" alt="'.$company_name.'">
		</td>';
		 
		 if($company_name !=""){  
		$msg=$msg.'<td>
			<h2><strong>'.$company_name.' </strong></h2>
			<h5>'.$description.'</h5>   
			<h5>&nbsp;</h5>   
			<h5>&nbsp;</h5>   
			<h5>&nbsp;</h5>   
			
		</td>
		<td>
			<h5>'.$company_location.'</h5>  
			<h5>'.$address.'-'.$postal_code.',</h5> 
			<h5>'.ucfirst(strtolower($town)).", ".ucfirst(strtolower($country)).'</h5>
			<h5><strong> Tel:</strong>	'.$mobile_number.'</h5> 
			<h5><strong>Email:</strong>'.$company_email.'</h5>
		</td>';
		  } 
	$msg=$msg.'</tr>
	</table>
	 <td  align="right">';
		 if($company_name ==""){echo $company_name;
			$landlord_name=""; $address="";
			foreach($user_info->result() as $u)
			{ 
			$email=$u->email; $landlord_name=ucfirst(strtolower($u->first_name)) ." ". ucfirst(strtolower($u->middle_name)) ." ".ucfirst(strtolower($u->last_name));
			$address=$u->address; $phone=$u->mobile_number; 
			} 
			 
		$msg=$msg.'<h2>'.$landlord_name.'</h2>
			<h5> '.$email.' </h5>
			<h5> '.$phone.'</h5>
			<h5>'.strtoupper($address).' </h5> 
			<h5> &nbsp;</h5> ';
		 }  
	$msg=$msg.'</td>';
	 
	echo json_encode(array("result"=>"ok","msg"=>$msg));
}

public function receive_pay()
	{
		$bal=0; $curr_bal=0; $edit_amount=0;  $payment_type_bal=0; $rent_bal=0; $deposit_bal=0; $item_deposit_value=0;
		$temp_val=0; $item_bal=0; $credit=0; $debit=0; $tenant_transaction_id=0;
		$tenant_id=$this->input->post('tenant_id');
		$amount=$this->input->post('amount'); 
		$pay_from=$this->input->post('from');
		$bank_acc=$this->input->post('bank');
		$unit_id=$this->input->post('category_id');
		$edit_flag=$this->input->post('edit_flag'); 
		$description=""; if($this->input->post('description')){ $description=$this->input->post('description'); }
		if($edit_flag==1){$amount=$this->input->post('edit_amount');}
		//get rent and deposit balances
		$bl=$this->Rent_model->get_data($table="tenant_current_account" , array('tenant_id'=>$pay_from));
		if($bl->num_rows()>0){  foreach($bl->result() as $b){ $rent_bal=$b->rent_balance; $deposit_bal=$b->deposit_balance; } }	
		$curr_bal=$this->get_balance($tenant_id);
		//
 		$mode=$this->input->post('receipt_no');
		$receipt_no='A'.date("m").date("d").rand(10,1210).date("y")."L";
 		if(!$mode){ $mode="";}
		$data=array(
		'tenant_id'=>$this->input->post('from'), 
		'payment_mode'=>$this->input->post('pay_mode'), 
		'payment_mode_code'=>$mode, 
		'date_paid'=>date('m/d/Y'),
		'description'=>'Receipt',		
		'amount'=>$amount,   
		'receipt_no'=>$receipt_no,   
		'type'=>'c',
        'balance'=>($curr_bal-($amount)),		
        'receipt'=>$this->do_upload($tenant_id),
        'delete_reason'=>$description,
		'bank_account_id'=>$bank_acc		
		); 
		$this->Rent_model->insert_data($table="tenant_transaction", $data);
		$c=$this->Rent_model->get_data($table="tenant_transaction" , array('receipt_no'=>$receipt_no,'tenant_id'=>$pay_from,'amount'=>$amount,'payment_mode_code'=>$mode,'date_paid'=>date('m/d/Y')), $order="id" ,$limit="1", $asc_desc="DESC");
		foreach($c->result() as $rows){  $tenant_transaction_id=$rows->id;}
        
		//update invoice
		$this->update_invoice($pay_from,$amount);
		//end
		
		 $encrypted_string = $this->encrypt->encode($tenant_transaction_id);		
		 $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
			foreach($this->input->post('checkedItems') as $key=>$r)
					{  
						echo $initial_item_value=$this->input->post('itemList_'.$r);
					}
			if($this->input->post('checkedItems'))
				{
					$depositPayment=0; 
					foreach($this->input->post('checkedItems') as $key=>$r)
					{  
						$initial_item_value=$this->input->post('itemList_'.$r);
						$item_value=$this->input->post('item_'.$r);
						$is_deposit=$this->input->post('is_deposit_'.$r);
						$pricing_id=$this->input->post('pricing_id_'.$r); 
						$payment_type=$this->input->post('pay_item_'.$r); 
						$description=$this->input->post('description_'.$r); 
						
			$value=0;  $payment_type_bal=0; $r_bal=0; $id_bal="";
			if($edit_flag==0){ $item_value=$initial_item_value; $value=$initial_item_value;}
			 
			$c=$this->Rent_model->get_data($table="tenant_payment", array('tenant_id'=>$pay_from,'status'=>0,'balance >'=>0,'payment_type'=>$payment_type,'is_deposit'=>$is_deposit));
			if($c->num_rows()>0){	foreach($c->result() as $f){ $id_bal=$f->id; $payment_type_with_bal=$f->payment_type; $payment_type_bal=$f->balance;  } }
			//
			$d=$this->Rent_model->get_data($table="tenant_payment", array('id'=>$id_bal,'tenant_id'=>$pay_from,'status'=>0,'balance >'=>0,'is_deposit'=>$is_deposit));
			// 
		
			//if item amount was changed  
			if($edit_flag==1)
			{
				$payment_type_with_bal="";  
				$k=$this->Rent_model->get_data($table="tenant_payment", array('tenant_id'=>$pay_from,'status'=>0,'balance >'=>0,'payment_type'=>$payment_type,'is_deposit'=>$is_deposit));
				if($k->num_rows() >0)
				{ 
					foreach($k->result() as $n){ $id_bal=$n->id; $payment_type_with_bal=$n->payment_type; $payment_type_bal=$n->balance;  }
				}else{ $payment_type_bal=$initial_item_value;}				
				$value=$item_value; 
				if($payment_type_bal>=$value){ $r_bal=$payment_type_bal-$value;} else{  $r_bal=$value-$payment_type_bal; }
				$check_if_paid=$this->Rent_model->get_data($table="tenant_payment", array('tenant_id'=>$pay_from,'transaction_id'=>$tenant_transaction_id,'payment_type'=>$payment_type,'is_deposit'=>$is_deposit,'status'=>0));
				if($check_if_paid->num_rows() <1){ 				
				 $this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$pay_from,'transaction_id'=>$tenant_transaction_id,'description'=>$description,'payment_type'=>$payment_type,'paid_amount'=>$value,'balance'=>$r_bal,'is_deposit'=>$is_deposit,'date_paid'=>date("Y-m-d")));
				if($is_deposit==1){  $depositPayment=$depositPayment+$value; }
				 
				//
				if($d->num_rows()>0)
					{
					  $this->Rent_model->update_data($table="tenant_payment", array('status'=>1,'description'=>$description,'payment_type'=>$payment_type_with_bal),array('id'=>$id_bal,'tenant_id'=>$pay_from,'balance >'=>0));
					}
				//
				 
				}	
			}else{  
			
			if($payment_type_bal==0){ 	$payment_type_bal=$item_value;	 } 
			 
			if($amount>=$value && $value>0)
			 {   
		  
				if($payment_type_bal>$value){ $r_bal=$payment_type_bal-$value;} else{  $r_bal=$value-$payment_type_bal; }
				$check_if_paid=$this->Rent_model->get_data($table="tenant_payment", array('tenant_id'=>$pay_from,'transaction_id'=>$tenant_transaction_id,'payment_type'=>$payment_type,'is_deposit'=>$is_deposit));
				
				if($check_if_paid->num_rows() <1){	
				   
					 	$this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$pay_from,'transaction_id'=>$tenant_transaction_id,'description'=>$description,'payment_type'=>$payment_type,'paid_amount'=>$value,'balance'=>0,'status'=>1,'is_deposit'=>$is_deposit,'date_paid'=>date("Y-m-d"))); 
					if($d->num_rows()>0)
					{
					  $this->Rent_model->update_data($table="tenant_payment", array('status'=>1,'description'=>$description,'payment_type'=>$payment_type_with_bal),array('id'=>$id_bal,'tenant_id'=>$pay_from,'balance >'=>0));
					}
					$amount=$amount-$value; 
					if($is_deposit==1){ $depositPayment=$depositPayment+$value; }
				}else{ continue;}
				$r_bal=0;
			 }
			 else if($value>$amount && $amount>0)
			 { 
				$value=$amount; 
				if($payment_type_bal>$value){ $r_bal=$payment_type_bal-$value;} else{  $r_bal=$value-$payment_type_bal; }
				$check_if_paid=$this->Rent_model->get_data($table="tenant_payment", array('tenant_id'=>$pay_from,'transaction_id'=>$tenant_transaction_id,'payment_type'=>$payment_type,'is_deposit'=>$is_deposit,'status'=>0));
				if($check_if_paid->num_rows() <1){
 				  $this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$pay_from,'transaction_id'=>$tenant_transaction_id,'description'=>$description,'payment_type'=>$payment_type,'paid_amount'=>$value,'balance'=>$r_bal,'is_deposit'=>$is_deposit,'date_paid'=>date("Y-m-d")));
				$amount=0; if($is_deposit==1){  $depositPayment=$depositPayment+$value; }
				//
				if($d->num_rows()>0)
					{
						$this->Rent_model->update_data($table="tenant_payment", array('status'=>1,'description'=>$description,'payment_type'=>$payment_type_with_bal),array('id'=>$id_bal,'tenant_id'=>$pay_from,'balance >'=>0));
					}
				//
				}else{ continue;}
				$r_bal=0;
			 } 
			 else if($amount==0)
			 {
				 if($is_deposit==1){$deposit_bal=$deposit_bal+$value; }
				  $this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$pay_from,'transaction_id'=>$tenant_transaction_id,'payment_type'=>$payment_type,'description'=>$description,'paid_amount'=>0,'balance'=>$value,'status'=>0,'is_deposit'=>$is_deposit,'date_paid'=>date("Y-m-d")));
				// 
			 	if($d->num_rows()>0)
			 	{
					 $this->Rent_model->update_data($table="tenant_payment", array('status'=>1,'description'=>$description,'payment_type'=>$payment_type_with_bal),array('id'=>$id_bal,'tenant_id'=>$pay_from,'balance >'=>0));
				}
				//break;
			 }
			  
		}
	}    
	$rent_bal=$this->get_balance($pay_from);
	$this->Rent_model->update_data($table="tenant_current_account",array('rent_balance'=>$rent_bal,'deposit_balance'=>($deposit_bal-$depositPayment),'total_amount'=>(($deposit_bal-$depositPayment)+$rent_bal)), array('tenant_id'=>$pay_from));
	$this->session->set_flashdata('temp','Payment details saved successfully');
	redirect(base_url().'rent/printReceipt/'.$enct);   
 }
		else
		{
			$this->session->set_flashdata('temp','Payment was not saved successfully. Try again later');
			 redirect(base_url().'rent/printReceipt/'.$enct); 
		}   
  
}

public function do_upload($id=""){  
		$mediaFile="";
		$config['upload_path'] = 'media/'; 
		$config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx';
		$config['max_size']	= '5000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
        $this->upload->initialize($config);
		$targetDir='./media/';   //directory name
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);
	for($i=0; $i<$cpt; $i++)
	{
		$_FILES['userfile']['name']= $files['userfile']['name'][$i];
		$_FILES['userfile']['type']= $files['userfile']['type'][$i];
		$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
		$_FILES['userfile']['error']= $files['userfile']['error'][$i]; 
		$_FILES['userfile']['size']= $files['userfile']['size'][$i];
		//$this->upload->initialize($this->set_upload_options());
		$this->upload->do_upload();
		//get file name
		$fileName = $_FILES['userfile']['name'];
		$images[] = $fileName;
		$targetFile = $targetDir.$fileName."_".$id."".date('mdHis'); 
		if(move_uploaded_file($_FILES['userfile']['tmp_name'],$targetFile))
		{
			$mediaFile=$fileName."_".$id."".date('mdHis'); 
		} 
			
		} 
			$fileName = implode(',',$images); 
	 
	return $mediaFile;
}

public function new_paymentItem()
{
	$name=$this->input->post('name');
	$description=$this->input->post('description');
	$amount=$this->input->post('amount'); 
	$category_id=$this->input->post('category_id');
	$priority=$this->input->post('priority');
	$tenant_id=$this->input->post('tenant_id');
	$q=$this->Payment_model->get_data(array('audit_number'=>1,'unit_id'=>$category_id,'payment_type'=>$name,'tenant_id'=>$tenant_id), $table="tenant_pricing");
	if($q->num_rows()>0){ echo json_encode(array('result'=>"false",'msg'=>'Item already exist')); return; }	 	
	if($this->Payment_model->insert_data($data=array('unit_id'=>$category_id,'payment_type'=>$name,'tenant_id'=>$tenant_id,'payment_type_value'=>$amount,'is_deposit'=>0,'priority'=>$priority,'is_one_time'=>1), $table="tenant_pricing")>0)
	{
		$bl=$this->Rent_model->get_data($table="tenant_current_account" , array('tenant_id'=>$tenant_id));
		if($bl->num_rows()>0){  foreach($bl->result() as $b){ $rent_bal=$b->rent_balance+$amount; $deposit_bal=$b->deposit_balance; } }	
		$this->Rent_model->update_data($table="tenant_current_account",array('rent_balance'=>$rent_bal,'deposit_balance'=>($deposit_bal),'total_amount'=>(($deposit_bal+$rent_bal))), array('tenant_id'=>$tenant_id));
	
		echo json_encode(array('result'=>"ok"));
	}
	else
	{
		echo json_encode(array('result'=>"false",'msg'=>'Item Not added. Try again later'));
	} 
} 
	
	public function removePaymentItem($table="tenant_pricing")
	{
		$id=$this->input->post('id');
		$tenant_id=$this->input->post('tenant_id');
		$amount=$this->input->post('amount');
		if($this->session->userdata('company_code')=="" || $id==""){ echo json_encode(array('result'=>"false"));  return;}
		if($this->Rent_model->update_data($table="tenant_pricing", array('audit_number'=>2),array('id'=>$id)))
		{
            $bl=$this->Rent_model->get_data($table="tenant_current_account" , array('tenant_id'=>$tenant_id));
			if($bl->num_rows()>0){  foreach($bl->result() as $b){ $rent_bal=$b->rent_balance-$amount; $deposit_bal=$b->deposit_balance; } }	
		    $this->Rent_model->update_data($table="tenant_current_account",array('rent_balance'=>$rent_bal,'total_amount'=>(($deposit_bal+$rent_bal))), array('tenant_id'=>$tenant_id));
			echo json_encode(array('result'=>"ok"));
		}
		else
		{
			echo json_encode(array('result'=>"false"));
		}
	}
	
	public function removeReceipt($id="",$tenant_id="")
	{
		$q=$this->Rent_model->get_data($table="tenant_transaction", array('id'=>$id));
		if($q->num_rows()>0){ 
		$r = $q->row(); 
		$tenant_id=$r->tenant_id;$date_paid=$r->date_paid; $pay_mode=$r->payment_mode; 
		$pay_mode_code=$r->payment_mode_code; $amount=$r->amount; $receipt_no=$r->receipt_no;
		$bank_acc_id=$r->bank_account_id; $receipt=$r->receipt; $balance=$r->balance;
		}
		$description="";
		if($this->input->post('description') !=""){ $description=$this->input->post('description'); }
		$company_code=$this->session->userdata('company_code');
		if($company_code !=""){
		$before=$this->get_balance($tenant_id); $rbal=0; $dbal=0; $rent_bal=0;  $deposit_bal=0;
		$this->Rent_model->update_data($table="tenant_transaction", array('audit_number'=>2,'status'=>1,'delete_reason'=>$description,'is_deleted'=>1,'delete_date'=>date('d-m-Y')),array('id'=>$id));
		
		if($this->Rent_model->insert_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'date_paid'=>$date_paid,'amount'=>$amount,'receipt_no'=>$receipt_no,'payment_mode'=>$pay_mode,'payment_mode_code'=>$pay_mode_code,'delete_reason'=>'Reversed receipt','balance'=>$balance,'type'=>'d','receipt'=>$receipt,'description'=>'Reversed receipt','bank_account_id'=>$bank_acc_id))>0)
		{ 
			$this->Rent_model->update_data($table="tenant_payment", array('audit_number'=>2),array('transaction_id'=>$id,'tenant_id'=>$tenant_id));
			$s=$this->Rent_model->get_data($table="tenant_payment", array('audit_number'=>2,'transaction_id'=>$id,'tenant_id'=>$tenant_id));
			foreach($s->result() as $rev)
			{ 
				if($rev->is_deposit==1){ $dbal=$dbal+$rev->balance; } 
				if($rev->is_deposit==0){ $rbal=$rbal+$rev->balance;} 
			}
		$bl=$this->Rent_model->get_data($table="tenant_current_account" , array('tenant_id'=>$tenant_id));
		if($bl->num_rows()>0){  foreach($bl->result() as $b){ $rent_bal=$b->rent_balance; $deposit_bal=$b->deposit_balance; } }	
		//
			$curr_bal=$this->get_balance($tenant_id);
		//
		$this->Rent_model->update_data($table="tenant_current_account", array('rent_balance'=>$rbal+$rent_bal,'deposit_balance'=>$dbal+$deposit_bal,'total_amount'=>$curr_bal),  array('tenant_id'=>$tenant_id));
				
			 echo json_encode(array('result'=>"ok",'before'=>$before,'after'=>$curr_bal));
		}
		else 
		{
			echo json_encode(array('result'=>"false"));
			
		} 
		}
		else{
			echo json_encode(array('result'=>"false"));
		}
	}

public function tenantInvoice()
{
		$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id");
		$data['current_acc']=$this->Property_model->get_data($table="tenant_current_account", array('audit_number'=>1));
		$this->load->view('accounts/header');
		$this->load->view('accounts/tenant_invoice',$data);
		$this->load->view('accounts/footer');
}


public function viewTenantInvoice($id)
{
		$hashed=new Encrypte();
		$id=$hashed->hashId($id);
		$query=$this->Tenant_model->get_data("update_receipt", array('tenant.audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant.tenant_status'=>1));
		$data['tenants']=$this->Tenant_model->get_data("update_receipt", array('tenant.audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant.tenant_status'=>1));
		foreach($query->result() as $f){ $unit_id=$f->property_unit_id;}
		if($id=="")
		{
			$data['received_rent']="";
			$data['rent_items']="";
		}
		else
		{ 
		$data['received_rent']=$this->Rent_model->get_data("tenant_transaction_view", array('audit_number'=>1,'type'=>'c','company_code'=>$this->session->userdata('company_code')));
		$data['rent_items']= $this->listItemsToPay($id);
		 
		}
		 
		$data['company_details']=$this->Rent_model->get_data($table="company_info", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')));
		//$data['payment_for']=$this->Rent_model->payment_for();
		$data['disabled']=$this->disabled; 
		$data['balance']=$this->get_balance($id); 
		$data['selected_tenant']=$this->Tenant_model->get_data('tenant_property_units',array('tenant.id'=>$id,'tenant.audit_number'=>1)); 
		$data['tenant_details']=$this->Tenant_model->get_data('tenant_property_units',array('tenant.id'=>$id,'tenant.audit_number'=>1)); 
		$this->load->view('accounts/header');
		$this->load->view('accounts/invoice',$data);
		$this->load->view('accounts/footer');
	
}
 
function listItemsToPay($tenant_id){
		 
		$balances = $this->Rent_model->get_data($table="tenant_current_account", array('audit_number'=>1,'tenant_id'=>$tenant_id));
		$paymentsNoDepo = $this->Rent_model->get_data($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id,'is_deposit'=>0,'is_one_time'=>0),$order_by="priority",$limit="",$asc_desc="ASC");
		$paymentsDepo = $this->Rent_model->get_data($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id,'is_deposit'=>1),$order_by="priority",$limit="",$asc_desc="ASC");
		$row = $balances->row();
		$it_exist=0;
		$items = array();
			if($row->deposit_balance>0){
			$prevBal = $this->Rent_model->get_data($table="tenant_payment", array('audit_number'=>1,'tenant_id'=>$tenant_id,'is_deposit'=>1,'status'=>0,'balance >'=>0));
			if($prevBal->num_rows()>0){
				foreach($prevBal->result_array() as $prow){
					array_push($items, array(
						"id"=>"","audit_number"=>1,"tenant_id"=>$prow['tenant_id'],
						"payment_type"=> $prow['payment_type'],
						"payment_type_value" => $prow['balance'],
						"description" =>' (Bal bf for '. $prow['date_paid'].')',
						"priority"=> "",
						"is_deposit"=>1,
						"is_one_time"=>""
					));
					$it_exist++;
				}
			  
			} if($it_exist <=0){
				foreach( $paymentsDepo->result_array() as $u){ array_push($items, $u); }
			}
		}
	 
		if($row->rent_balance>0){
			$exists = $this->Rent_model->get_data($table="tenant_payment", array('audit_number'=>1,'tenant_id'=>$tenant_id,'balance >'=>0,'is_deposit'=>0,'status'=>0));	
		$bal_exists=0; 
		if($exists->num_rows()>0)
			{
			
				foreach($exists->result_array() as $p)
				{
				 array_push($items, array(
					"id"=>$p['id'],"audit_number"=>1,"tenant_id"=>$p['tenant_id'],
					"payment_type"=> $p['payment_type'],
					"payment_type_value" => $p['balance'],
					"description" => ' (Bal bf for '. $p['date_paid'].')',
					"priority"=>"",
					"is_deposit"=>0,
					"is_one_time"=>""
				));
				$bal_exists++;
				}
			}
			
		 if($bal_exists<=0 && $it_exist <=0){ foreach($paymentsNoDepo->result_array() as $u){array_push($items, $u);}}
		 $paymentsNoDepo = $this->Rent_model->get_data($table="tenant_pricing", array('audit_number'=>1,'tenant_id'=>$tenant_id,'is_deposit'=>0,'is_one_time'=>1),$order_by="priority",$limit="",$asc_desc="ASC");
		
		foreach($paymentsNoDepo->result_array() as $u)
			 {  
				if($u['is_one_time']==1){
							$exists = $this->Rent_model->get_data($table="tenant_payment", array('audit_number'=>1,'tenant_id'=>$tenant_id,'payment_type'=> $u['payment_type'],'status'=>0));	
							if($exists->num_rows <=0)
							{
								array_push($items,array(
								"id"=>$u['id'],"audit_number"=>1,"tenant_id"=>$u['tenant_id'],
								"payment_type"=> $u['payment_type'],
								"payment_type_value" => $u['payment_type_value'],
								"description" => "",
								"priority"=> $u['priority'],
								"is_deposit"=>0,
								"is_one_time"=>$u['is_one_time']
								));
							}
						}
			}
		}
return $items; 
}
 
 public function update_invoice($tenant_id="", $amount="")
 {   
	if($tenant_id !="")
	{
		$q=$this->Rent_model->get_data($table="invoice", array('audit_number'=>1,'balance >'=>0,'status'=>0,'is_deleted'=>0,'tenant_id'=>$tenant_id,'company_code'=>$this->session->userdata('company_code')),$order_by="id",$limit="", $asc_desc="DESC");
		$extra=0;  $status=0;
		if($q->num_rows()>0)
		{
			foreach($q->result() as $r)
			 {  
				$id=$r->id;  $balance=$r->balance; $balance=($balance-$amount); 
				if($balance <0){ $extra=(0-$balance); $balance=0; $status=1;} 
				$q=$this->Rent_model->update_data($table="invoice",  array('balance'=>$balance, 'status'=>$status),array('audit_number'=>1,'id'=>$id,'tenant_id'=>$tenant_id,'company_code'=>$this->session->userdata('company_code')));
		        $amount=$extra; 
			 }
		}
	}else { } 
}
 
 public function check_days_to_expire()
 {
	 $obj=new send_mails();
	 $obj->checkExpiryDate(); 
 }
 
}			
?>