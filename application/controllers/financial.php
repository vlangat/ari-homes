<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financial extends CI_Controller {
	public function __construct()
	{
		parent::__construct();   
		$this->load->model("Rent_model");
		$this->load->model("Tenant_model"); 		
		$this->load->model("Payment_model"); 		
		$this->load->model("Property_model");
		$this->load->library('Expired');
		$this->load->helper('security');
		$this->load->library('rent_class');  
		$this->load->library('pdf');
        date_default_timezone_set('Africa/Nairobi'); 
		$obj=new Expired();  
		$this->disabled=$obj->check_subscription_expiry(); 		
	}

	public function property_report()
	{
		$total_units=0; $total_tenants=0;
		$id=$this->input->post('property');
		$month=$this->input->post('month');
		if($month==""){ $month=date('m');}
		$data['company_details']=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['property']=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		if($id==""){
			$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						$id=$row->id;
					}
				}
				$data['prop_report']=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
			
			}
		else{ 
		$data['prop_report']=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
		}
		$q=$this->Property_model->get_data($table="unit_property_details" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$r=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants_payment']=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['landlord']=$this->Property_model->get_data($table="landlord" ,array('company_code'=>$this->session->userdata('company_code'))); 
		foreach($q->result() as $row){ $total_units=$total_units+$row->total_units;   }
		foreach($r->result() as $rows){ $total_tenants=$total_tenants+1;   }
		$data['total_units']=$total_units; $data['total_tenants']=$total_tenants; $data['month']=$month;
		
		$this->load->view('accounts/header');
		$this->load->view('financial/property_report',$data);  
		$this->load->view('accounts/footer');
	}
		
	 
	public function landlord_report()
	{
		$total_units=0; $total_tenants=0;
		$id=$this->input->post('landlord');
		$month=$this->input->post('month');
		if($month==""){ $month=date('m');}
		$data['company_details']=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		 if($id==""){
			$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						$id=$row->landlord;
					}
				}
				$data['landlord_report']=$this->Property_model->get_data($table="property" ,array('landlord'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
			
			}
		else{ 
		$data['landlord_report']=$this->Property_model->get_data($table="property" ,array('landlord'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		}
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'landlord'=>$id)); 
		$data['tenants_payment']=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'landlord'=>$id)); 
		$data['landlords']=$this->Property_model->get_data($table="landlord" ,array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'))); 
		$data['landlord_details']=$this->Property_model->get_data($table="landlord" ,array('audit_number'=>1,'id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		$data['month']=$month; $data['selected_landlord']=$id; 
		$this->load->view('accounts/header');
		$this->load->view('financial/landlord_report',$data);  
		$this->load->view('accounts/footer');
	}
	
	public function property_expense()
	{
		$total_units=0; $total_tenants=0;
		$id=$this->input->post('property');
		$month=$this->input->post('month');
		if($month==""){ $month=date('m');}
		$data['company_details']=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['property']=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		if($id==""){
			$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						$id=$row->id;
					}
				}
				$data['prop_report']=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
			
			}
		else{ 
		$data['prop_report']=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
		 }
		$q=$this->Property_model->get_data($table="unit_property_details" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$r=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants_payment']=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'))); 
		$data['landlord']=$this->Property_model->get_data($table="landlord" ,array('company_code'=>$this->session->userdata('company_code'))); 
		foreach($q->result() as $row){ $total_units=$total_units+$row->total_units;   }
		foreach($r->result() as $rows){ $total_tenants=$total_tenants+1;   }
		$data['total_units']=$total_units; $data['total_tenants']=$total_tenants; $data['month']=$month;
		 
		$data['suppliers']=$this->Property_model->get_data($table="suppliers" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['expenses']=$this->Property_model->get_data($table="expenses" ,array('property_id'=>$id,'expense_type'=>1)); 
		$this->load->view('accounts/header');
		$this->load->view('financial/property_expense',$data);  
		$this->load->view('accounts/footer');
	}
	
	public function profit_loss_account()
	{
		$total=0; $total1=0;
		$obj2=new rent_class();
        $data= array(); 
        $year=$this->input->post('year'); 
		if($year==""){ $year=date('Y');}
		 echo json_encode(array(
		  'jan'=>$jan1=$obj2->management_fees($year,$month="01"), 
		  'feb'=>$feb1=$obj2->management_fees($year,$month="02"),
		  'mar'=>$mar1=$obj2->management_fees($year,$month="03"),
		  'apr'=>$apr1=$obj2->management_fees($year,$month="04"),
		  'may'=>$may1=$obj2->management_fees($year,$month="05"),
		  'jun'=>$jun1=$obj2->management_fees($year,$month="06"),
		  'jul'=>$jul1=$obj2->management_fees($year,$month="07"),
		  'aug'=>$aug1=$obj2->management_fees($year,$month="08"),
		  'sep'=>$sep1=$obj2->management_fees($year,$month="09"),
		  'oct'=>$oct1=$obj2->management_fees($year,$month="10"),
		  'nov'=>$nov1=$obj2->management_fees($year,$month="11"),
		  'dec'=>$dec1=$obj2->management_fees($year,$month="12"),
		    
		  'jan2'=>$jan2=$obj2->business_expense($year,$month="01"), 
		  'feb2'=>$feb2=$obj2->business_expense($year,$month="02"),
		  'mar2'=>$mar2=$obj2->business_expense($year,$month="03"),
		  'apr2'=>$apr2=$obj2->business_expense($year,$month="04"),
		  'may2'=>$may2=$obj2->business_expense($year,$month="05"),
		  'jun2'=>$jun2=$obj2->business_expense($year,$month="06"),
		  'jul2'=>$jul2=$obj2->business_expense($year,$month="07"),
		  'aug2'=>$aug2=$obj2->business_expense($year,$month="08"),
		  'sep2'=>$sep2=$obj2->business_expense($year,$month="09"),
		  'oct2'=>$oct2=$obj2->business_expense($year,$month="10"),
		  'nov2'=>$nov2=$obj2->business_expense($year,$month="11"),
		  'dec2'=>$dec2=$obj2->business_expense($year,$month="12"),
		  'total'=>$total=(($apr1-$apr2)+($may1-$may2)+($jan1-$jan2)+($feb1-$feb2)+($mar1-$mar2)+($jun1-$jun2)+($jul1-$jul2)+($aug1-$aug2)+($sep1-$sep2)+($oct1-$oct2)+($nov1-$nov2)+($dec1-$dec2))
		   
		  ));
		 
	}
	
	public function profit_loss()
	{
		$year=$this->input->post('year');
		$month=$this->input->post('month');
		if($year==""){ $year=date('Y');}
		if($month==""){ $month=date('m');}
		$user_info=$this->Property_model->get_data($table="user_info" ,array('company_code'=>$this->session->userdata('company_code'),'id'=>$this->session->userdata('id'))); 
		$cd=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['company_details']=$cd; $data['user_info']=$user_info;
		$logo=""; $company_name=""; $location=""; $name=""; $email=""; $company_location="";
        $u=$user_info->row();  $data['company_location']=$u->town; $data['name']=$u->first_name .' '. $u->middle_name .' '. $u->last_name;  
		$c=$cd->row();  $data['company_name']=$c->company_name; $data['company_location']=$c->town; $data['logo']=$c->logo; 
		if($company_name==""){ $company_name=$name;}
		if($company_location==""){ $company_location=$location; }
		$data['year']=$year; $data['month']=$month; 
		$this->load->view('accounts/header');
		$this->load->view('financial/profit_loss',$data);  
		$this->load->view('accounts/footer');
	}
	
	public function business_expense()
	{
		$year=$this->input->post('year');
		$month=$this->input->post('month');
		if($year==""){ $year=date('Y');  }
		if($month==""){ $month=date('m');}
		$data['user_info']=$this->Property_model->get_data($table="user_info",array('company_code'=>$this->session->userdata('company_code'))); 
		$data['company_details']=$this->Property_model->get_data($table="company_info",array('company_code'=>$this->session->userdata('company_code'))); 
		$data['property']=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		 
		$data['year']=$year; $data['month']=$month; 
		$data['suppliers']=$this->Property_model->get_data($table="suppliers" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['expenses']=$this->Property_model->get_data($table="expenses" ,array('expense_type'=>2,'company_code'=>$this->session->userdata('company_code'))); 
		
		$this->load->view('accounts/header');
		$this->load->view('financial/business_expense',$data);  
		$this->load->view('accounts/footer');
	}
		
	 
	public function edit_deposit()
	{
		if($this->Rent_model->update_data($table="refunded_deposits", $data=array('amount'=>$this->input->post('amount')), $condition=array('audit_number'=>1,'no'=>$this->input->post('id'))))
		 {
		 
			echo json_encode(array('result'=>'ok','status'=>1));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','status'=>0));
		}
		
	}
	
	public function remove_deposit()
	{
		if($this->Rent_model->update_data($table="refunded_deposits", $data=array('audit_number'=>2), $condition=array('audit_number'=>1,'no'=>$this->input->post('id'))))
		{
			echo json_encode(array('result'=>'ok','status'=>1));
		}
		else
		{
			echo json_encode(array('result'=>'fail','status'=>0));
		}
		
	}
	
	public function remove_expenses()
	{
		if($this->Rent_model->update_data($table="expenses", $data=array('audit_number'=>2), $condition=array('audit_number'=>1,'id'=>$this->input->post('id'))))
		$q=$this->Rent_model->getData($table="expenses", $data=array('audit_number'=>2,'id'=>$this->input->post('id')));
		if($q->num_rows()>0)
		 {
		   echo json_encode(array('result'=>'ok','status'=>1));
		 }
		else
		{
		  echo json_encode(array('result'=>'fail','status'=>0));
		} 
	}
	
public function property_reportCSV($id="", $month)
 {
		$setData=''; 
		$package_bal=$this->Payment_model->get_data($condition="","user_info"); 
		$balance_brought=0;$balance=0; $paid=0; $expected_amount=0; $e=0; $pd=0; $bf=0; $property_rent_vat=0;
		$total_units=0;$total_tenants=0; $management_fee=0; $landlord_name=""; $acc_no=""; $bank=""; $email="";$bank_branch="";
		//$id=$this->input->post('id');  $month=$this->input->post('month');
		if($id !=""){
			$q=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		 }
	 else{
			$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
		 }
	$landlord=$this->Property_model->get_data($table="landlord" ,array('company_code'=>$this->session->userdata('company_code'))); 
	 if($q->num_rows()>0)
	{ 
		foreach($q->result() as $row)
		{   $property_name=$row->property_name; 
			$id=$row->id;  $lr_no=$row->lr_no; $location=$row->location; $vat_management_fee=$row->vat_management_fee;$management_fee=$row->management_fee; $property_rent_vat=$row->property_rent_vat; 
			if($management_fee==""){ $management_fee=0;}
			foreach($landlord->result() as $l)
			{ 
				if($row->landlord==$l->id)
				{
					$landlord_name=$l->first_name.' '. $l->middle_name.' '. $l->last_name;
					$acc_no=$l->bank_acc; $bank=$l->bank_name; $email=$l->email;$bank_branch=$l->bank_branch;
				}  
			}
		}
	} 

	$q=$this->Property_model->get_data($table="unit_property_details" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
	$r=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
	$data['tenants_payment']=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
	$data['landlord']=$this->Property_model->get_data($table="landlord" ,array('company_code'=>$this->session->userdata('company_code'))); 
	foreach($q->result() as $row){ $total_units=$total_units+$row->total_units; }
	foreach($r->result() as $rows){ $total_tenants=$total_tenants+1;   }
	
	$columnHeader ='';
	$columnHeader .= "PROPERTY REPORT FOR THE MONTH OF  ".strtoupper(date('F', mktime(0, 0, 0, $month, 1)))."\t"."\t \t"."YEAR: ".date('Y')."\t\n";
	$columnHeader .="   " . "\t"."\t"."\t\n";
	$columnHeader .= "PROPERTY NAME:  ".strtoupper($property_name) ."\t"."\t "."\t "."LR/NO: ".$lr_no ."\t"."LOCATION: ".strtoupper($location)."\t\t\n";
	$columnHeader .= "LANDLORD: ".strtoupper($landlord_name) ."\t"."\t"." \t"."ACCOUNT NO: ".$acc_no."\t"."BANK: ".strtoupper($bank)."\t\t\n";
	$vacant_units=$total_units-$total_tenants;
	$columnHeader .= "TOTAL UNITS: ".$total_units ."\t"." \t"."OCCUPIED UNITS: ".$total_tenants."\t"."\t "."VACANT UNITS: ".$vacant_units."\t\n";
	$columnHeader .="   " . "\t"."\t"."\t\n";
	$columnHeader .= "UNIT NO"."\t"."TENANT"."\t"."BALANCE B/F"."\t"."EXPECTED RENT"."\t"."RECEIVED RENT"."\t"."BALANCE C/F"."\t";
		 
	$tenants=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
	
	$obj=new rent_class();
   
	$rowData = ''; $rent_collect=0;
	foreach($tenants->result() as $t)
	{  
		$tenant_id=$t->id; 
		$e=$obj->expected_pay($tenant_id, $month);
		$rent_collect=$rent_collect+$obj->get_receipt_rent($tenant_id, $month);
		if($t->tenant_deleted==2 && $e==0){
			 continue;
		}
		$bf=$obj->balance_forwarded($tenant_id,$month); $balance_brought=$balance_brought+$obj->balance_forwarded($tenant_id,$month);
		$expected_amount=$expected_amount+$e; $paid=$paid+$obj->paid_rent($tenant_id, $month);
		$pd=$obj->paid_rent($tenant_id, $month); $b=$bf+$e-$pd; $balance=$balance+$b; 
		$value = '"' .$t->house_no . '"' . "\t".strtoupper($t->first_name)." ".strtoupper($t->middle_name)." ".strtoupper($t->last_name) ."\t".$bf."\t".$e."\t".$pd."\t".$b."\n";
		
		$rowData .= $value;  
	}

	//$m=$management_fee/100*$paid;
	$m=$management_fee/100*$rent_collect;
	$ex=$obj->property_expense($id, $month);
	$reim=$obj->reimbursement($id, $month);
	$rowData =$rowData ."   " . "\t"."\t"."\t\n";
	$rowData =$rowData ."SUB TOTAL RENT COLLECTED  " . "\t"."\t".$balance_brought."\t".$expected_amount."\t".$paid."\t".$balance."\t\n";
	$rowData =$rowData ."   " . "\t"."\t"."\t\n";
	if($property_rent_vat>0){
		$rowData =$rowData ."VAT (".$property_rent_vat."%) " . "\t". "\t"."\t". "\t"."KES ".($property_rent_vat/100)*$paid ."\t\t\n";
	   }
	$rowData =$rowData ."   " . "\t"."\t"."\t\n";
	$rowData =$rowData ." LESS DEDUCTIONS " . "\t"."\t\n";
	$rowData =$rowData ."   " . "\t"."\t"."\t\n";
	$rowData =$rowData ."MANAGEMENT FEE (".$management_fee."%)" ."\t".$m."\n";
	$m_vat=$vat_management_fee/100*$m; 
	if($vat_management_fee >0){
	$rowData =$rowData ."VAT (". $vat_management_fee."%) ON MANAGEMENT FEE"."\t".$m_vat."\n";
	}
	$rowData =$rowData ."PROPERTY EXPENSE" ."\t".$ex."\n";

	$rowData =$rowData ."REIMBURSEMENT DEPOSITS " ."\t".$reim."\n";
	$rowData =$rowData ."TOTAL DEDUCTIONS " ."\t".$deducts=$m+$m_vat+$ex+$reim."\n";
	$payable=$paid-$deducts;
	$rowData =$rowData ."AMOUNT PAYABLE = [SUB TOTAL RENT COLLECTED] - [TOTAL DEDUCTIONS] " ."\t".$payable."\n"; 
	$setData .= trim($rowData)."\n";

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=PropertyReport".date('d')."-".$month."-".date('Y').".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo ucwords($columnHeader)."\n".$setData."\n";
	 
 }
 

 public function landlord_reportCSV($id="", $month)
 {
	$total_units=0; $total_tenants=0; $i=0; $total_vat=0;
    $property_name=""; $landlord_name="-"; $bank="-"; $bank_branch="-"; $acc_no="-"; $landlord_name="-"; $management_fee=0;
	$landlord_details=$this->Property_model->get_data($table="landlord" ,array('audit_number'=>1,'id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
	$tenants=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'landlord'=>$id)); 
	$tenants_payment=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'landlord'=>$id)); 
	$landlords=$this->Property_model->get_data($table="landlord" ,array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'))); 
	if($id=="")
	{
		$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
		if($q->num_rows()>0) {  foreach($q->result() as $row) { $id=$row->landlord; } }
		$landlord_report=$this->Property_model->get_data($table="property" ,array('landlord'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
			
	}else{ 
		$landlord_report=$this->Property_model->get_data($table="property" ,array('landlord'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		}
	 foreach($landlord_details->result() as $l)
	 {  
		$landlord_name=$l->first_name.' '. $l->middle_name.' '. $l->last_name;
		$acc_no=$l->bank_acc; $bank=$l->bank_name; $email=$l->email;$bank_branch=$l->bank_branch;
		$i++;
	 } 
		 
	$columnHeader =''; $rowData=''; $setData='';
	$columnHeader .= "LANDLORD REPORT FOR THE MONTH OF  ".strtoupper(date('F', mktime(0, 0, 0, $month, 1)))." "."YEAR ".date('Y')."\t\t\t\n";
	$columnHeader .="   " . "\t"."\t"."\t\n";
	$columnHeader .= "LANDLORD NAME: ".strtoupper($landlord_name)."\t\n";
	$columnHeader .= "ACCOUNT NO: ".$acc_no."\t"."\t "."BANK NAME: ".strtoupper($bank)."\t "."BRANCH: ".strtoupper($bank_branch)."\t\n";
	$columnHeader .= "MONTH: ".strtoupper(date('F', mktime(0, 0, 0, $month, 1)))."\t"."\t "."YEAR: ".date('Y')."\t "."DATE: ".date('d/m/Y')."\t\n";
	$columnHeader .="   " . "\t"."\t"."\t\n";
	$columnHeader .= "PROPERTY NAME"."\t"."AMOUNT RECEIVED"."\t"."VAT"."\t"."DEDUCTIONS"."\t"."AMOUNT PAYABLE"."\t";
	$obj=new rent_class();
	$total_received=0;   $total_deductions=0; $total_payable=0; $amtpayable=0; $ap=0; $vat_management_fee=0; $management_fee=0; $d=0; $deducts=0; $reim=0; $ex=0; $m=0;

	foreach($landlord_report->result() as $lr){ 
	$received=0; $deductions=0; $vat=0; $receivedRent=0;
	foreach($tenants->result() as $t){
	if($lr->id==$t->property_id)
	{  
			foreach($tenants_payment->result() as $tp)
			{
				if(($tp->type=="c") && ($t->id==$tp->tenant_id) && ($month==date("m",strtotime($tp->date_paid))))
				{ 
					$received=$received+$tp->amount; 
					$total_received=$total_received+$tp->amount;
					$receivedRent=$receivedRent+$obj->get_receipt_rent($tp->tenant_id, $month);						
											
				}  			
			}
	}

	} 
  if($i>0){
  $management_fee=$lr->management_fee; 
  $property_rent_vat=$lr->property_rent_vat; $vat_management_fee=$lr->vat_management_fee;
// $m=$management_fee/100*$received; 
$m=$management_fee/100*$receivedRent;
 $m_vat=$vat_management_fee/100*$m;
 $ex=$obj->property_expense($lr->id, $month); 
 $reim=$obj->reimbursement($lr->id, $month); 
  }
  if($received==0){  }else{ $vat=($property_rent_vat/100)*$received; }
  $total_vat=$total_vat+$vat;
  
  $deducts=$m+$ex+$reim+$m_vat;
 $ap=$received-$deducts; 
 $value = '"' .strtoupper($lr->property_name) . '"' . "\t".$received."\t".$vat."\t".$deducts."\t".$ap."\n";
 $rowData .= $value;
 
$d=$d+$deducts;
$amtpayable=$amtpayable+$ap;
$ap=0;
}
$rowData =$rowData ."   " . "\t"."\t"."\t\n";
$rowData =$rowData ."TOTAL " . "\t".$total_received."\t".$total_vat."\t".$d."\t".$amtpayable."\t\n";
$rowData =$rowData ."   " . "\t"."\t"."\t\n"; 		
   
$setData .= trim($rowData)."\n";

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=LandlordReport".date('d')."-".$month."-".date('Y').".xls");
header("Pragma: no-cache");
header("Expires: 0");

echo ucwords($columnHeader)."\n".$setData."\n";
 
 }
 
 
 public function property_expenseCSV($id="", $month="")
 {
	    $total_units=0; $total_tenants=0; $landlord="";
		$company_details=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$property=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		if($id=="")
		{
			$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
			if($q->num_rows()>0) { foreach($q->result() as $row) { $id=$row->id; } }
			$prop_report=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
		}
		else
		{ 
			$prop_report=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
		}
		 
		$q=$this->Property_model->get_data($table="unit_property_details" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$r=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants_payment']=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$landlord=$this->Property_model->get_data($table="landlord" ,array('company_code'=>$this->session->userdata('company_code'))); 
		foreach($q->result() as $row){ $total_units=$total_units+$row->total_units;   }
		foreach($r->result() as $rows){ $total_tenants=$total_tenants+1;   }
		$data['total_units']=$total_units; $data['total_tenants']=$total_tenants; $data['month']=$month;
		 
		$suppliers=$this->Property_model->get_data($table="suppliers" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$expenses=$this->Property_model->get_data($table="expenses" ,array('property_id'=>$id,'expense_type'=>1)); 
		$property_name=""; $landlord_name="-"; $bank="-"; $location=""; $lr_no=""; $bank_branch="-"; $acc_no="-"; $landlord_name="-"; $management_fee=0;
	foreach($prop_report->result() as $p)
	{
		$property_name=$p->property_name; $location=$p->location;$lr_no=$p->lr_no; $property_id=$p->id; $management_fee=$p->management_fee; $email="";
		if($management_fee==""){ $management_fee=0;}
		foreach($landlord->result() as $l){ 
		if($p->landlord==$l->id){
			$landlord_name=$l->first_name.' '. $l->middle_name.' '. $l->last_name;
			$acc_no=$l->bank_acc; $bank=$l->bank_name; $email=$l->email;$bank_branch=$l->bank_branch;
			} 
		}
	}	
	 	$vacant_units=$total_units-$total_tenants;
    $columnHeader =''; $rowData=''; $setData='';
	$columnHeader .= "PROPERTY EXPENSE REPORT FOR THE MONTH OF  ".strtoupper(date('F', mktime(0, 0, 0, $month, 1)))."\t"."\t \t"."YEAR: ".date('Y')."\t\n";
	$columnHeader .="   " . "\t"."\t"."\t\n";
	$columnHeader .= "PROPERTY NAME: ".strtoupper($property_name)."\t\n";
	$columnHeader .= "LR/NO: ".$lr_no."\t"."LOCATION: ".strtoupper($location)."\t\t\n";
	$columnHeader .= "TOTAL UNITS: ".$total_units."\t "."OCCUPIED UNITS: ".$total_tenants."\t\t "."VACANT UNITS: ".$vacant_units."\t\n";
	$columnHeader .= "MONTH: ".strtoupper(date('F', mktime(0, 0, 0, $month, 1)))."\t"."\t "."YEAR: ".date('Y')."\t "."DATE: ".date('d/m/Y')."\t\t\n";
	$columnHeader .="   " . "\t"."\t"."\t\n";
	$columnHeader .= "DATE"."\t"."SUPPLIER"."\t"."INVOICE NO"."\t"."PAYMENT MODE"."\t"."MODE NO"."\t"."DESCRIPTION"."\t"."AMOUNT"."\t";
	$total=0;
	foreach($expenses->result() as $e)
	{
		$supplier_name=""; 
	foreach($suppliers->result() as $s)
	{ 
	  if($e->supplier_id==$s->id){ $supplier_name=$s->company_name;}
	}
	$invoice=$e->supplier_invoice_no;
	if($invoice==""){ $invoice=$e->invoice_no; }
	 
	if(($month==date("m",strtotime($e->date_paid))))
		{

 $value = '"' .$e->date_paid . '"' . "\t".$supplier_name."\t".$invoice."\t".$e->payment_mode ."\t".$e->payment_method_code ."\t".$e->description ."\t".$e->amount ."\n";
 $rowData .= $value; 
 $total=$total+$e->amount;
		}
	}

if($total==0){
	$rowData .= " \t\t \t\t No records found" . "\t\t\t\n";
}	
$rowData .= "TOTAL  \t \t \t \t \t\t".$total."\n";
 
 
$setData .= trim($rowData)."\n";

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=PropertyExpenseReport".date('d')."-".$month."-".date('Y').".xls");
header("Pragma: no-cache");
header("Expires: 0");

echo ucwords($columnHeader)."\n".$setData."\n";
  
 }
 
 
 public function business_expenseCSV($year="", $month="")
 { 
		if($year==""){ $year=date('Y');}
		if($month==""){ $month=date('m');}
		$user_info=$this->Property_model->get_data($table="user_info",array('company_code'=>$this->session->userdata('company_code'))); 
		$company_details=$this->Property_model->get_data($table="company_info",array('company_code'=>$this->session->userdata('company_code'))); 
		$property=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		  
		$suppliers=$this->Property_model->get_data($table="suppliers" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$expenses=$this->Property_model->get_data($table="expenses" ,array('expense_type'=>2,'company_code'=>$this->session->userdata('company_code'))); 
		
		$logo=""; $company_name=""; $location=""; $name=""; $email="";$company_location="";
		foreach($user_info->result() as $u){ if($u->id==$this->session->userdata('id')){ $location=$u->town; $name=$u->first_name.' '. $u->middle_name.' '. $u->last_name; }} 
		foreach($company_details->result() as $c){ if($c->company_code==$this->session->userdata('company_code')){ $company_name=$c->company_name; $company_location=$c->town; $logo=$c->logo;} }
		if($company_name==""){ $company_name=$name; }
		if($company_location==""){ $company_location=$location; }
		$email=$this->session->userdata('email');
		$first_name=$this->session->userdata('first_name');
 
    $columnHeader =''; $rowData=''; $setData='';
	$columnHeader .= "BUSINESS EXPENSE REPORT FOR THE MONTH OF  ".strtoupper(date('F', mktime(0, 0, 0, $month, 1)))." YEAR ".date('Y')."\t \t \t \t\n";
	$columnHeader .="   " . "\t"."\t"."\t\n";
	$columnHeader .= "".strtoupper($company_name)."\t\n";
	$columnHeader .= "LOCATION: ".strtoupper($company_location)."\t\t\n";
	$columnHeader .= "MONTH: ".strtoupper(date('F', mktime(0, 0, 0, $month, 1)))."\t"."\t "."YEAR: ".$year."\t\t "."DATE: ".date('d/m/Y')."\t\t\n";
	$columnHeader .=" " . "\t"."\t"."\t\n";
	$columnHeader .= "DATE"."\t"."SUPPLIER"."\t"."INVOICE NO"."\t"."PAYMENT MODE"."\t"."MODE NO"."\t"."DESCRIPTION"."\t"."AMOUNT"."\t";
	$total=0;
	foreach($expenses->result() as $e)
	{
		$supplier_name=""; 
	foreach($suppliers->result() as $s)
	{ 
	  if($e->supplier_id==$s->id){ $supplier_name=$s->company_name;}
	}
	$invoice=$e->supplier_invoice_no;
	if($invoice==""){ $invoice=$e->invoice_no; }
	 
	if(($month==date("m",strtotime($e->date_paid))))
		{

		 $value = '"' .$e->date_paid . '"' . "\t".$supplier_name."\t".$invoice."\t".$e->payment_mode ."\t".$e->payment_method_code ."\t".$e->description ."\t".$e->amount ."\n";
		 $rowData .= $value; 
		 $total=$total+$e->amount;
		}
	}

if($total==0){
	$rowData .= " \t\t \t\t No records found" . "\t\t\t\n";
}	
$rowData .= "TOTAL  \t \t \t \t \t\t".$total."\n";
 
 
$setData .= trim($rowData)."\n";

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=BusinessExpenseReport".$month."/".$year.".xls");
header("Pragma: no-cache");
header("Expires: 0");

echo ucwords($columnHeader)."\n".$setData."\n";
  
 }
 
 
  public function profit_lossCSV($year="")
	{ 
		if($year==""){ $year=date('Y');} $total=0;
		$user_info=$this->Property_model->get_data($table="user_info" ,array('id'=>$this->session->userdata('id'),'company_code'=>$this->session->userdata('company_code'))); 
		$company_details=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		//$property=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		 
		$suppliers=$this->Property_model->get_data($table="suppliers" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$expenses=$this->Property_model->get_data($table="expenses" ,array('expense_type'=>2,'company_code'=>$this->session->userdata('company_code'))); 
			
		$logo=""; $company_name=""; $location=""; $name=""; $email=""; $company_location="";
		$u=$user_info->row(); if($u->id==$this->session->userdata('id')){ $location=$u->town; $name=$u->first_name .' '. $u->middle_name .' '. $u->last_name; } 
		 $c=$company_details->row(); if($c->company_code==$this->session->userdata('company_code')){ $company_name=$c->company_name; $company_location=$c->town; $logo=$c->logo;
		}
		if($company_name==""){ $company_name=$name; }
		if($company_location==""){ $company_location=$location; }
		$email=$this->session->userdata('email');
		$first_name=$this->session->userdata('first_name');
 
		$obj=new rent_class(); 
		$columnHeader =''; $rowData=''; $setData='';
		$columnHeader .= "AGENT INCOME STATEMENT YEAR ".date('Y')."\t \t \t \t\n";
		$columnHeader .="   " . "\t"."\t"."\t\n";
		$columnHeader .= "".strtoupper($company_name)."\t\n";
		$columnHeader .= "LOCATION: ".strtoupper($company_location)."\t\t\n";
		$columnHeader .= "YEAR: ".$year."\t\t "."DATE: ".date('d/m/Y')."\t\t\n";
		$columnHeader .=" " . "\t"."\t"."\t\n";
		$columnHeader .= "ITEM"."\t"."JAN"."\t"."FEB"."\t"."MARCH"."\t"."APRIL"."\t"."MAY"."\t"."JUNE"."\t"."JULY"."\t"."AUG"."\t"."SEP"."\t"."OCT"."\t"."NOV"."\t"."DEC"."\t"."\t";
		$value = "INCOME \t \t \t \t \t \t\n"; 
		$value .= " MANAGEMENT FEES\t" .$jan1=$obj->management_fees($year,$month="01") . "" . "\t".$feb1=$obj->management_fees($year,$month="02")."\t".$mar1=$obj->management_fees($year,$month="03")."\t".$apr1=$obj->management_fees($year,$month="04")."\t".$may1=$obj->management_fees($year,$month="05")."\t".$jun1=$obj->management_fees($year,$month="06")."\t".$jul1=$obj->management_fees($year,$month="07")."\t".$aug1=$obj->management_fees($year,$month="08") ."\t".$sep1=$obj->management_fees($year,$month="09")."\t".$oct1=$obj->management_fees($year,$month="10")."\t".$nov1=$obj->management_fees($year,$month="11")."\t".$dec1=$obj->management_fees($year,$month="12")."\t\n";
		$value .= "\t \t \t \t \t \t\n";
		$value .= "EXPENSES \t \t \t \t \t \t\n";
		$value .= "  BUSINESS FEES\t" .$jan2=$obj->business_expense($year,$month="01") . "" . "\t".$feb2=$obj->business_expense($year,$month="02")."\t".$mar2=$obj->business_expense($year,$month="03")."\t".$apr2=$obj->business_expense($year,$month="04")."\t".$may2=$obj->business_expense($year,$month="05")."\t".$jun2=$obj->business_expense($year,$month="06")."\t".$jul2=$obj->business_expense($year,$month="07")."\t".$aug2=$obj->business_expense($year,$month="08") ."\t".$sep2=$obj->business_expense($year,$month="09")."\t".$oct2=$obj->business_expense($year,$month="10")."\t".$nov2=$obj->management_fees($year,$month="11")."\t".$dec2=$obj->business_expense($year,$month="12")."\t\n";
		$value .= "  GROSS INCOME\t" .$jan=$jan1-$jan2. "" . "\t".$feb=$feb1-$feb2."\t".$mar=$mar1-$mar2."\t".$apr=$apr1-$apr2."\t".$may=$may1-$may2."\t".$jun=$jun1-$jun2."\t".$jul=$jul1-$jul2."\t".$aug=$aug1-$aug2."\t".$sep=$sep1-$sep2."\t".$oct=$oct1-$oct2."\t".$nov=$nov1-$nov2."\t".$dec=$dec1-$dec2."\t\n";
		$value .= "\t \t \t \t \t \t\n";
		$value .= "\t \t \t \t \t \t\n";
		$total=$total+$jan+$feb+$mar+$apr+$may+$jun+$jul+$aug+$sep+$oct+$nov+$dec;
		$rowData .= $value;	
	
	if($total==0) 
	{
		$rowData .= " \t\t \t\t No records found" . "\t\t\t\n";
	}	
	$rowData .= " TOTAL \t".$total."\n"; 
    $setData .= trim($rowData)."\n";

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=IncomeStatement".$year.".xls");
	header("Pragma: no-cache");
	header("Expires: 0");  
	echo ucwords($columnHeader)."\n".$setData."\n";
	}
 
}
?>