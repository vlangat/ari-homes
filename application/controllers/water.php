<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Water extends CI_Controller {
	public function __construct()
	{
		parent::__construct();   
		$this->load->model("Rent_model");
		$this->load->model("Tenant_model"); 		
		$this->load->model("Payment_model"); 		
		$this->load->model("Property_model");
		$this->load->library('Expired');
		$this->load->library('Encrypte');
		$this->load->helper('security');
		$this->load->library('rent_class');  
		$this->load->library('pdf');
        date_default_timezone_set('Africa/Nairobi'); 
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry(); 		
	}
 
 
	public function index($msg="",$id="")
	{ 
		
		$postId=$this->input->post('property_id'); 
		if($id=="") {	$id=$postId; }
		$water_unit_cost="";
		if($id=="")
		{
			
			$q=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		    if($q->num_rows()>0)
		    { $x=0;
			   foreach($q->result() as $r)
			   { 
					if($x==1){break;} $id=$r->id; $water_unit_cost=$r->water_unit_cost; $x++;
			   } 
			} 
		} 
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['water']=$this->Property_model->get_data($table="water_management",array('audit_number'=>1,'month'=>date('m'),'year'=>date('Y')));
		$data['property']=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if($id !="")
		{
			$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id,'audit_number'=>1));
		    $c=$this->Property_model->get_data($table="property",array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
			foreach($c->result() as $r)
			   { 
					 $id=$r->id; $water_unit_cost=$r->water_unit_cost;  
			   } 
		} 
		$data['disabled']=$this->disabled;
		$data['msg']=$msg;
		$data['selected_property']=$id;
		$data['id']=$id;
		$data['water_unit_cost']=$water_unit_cost;
		$this->load->view('accounts/header');
		$this->load->view('accounts/waterManagement',$data);
		$this->load->view('accounts/footer'); 
	}
	 
	public function receiveWaterPayment()
	{
		$data['tenants']=$this->Tenant_model->showTenants(array('tenant.audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant.tenant_status'=>1));
		//$data['rent']=$this->Rent_model->paidRent($condition=array('company_code'=>$this->session->userdata('company_code'),'month'=>date("m")));
		$data['received_rent']=$this->Rent_model->received_pay(array('tenant_transaction.audit_number'=>1,'tenant_transaction.type'=>'c','tenant_property_units.company_code'=>$this->session->userdata('company_code')));
		$data['payment_for']=$this->Rent_model->payment_for();
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/receiveWaterpay',$data);
		$this->load->view('accounts/footer');
	}
 public function waterReceipts($id="",$msg="")
	{
		    $water_unit_cost="";
			$hashed=new Encrypte();
            if($id==""){  $id=$this->input->post('property_id');}
			 
			if($id=="")
			{
				$q=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
			}
			else if($this->input->post('property_id')==""){
					$id=$hashed->hashId($id, $table="property",  $condition=array('company_code'=>$this->session->userdata('company_code')),$searchItem="id");		
					$q=$this->Property_model->get_data($table="property",array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
				}	
			else{

					$q=$this->Property_model->get_data($table="property",array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
				}
			if($q->num_rows()>0)
		    {  
			   foreach($q->result() as $r)
			   { 
					$id=$r->id; $water_unit_cost=$r->water_unit_cost;  
			   } 
			}
			
		$data['disabled']=$this->disabled;
		$data['msg']=$msg;
		$data['selected_property']=$id;
		$data['water_unit_cost']=$water_unit_cost;
		$data['id']=$id;
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['water']=$this->Property_model->get_data($table="water_payment_transaction",array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'property_id'=>$id,'month'=>date('m'),'year'=>date('Y'),'type'=>'c'));
		$data['property']=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		
		$this->load->view('accounts/header');
		$this->load->view('accounts/waterReceipts',$data);
		$this->load->view('accounts/footer'); 
		
		
	}
	
	public function waterReceipt($id="")
	{
		$tenant_id="";  
		$hashed=new Encrypte();
        $id=$hashed->hashId($id, $table="water_statement",  $condition=array('company_code'=>$this->session->userdata('company_code')),$searchItem="id");		
		$q=$this->Rent_model->get_data($table="water_statement", array('id'=>$id));
	   $data['receipt']=$this->Rent_model->get_data($table="water_statement", array('id'=>$id));
	   foreach($q->result() as $r){ $tenant_id=$r->tenant_id; }
		$data['tenants']=$this->Rent_model->get_data($table="tenant", array('id'=>$tenant_id));
		$this->load->view('accounts/header');
		$this->load->view('accounts/waterReceipt',$data);
		$this->load->view('accounts/footer');
	}
	
	public function editWaterReceipt()
	{
		$edit_id=$this->input->post('edit_id');
		$tenant_id=$this->input->post('tenant_id');
		$pay_mode=$this->input->post('pay_mode');
		$payment_mode_code=$this->input->post('receipt_no');
		$date=$this->input->post('date');
		$amount=$this->input->post('amount');
		$selected_property=$this->input->post('selected_property');
       
	    $initial_amount=0; $initial_balance=0;  $credit=0; $debit=0;
		//check tenant Payment balance. Unpaid amount  
		$b=$this->Rent_model->getData($table="water_statement" , array('id'=>$edit_id));
		foreach($b->result() as $row){ $tenant_id=$row->tenant_id; $initial_balance=$row->balance; $initial_amount=$row->amount; }
		
		 $check_bal=$this->Rent_model->getData($table="water_statement" , array('tenant_id'=>$tenant_id));
		 if($check_bal->num_rows()>0){ foreach($check_bal->result() as $balance){ if($balance->type=="c"){$credit=$credit+$balance->amount;}else if($balance->type=="d"){ $debit=$debit+$balance->amount;} } } 
		 if($credit>=$debit){ $curr_bal=(0-$credit-$debit);}else if($credit<$debit){$curr_bal= $debit-$credit;}
 		 $edit_amount=$amount;
		 
		 if($edit_amount>=$initial_amount)
		 {
			 $edit_bal=$initial_balance-($edit_amount-$initial_amount);
		 }
		 else
		 {
			 $edit_bal=$initial_balance+($initial_amount-$edit_amount);
		 }
		 //end
		  
		 $data=array('amount'=>$amount,'balance'=>$edit_bal,'date_paid'=>$date,'payment_mode'=>$pay_mode,'payment_mode_code'=>$payment_mode_code);		
		if($this->Rent_model->update_data($table="water_statement",$data,array('id'=>$edit_id,'tenant_id'=>$tenant_id))>0)
		{$this->session->set_flashdata("temp","Changes was made successfully");
		 $this->session->set_flashdata("selected_property",$selected_property);
		// redirect(base_url().'water/waterReceipts');
			echo json_encode(array('result'=>"ok"));	
		}
		else{
			echo json_encode(array('result'=>"false"));	
		}
	}
	
	public function receiveWaterPay($id="")
	{
		$bal=0; $curr_bal=0; $edit_amount=0;  $payment_type_bal=0;  $temp_val=0; $item_bal=0; $credit=0; $debit=0; $tenant_transaction_id=0;
		$edit_id=$this->input->post('edit_id');
		$tenant_id=$this->input->post('tenant_id');
		$amount=$this->input->post('amount'); 
		$curr_bal=$this->input->post('expected_amount'); 
		$from=$this->input->post('from');
		 if($edit_id==""){  
		//check tenant Payment balance. Unpaid amount
		 $receipt_no='A'.date("m").date("d").rand(10,1210).date("y")."L";
 		$mode=$this->input->post('receipt_no');
		$curr_bal=$curr_bal-($this->input->post('amount'));
 		if(!$mode){ $mode="";}
		$data=array(
		'tenant_id'=>$from,
		'company_code'=>$this->session->userdata('company_code'),		
		'payment_mode'=>$this->input->post('pay_mode'), 
		'payment_mode_code'=>$mode, 
		'date_paid'=>date('m/d/Y'),
		'month'=>date('m'),
		'year'=>date('Y'),
		'description'=>'Receipt',		
		'amount'=>$this->input->post('amount'),   
		'receipt_no'=>$receipt_no,   
		'type'=>'c',
        'balance'=>$curr_bal		
		); 
 			$q=$this->Rent_model->getData($table="water_statement", array('tenant_id'=>$from,'receipt_no'=>$receipt_no,'month'=>date('m'),'year'=>date('Y')));
			if($q->num_rows()<1){
				$this->Rent_model->insert_data($table="water_statement", $data);
				$this->Rent_model->update_data($table="water_management",array('balance'=>$curr_bal), array('tenant_id'=>$from,'month'=>date('m'),'year'=>date('Y')));
				$this->session->set_flashdata("temp","Water payment saved successfully");
				redirect(base_url().'water/receiveWaterPayment'); 
			}
			else
			{
				$this->session->set_flashdata("temp","Water payment not saved successfully");
				redirect(base_url().'water/receiveWaterPayment');
			}
		 }	
	}
 
public function getWaterBill($tenant_id="", $month="", $year="")
	{ 
		  if($month==""){ $month=date("m");}  if($year==""){ $year=date("Y"); }
		  $cost=0; $current_reading=0; $previous_reading=0; $water_unit_cost=0;
		  $query=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$tenant_id,'month'=>$month,'year'=>$year));
		  $q=$this->Tenant_model->get_data($table="tenant_property_units", array('id'=>$tenant_id));
		  
		  if($query->num_rows()>0)
		  {
			foreach($query->result() as $row)
			{
				$tenant_id=$row->tenant_id; $current_reading=$row->current_reading; $previous_reading=$row->previous_reading;
			foreach($q->result() as $w) { $water_unit_cost=$w->water_unit_cost; }
			}
		  }
		  $cost=($current_reading-$previous_reading)*$water_unit_cost;
		  
		  return  $cost;  
	}		 
	 
	public function getWaterReading($id="", $month="",$year="")
	{
		if($month==""){ $month=date('m'); } if($year==""){ $year=date('Y'); }
		$curr_bal=0; $curr_reading=0;  $prev_reading=0;  $credit=0; $debit=0;  $prev_reading_date="";  $curr_reading_date="";
		//check tenant Water Payment balance. Unpaid amount
		$check_bal=$this->Rent_model->getData($table="water_statement" , array('tenant_id'=>$id));
		 if($check_bal->num_rows()>0){ foreach($check_bal->result() as $b){ if($b->type=="c"){$credit=$credit+$b->amount;}else if($b->type=="d"){ $debit=$debit+$b->amount;} } } 
		 if($credit>=$debit){ $curr_bal=(0-$credit-$debit);} else if($credit<$debit){$curr_bal= $debit-$credit;}
 		 //end
		 
		$d=$this->Property_model->get_data($table="water_management",array('tenant_id'=>$id,'audit_number'=>1,'month'=>$month,'year'=>$year));
		if($d->num_rows()>0)
		{
			foreach($d->result() as $c){ $curr_reading=$c->current_reading; $prev_reading=$c->previous_reading; $unit_cost=$c->unit_cost;  $prev_reading_date=$c->previous_reading_date;  $curr_reading_date=$c->current_reading_date; }
		}
		echo json_encode(array('result'=>"ok",'curr_reading'=>$curr_reading,'unit_cost'=>$unit_cost,'prev_reading'=>$prev_reading,'prev_reading_date'=>$prev_reading_date,'curr_reading_date'=>$curr_reading_date,'balance'=>$curr_bal));
	 
	}
	
	
	public function waterReceipts2($id="",$msg="")
	{
			$water_unit_cost="";
			$hashed=new Encrypte();
            $id=$hashed->hashId($id, $table="property",  $condition=array('company_code'=>$this->session->userdata('company_code')),$searchItem="id");		
		 	$q=$this->Property_model->get_data($table="property",array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		    if($q->num_rows()>0)
		    {  
			   foreach($q->result() as $r)
			   { 
					$water_unit_cost=$r->water_unit_cost;  
			   } 
			}
			/*$month=date("m",strtotime($date));			
            $year=date("Y",strtotime($date));			
		    $water_cost=$this->getWaterBill($tenant_id,$month,$year);
			$query=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$tenant_id,'month'=>$month,'year'=>$year));
		   if($query->num_rows()>0)
			  {
				foreach($query->result() as $row)
				{
					 $current_reading=$row->current_reading; $previous_reading=$row->previous_reading; 
				}
			  }
			$data['water_cost']=$water_cost; 
			$data['current_reading']=$current_reading; $data['previous_reading']=$previous_reading; 
			*/
		$data['disabled']=$this->disabled;
		$data['msg']=$msg;
		$data['selected_property']=$id;
		$data['water_unit_cost']=$water_unit_cost;
		$data['id']=$id;
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['water']=$this->Property_model->get_data($table="water_management",array('audit_number'=>1,'month'=>date('m'),'year'=>date('Y')));
		$data['property']=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if($id !="")
		{
			$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id,'audit_number'=>1));
		}		 
		$this->load->view('accounts/header');
		$this->load->view('accounts/waterReceipts',$data);
		$this->load->view('accounts/footer'); 
	}
	
	public function waterReading($msg="")
	{
		$total=$this->input->post('total_number'); 
		$water_unit_cost=$this->input->post('water_unit_cost'); 
		$month=date('m');$year=date('Y');
		 for($x=1;$x<=$total;$x++)
		{
			$id=$this->input->post('id'.$x.'');
			$curr_reading=$this->input->post('curr_reading'.$x.'');
			$prev_reading=$this->input->post('prev_reading'.$x.'');
			$q=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$id,'month'=>$month,'year'=>$year));
			$s=""; $r="";
			$amount=$water_unit_cost*($curr_reading-$prev_reading);
			$receipt_no='A'.date("m").date("d").rand(10,1210).date("y")."L";
			$data=array(
							'tenant_id'=>$id,
							'company_code'=>$this->session->userdata('company_code'),	 
							'date_paid'=>date('m/d/Y'),
							'month'=>date('m'),
							'year'=>date('Y'),
							'description'=>'invoice',		
							'amount'=>$amount,   
							'receipt_no'=>$receipt_no,   
							'type'=>'d',
							'balance'=>0		
							); 
			$c=$this->Tenant_model->get_data($table="water_statement", array('tenant_id'=>$id,'type'=>'d','month'=>$month,'year'=>$year));	
			if($c->num_rows()<1){   $this->Tenant_model->insert_data($table="water_statement", $data);} 
			if($q->num_rows()<1)
			{ 
				$s=$this->Tenant_model->insert_data($table="water_management",array('unit_cost'=>$water_unit_cost,'current_reading_date'=>date('Y-m-d'),'current_reading'=>$curr_reading,'previous_reading'=>$prev_reading,'tenant_id'=>$id,'month'=>$month,'year'=>$year,'company_code'=>$this->session->userdata('company_code')));
			  
			}
			else
			{
				 $r=$this->Tenant_model->update_data($table="water_management", $condition=array('tenant_id'=>$id), array('unit_cost'=>$water_unit_cost,'current_reading_date'=>date('Y-m-d'),'current_reading'=>$curr_reading,'previous_reading'=>$prev_reading,'company_code'=>$this->session->userdata('company_code')));
				 $this->Tenant_model->update_data($table="water_statement", $condition=array('tenant_id'=>$id,'month'=>$month,'type'=>'d','year'=>$year), array('amount'=>$amount));
			}
		}
		if($s||$r)
		{
			$msg="Data saved successfully";
		}
		 	
	 $this->index($msg,$this->input->post('id_no'));  
	}
	
	
	public function updateWaterUnitCost()
	{
		$id=$this->input->post('property_id');
		$val=$this->input->post('unit_cost');
		$q=$this->Tenant_model->update_data($table="property", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code')), array('water_unit_cost'=>$val));
		if($q >0)
		{
		echo json_encode(array('result'=>"ok",'msg'=>"Updated successfully"));	
		}
		else
		{
			echo json_encode(array('result'=>"false",'msg'=>"Not saved successfully"));
		}
		
		
	}
	
	public function add_new_reading($msg="")
	{
		$c=json_encode(array('result'=>"false",'data'=>0));
		$q=$this->Tenant_model->get_data($table="tenant_property_units", $condition=array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if($q->num_rows()>0)
		{
			foreach($q->result() as $r)
			{
				$id=$r->id;  
				$query=$this->Tenant_model->get_data($table="water_management", $condition=array('tenant_id'=>$id), $order_by="id", $asc_desc="DESC");
				foreach($query->result() as $s)
				{ 
					$curr_reading=$s->current_reading; $prev_reading=$s->previous_reading;   $month=$s->month; $year=$s->year;
					 
					if($this->Tenant_model->update_data($table="water_management", $condition=array('tenant_id'=>$id,'month'=>$month,'year'=>$year), array('current_reading'=>$curr_reading,'previous_reading'=>$prev_reading)))
					 
					$y=$this->Tenant_model->get_data($table="water_management", $condition=array('tenant_id'=>$id,'month'=>date('m'),'year'=>date('Y')), $order_by="id", $asc_desc="DESC");
					if($y->num_rows()>0)
					{
						if($this->Tenant_model->update_data($table="water_management", $condition=array('tenant_id'=>$id,'month'=>date('m'),'year'=>date('Y')), array('current_reading'=>$curr_reading,'previous_reading'=>$prev_reading)))
						{
							$c=json_encode(array('result'=>"false",'data'=>1));
						}
					}
					else
					{
						if($this->Tenant_model->insert_data($table="water_management", $data=array('tenant_id'=>$id,'previous_reading'=>$curr_reading,'current_reading'=>0,'previous_reading_date'=>date('Y-m-d'),'current_reading_date'=>date('Y-m-d'),'month'=>date('m'),'year'=>date('Y'))))
				        {
							$c=json_encode(array('result'=>"ok",'data'=>1));
						}
					}
				} 
			}       
			  
		} 
		echo $c; 
	}
	
	 public function payment_details($id)
	{
		$q=$this->Rent_model->getData($table="water_statement", array('id'=>$id));
		if($q->num_rows()>0)
		{ 
		 
			 echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else 
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
 
	
}

?>