<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller{
 
  var $user_ip=null; 
  var $ari="";
  var $username=null;
  var $apikey=null;
  var $gateway=null;
public function __construct(){
		parent::__construct();
		$this->load->library('email');
		$this->load->helper('email');
		$this->load->helper('security');
		$this->load->library('session');
		$this->load->model("Admin_model");
		$this->load->library('AfricasTalkingGateway');  
		$this->load->model('Admin_model');
		$this->load->model('Authentication_model');
		$this->load->model('Property_model');
		$this->load->model('Tenant_model');
		$this->load->model('Rent_model');
		$this->load->library('SendSms'); 
		$this->load->library('send_mails'); 
		$this->load->library('Captcha');
		$this->load->library('Encrypte');
	    date_default_timezone_set('Africa/Nairobi');
		header('Access-Control-Allow-Origin: *');
		$this->ari="ARI";
		$this->username   = "daguindd";
		//$this->apikey     = "c33e58aae898f33cc54724a57f81985065f51be0473fa8bfc1bf161e12991dbe"; 
		$this->apikey     = "bbf2a40b99f48da7d75d94bb044addb5bdcd164a531d0b085d196c2cb590bf29"; 
		$this->gateway    = new AfricasTalkingGateway($this->username, $this->apikey);
}
public function index($msg="")
{
	$cap=new Captcha();
	$data['image']=$cap->index();
	$data['msg']=$msg;
	$this->load->view('admin/index',$data);  
}
 
public function logout()
{
    
	$this->session->sess_destroy();
//	$cap=new Captcha();
//	$data['image']=$cap->index();
	//$data['msg']="";
	//$this->load->view('admin/index',$data); 
	redirect(base_url().'admin/');

}

public function partners($id="")
{
	$page="partners";
	if($id !="")
	{
		$data['partner_details']=$this->Admin_model->get_data("partners",array('id'=>$id), $order_by="id");
		$data['subscriber_info']=$this->Admin_model->getData("user_info");
		$data['subscriptions']=$this->Admin_model->get_subscribers("package_payment");
		$data['subscribers']=$this->Admin_model->getData("package_payment");
		$data['packages']=$this->Admin_model->getData("package");
		$page="subscription";
	}
	else
	{
		$data['partners']=$this->Admin_model->get_data("partners", $condition="", $order_by="id");
	}
	
	$this->load->view('admin/header'); 
	$this->load->view('admin/'.$page, $data); 
	$this->load->view('admin/footer'); 

}
	 
public function login()
{
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		//$auth_password=md5($password); 
		$captcha_code=$this->input->post('captcha_code');
		 $userCaptcha=$this->input->post('userCaptcha');
		 $fileName=$this->input->post('captcha_name');
		 
		if(strcmp(strtoupper($userCaptcha),strtoupper($captcha_code)) != 0) 
		{  
		    $this->session->set_flashdata('temp',"You have provided wrong image text!");
		    $this->session->set_flashdata('temp_userId',$email);
			
			redirect(base_url().'admin/');
			 
		}else{ 
		
	$salt='';
	$q=$this->Authentication_model->get_data($table="account", array('email'=>$email,'user_enabled'=>1));
		if($q->num_rows() >0) 
		{ 
			foreach($q->result() as $r){  $salt=$r->ref_time; } 
		}
	$auth_password = base64_encode(do_hash($password . $salt,'sha512') . $salt);
	$check_if_exist=$this->Admin_model->login(array('email'=>$email,'verify'=>$auth_password,'level'=>2));
	if ($check_if_exist->num_rows() <=0) 
	{ 
        $this->session->set_flashdata('temp_userId',$email);
		$this->session->set_flashdata('temp',"Wrong username or password"); 
		redirect(base_url().'admin/');
	}
	else
	{
		foreach($check_if_exist->result() as $row){
			$this->session->set_userdata('first_name',$row->first_name); 
			$this->session->set_userdata('last_name',$row->last_name); 
			$this->session->set_userdata('phone',$row->mobile_number);  
			$this->session->set_userdata('photo',$row->photo);  
			$this->session->set_userdata('admin_id',$row->id);  
			$this->session->set_userdata('email', $row->email); 
			$this->session->set_userdata('company_code',$row->company_code);
			$this->session->set_userdata('username', $row->email);  
			$this->Admin_model->update_data($table="enter", $condition=array('user_info_id'=>$this->session->userdata('admin_id')), $data=array('last_login'=>date('Y-m-d H:i:s')));
			$user_ip=$this->input->ip_address();
			$this->Admin_model->insert_data($table="login_ip_history", array('user_id'=>$row->id,'login_time'=>date('Y-m-d H:i:s'),'ip_address'=>$user_ip));
		}
  
		 redirect(base_url().'admin/home');
	} 
	
}
}

public function view($id)
	{
		$id=$this->hashId($id, $table="enter",  $condition=array('audit_number'=>1),$searchItem="user_info_id");		
		
		$this->load->view('admin/header');
		$data['package_type']=$this->Admin_model->getData('package',array('audit_number'=>1)); 
		$q=$this->Admin_model->getData('account',array('id'=>$id)); 
		$company_code="";
		if($q->num_rows()<1){ redirect(base_url().'admin/home');}
		foreach($q->result() as $r){ $company_code=$r->company_code; }
		$data['packages']=$this->Admin_model->getData('package_payment',array('company_code'=>$r->company_code));
		
		$data['company_code']=$company_code; 
		$data['users']=$this->Admin_model->getData('account',array('id'=>$id,'company_code'=>$company_code));
		$data['tenants']=$this->Admin_model->getData('tenant_property_units',array('company_code'=>$company_code));
		$data['properties']=$this->Admin_model->getData('property',array('company_code'=>$company_code));
		$data['company_info']=$this->Admin_model->getData('company_info', array('company_code'=>$company_code));
		$data['user_id']=$id;
		$this->load->view('admin/view',$data);
		$this->load->view('admin/footer');
		 
	} 
	
public function home()
{
	//$data['users']=$this->Admin_model->getData('account',$condition=array('audit_number >'=>0,'user_type <>'=>3), $order_by="id",$asc_desc="desc"); 
	$data['users']=$this->Admin_model->getData('account',$condition=array('audit_number >'=>0), $order_by="id",$asc_desc="desc"); 
	$data['properties']=$this->Admin_model->getData('property', array('audit_number'=>1,'property_deleted'=>1));
	$data['tenant_info']=$this->Admin_model->getData('tenant_property_units', array('audit_number'=>1,'tenant_status'=>1));
	$data['added_users']=$this->Admin_model->getData('account',array('user_enabled'=>1,'user_added_by <>'=>0));
	$data['company_info']=$this->Admin_model->getData('company_info');
	$data['tenants']=$this->Admin_model->getData('tenant',array('audit_number'=>1,'tenant_deleted'=>1));
	$this->load->view('admin/header');
	$this->load->view('admin/dashboard',$data);
	$this->load->view('admin/footer');
	
}

public function signUpTenants()
{
	$data['users']=$this->Admin_model->getData('account',$condition=array('audit_number >'=>0,'user_type'=>3), $order_by="id",$asc_desc="desc"); 
	$data['properties']=$this->Admin_model->getData('property', array('audit_number'=>1,'property_deleted'=>1));
	$data['tenant_info']=$this->Admin_model->getData('tenant_property_units', array('audit_number'=>1,'tenant_status'=>1));
	$data['company_info']=$this->Admin_model->getData('company_info');
	$data['tenants']=$this->Admin_model->getData('tenant',array('audit_number'=>1,'tenant_deleted'=>1));
	$this->load->view('admin/header');
	$this->load->view('admin/signUptenants',$data);
	$this->load->view('admin/footer'); 
}

 public function referredAgents()
  {
		$data['agents']=$this->Tenant_model->get_data($table="referred_agents");
		$data['agents_details']=$this->Tenant_model->get_data($table="company_info");
		$data['tenants']=$this->Tenant_model->get_data($table="tenant",array('audit_number'=>1));
		  
		$this->load->view('admin/header');
		$this->load->view('admin/referred_agents',$data);
		$this->load->view('admin/footer'); 
  }
public function package($transactions="")
{ 
	$data['payment_info']=$this->Admin_model->getData('pesapi_payment',array('super_type'=>1)); 
	$this->load->view('admin/header');
	$this->load->view('admin/pay_transactions',$data);
	$this->load->view('admin/footer');
	
}
	
public function paid_packages($pay_mode="mpesa")
	{ 
		$data['payment_info']=$this->Admin_model->getData('package_payment',array('payment_method'=>$pay_mode)); 
		$data['subscriber_info']=$this->Admin_model->getData('account',array('user_enabled'=>1,'user_added_by'=>0)); 
		$data['pay_mode']=$pay_mode; 
		$this->load->view('admin/header');
		$this->load->view('admin/paid_packages',$data);
		$this->load->view('admin/footer'); 	
	}
	 
	public function user_privileges($id="")
	{
		$value=0;
		$id=$this->hashId($id, $table="user_info",  $condition=array('audit_number'=>1),$searchItem="id");		
		$q=$this->Authentication_model->get_data($table="user_info", array('id'=>$id));
		if($q->num_rows()<=0)
		{
		     redirect(base_url() ."admin/users");	
		}
		$data['data']=$this->Authentication_model->get_data($table="account", array('email'=>$this->session->userdata('email'),'id'=>$this->session->userdata('id'),'company_code'=>$this->session->userdata('company_code')));
		
		$p=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>1));
		if($p->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>1,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['property']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>1));
		$t=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>2));
		if($t->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>2,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['tenants']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>2));
		
		$r=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>3));
		if($r->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>3,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['rent']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>3));
		
		$s=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>4));
		if($s->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>4,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['suppliers']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>4));
		
		$sub=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>5));
		if($sub->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>5,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['subscription']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>5));
		$u=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>6));
		
		if($u->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>6,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['users']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>6));
		//$data['disabled']=$this->disabled;
		$data['user_id']=$id;
		$data['msg']="";
		$this->load->view('admin/header');
		$this->load->view('admin/userPrivileges',$data);
		$this->load->view('admin/footer');
	}
	
	public function assign_privilege($page="userPrivileges")
	{
		$data['msg']="";
		$user_id=$this->input->post('user_id');
		$addProperty=$this->input->post('p_addPrivilege');
		$editProperty=$this->input->post('p_editPrivilege');
		$deleteProperty=$this->input->post('p_deletePrivilege');
		$viewProperty=$this->input->post('p_viewPrivilege');
		$q=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>1,'user_id'=>$user_id));		
		if($q->num_rows()>0){
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>1,'user_id'=>$user_id), array('addPrivilege'=>$addProperty,'editPrivilege'=>$editProperty,'viewPrivilege'=>$viewProperty,'deletePrivilege'=>$deleteProperty));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>1,'user_id'=>$user_id,'addPrivilege'=>$addProperty,'editPrivilege'=>$editProperty,'viewPrivilege'=>$viewProperty,'deletePrivilege'=>$deleteProperty));		
		 }
		$addTenant=$this->input->post('t_addPrivilege');
		$editTenant=$this->input->post('t_editPrivilege');
		$deleteTenant=$this->input->post('t_deletePrivilege');
		$viewTenant=$this->input->post('t_viewPrivilege');
		 $r=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>2,'user_id'=>$user_id));		
		if($r->num_rows()>0){
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>2,'user_id'=>$user_id), array('addPrivilege'=>$addTenant,'editPrivilege'=>$editTenant,'viewPrivilege'=>$viewTenant,'deletePrivilege'=>$deleteTenant));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>2,'user_id'=>$user_id,'addPrivilege'=>$addTenant,'editPrivilege'=>$editTenant,'viewPrivilege'=>$viewTenant,'deletePrivilege'=>$deleteTenant));		
		 }
		$addRent=$this->input->post('r_addPrivilege');
		$editRent=$this->input->post('r_editPrivilege');
		$deleteRent=$this->input->post('r_deletePrivilege');
		$viewRent=$this->input->post('r_viewPrivilege'); 	
	    $s=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>3,'user_id'=>$user_id));		
		if($s->num_rows()>0){
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>3,'user_id'=>$user_id),array('addPrivilege'=>$addRent,'editPrivilege'=>$editRent,'viewPrivilege'=>$viewRent,'deletePrivilege'=>$deleteRent));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>3,'user_id'=>$user_id,'addPrivilege'=>$addRent,'editPrivilege'=>$editRent,'viewPrivilege'=>$viewRent,'deletePrivilege'=>$deleteRent));		
		 }
		 
		$addSupplier=$this->input->post('s_addPrivilege');
		$editSupplier=$this->input->post('s_editPrivilege');
		$deleteSupplier=$this->input->post('s_deletePrivilege');
		$viewSupplier=$this->input->post('s_viewPrivilege');
		$s=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>4,'user_id'=>$user_id));		
		if($s->num_rows()>0)
		{
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>4,'user_id'=>$user_id),array('addPrivilege'=>$addSupplier,'editPrivilege'=>$editSupplier,'viewPrivilege'=>$viewSupplier,'deletePrivilege'=>$deleteSupplier));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>4,'user_id'=>$user_id,'addPrivilege'=>$addSupplier,'editPrivilege'=>$editSupplier,'viewPrivilege'=>$viewSupplier,'deletePrivilege'=>$deleteSupplier));		
		 }
		$addSubscription=$this->input->post('sub_addPrivilege');
		$editSubscription=$this->input->post('sub_editPrivilege');
		$deleteSubscription=$this->input->post('sub_deletePrivilege');
		$viewSubscription=$this->input->post('sub_viewPrivilege');
		 
		$t=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>5,'user_id'=>$user_id));		
		if($t->num_rows()>0){
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>5,'user_id'=>$user_id),array('addPrivilege'=>$addSubscription,'editPrivilege'=>$editSubscription,'viewPrivilege'=>$viewSubscription,'deletePrivilege'=>$deleteSubscription));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>5,'user_id'=>$user_id,'addPrivilege'=>$addSubscription,'editPrivilege'=>$editSubscription,'viewPrivilege'=>$viewSubscription,'deletePrivilege'=>$deleteSubscription));		
		 }
		 
		$addUsers=$this->input->post('users_addPrivilege');
		$editUsers=$this->input->post('users_editPrivilege');
		$deleteUsers=$this->input->post('users_deletePrivilege');
		$viewUsers=$this->input->post('users_viewPrivilege');
		
		//$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>5,'user_id'=>$user_id), array('addPrivilege'=>$addSubscription,'editPrivilege'=>$editSubscription,'viewPrivilege'=>$viewSubscription,'deletePrivilege'=>$deleteSubscription));		    
	    $u=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id));		
		if($u->num_rows()>0)
		{
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id),array('addPrivilege'=>$addSubscription,'editPrivilege'=>$editSubscription,'viewPrivilege'=>$viewSubscription,'deletePrivilege'=>$deleteSubscription));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id,'addPrivilege'=>$addSubscription,'editPrivilege'=>$editSubscription,'viewPrivilege'=>$viewSubscription,'deletePrivilege'=>$deleteSubscription));		
		 }
		
		//if($this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id), array('addPrivilege'=>$addUsers,'editPrivilege'=>$editUsers,'viewPrivilege'=>$viewUsers,'deletePrivilege'=>$deleteUsers)))
		if($this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id), array('addPrivilege'=>$addUsers,'editPrivilege'=>$editUsers,'viewPrivilege'=>$viewUsers,'deletePrivilege'=>$deleteUsers)))
		{
			$data['msg']="User privileges saved successfully";
		}
		else
		{
			$data['msg']="No changes made to user privileges";
		}
		 
		$data['property']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>1));
		$data['tenants']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>2));
		$data['rent']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>3));
		$data['suppliers']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>4));
		$data['subscription']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>5));
		$data['users']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>6));
		$data['data']=$this->Authentication_model->get(array('company_code'=>$this->session->userdata('company_code'),'id <>'=>$this->session->userdata('id')));
        //$data['disabled']=$this->disabled;
		$data['user_id']=$user_id;
		$this->load->view('admin/header');
		$this->load->view('admin/userPrivileges',$data);
		$this->load->view('admin/footer'); 
	}
	
	
	
public function ip_history()
	{
		$this->load->view('admin/header');
		$data['login_history']=$this->Admin_model->get_data('login_ip_history','','id'); 
		$data['user_info']=$this->Admin_model->getData('account'); 
		$this->load->view('admin/login_history',$data);
		$this->load->view('admin/footer'); 
	} 
	
public function logs()
	{
		$this->load->view('admin/header');
		$data['log_activity']=$this->Admin_model->get_data("logs", $condition="",$order_by="id");
		$data['user_info']=$this->Admin_model->getData('account'); 		
		$this->load->view('admin/log_activity',$data);
		$this->load->view('admin/footer');
		
	} 


public function remove_log()
	{ 
		$id=$this->input->post('id');
		$this->Admin_model->delete_data('login_ip_history',array('id'=>$id));
		if($this->db->affected_rows()>0)
		{
		 
			echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','data'=>0));
		} 
  }

  public function remove_sessions()
	{ 
		$id=$this->input->post('id'); 
		$this->Admin_model->delete_data('logs',array('id'=>$id));
		if($this->db->affected_rows()>0)
		{
		 
			echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','data'=>0));
		} 
  }

public function remove_multiple_logs()
{
	if($id=="" && !empty($this->input->post('checkbox')))
		{ 
			foreach($this->input->post('checkbox') as $value)
			{ 
				$id=$value; 
				$this->Admin_model->delete_data('logs',array('id'=>$id));
			} 
		} 
		if($id==""){ $id=$this->input->post('id'); }
		if($id==""){ $this->index(); return; }
		redirect(base_url().'admin/logs');
		
}
public function remove_multiple_activity()
{
	if($id=="" && !empty($this->input->post('checkbox')))
		{ 
			foreach($this->input->post('checkbox') as $value)
			{ 
				$id=$value; 
				$this->Admin_model->delete_data('login_ip_history',array('id'=>$id));
			} 
		} 
		if($id==""){ $id=$this->input->post('id'); }
		if($id==""){ $this->index(); return; }
		redirect(base_url().'admin/ip_history');
		
}

public function activate_subscriber()
{ 
	$company_code=$this->input->post('company_code');
	$id=$this->input->post('id');
	$user_id=$this->input->post('user_id');
	$q=$this->Admin_model->getData('package_payment',array('company_code'=>$company_code,'id'=>$id));
	if($q->num_rows()>0){ foreach($q->result() as $r){ $audit_number=$r->audit_number; } }
	if($audit_number==1){ $audit_number=2;}else{ $audit_number=1;} 
	$this->Admin_model->update_data($table="paying_for_users", $condition=array('user_info_id'=>$user_id,'company_code'=>$company_code), $data=array('audit_number'=>$audit_number));
	$this->Admin_model->update_data($table="sms_status", $condition=array('company_code'=>$company_code), $data=array('audit_number'=>$audit_number));
	$this->Admin_model->update_data($table="package_current_account", $condition=array('user_info_id'=>$user_id,'company_code'=>$company_code), $data=array('audit_number'=>$audit_number));
	$this->Admin_model->update_data($table="package_payment", $condition=array('user_info_id'=>$user_id,'company_code'=>$company_code), $data=array('audit_number'=>$audit_number));
	if($audit_number==2){ $msg="Deactivated "; }else{ $msg="Activated "; }
	if($this->db->affected_rows()>0)
	{ 
		echo  json_encode(array('result'=>"ok",'data'=>1,'msg'=>$msg));
	}
	else
	{
		echo json_encode(array('result'=>"false",'data'=>0,'msg'=>$msg));
	}  
}

public function reset_admin_account()
{  
	$this->load->model('Authentication_model');
	$company_code=$this->input->post('company_code'); 
	$user_info_id=$this->input->post('user_id');
	$phone=""; $name=""; $email="";
	$q=$this->Admin_model->getData('account',array('company_code'=>$company_code,'id'=>$user_info_id));
	if($q->num_rows()>0){ foreach($q->result() as $r){ $email=$r->email; $name=$r->first_name; $phone=$r->mobile_number; } }
			$pass=do_hash($email,'sha512');
			$code=$pass;  $ref_no=rand(10000,100000);
			$data=$this->Authentication_model->getPassword($user_info_id,$pass,$reset_ref_no=md5($ref_no));
			$pass=do_hash($email,'sha512');
			$now=time();
			$to=$email;
			$phone="+254".substr($phone,1);
			$body='<html> 
			<div style="width:90%;padding:10px;background:#ffffff;">
			<div style="padding-left:10px"> 
			 <h4 align="left"> Dear '.$name.',</h4>
			 <p style="line-height:15px">
				A password reset had been requested for your account, click on the link below to reset your password. If you did not make this request, ignore this email. This link will expire in 24 hours
			 </p>
			 <p>
			   To accept changes please  click on the button below. </p><br/>
			<center>
		<button style="background:#006699;height: 50px; width: 200px; border: 0;"> <a href="'.base_url().'admin/reset/'.$pass.'/'.$now.'" style="text-decoration:none;color:#ffffff">Reset Password </a></button></center>
			<h4 align="left"> &nbsp; Having Trouble? </h4>
			<p> If the above button does not work try copying and paste this link into your browser  </p> 
			<font color="#006699" style="background:;"> <b> '.base_url().'admin/reset/'.$pass.'/'.$now.' </b> </font>
			 '.$this->getFooter().'
			</div>
			</div>
			</body>
		</html>';
		$from="ARI Homes";
		$subject='Password Reset';
	$this->sendMail($to,$body,$subject,$from);
		$this->Authentication_model->log_activity($user=$email, $ip=$this->input->ip_address(), "Reset password",$status=1);
		$message=$ref_no." is your ARI Homes verification code"; $from="ARI Homes";
		$obj=new SendSms();
		$obj->send_sms($receipients=$phone,$message,$this->ari);
		echo  json_encode(array('result'=>"ok"));
	 
}

public function reset_account()
{  
	$this->load->model('Authentication_model');
	$company_code=$this->input->post('company_code'); 
	$user_info_id=$this->input->post('user_id');
	$phone=""; $name=""; $email="";
	$q=$this->Admin_model->getData('account',array('company_code'=>$company_code,'id'=>$user_info_id));
	if($q->num_rows()>0){ foreach($q->result() as $r){ $email=$r->email; $name=$r->first_name; $phone=$r->mobile_number; } }
			$pass=do_hash($email,'sha512');
			$code=$pass;  $ref_no=rand(10000,100000);
			$data=$this->Authentication_model->getPassword($user_info_id,$pass,$reset_ref_no=md5($ref_no));
			$pass=do_hash($email,'sha512');
			$now=time();
			$to=$email;
			$phone="+254".substr($phone,1);
			$body='<html> 
			<div style="width:90%;padding:10px;background:#ffffff;">
			<div style="padding-left:10px"> 
			 <center> <img  src="'.base_url().'images/login/ARI-Logo.png" height="60"></center>
			
			 <font size="3" align="left"> Dear '.$name.',</font>
			 <p style="line-height:15px">
				A password reset had been requested for your account, click on the link below to reset your password. If you did not make this request, ignore this email. This link will expire in 24 hours
			 </p>
			 <p>
			   To accept changes please  click on the button below. </p><br/>
			<center>
		<button style="background:#006699;height: 50px; width: 200px; border: 0;"> <a href="'.base_url().'auth/reset/'.$pass.'/'.$now.'" style="text-decoration:none;color:#ffffff">Reset Password </a></button></center>
			<h4 align="left"> &nbsp; Having Trouble? </h4>
			<p> If the above button does not work try copying and paste this link into your browser  </p> 
			<font color="#006699" style="background:beige"> <b> '.base_url().'auth/reset/'.$pass.'/'.$now.' </b> </font>
			 '.$this->getFooter().'
			</div>
			</div>
			</body>
		</html>';
		$from="ARI Homes";
		$subject='Password Reset';
		$this->sendMail($to,$body,$subject,$from);
		$this->Authentication_model->log_activity($user=$email, $ip=$this->input->ip_address(), "Reset password",$status=1);
		$message=$ref_no." is your ARI Homes verification code";
		$obj=new SendSms();
		$obj->send_sms($receipients=$phone,$message,$this->ari);
		echo  json_encode(array('result'=>"ok"));
	 
}

public function add_payment()
	{
		$edit_id=$this->input->post('edit_id');
		$data=array(
		'name'=>$this->input->post('name'),
		'phonenumber'=>$this->input->post('phone'), 
		'payment_mode'=>$this->input->post('mode'),
		'super_type'=>1, 
		'account_id'=>1,
		'amount'=>100*($this->input->post('amount')),
		'note'=>$this->input->post('description'),
		'receipt'=>$this->input->post('code'),
		'status'=>0,
		'time'=>date('Y-m-d H:i:s'));
		if($edit_id=="")
		{
			$q=$this->Admin_model->getData($table="pesapi_payment", array('receipt'=>$this->input->post('code')));
			if($q->num_rows()<1)
			{
			 $this->Admin_model->insert_data($table="pesapi_payment", $data);
				 	echo  json_encode(array('result'=>"ok",'data'=>1));
			 
			}
			else
			{
				echo json_encode(array('result'=>"false",'data'=>0));
			} 
		}
		else
		{
			 $this->Admin_model->update_data($table="pesapi_payment", array('id'=>$edit_id), $data);
			$r=$this->Admin_model->getData($table="pesapi_payment", array('id'=>$edit_id,'receipt'=>$this->input->post('code')));
			if($r->num_rows()>0) 
			 {
				echo  json_encode(array('result'=>"ok",'data'=>1));
			 }
			else
			{
				echo json_encode(array('result'=>"false",'data'=>0));
			}
		}	
		
	}
	
	public function messages($type="")
	{
		$data['count_messages']=$this->Admin_model->getData('messages',$condition=array('read_status'=>'unread','delete_status <>'=>3)); 
		$data['messages']=$this->Admin_model->getData('messages',$condition=array('sent_by'=>$this->session->userdata('email'),'delete_status <>'=>3), $order_by="id", $asc_desc="DESC"); 
		$data['receipients']=$this->Admin_model->getReceipient(array('user_enabled'=>1));
		 
		$bal_obj = $this->gateway->getUserData();
		 $bal=$bal_obj->balance;
		$data['sms_balance']=$bal; 
		$data['total_sms']=10; 
		$this->load->view('admin/header'); 
		$this->load->view('admin/bulk_sms',$data);
		$this->load->view('admin/footer'); 
		
	}
	
	public function account()
	{
		$data['data']=$this->Admin_model->getData('account',array('email'=>$this->session->userdata('email'),'id'=>$this->session->userdata('admin_id')));
		$this->load->view('admin/header');
		$this->load->view('admin/account',$data);
		$this->load->view('admin/footer');
		
	} 

public function administrators()
{ 
	$data['users']=$this->Admin_model->getData('account',$condition=array('audit_number'=>1,'level'=>2), $order_by="id",$asc_desc="desc");
	$data['company_info']=$this->Admin_model->getData('company_info');
	$data['packages']=$this->Admin_model->getData('package_payment');
	$this->load->view('admin/header');
	$this->load->view('admin/administrators',$data); 
	$this->load->view('admin/footer'); 
}

public function users($status="paid")
{ 
	$data['status']=$status;
	$data['users']=$this->Admin_model->getData('account',$condition=array('audit_number'=>1), $order_by="id",$asc_desc="desc");
	$data['company_info']=$this->Admin_model->getData('company_info');
	$data['packages']=$this->Admin_model->getData('package',$condition=array('audit_number'=>1));
	$data['paid_packages']=$this->Admin_model->getData('package_payment',$condition=array('audit_number'=>1,'paid_users <>'=>0));
	$this->load->view('admin/header');
	if($status=="paid"){
		$this->load->view('admin/paid_users',$data);
	}else if($status=="testAccount"){
		$this->load->view('admin/testAccount',$data);
	}
	 else if($status=="achieved"){
		$this->load->view('admin/achieved',$data);
	}else
	{
		$this->load->view('admin/free_users',$data);
	}
	$this->load->view('admin/footer'); 
}

function properties($status=""){
	
	if($status=="")
	{
            $data['status']="Live";
		 $data['property']=$this->Property_model->get_data($table="property", array('property_deleted'=>1,'audit_number'=>1), $order_by="id");
	}
	else
	{
		$data['property']=$this->Property_model->get_data($table="property", array('property_deleted'=>2,'audit_number'=>2),$order_by="id");
	    $data['status']="Removed";
	}
	$data['units']=$this->Property_model->get_data($table="property_units", array('audit_number'=>1));
	//$data['tenants']=$this->Tenant_model->showTenants(array('tenant.audit_number'=>1,'tenant.tenant_deleted'=>1,'tenant.added_by'=>$this->session->userdata('company_code')));
	//$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')));
	$data['rent']=$this->Rent_model->get_data($table="tenant_pricing"); 
	$data['paid_rent']=$this->Rent_model->get_paid_rent($table="tenant_transaction",  array('tenant_property_units.audit_number'=>1,'tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_transaction.type'=>'c')); 
	$data['company_info']=$this->Admin_model->getData('company_info');
	$data['user_info']=$this->Admin_model->getData('user_info',array('user_added_by'=>0));
	$data['tenants']=$this->Admin_model->getData('tenant_property_units');
	$this->load->view('admin/header');
	$this->load->view('admin/properties',$data);
	$this->load->view('admin/footer'); 
}
 public function loadUsers()
	{ 
		$q=$this->Admin_model->getData('account',array('user_added_by'=>$this->session->userdata('admin_id')));
		if($q->num_rows()>0)
		{
		 
			echo json_encode(array('result'=>'ok','data'=>$q->result_array()));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','data'=>0));
		}
		 
		
  }
	public function getMessages($type="")
	{
		$start=0; //$this->input->post('start');
		$end=12; ///$this->input->post('end');
		$delete_status=1;
		if($type=="trash"){ $delete_status=2;}
		$q=$this->Admin_model->getMessages($condition=array('message_type'=>$type,'company_code'=>$this->session->userdata('company_code'),'sent_by'=>$this->session->userdata('email'),'delete_status'=>$delete_status),$start,$end);
		if ($q->num_rows()>0) 
		{ 
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	} 	
	
	public function displayMessages($id="")
	{
		$q=$this->Admin_model->getData('messages',$condition=array('id'=>$id)); 
	    if ($q->num_rows()>0) 
		{ 
			foreach($q->result() as $row){ $msg_type=$row->message_type;}
			if($msg_type=="inbox"){
					$this->Admin_model->updateMessageInfo($id,array('read_status'=>2));
				}
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}

	public function removeMessages($id="")
	{
		 $q=$this->Admin_model->getData('messages',$condition=array('id'=>$id)); 
	    foreach($q->result() as $row) 
		{ 
			$status=$row->delete_status;
		}
		if($status==2){ $status=3;} else { $status=2;}
		if($this->Admin_model->updateMessageInfo($id,array('delete_status'=>$status,'message_type'=>'trash')))
		{
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	} 	
	
	public function deleteAdminMsg($id="")
	{
		$q=$this->Admin_model->delete_data('messages',$condition=array('id'=>$id)); 
	    
		if($this->db->affected_rows()>0)
		{
			$this->messages($type=""); 
		}
		else
		{
			$this->messages($type=""); 
		}
		
	} 	
	
	public function composeMail()
	{
		$id=$this->input->post('id');
		$to=$this->input->post('to');
		$cc=$this->input->post('cc');
		$body=$this->input->post('body');
		$subject=$this->input->post('subject');
		$from =$this->session->userdata('first_name');
        $footer='<br/> <div style="padding:10px"> 
		<font size="3" align="left"> Thank You, <br/>  
		 Team ARI Homes
		</font>
		 <div>
		<font size="2">
		 <center> &copy; '.date("Y").' ARI Homes  </center> </font>
		<center><font size="2"> 1 <sup>st </sup> Floor | All Africa Council of Churches | Sir Francis Ibiam House | Waiyaki Way, Westlands, Nairobi  <br/>
		 <a href="#">Terms </a></font> 
		 </center> 
		</div>';
		$this->load->library('email');
		$this->email->from('www.ari.co.ke',$from);
		$this->email->to($to); 
		$this->email->cc($cc);
		$this->email->subject($subject);
		$this->email->message($body.$footer);	
		$this->email->set_mailtype("html");
		$this->email->send();
		//$this->email->print_debugger();
		 
		if($id !=""){
			if($this->Admin_model->sendMail($id,$data=array('sent_by'=>$this->session->userdata('username'),'name'=>$this->session->userdata('first_name'),'to'=>$to,'message_type'=>$this->input->post('message_type'),'subject'=>$subject,'message'=>$body)))
			{
				echo  json_encode(array('result'=>"ok",'data'=>1));
			}
			else
		    {
			echo json_encode(array('result'=>"false",'data'=>0));
		   }
		}
		else{
			if($this->Admin_model->sendMail($id="",$data=array('sent_by'=>$this->session->userdata('username'),'name'=>$this->session->userdata('first_name'),'to'=>$to,'message_type'=>$this->input->post('message_type'),'subject'=>$subject,'message'=>$body)))
			{
				echo  json_encode(array('result'=>"ok",'data'=>1));
			}
			else
			{
				echo json_encode(array('result'=>"false",'data'=>0));
			}			
	 } 
		
	}
	
	
	public function draft_sms()
	{ 
		$id=$this->input->post('id');
		$to="";  
		$body=$this->input->post('body');
		$subject='';
		$from =$this->session->userdata('first_name');
		foreach($this->input->post('to') as $value) {  $to=$to.$value.",";}
		if($id !="")
		{
			 $this->Admin_model->updateMessageInfo($id,$data=array('sent_by'=>$this->session->userdata('username'),'name'=>$this->session->userdata('first_name'),'to'=>$to,'message_type'=>$this->input->post('message_type'),'message'=>$body));
		}
		else
		{
			$this->Admin_model->sendMail($id="",array('company_code'=>$this->session->userdata('company_code'),'sent_by'=>$this->session->userdata('email'),'name'=>$this->session->userdata('first_name'),'to'=>$to,'message_type'=>$this->input->post('message_type'),'message'=>$body));
		 
			echo  json_encode(array('result'=>"ok",'data'=>1));
		} 
	}
	
	public function saveDraft()
	{
		$id=$this->input->post('id');
		$to=$this->input->post('to'); 
		$body=$this->input->post('body');
		$subject=$this->input->post('subject');
		$from =$this->session->userdata('first_name');
		if($this->Admin_model->updateMessageInfo($id,$data=array('sent_by'=>$this->session->userdata('email'),'name'=>$this->session->userdata('first_name'),'to'=>$to,'message_type'=>$this->input->post('message_type'),'message'=>$body)))
		 {
				echo  json_encode(array('result'=>"ok",'data'=>1));
		 }
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}


	public function activate($id,$status="2")
	{
		
		$this->Admin_model->updateUser('enter',array('user_info_id'=>$id), $data=array('user_enabled'=>$status)); 
		$q=$this->Admin_model->get_data('enter',array('user_info_id'=>$id,'user_enabled'=>$status)); 
		if ($q->num_rows()>0) 
		{ 
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	
	public function add_property($id)
	{
		$q=$this->Property_model->get_data($table="property", array('id'=>$id));	
		if($q->num_rows()>0) { 
		$data=$this->Property_model->update_data($table="property", array('id'=>$id,'property_deleted'=>2,'company_code'=>$this->session->userdata('company_code')), array('property_deleted'=>1,'property_delete_date'=>date("Y-m-d"),'audit_number'=>1,'property_deleted_by'=>$this->session->userdata('id')));	
		//if($this->db->affected_rows()>0)
		$q=$this->Property_model->get_data('property',array('id'=>$id,'property_deleted'=>1,'audit_number'=>1,'company_code'=>$this->session->userdata('company_code'))); 
		if ($q->num_rows()>0) 
		{
			$p=$this->Property_model->get_data($table="property_units", array('property_id'=>$id));
			foreach($p->result() as $r){
				$unit_id=$r->id;
				$this->Property_model->update_data($table="tenant", array('property_unit_id'=>$unit_id,'audit_number'=>2), array('audit_number'=>1,'tenant_deleted'=>1,'delete_date'=>date("Y-m-d"),'deleted_by'=>$this->session->userdata('id'),'tenant_status'=>1));	
				$tenants=$this->Property_model->get_data($table="tenant", array('property_unit_id'=>$unit_id));
				foreach($tenants->result() as $t){ 
						$tenant_id=$t->id;
						$this->Property_model->update_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'audit_number'=>2), array('audit_number'=>1));	
						$this->Property_model->update_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'audit_number'=>2), array('audit_number'=>1));	
						$this->Property_model->update_data($table="property_units", array('property_id'=>$id,'audit_number'=>2), array('audit_number'=>1));	
						$this->Property_model->update_data($table="tenant_payment", array('tenant_id'=>$tenant_id,'audit_number'=>2), array('audit_number'=>1));	
						$this->Property_model->update_data($table="pricing_details", array('unit_id'=>$unit_id,'audit_number'=>2), array('audit_number'=>1));	
						$this->Property_model->update_data($table="current_account", array('tenant_id'=>$tenant_id,'audit_number'=>2), array('audit_number'=>1));	
					 
					}
				}
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}  
		}
	}
	
	
	
	public function addPartner()
	{
		$code="ARI".rand(1245,9937); $id=0;
		$data=array(
					'first_name'=>$this->input->post('fname'),
					'middle_name'=>$this->input->post('mname'),
					'last_name'=>$this->input->post('lname'),
					'email'=>$this->input->post('email'),
					'phone'=>$this->input->post('phone'),
					'alt_phone'=>$this->input->post('alt_phone'),
					'company'=>$this->input->post('company'),
					'kra_pin'=>$this->input->post('kra'),
					'id_passport_number'=>$id,
					'partner_code'=>$code
					);
		if($this->Admin_model->addPartner($data))
		{
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}

	public function edit_Partner()
	{
		$code= $this->input->post('partner_code');
		$edit_id= $this->input->post('edit_id');
		$data=array(
					'first_name'=>$this->input->post('fname'),
					'last_name'=>$this->input->post('lname'),
					'email'=>$this->input->post('email'),
					'phone'=>$this->input->post('phone'),
					'alt_phone'=>$this->input->post('alt_phone'),
					'company'=>$this->input->post('company'),
					'kra_pin'=>$this->input->post('kra'),
					'id_passport_number'=>$this->input->post('id') 
					);
		if($this->Admin_model->update_data("partners",array('id'=>$edit_id,'partner_code'=>$code), $data))
		{
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
 
public function activation($code)
 {
	 $q=$this->Authentication_model->activation($code);
	 if($this->db->affected_rows()>0)
	 {
		$status=$this->session->set_flashdata('temp','Account activated successfully. You can now login');	
	 }
	 else
	 {
		 $status=$this->session->set_flashdata('temp','Account activation was not successfully. Please ensure that you have clicked the link provided in your mail');
	 }
		redirect(base_url() ."manager/");
}

public function getPassword()
	{
		$code=rand(10000,200000);
		$pass=do_hash($code,'md5');
		$data=$this->Authentication_model->getPassword($email=($this->input->post('email')),$pass);
		$check=$this->Authentication_model->login(array('email'=>$email));
		if($check->num_rows()>0)
		{
			foreach($check->result() as $row){$name=$row->first_name;}
			$to=$email;
			
			$body='<html>
		
		<div style="width:90%;padding:10px;background:#ffffff;">
		<div style="padding-left:10px">
		 <center> <img  src="'.base_url().'images/login/ARI-Logo.png" height="60"></center>
		 <font size="3" align="left"> Dear '.$name.',</font>
		 <p style="line-height:15px">
		we are sending this email to notify you that your password has been reset.  
		 </p>
		 <p>
		  The above password can be changed after login. To accept changes please  click on the button below. </p><br/>
		<center>
	<button style="background:#006699;height: 50px; width: 200px; border: 0;"> <a href="'.base_url().'auth/reset/'.$pass.'"style="text-decoration:none;color:#ffffff">Reset Password </a></button></center>
		<h4 align="left"> &nbsp; Having Trouble? </h4>
		<p> If the above button does not work try copying and paste this link into your browser  </p> 
		<font color="#006699" style="background:beige"> <b> '.base_url().'auth/reset/'.$pass.' </b> </font>
		 <font size="3" align="left">Thanks, <br/>   
		Team ARI Homes  
		</font> 
	 <div>
		<font size="2">
		 <center> &copy; '.date("Y").' ARI Homes   </center> </font>
		<center><font size="2"> 1 <sup>st </sup> Floor | All Africa Council of Churches | Sir Francis Ibiam House | Waiyaki Way, Westlands, Nairobi  <br/>
		 
		 <a href="#">Terms </a>  </font>
		 </center>
		  
		</div>
		</div>
		</div>
		</body>
		</html>';
		$from="ARI Limited";
		$subject='Password Reset';
		$this->sendMail($to,$body,$subject,$from);       
		echo json_encode(array('result'=>"ok",'msg'=>'Password sent to your email. Please check your inbox or spam folder'));
		}
		else
		{
			echo json_encode(array('result'=>"false",'msg'=>'The Email you have provided does not exist or account is not active. Please create account if not yet!'));
		}
	  
 }
 
public function reset($code="",$expiry_time="")
{
	if($code==""){ redirect(base_url() ."admin/"); }
	$now=time();
	$diff=$now-$expiry_time; $days=floor($diff/(60*60*24));
	if($days>0) 
	{  
			$status=$this->session->set_flashdata('temp','The link has expired. Click on forget password link to reset your password again');
			redirect(base_url() ."admin/");
	}
	else{
		$check=$this->Authentication_model->proof_user(array('reset_password_code'=>$code));
		if($check->num_rows()>0)
		{
			foreach($check->result() as $row){$email=$row->email;}
			$this->Authentication_model->reset($code);
			$this->session->set_flashdata('reset_pass',$code);
			$this->session->set_flashdata('email',$email);
			$status=$this->session->set_flashdata('password_resetting','Password Reset successful');
			
		}
		else
		{  
			  $status=$this->session->set_flashdata('temp','Password was not reset. Please ensure that you have clicked the link provided in your mail');
		}
		redirect(base_url() ."admin/");		
	}
}

	public function profile()
	{
		$data['data']=$this->Authentication_model->get(array('email'=>$this->session->userdata('email'),'id'=>$this->session->userdata('id'),'company_id'=>$this->session->userdata('company_id')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/singleUserAccount',$data);
		$this->load->view('accounts/footer');
	}
	public function agent()
	{
		$data['data']=$this->Authentication_model->get(array('email'=>$this->session->userdata('email'),'id'=>$this->session->userdata('id'),'company_id'=>$this->session->userdata('company_id')));
		$data['company']=$this->Authentication_model->getCompanyDetails(array('email'=>$this->session->userdata('email') ,'company_id'=>$this->session->userdata('company_id')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/agentAccount', $data);
		$this->load->view('accounts/footer');
	}
	
	public function changePassword()
	{
		$captcha_code=$this->input->post('captcha_code');
		$userCaptcha=$this->input->post('userCaptcha');
		$fileName=$this->input->post('captcha_name');
		$id=$this->input->post('user_info_id');
		
		$oldpass=$this->input->post('curr_password'); 
		$newpass=$this->input->post('new_pass');
		$code=$this->input->post('code');
		$mail=$this->input->post('email');  

		$code=md5($code);
		$salt = sha1(rand());
		$salt = substr($salt, 0, 10);  
		$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		while($check_salt->num_rows() >0)
		{
			$salt = sha1(rand());
			$salt = substr($salt, 0, 10); 
			$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		}
		$newpass=base64_encode(do_hash($newpass . $salt,'sha512') . $salt);
		
if(strcmp(strtoupper($userCaptcha),strtoupper($captcha_code)) != 0) 
		{  
		    $this->session->set_flashdata('temp',"You have provided wrong image text!");
			redirect(base_url() ."admin/");
			return false;
		}
		 if($id !=""){
			 
		$this->Authentication_model->update_data("enter", array('user_info_id'=>$id),$data=array('verify'=>$newpass,'ref_time'=> $salt));
         
			 $this->session->set_flashdata('temp',"Account activated successfully. You can login!");
			redirect(base_url() ."admin/"); 
		 }else{
		
	 
		$this->Authentication_model->update_data("enter", array('reset_password_code'=>$oldpass,'reset_ref_no'=>$code),$data=array('verify'=>$newpass,'ref_time'=> $salt));
                $q=$this->Authentication_model->get_data("enter", array('reset_password_code'=>$oldpass,'reset_ref_no'=>$code));
		if($q->num_rows()>0)
		{
			$user_mail=$mail; $name="";
			$q=$this->Authentication_model->get_data("account",array('reset_password_code'=>$oldpass));
		    if($q->num_rows()>0){  foreach($q->result() as $r){   $name=$r->first_name; $user_mail=$r->email; } }
			
			$this->Authentication_model->update_data($table="enter", array('reset_password_code'=>$oldpass), array('reset_password_code'=>'','reset_ref_no'=>''));
			$this->Authentication_model->log_activity($user=$mail, $ip=$this->user_ip, $message="Changed  password",$status=1);
			 
			$body='<img src='.base_url().'images/login/ARI-Logo"><h4> Dear '.$name.',</h4> <br/><p>The password for your ARI Account '.$user_mail.' was just changed on '.date('d-m-Y H:i'). '.&nbsp;
			Do you recognize this activity? If this was not you please contact ARI Limited administrator. <br/> Contact our support team using the email: <a href="mailto:support@ari.co.ke">support@ari.co.ke </a>  in case of any questions</p>
			 '.$this->getFooter();
			
			$this->sendMail($to=$user_mail,$body,$subject="Password Reset",$from="ARI Limited");
			$msg=$this->session->set_flashdata('temp','Password changed successfully ');
			redirect(base_url() ."admin/");
		}
		else
		{
			$this->Authentication_model->log_activity($user=$mail, $ip=$this->user_ip, $message="Failed to change reset password",$status=2);
			$msg=$this->session->set_flashdata('temp','Password  not changed. Try again later');
			redirect(base_url() ."admin/");
		} 
		 }		
}
  
  public function updatePassword()
	{
		 $oldpass=$this->input->post('curr_password'); 
		$newpass=$this->input->post('new_pass');   
		$salt ="0"; 
		$q=$this->Authentication_model->get_data($table="enter", array('user_info_id'=>$this->session->userdata('admin_id')));
		if ($q->num_rows() >0) 
		{ 
			foreach($q->result() as $r){ $salt=$r->ref_time;	} 
		}
		$salt = substr($salt, 0, 10);

		$newpass=base64_encode(do_hash($newpass . $salt,'sha512') . $salt); 
		$oldpass=base64_encode(do_hash($oldpass . $salt,'sha512') . $salt); 
		$q=$this->Authentication_model->get_data($table="enter", array('user_info_id'=>$this->session->userdata('admin_id'),'verify'=>$oldpass));
	if($q->num_rows()>0)
		{
			//$this->Authentication_model->changePass(array('user_info_id'=>$this->session->userdata('id')),$newpass,$salt); 
			if($this->Authentication_model->changePass(array('user_info_id'=>$this->session->userdata('admin_id')),$newpass,$salt))
			{
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="changed password ",$status=1);
		  
				echo json_encode(array('result'=>'ok','data'=>1));
			}
			else
			{
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed update password",$status=2);
		  
			  echo json_encode(array('result'=>'fail','data'=>0));
			}
		}
	else
	{
		echo json_encode(array('result'=>'fail','data'=>0));
	}	
		
  }

  public function updateProfile()
	{
		 $data=array(
		 'first_name'=>$this->input->post('fname'), 
		 'last_name'=>$this->input->post('lname'),
		 //'company_name'=>$this->input->post('company'), 
		 'mobile_number'=>$this->input->post('phone')
			);
		if($this->Admin_model->updateUser('user_info',$condition=array('id'=>$this->session->userdata('admin_id'),'email'=>$this->session->userdata('email')),$data))
		//if($this->db->affected_rows()>0)
		{
				$this->session->set_userdata('first_name',$this->input->post('fname')); 
				$this->session->set_userdata('last_name',$this->input->post('lname')); 
				$this->session->set_userdata('phone',$this->input->post('phone'));  
				echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','data'=>0));
		}
		 	
  }

  public function updateCompanyDetails()
	{
		
	 $data=array
			( 
			 'email'=>$this->input->post('email'),
			 'country'=>$this->input->post('country'), 
			 'town'=>$this->input->post('town'), 
			 'company_name'=>$this->input->post('company_name'), 
			 'phone'=>$this->input->post('phone')
			);
		$this->Authentication_model->updateCompanyProfile($condition=array('company_id'=>$this->session->userdata('company_id'),'email'=>$this->session->userdata('email')),$data);
		//$this->changeCompanyLogo();
		if($this->changeCompanyLogo())
		{
	          $status=$this->session->set_flashdata('temp','Changes made successfully');
		  redirect(base_url() ."auth/agent#tab_1_4");
			//echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
		  
		 $status=$this->session->set_flashdata('temp','Changes not saved successfully');
		  redirect(base_url() ."auth/agent#tab_1_4");
		  //echo json_encode(array('result'=>'fail','data'=>0));
		}
	}
  
 

  public function removeUser($id="", $status="")
  {
	  $company_code=$this->session->userdata('company_code');
	  if($company_code==""){ echo json_encode(array('result'=>'fail','data'=>0)); return;}
	  
	  if($status==2){ $status=1;} else { $status=2;} 
	  $this->Authentication_model->removeUser($condition=array('user_info_id'=>$id), $data=array('user_enabled'=>$status));
	   $q= $this->Authentication_model->get_data("enter",$condition=array('user_info_id'=>$id,'user_enabled'=>$status));
	  if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>'fail','data'=>0));
		}
  }
//-----activate user added by admin
  
  public function markTestAccount($id="",$status=1)
  {
	  $company_code=$this->session->userdata('company_code');
	  if($company_code==""){ echo json_encode(array('result'=>'fail','data'=>0)); return;}
	  if($status==0){ $status=1;} else if($status==1){ $status=0;} 
	    
	  $this->Authentication_model->update_data("enter", $condition=array('user_info_id'=>$id), $data=array('testAccount'=>$status));
	   $q= $this->Authentication_model->get_data("enter",$condition=array('user_info_id'=>$id,'testAccount'=>$status));
	  if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>'fail','data'=>0));
		}
  }
//-----activate user added by admin

public function userActivation($id="",$added_by="",$account_id="")
{
	//$this->session->sess_destroy();
	$hashed=new Encrypte();
 $id=$hashed->hashId($id, $table="user_info",  $condition=array('audit_number'=>1),$searchItem="id");
 $added_by=$hashed->hashId($added_by, $table="user_info",  $condition=array('audit_number'=>1),$searchItem="id");
 $account_id=$hashed->hashId($account_id, $table="user_info",  $condition=array('audit_number'=>1),$searchItem="company_code");
	 if($id=="")
	 {  
		 $status=$this->session->set_flashdata('temp','Activation was not success. Try again later');
		  redirect(base_url() ."admin/");
	 }
	$q=$this->Authentication_model->get_data($table="enter", array('user_info_id'=>$id));
	if($q->num_rows()>0)
	{ 
		 redirect(base_url() ."admin/");
	}
	$user_type=1; $company="";
	$q=$this->Authentication_model->proof_user(array('id'=>$added_by));if($q->num_rows()>0){ foreach($q->result() as $r){$user_type=$r->user_type;}}
	$c=$this->Authentication_model->get_data($table="company_info", array('company_code'=>$account_id));
	if($c->num_rows()>0)
	{
		foreach($c->result() as $r){ $company=$r->company_name;}
	}
	$data['company']=$company;  $email="";
	//$this->Authentication_model->insert_data($tabla="enter", array('user_info_id'=>$id,'password'=>'','activation'=>1,'user_type'=>$user_type,'deactivated'=>1,'user_enabled'=>1));
	 if($this->Authentication_model->insert_data($tabla="enter", array('user_info_id'=>$id,'verify'=>md5($id),'activation'=>1,'level'=>2,'user_type'=>$user_type,'deactivated'=>1,'user_enabled'=>1)))
	{
		$q=$this->Admin_model->getData('account',array('id'=>$id));
	    if($q->num_rows()>0){ foreach($q->result() as $r){  $email=$r->email;$phone=$r->mobile_number; } }
		$ref_no=rand(10000,100000);
			//$data=$this->Authentication_model->getPassword($user_info_id=$id,$pass,$reset_ref_no=md5($ref_no));
		//$activation_code=md5($mail); 
		$salt = sha1(rand());
        $salt = substr($salt, 0, 10);  
		$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		while($check_salt->num_rows() >0)
		{
			$salt = sha1(rand());
			$salt = substr($salt, 0, 10); 
			$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		}
		
		$password =base64_encode(do_hash($ref_no . $salt,'sha512') . $salt);
	    $this->Authentication_model->update_data($table="enter", array('user_info_id'=>$id),array('ref_time'=>$salt,'verify'=>$password));
			$pass=do_hash($email,'sha512');
			$now=time();
			$to=$email;
			$phone="+254".substr($phone,1);
			$message=$ref_no." is your ARI Admin verification code, use it as your initial password to login to www.arihomes.co.ke/admin";
			$obj=new SendSms();
			$obj->send_sms($phone,$message,$this->ari);
		 $this->session->set_flashdata('id',$id);  
		 $this->session->set_flashdata('temp','Provide your password to use');	
		 redirect(base_url() ."admin/");
	}
	else
	{   
		 $this->session->set_flashdata('temp','Activation was not success. Please click the link again');	 
		 redirect(base_url() ."admin/");
	} 
}
//end of activation
 
public function acceptAccount()
{
$email=$this->input->post('email');
$password=$this->input->post('password');
$company=$this->input->post('company'); 
$fname=ucfirst(strtolower($this->input->post('first_name')));
$lname=ucfirst(strtolower($this->input->post('last_name')));
//$activation_code=do_hash($email,'sha512');
//$pass=do_hash($password,'sha512'); 
$salt = sha1(rand()); $salt = substr($salt, 0, 10);
	$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
	while($check_salt->num_rows() >0)
	{
		$salt = sha1(rand());
		$salt = substr($salt, 0, 10); 
		$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
	}

$pass=base64_encode(do_hash($password . $salt,'sha512') . $salt); 
if($this->Authentication_model->acceptAccount($email,$pass,$salt))
	{
		$msg=$this->session->set_flashdata('temp','Your account is now active, you can login');
		redirect(base_url() ."admin/");
	} 
	else
	{ 
		$msg=$this->session->set_flashdata('temp','Account was not activated successfully');
		redirect(base_url() ."admin/");
	}  

}
 
	
	public function addUser()
	{
		$email=$this->input->post('email');
		$phone=$this->input->post('phone');
		$first_name=ucfirst(strtolower($this->input->post('fname')));
		$data=array('email'=>$this->input->post('email'),'mobile_number'=>$phone,'first_name'=>$this->input->post('fname'),'last_name'=>$this->input->post('lname'),'user_added_by'=>$this->session->userdata('admin_id'),'registration_date'=>date('Y-m-d'),'company_code'=>$this->session->userdata('company_code'));
		$check_if_exist=$this->Authentication_model->proof_user(array('email'=>$email));
		if($check_if_exist->num_rows() >0) 
		{ 
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="tried to add new user email ".$email,$status=2);
			echo json_encode(array('result'=>'false','data'=>0,'msg'=>'Email provided already exists. Please use a different email address' )); 
		}
		else
		{   
		//$q=$this->Authentication_model->addUser($data);
		if($this->Authentication_model->addUser($data))
		{
			 
			$query=$this->Authentication_model->login(array('email'=>$email));
			foreach($query->result() as $row) { $id=$row->id; }
			$this->insertPrivilege($id);
			$expiry_date="0000-00-00";  $subscription_date=date('Y-m-d');
			$q=$this->Authentication_model->get_data($table="user_info", array('id'=>$this->session->userdata('admin_id')));
			if($q->num_rows()>0){ foreach($q->result() as $r) { $expiry_date=$r->acc_expiry_date;  $subscription_date=$r->subscription_date; } }
			
			$this->Authentication_model->update_data($table="user_info",$condition=array('id'=>$id), array('acc_expiry_date'=>$expiry_date,'subscription_date'=>$subscription_date));
			
			$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
			$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
			while($check_salt->num_rows() >0)
			{
				$salt = sha1(rand());
				$salt = substr($salt, 0, 10); 
				$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
			}

			$id =base64_encode(do_hash($id . $salt,'sha512') . $salt);
			//$account_id =base64_encode(do_hash($account_id . $salt,'sha512') . $salt);
			$user_added_by =base64_encode(do_hash($this->session->userdata('admin_id') . $salt,'sha512') . $salt);
			$company_code =base64_encode(do_hash($this->session->userdata('company_code') . $salt,'sha512') . $salt);
			
			$to=$email;
			$from="ARI Homes";
			$subject="Account activation";
			$body='<html>
			
			<div style="width:90%;padding:10px;background:ffffff;">
			<div style="padding-left:10px">
			 
			<p style="line-height:5px"> 
			<font align="left">  Dear '.ucfirst(strtolower($first_name)).',</font>
			 You were added to ARI Property Management system  by '.ucfirst(strtolower($this->session->userdata('first_name'))).'( Email: '.$this->session->userdata('email').') on '. date("d-m-Y").'
			 </p>
			 <p>
			 Please activate your account by clicking on the button below. </p> <br/>
			 <center><button style="background:#006699; border: 0; height: 50px; width: 200px;"> 
			 <a href="'.base_url().'admin/userActivation/'.$id.'/'.$user_added_by.'/'.$company_code.'" style="text-decoration:none;color:#ffffff"> Activate My Account</a></button>  </center>
			<h4 align="left"> &nbsp; Having Trouble? </h4>
			<p> If the above button does not work try copying and paste this link into your browser  </p> 
			<font color="#006699" style="background:;"> <b> '.base_url().'admin/userActivation/'.$id.'/'.$user_added_by.'/'.$company_code.' </b> </font><br/>
			 '.$this->getFooter().'
			</div>
			</center>
			</body>';
					
			$this->sendMail($to,$body,$subject,$from); 
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="added new user to the system. User Email: ".$email,$status=1);
			  
			echo json_encode(array('result'=>'ok','data'=>1,'user_id'=>$id,'msg'=>'User added successfully. Email notification sent to the user' )); 
		 } 
		  else
			{
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="tried to add new user",$status=2);
			  
				echo json_encode(array('result'=>'false','data'=>0,'msg'=>'User not added. Please try again' )); 
			} 
	}
}
 	
	
function tenants($status="")
{
	$data['status']="Live";
	if($status=="")
	{
		$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>1,'tenant_status'=>1), $order_by="id", $asc_desc="DESC");
	}
	else{
		$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>2,'tenant_status '=>2), $order_by="id",$asc_desc="DESC");
		$data['status']="Removed";
		}
	$data['company_info']=$this->Admin_model->getData('company_info'); 
	$data['user_info']=$this->Admin_model->getData('user_info'); 
	$this->load->view('admin/header');
	$this->load->view('admin/tenants',$data);
	$this->load->view('admin/footer');
}

public function addTenant($id)
	{
		$this->Property_model->update_data($table="tenant", array('id'=>$id,'audit_number'=>2,'tenant_deleted'=>2), array('tenant_status'=>1,'audit_number'=>1,'tenant_deleted'=>1));	
		$q=$this->Tenant_model->get_data('tenant',array('id'=>$id,'audit_number'=>1,'tenant_deleted'=>1));
		if($q->num_rows()>0)
		{
			$tenant_id=$id;
			$this->Property_model->update_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'audit_number'=>2), array('audit_number'=>1));	
			$this->Property_model->update_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'audit_number'=>2), array('audit_number'=>1)); 
			$this->Property_model->update_data($table="tenant_payment", array('tenant_id'=>$tenant_id,'audit_number'=>2), array('audit_number'=>1)); 	
			$this->Property_model->update_data($table="current_account", array('tenant_id'=>$tenant_id,'audit_number'=>2), array('audit_number'=>1));	
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
  public function changePhoto($table="")
	{ 
		$config['upload_path'] = 'media/'; 
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '5000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
        $this->upload->initialize($config);
		$targetDir='./media/';   //directory name
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);
		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i]; 
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			//$this->upload->initialize($this->set_upload_options());
			$this->upload->do_upload();
			//get file name
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
			$targetFile = $targetDir.date("Y-m-d-H:i")."_".$fileName; 
			echo $fileName=date("Y-m-d-H:i")."_".$fileName;
			if(move_uploaded_file($_FILES['userfile']['tmp_name'],$targetFile))
			{
				$condition=array('id'=>$this->session->userdata('id'));
				//$upload_data = $this->upload->data(); 
				//$file_name =   $upload_data['file_name'];
				if($table==""){$table="user_info";} 
				$this->Admin_model->uploadPhoto($fileName,$condition,$table);
				//$data = array('upload_data' => $this->upload->data());
				$this->session->set_userdata('photo', $fileName);
				$status=$this->session->set_flashdata('temp','Photo uploaded successfully');
				
			}
			else
			{
				$status=$this->session->set_flashdata('temp',' <font color="red">There was an error in saving Image </font>');	 
			}
			
		$fileName = implode(',',$images); 
		}
		redirect(base_url() ."admin/account#tab_1_2");
   
	}
////end of upload function

public function insertPrivilege($id="", $value=0)
{
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>1,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>2,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>3,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>4,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>5,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>6,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
		
	
}
 
 
  public function changeCompanyLogo()
	{
		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('photo'))
		{
			$error = array('error' => $this->upload->display_errors());
			return;//$status=$this->session->set_flashdata('img_info',' <font color="red"> There was an error in saving Image </font> ');	 
		}
		else
		{
		$condition=array('company_id'=>$this->session->userdata('company_id'));
		$upload_data = $this->upload->data(); 
		$file_name =   $upload_data['file_name'];
		$file_name.$table="company_details";
		$this->Authentication_model->uploadPhoto($file_name,$condition,$table);

		$data = array('upload_data' => $this->upload->data());
               //$this->session->set_userdata('photo', $file_name);
		$status=$this->session->set_flashdata('temp','Changes made successfully');
		}
		 redirect(base_url() ."auth/agent#tab_1_4");
	}
	 
public function sms()
{
 
 $recipients="";
 $count=0; 
 
foreach($this->input->post('to') as $value) {  $recipients=$recipients."+254".substr($value,1).","; $count++;}
  
$message    = $this->input->post('body');  
try 
{ 
  // Thats it, hit send and we'll take care of the rest. 
 $results = $this->gateway->sendMessage($recipients, $message,$from="ARI");
		 
 foreach($results as $result) { $cost=$result->cost;   } 
  /******get sms balance */
  $data = $this->gateway->getUserData();
  $balance=$data->balance;
  /**** end of balance ***/ 
  $pages=$this->input->post('pages');
  $sent_sms=$count*$pages; 
   
 $this->saveMessage($message,$recipients,$type=$this->input->post('message_type'),$sent_sms);
  
}
catch ( AfricasTalkingGatewayException $e )
{
  //echo "Encountered an error while sending: ".$e->getMessage();
  echo json_encode(array('result'=>"false",'data'=>0));
}

}

public function saveMessage($message,$to,$type,$sent_sms)
	{
			//$this->Admin_model->sendMail($id=0,$data=array('sent_by'=>$this->session->userdata('username'),'name'=>$this->session->userdata('first_name'),'to'=>$to,'message_type'=>$type,'message'=>$message));
			if($this->Admin_model->sendMail($id=0,$data=array('sent_by'=>$this->session->userdata('username'),'name'=>$this->session->userdata('first_name'),'to'=>$to,'message_type'=>$type,'message'=>$message))){
				echo  json_encode(array('result'=>"ok",'sms_cost'=>$sent_sms,'receipients'=>$to,'data'=>1));
			}
			else
		    {
				echo json_encode(array('result'=>"false",'data'=>0));
		   }
	}
	
public function hashId($enct="", $table="",  $condition="",$searchItem="")
{

			if($condition==""  || $table=="" ||  $enct=="" ||  $searchItem=="")
			{ 
				 $id=""; 
			}
			else{
			$q=$this->Authentication_model->get_data($table, $condition); 
			if($q->num_rows()>0)
			 {
			   foreach($q->result() as $r)
			   {
				$id=$r->$searchItem; 
				$s = sha1('2ab'); $s = substr($s, 0, 10); 
				$e =base64_encode(do_hash($id . $s,'sha512') . $s);
				if($e==$enct)
				{  
					$id=$r->$searchItem;   break;
				} 
				else{ $id=""; }
			   } 
			  
			 }
			} 
			
			return  $id; 
}



 public function getFooter()
 {
	 $footer='
		<font size="2" align="left"> Thanks, <br/>  
		 Team ARI  
		</font>
		<img  src="'.base_url().'images/ari_strip.png" width="100%"> <br/>
		 <div>
			<font size="2"> <center> &copy; '.date("Y").' ARI Homes   </center> </font>
			<center> <font> <a href="mailto:support@ari.co.ke">support@ari.co.ke </a> | +254 789 502 424 | +254 789 732 828 </font> </center> 
		   <center><font size="2"> 1 <sup>st </sup> Floor | All Africa Council of Churches | Sir Francis Ibiam House | Waiyaki Way, Westlands, Nairobi  <br/>
			 
			 <a href="'.base_url().'media/ARI Homes Terms and Conditions 02062017.pdf">Terms </a> </font>
			 </center>
			 
			</div>';
			return $footer;
 }
 
 
 public function sendMail($to,$body,$subject,$from)
    {
		$obj=new send_mails();
		$obj->sendMail($to,$body,$subject,$from,$fileName);
	}

}
?>