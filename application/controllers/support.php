<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Support extends CI_Controller {
 
 public function __construct()
	{
		parent::__construct();  
		$this->load->helper('security');
		$this->load->model("Authentication_model");
		$this->load->model("Property_model"); 
		$this->load->model("Admin_model");  
		$this->load->model("Tenant_model");  
		$this->load->library('upload');
		date_default_timezone_set('Africa/Nairobi');
	}
	
	public function index()
	{ 
	    $data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('email'=>$this->session->userdata('email'),'audit_number'=>1),  $order_by="id");
		$this->load->view('accounts/header');
		$this->load->view('accounts/support', $data);
		$this->load->view('accounts/footer');
	}

	public function admin_view()
	{ 
	    $data['inbox']=$this->Authentication_model->get_data($table="user_support", $condition=array('support_type'=>1,'audit_number'=>1), $order_by="id");
	    $data['trashed']=$this->Authentication_model->get_data($table="user_support", $condition=array('audit_number'=>2), $order_by="id");
	    $data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('support_type'=>1,'audit_number'=>1), $order_by="id");
	    $data['admin_queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('support_type'=>2,'audit_number'=>1), $order_by="id");
		$data['receipients']=$this->Authentication_model->get_data($table="account", $condition=array('user_enabled'=>1,'level'=>1));
		$this->load->view('admin/header');
		$this->load->view('admin/chat/support', $data);
		//$this->load->view('admin/support', $data);
		$this->load->view('admin/footer');
	}
	
	public function chat()
	{ 
	    $data['chats']=$this->Admin_model->get_data($table="chat_messages", $condition=array('chatType'=>1), $order_by="id", $asc_desc="");
	    $data['messages']=$this->Admin_model->get_data($table="chat_messages", $condition=array('chatType'=>1), $order_by="id", $asc_desc="");
	    $data['receipients']=$this->Authentication_model->get_data($table="account", $condition=array('user_enabled'=>1,'level'=>1));
		$this->load->view('admin/header');
		$this->load->view('admin/chat/support', $data);
		//$this->load->view('admin/support', $data);
		$this->load->view('admin/footer');
	}
	
	public function liveChat()
	{ 
	    $data['users']=$this->Admin_model->getData('account',$condition=array('audit_number >'=>0), $order_by="id",$asc_desc="desc"); 
	    $data['company_info']=$this->Admin_model->getData('company_info');
	    $this->load->view('admin/header');
		$this->load->view('admin/chat/liveChat', $data); 
		$this->load->view('admin/footer');
	}
	
	
	public function trash()
	{ 
	    $data['trashed']=$this->Authentication_model->get_data($table="user_support", $condition=array('audit_number'=>2), $order_by="id");
		$data['inbox']=$this->Authentication_model->get_data($table="user_support", $condition=array('support_type'=>1,'audit_number'=>1), $order_by="id");
	    $data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('audit_number'=>2), $order_by="id");
	    $data['admin_queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('support_type'=>2,'audit_number'=>2), $order_by="id");
		$data['receipients']=$this->Authentication_model->get_data($table="account", $condition=array('user_enabled'=>1,'level'=>1));
		$this->load->view('admin/header');
		$this->load->view('admin/support', $data);
		$this->load->view('admin/footer');
	}
	
	public function sent()
	{ 
	    $data['trashed']=$this->Authentication_model->get_data($table="user_support", $condition=array('audit_number'=>2), $order_by="id");
		$data['inbox']=$this->Authentication_model->get_data($table="user_support", $condition=array('support_type'=>1,'audit_number'=>1), $order_by="id");
	    $data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('support_type'=>2,'audit_number'=>1), $order_by="id");
	    $data['admin_queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('support_type'=>2,'audit_number'=>1), $order_by="id");
		$data['receipients']=$this->Authentication_model->get_data($table="account", $condition=array('user_enabled'=>1,'level'=>1));
		$this->load->view('admin/header');
		$this->load->view('admin/support', $data); 
		$this->load->view('admin/footer');
	}
	
	public function send_query()
	{
		$subject=$this->input->post('subject');
		$body=$this->input->post('message');
		$data=array('email'=>$this->session->userdata('email'),'name'=>$this->session->userdata('first_name'),'subject'=>$subject,'description'=>$body,'date_sent'=>date('Y-m-d H:i:s'));
		$this->Authentication_model->insert_data($table="user_support", $data);
		$data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('email'=>$this->session->userdata('email')), $order_by="id");
		$this->load->view('accounts/header');
		$this->load->view('accounts/support',$data);
		$this->load->view('accounts/footer');
	}
	
	public function reply_response()
	{ 
		$body=$this->input->post('message');
		$id=$this->input->post('response_id'); 
		$admin_id=$this->session->userdata('admin_id'); 
		if($admin_id==""){$response_type=1;}else{ $response_type=2;}
		$data=array('email'=>$this->session->userdata('email'),'name'=>$this->session->userdata('first_name'), 'response_id'=>$id,'response_type'=>$response_type,'response_message'=>$body,'response_date'=>date('Y-m-d H:i:s'));
		$this->Authentication_model->insert_data($table="support_responses", $data);
		$data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('id'=>$id));
		$data['responses']=$this->Authentication_model->get_data($table="support_responses", $condition=array('response_id'=>$id));
		if($admin_id=="")
		{
			$this->load->view('accounts/header');
			$this->load->view('accounts/query_response', $data);
			$this->load->view('accounts/footer');
		}
		else
		{
			$this->load->view('admin/header');
			$this->load->view('accounts/query_response', $data);
			$this->load->view('admin/footer');
		}
	}
	

	public function admin_reply()
	{ 
		$body=$this->input->post('message');
		$id=$this->input->post('response_id'); 
		$tokenId=$this->input->post('token_id'); 
		$admin_id=$this->session->userdata('admin_id'); 
		//if($admin_id==""){$response_type=1;}else{ $response_type=2;}
		//$data=array('email'=>$this->session->userdata('email'),'name'=>$this->session->userdata('first_name'), 'response_id'=>$id,'response_type'=>$response_type,'response_message'=>$body,'response_date'=>date('Y-m-d H:i:s'));
		$this->Authentication_model->insert_data($table="chat_messages", array('message'=>$body,'timestamp'=>time(),'tokenId'=>$tokenId,'chatType'=>2,'status'=>'open','email'=>$this->session->userdata('email')));
		$data['chats']=$this->Authentication_model->get_data($table="messages_view", $condition="");
		$this->load->view('admin/header');
		$this->load->view('admin/chat/query_response', $data);
		$this->load->view('admin/footer'); 
	}
	
	public function response($id,$id2="")
	{
		if($id2=="")
		{
			
			$q=$this->Authentication_model->get_data($table="user_support", $condition=array('id'=>$id));
			$data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('id'=>$id));
			$data['responses']=$this->Authentication_model->get_data($table="support_responses", $condition=array('response_id'=>$id),$order_by="response_id", $desc_asc="DESC");
		
		}else
		{
				$q=$this->Authentication_model->get_data($table="support_notification", $condition=array('id'=>$id));
				foreach($q->result() as $r)
				{
						 $support_type=$r->response_type;
						 $response_id=$r->response_id;
							if($support_type==2){
								 $this->Authentication_model->update_data($table="user_support", array('id'=>$response_id), array('status'=>4));
							$this->Authentication_model->update_data($table="support_responses", array('id'=>$id), array('status'=>2));
				
							}
				 }
			
			$data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('id'=>$response_id));
			$data['responses']=$this->Authentication_model->get_data($table="support_responses", $condition=array('id'=>$id),$order_by="id");
		
		}	 
		$this->load->view('accounts/header');
		$this->load->view('accounts/query_response', $data);
		$this->load->view('accounts/footer');
		
	}
	
 public function admin_response($id,$id2="")
	{
		$data['queries']=$this->Authentication_model->get_data($table="user_support", $condition=array('id'=>$id), $order_by="id");
		$data['responses']=$this->Authentication_model->get_data($table="support_responses", $condition=array('response_id'=>$id));
		$data['customer_responses']=$this->Authentication_model->get_data($table="support_responses", $condition=array('response_id'=>$id,'email'=>$this->session->userdata('email')));
		$this->load->view('admin/header');
		$this->load->view('accounts/query_response', $data);
		$this->load->view('admin/footer');
	}
	
	public function startLiveChat($id,$par="")
	{ 
		
		$token_id=rand(1000,20000); $email="";
		if($par=="viewChat"){ 
		$q=$this->Authentication_model->update_data($table="chat_messages", $condition=array('id'=>$id),array('seen'=>1)); 
		$q=$this->Authentication_model->get_data($table="messages_view", $condition=array('id'=>$id)); 
		foreach($q->result() as $r){ $email=$r->email;  $token_id=$r->tokenId; }
		}else{
		$email=$this->hashId($id, $table="account",  $condition=array('audit_number'=>1),$searchItem="email");		
		 }
		$data['chats']=$this->Authentication_model->get_data($table="messages_view", $condition=array('tokenId'=>$token_id), $order_by="id");
		$this->load->view('admin/header');
		$data['tokenId']=$token_id;
		$data['receiver']=$email;
		$this->load->view('admin/chat/query_response', $data);
		$this->load->view('admin/footer');
	}
	
	public function responses($id)
	{
		$tokenId="";
		//$id=$this->hashId($id, $table="chat_messages",  $condition=array('status'=>'open'),$searchItem="id");		
		$q=$this->Authentication_model->get_data($table="chat_messages", $condition=array('id'=>$id));
		foreach($q->result() as $r){ $tokenId=$r->tokenId; }
		$data['chats']=$this->Authentication_model->get_data($table="messages_view", $condition=array('tokenId'=>$tokenId), $order_by="id");
		$this->load->view('admin/header');
		$data['tokenId']=$tokenId;
		$data['receiver']="";
		$this->load->view('admin/chat/query_response', $data);
		$this->load->view('admin/footer'); 
	}
	
	
	public function change_query_status($id, $status)
	{
	 
	$this->Authentication_model->update_data($table="user_support", array('id'=>$id), array('status'=>$status));
	if($this->db->affected_rows()>0)
	{
		 
	  echo json_encode(array('result'=>'ok'));	
		
	}
	else
	{
		echo json_encode(array('result'=>'false'));	
	}

	}
	
public function changeStatus($id="")
	{
	$status="closed";
	$this->Authentication_model->update_data($table="chat_messages", array('tokenId'=>$id), array('status'=>$status));
	$q=$this->Authentication_model->get_data($table="chat_messages", array('tokenId'=>$id,'status'=>$status));
 
	if($q->num_rows()>0)
	{
		 
	  echo json_encode(array('result'=>'ok'));	
		
	}
	else
	{
		echo json_encode(array('result'=>'false'));	
	}

	}
	 
	public function compose_message()
	 {
		$from="ARI Limited";
		$subject=$this->input->post('subject');
		$body=$this->input->post('body');
		$count=0; $to="";
		foreach($this->input->post('to') as $value)
			{
			  $to=$value;	
			} 
		 if($to=="all")
		{
			$q=$this->Authentication_model->get_data($table="user_support", $condition="", $order_by="id");
			foreach($q->result() as $r)
				{
					$recipients=$r->email;
					$data=array('email'=>$recipients,'name'=>'','subject'=>$subject,'description'=>$body,'support_type'=>2, 'status'=>3,'date_sent'=>date('Y-m-d H:i:s'));
					$this->Authentication_model->insert_data($table="user_support", $data);
				}
		}
		else{
			foreach($this->input->post('to') as $value)
			{ 
			 $recipients=$value; 
			 $data=array('email'=>$recipients,'name'=>'','subject'=>$subject,'description'=>$body,'support_type'=>2, 'status'=>3,'date_sent'=>date('Y-m-d H:i:s'));
			 $this->Authentication_model->insert_data($table="user_support", $data);
			}
		}
		
		if($this->db->affected_rows()>0)
		{
			 
			echo json_encode(array('result'=>'ok','data'=>1,'msg'=>'Message sent successfully' )); 
		}
		else
		{
			echo json_encode(array('result'=>'false','data'=>0,'msg'=>'Message not added. Please try again' )); 
		}  
	 
}

 public function removeMessages($id="")
	{
		$q=$this->Authentication_model->get_data('user_support',$condition=array('id'=>$id)); 
		foreach($q->result() as $row) 
		{ 
			$status=$row->audit_number;
		}
		if($status==2){ $status=1;} else { $status=2;}
		if($this->Authentication_model->update_data($table="user_support", array('id'=>$id), array('audit_number'=>$status)))
		{
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}

	} 	
	
public function loadNotifications()
 {
	$data= $this->Authentication_model->get_data($table="support_notification", $condition=array('to'=>$this->session->userdata('email'),'response_type'=>2,'status'=>1)); 
	if($this->db->affected_rows()>0){
	 echo json_encode(array('result'=>'ok','data'=> $data->result_array() ));
	}
	else
	{
		echo json_encode(array('result'=>'false','data'=>0 ));
	}  
 }
 
 public function hashId($enct="", $table="",  $condition="",$searchItem="")
{

			if($condition==""  || $table=="" ||  $enct=="" ||  $searchItem=="")
			{ 
				 $id=""; 
			}
			else{
			$q=$this->Authentication_model->get_data($table, $condition); 
			if($q->num_rows()>0)
			 {
			   foreach($q->result() as $r)
			   {
				$id=$r->$searchItem; 
				$s = sha1('2ab'); $s = substr($s, 0, 10); 
				$e =base64_encode(do_hash($id . $s,'sha512') . $s);
				if($e==$enct)
				{  
					$id=$r->$searchItem;   break;
				} 
				else{ $id=""; }
			   } 
			  
			 }
			} 
			
			return  $id; 
}


 
}