<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property extends CI_Controller 
{
	 var $user_ip=null;
	 var $disabled="";
	public function __construct()
		{
		parent::__construct();
		$this->load->model("Property_model");
		$this->load->model("Authentication_model");
		$this->load->model("Tenant_model");
		$this->load->model("Payment_model");
		$this->load->model("Rent_model");
		$this->load->library('upload');
		$this->load->library('Expired');
		$this->load->library('Encrypte');
		$this->load->helper('Security');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Africa/Nairobi');
		$this->user_ip=$this->input->ip_address();
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry();
		} 
	
	public function index()
	{
		$data['disabled']=$this->disabled; 
		$month=date("m"); $year=date("Y"); $total_rent=0; $expected_rent=0; $total_properties=0; $total_tenants=0; $total_units=0;
		$prop=$this->Property_model->get_data($table="property_units_details", array('company_code'=>$this->session->userdata('company_code'),'property_deleted'=>1), $order_by="id");
		$units=$this->Property_model->get_data($table="unit_property_details", array('company_code'=>$this->session->userdata('company_code'),'unit_property_details.audit_number'=>1));
		$tenants=$this->Property_model->get_data($table="tenant_property_units",array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')));
		$rent_expected=$this->Property_model->get_data($table="tenant_transaction_view",  array('month_paid'=>$month,'year_paid'=>$year,'audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'type'=>'d')); 
		$rent_collected=$this->Property_model->get_data($table="tenant_transaction_view",  array('month_paid'=>$month,'year_paid'=>$year,'audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'type'=>'c')); 
		
		$data['property']=$prop;  $data['total_properties']=$prop->num_rows();  $total_u=0; foreach($units->result() as $u){ $total_u=$total_u+$u->total_units; }  
		$data['units']=$units; $data['total_units']=$total_u; 
		//
		$data['tenants']=$tenants;  $data['total_tenants']=$tenants->num_rows(); 
		foreach($rent_collected->result() as $r){ $total_rent=$total_rent+$r->amount; } 
		foreach($rent_expected->result() as $e){ $expected_rent=$expected_rent+$e->amount; } 
		$data['total_rent']=$total_rent; $data['expected_rent']=$expected_rent;
		//
		$this->load->view('accounts/header');
		$this->load->view('accounts/viewProperty',$data);
		$this->load->view('accounts/footer');
		
	}
	
	public function units()
	{
		$data['disabled']="";
		$data['units']=$this->Property_model->get_data($table="property_units",array('property_id'=>$this->session->userdata('company_code')));
		$data['property']=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['package_account']=$this->Property_model->get_data($table="package_current_account",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		
		$this->load->view('accounts/header');
		$this->load->view('accounts/unit_details',$data);
		$this->load->view('accounts/footer');
	}
	
	public function getProperties()
	{
		  $q=$this->Property_model->get_data($table="property_units_details",$condition=array('company_code'=>$this->session->userdata('company_code')) );
		  if($q->num_rows() >0){ echo json_encode(array('result'=>"ok",'data'=>$q->result_array())); }
		  else{ echo json_encode(array('result'=>"false",'msg'=>'Bad Request' )); }
	}
	

public function getData($id="")
	{
		if($id==""){ echo json_encode(array('result'=>"false",'data'=>0)); }
		else
		{
			 $q=$this->Property_model->get_data($table="property", array('id'=>$id,'company_code'=>$this->session->userdata('company_code') ));
			 if($q->num_rows()>0)
			{ 
				 echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
			}
			else
			{
				echo json_encode(array('result'=>"false"));
			}
		}
	}
	
	public function getUnits($id="")
	{
		if($id==""){   echo json_encode(array('result'=>"false",'data'=>0));}else{
		$data=$this->Property_model->get_data($table="property_unit_category", array('unit_id'=>$id));
		if($data->num_rows()>0)
		{ 
			 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		}
	}
	 
	public function add($msg="",$id="",$name="")
	{ 
		$data['msg']=$msg;
		$data['property_name']=$name;
		$data['id']=$id;
		$data['disabled']=$this->disabled;
		$data['units']=$this->Property_model->get_data($table="property_units",array('property_id'=>$this->session->userdata('company_code')));
		$data['property']=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['package_account']=$this->Property_model->get_data($table="package_current_account",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$this->load->view('accounts/header');
		$this->load->view('accounts/add_property',$data);
		$this->load->view('accounts/footer');	
	}
	  
	public function view_property($enct="")
	{
		 $hashed=new Encrypte();
		$id=$hashed->hashId($enct); 		
		if($id==""){ redirect(base_url() ."property/"); }
        $q=$this->Property_model->get_data($table="property", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		if($q->num_rows()<=0)
		 {
			redirect(base_url() ."property/"); 
		 } 
		$data['disabled']=$this->disabled;
		$data['documents']=$this->Tenant_model->get_data("media", array('property_id'=>$id,'deleted'=>1));
		$data['property']=$this->Property_model->get_data($table="property", $condition=array('id'=>$id)); 
		$data['units']=$this->Property_model->get_data($table="property_units", $condition=array('property_id'=>$id,'audit_number'=>1)); 
		$data['tenants']=$this->Tenant_model->get_data($table="tenant_property_units",$condition=array('property_id'=>$id,'tenant_status'=>1,'audit_number'=>1));
		$data['paid_rent']=$this->Rent_model->get_data($table="tenant_transaction",  array('type'=>'c','audit_number'=>1)); 
		$data['rent']=$this->Rent_model->get_data($table="tenant_pricing"); 
		$data['amenities']=$this->Property_model->get_data($table="property_amenity", $condition=array('property_id'=>$id));
		$this->load->view('accounts/header');
		$this->load->view('accounts/singleProperty',$data);
		$this->load->view('accounts/footer'); 
	}
	
	public function edit($enct="", $msg="")
	{
		$hashed=new Encrypte();
		$id=$hashed->hashId($enct); 		
		if($id==""){ redirect(base_url() ."property/"); }
        $q=$this->Property_model->get_data($table="property", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		if($q->num_rows()<=0)
		 {
			redirect(base_url() ."property/"); 
		 }
		$this->session->set_userdata(array('property_id'=>$id));
		$data['msg']=$msg;
		$data['enct']=$enct;
		$data['disabled']=$this->disabled;
		$q=$this->Property_model->get_data($table="invoice_settings", $condition=array('company_code'=>$this->session->userdata('company_code')));
		$invoice_date="28";
		
		if($q->num_rows()>0){ $r=$q->row(); $invoice_date=$r->day_to_sent; }else{ $this->Property_model->get_data($table="invoice_settings", $data=array('company_code'=>$this->session->userdata('company_code'),'day_to_sent'=>28,'email'=>'yes','automatic'=>'yes')); }
		
		$data['invoice_date']=$invoice_date;
		$data['documents']=$this->Tenant_model->get_data("media", array('property_id'=>$id,'deleted'=>1));
		$data['data']=$this->Property_model->get_data($table="property", $condition=array('id'=>$id)); 
		$data['indoor_amenities']=$this->Property_model->get_data($table="amenities", array('amenity_type'=>1));
		$data['outdoor_amenities']=$this->Property_model->get_data($table="amenities", array('amenity_type'=>2));
		$data['checked_indoor']=$this->Property_model->get_data($table="property_amenity", array('property_id'=>$this->session->userdata('property_id'),'amenity_type'=>1));
		$data['checked_outdoor']=$this->Property_model->get_data($table="property_amenity", array('property_id'=>$this->session->userdata('property_id'),'amenity_type'=>2));
		$this->load->view('accounts/header');
		$this->load->view('accounts/editProperty',$data);
		$this->load->view('accounts/footer');
	}
	
	public function doneEdit()
	{
		$id=$this->input->post('id');
		$category_id=$this->input->post('category_id');
		$property_id=$this->input->post('property_id');
		$data=array(    
			'total_units'=>$this->input->post('total_units'),
			'number_of_baths'=>$this->input->post('baths'), 
			'fully_furnished'=>$this->input->post('furnished')
			);
		$this->Property_model->update_data($table="property_category",$condition=array('id'=>$category_id,'property_id'=>$property_id),
			array('category_name'=>$this->input->post('category')));
		//if($this->Property_model->doneEdit($condition=array('id'=>$id,'property_id'=>$property_id),$data))
		if($this->Property_model->update_data("property_units",$condition=array('id'=>$id,'property_id'=>$property_id),$data))
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="change unit details unit id: ".$id,$status=1);
			echo json_encode(array('result'=>"ok"));
		}
		else
		{
			echo json_encode(array('result'=>"false"));
		}	
	}
	
	public function add_property()
	{
		$vat_management_fee=$this->input->post('vat_management_fee');
		$vat_on_property_rent=$this->input->post('vat_on_property_rent');
		$management_fee=$this->input->post('management_fee');
		$floors=$this->input->post('floors');
		if($vat_management_fee==""){ $vat_management_fee=0; }
		if($management_fee==""){ $management_fee=0; }
		if($vat_on_property_rent==""){ $vat_on_property_rent=0; }
		if($floors==""){ $floors=0; }
		$data=array(
		'company_code'=>$this->session->userdata('company_code'),
		'property_name'=>$this->input->post('property_name'),
		'lr_no'=>$this->input->post('lr_no'),
		'property_type'=>$this->input->post('property_type'),
		'floors'=>$floors,
		'car_spaces'=>$this->input->post('car_spaces'),
		'location'=>$this->input->post('location'),
		'street'=>$this->input->post('street'),
		'town'=>$this->input->post('city_town'),
		'country'=>$this->input->post('country'), 
		'management_fee'=>$management_fee,
		'water_unit_cost'=>$this->input->post('water_unit_cost'),
		'property_rent_vat'=>$vat_on_property_rent, 
		'vat_management_fee'=>$vat_management_fee,
		'property_added_by'=>$this->session->userdata('id'),
		'property_added_date'=>date('Y-m-d') 
		);  
		
		$q=$this->Property_model->get_data($table="property", array('property_name'=>$this->input->post('property_name'),'property_deleted'=>1,'company_code'=>$this->session->userdata('company_code')));
		if($q->num_rows()<=0)
		{
			 $this->Property_model->insert_data($table="property", $data); 
			  
				$c=$this->Property_model->get_data($table="property", array('property_name'=>$this->input->post('property_name'),'property_type'=>$this->input->post('property_type')));
				foreach($c->result() as $row){ $id=$row->id; }
				$d=$this->Property_model->get_data($table="property", array('property_name'=>$this->input->post('property_name'),'property_type'=>$this->input->post('property_type')));
				foreach($d->result() as $row){ $id=$row->id; }
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add property id: ".$id,$status=1);
			    //update invoice settings (Invoice to be sent automatically)
				$q=$this->Property_model->get_data($table="invoice_settings", $condition=array('company_code'=>$this->session->userdata('company_code')));
				if($q->num_rows()>0){
				$day=$this->input->post('day');$company_code=$this->session->userdata('company_code');	
				$data=array('automatic'=>'yes','day_to_sent'=>$day,'email'=>'yes');
				$this->Property_model->update_data($table="invoice_settings", array('company_code'=>$company_code), $data);
				}else
				{
					$this->Property_model->get_data($table="invoice_settings", $data=array('company_code'=>$this->session->userdata('company_code'),'day_to_sent'=>28,'email'=>'yes','automatic'=>'yes')); 
				}
				//end of invoice settings
				 $this->add($msg="Property added successfully!",$id, $this->input->post('property_name'));
				//$this->session->set_flashdata('temp',$msg);
				//redirect(base_url().'property/add'); 
			 
		}
		else{
			$msg=""; $property="";
			//foreach($q->result() as $row){ $id=$row->id; $property=$row->property_name; }
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add property, property with same name",$status=2);
			//$msg="Property with the same name already exists!";
			$data['property_name']=$property;
			$this->add($msg="error", $id="", $property);
		}
	}
	
	public function edit_property()
	{
		$id=$this->input->post('property_id');
		$enct=$this->input->post('enct_property_id');
		$vat_management_fee=$this->input->post('vat_management_fee');
		$vat_on_property_rent=$this->input->post('vat_on_property_rent');
		$management_fee=$this->input->post('management_fee');
		$floors=$this->input->post('floors');
		if($vat_management_fee==""){ $vat_management_fee=0; }
		if($management_fee==""){ $management_fee=0; }
		if($vat_on_property_rent==""){ $vat_on_property_rent=0; }
		if($floors==""){ $floors=0; }
		$data=array(
		'property_name'=>$this->input->post('property_name'),
		'property_type'=>$this->input->post('property_type'),
		'lr_no'=>$this->input->post('lr_no'),
		'water_unit_cost'=>$this->input->post('water_unit_cost'),
		'management_fee'=>$management_fee,
		'property_rent_vat'=>$vat_on_property_rent, 
		'vat_management_fee'=>$vat_management_fee,
		'floors'=>$floors,
		'car_spaces'=>$this->input->post('car_spaces'),
		'location'=>$this->input->post('location'),
		'street'=>$this->input->post('street'),
		'town'=>$this->input->post('city_town'),
		'country'=>$this->input->post('country')
		); 
	 	$this->Property_model->delete_data($table="property_features", array('property_id'=>$this->session->userdata('property_id')));
		if($this->Property_model->update_data($table="property",$condition=array('id'=>$id),$data))
		{ 
		   
		 //update invoice settings (Invoice to be sent automatically)
				$day=$this->input->post('day');$company_code=$this->session->userdata('company_code');	
				$data=array('day_to_sent'=>$day);
				$this->Property_model->update_data($table="invoice_settings", array('company_code'=>$company_code), $data);
		//end of invoice settings
		$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="edit property id: ".$id,$status=1);
		$this->edit($enct,$msg="Changes saved successfully");
		}	 
		else
		{ 
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="attempt to edit property id: ".$id,$status=2);
			
			 $this->edit($enct, $msg="Changes was not made successfully"); 
		} 
	}

	public function show_units($id="")
	{
		if($id==""){ echo json_encode(array('result'=>"false",'data'=>0));}else{
		//$q=$this->Property_model->get_data($table="property_units", array('property_id'=>$id));
		$q=$this->Property_model->get_data($table="unit_property_details", array('property_id'=>$id,'audit_number'=>1));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else{
			echo json_encode(array('result'=>"false"));
		}
		}
	}

	public function add_units()
	{
			$data=array( 
			'property_id'=>$this->input->post('property'),
			'property_category_id'=>$this->input->post('category'),
			'total_units'=>$this->input->post('units'),
			'number_of_baths'=>$this->input->post('baths'),
			'fully_furnished'=>$this->input->post('furnished')
			); 
		$q=$this->Property_model->get_data($table="property_units",array('property_category_id'=>$this->input->post('category'),'property_id'=>$this->input->post('property')));	
		if($q->num_rows()>0)
			{
				echo json_encode(array('result'=>'false','msg'=>'Failed to add unit details. Details for the same already exists'));
			}
		 else{
			if($this->Property_model->insert_data($table="property_units",$data))
			{
				$query=$this->Property_model->get_data($table="unit_property_details", array('property_category_id'=>$this->input->post('category'),'property_id'=>$this->input->post('property')));
				if($query->num_rows()>0){
					foreach($query->result() as $r){ $unit_id=$r->unit_id; $category=$r->category_name; $property=$r->property_name; }
				    
				$this->session->set_userdata(array('property_category_id'=>$this->input->post('category')));
			    $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add new unit  to property id: ".$this->input->post('property'),$status=1);
				echo json_encode(array('result'=>'ok','unit_id'=>$unit_id,'category'=>$category,'property'=>$property,'msg'=>' Data saved successfully '));
				}
			}
			else
			{
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new unit  to property id: ".$this->input->post('property'),$status=2);
			
				echo json_encode(array('result'=>'false','msg'=>'Units details not added. Contact the administrator'));
			}
		}
	}
 
	public function add_category()
	{
		//$q=$this->Property_model->checkIfCategoryExist(array('property_id'=>$this->input->post('property_id'),'category_name'=>$this->input->post('category_name')));
		$q=$this->Property_model->get_data("property_category",array('property_id'=>$this->input->post('property_id'),'category_name'=>$this->input->post('category_name')));
		if($q->num_rows()>0)
		{
			 
		  echo json_encode(array('result'=>'false','msg'=>'Category name already exists'));	
			
		}else{
		
		$data=array('property_id'=>$this->input->post('property_id'),'is_user_defined'=>1,'company_code'=>$this->session->userdata('company_code'),'category_name'=>$this->input->post('category_name')); 
		if($this->Property_model->insert_data($table="property_category", $data))
		{
			$id="";
			//$c=$this->Property_model->checkIfCategoryExist(array('property_id'=>$this->input->post('property_id'),'category_name'=>$this->input->post('category_name')));
			$c=$this->Property_model->get_data("property_category", array('property_id'=>$this->input->post('property_id'),'category_name'=>$this->input->post('category_name')));
			if($c->num_rows()>0)
			{
				foreach($c->result() as $row){ $id=$row->id;}
			}
			echo json_encode(array('result'=>'ok','id'=>$id,'name'=>$this->input->post('category_name'),'msg'=>'Category added successfully'));
		}
		else
		{
			echo json_encode(array('result'=>'false','msg'=>'Failed to add category'));
		}
	}
	}
                 
	
 	public function getCategory($id="")
	{
		if($id==""){ echo json_encode(array('result'=>"false")); }else{
			//$data=$this->Property_model->getCategory(array('audit_number'=>1,'property_id'=>$id,'company_code'=>$this->session->userdata('company_code')));	
			$data=$this->Property_model->get_data($table="unit_property_details",array('audit_number'=>1,'property_id'=>$id,'company_code'=>$this->session->userdata('company_code')),$order_by="unit_id");	
			if($data->num_rows()>0)
			{         
				 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
			}
			else
			{
				echo json_encode(array('result'=>"false"));
			}
		}
	}
	
	public function getCategoryDetails($id="")
	{
		$prop=""; $category="";
		$data=$this->Property_model->get_data("unit_property_details", array('unit_id'=>$id));	
		if($data->num_rows()>0)
		{    
			foreach($data->result() as $r){ $prop=$r->property_name; $category=$r->category_name;}    
			 echo json_encode(array('result'=>"ok",'property'=>$prop,'category'=>$category));
		}
		else{
			echo json_encode(array('result'=>"false",'property'=>$prop,'category'=>$category));
		}
	}
	
	public function property_Category($id="")
	{
		if($id==""){ echo json_encode(array('result'=>"false",'data'=>0));}else{
		//$data=$this->Property_model->property_Category(array('property_id'=>$id,'company_code'=>$this->session->userdata('company_code'),'is_user_defined'=>1));	
		$data=$this->Property_model->get_data("property_category", array('property_id'=>$id,'company_code'=>$this->session->userdata('company_code'),'is_user_defined'=>1));	
		if($data->num_rows()>0)
		{         
			 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		}
	}

 public function deleteUnit($id="")
	{
		if($id==""){ redirect(base_url().'property/');}
		$query=$this->Property_model->get_data($table="property_units", array('id'=>$id,'audit_number'=>1));
		if($query->num_rows()>0)
		{  
			//$this->Property_model->delete_data($table="property_units", array('id'=>$id));
			$this->Property_model->update_data($table="property_units", array('id'=>$id),array('audit_number'=>2));
			$q=$this->Property_model->get_data($table="property_units", array('id'=>$id,'audit_number'=>1));
			
			if($q->num_rows()<=0)
			{ 
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="removed property unit id: ".$id,$status=1);
				 echo json_encode(array('result'=>"ok"));
			}
			else
			{
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to remove property unit id: ".$id,$status=2);
				
				echo json_encode(array('result'=>"false"));
			}
		}
	else
	{ 
		echo json_encode(array('result'=>"false"));
	}	
}

public function get_property_Category($id="",$c="")
	{
			if($id !="")
			{
				//$data=$this->Property_model->getCategory(array('audit_number'=>1,'property_id'=>$id,'company_code'=>$this->session->userdata('company_code')));	
				$data=$this->Property_model->get_data("property_category_view",array('audit_number'=>1,'property_id'=>$id,'company_code'=>$this->session->userdata('company_code')));	
			
		    }
			if($data->num_rows()<=0){

				if($c=="")
				{
					$data=$this->Property_model->get_data($table="property_category", array('property_type <>'=>"Office",'is_user_defined'=>2));	
				}
				else
				{	
					$data=$this->Property_model->get_data($table="property_category", array('property_type'=>'Office','is_user_defined'=>2));	
				}
			
			}
			
		 
		if($data->num_rows()>0)
		{         
			 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		 
	}

 	public function insertPricing_details()
	{
		//$property_category_id=$this->input->post('property_category_id');
		$unit_id=$this->input->post('unit_id');
		$payment_type=$this->input->post('payment_type');
		$payment_type_value=$this->input->post('payment_type_value');
		$deposit=$this->input->post('pricing_deposit');
		$priority=$this->input->post('priority');
		$check_priority=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id,'priority'=>$priority));	
		$check_if_exists=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id,'payment_type'=>$payment_type,'audit_number'=>1));
/*if($check_priority->num_rows()>0)
		{
			echo json_encode(array('result'=>"false",'msg'=>'Priority already exists','data'=>0));
		}*/
	 
		while($check_priority->num_rows()>0)
		{
			$priority=$priority+1;//echo json_encode(array('result'=>"false",'msg'=>'Priority already exists','data'=>0));
			$check_priority=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id,'priority'=>$priority));	
		}
		if($check_if_exists->num_rows()>0)
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add pricing details for unit id: ".$unit_id,$status=2);
			echo json_encode(array('result'=>"false",'msg'=>'Payment details for '.$payment_type.' already exists','data'=>0));
		}
		else{
				if($payment_type_value>0)
				{
					$this->Property_model->insert_data($table="pricing_details", array('unit_id'=>$unit_id,'payment_type'=>$payment_type,'payment_type_value'=>$payment_type_value,'priority'=>$priority,'is_deposit'=>0));
				
				} 
				 if($deposit >0)
				 {
					  $check_if_avail=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id,'payment_type'=>($payment_type." Deposit"),'audit_number'=>1));
                       if($check_if_avail->num_rows() <=0)
					   {
						$this->Property_model->insert_data($table="pricing_details", array('unit_id'=>$unit_id,'payment_type'=>($payment_type." Deposit"),'payment_type_value'=>$deposit,'is_deposit'=>1,'priority'=>$priority));
					    echo json_encode(array('result'=>"ok",'msg'=>'Data save successfully'));
					   return;
					   }else{
						  echo json_encode(array('result'=>"false",'msg'=>'Payment details for '.$payment_type.' Deposit already exists!'));
					   return;
					   }
				}
				 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add pricing details for unit id: ".$unit_id,$status=2);
				  echo json_encode(array('result'=>"ok",'msg'=>'Data save successfully')); 
				} 
			 
	} 

	
	public function updatePricing_details()
	{
		$pricing_id=$this->input->post('id');
		$unit_id=$this->input->post('unit_id'); 
		//$property_category_id=$this->input->post('property_category_id');
		$payment_type=$this->input->post('payment_type');
		$payment_type_value=$this->input->post('payment_type_value');
		$priority=$this->input->post('priority');
		$r=$this->Property_model->update_data($table="pricing_details", array('id'=>$pricing_id,'unit_id'=>$unit_id), array('payment_type'=>$payment_type,'payment_type_value'=>$payment_type_value,'priority'=>$priority));	
		$query=$this->Property_model->update_data($table="tenant_pricing", array('unit_id'=>$unit_id), array('payment_type'=>$payment_type,'payment_type_value'=>$payment_type_value,'priority'=>$priority));	
		if($r||$query)
		{ 
	$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="changed pricing details for unit id: ".$unit_id,$status=1);
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
	else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		} 
	} 
	
	public function getPricing_details($id="")
	{
		$data=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$id,'audit_number'=>1));	
		if($data->num_rows()>0)
		{ 
			 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	public function check_pricing_details($property_id="")
	{
		if($property_id == ""){	echo json_encode(array('result'=>"false"));	}
		else{
			$category_id=$this->input->post('category_id');
			//$q=$this->Property_model->get_data($table="property_units", array('property_category_id'=>$category_id,'property_id'=>$property_id));	
			$q=$this->Property_model->get_data($table="property_units", array('id'=>$category_id,'property_id'=>$property_id));	
			if($q->num_rows()>0)
			{ 
				foreach($q->result() as $r){ $unit_id=$r->id;}  
				$data=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id));	
				if($data->num_rows()>0)
				{ 
					 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
				}
				else { echo json_encode(array('result'=>"false")); }
				
			}else{ echo json_encode(array('result'=>"false")); }  
		}
	}

	public function check_tenant_deposit()
	{
		    $unit_id=$this->input->post('unit_id'); 
			$data=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id));	
			if($data->num_rows()>0)
			{ 
				 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
			}
			else
			{
				echo json_encode(array('result'=>"false",'data'=>0));
			} 
		
	} 
	
	public function remove_property($id="")
	{
		if($id==""){ redirect(base_url() ."property/");}
		$q=$this->Property_model->get_data($table="property", array('id'=>$id));	
		if($q->num_rows()>0) { 
		//$data=$this->Property_model->update_data($table="property", array('id'=>$id,'property_deleted'=>1,'company_code'=>$this->session->userdata('company_code')), array('property_deleted'=>2,'property_delete_date'=>date("Y-m-d"),'audit_number'=>2,'property_deleted_by'=>$this->session->userdata('id')));	
		$this->Property_model->update_data($table="property", array('id'=>$id,'property_deleted'=>1,'company_code'=>$this->session->userdata('company_code')), array('property_deleted'=>2,'property_delete_date'=>date("Y-m-d"),'audit_number'=>2,'property_deleted_by'=>$this->session->userdata('id')));
		$q=$this->Property_model->get_data('property',array('id'=>$id,'property_deleted'=>2,'audit_number'=>2,'company_code'=>$this->session->userdata('company_code'))); 
	if ($q->num_rows()>0) 
		{
			$p=$this->Property_model->get_data($table="property_units", array('property_id'=>$id));
			foreach($p->result() as $r){
				$unit_id=$r->id;
				$this->Property_model->update_data($table="tenant", array('property_unit_id'=>$unit_id,'audit_number'=>1), array('audit_number'=>2,'tenant_deleted'=>2,'delete_date'=>date("Y-m-d"),'deleted_by'=>$this->session->userdata('id'),'tenant_status'=>2));	
				$tenants=$this->Property_model->get_data($table="tenant", array('property_unit_id'=>$unit_id));
				foreach($tenants->result() as $t){ 
						$tenant_id=$t->id;
						$this->Property_model->update_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2));	
						$this->Property_model->update_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2));	
						$this->Property_model->update_data($table="property_units", array('property_id'=>$id,'audit_number'=>1), array('audit_number'=>2));	
						$this->Property_model->update_data($table="tenant_payment", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2));	
						$this->Property_model->update_data($table="pricing_details", array('unit_id'=>$unit_id,'audit_number'=>1), array('audit_number'=>2));	
						$this->Property_model->update_data($table="current_account", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2));	
					 
					}
				}
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="removed property id: ".$id,$status=1);
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="Failed to removed property id: ".$id,$status=2);
			
			echo json_encode(array('result'=>"false",'data'=>0));
		}  
		}
	}
	
	public function remove_pricing_details()
	{
		$unit_id=0;
		$id=$this->input->post('id');
		$q=$this->Property_model->get_data($table="pricing_details", array('id'=>$id));	
		if($q->num_rows()>0) {  foreach($q->result() as $r){ $unit_id=$r->unit_id; } }
		//$data=$this->Property_model->delete_data($table="pricing_details", array('id'=>$id));	
		if($this->Property_model->delete_data($table="pricing_details", array('id'=>$id)))
		{ 
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="removed pricing details for unit id: ".$unit_id,$status=1);
			echo json_encode(array('result'=>"ok",'unit_id'=>$unit_id,'data'=>1));
		}
		else
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to remove pricing details for unit id: ".$unit_id,$status=2);
			
			echo json_encode(array('result'=>"false",'unit_id'=>$unit_id,'data'=>0));
		}  
	}
	
	public function edit_pricing_details($id)
	{
		$data=$this->Property_model->get_data($table="pricing_details", array('id'=>$id));	
		if($data->num_rows()>0)
		{ 
			 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false"));
		} 
		
	}
	
public function view_pricing_details($category_id, $property_id)
{
	$unit_id=0;
	$data=$this->Property_model->get_data($table="property_units", array('audit_number'=>1,'property_category_id'=>$category_id,'property_id'=>$property_id));	
	if($data->num_rows()>0)
	{ 
		foreach($data->result() as $r){  $unit_id=$r->id;}
		$q=$this->Property_model->get_data($table="pricing_details", array('audit_number'=>1,'unit_id'=>$unit_id));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'unit_id'=>$unit_id,'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'unit_id'=>$unit_id));
		}
	}
	else
	{
		echo json_encode(array('result'=>"false",'unit_id'=>$unit_id,'data'=>0));
	}
}
 
 public function checkProperty($id="")
	{
		$allocated_spaces=0; $spaces=0;  $allocated_units=0;  $available_units=0;  $total_floors=0;  $total_tenants=0; 
		if($id=="")
		{  
			echo json_encode(array('result'=>"false",'total_floors'=>0,'total_tenants'=>$total_tenants,'car_spaces'=>0, 'units_available'=>$available_units,'data'=>0)); 
		}
		else
		{
			$data=$this->Property_model->get_data($table="property", array('id'=>$id,'company_code'=>$this->session->userdata('company_code')));	
			if($data->num_rows()>0)
			{ 
				foreach($data->result() as $row){  $spaces=$row->car_spaces; $total_floors=$row->floors;}
				$data1=$this->Property_model->get_data($table="property_units", array('property_id'=>$id));
				foreach($data1->result() as $rows){  $allocated_units=$allocated_units+$rows->total_units;}
				$data2=$this->Tenant_model->get_data($table="tenant_property_units", array('property_id'=>$id,'tenant_status'=>1));
				foreach($data2->result() as $car){ $total_tenants++; $allocated_spaces=$allocated_spaces+$car->parking_allocated;}
				$available_spaces=$spaces-$allocated_spaces;
				$available_units=$allocated_units-$total_tenants;
				echo json_encode(array('result'=>"ok",'total_tenants'=>$total_tenants,'total_floors'=>$total_floors,'car_spaces'=>$available_spaces, 'units_available'=>$available_units,'data'=>1));
			}
			else
			{
				echo json_encode(array('result'=>"false",'total_floors'=>0,'total_tenants'=>$total_tenants,'car_spaces'=>0, 'units_available'=>$available_units,'data'=>0));
			}
		}  
	}
	
	public function checkUnits($prop_id="",$category_id="")
	{  
		$available_units=0;  $total_tenants=0; $allocated_units=0;
		//$data1=$this->Property_model->get_data($table="property_units", array('audit_number'=>1,'property_category_id'=>$category_id,'property_id'=>$prop_id));
		$data1=$this->Property_model->get_data($table="property_units", array('audit_number'=>1,'id'=>$category_id,'property_id'=>$prop_id));
		if($data1->num_rows()>0)
		{
			$unit_id="";
			foreach($data1->result() as $rows){ $unit_id=$rows->id; $allocated_units=$allocated_units+$rows->total_units;}
			$data2=$this->Tenant_model->get_data($table="tenant_property_units", array('property_unit_id'=>$unit_id));
			foreach($data2->result() as $t){ $total_tenants++; }  
			$available_units=$allocated_units-$total_tenants;
			echo json_encode(array('result'=>"ok",'units_available'=>$available_units));
		}
		else
		{
			echo json_encode(array('result'=>"false",'total_floors'=>0,'car_spaces'=>0, 'units_available'=>$available_units));
		}
	} 
	
  public function deleteFile($id="",$tenant_id="")
	{ 	
		//if($this->Property_model->deleteFile(array('id'=>$id)))
		if($this->Property_model->update_data("media",array('id'=>$id),array('audit_number'=>1)))
		{  
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="removed file with id: ".$id,$status=1);
			
			 echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to removed file id: ".$id,$status=2);
			
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}

	public function do_upload($id, $business_no="")
	{
		$config['upload_path'] = 'media/'; 
		$config['allowed_types'] = 'jpeg|jpg|png|pdf|gif';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
        $this->upload->initialize($config);
		$targetDir='./media/';   //directory name
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);
		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			//$this->upload->initialize($this->set_upload_options());
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];//get file name
			$images[] = $fileName;
			$targetFile = $targetDir.$business_no."_".$fileName;
			if(move_uploaded_file($_FILES['userfile']['tmp_name'],$targetFile))
			{
				//upload files to images directiry and also  to the database
				$data=array('media'=>$business_no."_".$fileName,'property_id'=>$id,'added_by'=>$this->session->userdata('id'),'date_added'=>date("Y-m-d H:i:s"));
				$check=$this->Property_model->get_data("media", array('media'=>$business_no."_".$fileName,'property_id'=>$id));
				if($check->num_rows()<=0)
				{   
					$this->Property_model->insert_data($table='media',$data); //pass parameter with data to model
				}
		}
	}
		$fileName = implode(',',$images);
		if($business_no==""){		
				  $this->view_property($id);
		}
}
 
 public function hashSSHA($password){
 
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        //$encrypted = base64_encode(sha1($password . $salt, true) . $salt);
		$encrypted = base64_encode(do_hash($password . $salt,'sha512') . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        echo  json_encode($hash);
		$this->checkhashSSHA($salt, $password);
    }
	
	public function checkhashSSHA($salt, $password){
 
        $hash = base64_encode(do_hash($password . $salt,'sha512') . $salt);
 
        echo '<br/> After: '.json_encode($hash);
    }
public function convert_id($id="")
{   
	if($id==""){ $enct="";}else{
	$encrypted_string = $this->encrypt->encode($id);		
	$enct=str_replace(array('+', '/', '='), array('-', '_', ''), $encrypted_string);
     }
	echo  json_encode(array('converted_id'=>$enct));						
} 
	
}

?>