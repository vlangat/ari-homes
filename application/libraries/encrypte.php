<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Encrypte extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();  
		$this->load->model("Authentication_model"); 
		date_default_timezone_set('Africa/Nairobi'); 
	}
 
public function hashId($enct="")
	{
		$dec_id=str_replace(array('-', '_', ''), array('+', '/', '='), $enct);
		$id=$this->encrypt->decode($dec_id); 
		return  $id; 
    }

}

?>