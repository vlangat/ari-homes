<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rent_class extends CI_Controller {
	var $disabled="";
	public function __construct()
	{
		parent::__construct();  
		$this->load->model("Authentication_model");
		$this->load->model("Property_model"); 
		$this->load->model("Tenant_model"); 
		$this->load->model("Rent_model"); 
		$this->load->library('upload');
		$this->load->library('Expired'); 
		date_default_timezone_set('Africa/Nairobi');
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry();
	}
public function expected_pay_day($tenant_id)
	{
		$day=""; $days_of_year=date("z", mktime(0,0,0,12,31,date('Y'))) + 1;
		$month = date("m");	 $year = date("Y"); $days_of_month=date("t",mktime(0,0,0,$month,1,$year)); 
		$expiry_date=date_create(date("Y-m-d"));
		//$day=""; $rent_frequency="Monthly"; 
		$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
		if($q->num_rows()>0)
		{
			 $r = $q->row();  
			 $date_added=$r->date_added; $rent_frequency=$r->rent_frequency; $expiry_date=$r->expected_pay_day;
			$now=date("d");
			if($rent_frequency=="Monthly")
			{
				
				$date_added=date_create($date_added);
		        date_add($date_added,date_interval_create_from_date_string($days_of_month-$now+$expiry_date  ."days"));
		        $day=date_format($date_added,"Y-m-d");  
			}
			else if($rent_frequency=="Daily"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(1 ."days"));
				$day=date_format($date,"Y-m-d"); 
			} 
			else if($rent_frequency=="Quarterly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(90-$now+$expiry_date ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Yearly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string($days_of_year-$now+$expiry_date."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Weekly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(7 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
		}
		 return  $day; 
		
}

public function expected_pay($id="", $month="")
{
			$debit=0;  $year=date('Y');
			if($month==""){ $month=date('m'); } 
			if($id=="")
			{
				echo json_encode(array('result'=>"false")); return;
			}
			
			else
			{
			//$q=$this->Rent_model->get_statement($condition=array('month_paid'=>$month,'tenant_id'=>$id));
			$this->db->select("id,SUM(amount) as amount ")
			->where(array('tenant_id'=>$id,'type'=>'d','audit_number'=>1))
			->where("SUBSTRING(tenant_transaction.date_paid,1,2)",$month)
			->where("SUBSTRING(tenant_transaction.date_paid,-4,4)",$year)
			->group_by('id');
			$q=$this->db->get('tenant_transaction');
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						//if($row->type=="d" && ($month==date("m",strtotime($row->date_paid))))
						//{ 
							$debit=($debit+$row->amount);
						//}   
					} 
				}
			}
		return $debit; 
}

public function property_expense($id="", $month="")
{
			$expenses=0;
			$q=$this->Rent_model->get_data($table="expenses", $condition=array('audit_number'=>1,'MONTH(date_paid)'=>$month,'property_id'=>$id,'expense_type'=>1));
			if($id=="")
			{
				$expenses=0;
			}
			else
			{
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						if($month==date("m",strtotime($row->date_paid))){ $expenses=$expenses+$row->amount; }  
						
					}
				}
			}
	return $expenses;
}

public function reimbursement($id="", $month="")
{
	$deposits=0;
	$q=$this->Rent_model->get_data($table="refunded_deposits", $condition=array('audit_number'=>1,'SUBSTRING(date_refunded,6,2)'=>$month,'property_id'=>$id));
			if($id=="") { $deposits=0; }
			else
			{
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{ 
						//if($month==date("m",strtotime($row->date_refunded))){ $deposits=$deposits+$row->amount; }  
						$deposits=$deposits+$row->amount; 
					}
				}
			}
	return $deposits;
}


public function balance_forwarded($id="", $month="")
{
			$debit=0;   $balance=0; $credit=0;
			if($month==""){ $month=date('m'); } 
			if($month==1){ $month=12; } else { $month=$month-1;} 
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'SUBSTRING(date_paid,1,2)'=>$month));
			if($id=="")
			{
				$balance=0;
			}
			else
			{
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						if($row->type=="d" && ($month==date("m",strtotime($row->date_paid)))){ $debit=$debit+$row->amount; }  
						if($row->type=="c" && ($month==date("m",strtotime($row->date_paid)))){$credit=$credit+$row->amount;} 
						if($credit>=$debit){ $balance=($credit-$debit);} else if($credit<$debit) { $balance= $debit-$credit; } 
						 
					} 
				}
			}
		return $balance; 
}

public function getPrevBalance($id="", $par2="")
	{
		    $credit=0; $debit=0; $prev_bal=0; $rent=0;
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'month_paid'=>$month));
			if($id=="")
			{
				echo json_encode(array('result'=>"false"));
			}
			else
			{
			if($q->num_rows()>0)
				{
					$count=$q->num_rows();
					foreach($q->result() as $row)
					{
						if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
						if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
						//if($count==2){  $prev_bal=$debit-$credit; }	
						if($row->type=='d'){  $rent=$row->amount; }
						$count--;
					} 
				}
			}
		
		//$return_val= $prev_bal;
		$return_val= $rent;
		if($par2==""){ } else{  $return_val=$rent; }
		
		 
		return $return_val;
	}
	
	
	public function paid_rent($id="", $month="", $year="")
	{
		if($month==""){ $month=date('m'); }
		if($year==""){ $year=date('Y'); }
		$paid=0;
		$q=$this->Rent_model->get_paid_rent($table="tenant_transaction",  array('tenant_id'=>$id,'SUBSTRING(tenant_transaction.date_paid, 1, 2) ='=>$month,'tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_transaction.type'=>'c','tenant_transaction.audit_number'=>1)); 
		 
		 foreach($q->result()  as $row)
			{  
				if($month==date("m",strtotime($row->date_paid)) && $year==date("Y",strtotime($row->date_paid)))
				{ 
					$paid=$paid+$row->amount;
				}
			}
		return $paid;
	}
	
public function get_receipt_rent($id="", $month="", $year="")
{ 
	if($month==""){ $month=date('m'); }
	if($year==""){ $year=date('Y'); }
	$tenant_transaction_id=0; $amount=0;
	$q=$this->Rent_model->get_data($table="tenant_transaction",  array('tenant_id'=>$id,'SUBSTRING(date_paid,1,2)'=>$month,'SUBSTRING(date_paid,-4,4)'=>$year,'description'=>'Receipt','type'=>'c','audit_number'=>1)); 
	
	if($q->num_rows() >0)
	{
		foreach($q->result()  as $row)
		{  
			//if($month==date("m",strtotime($row->date_paid)) && $year==date("Y",strtotime($row->date_paid)))
			//{ 
				$tenant_transaction_id=$row->id;
				//$receipt_items=$this->Rent_model->get_receipt($tenant_transaction_id);
				//foreach($receipt_items->result() as $r){ if($r->payment_type=="Rent"){$amount=$amount+$r->paid_amount;}}  
				//$s=$this->Rent_model->getData($table="tenant_pricing",  array('tenant_id'=>$id,'payment_type'=>'Rent','audit_number'=>1)); 
				$this->db->select("id,SUM(paid_amount) as payment_type_value")
                    ->where(array('tenant_id'=>$id,'transaction_id'=>$tenant_transaction_id,'payment_type'=>'Rent','audit_number'=>1))
                    ->group_by('id');
                   $s=$this->db->get('tenant_payment');
                    
				foreach($s->result() as $c){  $amount=$amount+$c->payment_type_value;} 
			//} 
		
		}
	} 
	return $amount;
	 
}
 
	
public function business_expense($year="", $month="")
{
	
	$expenses=0;
	if($year==""){  $year=date("Y");}
	/*if($year==""){  $year=date("Y");}
	$q=$this->Rent_model->getData($table="expenses", $condition=array('audit_number'=>1,'MONTH(date_paid)'=>$month,'company_code'=>$this->session->userdata('company_code'),'expense_type'=>2));
	 
		if($q->num_rows()>0)
			{ 
				foreach($q->result() as $row)
				{
					if($month==date("m",strtotime($row->date_paid)) && $year==date("Y",strtotime($row->date_paid))){ $expenses=$expenses+$row->amount; }  
				}
			} */
			$this->db->select("id,date_paid,SUM(amount) as total")
                    ->where('company_code',$this->session->userdata('company_code'))
                    ->where('expense_type','2')
                    ->where("SUBSTRING(expenses.date_paid,1,2)",$month)
                    ->where("SUBSTRING(expenses.date_paid,-4,4)",$year)
                    ->group_by('id');
                    $q=$this->db->get('expenses');
					
			if($q->num_rows()>0)
			{  
				foreach($q->result() as $row){ $expenses=($expenses+$row->total); }
			}
			if($expenses==""){ $expenses=0;} 	 
	return $expenses;
}


public function management_fees($year="", $month="")
{
	$paid=0;
	$q=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'))); 
	foreach($q->result() as $t)
	{  
		$tenant_id=$t->id; 
		$management_fee=$t->management_fee; 
		$e=$this->expected_pay($tenant_id, $month);
		if($t->tenant_deleted==2 && $e==0){
			 continue;
		}
		else
		{
		  //$paid=$paid+($management_fee/100*$this->paid_rent($tenant_id, $month ,$year));
		  $paid=$paid+($management_fee/100*$this->get_receipt_rent($tenant_id, $month ,$year));
		}
	}
	return $paid;
}

public function get_balance($id)
{
	 $credit=0; $debit=0; 
	 $check_bal=$this->Rent_model->get_data($table="tenant_transaction" , array('audit_number'=>1,'tenant_id'=>$id));
	 if($check_bal->num_rows()>0){ foreach($check_bal->result() as $balance){ if($balance->type=="c"){$credit=$credit+$balance->amount;}else if($balance->type=="d"){ $debit=$debit+$balance->amount;} } } 
	 //if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit){$curr_bal= $debit-$credit;}
	 $curr_bal= $debit-$credit;
	 $this->Rent_model->update_data($table="tenant_current_account", array('total_amount'=>($curr_bal)),  array('tenant_id'=>$id));
		
	 return  $curr_bal;
}
	
}

?>