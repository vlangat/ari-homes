<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Captcha extends CI_Controller {
  
  public function index(){
		// loading captcha helper
	   $this->load->helper('captcha'); 
      // numeric random number for captcha
      $random_number = substr(number_format(time() * rand(),0,'',''),0,6);
      // setting up captcha config
      $vals = array(
             //'word' => $random_number,
			 'img_path' => 'captcha/',
				'img_url' => base_url() . 'captcha/',
				'img_width' => 158,
				'img_height' => 50,
				'font_size' => 22,
				'font_path' => './font/monofont.ttf',
				'expiration' => 7200
            );
      $data['captcha'] = create_captcha($vals);
	 
      $this->session->set_userdata('captchaWord',$data['captcha']['word']);
      $this->session->set_userdata('image',$data['captcha']['image']);
      //$this->load->view('captcha', $data); 
      return $data['captcha']['image'];
	}
	
  public function getVal()
  {
	  return $this->session->userdata('captchaWord');
  }
  
  public function check_captcha($str)
  {
    $word = $this->session->userdata('captchaWord');
    if(strcmp(strtoupper($str),strtoupper($word)) == 0)
	{
      return true;
    }
    else{ 
      $this->form_validation->set_message('check_captcha', 'Please enter correct words!');
      return false;
    }
  }
}
?>