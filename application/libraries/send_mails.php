<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Send_mails extends CI_Controller {
	var $disabled="";
	public function __construct()
	{
		parent::__construct();  
		$this->load->model("Authentication_model");
		$this->load->model("Property_model"); 
		$this->load->model("Tenant_model"); 
		$this->load->model("Rent_model");   
		$this->load->model("Payment_model");   
		date_default_timezone_set('Africa/Nairobi'); 
	}
	
public function checkExpiryDate($id="")
 {
		 
	    $expiring_date=$this->Payment_model->get_data(array('audit_number'=>1,'user_added_by'=>0), $table="user_info");
		$days=0; $expiry_date=""; 
		if($expiring_date->num_rows() >0){
		foreach($expiring_date->result() as $e)
		{ 
			$expiry_date=$e->acc_expiry_date; $email=$e->email; $name=ucfirst(strtolower($e->first_name));   
			if($expiry_date=="0000-00-00"){ }
			else{ 
				$now=time(); 
				$diff_time=strtotime($expiry_date)-$now;
				$days=floor($diff_time/(60*60*24));  
				//return $days; 
				}
				  
				if($days==7)
				{ 
					$body=' <html>
					<div style="width:90%;padding:10px;background:ffffff;">
					<div style="padding-left:10px;line-height:20px">
					<center> <img  src="'.base_url().'images/login/ARI_Homes_Logo.png" height="60"></center>
					<p><font size="3" align="left"> Dear  '.$name.',</font><br/>
					Your subscription will expire on '.$expiry_date.', you have only '.$days.' Day(s) remaining for the system  to expire<br/> 
					Please subscribe or renew your package before '.$expiry_date.' all the services provided by our Property Management System.
					</p> 
					<br/> '.$this->getFooter().'
					</div>
					</div>
					</center>
					</body>';
					$from='ARI Homes';
					$subject='Account Subscription Reminder';
					$this->sendMail($email,$body,$subject,$from);
						
				}
				else if($days==0)
				{
					
					$body=' <html>
					<div style="width:90%;padding:10px;background:ffffff;">
					<div style="padding-left:10px;line-height:20px">
					<center> <img  src="'.base_url().'images/login/ARI_Homes_Logo.png" height="60"></center>
					<p><font size="3" align="left"> Dear  '.$name.',</font><br/>
					Your subscription has expired,
					Please subscribe or renew your package  to enjoy all the services provided by  our Property Management System.
					</p><br/>
					<p> Thank you </p>
					<br/> '.$this->getFooter().'
					</div>
					</div>
					</center>
					</body>';
					$from='ARI Homes';
					$subject='Account Subscription Reminder';
					$this->sendMail($email,$body,$subject,$from);
				
				}
			
	}
	echo json_encode(array('result'=>"ok",'msg'=>'Message sent successfully'));	
 }else{ echo json_encode(array('result'=>"false",'msg'=>'No users whose subscription has expired'));	 }
}
 
 
 public function getFooter()
 {
	 $footer='<br/>
		<p align="left"> Thanks, <br/>  
		 Team ARI Homes
		</p>
		  <div>
			<font size="2"> <center> &copy; '.date("Y").' ARI Homes   </center> </font>
			<center> <font> <a href="mailto:support@arihomes.co.ke">Support@arihomes.co.ke </a> | +254 789 502 424 | +254 789 732 828 </font> </center> 
		   <center><font size="2"> 1 <sup>st </sup> Floor | All Africa Council of Churches | Sir Francis Ibiam House | Waiyaki Way, Westlands, Nairobi  <br/>
			 
			<a href="'.base_url().'media/ARI Homes Terms and Conditions 02062017.pdf">Terms </a> </font>
			</center>
			 
			</div>';
			return $footer;
 }
 
 public function sendMail($to,$body,$subject,$from="ARI Homes",$fileName="")
    {
		$this->load->library('email'); 
		$this->email->from('info@arihomes.co.ke',$from);
		$this->email->to($to); 
		$this->email->subject($subject);
		$this->email->message($body);  
		if($fileName !="")
		{  
			$this->email->attach($_SERVER["DOCUMENT_ROOT"].'media/'.$fileName); 
		} 
		$this->email->set_mailtype("html");		
		$this->email->send();
		//echo $this->email->print_debugger();
		$this->email->print_debugger();

	}  

}

?>