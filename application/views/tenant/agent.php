<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		var tenant=$("#tenants").val();
		var pay_mode=$("#pay_mode").val();
		var receipt_no=$("#receipt_no").val();
		var amount=$("#amount").val();
		var pay_date=$("#pay_date").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
		//if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
		 
	}
	 
</script>

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   User  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Agent</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  
<div class="row">
	
	<div class="col-md-12"   style="min-height:400px">  
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit"> 
			<div class="portlet light">
				<div class="portlet-title">
					<div class="col-md-12" style="background:#006699;padding:6px;">
						<font color="#ffffff"><strong> &nbsp;  Welcome </strong> </font> 
					</div>
				</div> 
	   
	 <?php if($agent==""){?>
	 <div class="row">
			<div class="col-md-12">
			<p> Oops! Your agent/landlord is not currently using ARI. Refer him/her for better services  </p>
			<p>	
				<label class="control-label">Agent Name </label> 
				<input   class="form-control" type="text"   required id="agent_name" name="agent_name" value=""> 
			</p>
			<p>
				<label class="control-label">Email </label>
				<input   class="form-control" type="text"  id="email" name="email" value="">
			</p>			
			<p>
				<label class="control-label">Phone </label>
				<input   class="form-control" type="text"  id="phone" name="phone" value="">
			</p>			
			<p> 
				<button type="submit" class="btn green" id="save"> Submit   </button>
				&nbsp; &nbsp; &nbsp; <font id="error">     </font>
			</p> 
			</div>
		</div>
	 <?php }else{ ?>
	 
		<div class="row">
			 <div class="col-md-12">
			  <p> Your Agent is using ARI and you are listed under :</p>
				<ul>
				<?php foreach($agent->result() as $r){?>
					<li><h4> <?=$r->property_name?></h4> </i>
				<?php }?>
				</ul>
				<p>Want to view your rent statement? <a href="<?=base_url();?>tenantSignUp/statement"> Click here </a> </p>
			 </div>
		</div>
	 <?php }?>
	</div>
<p>  				</p>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>     
 

<div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">    Success Message </b> 
		</div>
	 <div class="modal-body">
		<div class="row">
			<div class="col-md-12">  
				<p id="success_msg">
				
				</p>
			</div>
		</div>    
	</div>
	<div class="modal-footer" >  
		<!--<button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> -->
		&nbsp;
	</div> 
</div>

  
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 
function validate_char(id)
{
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		$("#success").modal('toggle');  
        return false;
		}
    
} 
 
 
  $("#save").click(function(){     
	var email=$("input[name='email']").val();  
	var phone=$("input[name='phone']").val();
	var name=$("input[name='agent_name']").val(); 
	if(!name){ $("#error").html("<font color='red'>  Enter agent name </font>"); $("input[name='name']").focus(); return false;}
	if(!email){ $("#error").html("<font color='red'>  Enter email address </font>"); $("input[name='email']").focus(); return false;}
	if(!phone){ $("#error").html("<font color='red'>  Enter phone number </font>"); $("input[name='phone']").focus(); return false;}
	$.ajax(
	  {
		url:"<?=base_url();?>auth/referAgent",
		type:"POST",  
		data:{
			'phone':phone,
			'email':email,
			'name':name 
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#amount").val('');
			       $("#error").html("<font color='green'>   Thank you for referring "+name+" to ARI. We shall contact and sign him up for the service </font>");
				 setTimeout(function()
				{ 
					//window.location="<?=base_url();?>auth/tenant";
                },3000); 
			 }
			 else
			 {
				$("#error").html("<font color='red'>   Details not saved successfully </font>");
				//window.location="<?=base_url();?>auth/tenant";				
			 }
		}
  })
 });
 
 
</script> 