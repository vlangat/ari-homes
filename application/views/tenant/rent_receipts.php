<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		var tenant=$("#tenants").val();
		var pay_mode=$("#pay_mode").val();
		var receipt_no=$("#receipt_no").val();
		var amount=$("#amount").val();
		var pay_date=$("#pay_date").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
	//	if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		 if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
		 
	}
	function validate_edit()
	{    
		var name=$("#edit_user_name").val();
		var pay_mode=$("#edit_pay_mode").val();
		var amount=$("#edit_amount").val();
		var receipt_no=$("#edit_receipt_no").val();
		var pay_date=$("#edit_pay_date").val(); 
		if(name==""||name==null){ $("#edit_error1").html("<font color='red'> Tenant Name   is empty </font>");$("#edit_user_name").focus(); return false;}
		if(amount==""||amount==null){ $("#edit_error4").html("<font color='red'> Amount field is empty </font>"); $("#edit_amount").focus(); return false;}
		if(pay_date==""||pay_date==null){ $("#edit_error3").html("<font color='red'> Date field is empty</font>"); $("#edit_pay_date").focus(); return false;}
		//if(receipt_no==""||receipt_no==null){ $("#edit_error2").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>"); $("#edit_receipt_no").focus(); return false;}
		if(/^[a-zA-Z0-9- ]*$/.test(amount) == false){ $("#edit_amount").focus(); $("#edit_error4").html("<font color='red'>Amount contains illegal characters </font>");  return false; }else{$("#edit_error4").empty();}
	if(/^[a-zA-Z0-9- ]*$/.test(name) == false){ $("#edit_user_name").focus(); $("#edit_error1").html("<font color='red'>Name  should not have special characters </font>");  return false; } else{$("#edit_error1").empty();} 
	if(/^[a-zA-Z0-9- ]*$/.test(receipt_no) == false){ $("#edit_receipt_no").focus(); $("#edit_error2").html("<font color='red'>Receipt No should not have special characters </font>");  return false; } else{$("#edit_error2").empty();} 
	
		
		return confirm("Update details for "+name+"?");
	}
	</script>
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>tenantSignUp/statement/home"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Rent  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Receipts</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="col-md-12"    style="background:#006699;padding:8px;">
			 <font color="#ffffff">  Received Payment Details</font>
			</div>	
<div class="portlet light portlet-fit ">
	<div class="portlet-body">
	
	<hr/> 
 <!--<h4><strong>  Received Payment Details </strong> </h4>-->
<table class="table table-striped table-hover table-bordered"  id="sample_editable_1">
<thead>
	<tr>
		<th> &nbsp;#  </th> 
		<th> Date Paid </th>
		<th> Name </th>
		<th> Amount </th> 
		<th> Payment Mode </th> 
		<th> View </th>   
	</tr>
</thead>
<tbody>
  
  <?php $x=1;  $tenant_type=""; foreach($received_rent->result() as $rows):?>
	<tr>
		<td> <?php echo $x?><!--<input name="checkbox[]" type="checkbox" id="checkbox[]" value="">--> </td>
		<td class="center"> <?=$rows->date_paid?>  </td>
		<td> 
		<?php 	$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
				$enct =base64_encode(do_hash($rows->id . $salt,'sha512') . $salt);
				foreach($tenants->result() as $t){ if($t->id==$rows->tenant_id){$tenant_type=$t->tenant_type;} } if($tenant_type=="residential"){ $tenant_name=$t->last_name." ".$t->middle_name." ".$t->first_name;}else{ $tenant_name=$t->company_name;}
				echo $tenant_name; 
		   ?>
		</td>
		<td>  <?=$rows->amount?>   </td>  
		<td>
			<?=$rows->payment_mode?>
		</td>
		<td id="view_<?=$x?>">
		  <a href="<?=base_url();?>tenantSignUp/printTenantReceipt/<?=$enct?>"> <i class="fa fa-files-o" aria-hidden="true"></i> View Receipt</a>
		</td>
		 
	</tr>
	<?php $x++; endforeach;?>
</tbody>
</table>

</div> 
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div>
</div>
<!---Edit payment-->


<div id="edit_payment" class="modal fade" tabindex="-1" data-width="800" aria-hidden="true"> 

	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Edit Payment Details </b> </font></h4>
	 </div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
			<form action="<?=base_url();?>rent/receive_pay" method="post" onsubmit="return validate_edit()">
				<div class="col-md-6">
				<p> <label class="control-label"> Name </label> <br/>
							<input   class="form-control" type="hidden" id="editing_id" name="edit_id" >
							<input   class="form-control" type="hidden" id="editing_tenant_id" name="tenant_id" >
							<input   class="form-control" type="text" name="from"   id="edit_user_name"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" >
									   
								 
								<label id="edit_error1">					</label> 
						</p> 
						<p> <label class="control-label">Payment Method </label>
								<select class="form-control" type="text"  name="pay_mode" id="edit_pay_mode">
									<option value="Cash"> Cash </option>
									<option value="Cheque"> Cheque </option>
									<option value="Mpesa"> Mpesa </option>
									<option value="Bank Receipt"> Bank Receipt </option>
								</select> 
								<label >					</label> 
						</p>
						
						<p> 
						 <label class="control-label"> Date </label>
							<input type="text" required class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value=""   onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" name="date" id="edit_pay_date">
							<label id="edit_error3">					</label> 
						</p>
</div>
			<div class="col-md-6">	
						<p> <label class="control-label"> Amount Paid </label>
							<input   class="form-control" type="text" name="amount"  id="edit_amount" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);">
							<label id="edit_error4">					</label> 
						</p> 					
						 <p> <label class="control-label">Receipt/Cheque No </label>
							<input   class="form-control" type="text" name="receipt_no"  id="edit_receipt_no" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);">
							<label id="edit_error2">					</label>  
						</p> 
							 	 
			</div> 
		</div> 
		<div class="modal-footer" ><center> <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" <?=$disabled;?>> Save Changes </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
	</center>
</div>	
</form> 	 
</div>			
	</div>	

	</div> 
	

<!--->
 <!-- responsive -->
<div id="add_new" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			 
			<h5><b> Specify other Payment </b></h5>
			<hr/>
			<p>
			<label class="control-label">Name/Description </label>
			<input   class="form-control" type="text" placeholder="electricity" id="amenity_name"> </p>
			<p>
			</div>
		</div> 
	</div> 
<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_new_one" <?=$disabled;?>> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>


<div id="success" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b style="font-size:20px;color:green">    Warning Message </b></h5>
				<hr/>
				<p id="success_msg">
				   
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
 <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">    Success Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="">
				   <?php if($this->session->flashdata('temp')){ echo $this->session->flashdata('temp');}  ?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<!--<button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> -->
		&nbsp;
	</div> 
</div>
 
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 
function validate_char(id)
{
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		$("#success").modal('toggle');  
        return false;
		}
    
}

$(document).ready(function () { 

  checkPrivilege();
  var saving_success="<?php echo $this->session->flashdata('temp');?>";
 if(!saving_success)
 { }
else{
	$("#data_saving_success").modal('toggle');
	setTimeout(function(){
						 $("#data_saving_success").modal('hide'); 
                      }, 2000); 
      	
	}
	$("#add_new_one").click(function(){ 
	var name=$("#amenity_name").val();
 if(!name){ $("#status_message").html("<font> Name required </font>"); $("#amenity_name").focus();  return false;}
  $.ajax(
  {
		url:"<?=base_url();?>payment/add_new_amenity",
		type:"POST",  
		data:{
			'amenity_name':name
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#status_message").html("<font color='green'> Added successfully </font>");
				  setTimeout(function(){
								$('#add_new').modal('hide');
                               $("#status_message").empty();
                      }, 2000); 
					  document.location.reload(true);
			 }
			 else{
				 $("#status_message").html("<font color='red'> Not added. Try again later </font>");
			 }

		}			 
  })
 
 });

 });
  
  function add_other(value)
  {
	  if(value=="other")
	  {
		$("#add_new").modal('show');
	  }
  }
   
 
  
  function editPayment(id,name)
  {
	    
	  $.ajax(
		{
		url:"<?=base_url();?>rent/payment_details/"+id+"/"+name,
		type:"POST", 
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 var data=obj.data;
				for(var i=0; i<=data.length; i++)
				{
					var payment=data[i];
					var tenant_name=name;
					var no=payment['id'];  
					var amount=payment['amount'];
					var tenant_id=payment['tenant_id'];
					var pay_mode=payment['payment_mode']; 
					var code=payment['payment_mode_code'];  
					var date=payment['date_paid'];   
					$("#edit_pay_mode").val(pay_mode);
					$("#edit_pay_date").val(date);
					$("#edit_amount").val(amount);  
					$("#editing_id").val(no);  
					$("#editing_tenant_id").val(tenant_id);  
					$("#edit_receipt_no").val(code);  
					$("#edit_user_name").val(tenant_name);  
					$("#edit_payment").modal('show');					
				}
			 }
			 else
			 {
				alert('You cannot edit this receipt'); 
			 }
		}
  }
)
  }
  
 function disable_method()
  { 
	var value=document.getElementById("pay_mode").value;
	 if(value=="Cash")
	  {
		 
	document.getElementById('error5').value ="";
	document.getElementById('receipt_no').value ="--";
	document.getElementById('receipt_no').disabled = true;
      
	  }
	  else
	  {
			document.getElementById('receipt_no').value ="";
			document.getElementById('receipt_no').disabled = false;
	  }  
}

function checkPrivilege()
 { 
	 var i="<?=$x?>";
	$.ajax({
		url:"<?=base_url();?>Auth/checkPrivilege/3",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			 
			if(obj.add==0){ 
			   	document.getElementById('save').disabled=true;	  
			}
			if(obj.view==0){ 
			for(var x=1; x<i;x++){   $("#view_"+x).html("<font color='' onclick=\"alert('You have no privilege to view')\"> <a href='#' > View Receipt </a></font>"); }
              		  
			}
			 if(obj.edit==0){  
				for(var x=1; x<i;x++){   $("#edit_"+x).html("<font color='' onclick=\"alert('You have no privilege to edit')\"> <a href='#' ><i class='fa fa-edit'> </i> Edit   </a></font>"); }
             
			}
			if(obj.delete==0){ 
			    
			   //document.getElementById('confirm').disabled=true;   
			}
		}
	 })
 } 
</script> 