 			<!-- .subfooter start --> 
			<div class="subfooter">
				<div class="container">
					<div class="row">
					<div class="col-md-4">
					 
					<ul class="fh5co-footer-links">
						<li><a href="<?=base_url();?>auth/index">Home</a></li>
						<li><a href="<?=base_url();?>web/about">About</a></li>
					<li><a href="<?=base_url();?>web/how_it_works">Video Tutorials</a></li> 
						<li><a href="<?=base_url();?>web/pricing">Pricing</a></li>
						<li><a href="<?=base_url();?>web/contact">Contact Us</a></li> 
					</ul>
				</div>

				<div class="col-md-4">
					<!--<h3>Contact Us &amp;  </h3>-->
					<ul class="fh5co-footer-links">
						<li> Email: info@ari.co.ke  </li>
						<li> Cell: +254 725 992355 </li>
						<li> Cell: +254 780 338783 </li>
					</ul>
				</div>
 

				<div class="col-md-4">
					<!--<h3>Legal</h3>-->
					<ul class="fh5co-footer-links">
						<li>Suite 9, 1<sup> st</sup> Floor</a></li>
						<li>AACC | Sir Francis Ibiam House</li>
						<li>Waiyaki Way, Westlands</li> 
						<li>Nairobi, Kenya</li>
					</ul>
				</div>
					 
					</div>
				</div>
			</div>
			<!-- .subfooter end -->

		</footer>
		<!-- footer end -->
<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
		<!-- JavaScript --> 
		<script type="text/javascript" src="<?=base_url();?>ari_template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url();?>ari_template/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=base_url();?>ari_template/plugins/modernizr.js"></script>
		<script type="text/javascript" src="<?=base_url();?>ari_template/plugins/isotope/isotope.pkgd.min.js"></script>
		<script type="text/javascript" src="<?=base_url();?>ari_template/plugins/jquery.backstretch.min.js"></script>
		<script type="text/javascript" src="<?=base_url();?>ari_template/plugins/jquery.appear.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="<?=base_url();?>ari_template/js/custom.js"></script>

	</body>
</html>