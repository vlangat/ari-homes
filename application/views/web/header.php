<!DOCTYPE HTML>
<html>
<head profile="http://www.w3.org/2005/10/profile">
<link rel="icon" 
  type="image/png" 
  href="<?=base_url();?>images/login/favicon.png">
	
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ARI Homes | Real Estate Management Solution</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="The Best Real Estate Management System in Kenya" />
<meta name="keywords" content="property management, real estate, ARI Homes,rental, online poperty management system" />
<meta name="author" content="" />
<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico">

<link href="<?=base_url();?>ari_template/bootstrap/css/bootstrap.css" rel="stylesheet"> 
<link href="<?=base_url();?>ari_template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet"> 
<link href="<?=base_url();?>ari_template/css/animations.css" rel="stylesheet"> 
<link href="<?=base_url();?>ari_template/css/style.css" rel="stylesheet"> 
<link href="<?=base_url();?>ari_template/css/custom.css" rel="stylesheet">
</head>

<body class="no-trans" style="background:#fff url('<?=base_url();?>images/Banner.png');background-repeat:no-repeat;opacity:0.9;background-size:100%;padding-bottom:2px; background-position:relative;background-attachment:;">
<!-- scrollToTop --> 
<div class="scrollToTop"><i class="icon-up-open-big"></i></div>

<!-- header start --> 
<header class="header fixed clearfix navbar navbar-fixed-top">
	<div class="container">
		<div class="row">
			<div class="col-md-2">

				<!-- header-left start --> 
				<div class="header-left">

					<!-- logo -->
					<div class="logo smooth-scroll">
						<a href="<?=base_url();?>"><img id="logo" src="<?=base_url();?>images/login/ARI_Homes_Logo.png" height="100" style="height:80px;" alt="ARI Homes"></a>
					</div>

					<!-- name-and-slogan -->
					<!--<div class="logo-section smooth-scroll">
						<div class="brand"><a href="#banner">ARI Homes</a></div>								
					</div>-->

				</div>
				<!-- header-left end -->

			</div>
			<div class="col-md-9">

				<!-- header-right start --> 
				<div class="header-right">

					<!-- main-navigation start --> 
					<div class="main-navigation animated">

						<!-- navbar start --> 
						<nav class="navbar navbar-default" role="navigation">
							<div class="container-fluid">

								<!-- Toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>

								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
									<ul class="nav navbar-nav navbar-left">
										<li class="active"><a href="<?=base_url();?>">Home </a></li> 
										<li><a href="<?=base_url();?>web/about">About Us </a></li> 
										<li><a href="<?=base_url();?>web/how_it_works">Video Tutorials </a></li>
										<li><a href="<?=base_url();?>web/pricing">Pricing </a></li>
										 
										<li><a href="<?=base_url();?>web/contact">Contact Us </a></li>
										<li><a href="<?=base_url();?>">Sign Up /Log in </a></li>
									</ul>
								</div>

							</div>
						</nav>
						<!-- navbar end -->

					</div>
					<!-- main-navigation end -->

				</div>
				<!-- header-right end -->

			</div>
			<div class="col-md-1">
			
			</div>
		</div>
	</div>
</header>
<!-- header end -->