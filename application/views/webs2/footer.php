 <footer   id="myfooter"  style="background:#000;padding-top:8px;clear:both">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				 
				<div class="col-md-4">
					 
					<ul class="fh5co-footer-links">
						<li><a href="<?=base_url();?>auth/index">Home</a></li>
						<li><a href="<?=base_url();?>web/about">About</a></li>
					<!--	<li><a href="<?=base_url();?>web/how_it_works">How it Works</a></li>-->
						<li><a href="<?=base_url();?>web/pricing">Pricing</a></li>
						<li><a href="<?=base_url();?>web/contact">Contact Us</a></li> 
					</ul>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<!--<h3>Contact Us &amp;  </h3>-->
					<ul class="fh5co-footer-links">
						<li> Email: info@ari.co.ke  </li>
						<li> Cell: +254 725 992355</li>
						<li> Cell: +254 789 502 424 </li>
						<li> Web: www.arihomes.co.ke </li> 
					</ul>
				</div>
 

				<div class="col-md-4 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<!--<h3>Legal</h3>-->
					<ul class="fh5co-footer-links">
						<li>Suite 9, 1<sup> st</sup> Floor</a></li>
						<li>AACC | Sir Francis Ibiam House</li>
						<li>Waiyaki Way, Westlands</li> 
						<li>Nairobi, Kenya</li>
					</ul>
				</div>
			</div>
<!--
			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small> 
						<small class="block">Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a> &amp; <a href="https://www.pexels.com/" target="_blank">Pexels</a></small>
					</p>
				</div>
			</div>-->

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?=base_url();?>ari_template/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?=base_url();?>ari_template/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?=base_url();?>ari_template/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?=base_url();?>ari_template/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="<?=base_url();?>ari_template/js/jquery.stellar.min.js"></script>
	<!-- Carousel -->
	<script src="<?=base_url();?>ari_template/js/owl.carousel.min.js"></script>
	<!-- Flexslider -->
	<script src="<?=base_url();?>ari_template/js/jquery.flexslider-min.js"></script>
	<!-- countTo -->
	<script src="<?=base_url();?>ari_template/js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="<?=base_url();?>ari_template/js/jquery.magnific-popup.min.js"></script>
	<script src="<?=base_url();?>ari_template/js/magnific-popup-options.js"></script>
	<!-- Count Down -->
	<script src="<?=base_url();?>ari_template/js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="<?=base_url();?>ari_template/js/main.js"></script>
	

	<script>
  /*  var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
	*/
	 
 
  function get_pass()
	{   
		var email=	$("#reset_pass_email").val(); 
		if(email){ } else{$("#reset_pass_email").focus(); return false;}
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#reset_status").html("<font color='red'> Email provided is invalid"); return false;}
		return true; 
	}
	
 

	
function login() 
	{   
	  
		var password=$("#login_pass").val();
		var username=$("#username").val(); 
		//var userCaptcha=$("#userCaptcha").val(); 
		var email=username; 
		$("#login_msg").html("");
		if(username){ } else{$("#login_msg").html("<font color='red'> Please Enter username  </font>");$("#username").focus();  return false;}
		if(!password){ $("#login_msg").html("<font color='red'> Please provide your password </font>");$("#login_pass").focus(); return false; }
		//if(!userCaptcha || userCaptcha==""){ $("#userCaptcha").html("<font color='red'> Enter Image text  </font>");$("#userCaptcha").focus(); return false; }

		$("#login_msg").empty();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) 
		{ 
		alert("Invalid E-mail address");
		$("#username").focus();
		return false;
		}
		/*$("#login_msg").html("<div class = 'alert alert-success alert-dismissable' style='background:lightgreen;opacity:1.0;'>"+
		"<button type = 'button' class = 'close' data-dismiss = 'alert' aria-hidden = 'true'>&times;"+
		"</button> <font color='#006699'> Authenticating Please wait... </font></div>");
*/
}


	function getCaptcha() 
	{   
		 $.ajax({
		 url:"<?=base_url();?>auth/getCaptcha/",
		 type:"POST",
		 async:false, 
		 success:function(data)
		 {
			 var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
					$("#captcha_image").html(obj.image);
					$("#a_captcha_image").html(obj.image);
					$("#captcha_image_forgot").html(obj.image);
					$("#captcha_image_reset").html(obj.image);
					$("#captcha_name").val(obj.image);				 
					$("#captcha_name_forgot").val(obj.image);				 
					$("#captcha_name_reset").val(obj.image);				 
					$("#captcha_val").val(obj.val);  
					$("#a_captcha_val").val(obj.val);  
					$("#captcha_val_forgot").val(obj.val);  
					$("#captcha_val_reset").val(obj.val);  

			 }
			 else{
					$("#captcha_image").html(obj.image);
					$("#captcha_image_forgot").html(obj.image);
					$("#captcha_image_reset").html(obj.image);
					$("#captcha_name").val(obj.image);				 
					$("#captcha_name_forgot").val(obj.image);				 
					$("#captcha_name_reset").val(obj.image);				 
					$("#captcha_val").val(obj.val);  
					$("#captcha_val_forgot").val(obj.val);  
					$("#captcha_val_reset").val(obj.val);  
				 }
		 }})	  
} 


  function sign_up()
	{  
		var pass =	$("#pass").val();
		var fname =	$("#fname").val();
		var lname =	$("#lname").val();   	
		var email =	$("#email").val();   	
		var type =	$("#type").val();  
		var partner_code=$("#partner").val(); 
		var company=$("#company").val(); 
		var status =$("#status").val(); 
		var phone =$("#phone").val(); 
		var captcha_val =$("#captcha_val").val(); 
		var user_Captcha =$("#user_Captcha").val();
		var captcha_name=$("#captcha_name").val(); 
		if(!fname){  $("#error").html("<font color='red'> Provide your First Name</font>"); $("#fname").focus();   return false;   } else{ $("#error").empty();}
		if(!lname){    $("#error").html("<font color='red'> Provide your Last Name</font>"); $("#lname").focus(); return false;   } else{ $("#error").empty();} 
		if(!type){    $("#error").html("<font color='red'> Select register as</font>");  $("#type").focus(); return false; }  else{ $("#error").empty();} 
		if(type==2 && company==""){    $("#error").html("<font color='red'>  Company Name required if need to sign up as Agent</font>");  $("#company").focus(); return false; } else{ $("#error").empty();}  
		if(!email){   $("#error").html("<font color='red'> Provide your Email Address </font> ");  $("#email").focus(); return false;   }else{ $("#error").empty();}
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#error").html("<font color='red'> Invalid E-mail address"); $("#email").focus(); return false;}	
		if(!phone){   $("#error").html("<font color='red'> Provide your phone Number </font> ");  $("#phone").focus(); return false;   }else{ $("#error").empty();}
		if(phone.length <10){   $("#error").html("<font color='red'> Re-check your phone number </font> ");  $("#phone").focus(); return false;   }else{ $("#error").empty();}
		if(!pass){   $("#error").html("<font color='red'> Provide your password </font> ");  $("#pass").focus(); return false;   }else{ $("#error").empty();}
		if(pass.length<6){$("#error").html("<font color='red'> Password should be at least 6 characters </font>"); return false;}else{ $("#error").empty();}
		
		var x = document.getElementById("tnc").checked; 
		if(!user_Captcha){   $("#error").html("<font color='red'>Enter the captcha text  ");  $("#user_Captcha").focus(); return false;   }else{ $("#error").empty();}
		if(captcha_val ==user_Captcha){ } else{ $("#error").html("<font color='red'>Wrong  captcha text. Please re-enter  ");  $("#user_Captcha").focus(); return false;}
		if(x==false){$("#error").html("<font color='red'> Accept terms and conditions to proceed"); $("#tnc").focus(); return false;}else{ $("#error").empty();}
		return true;
	} 	
	</script>
	</body>
</html>
