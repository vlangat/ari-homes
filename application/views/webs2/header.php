<!DOCTYPE HTML>
<html>
	  <head profile="http://www.w3.org/2005/10/profile">
<link rel="icon" 
      type="image/png" 
      href="<?=base_url();?>images/login/favicon.png">
	    
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ARI Homes | Real Estate Management Solution</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="The Best Real Estate Management System in Kenya" />
	<meta name="keywords" content="property management, real estate, ARI Homes,rental, online poperty management system" />
	<meta name="author" content="" />
 
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	 
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?=base_url();?>ari_template/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?=base_url();?>ari_template/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?=base_url();?>ari_template/css/bootstrap.css">

	<!-- Magnific Popup -->
	<!--<link rel="stylesheet" href="<?=base_url();?>ari_template/css/magnific-popup.css">-->

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?=base_url();?>ari_template/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=base_url();?>ari_template/css/owl.theme.default.min.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?=base_url();?>ari_template/css/flexslider.css">

	<!-- Pricing -->
	<link rel="stylesheet" href="<?=base_url();?>ari_template/css/pricing.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?=base_url();?>ari_template/css/style.css">
<script src="<?=base_url();?>ari_template/js/jquery.min.js"></script>
	<!-- Modernizr JS -->
	<script src="<?=base_url();?>ari_template/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	
	<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
<style>iframe{padding-right:8px;border:2px solid #fff} </style>
	</head>
	
	<script type='text/javascript'>
function validate()
{
    var TCode = document.getElementById('company').value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) {

        alert('Company name field contains illegal characters');
       document.getElementById('company').value="";
        return false;
    }

    return true;     
}
</script>

<body style="min-height:300px;background:#fff url('<?=base_url();?>images/login/p2.png');background-repeat:no-repeat;opacity:0.9;background-size:100% 36%; background-position:relative;background-attachment:;">
<?php if(empty($data)){  $first_name=""; $phone=""; $last_name=""; $user_type=""; $email=""; $company=""; }
else{ 
		foreach($data->result() as $row)
		{
			$first_name=$row->first_name; $phone=$row->mobile_number; $last_name=$row->last_name;$user_type=$row->user_type;$email=$row->email;
		}  
	}
?> 	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="top">
  
	<div class="top-menu">
		<div class="container">
			<div class="row">
				<div class="col-xs-2">
					<div id="fh5co-logo"><a href="<?=base_url();?>web"> <img src="<?=base_url();?>images/login/ARI_Homes_Logo.png" height="130"></div>
				</div>
				<div class="col-xs-10 text-right menu-1">
					<ul>
						<li class="active"><a href="<?=base_url();?>web"> Home </a> | </li> 
						<li><a href="<?=base_url();?>web/about">About</a> |</li>
					 <li><a href="<?=base_url();?>web/how_it_works">How it works</a> |</li> 
						<li><a href="<?=base_url();?>web/pricing">Pricing</a> |</li>
						
						<!--<li class="has-dropdown">
							<a href="blog.html">Blog</a>
							<ul class="dropdown">
								<li><a href="#">Web Design</a></li>
								<li><a href="#">eCommerce</a></li>
								<li><a href="#">Branding</a></li>
								<li><a href="#">API</a></li>
							</ul>
						</li>-->
						<li><a href="<?=base_url();?>index.php/web/contact">Contact Us</a></li>
						<li>&nbsp; </li> 
						<li>&nbsp; </li> 
						<li>&nbsp; </li>
						<li>&nbsp; </li> 
					 </ul>
				</div>
			</div>
			
		</div>
	</div>
</nav>
<p>  </p>

</div>
</div> 

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true" data-width="300" data-height="200">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <?php  echo form_open_multipart('auth/getPassword/', 'onSubmit="return get_pass()"');?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter your E-mail address below to reset your password</h4>
      </div>
      <div class="modal-body">
        <div class="row">
		 <div class="col-md-12"> 	
		<p>
             <input class="form-control placeholder-no-fix"required="required" id="reset_pass_email" type="email" autocomplete="off" placeholder="Email" name="email" /> 
		</p> 
		 </div>
		<div class="col-md-4">
		 <font  id="captcha_image_forgot"><label for="captcha">   <?=$this->session->userdata('image');?> </label> </font> 
		 
		</div>
		<div class="col-md-8"> 	
				<input type="hidden"  name="captcha_name"  id="captcha_name_forgot" class="form-control" value='<?=$this->session->userdata('image');?>'/>
				<input type="hidden"  name="captcha_code" id="captcha_val_forgot" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
				<input type="text" autocomplete="off" required name="userCaptcha"  id="user_Captcha_forgot" class="form-control" placeholder="Enter the numbers" value=""/>
			</p> 
		  </div>
	<div class="col-md-12"> 	
		 
		 <font align="left">  Can't read the image?  &nbsp; <a href='javascript:;' onclick="getCaptcha()" style="color:#006699"><i class="fa fa-refresh"></i> Refresh </a> </font>  
	</div>
	</div>	  
      </div>
      <div class="modal-footer"><font id="reset_status">   </font>
	   <button type="submit" id="register-submit-btn" name="submit"class="btn btn-success"> Submit</button>
       
	   <button type="button" id="register-submit-btn"  class="btn btn-default" data-dismiss="modal">Close</button>
		
      </div>
	  <?php echo form_close(); ?>
    </div>

  </div>
</div>

<div id="activateAcc" class="modal fade" tabindex="-1" aria-hidden="true" data-width="300" data-height="200">
  <div class="modal-dialog"> 
    <!-- Modal content--> 
    <div class="modal-content">
	<?php   echo form_open_multipart('auth/acceptAccount/', 'onSubmit="return activate()"'); ?>
   <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">  Complete the details on this form to activate Account</h5>
	</div>
	<div class="modal-body">
		
	<div class="form-group">
	   
	<div class="row">
	<div class="col-md-6"><p>
		<input placeholder="First Name" id="a_fname" name="first_name" class="form-control" type="text" value="" onchange="checkChars('fname')"> </p>
	</div>
	<div class="col-md-6">
		<p><input placeholder="Last Name" id="a_lname" name="last_name" class="form-control" type="text" value="" onchange="checkChars('lname')"> </p>
	</div> 
		<div class="col-md-6">
		<div class="form-group">
			<p>
				<select class="form-control"  id="a_type" name="user_type" placeholder="Register As"> 
					<option value=""> --Register As--  </option>
					<option value="1">Landlord</option>
					<option value="2">Business (Estate Agent)</option>
					<!--<option value="3">Tenant </option>-->
				</select>
			</p>
			</div>
		</div>  
	<div class="col-md-6">
		<p><input placeholder="Company Name (Optional)" id="a_company" name="company" class="form-control" type="text"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
  		 
	<div class="col-md-6">
		<p><input placeholder="Email" id="a_email"  name="email" class="form-control" type="email" readonly value="<?=$email?>" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
	 <div class="col-md-6">
			<p><input type="number" autocomplete="off" name="phone"   id="a_phone" value="" class="form-control" placeholder="Phone Number" value="<?=$phone?>"/></p>
	 </div> 
	<div class="col-md-12"> 
	 <p><input placeholder="Password" id="a_pass" name="password" class="form-control" type="password"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
		
	</div>
	<div class="col-md-6"> 	  
		<font><label for="captcha"  id="a_captcha_image">   <?=$this->session->userdata('image');?>  </label> </font>
		<font style="float:right"><a href='javascript:;' style="color:blue" onclick="getCaptcha()"> <i class="fa fa-refresh"></i> Refresh  </a> </font> 
	 
	</div> 
		
			<div class="col-md-6"> 
				<div class="form-group">	
			<input type="text" autocomplete="off" name="userCaptcha"  id="a_user_Captcha" class="form-control"   placeholder="Enter the text shown " value=""/>
				<input type="hidden"  name="captcha_name"  id="a_captcha_name" class="form-control" value='<?=$this->session->userdata('image');?>'/>
				<input type="hidden"  name="captcha_code" id="a_captcha_val" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
				</div> 
			</div> 
		<div class="col-md-12"> <font   id="a_error">   </font> <hr/> </div>
		<div class="col-md-12"> 
				<div class="form-group">
				<input  id="a_status" name="status" value="0" class="form-control" type="hidden"/> 
				<label class="check">
				<input type="checkbox" id="a_tnc" name="tnc"/> Agree to the
				<a style="color:blue" href="#terms_n_condition" data-toggle="modal"> Terms & conditions </a>
				</label> 
				
				</div>
			</div>  
	</div>
   </div>  
    </div>
    <div class="modal-footer"> <font id="a_reset_status">   </font>
	
	 <input type="submit" value="Submit" id="a_register-submit-btn" class="btn btn-success uppercase">  
        
		<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                
    </div> 
    </div> 
	<?php 
	 echo form_close();
	?> 
	 
</div> 
</div> 


<div id="reset_pass" class="modal fade" tabindex="-1" aria-hidden="true" data-width="300" data-height="200">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

 <?php  echo form_open_multipart('auth/changePassword/', 'onSubmit="return change()"');?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h4 class="modal-title">Change your  Password </h4>
	</div> 
	<div class="modal-body" style="">  
		<input type="hidden"  id="curr_password" name="curr_password" value="" class="form-control" />  
		<input type="hidden"  name="email" value="<?=$this->session->flashdata('email');?>" class="form-control"/>  
		<div class="form-group">
			<label class="control-label"> New Password</label>
			<input type="password" required id="new_pass" name="new_pass"  class="form-control" />
		</div>
		<div class="form-group">
			<label class="control-label"> Verification Code (Sent via SMS)</label>
			<input type="number" required autocomplete="off" name="code" id="code"   class="form-control" onchange="checkChars('code')" />
		</div>

	  <div class="form-group">
			<p> <font  id="captcha_image_reset"><label for="captcha">   <?=$this->session->userdata('image');?>   </font>
		  <font align="left"> &nbsp; Can't read the image?  Click <a href='javascript:;' style="color:#006699" onclick="getCaptcha()">here </a> </font> </label></p>
		<input type="hidden"  name="captcha_name"  id="captcha_name_reset" class="form-control" value='<?=$this->session->userdata('image');?>'/>
		<input type="hidden"  name="captcha_code" id="captcha_val_reset" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
		 <input type="text" required autocomplete="off" name="userCaptcha"  id="user_Captcha_reset" class="form-control" placeholder="Enter the  above numbers" value=""/>
   		 
	</div>		
			
        </div> 
			<div class="modal-footer"><font style="float:left" id="reset_error"> <?=$this->session->flashdata('temp')?>  </font>
				<input type="submit" value="Submit" id="register-submit-btn" class="btn btn-success uppercase"> 
				<button type="button" data-dismiss="modal" id="register-submit-btn" class="btn dark btn-outline">Close</button>
			</div> 
		 
<?php echo form_close(); ?>
</div> 
</div> 
</div> 
     
	<script language="javascript">
 
$(document).ready(function()
{  
  
	window.history.forward(-1);
	var reset_pass="<?=$this->session->flashdata('reset_pass')?>"; 
	var password_resetting="<?=$this->session->flashdata('password_resetting')?>";
	 if(!password_resetting)
	 { 
		return false; 		
	 }
	 else
	 {  
		$("#curr_password").val(reset_pass);
		$("#reset_pass").modal('toggle'); 
	 }		
});

</script>


<script language="javascript"> 
function activate()
	{    
		var pass =	$("#a_pass").val();
		var fname =	$("#a_fname").val();
		var lname =	$("#a_lname").val();   	
		var email =	$("#a_email").val();   	
		var type =	$("#a_type").val();  
		var partner_code=$("#a_partner").val(); 
		var company=$("#a_company").val(); 
		var status =$("#a_status").val(); 
		var phone =$("#a_phone").val(); 
		var captcha_val =$("#a_captcha_val").val(); 
		var user_Captcha =$("#a_user_Captcha").val();
		var captcha_name=$("#a_captcha_name").val(); 
		if(!fname){  $("#a_error").html("<font color='red'> Provide your First Name</font>"); $("#a_fname").focus();   return false;   } else{ $("#a_error").empty();}
		if(!lname){    $("#a_error").html("<font color='red'> Provide your Last Name</font>"); $("#a_lname").focus(); return false;   } else{ $("#a_error").empty();} 
		if(!type){    $("#a_error").html("<font color='red'> Select register as</font>");  $("#a_type").focus(); return false; }  else{ $("#a_error").empty();} 
		if(type==2 && company==""){    $("#a_error").html("<font color='red'>  Company Name required if need to sign up as Agent</font>");  $("#a_company").focus(); return false; } else{ $("#a_error").empty();}  
		if(!email){   $("#a_error").html("<font color='red'> Provide your Email Address </font> ");  $("#a_email").focus(); return false;   }else{ $("#a_error").empty();}
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#a_error").html("<font color='red'> Invalid E-mail address"); $("#a_email").focus(); return false;}	
		if(!phone){   $("#a_error").html("<font color='red'> Provide your phone Number </font> ");  $("#a_phone").focus(); return false;   }else{ $("#a_error").empty();}
		if(phone.length <10){   $("#a_error").html("<font color='red'> Re-check your phone number </font> ");  $("#a_phone").focus(); return false;   }else{ $("#a_error").empty();}
		if(!pass){   $("#a_error").html("<font color='red'> Provide your password </font> ");  $("#a_pass").focus(); return false;   }else{ $("#a_error").empty();}
		if(pass.length<6){$("#a_error").html("<font color='red'> Password should be at least 6 characters </font>"); return false;}else{ $("#a_error").empty();}
		
		var x = document.getElementById("a_tnc").checked;
		if(!user_Captcha){   $("#a_error").html("<font color='red'>Enter the captcha text  ");  $("#a_user_Captcha").focus(); return false;   }else{ $("#a_error").empty();}
		if(captcha_val ==user_Captcha){ } else{ $("#a_error").html("<font color='red'>Wrong  captcha text. Please re-enter  ");  $("#a_user_Captcha").focus(); return false;}
		if(x==false){$("#a_error").html("<font color='red'> Accept terms and conditions to proceed"); $("#a_tnc").focus(); return false;}else{ $("#a_error").empty();}
		return true;
	} 
	
$(function()
{     
	//var activate="<?=$this->session->flashdata('activate');?>"; 
	var fname="<?=$first_name?>"; 
	var lname="<?=$last_name?>"; 
	var email="<?=$email?>"; 
	var user_type="<?=$user_type?>";  
	var company="<?=$company?>";  
	var phone="<?=$phone?>";  
	if(!email||email==null)
	{
	  return false;
	}
	else
	{ 
		 $("#a_fname").val(fname);
		 $("#a_lname").val(lname);   	
		 //$("#a_email").val(email);   	
		 $("#a_phone").val(phone);   	
		 $("#a_company").val(company); 
		 $("#a_type").val(user_type); 
		 $("#a_status").val(1); 
		 document.getElementById('a_fname').disabled = true;
		 document.getElementById('a_lname').disabled = true;
		 document.getElementById('a_phone').disabled = true;
		 document.getElementById('a_company').disabled = true;
		 document.getElementById('a_type').disabled = true;
		// document.getElementById('a_email').disabled = true;
		 $('#activateAcc').modal('show');
	}

});

  function change()
	{  
		var email="<?php echo $this->session->userdata('email');?>";
		var curr_pass=$("#curr_password").val();
		var new_pass=$("#new_pass").val();
		var code=$("#code").val();  	
		if(!new_pass){  $("#reset_error").html("<font color='red'> Enter new password </font>"); $("#new_pass").focus(); return false;}else{ $("#reset_error").empty();}
		if(!code){	$("#reset_error").html("<font color='red'> Enter verification code </font>"); $("#code").focus(); return false;}else{ $("#reset_error").empty();}
		return true;
	}
</script>