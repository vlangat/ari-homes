 
<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Venit | Property Management</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES --> 
        <link href="<?=base_url();?>template/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=base_url();?>template/theme/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
		 <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/pages/css/login-5.css" rel="stylesheet" type="text/css" />
       <!-- <link href="<?=base_url();?>template/theme/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />-->
        <!--<!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <!--<link rel="shortcut icon" href="favicon.ico" />  --->
<script type='text/javascript'>
function checkChars(id)
{
   var TCode = document.getElementById(id).value;
   if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false)
   {
		//alert(id+' contains illegal characters');
		$("#error_msg").html("<font color='red'>  Input you have provided contains illegal characters.Only [Aa-Zz] or [0-9] required</font>"); 
		 document.getElementById(id).value="";
		$("#text_field_error").modal('toggle'); 
        return false;
   }
    return true;     
}
</script>
<script type='text/javascript'>
function refreshCaptcha(){
	var img = document.images['captchaimg'];
	img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}
</script>
<script language="javascript" type="text/javascript">       
  window.history.forward();     
</script>  

</head>
    <!-- END HEAD -->
<body class=" login" onload="history.go(1);">
        <!-- BEGIN : LOGIN PAGE 5-1 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-5 bs-reset">
                    <div class="login-bg" style="background-image:url('<?=base_url();?>images/login/p1.png')">
                         <a href="https://ari.co.ke/">  <img class="login-logo" src="<?=base_url()?>images/login/ARI-Logo.png" width="150"/> </a>

					</div>
                </div>
                <div class="col-md-7 login-container bs-reset">

<p> </p>
<div style="padding-left:55px;color:#a0a9b4"> 
&nbsp; &nbsp; &nbsp; <a href="https://ari.co.ke/" style="color:#a0a9b4">Home</a> |	<a href="https://ari.co.ke/howitworks" style="color:#a0a9b4">How it works</a> | <a href="https://ari.co.ke/pricing" style="color:#a0a9b4">Pricing</a> | <a href="https://ari.co.ke/becomeapartner" style="color:#a0a9b4"> Become a partner </a> |	<a href="https://ari.co.ke/our-location" style="color:#a0a9b4"> Contacts</a>	 | <a href="https://ari.co.ke/online/Auth/#signup" id="sign_up_page" style="color:#a0a9b4">Sign Up </a> 
 </div> 
                    <div class="login-content">
					
                        <h1>ARI Admin Login</h1>
                        <p> Login to manage your properties and tenants, contact us by sending an email to support[at]ari.co.ke  for any assistance. </p><p> If you do not have an account, click on create an account</p>
<p> 	<a   data-toggle="modal" id="create_acc"><button  type="button"  class="btn green-dark"> Create an account </button></a> </p>
<div  class="login-form" >

                   <h4>  Login </h4>    

                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Enter any username and password. </span>
                            </div>
                            <div class="row"> 
 <p id="login_msg">  
 <?php if($this->session->flashdata('temp')){
					 $msg=$this->session->flashdata('temp');
										 
				echo '<div class="alert alert-success" style="opacity:1.0;">
					   <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
						  &times;
					   </button>
						
					   <font color="#000000">'. $msg. '</font>
					</div>'; 	
					}
									
				?>

 </p>
 <?php  echo form_open_multipart('Auth/login/', 'onSubmit="return login()"');?>
			<div class="col-xs-6">
				<input class="form-control form-control-solid placeholder-no-fix form-group" type="email"  id="username"    placeholder="Email" name="username" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" required  /> 
			</div>
			<div class="col-xs-6">
				<input class="form-control form-control-solid placeholder-no-fix form-group" type="password" id="login_pass" autocomplete="off"   placeholder="Password" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" name="password" required/>
			</div>
		</div>
<div class="row">
<div class="col-sm-6">
	<div class="rem-password"> 	 
	<label for="captcha"> <?=$this->session->userdata('image');?> </label>
	<label for="captcha"> Can't read the image? Click <a href="<?=base_url();?>">here </a> to refresh</label>
	</div>
</div>
  <div class="col-sm-6">
    <input type="hidden"  name="captcha_name"  id="login_captcha_name" class="form-control" value='<?=$this->session->userdata('image');?>'/>
    <input type="hidden"  name="captcha_code" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
    <input type="text" autocomplete="off" name="userCaptcha" required id="userCaptcha" class="form-control" placeholder="Enter the Image Text" required value=""/>
  </div> 
	<div class="col-sm-12 text-right">
		<div class="forgot-password">
		  <a href="#forget_pass_modal" data-toggle="modal"   class="forget-password">Forgot Password?</a> 
		</div>
		<button class="btn red"  id="login" type="submit">Sign In</button>
	</div>
</div> 
                           
				<?php echo form_close(); ?>
			</div> 
	  <hr/>
		<div class="login-footer">
			<div class="row bs-reset"> 
				<div class="col-xs-7 bs-reset">
					<div class="login-copyright text-center">
						<p>Copyright &copy; ARI <?php echo date("Y");?></p>
					</div>
				</div>
			</div>
		</div> 
	</div>
</div>
             
	</div>
</div>
 
	 	
        <!-- END : LOGIN PAGE 5-1 -->
		 <!-- responsive -->
<div id="signup" class="modal fade" tabindex="-1" data-width="620">
<div class="modal-header" style="background:aliceblue">
	<!--<button type="button"   class="close" data-dismiss="modal" aria-hidden="true" style="color:#000">  &times </button>-->
   <font class="modal-title" size="4"><b> Join ARI today </b>  &nbsp; &nbsp; <font  size="2" align="right">  Have an account? <a href="<?=base_url();?>"> Login </a>  &nbsp; &nbsp; &nbsp; &nbsp; </font> </font><font id="close"    style="float:right;font-size:18px;color:#000"> <a href="#"> &times;</a> </font>
</div>
<div class="modal-body"> 
<div class="row">
	<?php //echo form_open('Auth/signUp/');?> 
	<?php  echo form_open_multipart('Auth/signUp/', 'onSubmit="return sign_up()"');?>
<div class="col-md-12"> 
	<div class="col-md-6"><p>
		<input placeholder="First Name" id="fname" name="first_name" class="form-control" type="text" onchange="checkChars('fname')"> </p>
	</div>
	<div class="col-md-6">
		<p><input placeholder="Last Name" id="lname" name="last_name" class="form-control" type="text" onchange="checkChars('lname')"> </p>
	</div>
</div> 
<div class="col-md-12">    
		<div class="col-md-6">
		<div class="form-group">
			<p>
				<select class="form-control"  id="type" name="user_type" placeholder="Register As"> 
					<option value=""> --Register As--  </option>
					<option value="1">Individual</option>
					<option value="2">Business (Estate Agent)</option>
				</select>
				</p>
			</div>
		</div>  
	<div class="col-md-6">
		<p><input placeholder="Company Name (Optional)" id="company" name="company" class="form-control" type="text"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
</div> 		
 	
<div class="col-md-12"> 
	<div class="col-md-6">
		<p><input placeholder="Email" id="email"  name="email" class="form-control" type="email"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
	 <div class="col-md-6">
			<p><input type="number" autocomplete="off" name="phone"   id="phone" class="form-control" placeholder="Phone Number" value=""/></p>
	 </div>
</div>  
<div class="col-md-12"> 
		<div class="col-md-6"> 
		 <p><input placeholder="Password" id="pass" name="password" class="form-control" type="password"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
			
		 </div>
		<div class="col-md-3"> 	 					 
			<p  id="captcha_image"><label for="captcha">   <?=$this->session->userdata('image');?>  </label></p> 
		</div>
	<div class="col-md-3">
		<p>
		<input type="hidden"  name="captcha_name"  id="captcha_name" class="form-control" value='<?=$this->session->userdata('image');?>'/>
		<input type="hidden"  name="captcha_code" id="captcha_val" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
			<input type="text" autocomplete="off" name="userCaptcha"  id="user_Captcha" class="form-control" required placeholder="Enter number" value=""/>
   		</p>	
	 </div>
</div>
		<div class="col-md-12"> 
			<div class="col-md-6"> 
					&nbsp;
			</div>
			<div class="col-md-6"> 
				<label>Can't read the image?  Click <a href='javascript:;' onclick="getCaptcha()">here </a>  </label>
			</div>
		</div>
<div class="col-md-12">  
	<div class="col-md-12">
		<input  id="status" name="status" value="0" class="form-control" type="hidden"> 
		<div class="form-group margin-top-10 margin-bottom-10">
			<label class="check">
			<input type="checkbox"   id="tnc" name="tnc" /> Agree to the
			<a href="javascript:;"> Terms    & conditions </a>
			</label> 
			<p id="error">   </p>
		</div>
	</div>
</div>
<div class="col-md-12"> 
	<div class="col-md-4">
	&nbsp;
	</div>	 
	<div class="col-md-4"> 
		<p> <input type="submit" class="btn blue btn-block" value=" Sign Up "></p>
	</div>
	<div class="col-md-4"> 
	&nbsp;
	</div>
</div>
<?php echo form_close(); ?>
<div class="col-md-12"> 
	<div class="col-md-12">
		<font size="1" color="#000000">
			By signing up you agree to the <a href="javascript:;"> ARI  Terms  & conditions </a> 
		</font> 
</div>                                     									  
 
</div>

</div>
</div>
</div>

<div id="forget_pass_modal" class="modal fade" tabindex="-1" aria-hidden="true" data-width="500" data-height="200">
  <?php  echo form_open_multipart('Auth/getPassword/', 'onSubmit="return get_pass()"');?>
	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">  Enter your E-mail address below to reset your password </h5>
	</div>
	<div class="modal-body" style="height:86px">
		 
		 <div class="col-md-12"> 	
		<p>
             <input class="form-control placeholder-no-fix"required="required" id="reset_pass_email" type="email" autocomplete="off" placeholder="Email" name="email" /> 
		</p> 
		 </div>
		<div class="col-md-4">
		 <font  id="captcha_image_forgot"><label for="captcha">   <?=$this->session->userdata('image');?> </label> </font> 
		 
		</div>
		<div class="col-md-8"> 	
				<input type="hidden"  name="captcha_name"  id="captcha_name_forgot" class="form-control" value='<?=$this->session->userdata('image');?>'/>
				<input type="hidden"  name="captcha_code" id="captcha_val_forgot" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
				<input type="text" autocomplete="off" required name="userCaptcha"  id="user_Captcha" class="form-control" placeholder="Enter the numbers" value=""/>
			</p> 
		  </div>
	<div class="col-md-12"> 	
		 
		 <font align="left">  Can't read the image?  Click <a href='javascript:;' onclick="getCaptcha()">here </a> </font>  
	</div>
		  
    </div>
    <div class="modal-footer"> <font id="reset_status">   </font>
		<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            <input type="submit"  name="submit"class="btn green"value="Submit">     
    </div> 
	<?php echo form_close(); ?>
</div> 


<div id="welcome_dialog" class="modal fade" tabindex="-1" aria-hidden="true"> 
		<div class="modal-header"> 
			<h4 class="modal-title">  <strong> ARI Property Management </strong></h4>
		</div> 
		<div class="modal-body" style="height:86px">
			<h5 class="dialog_message">   </h5>
        </div>
        <div class="modal-footer">  
	<center>	<button type="button" data-dismiss="modal" class="btn green ">&nbsp;  OK &nbsp;</button> </center>
              
    </div>
</div> 
      
<div id="reset_pass" class="modal fade" tabindex="-1" aria-hidden="true">
 <?php  echo form_open_multipart('Auth/changePassword/', 'onSubmit="return change()"');?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h4 class="modal-title">Change your  Password </h4>
	</div> 
	<div class="modal-body" style="">  
		<input type="hidden"  id="curr_password" name="curr_password" value="" class="form-control" />  
		<input type="hidden"  name="email" value="<?=$this->session->flashdata('email');?>" class="form-control"/>  
		<div class="form-group">
			<label class="control-label"> New Password</label>
			<input type="password" required id="new_pass" name="new_pass"  class="form-control" />
		</div>
		<div class="form-group">
			<label class="control-label"> Verification Code (Sent via SMS)</label>
			<input type="text" required autocomplete="off" name="code" id="code"   class="form-control" onchange="checkChars('code')" />
		</div>

	  <div class="form-group">
			<p> <font  id="captcha_image_reset"><label for="captcha">   <?=$this->session->userdata('image');?>   </font>
		  <font align="left"> &nbsp; Can't read the image?  Click <a href='javascript:;' onclick="getCaptcha()">here </a> </font> </label></p>
		<input type="hidden"  name="captcha_name"  id="captcha_name_reset" class="form-control" value='<?=$this->session->userdata('image');?>'/>
		<input type="hidden"  name="captcha_code" id="captcha_val_reset" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
		 <input type="text" required autocomplete="off" name="userCaptcha"  id="user_Captcha" class="form-control" placeholder="Enter the  above numbers" value=""/>
   		 
	</div>		
			
			<div class="modal-footer"><font style="float:left" id="reset_error"> <?=$this->session->flashdata('temp')?>  </font>
				<input type="submit"  class="btn green"value="Submit"/> 
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
			</div> 
        </div> 
		
	</div>
<?php echo form_close(); ?>
</div> 
     

<div id="text_field_error" class="modal fade" tabindex="-1" data-width="450">
	  <div class="modal-body">
			<div class="row">
				<div class="col-md-12"> 
				<h5> <b> Venit Message </b> </h5>
				<hr/>
					<p id="error_msg">
					  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>            
<?php if(empty($data)){  $first_name=""; $last_name="";$user_type=""; $email=""; $company="";  }
else {
		foreach($data->result() as $row)
		{
			$first_name=$row->first_name; $last_name=$row->last_name;$user_type=$row->user_type;$email=$row->email;
		}  
	}
	?>                       									  
                             									  
   
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/login-5.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/login-5.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
		   <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS --> 
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>

<script language="javascript">
 
$(document).ready(function()
{  
	 
  window.history.forward(-1);
 
	 var reset_pass="<?=$this->session->flashdata('reset_pass')?>";
         var password_resetting="<?=$this->session->flashdata('password_resetting')?>";
	 if(!password_resetting){
		 
		return false; 
		
	 }
	 else
	 {  
		$("#curr_password").val(reset_pass);
		 $("#reset_pass").modal('toggle'); 
	 }		
});

</script>

<script language="javascript"> 

 $(document).keypress(function(e){
    if(e.which == 13){
		login(); 
    }
});


function getCaptcha() 
	{  
		 
		 $.ajax({
		 url:"<?=base_url();?>Auth/getCaptcha/",
		 type:"POST",
		 async:false, 
		 success:function(data)
		 {
			 var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
					$("#captcha_image").html(obj.image);
					$("#captcha_image_forgot").html(obj.image);
					$("#captcha_image_reset").html(obj.image);
					$("#captcha_name").val(obj.image);				 
					$("#captcha_name_forgot").val(obj.image);				 
					$("#captcha_name_reset").val(obj.image);				 
					$("#captcha_val").val(obj.val);  
					$("#captcha_val_forgot").val(obj.val);  
					$("#captcha_val_reset").val(obj.val);  

			 }
			 else{
					$("#captcha_image").html(obj.image);
					$("#captcha_image_forgot").html(obj.image);
					$("#captcha_image_reset").html(obj.image);
					$("#captcha_name").val(obj.image);				 
					$("#captcha_name_forgot").val(obj.image);				 
					$("#captcha_name_reset").val(obj.image);				 
					$("#captcha_val").val(obj.val);  
					$("#captcha_val_forgot").val(obj.val);  
					$("#captcha_val_reset").val(obj.val);  
				 }
		 }
		 })	  
} 
 function sign_up()
	{    
		var pass =	$("#pass").val();
		var fname =	$("#fname").val();
		var lname =	$("#lname").val();   	
		var email =	$("#email").val();   	
		var type =	$("#type").val();  
		var partner_code=$("#partner").val(); 
		var company=$("#company").val(); 
		var status =$("#status").val(); 
		var phone =$("#phone").val(); 
		var captcha_val =$("#captcha_val").val(); 
		var user_Captcha =$("#user_Captcha").val();
		var captcha_name=$("#captcha_name").val(); 
		if(!fname){  $("#error").html("<font color='red'> Provide your First Name</font>"); $("#fname").focus();   return false;   } else{ $("#error").empty();}
		if(!lname){    $("#error").html("<font color='red'> Provide your Last Name</font>"); $("#lname").focus(); return false;   } else{ $("#error").empty();} 
		if(!type){    $("#error").html("<font color='red'> Select register as</font>");  $("#type").focus(); return false; }  else{ $("#error").empty();} 
		if(type==2 && company==""){    $("#error").html("<font color='red'>  Company Name required if need to sign up as Agent</font>");  $("#company").focus(); return false; } else{ $("#error").empty();}  
		if(!email){   $("#error").html("<font color='red'> Provide your Email Address </font> ");  $("#email").focus(); return false;   }else{ $("#error").empty();}
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#error").html("<font color='red'> Invalid E-mail address"); $("#email").focus(); return false;}	
		if(!phone){   $("#error").html("<font color='red'> Provide your phone Number </font> ");  $("#phone").focus(); return false;   }else{ $("#error").empty();}
		if(phone.length <10){   $("#error").html("<font color='red'> Re-check your phone number </font> ");  $("#phone").focus(); return false;   }else{ $("#error").empty();}
		if(!pass){   $("#error").html("<font color='red'> Provide your password </font> ");  $("#pass").focus(); return false;   }else{ $("#error").empty();}
		if(pass.length<6){$("#error").html("<font color='red'> Password should be at least 6 characters </font>"); return false;}else{ $("#error").empty();}
		
		var x = document.getElementById("tnc").checked;
		if(!user_Captcha){   $("#error").html("<font color='red'>Enter the captcha text  ");  $("#user_Captcha").focus(); return false;   }else{ $("#error").empty();}
		if(captcha_val ==user_Captcha){ } else{ $("#error").html("<font color='red'>Wrong  captcha text. Please re-enter  ");  $("#user_Captcha").focus(); return false;}
		if(x==false){$("#error").html("<font color='red'> Accept terms and conditions to proceed"); $("#tnc").focus(); return false;}else{ $("#error").empty();}
		return true;
	} 
 

 function get_pass()
	{   
	 
		var email=	$("#reset_pass_email").val(); 
		if(email){ } else{$("#reset_pass_email").focus(); return false;}
		var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#reset_status").html("<font color='red'> Email provided is invalid"); return false;}
		return true; 
	}
	
 
 function change()
	{  
		var email="<?php echo $this->session->userdata('email');?>";
		var curr_pass=$("#curr_password").val();
		var new_pass=$("#new_pass").val();
		var code=$("#code").val();  	
	   if(!new_pass){  $("#reset_error").html("<font color='red'> Enter new password </font>"); $("#new_pass").focus(); return false;}else{ $("#reset_error").empty();}
	   if(!code){	$("#reset_error").html("<font color='red'> Enter verification code </font>"); $("#code").focus(); return false;}else{ $("#reset_error").empty();}
		return true;
	}
	
function login() 
	{   
	 
		var password=$("#login_pass").val();
		var username=$("#username").val(); 
		var userCaptcha=$("#userCaptcha").val();

		var email=username; 
		$("#login_msg").html("");
		if(username){ } else{$("#login_msg").html("<font color='red'> Please Enter username  </font>");$("#username").focus();  return false;}
		if(!password){ $("#login_msg").html("<font color='red'> Please provide your password </font>");$("#login_pass").focus(); return false; }
		if(!userCaptcha || userCaptcha==""){ $("#userCaptcha").html("<font color='red'> Enter Image text  </font>");$("#userCaptcha").focus(); return false; }

		$("#login_msg").empty();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#login_msg").html("<font color='red'> Invalid E-mail address"); return false;}
		$("#login_msg").html("<div class = 'alert alert-success alert-dismissable' style='background:lightgreen;opacity:1.0;'>"+
		"<button type = 'button' class = 'close' data-dismiss = 'alert' aria-hidden = 'true'>&times;"+
		"</button> <font color='#006699'> Authenticating Please wait... </font></div>");

} 
 
$(function()
{   
	//var activate="<?=$this->session->flashdata('activate');?>"; 
	var fname="<?=$first_name?>"; 
	var lname="<?=$last_name?>"; 
	var email="<?=$email?>"; 
	var user_type="<?=$user_type?>";  
	var company="<?=$company?>";  
	if(!email||email==null)
	{
	  return false;
	}
	else
	{ 
		 $("#fname").val(fname);
		 $("#lname").val(lname);   	
		 $("#email").val(email);   	
		 $("#company").val(company); 
		 $("#type").val(user_type); 
		 $("#status").val(1); 
		 document.getElementById('fname').disabled = true;
		 document.getElementById('lname').disabled = true;
		 document.getElementById('phone').disabled = true;
		 document.getElementById('company').disabled = true;
		 document.getElementById('type').disabled = true;
		 document.getElementById('email').disabled = true;
		 $('#signup').modal('show');
	}

});


$(function(){
 
		var x=document.URL;
        if (x=="https://ari.co.ke/online/Auth/#signup") { 
 		    $("#signup").modal({backdrop: "static"}); 
        } 
		
		$("#create_acc").click(function()
		{
			$("#signup").modal({backdrop: "static"}); 
		});
		
		$("#sign_up_page").click(function()
		{
			$("#signup").modal({backdrop: "static"}); 
		});
		
       $("#close").click(function()
		{
			window.location = "https://ari.co.ke/review/Auth/";
		});
	});
	
  
 
</script>