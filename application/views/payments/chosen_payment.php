<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Packages </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Payment Checkout</span>
</li>
</ul> 
 
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
		<div style="background:#1bb968;padding:5px;">
			 <font color="#ffffff"><h4> <strong> Your preferred Payment method is <font id="method"> <?= strtoupper($mode)?> </font> </strong> </h4> </font> </strong> </font>
		</div> 
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
	<div class="portlet light portlet-fit "> 
	 <input type="hidden" id="title" value="">
			 
<div class="row">
			<div class="col-md-12"><h4>  &nbsp;<strong>  Choose a different payment method  </strong>  </h4> </div>
		<div class="col-md-12">
		  <div class="col-md-4">  
			<div class="radio-list">
						<label class="radio-inline">
						&nbsp; <input type="radio" name="pay_mode"  value="mpesa"  onclick="getData('mpesa')" id="mpesa"> <img src="<?=base_url();?>images/mpesa.png" height="75" width="150"> 
					</label>
			</div>
		  </div>
		<div class="col-md-4">
		<div class="radio-list">
			<label class="radio-inline">
				<input type="radio"    value="gtbank"  name="pay_mode" onclick="getData('gtbank')" id="gtbank"> <img src="<?=base_url();?>images/GTBank.png" height="75" width="150"> 
			</label>
			</div>
		  </div> 
	</div>
</div>

<?php $x=0; foreach($previous_pay->result() as $r){ $x++; } ?>
 
		 <div class="row">
	<div class="col-md-6 col-sm-6">
   <div class="portlet light "> 
			<div class="portlet-body">
			<p>   </p>
			<div id="content" style="width:100%">
<table border="0" width="100%"   id="chosen_payment"   style="border:1px solid lightgrey"> 
<tr>	<td colspan="2">
<p>  </p>		
			<ul style="padding:0px;list-style-type:none">
			<li>	<strong> Invoice No	# <?=$this->session->userdata('invoice_no')?> </strong>	</li>
			<li>	<strong> <font color="red"> UNPAID	</font> </strong> </li>
			<li>	&nbsp;	</li>
			<li>	<strong> Invoiced To	</strong></li>
			<li>	&nbsp;	</li>
			<li>	<?=strtoupper($this->session->userdata('first_name'))?> <?=strtoupper($this->session->userdata('last_name'))?>	</li>
			<li>	<?=$this->session->userdata('email')?>	</li>
			<li>	<?=$this->session->userdata('company_name')?> </li>
			<li> 	Nairobi, Kenya	</li>
			</ul>
		</td>
	</tr>	
		<tr   bgcolor="#32c5d2"> 
		<td colspan="2">  <strong id="strong">  Invoice Items </strong>   </td>
		</tr>
		<tr  > <td>      
		<?=$this->session->userdata('years');?> Year(s), <?=$this->session->userdata('users');?> User(s)  &nbsp; </td> <td align="right"> KES <?=$this->session->userdata('total_package_bill');?>  <br/> 
						
		</td></tr>
		<tr > <td><?=$this->session->userdata('total_sms');?>  SMS </td> <td align="right">KES  <?=($sms_cost=$this->session->userdata('total_sms_cost'));?> </td></tr>
		<tr > <td><b> Sub Total </b></td> 
		<td align="right"><b>KES  <?=($sub_total=$this->session->userdata('total_sub_total')+$sms_cost);?> </b> </td> </tr>   
		<tr > <td> VAT TAX @16%  </td> <td align="right"> KES <?=$vat=$this->session->userdata('total_vat');?> </td></tr>
		<?php if($credit<0){ $credit=0-$credit;} ?>
		<tr > <td> Credit </td> <td align="right"> KES <?=$credit?> </td></tr>
		<tr height="20"> <td><strong>  Total </strong> </td> <td align="right"><h4> <b>  KES <?=($vat+$sub_total-$credit);?> </h4>  </b>  </td></tr>
	</table><?php $total=$vat+$sub_total-$credit;?>
			 <p>    </p>
			 </div>
			 <div id="editor"></div>
			 <a  href="javascript:;"  class="btn grey" id="btn_invoice">   PRINT INVOICE  </a> 
		 </div>
		</div>
		 
		</div>
		
	<div class="col-md-6 col-sm-6"> 
		<div class="portlet light "> 
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12"> 
					<p>   </p>
					<?php if($total>0){?>
			<form action="#" method="post" onSubmit="return false">
				<table border="0" width="100%" cellpadding="30" style="border:1px solid lightgrey"> 
					<tbody> 
					<tr align="center" id="mpesa_logo" style="display:none"> 
						<td> 
							  <img src="<?=base_url();?>images/mpesa.png" > 						
						</td>
					</tr> 
					 
					<tr height="40" id="mpesa_pay" style="display:none"> <td align="left" > 
						   <ul style="list-style-type:none;padding:12px">
								<li>	&nbsp;	</li>
								<li>	Due Date :<?php echo date("d-m-Y");?> </li>
								<li>	&nbsp;	<hr/></li>
								<li>	<strong> From your Mobile Phone </strong>	</li>
								<li>	   &nbsp;	  </li>
								<li>	  Go to <strong>  MPESA Menu  </strong>	</li>
								<li>	   &nbsp;	  </li> 
								<li>	  Select <strong> Lipa na Mpesa </strong> 	</li>
								<li>	   &nbsp;	  </li>
								<li>	  Select <strong> Pay Bill  </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	  Enter the Business No: <strong> 990850</strong> 	</li>
								<li>	   &nbsp;	  </li>
								<li>	 Enter Account number:  <strong> <?=$this->session->userdata('invoice_no');?>  </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	   Enter  Amount: <strong>KES  <?=$total?> </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	   Enter your MPESA PIN, Click Ok 	</li>
								<li>	   &nbsp;	  </li>	
								<li>	   Confirm by Clicking Ok   	</li>
								<li>	   &nbsp;	  </li>   
							</ul>
						</td>
						</tr>
						<tr id="gtbank_logo" height="40"  style="display:none">
						<td align="center" style="padding:10px">  <img src="<?=base_url();?>images/GTBank.png" height="60" width="120">  </td>
						</tr>
						<tr id="gtbank_pay" height="40" style="display:none">
						     <td align="left">    
								<ul style="list-style-type:none;padding:12px">
								<li>	<h4> <b> PAY TO: </b> </h4>	</li>
								<li>	&nbsp;	</li>
								<li>	Account Name: <strong> ARI Limited	</strong> </li>
								<li>	   &nbsp;	</li> 
								<li>	Account No :  <strong>  21300036452	</strong> </li>
								<li>	   &nbsp;	</li> 
								<li>	Branch Name: <strong> Westlands </strong> 	</li>
								<li>	   &nbsp;	</li> 
								<li>	 BIC/SWIFT: <strong>  GTBIKENA </strong> </li>
								<li>	   &nbsp;	</li> 
								<li>	Reference Number: <strong>  <?=$this->session->userdata('invoice_no')?>	</strong> </li>
								<li>	   &nbsp;	</li> 
								<li>	<i> Note: "Provide this reference number as more details on the banking slip"	</i></li>
								<li>	   &nbsp;	</li> 
								</ul> 

							 </td>
						</tr>
					 
						</tr>
						 
			</tbody>						
					</table>
					
					<div class="row">
					<div class="col-md-12">  
					 <p>  </p>
					<!--<p> Wait for 3 Mins to allow API synchronization  </p>-->
					 <hr/>  
					 <p> Enter your  Transaction Code below</p>
						 
							<input type="text" value=""  id="transaction_code" class="form-control input-medium">
						  <label id="error">  </label>
					  </div>
					  <div class="col-md-12"> 
					  <!-- <p>   <hr/> </p>
						  <p id="partner_token"> Enter a Partner Token if available,Leave blank if not available </p>
						  
							<input type="text" value="" id="token" class="form-control input-medium">
						-->
						<HR id="hr"/> 
						 
					</form>
					 <p>    </p>
					 
					<a  href="javascript:;" class="btn green"  id="button_large">   VERIFY & ACTIVATE </a> 
					  <?php
					  }else{
						  ?>
						  <div class="col-md-12" id="m-pesa_only">  
					 <p> &nbsp; </p>
					<p> Your Account Balance is sufficient to purchase the selected package plan .  </p>
					  <p>  </p>
					 <p> Click on   "Confirm Transaction" button  below to accept  the Transaction.</p>
					 <p>  </p> 
					 <p> <b> NB: </b> This transaction cannot be cancelled or reversed once confirmed</p>
					 <label id="error1">  </label>
					 <hr/> 
					  </div>
					  
						  <a  href="javascript:;" class="btn grey"  id="confirm_pay" >    CONFIRM TRANSACTION </a> 
					 <?php } ?>
					<div class="margin-bottom-10 visible-sm"> </div>
					 
				</div>
				</div>
		</div>
	</div>
</div>
</div>
 

</div>

		 </div>
		<!-- END EXAMPLE TABLE PORTLET-->
 
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
            <!-- END CONTENT -->
<div class="modal fade draggable-modal" id="success" tabindex="-1" role="basic" aria-hidden="true">
	
	<div class="modal-body"><div class="modal-header" id="modal-header">  </div>
	<div class="row">
	<div class="col-md-12">  
		 
			<p id="modal-body">   </p> 
		 
		</div>
	</div>
</div>
<div class="modal-footer" ><center>  
		<button type="button" data-dismiss="modal" id="got_it" class="btn green ">&nbsp;&nbsp;&nbsp; Got It  &nbsp;&nbsp;&nbsp; </button>
		</center>
</div>
<!-- /.modal-dialog -->
</div>
        
      
        <!-- END CONTAINER -->
<script language="javascript">
	
	$('#transaction_code').keyup(function (e) {
	if (e.which == 13) {
		$('#button_large').trigger('click');
		 
	}
});

	$(document).ready(function() { 
		getData();
	    var selected="<?php echo $mode;?>";	 
		jQuery.uniform.update(jQuery("#"+selected+"").attr('checked',true)); 
	});
 $(function(){
	   var prev_pay="<?php echo $x;?>"; 
		if(prev_pay>0)
		{  
	    $("#partner_token").hide();
	    $("#token").hide();
	    $("#hr").hide();
			document.getElementById('token').disabled = true;
		} 
	 
 });

function getData(id)
	{ 
	 
	var selected=id;
	if(!id)
	{
		var selected="<?php echo $mode;?>"; 
	}
	jQuery.uniform.update(jQuery("#"+selected+"").attr('checked',true)); 
	$("#title").val(selected);  
	$("#method").html(selected.toUpperCase());
	if(selected=="mpesa")
	{
		$("#gtbank_logo").hide();
		$("#gtbank_pay").hide(); 
		$("#mpesa_only").show();
		$("#mpesa_pay").show();
		$("#mpesa_logo").show();
	}
	else if(selected=="gtbank")
	{
		//$("#mpesa_only").hide();
		$("#mpesa_pay").hide();
		$("#mpesa_logo").hide(); 
		$("#gtbank_logo").show();
		$("#gtbank_pay").show();
	}
	else{
		//$("#mpesa_only").hide();
		$("#mpesa_pay").hide();
		$("#mpesa_logo").hide();
		$("#gtbank_logo").hide();
		$("#gtbank_pay").hide();
		//$("#paybal_logo").show();
		//$("#paybal_pay").show();
		
	}
		
 
	}

 
$("#button_large").click( function () { 
	var total="<?php echo $total;?>"; 
	var credit="<?php echo $credit;?>"; 
	var package_name="<?php echo $this->session->userdata('package');?>"; 
	var years="<?php echo $this->session->userdata('years');?>";   
	var total_sms="<?php echo $this->session->userdata('total_sms');?>";   
	var selected=$("#title").val();  
	var transaction_code=$("#transaction_code").val();
	var token=$("#token").val();  
		if(!transaction_code){ $("#error").html("<font color='red'> Transaction code required </font>");return false;}
		if(!token){ token="";}
		$("#error").html("<font color='red'>Verifying code...</font>");
		$.ajax({
			'url':"<?=base_url();?>payment/proof_payment",
			'type':"POST",
			async:false,
			data:
			{
				'transaction_code':transaction_code, 
				'transaction_mode':selected, 
				'years':years,
				'total_sms':total_sms,
				'package':package_name,
				'total_amount_to_pay':total,
				'credit':credit,
				'token':token
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{
					 
					$("#error").html("<font color='green' size='4'> <i class='fa fa-check' style='font-size:20px;'> </i> Transaction code Verified successfully. "+obj.msg+"</font>");
					setTimeout(function(){
						  $("#error").empty();
						 window.location.replace("<?=base_url();?>payment/package/success_payment");
                      }, 3000);
					 
				}
				else if(obj.result=="insufficient")
				{

					$("#error").html("<font color='red' size='3'>  You paid Less Amount. Top up with (KES "+obj.data+")  to complete the transaction "+obj.msg+"</font>");
					setTimeout(function(){
						  
						$("#error").html("<font color='blue'> Refreshing page...</font>");
						location.reload();
                      }, 3000);
				}
				else{
					$("#error").html("<font color='red'>  Failed to verify! Ensure  Transaction code provided is correct.  "+obj.msg+" </font>")
					 return false;
				}
			}
			
		}		
		)
		 
});
	
$("#confirm_pay").click( function (){   
	var overpay="<?=$total?>";  
	var r=confirm("Are you sure you want to proceed with transaction?");
	if(r==true){ 
	 $("#error1").html("<font color='blue' size='4'> Transaction in progress...please wait</font>");
	 $.ajax({
		'url':"<?=base_url();?>payment/over_pay_transaction",
		'type':"POST",
		async:false,
		data:
		{
			'mpesa_code':'',
			'overpay':overpay 
		},
		success:function(data)
		{   
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				$("#error1").html("<font color='green' size='3'> <i class='fa fa-check' style='font-size:20px;'> </i> Transaction was successfully </font>");
				setTimeout(function(){
					$("#error1").html("<font color='green' size='3'> Redirecting...</font>");
				 
					 window.location.replace("<?=base_url();?>payment/package/success_payment");
				  }, 3000); 
			}
		}
	 }) 
	} 
});
	
	
$("#got_it").click( function () {
	window.location.replace("<?=base_url();?>payment/package");
	
});
	
	$("#btn_invoice").click( function () {
            var divContents = $("#content").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Invoice</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
</script>