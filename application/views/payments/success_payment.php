<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Payment </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Success Payment  </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">

	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
			<div class="portlet-title">
			<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Payment Successful </font>
	</div>
  <?php if($this->session->userdata('users')==0){ }else{ ?>
  <div class="col-md-12"> &nbsp; </div>
	 <div class="col-md-12">
		 <h4>  <font color="#006699"><strong>  Congratulation for purchasing  <?=$this->session->userdata('years');?> Year subscription for  <?=$this->session->userdata('users');?> User(s)  </strong> </font>   </h4>
	</div> 
  <?php } ?>
<div class="col-md-9 col-sm-6" >
   <div class="portlet light "> 
		<div class="portlet-body">
			 
<table border="0" id="success_payment"  width="90%"> 
			<tr>
				<td><img src="<?=base_url();?>images/login/ARI_Homes_Logo.png"  height="80" > 
					<h4> <strong> Invoice Number <?=$this->session->userdata('receipt_no')?> </strong> </h4>	
				</td> 
				<td align="right">
					 <h4><font color="green" size="4"> <strong> PAID </strong>  </font> </h4>
				</td>
			</tr>  
			<tr>
			<td>
			<p>  </p>		
			<ul style="font-size:14px;padding:4px;list-style-type:none">
			<li>	<strong> Invoiced To:	</strong></li> 
			<li>	<?=strtoupper($this->session->userdata('first_name'))?> <?=strtoupper($this->session->userdata('last_name'))?>	</li>
			<li>	<?=$this->session->userdata('email')?>	</li>
			<li>	<?=$this->session->userdata('company_name')?>	</li>
			<li> 	Nairobi, Kenya	</li>
			<li> 	&nbsp;	</li>
			<li> 	<strong> Payment Method:   </strong><br/>	<?=strtoupper($this->session->userdata('mode'))?>  </li>
			</ul>
		</td>
		<td> 
		<ul style="font-size:14px;text-align:right; list-style-type:none">
			<li> <strong> Pay To: </strong></li>
			<li>  AACC 1<sup>st</sup> Floor</li>
			<li>  Waiyaki Way, Nairobi </li>
			<li>  P.O.BOX 5710-00100 </li>
			<li>  Nairobi, Kenya </li>
			<li>  Contact: +254 725 992 355 </li>
			<li>  &nbsp; </li>
			<li> <strong> Invoice Date </strong> <?=date("d/m/Y")?> </li>
		</ul>
		 
		</td>
	</tr>	
	</table>
	<table border="0" id="success_payment"  width="90%"  style="border:1px solid lightgrey"> 
		<tr height="45" bgcolor="#DFDAD9"> 
			<td colspan="2"><strong> Invoice Items </strong> </td> 
		</tr>
		<tr> 
			<td><strong>  Description </strong>  </td> <td align="right"> <strong>   Amount &nbsp;&nbsp;  </strong>  </td>
		</tr>
		<?php
		if($this->session->userdata('users')==0){ $vat=0; $v=16/100*($this->session->userdata('total_sms_cost')); }else{?>
		<tr height="40"> <td>  <?=$this->session->userdata('years');?> Year(s), <?=$this->session->userdata('users');?> User(s)  &nbsp; </td> <td align="right"> Ksh  <?=$this->session->userdata('total_package_bill');?>   </td></tr>
		<?php 
			$vat=$v=$this->session->userdata('total_vat');
		} ?>
		<tr height="40"> <td> <?=$this->session->userdata('total_sms');?>  SMS </td> <td align="right">Ksh   <?=($sms=$this->session->userdata('total_sms_cost'));?></td></tr>
		<tr  ><td colspan="2">   </td>  </tr>
		<tr height="40"> <td> </td> <td align="right"> <b> Sub Total </b> &nbsp;  Ksh   <?=($sub_total=$this->session->userdata('total_sub_total')+$sms);?>  </td></tr> 
		<tr height="40"> <td>  </td> <td align="right"> <b> 16.00% VAT TAX  </b> &nbsp;   Ksh   <?=$v;?>  </td></tr>
		<tr height="20"> <td> </td> <td align="right"> <b> Credit </b> &nbsp;    Ksh  <?=$credit=$this->session->userdata('credit')?>  </td></tr>
		
		<tr  > <td> </td> <td align="right"><h4> <strong>  Total </strong>  &nbsp;   Ksh  <?=$t=($vat+$sub_total-$credit);?>  </h4>    </td></tr>
	</table>
	<p> </p>
			 <p> * Indicates a taxed item   </p>
			 <table  id="success_payment"  width="90%" class="table" > 
				<tr > <th> Transaction Date </th>  <th> Payment Mode   </th>  <th> Transaction Id </th>  <th> Amount</th> </tr>
				<tr> <td> <?=date('d/m/Y')?> </td> <td> <?=strtoupper($this->session->userdata('mode'))?> </td> <td> <?=$vat=$this->session->userdata('transaction_code');?> </td> <td> <?=$paid=$this->session->userdata('paid_amount');?> </td> </tr>
				<tr> <td colspan="3">  </td>  <td> Balance Ksh. <?=$balance;?> </td> </tr>
				</table>
				<p> </p>
			 </div>
		
			 <a  href="javascript:;" class="btn grey"  id="btn_receipt" >  <b> <i class="fa fa-print"></i> Print  </b> &nbsp;</a>   
		 </div>
		</div>
		
			 </div>
			 </div>
			 </div> 
		 
		</div> 
	</div>
<!-- END EXAMPLE TABLE PORTLET--> 
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
            <!-- END CONTENT -->
          
        
      
        <!-- END CONTAINER -->
<script language="javascript">
 
 var doc = new jsPDF();
var specialElementHandlers = {
'#editor': function (element, renderer) {
return true;
}
};

$(document).ready(function() {
$('#download').click(function () { 
doc.fromHTML($('.pormtlet-body').html(), 15, 15, {
'width': 170,
'elementHandlers': specialElementHandlers
});
doc.save('invoice.pdf');
});
}); 
 
 $("#btn_receipt").click( function () {
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Receipt </title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
</script>
