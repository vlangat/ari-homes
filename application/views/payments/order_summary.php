<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		var users=$("#users").val();
		if(users==""||users==null){ $("#error").html("<font color='red'> Please enter at least 1 User  </font>");return false;}
		 
	}
	</script>
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Packages </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Order Summary</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  <?php if(!$credit){ $credit=0;}else{ $credit=0-$credit;}?>
<div class="row">
	<div class="col-md-12">
	<div style="background:#1bb968;padding:6px;">
							<font color="#ffffff"> <strong> &nbsp;&nbsp; Order Summary </strong> </font>
							
					</div> 
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
			 <div class="portlet-title">
					
				<!--	</div> -->
			 
		 <div class="row">
		 <form action="<?=base_url();?>payment/payment_checkout" method="post" onsubmit="return validate()">
	<div class="col-md-7 col-sm-6">
   <div class="portlet light">
			<div class="portlet-body"> 
					<table border="0" width="100%" id="order_summary" style="font-family:arial,times new roman;border:1px solid lightgrey"> 
					<tbody>
						<tr  bgcolor="#32c5d2">
							<td>  <strong> <font color="#fff"> Product </font> </strong>  </td>
						</tr>
						<tr height="20"> <td> <b>  <?=ucfirst(strtolower($package))?>   Plan    </b></td></tr>
						<tr  > 
							<td>  
							<input type="hidden" name="package" value="<?php echo $package;?>">
							<div class="form-group">
								<label>Number of Users </label>
								<input type="number" class="form-control input-medium"  name="users"  id="users" readonly value="10" min="1" <?php if($package=="professional"){  echo 'max="5"'; }else{ echo ' max="250"';   } ?> onchange="setData()">
							 <label id="error">  </label>
							 </div>
							</td>
						</tr>
						<tr height="40"> <td> 
						<div class="form-group">
								<label> Billing Cycle </label>
								<select class="form-control input-medium"  name="pay" id="pay"    onchange="setData()">
								<?php /*foreach($billing_cycle->result() as $row){
										 ?>
											<option value="<?=$row->amount?>"> <?=$row->package_name?> </option>
										<?php 
										 
									}*/
									?>
							 <option value="250"> 1 Year @ KES 30,000  </option>
								</select>
						</div>
						
						</td></tr>
						<tr height="40">
							<td>  
						<p > <b> <font  color="#006699"> Other Value Add's you might be interested in </font></b></p>
							 <input  type="hidden"  id="sms" name="sms" value="" >
							 <h4> <strong>  Bulk SMS </strong> </h4>
							 <?php $i=1; foreach($sms->result() as $r){?>
							  <input type="checkbox" value="<?=$r->value?>" id="checkbox<?=$i?>" onclick="return checkboxValue(this.value, '<?=$i?>')"> <?=$r->package_name?> @ KES <?=$r->amount?>
							 <?php $i++;} ?>
						  <p>   </p>
						<p>  FAQ: Send SMS to all networks for KES 3 </p>
						   
							
							</td>
						</tr> 				 
				   
			<input type="hidden" value="<?=$credit?>" name="credit">
			<input type="hidden" id="years" name="years">
			<input type="hidden" id="total_package_bill" name="total_bill">
			<input type="hidden" id="total_sms" name="total_sms">
			<input type="hidden" id="total_sms_cost" name="total_sms_cost">
			<input type="hidden" id="total_sub_total" name="total_sub_total">
			<input type="hidden" id="total_sum_total" name="total_sum_total">
			<input type="hidden" id="total_vat" name="total_vat" >
			</tbody>						
		</table>
					<p> </p>
					<p> Configure your desired plans and continue to checkout </p>
			</div>
		</div>
		
		
		</div>
		
	<div class="col-md-5 col-sm-6"> 
		<div class="portlet light "> 
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12"> 
					<table border="1" width="100%" id="order_summary"   style="border:1px solid lightgrey"> 
						<tr   bgcolor="#32c5d2"> <td colspan="2">  <strong> <font color="#fff">  Price </font> </strong>  </td></tr>
						<tr height="40"> <td id="billing_by_user"> <?=$this->session->userdata('pay_val');?> Year(s), <?=$this->session->userdata('users');?> User(s) </td> <td align="right" id="total_bill">     </td></tr>
						<tr height="40"> <td > Sub Total </td> <td align="right" id="sub_total">   </td></tr>
						<tr height="40"> <td> Set Up Fees </td> <td align="right" > KES 0</td></tr>
						<tr height="40"> <td id="selected_sms">   </td> <td align="right" id="sms_cost">   </td></tr>
						<tr height="40"> <td> Sum Total</td> <td align="right"  id="sum_total">  </td></tr>
						<tr height="40"> <td > VAT TAX @16%  </td> <td align="right" id="vat">   </td></tr> 
						<tr> <td> Credit  </td> <td align="right"> KES <?=$credit;?> </td></tr>
						<tr height="60"> <td align="right" colspan="2"> <h3> <b id="total">    <br/> </h3> Total Due Today </b> </td></tr>
					 </table>
					 <p>    </p>
					 <input type="submit"   class="btn green" id="button_medium" value=" CONTINUE "> 
					 
					</div>
					<div class="margin-bottom-10 visible-sm"> </div>
					 
				</div>
			</div>
		</div>
	</div>
 
</form>
</div>

 </div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">

function setData()
	{    
		var users=$("input[name=users]").val(); 
		//x.charAt(0);
		var y=$("#pay option:selected").text(); 
		var years=y.trim().charAt(0);
		$('#years').val(years);
		var bill=$("#pay").val(); 
		var sms=$('#sms').val(); 
		if(!sms){ sms=0;}		
		$("#billing_by_user").html(years+" Year(s),  &nbsp; "+users+" User(s)");
		$("#selected_sms").html(sms +" SMS"); 
		var sms_cost=0; 
		var total_months=12; 
		var sms_cost=sms*3;
		var sub_total=0; 
		var sub_total=0;
        var package_cost=bill*users*total_months*years;		
		var sub_total=(package_cost);
		$("#sms_cost").html("KES "+sms_cost);
		var sum_total=0;   
		var sum_total=sms_cost+sub_total;
		$("#sum_total").html("KES "+sum_total);
		$("#sub_total").html("KES "+sub_total);
		$("#total_bill").html("KES "+sub_total);
        var vat=0;		
        var vat=Math.round(16/100*sub_total);		
		$("#vat").html("KES "+Math.round(vat));
		var credit="<?=$credit?>"; 
		$("#total").html("KES "+(sum_total+vat-Math.round(credit))); 
		$("#total_vat").val(vat);$("#total_sms").val(sms); $("#total_package_bill").val(sub_total);$("#total_sub_total").val(sub_total); $("#total_sms_cost").val(sms_cost);$("#total_sum_total").val(sum_total);
	}
	
	 


function checkboxValue(value,id)
	{   
	var sms=$("#sms").val(value); 
	
	jQuery.uniform.update(jQuery("#checkbox"+id).attr('checked',true));
if(id==2){	
	jQuery.uniform.update(jQuery("#checkbox1").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox3").attr('checked',false)); 
	  
}else if(id==1){
	jQuery.uniform.update(jQuery("#checkbox2").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox3").attr('checked',false)); 
}else if(id==3){
	jQuery.uniform.update(jQuery("#checkbox1").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox2").attr('checked',false)); 
}	
	if($("#checkbox1").prop('checked') == false && $("#checkbox2").prop('checked') == false && $("#checkbox3").prop('checked') == false) 
		{ 
			$("#sms").val(0);
		}else{
			$("#sms").val(value); 
	}	
    setData();
  }

 
 

$(document).ready(function(){
	 setData();
	  
	})
</script>