<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	 Subscriptions 
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Packages</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12"    style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Packages </font>
				
		</div>		
 <div class="col-md-12">  &nbsp;  </div>
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
		<!--	<div class="portlet light ">-->
			
			<div class="portlet-title">
				<h4> <strong> Choose a plan </strong>   </h4>
				<h5> Choose a plan that fits your business </h5>
		 </div>
		<!-- </div>-->
		<!-- END EXAMPLE TABLE PORTLET--> 
<div class="row"  style="padding:0px 50px 0px 50px">
	<div class="col-md-6 col-sm-6">
   <div class="portlet light "> 
			<div class="portlet-body">
					<table border="1" width="100%" id="select_package" > 
						<tr   bgcolor="#32c5d2"> <td align="center"><h4> <strong id="strong">  Premium Package </strong> </h4> </td></tr>
						<tr> <td> KES 30,000  Annually (10 Users)</td></tr>
						<tr> <td> Generate Income Statements</td></tr>
						<tr> <td> Track Expenses</td></tr> 
						<tr> <td> Manage Landlords</td></tr>
						<tr> <td> Add Properties and Tenants</td></tr> 
						<tr> <td> Total of 10 Users </td></tr>
						<tr height="30"> <td align="center">	<a  href="<?=base_url();?>payment/order_summary/premium" class="btn red" id="button_large1">   BUY NOW  </a> </td></tr>
					</table>
			</div>
		</div>
		
		
		</div>
		
	<div class="col-md-6 col-sm-6"> 
		<div class="portlet light "> 
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12"> 
					<table border="1" width="100%" cellspacing="18"  id="select_package"> 
						<tr height="50" bgcolor="#32c5d2"> <td align="center">
						<h4> <strong id="strong">  Custom Package </strong> </h4> </td></tr>
						<tr> <td> Email: info@ari.co.ke</td></tr>
						<tr> <td> Cell: +254 725 992 355</td></tr>
						<tr> <td> Cell: +254 789 502 424</td></tr>  
						<tr> <td> Web: www.arihomes.co.ke </td></tr>
						<tr> <td> &nbsp; </td></tr>
						<tr> <td> &nbsp; </td></tr>
						<tr height="35"> <td align="center"> <a  href="javascript:;" onclick="request_quote()" class="btn blue"  id="button_large2">  REQUEST QUOTE  </a>  </td></tr>
					</table>
					</div>
					
					<div class="margin-bottom-10 visible-sm"> </div> 
					
				</div>
				
			</div>
		</div>
	</div>
	<div class="col-md-12">
<p><strong> NB: </strong> We offer Data Entry support service for KES 25,000 to agencies with many properties and tenants, for a period of 1 month</p>
					
</div>			
</div> 

<div class="row">
<div class="col-md-12 col-sm-6"> 
		<div class="portlet light "> 
			<div class="portlet-body"> 
				<div class="row">
				<div class="col-md-12"    style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Buy  Bulk SMS  </font>
				
		</div>
	 <div class="col-md-8" style="padding:0px 50px 0px 50px">
			 	
 <div class="col-md-12">  &nbsp;  </div>  
			 <p>
			 <form action="<?=site_url('payment/buy_sms');?>"  method="post" onsubmit="return check_sms()"> 
			 <input  type="hidden" id="sms" name="sms" value="" >
			 <input  type="hidden" id="total_cost" name="total_cost" value="" >
				<h5> <strong> Choose your desired SMS plan </strong> </h5>
					  <p>   </p>
						<p> 
						<input  type="checkbox" id="checkbox1"   value="200"   onclick="return checkboxValue(this.value, 1)" > 200 SMS for KES 600 &nbsp; <input type="checkbox" id="checkbox2"   onchange="checkboxValue(this.value,2)" value="500"> 500 SMS for KES 1,500 &nbsp;  <input type="checkbox" id="checkbox3" value="1000" onclick="checkboxValue(this.value,3)" > 1,000 SMS for KES 3,000
				 <p> 
				 <p>  <b> FAQ: </b>  Send SMS to all networks for KES 3 </p>
				 <font color="red" id="error"> </font>		   
				 <p> </p>	   
		
 
	<input type="submit" class="btn green"  value="Buy Now"> 
	</form>
</div>
	</div>
		</div>
	</div>
</div>   
</div>

</div>

<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
<!-- END CONTENT -->
 <div id="request_quote" class="modal fade" tabindex="-1" data-width="600">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b>  Request Quote </b></h5>
				<hr/>
				<div class="form-group">
					<label>Number of Users </label>
					<input type="number" class="form-control"  name="users"  id="users"   value="11" min="11">
					
				</div>
				<label id="request_error">   </label>
			</div>
		</div>
</div>
<div class="modal-footer" > 
	<button type="button" class="btn green" onclick="confirm_request()">Submit </button>
	<button type="button" data-dismiss="modal"  class="btn red"> Close </button> 
</div>
</div>
          
        
      
<!-- END CONTAINER -->
<script language="javascript">

 

function checkboxValue(value,id)
	{   
	var sms=$("#sms").val(value); 
	
	jQuery.uniform.update(jQuery("#checkbox"+id).attr('checked',true));
if(id==2){	
	jQuery.uniform.update(jQuery("#checkbox1").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox3").attr('checked',false)); 
	  
}else if(id==1){
	jQuery.uniform.update(jQuery("#checkbox2").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox3").attr('checked',false)); 
}else if(id==3){
	jQuery.uniform.update(jQuery("#checkbox1").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox2").attr('checked',false)); 
}	
	if($("#checkbox1").prop('checked') == false && $("#checkbox2").prop('checked') == false && $("#checkbox3").prop('checked') == false) 
		{ 
			$("#sms").val(0);
		}else{
			$("#sms").val(value); 
	}	
    //setData();
  }
  
 function request_quote()
 { 
	 $("#request_quote").modal('show');
 }
 
 function confirm_request()
 {
	var users=0;
	users=$('#users').val();   
	if(!users||users==0){  $('#request_error').html("<font color='red'> Enter the number of users</font>"); $('#users').focus();  return false; }
	users=parseInt(users); 
	if(users<11){  $('#request_error').html("<font color='red'> Users should be more than 10 </font>"); $('#users').focus();  return false; }
	 $('#request_error').html("<font color='green'> Please wait... </font>");
    $.ajax({
		url:"<?=base_url();?>payment/request_quote", 
		type:"POST", 
		contentType: 'application/json',
		data:{
			'users':users 
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				$('#request_error').html("<font color='green'> Thank you! Request received and someone will get back to you soon</font>");  
			    setTimeout(function(){
					$('#request_error').empty();
					$('#users').val(11); 
					$("#request_quote").modal('hide');
					}, 3000);
			}
			else
			{
				 $('#request_error').html("<font color='red'> Request for Enterprise quote not sent  </font>");  
			}
		}
	})
	
 
 }
 
 function check_sms()
 {
	var sms=0;
	sms=$('#sms').val();   
	if(!sms||sms==0){  $('#error').html("<font color='red'> You have not yet selected any  SMS plan. Select your desired plan to proceed</font>"); $('#checkbox1').focus();  return false; }
	var sms_cost=sms*3; 
	$("#total_cost").val(sms_cost);	
	return true;	   
 } 
 
  $(document).ready(function()
  {
	  $("#buy_sms").click(function(){
		var sms=$('#sms').val();  
		 if(!sms){ sms=0;}
		var sms_cost=sms*3; 
		$.ajax({
		url:"<?=base_url();?>payment/buy_sms",
		//url:"<?=base_url();?>payment/add_sms",
		type:"POST", 
		contentType: 'application/json',
		data:{
			'sms':sms,
			//'mpesa_code':code,
			'total_cost':sms_cost
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				window.location = "<?=base_url();?>payment/buy_sms";
		      // $("#error").html("<font color='green'>You have successfully bought SMS </font>");
			}
			else
			{
				$("#error").html("<font color='red'>"+obj.msg+"</font>");
			}
		}
	})
	
	});
  });
</script>