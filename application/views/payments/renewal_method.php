<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Packages </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Payment Checkout</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">  
		<div style="background:#1bb968;padding:6px;">
			 <font color="#ffffff"><h4> <strong> Your preferred Payment method is <font id="method"> <?= strtoupper($mode)?> </font> </strong> </h4> </font> </strong> </font>
		</div> 
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
	<div class="portlet light portlet-fit "> 
		 <input type="hidden" id="title" value="">
			 
<div class="row">
			<div class="col-md-12"><h4>  &nbsp; <strong>  Choose a different payment method  </strong>  </h4> </div>
		<div class="col-md-12">
		  <div class="col-md-4">  
			<div class="radio-list">
						<label class="radio-inline">
						<input type="radio" name="pay_mode"  value="mpesa"  onclick="getData('mpesa')" id="mpesa"> <img src="<?=base_url();?>images/mpesa.png" height="75" width="150"> 
						</label>
			</div>
		  </div>
		<div class="col-md-4">
		<div class="radio-list">
			<label class="radio-inline">
				<input type="radio" name="pay_mode"  value="gtbank" onclick="getData('gtbank')" id="gtbank"> <img src="<?=base_url();?>images/GTBank.png" height="75" width="150"> 
			</label>
			</div>
		  </div>
		<!--  <div class="col-md-4">
		 <input type="radio" name="mode"  value="paybal" onclick="getData('paypal')" id="paypal">  <img src="<?=base_url();?>images/paypal_Used.png" height="100" width="200"> 
		  </div>--->
	</div>
</div>

<?php $x=0; foreach($previous_pay->result() as $r){ $x++; } ?>
 
		 <div class="row">
	<div class="col-md-6 col-sm-6">
   <div class="portlet light "> 
			<div class="portlet-body">
			<p>   </p>
			<div id="content" style="width:100%">
<table border="0" width="100%"   id="chosen_payment"   style="border:1px solid lightgrey"> 
<tr>	<td colspan="2">
<p>  </p>		
			<ul style="padding:0px;list-style-type:none">
			<li>	<strong> Invoice No	# <?=$invoice_no?> </strong>	</li>
			<li>	<strong> <font color="red"> UNPAID	</font> </strong> </li>
			<li>	&nbsp;	</li>
			<li>	<strong> Invoiced To	</strong></li>
			<li>	&nbsp;	</li>
			<li>	<?=strtoupper($this->session->userdata('first_name'))?> <?=strtoupper($this->session->userdata('last_name'))?>	</li>
			<li>	<?=$this->session->userdata('email')?>	</li>
			<li>	<?=$this->session->userdata('company_name')?> </li>
			<li> 	Nairobi, Kenya	</li>
			</ul>
		</td>
	</tr>	
		<tr   bgcolor="#32c5d2"> 
		<td colspan="2">  <strong id="strong">  Invoice Items </strong>   </td>
		</tr>	 <?php $sms_cost=0; $total_sms=0;?>
		<tr> <td>      
		<?=$years;?> Year(s), <?=$users;?> User(s)  &nbsp; </td> <td align="right"> KES <?=$total_sub_total=$package_cost;?>  <br/> 
						
		</td></tr> 
		<tr > <td><b> Sub Total </b></td> 
		<td align="right"><b>KES  <?=($sub_total=$total_sub_total+$sms_cost);?> </b> </td> </tr>   
		<tr > <td> VAT TAX @16%  </td> <td align="right"> KES <?=$vat;?> </td></tr>
		<tr > <td> Credit </td> <td align="right"> <?php  $credit=0; if($balance<0){ $credit=0-$balance;}?> KES <?=$credit?> </td></tr>
		<tr height="20"> <td><strong>  Total </strong> </td> <td align="right"><h4> <b>  KES <?=($total_cost);?> </h4>  </b>  </td></tr>
	</table><?php $total=$total_cost;?>
			 <p>    </p>
			 </div>
			 <div id="editor"></div>
			 <a  href="javascript:;"  class="btn grey" id="btn_invoice">   DOWNLOAD INVOICE  </a> 
		 </div>
		</div>
		 
		</div>
		
	<div class="col-md-6 col-sm-6"> 
		<div class="portlet light "> 
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12"> 
					<p>   </p>
					
			<form action="<?=base_url();?>payment/renewal" method="post"> 
			<?=$selected_users?>
						<input type="hidden" value="<?=$mode?>"  id="mode" name="transaction_mode">
						 <input type="hidden" value="<?=$sub_total+$vat?>"    name="amount_to_pay">
						 <input type="hidden" value="<?=$total?>"  id="amount" name="amount">
						 <input type="hidden" value="<?=$package?>"   name="package_name">
						 <input type="hidden" value="<?=$years?>"   name="years">
						
					<?php if($total>0){?>
				<table border="0" width="100%" cellpadding="30" style="border:1px solid lightgrey"> 
					<tbody> 
					<tr align="center" id="mpesa_logo"> 
						<td> 
							  <img src="<?=base_url();?>images/mpesa.png" > 						
						</td>
					</tr> 
					 
					<tr height="40" id="mpesa_pay"> <td align="left" > 
						   <ul style="list-style-type:none;padding:12px">
								<li>	&nbsp;	</li>
								<li>	Due Date :<?php echo date("d-m-Y");?> </li>
								<li>	&nbsp;	<hr/></li>
								<li>	<strong> From your Mobile Phone </strong>	</li>
								<li>	   &nbsp;	  </li>
								<li>	  Go to <strong>  MPESA Menu  </strong>	</li>
								<li>	   &nbsp;	  </li> 
								<li>	  Select <strong> Lipa na Mpesa </strong> 	</li>
								<li>	   &nbsp;	  </li>
								<li>	  Select <strong> Pay Bill  </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	  Enter the Business No: <strong> 990850</strong> 	</li>
								<li>	   &nbsp;	  </li>
								<li>	 Enter Account number:  <strong> <?=$invoice_no;?>  </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	   Enter  Amount: <strong>KES  <?=$total?> </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	   Enter your MPESA PIN, Click Ok 	</li>
								<li>	   &nbsp;	  </li>	
								<li>	   Confirm by Clicking Ok   	</li>
								<li>	   &nbsp;	  </li>   
							</ul>
						</td>
						</tr>
						<tr id="gtbank_logo" height="40" >
						<td align="center" style="padding:10px">  <img src="<?=base_url();?>images/GTBank.png" height="60" width="120">  </td>
						</tr>
						<tr id="gtbank_pay" height="40">
						     <td align="left">    
								<ul style="list-style-type:none;padding:12px">
								<li>	<h4> <b> PAY TO: </b> </h4>	</li>
								<li>	&nbsp;	</li>
								<li>	Account Name: <strong> ARI Limited	</strong> </li>
								<li>	   &nbsp;	</li> 
								<li>	Account No :  <strong>  21300036452	</strong> </li>
								<li>	   &nbsp;	</li> 
								<li>	Branch Name: <strong> Westlands </strong> 	</li>
								<li>	   &nbsp;	</li> 
								<li>	 BIC/SWIFT: <strong>  GTBIKENA </strong> </li>
								<li>	   &nbsp;	</li> 
								<li>	Reference Number: <strong>  <?=$invoice_no?>	</strong> </li>
								<li>	   &nbsp;	</li> 
								<li>	<i> Note: "Provide this reference number as more details on the banking slip"	</i></li>
								<li>	   &nbsp;	</li> 
								</ul> 

							 </td>
						</tr>
					 
						</tr> 
			</tbody>						
					</table>
					
					<div class="row">
					<div class="col-md-12">  
					 <p>  </p>
				<!--	<p> Wait for 3 Mins to allow API synchronization  </p> 
					 <hr/>  -->
					 <p> Enter your Transaction Code below</p>
						<input type="text" value="" name="transaction_code" id="transaction_code" class="form-control input-medium">
						  <label id="error">  </label>
					  </div>
					  <div class="col-md-12"> 
					   <p>   <hr/> </p>
						    
					 <p>    </p>
					 <?php
					  }else{
						  ?>
						  <div class="col-md-12" id="mpesa_only">  
					 <p> &nbsp; </p>
					<p> Your Account Balance is sufficient to renew the selected user(s).  </p>
					  <p>  </p>
					 <p> Click on   "Confirm Transaction" button  below to accept  the Transaction.</p>
					 <p>  </p> 
					 <p> <b> NB: </b> Once you click button below, transaction cannot be cancelled whatsoever </p>
					 <label id="error1">  </label>
					 <hr/> 
					  </div>
					  
						<!--  <a  href="javascript:;" class="btn grey"  id="confirm_pay" onclick="form.submit(this)">    CONFIRM TRANSACTION </a> -->
					 <?php } ?>
					  
					<input type="submit"   class="btn green"  id="button_large" onclick="return checkCode()" value="VERIFY & CONTINUE"> 
					 
					<div class="margin-bottom-10 visible-sm"> </div>
					 
				</div>
				</div>
				</form>
		</div>
	</div>
</div>
</div>
 

</div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->
 
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
            <!-- END CONTENT -->
<div class="modal fade draggable-modal" id="success" tabindex="-1" role="basic" aria-hidden="true">
	
	<div class="modal-body"><div class="modal-header" id="modal-header">  </div>
	<div class="row">
	<div class="col-md-12">  
		 
			<p id="modal-body">   </p> 
		 
		</div>
	</div>
</div>
<div class="modal-footer" ><center>  
		<button type="button" data-dismiss="modal" id="got_it" class="btn green ">&nbsp;&nbsp;&nbsp; Got It  &nbsp;&nbsp;&nbsp; </button>
		</center>
</div>
<!-- /.modal-dialog -->
</div>
        
      
        <!-- END CONTAINER -->
<script language="javascript">


function getData(id)
	{ 
	 
	var selected=id;
	if(!id){
		var selected="<?php echo $mode;?>"; 
	}
	$("#title").val(selected); 
	$("#mode").val(selected); 
	$("#method").html(selected.toUpperCase());
	jQuery.uniform.update(jQuery("#"+selected+"").attr('checked',true)); 
	if(selected=="mpesa")
	{
		$("#gtbank_logo").hide();
		$("#gtbank_pay").hide();
		$("#paybal_logo").hide();
		$("#paybal_pay").hide();
		//$("#mpesa_only").show();
		$("#mpesa_pay").show();
		$("#mpesa_logo").show();
	}
	else if(selected=="gtbank")
	{
		//$("#mpesa_only").hide();
		$("#mpesa_pay").hide();
		$("#mpesa_logo").hide();
		$("#paybal_logo").hide(); 
		$("#paybal_pay").hide();
		$("#gtbank_logo").show();
		$("#gtbank_pay").show();
	}
 	
 
	}
	
	$(document).ready(function() {
				
		getData();
	    var selected="<?php echo $mode;?>";	 
		jQuery.uniform.update(jQuery("#"+selected+"").attr('checked',true)); 
	});
 
	
 
	$("#btn_invoice").click(function () {
            var divContents = $("#content").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Invoice</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
		
function confirmTransaction()
{      
	var r=confirm("Are you sure you want to proceed with transaction?");
	if(r==true)
	{ 
		$("#error1").html("<font color='#006699' size='3'> Transaction in progress...please wait</font>");
	  	return true;			  
	}
	else{
			return false;	 
		} 


}

function checkCode()
{       var x=2;  
        var amount="<?=$total?>"; 
		amount=Math.round(amount);  
		if(amount<=0){    x=1; return true;}
		var transaction_code=$("#transaction_code").val();  
	 	var users ="<?=$users?>"; 
		if(users==0 || !users){ $("#error1").html("<font color='red'> No users selected to   re-new </font>"); return false;}
		var amount=$("#amount").val();  
		if(!transaction_code){ $("#error").html("<font color='red'> Transaction code required </font>");return false;}
		 
		$.ajax({
			'url':"<?=base_url();?>payment/checkCode",
			'type':"POST",
			async:false,
			data:
			{
				'transaction_code':transaction_code, 
				'amount':amount
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{
					$("#error").html("<font color='green' size='3'>  Transaction  code verification success</font> ");
					 //show notification to proceed with transactions
					 x=1;  
				}
				else if(obj.result=="insufficient")
				{
					$("#error").html("<font color='red' size='3'>  You paid Less Amount. Clear your Balance KES. "+obj.data+"</font>");
					x=2;
				}
				else{
						$("#error").html("<font color='red'>  Failed to verify! Ensure that   M-pesa Transaction code provided is correct.  </font>")
						x=2;
					}
			}
			
		})
		
		if(x==2)
		{
			return false;
		}

 
}
</script>