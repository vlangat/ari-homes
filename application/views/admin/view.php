<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->  
		<!-- BEGIN PAGE CONTENT BODY -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMBS -->
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="index.html">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">Pages</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>View User</span>
					</li>
			</ul> 
		<?php
		$total_tenants=0; foreach($tenants->result() as $r){   $total_tenants++; }
		$total_users=0; foreach($users->result() as $row){   $total_users++; }
		$total_prop=0; foreach($properties->result() as $prop){   $total_prop++; }
		?>
		
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="inbox">
	<div class="row">
	<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-green-sharp">
						<span data-counter="counterup" data-value="<?php echo $total_prop;?>"> 0 </span>
						<small class="font-green-sharp"> </small>
					</h3>
					<small>TOTAL PROPERTIES</small>
				</div>
				<div class="icon">
					<i class="icon-home"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
						<span class="sr-only">76% progress</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> progress </div>
					<div class="status-number"> 76% </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-red-haze">
						<span data-counter="counterup" data-value="<?=$total_tenants?>"> 0 </span>
					</h3>
					<small> TOTAL TENANTS </small>
				</div>
				<div class="icon">
					<i class="icon-users"> </i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
						<span class="sr-only">85% change</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> change </div>
					<div class="status-number"> 85% </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-blue-sharp">
						<span data-counter="counterup" data-value="<?php echo $total_users;?>">0</span>
					</h3>
					<small> NO OF USERS </small>
				</div>
				<div class="icon">
					<i class="fa fa-users"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 45%;" class="progress-bar progress-bar-success blue-sharp">
						<span class="sr-only">45% grow</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> grow </div>
					<div class="status-number"> 45% </div>
				</div>
			</div>
		</div>
	</div>
 
</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div> 
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12" style="background:#006699;padding:3px;">
			<font color="#ffffff"> User's  Details </font>
		</div>	
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">First Name </label>
			<input type="hidden" id="id_no" value="<?=$row->id;?>" class="form-control"  readonly  />
			<input type="text" name="fname" value="<?=$row->first_name;?>" class="form-control"  readonly  />
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">Last Name </label>
			<input type="text" name="lname" value="<?=$row->last_name;?>" class="form-control"  readonly />
		</div>
	</div>		
</div>
 
 <div class="col-md-12"> 
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">Company </label>
			<?php $company_name="";
							foreach($company_info->result() as $c){  
								 $company_name=$c->company_name; 
							}
							?> 
			<input type="text" name="fname" value="<?=$company_name;?>" class="form-control"   readonly />
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">Email </label>
			<input type="text" name="lname" value="<?=$row->email;?>" class="form-control"  readonly />
		</div>
	</div>		
</div>

<div class="col-md-12"> 
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">Phone </label>
			<input type="text" name="fname" value="<?=$row->mobile_number;?>" class="form-control"  readonly />
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label"> Registration Date </label>
			<input type="text" name=" " value="<?=$row->registration_date;?>" class="form-control"  readonly />
		</div>
	</div>		
</div>

<div class="col-md-12"> 
<div class="col-md-6">
	<div class="form-group">
			<label class="control-label">Status </label> 
			<input type="text" name="status" readonly value="<?php if($row->user_enabled==1){ $status="Deactivate";echo 'active';} else { echo 'inactive'; $status="Activate";};?>" class="form-control" />
			<input type="hidden" id="status" readonly value="<?=$row->user_enabled?>" class="form-control" />
			<input type="hidden" id="testStatus" readonly value="<?=$testAccount=$row->testAccount?>" class="form-control" />
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label"> Package </label> 
			<?php $user_type="Free"; $package_id=0;  $package_name="30 Days Trial";
			foreach($packages->result() as $p){  $package_id=$p->package_id; }
			foreach($package_type->result() as $pt){  if($package_id==$pt->id){$package_name=$pt->package_name; $user_type=$pt->package_type;} }
							
							//if($package_id==1){ $user_type="Premium"; } if($package_id==2){ $user_type="Custom"; } 
							?>
			 <input type="text" id="package" readonly value="<?=$user_type?> (<?=$package_name?>)" class="form-control" />
		</div>
	</div>		      
</div>
 
 <div class="col-md-12"> 
 
<div class="col-md-4">
		<div class="form-group"> 
			<a  id="achieve" class="btn btn-success"><?php if($row->testAccount == 2){ echo 'Achieved'; }else{ echo 'Achieve';}?> </a>
		</div>
</div>	
<div class="col-md-4">
		<div class="form-group">
			<a  id="activate" class="btn red"> <?php echo $status ?></a>
		</div>
</div>	

<div class="col-md-4">
		<div class="form-group">
			<a  id="markTestAccount" class="btn green"> <?php if($testAccount==0){?> Mark As Test Account<?php }else{ echo 'Mark As Live Account';}?></a>
		</div>
</div>	
<div class="col-md-12">
	 <p id="error_message">    </p>
</div>								
</div>

</div>
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
          
        </div>
        <!-- END CONTAINER -->
 
    </body>

</html>

<script language="javascript">
$(document).ready(function()
{ 
$("#activate").click(function()
	{   
	 
		var id=	$("#id_no").val();
        var status=$("#status").val();	
        if(status==1){ status=2;}else{ status=1;}		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/activate/"+id+"/"+status,
		 type:"POST",
		 async:false,
		 success:function(data)
		 { 
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				$("#error_message").html(" <font color='green'> <i class='fa fa-check'></i> Changes made successfully </font>");
          setTimeout(function(){
			  location.reload();
			 },3000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				$("#error_message").html(" <font color='red'><i class='fa fa-close'></i>  Changes to the user details not made </font>");
				return false;
			 }
		 }
		  }
		 )	  
});


$("#markTestAccount").click(function()
	{   
	 
		var id=	$("#id_no").val();
        var status=$("#testStatus").val();		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/markTestAccount/"+id+"/"+status,
		 type:"POST",
		 async:false,
		 success:function(data)
		 { 
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				$("#error_message").html(" <font color='green'> <i class='fa fa-check'></i> Changes made successfully </font>");
              setTimeout(function(){
			  location.reload();
			 },3000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				$("#error_message").html(" <font color='red'><i class='fa fa-close'></i>  Changes to the user details not made </font>");
				return false;
			 }
		 }
		  }
		 )	  
});

$("#achieve").click(function()
	{   
	 
		var id=	$("#id_no").val();
        var status=2;		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/markTestAccount/"+id+"/"+status,
		 type:"POST",
		 async:false,
		 success:function(data)
		 { 
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				$("#error_message").html(" <font color='green'> <i class='fa fa-check'></i> Achieved successfully </font>");
              setTimeout(function(){
			  location.reload();
			 },3000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				$("#error_message").html(" <font color='red'><i class='fa fa-close'></i>  Changes to the user details not made </font>");
				return false;
			 }
		 }
		  }
		 )	  
});


});

</script>