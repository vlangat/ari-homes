<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN HEADER -->      
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Tenants</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#"> <?=$status?> Tenant</a>
		 
	</li> 
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet light portlet-fit ">
	<div class="portlet-body">
	<div class="row"> 
 
	<div class="col-md-12" style="background:#006699;padding:6px;">
		<font color="#ffffff"> <?=$status?> Tenants </font>
	</div>
 
<div class="col-md-12">  &nbsp;  </div>	
</div> 

<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr>
	<th> # </th> 
	<th> Tenant   </th>
	<th> Property </th>
	<th> Unit Category </th> 
	<th> Email </th>
	<th> Mobile No </th> 
	<th> DateAdded </th> 
	<th> Owner </th> 
	<th> Action </th> 
	</tr>
</thead>
<tbody>
<?php   $x=1; foreach($tenants->result() as $row):?>
	<tr>
		<td> <?=$x?></td>
		<!--<td>
		<?php  $company_name="";
			/*foreach($company_info->result() as $c){ 
				if($row->company_code==$c->company_code){
					 echo $company_name=$c->company_name ;
				}
			}*/?>
		</td>-->
		<td><?php if($row->tenant_type=="residential"){?> <?php echo $row->first_name; }else {?><?=$row->company_name?><?php } ?></td>
		<td> <?=$row->property_name?>	</td>
		<td> <?=$row->category_name?>	</td> 
		<td class="center"> <?=$row->tenant_email?></td> 
		<td> <?=$row->mobile_number?> </td> 
		
		<td><?=$row->date_added?></td> 
		<td> 
		<?php  $company_code=$row->company_code;
		foreach($company_info->result() as $c) 
		{ 
			if($company_code==$c->company_code)
			{
				  $company_name=$c->company_name;
			}
		}
        if($company_name=="")
		{
			foreach($user_info->result() as $user){
				if($user->company_code==$company_code)
				{
					  $company_name=$user->first_name .' '. $user->last_name;
				}
			}
		}
		echo $company_name;
		?> 
		</td> 
		<td>
		<?php if($row->tenant_status==1){?>
			<a href="javascript:;"  style="color:red"  onclick="delete_tenant('<?=$row->id?>')">
			<i class="fa fa-trash"> </i>  
				Remove   
			</a>
		<?php }
		else if($row->tenant_status==2){?>
			<a href="javascript:;" style="color:green" onclick="add_tenant('<?=$row->id?>')">
			<i class="fa fa-plus"> </i>  
				Add   
			</a>
		<?php } ?>
		 </td>
	</tr> 
	 
<?php $x++; endforeach;?>
 
</tbody>
</table>
 
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY --> 
<!-- END CONTAINER --> 


<div id="warning" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title"><b  style="font-size:18px;color:brown"> <i class="fa fa-info"> </i>  Warning Message</b></h4>
			</div>
			<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12">  
   
			<p>
				Are you sure you want to  perform  selected action on this tenant? 
			</p>
			<input type="hidden" id="del_id">
		</div>
		</div>
</div>
<div class="modal-footer" >  
	<button type="button"   id="confirm_del" class="btn green">Yes</button>
	<button type="button" data-dismiss="modal" class="btn default">No</button>
</div>
</div>

</div>
</div>
</div>


<script language="javascript">
 
  $(function(){
	$("#confirm_del").click(function(){
	var id=$("#del_id").val(); 
	$.ajax({
		url:"<?=base_url();?>tenants/removeTenant/"+id,
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;
			if(obj.result=="ok")
			{  
						setTimeout(function(){ 
							$('#warning').modal('hide');
							location.reload();
                      }, 1500);
		       
			}
			else
			{
				alert('Not deleted Try again later');
				return false;
			}
		}
	 })
	 });
	 
	 
});


function delete_tenant(id)
 {  
	$("#del_id").val(id);
	$("#warning").modal('show');
 }	
 
function add_tenant(id)
 {  
	var msg="Add Tenant";	 
	var x=confirm("Are you you to return this tenant?");
	if(x==true){   	 
		$.ajax({
		 url:"<?=base_url();?>admin/addTenant/"+id,
		 type:"POST",
		 async:false,
		 success:function(data)
		 {  
			var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 setTimeout(function(){
				location.reload();
			 },2000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				alert(" Changes to the tenant was  not made Try again later ");
				return false;
			 }
		 }
		  }
		 )	
	}
else{
		return false;
	}	
}
 	
</script> 