<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Users </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Log Activity </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	
	<div class="col-md-12">
	<div class="col-md-12" style="background:#006699;padding:6px;">
				<font color="#ffffff"> Audit Trail </font>
		</div>
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar" style="min-height:400px">
  
				<table class="table table-striped table-hover table-bordered" id="table">
					<thead>
						<tr>         
							<th> # </th> 
							<th> Login Date </th> 
							<th> User Name  </th> 
							<th> Email </th>  
							<th> Ip Address </th>  
						</tr>
					</thead>
					<tbody id="logs">
					<td colspan='5' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>");
		
					</tbody>
				</table>
				 
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
	<i class="icon-login"></i>
</a>

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

<script language="javascript">
 function validate()
{	
var formobj = document.forms[0];
var counter = 0;
for (var j = 0; j < formobj.elements.length; j++)
{
    if (formobj.elements[j].type == "checkbox")
    {
        if (formobj.elements[j].checked)
        {
            counter++;
        }
    }       
}
if(counter==0){  
    alert("Please select at least one.");
	return false;
 }
 

	/*var chks = document.getElementsByName('checkbox[]');
	var hasChecked = false;
	for (var i = 0; i < chks.length; i++)
	{
		if (chks[i].checked)
		{
		hasChecked = true;
		break;
	}
	}
	if (hasChecked == false)
	{
	alert("Please select at least one.");
	return false;
	}*/
	 var x=confirm("Are you you to remove this  log activity?");
	if(x==true){  
	  	 	
	return true;
	}
	else{
		return false;
	}
}

 
function delete_rec(id)
	{ 
	 
	var x=confirm("Are you you to remove this log activity?");
	if(x==true){  
	  	 	
	    //$("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/remove_log/",
		 type:"POST",
		 async:false,
		 data:{
			'id':id 
		 },
		 success:function(data)
		 {  
			var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 setTimeout(function(){
				//location.reload();
				window.location.href = "<?=base_url();?>admin/ip_history"; 
			 },2000);			  
			 }
			 else
			 {
				//$("#error_message").empty();  
				alert(" Log activity not removed! Try again later");
				return false;
			 }
		 }
		  }
		 )	
	}
else{ return false;}	
	}
	
/*	
	$(document).ready(function() {
	    var table= $('#table').DataTable({
              destroy: true,
              responsive: true,
               "ajax":{
                   url:'/tables/login_history',
                   type: 'get'
               },
               language: {
                    searchPlaceholder: "Search records.."
                }
    	});
	});  */
 function login()
{
		var content=""; 
		$("#logs").html("<tr><td colspan='5' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>");
		$.ajax({
		url:'<?=base_url();?>tables/login_history/', 		
		type: 'POST', 
		async:false, 
		success:function (data)
		{  
			var obj = JSON.parse(data); 
			var data = obj.data; var count=0; var count=parseInt(data.length);$("#countr").val(count);  
            
			if(count >0){ 
				for(var i=0; i<data.length; i++)
				{
					var p = data[i];    
					content=content+"<tr><td>"+(i+1)+"</td> "+
					"<td>"+p['login_time']+"</td> <td>"+p['first_name']+" "+p['last_name']+"</td><td>"+p['email']+"</td><td>"+p['ip_address']+"</td></tr>";
					count++;
				} 
				$("#logs").html(content); 
			}
			else{
					$("#logs").html("<tr><td colspan='5' align='center'>No data available</td></tr>");
				} 
			$('#table').DataTable();
		}
	});
 
}
  
 $(document).ready(
 function(){   
	login();
 });
</script>