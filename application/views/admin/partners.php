<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
	   
		</div>
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE CONTENT BODY -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMBS -->
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="index.html">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">Pages</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Partners</span>
					</li>
			</ul>
		<?php //foreach($user_details->result() as $row){}?>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="inbox">
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div> 
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12" style="background:#006699;padding:10px;">
			<font color="#ffffff"> Please fill this form to register a new  Partner  </font>
			
		</div>
			<p> &nbsp; </p>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">First Name </label> 
			<input type="text" id="fname" value="" class="form-control"   />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Middle Name </label>
			<input type="text" id="mname" value="" class="form-control"  />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Last Name </label>
			<input type="text" id="lname" value="" class="form-control"  />
		</div>
	</div>		
	</div>
 
 <div class="col-md-12"> 
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Company </label>
			<input type="text" id="company" value="" class="form-control"  />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Email </label>
			<input type="text" id="email" value="" class="form-control"  />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Phone </label>
			<input type="text" id="phone" value="" class="form-control" />
		</div>
	</div>		
</div>

<div class="col-md-12"> 
	 
		<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Alt Phone </label>
			<input type="text" id="alt_phone" value="" class="form-control" />
		</div>
	</div> 
<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> National Id </label>
			<input type="number" id="national_id" value="" class="form-control" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">KRA Pin </label>
			<input type="text" id="kra" value="" class="form-control"/>
		</div>
	</div> 		      
</div> 

<div class="col-md-12">
  
<div class="col-md-4">
 
		<div class="form-group">
			<a   id="save" class="btn green">  </i> Register Partner</a>
		</div>
</div> 
 	
<div class="col-md-8">
	 <p id="error_message"> </p>
</div>								
</div>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit ">
	<div class="portlet-body">
	<hr/>
<form name="form1" method="post" action="<?=site_url();?>tenants/viewTenant" onSubmit="return validate();">
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr>
		<th>  #  </th>
		<th> Name </th>
		<th> Email </th>
		<th> Phone No </th>
		<th> Alt Phone  </th>
		<th> Partner Code </th> 
		<th> Edit </th> 
		<th> View </th> 
	</tr>
</thead>
<tbody>
<?php $x=1; foreach($partners->result() as $row):?>
	<tr>
		<td> <?=$x?>  </td>
		<td> <?=$row->first_name. " ".$row->middle_name . " ".$row->last_name ?>   </td>
		<td> <?=$row->email?> </td>
		<td> <?=$row->phone?> </td>
		<td> <?=$row->alt_phone?> </td>
		<td class="center"> <?=$row->partner_code?> </td>
		 
		<td>
		  <a href="javascript:;" onclick="edit_partner('<?=$row->id?>','<?=$row->first_name?>','<?=$row->last_name?>','<?=$row->phone?>','<?=$row->alt_phone?>','<?=$row->email?>','<?=$row->company?>','<?=$row->partner_code?>','<?=$row->id_passport_number?>','<?=$row->kra_pin?>')"> <i class="fa fa-edit"> </i> Edit </a>
		</td>
		<td>
		  <a href="<?=base_url();?>admin/partners/<?=$row->id?>"> <i class="fa fa-file-o"> </i> View Transaction</a>
		</td> 
	</tr>
	<?php $x++;
	endforeach;?>
</tbody>
</table>
 
 
</form>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
          
        </div>
        </div>
        </div> 
        <!-- END CONTAINER -->
 
<div id="edit"   class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"> 
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" id="task">Edit Partner </font></h4>
	</div>
<div class="modal-body">
<div class="row">
			<div class="col-md-6">   
		<div class="form-group">
			<label class="control-label">First Name </label> 
			<input type="text" id="e_fname" value="" class="form-control"   />
			<input type="hidden" id="e_partner_code" value="" class="form-control"   />
			<input type="hidden" id="edit_id" value="" class="form-control"   />
		</div>
		<div class="form-group">
			<label class="control-label">Phone </label>
			<input type="text" id="e_phone" value="" class="form-control" />
		</div>  
		<div class="form-group">
			<label class="control-label">Company </label>
			<input type="text" id="e_company" value="" class="form-control"  />
		</div> 
		
		<div class="form-group">
			<label class="control-label"> National Id </label>
			<input type="text" id="e_national_id" value="" class="form-control" />
		</div> 
		</div>
 <div class="col-md-6"> 
  
		<div class="form-group">
			<label class="control-label">Last Name </label>
			<input type="text" id="e_lname" value="" class="form-control"  />
		</div> 
  
		
		<div class="form-group">
			<label class="control-label">Alt Phone </label>
			<input type="text" id="e_alt_phone" value="" class="form-control" />
		</div> 
			 
 
	 <div class="form-group">
			<label class="control-label">Email </label>
			<input type="text" id="e_email" value="" class="form-control"  />
		</div> 
		<div class="form-group">
			<label class="control-label">KRA Pin </label>
			<input type="text" id="e_kra" value="" class="form-control"/>
		</div> 		 
		 
	</div> 
	</div> 
	 
		<font id="error_msg"> </font>
	<div class="modal-footer"> 
	
	<input type="submit" id="done_edit" class="btn green"value="Save Changes">
	<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
	</div>  
	  
</div>  
</div>  
</div>  
</div>  

<script language="javascript">
$(document).ready(function()
{ 

$("#save").click(function()
	{   
	 
		var fname=	$("#fname").val();
		var lname=	$("#lname").val();
		var mname=	$("#mname").val();
		var email=	$("#email").val();
        var company=$("#company").val();	
        var phone=$("#phone").val();	
        var alt_phone=$("#alt_phone").val();	
        var national_id=$("#national_id").val();	
        var kra=$("#kra").val();	
        if(!(email)||!(phone)||!(kra)||!(national_id)){ $("#error_message").html("<font color='red'> Fill all the required inputs </font>"); return false; }		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/addPartner/",
		 type:"POST",
		 async:false,
		 data:{
			 'fname':fname,
			 'lname':lname,
			 'mname':mname,
			 'company':company,
			 'email':email,
			 'phone':phone,
			 'alt_phone':alt_phone,
			 'kra':kra,
			 'id':national_id
		 },
		 success:function(data)
		 {  
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				$("#error_message").html(" <font color='green'> <i class='fa fa-check'></i> Partner registered successfully </font>");
				setTimeout(function(){
			  location.reload();
			 },3000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				$("#error_message").html(" <font color='red'><i class='fa fa-close'></i>  Partner Not registered successfully </font>");
				return false;
			 }
		 }
		  }
		 )	  
});

$("#done_edit").click(function()
	{   
	 $("#error_msg").empty(); 
		var id=	$("#edit_id").val();
		var code=	$("#e_partner_code").val();
		var fname=	$("#e_fname").val();
		var lname=	$("#e_lname").val();
		var email=	$("#e_email").val();
        var company=$("#e_company").val();	
        var phone=$("#e_phone").val();	
        var alt_phone=$("#e_alt_phone").val();	
        var national_id=$("#e_national_id").val();	
        var kra=$("#e_kra").val();	
        if(!(email)||!(phone)||!(kra)||!(national_id)){ $("#error_msg").html("<font color='red'> Fill all the required inputs </font>"); return false; }		
	    $("#error_msg").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/edit_Partner/",
		 type:"POST",
		 async:false,
		 data:{
			 'edit_id':id,
			 'partner_code':code,
			 'fname':fname,
			 'lname':lname,
			 'company':company,
			 'email':email,
			 'phone':phone,
			 'alt_phone':alt_phone,
			 'kra':kra,
			 'id':national_id
		 },
		 success:function(data)
		 { 
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				$("#error_msg").html(" <font color='green'> <i class='fa fa-check'></i> Partner details updated successfully </font>");
				setTimeout(function(){
			  location.reload();
			 },3000);			  
			 }
			 else
			 {
				$("#error_msg").empty();  
				$("#error_msg").html(" <font color='red'><i class='fa fa-close'></i>  Partner details not updated successfully </font>");
				return false;
			 }
		 }
		  }
		 )	  
});



});
 
 function edit_partner(id, first_name , last_name,  phone , alt_phone ,email,company, partner_code,national_id,kra)
{  
	$("#edit_id").val(id);
	$("#e_fname").val(first_name);
	$("#e_lname").val(last_name);
	$("#e_phone").val(phone);
	$("#e_email").val(email);
	$("#e_company").val(company);
	$("#e_alt_phone").val(alt_phone);
	$("#e_partner_code").val(partner_code);
	$("#e_national_id").val(national_id);
	$("#e_kra").val(kra);
	$("#edit").modal('show');
}
 


</script>