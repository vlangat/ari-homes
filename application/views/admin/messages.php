<?php  $count1=0;$count2=0;$count3=0;$count4=0; foreach($count_messages->result() as $msg){ 
if($msg->message_type=="inbox"){
$count1++;
}
if($msg->message_type=="sent"){
$count2++;
}
if($msg->message_type=="draft"){
$count3++;
}
if($msg->message_type=="trash"){
$count4++;
}
}


foreach($subscription->result() as $row){ $sms=$row->sms;$sent_sms=$row->sent_sms;  }
?>

<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
   
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE CONTENT BODY -->
	<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Pages</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Messages</span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER --> 
<div class="row">
<div class="col-lg-3">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value="7800"><?=$sms?></span>
				<small class="font-green-sharp">		</small>
			</h3>
			<small> Total SMS </small>
		</div>
		<div class="icon">
			<i class="fa fa-envelope-o"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:100%;" class="progress-bar progress-bar-success green-sharp">
				<span class="sr-only"> </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Subscribed SMS </div>
			 <div class="status-number"> 100% </div> 
		</div>
	</div>
</div>
</div>
<div class="col-lg-5">
<div class="dashboard-stat2">
	<div class="display">
		<div class="number">
			<h3 class="font-red-haze">
				<span data-counter="counterup" data-value="1349"> <?=$sent_sms?> </span>
			</h3>
			<small> Sent SMS </small>
		</div>
		<div class="icon">
			<i class="fa fa-comment">	</i>   
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?=$x=$sent_sms/$sms*100?>%;" class="progress-bar progress-bar-success red-haze">
				<span class="sr-only">   </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title">  No of  Sent SMS </div>
			<div class="status-number">  <?=$x?>%    </div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-4 ">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-purple-soft" style="color:green">
				<span style="color:green" data-counter="counterup" data-value="276"><?=$sms-$sent_sms?></span>
			</h3>
			<small>   Remaining SMS </small>
		</div>
		<div class="icon">
			<i class="fa fa-envelope"> </i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width: 57%;" class="progress-bar progress-bar-success purple-soft">
				<span class="sr-only"> </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Remaining SMS </div>
			<div class="status-number"> <?=100-$x?>% </div>
		</div>
	</div>
</div>
</div>
</div>
<div class="page-content-inner">
<div class="inbox">
<div class="row">
<div class="col-md-3">
	<div class="inbox-sidebar"><!--onclick="compose()"-->
		<a href="javascript:;" id="compose_msg"  data-title="Compose" class="btn red compose-btn btn-block">
			<i class="fa fa-edit"> </i> Compose New Message</a>
		<ul class="inbox-nav"> 
<li class="divider"></li>		
			<li>
				<a href="javascript:;" onclick="loadMessages('sent','','')" data-type="sent" data-title="Sent"> Sent 
				<span class="badge badge-success"><?=$count2?></span>
				</a>
				
			</li>
			<li class="divider"></li>
			<li>
				<a href="javascript:;" onclick="loadMessages('draft','','')" data-type="draft" data-title="Draft"> Draft
					<span class="badge badge-danger"> <?=$count3?> </span>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a href="javascript:;" onclick="loadMessages('trash','','')" class="sbold uppercase" data-title="Trash"> Trash
					<span class="badge badge-info">  <?=$count4?> </span>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a href="javascript:;"    data-title="Trash"> Out Box
					<span class="badge badge-info">  <?=$count4?> </span>
				</a>
			</li>
			<li class="divider"></li>
		</ul>
 
</div>
</div>
<div class="col-md-9">
<div class="inbox-body">
<div class="inbox-header">
	<h1 class="pull-left"> <font id="sms_title" size="3"> <font> </h1>
	<input type="hidden" id="out_of" value="10">
	<input type="hidden"  id="start_counter">
	<input type="hidden"  id="total_pages">
	<input type="hidden"  id="heading">
 
</div>
<div class="inbox-content">  
 
<table class="table table-striped table-advance table-hover">
    <thead>
        <tr>
            <th colspan="3"> 
                <div class="btn-group input-actions">
                    <a class="btn btn-sm blue btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> Actions
                        <i class="fa fa-angle-down"></i>
                    </a>
					<ul class="dropdown-menu"> 
                       <li>
                            <a href="javascript:;" id="delete">
                            <i class="fa fa-trash-o">		</i> Delete </a>
                        </li>
                    </ul>
                </div>
            </th>
            <th class="pagination-control" colspan="3">
                <span class="pagination-info">Sent Messages &nbsp;  <font id="start_val">  </font>-<font id="end_val">  </font> of <font> <?=$count2?> </font> </span>
                <a class="btn btn-sm blue btn-outline" onclick="previous_page()">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="btn btn-sm blue btn-outline" onclick="next_page()">
                    <i class="fa fa-angle-right"> </i>
                </a>
            </th>
        </tr>
    </thead>
    <tbody  id="msg_body">
	<!-- <?php   //foreach($messages->result() as $msg){ ?>
	<tr class="<?// =$msg->read?>" data-messageid="<?//=$msg->id?>" onclick="readMsg('<?//=$msg->id?>')">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
			</td>
            <td class="inbox-small-cells">
                <i class="fa fa-star"></i>
            </td>
            <td class="view-message hidden-xs">  <?// =$msg->name?> </td>
            <td class="view-message ">  <? //=$msg->message?>   </td>
            <td class="view-message inbox-small-cells">
               <!-- <i class="fa fa-paperclip"></i>-- 
            </td>
            <td class="view-message text-right"> <?//=$msg->date_sent?> </td>
        </tr> -->
		<?php  //}?>
         
    </tbody>
</table>
<p id="success"> </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>

 <div id="confirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Confirm Deletion</h4>
			</div>
			<div class="modal-body">
				<p>  Do you want to remove the selected Messages? </p>
			</div>
			<div class="modal-footer">
				<button class="btn default" data-dismiss="modal" aria-hidden="true">No</button>
				<button data-dismiss="modal" id="confirmDelete" class="btn blue">Yes</button>
			</div>
		 
</div>


<div id="compose_sms" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	 
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Compose New Message</h4>
			</div>
			<div class="modal-body">
 <input type="hidden" value="" id="msg_id">
    <div  >
     <label > To: </label> 
        <div> 
	 <select  class='selectpicker' multiple data-live-search='true' id='to'   title='Select Receipient...'>
		 <?php foreach($tenants->result() as $row){?>
			<option value="<?=ucfirst(strtolower($row->mobile_no))?>">
				<?php $name=$row->first_name." ".$row->last_name; if($name !=""){ echo $name.",". $row->property_name.",". $row->house_no."(".$row->mobile_no.")"; }else{ echo $row->company_name.",". $row->property_name.",". $row->house_no."(".$row->mobile_no.")";}?>
			</option>
			<?php } ?> 
	 </select>		
        </div>
    </div>   
	<p>   </p>
    <div class='inbox-form-group'> <label class='control-label'> Message   </label>
        <textarea class='inbox-editor inbox-wysihtml5 form-control' id='message' rows='12'>       </textarea>
    </div>
    <div>
	<p id="counter"> </p> <p id="error"> </p>
    </div> 
<div class="modal-footer">
		<button class='btn green' onclick='send()'>
	<i class='fa fa-check'></i> Send </button>
	<button class='btn default inbox-discard-btn' onclick='discard()'> Discard </button>
	<button class='btn default' onclick='draft()'> Draft </button>
	<button class="btn default" data-dismiss="modal" aria-hidden="true"> Close </button>
	 
</div>
		 
</div>
</div>
<!-- END CONTAINER -->
<script type="text/javascript">

function validate()
{
var chks = document.getElementsByName('checkbox[]');
var hasChecked = false;
for (var i = 0; i < chks.length; i++)
{
if (chks[i].checked)
{
hasChecked = true;
break;
}
}
if (hasChecked == false)
{
//alert("Please select at least one.");
jAlert('Please select at least one message to perform the action');
return false;
}
$("#confirmModal").modal('show');
return true;
}


$(function(){
$("#start_val").html(0);
var out_of="<?=$count2?>";
$("#end_val").html(10);	

loadMessages('sent',"0","10");
 if(out_of<10) {$("#end_val").html(out_of);	}
 $("#message").keydown(function(){ 
	var str = $("#message").val();
    var char_length = str.length; 
	var pages=1;
	var display_value=0;
	display_value=(160-char_length);
	if(char_length>160){pages=char_length/160;}
	pages=Math.ceil(pages);
	var x=pages-2;
	while(x>=0){ if(display_value<=0){display_value=display_value+160;} x--;  } 
	$("#counter").html(display_value+"/"+pages);
	$("#total_pages").val(pages)
  }); 
  
$("#delete").click(function(){  
validate();

  //return confirm("Are you sure you want to delete the message?");
 
 });
 

 $("#compose_msg").click(function(){
	  $("#compose_sms").modal('show');
  });
  $("#confirmDelete").click(function(){
				var favorite = [];
				$.each($("input[name='checkbox[]']:checked"), function(){            
					favorite=($(this).val()); 
					deleteMessage(favorite);
				});
	  location.reload();
 });

 }); 
 
 function deleteMessage(id)
 {
	 
	 $.ajax({
			url:"<?=base_url();?>admin/removeMessages/"+id,
			type:"post",
			success: function(data){ 
			var obj = JSON.parse(data);
			if (obj.result=="ok"){ 	 
                //jAlert('There was an error in deleting the messages');
			}
			else
			{ 
				jAlert('There was an error in deleting the messages');
			}
	}	
}	
)	
}

function loadMessages(msg_type,start_limit="",end_limit="")
{  
$("#success").empty();
if(start_limit==""){	start_limit=0;	}
if(end_limit==""){ 	end_limit=10;	}
$("#start_val").html(start_limit);
var out_of="<?=$count2?>";
if(end_limit>out_of){end_limit=out_of;}
$("#end_val").html(end_limit);
$("#start_counter").val(end_limit);  
$('#sms_title').html(msg_type.toUpperCase());
$('#heading').val(msg_type);
$('#msg_body').html("<tr> <td colspan='6'> Loading messages...</td> </tr>");
	$.ajax({
url:"<?=base_url();?>admin/getMessages/"+msg_type,
type:"post",
data:{
	'start':start_limit,
	'end':end_limit
},
success: function(data){ 
 var message_body=""; 
var obj = JSON.parse(data);  
if (obj.result=="ok"){ 	 
	var data = obj.data;  
   for(var i=0; i<data.length; i++)
   {
	var message = data[i]; 
	var id=message['id'];
	var content=message['message'];
	var from=message['sent_by'];
	var name=message['name'];
	var date=message['date_sent']; 
	var status=message['read']; var color="#000000";
	if(msg_type=="sent"||msg_type=="trash"||msg_type=="draft"){ status=""}
	if(status=="read"){ color="#000000"; status=""}
	message_body+="<tr class='"+status+"' data-messageid='1' > <td class='inbox-small-cells'>"+
	"<input name='checkbox[]' type='checkbox' id='checkbox[]' class='mail-checkbox' value='"+message['id']+"'> "+
	"</td>"+
           " <td class='inbox-small-cells' > <i class='fa fa-star'></i> </td> <td class='view-message hidden-xs'> "+name+" </td>"+
            "<td class='view-message ' onclick=\"readMsg('"+ message['id'] +"')\"> <a href='#' style='text-decoration:none;color:"+color +"' >"+content+" </a></td> <td class='view-message inbox-small-cells'>"+
             " </td> <td class='view-message text-right'> "+date+" </td> </tr>";
			 
		$("#msg_body").append(message_body);	
	}
	$("#msg_body").empty();
	$("#msg_body").html(message_body);
}else{ 
		$("#msg_body").empty();
		$("#msg_body").append("<tr> <td colspan='6' align='center'> No messages found </td></tr>");
	}
}
})
}

function readMsg(id)
{
	$('#msg_body').html("<tr> <td colspan='6'> Loading messages...</td> </tr>");
	var r=$('#heading').val(); 
	$.ajax({
url:"<?=base_url();?>admin/displayMessages/"+id,
type:"post",
success: function(data){ 
var obj = JSON.parse(data);
if (obj.result=="ok"){ 	 
	var data = obj.data; 
    var message_body=""; 
	  
   for(var i=0; i<data.length; i++)
   {
	var message = data[i]; 
	var id=message['id'];
	var content=message['message'];
	var from=message['sent_by'];
	var to=message['to'];
	var name=message['name'];
	var subject=message['subject']; 
	var date=message['date_sent']; 
	if(r=="draft"){ sendDraft(id,to,from,subject,content);   return false;} 
  var msg="";if(r=="sent"||r=='draft'||r=="trash"){   }else{msg="<button data-messageid='23' onclick=\"reply('"+from+"','"+subject+"')\" class='btn green reply-btn'> <i class='fa fa-reply'></i> Reply  </button>";}
	message_body+="<tr><td colspan='5' class='inbox-small-cells'><div class='inbox-view-info'>"+
			"<div class='row'> <div class='col-md-7'>"+
            "<img src='<?=base_url();?>images/me.jpg' height='40' width='40' class='inbox-author'>"+
            "<span class='sbold'> "+name+" </span>"+
            "<span>&#60;"+from+"</font>&#62; </span> to"+
            "<span class='sbold'> "+to+" </span> on "+date+" </div>"+
       "<div class='col-md-5 inbox-info-btn'>"+
          " <div class='btn-group'>"+ msg+ ""+
            "</div>"+
            "</div>"+
            "</div>"+
      "</div>"+ 
"<div class='inbox-view'>"+
   " <p>"+
       " "+content+" </p>"+ 
  "</div>"+
"<hr/>"+
"<div class='inbox-attached'>"+
   " <div class='margin-bottom-15'>"+
   
     "</div></div>" +
"	</td> </tr>";
   }		
		$("#msg_body").empty();
		$("#msg_body").append(message_body);
	//alert(content);
}
}
   })

}

function compose()
{
	var message_body="";
	message_body="<tr><td colspan='6'>"+
    "<div  >"+
     "<label > To: </label> <select  class='selectpicker' multiple data-live-search='true'    title='Select Receipient...'> <option> langat </option> <option> kipkoech </option><option> ben </option> </select> "+
       " <div class='controls controls-to'>"+ 
		"<input type='text' class='form-control' id='to'>"+ 
       " </div>"+
   " </div>"+   
	"<p>   </p>"+
    "<div class='inbox-form-group'><select  class='selectpicker' multiple data-live-search='true' title='Select Receipient...'> <option> langat </option> <option> kipkoech </option><option> ben </option> </select> "+
	 "  <label class='control-label'> Message   </label>"+
     "   <textarea class='inbox-editor inbox-wysihtml5 form-control' id='message' rows='12'></textarea>"+
    "</div>"+
	"<div class='inbox-compose-btn'>"+
	"<button class='btn green' onclick='send()'>"+
	"<i class='fa fa-check'></i> Send </button>"+
	"<button class='btn default inbox-discard-btn' onclick='discard()'>Discard</button>"+
	"<button class='btn default' onclick='draft()'> Draft </button>"+
	"</div>"+
	"</td> </tr>";
		$("#msg_body").empty();
		$("#msg_body").append(message_body);
}


function send(status){  	
	if(!status){ status="sent";}
	 var id=$("#msg_id").val();
	var to=$("#to").val();
	var body=$("#message").val(); 
	if(!to){ $("#error").html("<font color='red'>Receipient Number is Required</font>"); $("#to").focus(); return false;}
	if(!body){ $("#error").html("<font color='red'>Message is empty. Write your message </font>"); $("#message").focus(); return false;}
	$("#error").html("<font color='#006699'> Please waiting...</font>"); 
		 
 $.ajax(
{ 
		url:"<?=base_url();?>sms/send_sms",
		type:"POST",
		async:false,
		data:
		{
			'id':id,  
			'to':to,  
			'pages':$("#total_pages").val(),
			'message_type':status,
			'body':body
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				if(status=="inbox"){
					$("#success").html("<font color='green'> Message sent successfully </font>");
				}else{
					$("#success").html("<font color='green'> Message successfully saved as draft  </font>");
				}
				$("#to").val('');
			   $("#message").val('');
			   $("#cc").val('');
			   $("#subject").val('');
			   location.reload();
			}
			else{
				$("#success").html("<font color='red'> Error occurred! Please contact your administrator  </font>");
			}
				
		}
	
}
)
 
}

function send_saved_draft(){  
	var id=$("#draft_id").val();
	var to=$("#draft_to").val();
	var body=$("#draft_message").val();
	if(!to){ $("#draft_error").html("<font color='red'>Select at least one Receipient </font>"); $("#draft_to").focus(); return false;}
	if(!body){ $("#draft_error").html("<font color='red'>Message is empty. Write your message </font>"); $("#draft_message").focus(); return false;}
	//$("#draft_error").html("<font color='#006699'> Please waiting...</font>"); 
	var status="sent";
	$("#to").val(to);
	$("#message").val(body);
	$("#msg_id").val(id);	
	$("#compose_sms").modal('show');
 
}

function discard(){
   $("#to").val('');
   $("#message").val('');
   $("#cc").val('');
   $("#subject").val('');
	
}

 function draft(){
	 var body=$("#message").val();
	 var to=$("#to").val();
	 var status="draft";
	var id=$("#draft_id").val();
	 $.ajax(
	{ 
		url:"<?=base_url();?>admin/draft_sms",
		type:"POST",
		async:false,
		data:
		{
			'id':id,
			'to':to,  
			'message_type':status,
			'body':body
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				if(status=="inbox"){
					$("#success").html("<font color='green'> Message sent successfully </font>");
				}else{
					$("#success").html("<font color='green'> Message successfully saved as draft  </font>");
				}
		$("#to").val('');
	   $("#message").val('');  
	   location.reload();
			}
			else{
				$("#success").html("<font color='red'> Error occurred! Please contact your administrator  </font>");
			}
				
		}
	
}
) 	
	
}

function reply(from,subject)
{
	var message_body="";
	message_body="<div class='inbox-form-group'>"+
    " <div class='controls-row'>"+
    "<input type='hidden' id='text_to' value='"+from+"'>"+
    "<input type='hidden' id='text_subject' value='"+subject+"'>"+
    "<textarea class='inbox-editor inbox-wysihtml5 form-control' id='reply_text' rows='8' cols='60'></textarea>"+
    "<div class='inbox-compose-btn'>"+
	"<button class='btn green' onclick='replyMessage()'>"+
	"<i class='fa fa-check'></i> Reply </button>"+ 
	"</div>"+
	"</div>"+
   " </div>"; 
		$("#msg_body").append(message_body);
}

function replyMessage()
{
	var to = $("#text_to").val();
	var reply_text=$("#reply_text").val();
	var subject=$("#text_subject").val();
	  if(!reply_text){ $("#success").html("<font color='red'>Reply Message Required</font>");return false;}
      $("#success").html("<font color='#006699'> Please waiting...</font>");
$.ajax(
{ 
		url:"<?=base_url();?>admin/composeMail/reply",
		type:"POST",
		async:false,
		data:
		{
			'to':to,
			'subject':subject,
			'cc':'',
			'message_type':'reply',
			'body':reply_text
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				 
					$("#success").html("<font color='green'> Message replied successfully </font>");
				 
		$("#to").val('');
	   $("#message").val('');
	   $("#cc").val('');
	   $("#subject").val('');
	   location.reload();
			}
			else{
				$("#success").html("<font color='red'> Error occurred! Please contact your administrator  </font>");
			}
				
		}
	
}
)
	
}

function sendDraft(id,to,from,subject,content)
{ 
	var message_body="";
	message_body="<tr> <td colspan='6'>  "+
    "<div class='inbox-form-group mail-to'>"+
     "   <label class='control-label'>To:</label>"+
       " <div class='controls controls-to'>"+
         "   <input type='text' class='form-control' id='draft_to' value='"+to+"'>"+ 
         "   <input type='hidden' class='form-control' id='draft_id' value='"+id+"'>"+ 
       " </div>"+
   " </div>"+ 
	  
	"<p>   </p>"+
    "<div class='inbox-form-group'>"+
     "   <textarea class='inbox-editor inbox-wysihtml5 form-control' id='draft_message' rows='12'> "+content+" </textarea>"+
    "</div><p id='draft_error'></p>"+
	"<div class='inbox-compose-btn'>"+
	"<button class='btn green' onclick='send_saved_draft()'>"+
	"<i class='fa fa-check'></i> Send </button>"+ 
	"<button class='btn default' onclick='saveDraft()'> Save Draft </button>"+
	"</div>"+
	"</td> </tr>";
		$("#msg_body").empty();
		$("#msg_body").append(message_body);
}


function saveDraft(){  	 
	var id=$("#draft_id").val();
	var to=$("#draft_to").val();
	var body=$("#draft_message").val();
	if(!body){ alert('You cannot save empty message');return false;}
	$("#draft_error").empty();  
$.ajax(
    { 
		url:"<?=base_url();?>admin/saveDraft",
		type:"POST",
		async:false,
		data:
		{
			'to':to,
			'id':id,  
			'message_type':'draft',
			'body':body
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				 
					 $("#success").empty();
					$("#success").html("<font color='green'> Draft saved successfully  </font>");
				 
			}
			else{
				$("#success").html("<font color='red'> Error occurred! Please contact your administrator  </font>");
			}
				
		}
	
}
)
	
	
}
function next_page()
{ 
	var from=$("#start_counter").val();
	var limit=parseInt(from)+10;
	$("#sms_title").html('SENT'); 
	loadMessages('sent',from,limit);  
} 
function previous_page()
{ 
	var limit=$("#start_counter").val();
	var from=parseInt(from)-10;
	$("#sms_title").html('SENT'); 
	loadMessages('sent',from,limit);  
} 
</script>

