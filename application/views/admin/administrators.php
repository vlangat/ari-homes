<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Users </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Administrators </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
  
<?php $u=0; $new=0; foreach($users->result() as $user){
		$u++; 
		$now=time(); $diff_time=$now-strtotime($user->registration_date);  $days=floor($diff_time/(60*60*24));
		if($days<=7)
		{ 
			$new++;  
		}
}
?> 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar">
 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th>
							<th> Name  </th>
							<!--<th> Company </th> -->
							<th> Email</th>  
							<th> Mobile </th>  
							<th> Date Registered </th> 
							<th> Package  </th> 
							<th> Last Login </th> 
							<th> Status </th> 
							<th> &nbsp; </th> 
							<th> &nbsp; </th> 
						</tr>
					</thead>
					<tbody>
						 <?php $i=1; foreach($users->result() as $user){ ?>
							 
							 <?php $user_type="Free"; $package_id=0;$x=0;
								foreach($packages->result() as $p){ 
										if($p->company_code==$user->company_code)
										{
										 $package_id=$p->package_id; $x++;
										} 
										$user_type="Registered";
									}
							//if($package_id==1){ $user_type="Professional"; }if($package_id==2){ $user_type="Enterprise"; }
							if($x==0){ 	continue;	}

				$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
				$enct =base64_encode(do_hash($user->id  . $salt,'sha512') . $salt);							
							?>
						<tr>
							<td> <?=$i?> </td>
							<td><?=$user->first_name?> <?=$user->last_name?></td>
							<!--<td><?php /*$company_name="";
							foreach($company_info->result() as $c)
							{ 
								if($user->company_code==$c->company_code)
								{
									 $company_name=$c->company_name ;
								}
							}
							?>
							<?=$company_name?>*/?>
							</td>-->
							<td> <?=$user->email?> </td>
							<td> <?=$user->mobile_number?> </td>
							   
							<td> <?=$user->registration_date?> </td>
							<td> <?php echo $user_type;	 ?> </td> 
							<td> <?=$user->last_login?> </td>
							 <?php $status="Active"; $color="green";
								if($user->user_enabled==2){
									$status="Inactive"; $color="red"; 
								}?>
							<td>
								<a href="javascript:;"  onclick="activate('<?=$user->id?>','<?=$user->user_enabled?>')" id="activate" style="color:<?=$color?>"> <?=$status?> </a>
							</td> 
							<td>
								<a href="<?=base_url();?>admin/user_privileges/<?=$enct?>" style="color:blue"> Permission </a>
							</td> 
							<td>
								<a href="javascript:;" style="color:blue" onclick="reset_account('<?=$user->id?>','<?=$user->company_code?>')"> Reset Account </a>
							</td> 
						</tr>
						 <?php $i++; 
						 
						 }?>  
						 
					</tbody>
				</table>
				<button class='btn green' id="add_btn">
			<i class='fa fa-plus'> </i> Add New </button> 
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>

<div id="reset_account" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title"> Activate User </h4>
			</div>
			<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				
			<div class="col-md-12">  
						 <p>
						 <input type="hidden" value="" id="activation_id">
						 <input type="hidden" value="" id="user_id">
						 <input type="hidden" value="" id="company_code">
						 Are you sure you want to reset password for selected account?
					   </p>
			</div>
			</div>  <span id="error_msg">   </span>
			</div>
			<div class="modal-footer" >
					<input type="submit" class="btn green"  id="submit" value="Yes"> 
					<button type="button" data-dismiss="modal" class="btn btn-outline dark" > No </button>
			</div> 
			</div>
</div>
</div>
</div>


<div id="responsive_2" class="modal fade" tabindex="-1" aria-hidden="true">
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" id="task"> Add User </font></h4>
	</div>
<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div  class="form-horizontal">
				<div class="form-group">
					<div class="col-md-12">
					First Name:
					<input type="text"  class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off"required="required"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="First_Name"  />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					Second Name:
					<input type="text" id="Last_Name"  class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"   />
					</div>
				</div>
			<div class="form-group">
			<div class="col-md-12">
				Email:
				<input type="email" id="email"class="form-control todo-taskbody-tasktitle" required="required" autocomplete="off"     onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" />
			</div>
	</div>
	<div class="form-group">
			<div class="col-md-12">
				Phone Number:
				<input type="number" id="phone"class="form-control todo-taskbody-tasktitle" required="required" autocomplete="off"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" />
			</div>
	</div>
		<div class="form-group">
			<div class="col-md-12">    
				</div>
			<input type="hidden" id="added_by"class="form-control todo-taskbody-tasktitle"value="<?php echo $this->session->userdata('id');?>" placeholder="">
		</div>
	</div>
	</div>
</div>
</div><font id="add_user"> </font>
<div class="modal-footer"> 
<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
<input type="submit" id="add_new_user" class="btn green"value="Save Changes">
</div>   
</div>
</div> 
</div> 
  
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
	<i class="icon-login"></i>
</a>

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

<script language="javascript">
 
function activate(id, status)
	{ 
	var msg="Deactivate";	
	if(status==2){ msg="Activate";}
	
	var x=confirm("Are you you to "+msg+" this user?");
	if(x==true){  
	  	if(status==1){ status=2;}else{ status=1;}		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/activate/"+id+"/"+status,
		 type:"POST",
		 async:false,
		 success:function(data)
		 {  
			var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 setTimeout(function(){
				location.reload();
			 },2000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				alert(" Changes to the user details not made ");
				return false;
			 }
		 }
		  }
		 )	
	}
	else{ return false;}	
}
	
function reset_account(user_id,company_code)
{  
	$("#title").html('<font> Reset Account </font>'); 
	$("#user_id").val(user_id);  
    $("#company_code").val(company_code);  
	$("#reset_account").modal('show'); 
} 
 
$(function(){  

$("#add_btn").click(function()
{ 
    $("#responsive_2").modal('show');   
});
	$("#submit").click(function(){     
	var user_id=$("#user_id").val(); 
	var company_code=$("#company_code").val();  
	if(!company_code){   return false;} 
	 
	$.ajax(
	{ 
			url:"<?=base_url();?>admin/reset_admin_account/",
			type:"POST",
			async:false,
			data:
			{  
				'user_id':user_id,
				'company_code':company_code
			},			
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					 setTimeout(function()
					 {
						location.reload();
					 },2000);
				} 
			}	 
	})
 });
    
	
	$("#add_new_user").click(function()
	{  
		var email=$("#email").val();
		var phone=$("#phone").val();
		var fname=$("#First_Name").val();
		var lname=$("#Last_Name").val();  	 
		if(!fname){   $("#First_Name").focus();  $("#add_user").html("<font color='red'> First Name required </font>"); return false;} 		
		if(!lname){   $("#Last_Name").focus();  $("#add_user").html("<font color='red'> Last Name required </font>"); return false;} 		
		if(!email){   $("#email").focus();  $("#add_user").html("<font color='red'> Email required </font>"); return false;} 		
		if(!phone){   $("#phone").focus();  $("#add_user").html("<font color='red'> Phone Number required </font>"); return false;} 		
		if(email&&fname&&lname){ } else{$("#add_user").html("<font color='red'> Empty fields not required </font>"); return false;}
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#add_user").html("<font color='red'> Email provided is not a valid e-mail address"); return false;}
		$("#add_user").html("<font color='blue'> Adding new user....</font>");
		$.ajax({
		 url:"<?=base_url();?>admin/addUser",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'lname':lname, 
		   'email':email, 
		   'phone':phone 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){
				 $("#First_Name").val('');$("#Last_Name").val(''); $("#email").val('');
				  $("#phone").val('');
				 $("#add_user").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>"+obj.msg+"  </font>"); 
                 getAgents(); 
                $("#user_id").val(obj.user_id); 
					setTimeout(function(){
					$('#add_user').empty();
					$('#responsive_2').modal('hide');
					$("#privileges").modal('show');
				}, 3000);				
					
			 }
			 else{
				 $("#add_user").html("<font color='red'> "+obj.msg+"</font>");return false;
			 }
		 }
		 
		  }
		 )	  
});
});
 
</script>