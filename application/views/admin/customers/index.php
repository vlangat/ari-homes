 <?php $i=0;$converted=0;
      foreach($customer_info->result() as $p){ 
      foreach($users->result() as $u){ if($p->email==$u->email){$converted++; }}
     $i++; } 
        $d=0; foreach($demos->result() as $dem){ $d++; }
        
  ?>
 <!-- BEGIN PAGE CONTENT BODY -->
 <script src="<?=base_url();?>js/autocomplete.js"></script> 
   <script>
$(function() {
  
	  $("#nameSearch").autocomplete({
      source: '<?=base_url();?>customers/getCustomers'
    }); 
 $("#nameSearch").autocomplete( "option", "appendTo", "#modal-body" );

});
</script>
 
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Customers </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 <div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-green-sharp">
						<span data-counter="counterup"  data-value="<?=$i;?>"> 0 </span>
						<small class="font-green-sharp"> </small>
					</h3>
					<small> POSSIBLE CUSTOMERS</small>
				</div>
				<div class="icon">
					<i class="fa fa-users"> </i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
						<span class="sr-only">76% progress</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> progress </div>
					<div class="status-number"> 76% </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-red-haze">
						<span data-counter="counterup" data-value="<?php echo $i;?>">0</span>
					</h3>
					<small> TOTAL CALLS </small>
				</div>
				<div class="icon">
					<i class="icon-home"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
						<span class="sr-only">85% change</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> change </div>
					<div class="status-number"> 85% </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-blue-sharp">
						<span data-counter="counterup" data-value="<?php echo $d;?>"> 0 </span>
					</h3>
					<small> DEMO  </small>
				</div>
				<div class="icon">
					<i class="icon-users"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 45%;" class="progress-bar progress-bar-success blue-sharp">
						<span class="sr-only">45% grow</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> grow </div>
					<div class="status-number"> 45% </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-purple-soft">
						<span data-counter="counterup" data-value="<?php echo $converted;?>"> 0</span>
					</h3>
					<small>CONVERTED</small>
				</div>
				<div class="icon">
					<i class="icon-user"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 57%;" class="progress-bar progress-bar-success purple-soft">
						<span class="sr-only">56% change</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> change </div>
					<div class="status-number"> 57% </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12"> 
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<p>&nbsp;</p>
				<div class="table-toolbar"></div>
				<table class="table table-striped table-hover table-bordered" id="sample_editable">
					<thead>
						<tr>
							<th>#</th> 
							<th> Status </th>
							<th> Company </th>
							<th> ContactPerson</th> 
							<th> Email</th> 
							<th> Phone</th>  
							<!--<th> DateAdded </th>							
							<th> AddedBy </th>							
							<!--<th> Remarks </th>
							<th> Status </th> -->
							<th> Action </th> 							
							<th> Edit </th>   
							<th> Delete </th>   
						</tr>
					</thead>
					<tbody>
						 <?php $x=1;
						 foreach($customer_info->result() as $p){  $total=0;?>
						<tr>
							<td><?=$x?> </td>
							<td> 
							<?php if($p->comment=="demo"){?> 
							<span class="badge badge-warning">&nbsp;</span> Demo
							<?php }else if($p->comment=="call again")
							{?>
								<span class="badge badge-info">&nbsp;</span>CallAgain
						<?php }
							else if($p->comment=="no response")
							{?>
								<span class="badge badge-danger">&nbsp;&nbsp;</span> NoResponse
						<?php }else if($p->comment=="not interested"){?>
								<span class="badge badge-danger">&nbsp;</span> NotInterested
							 
							<?php }else{?>
								<span class="badge badge-success">&nbsp;&nbsp;</span>
								To Call
							<?php }?>
							</td>							
							<td><?=$p->company?></td>
							<td> <?=$p->contact_person?> </td> 
							<td><?=$p->email?> </td>
							<td><?=$p->phone?> </td>   
							<!--<td> <?php
							//=$p->remarks
							?> </td>-->
							<!--<td> 
							<?//=$p->status?> 
							</td>-->
							<td> 
								<select class="form-control" id="action_<?=$p->id?>" onchange="updateStatus('<?=$p->id?>','<?=$p->company?>')">
								<option>--select--</option>
								<option>Demo</option>
								<option>Call Again</option>
								<option>No Response</option>
								<!--<option>Not Interested</option>-->
							</select>
 							</td> 
							<td>
								<a   href="javascript:;" onclick="edit('<?=$p->id?>','<?=$p->company?>','<?=$p->phone?>','<?=$p->contact_person?>','<?=$p->email?>','<?=$p->remarks?>','<?=$p->location?>')"> <i class="fa fa-edit"> </i>  Edit </a>
							</td>
							<td>
								<a   href="javascript:;" onclick="deleteCustomer('<?=$p->id?>')"> <i class="fa fa-trash"> </i>  Delete </a>
							</td> 
						</tr>
						 <?php $x++; //$total=$total+$amount;
						 }
						 ?>  
						 
					</tbody>
				</table>
				<button class='btn green' id="add_new">
			<i class='fa fa-plus'> </i> Add New </button> 
			
			<input type="hidden" value="" id="ActionId">
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div> 
    
<div id="add_customer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Add Possible Customers</h4>
			</div>
			<div class="modal-body">
	<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
	<div class="row">
	<div class="col-md-12" id="modal-body">  
				 <p>
					<label class="control-label">Company * </label>
					<input   class="form-control" type="text" id="nameSearch"  name="name"/> 
					<input   class="form-control" type="hidden" value="" id="edit_id">
				</p>
				<p>
				<label class="control-label"> Location * </label>
					<input class="form-control" type="text" id="location"  name="location" onchange="checkChar('name')" autocomplete="on" /> 
					<input   class="form-control" type="hidden" value="" id="edit_id">
				
				</p>
				 
			<p>
			<label class="control-label">Contact Person   </label>
				<input   class="form-control" type="text" onchange="checkChar('contact_person')" id="contact_person"  name="contact_person"> </p>
			  
			 <p>
			<label class="control-label">Phone No * </label>
				<input   class="form-control" type="text" id="phone"  onkeypress="return checkIt(event)"   name="phone"> </p>
			 <p>
			<label class="control-label">Email *</label>
				<input   class="form-control" type="email" id="email"   name="email"> </p>
			 
			<!-- <p>
			 <label class="control-label"> Email to Send </label>
			<select  class="form-control" id="email_type" name="email_type"> 
				<option value="email_1">Online Real Estate Management Solution </option>
				<!--<option value="email_2">Email 1</option>
				<option value="email_3">Email 1</option>
				<option value="email_4">Email 1</option>-->
			<!--</select>
			<input type="hidden" class="form-control" id="remarks" value="" name="remarks" ></textarea>
			</p>-->
		</div>
		</div> 
		<span id="add_msg">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="submit" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>

    
<div id="add_action" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Schedule  Demo  </h4>
				<input type="hidden" class="form-control" id="demo_id" value="" > 
				<input type="hidden" class="form-control" id="customer_name" value="" > 
			</div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				<div class="col-md-12">  
				 <p>
				<div class="form-group"> 
					<label class="control-label"> Demo Date </label>
					<input type="text" required placeholder="<?php echo date('d/m/Y')?>" class="form-control input-xxlarge date-picker" data-date-format="dd/mm/yyyy" data-date-start-date="-0d" value="<?=date('d/m/Y')?>" data-date-format="dd/mm/yyyy"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Demodate" name="Demodate" />
				</div> </p>
				<p>
			 <div class="form-group">
				<label > Time: </label> 
				<input type="text" id='time' class="form-control timepicker timepicker-24"  placeholder="<?=date('H:i')?>">
				<p>   </p>		
			</div>
			 </p>
			 <p>
			 <label class="control-label"> Assign To </label> 
		 <select id="assign_to" class="form-control">
			<?php foreach($users->result() as $r){?>
			
			    <option value="<?=$r->email?>"><?=$r->first_name?> <?=$r->middle_name?> <?=$r->last_name?></option>
			     
			<?php }?>
			
			</select>
			</p>
			 <p>
			 <label class="control-label"> Your Comments (Optional)</label> 
			<textarea type="text" class="form-control" id="remarks" value="" ></textarea>
			</p>
		</div>
		</div>  <span id="demo_msg">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="save_demo" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>


<div id="add_action2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Call Again</h4> 
			</div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				<div class="col-md-12">  
				 <p>
				<div class="form-group"> 
					<label class="control-label">Date to Call Again </label>
					<input type="text" required class="form-control  input-xxlarge date-picker" value="<?=date('m/d/Y')?>" data-date-format="dd/mm/yyyy"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="date_to_call_again" name="Demodate" />
				</div>
				</p>				
				 <p>
				 <label class="control-label"> Your Comments (Optional)</label> 
				<textarea type="text" class="form-control" id="comment2" value="" ></textarea>
				</p>
			</div>
			</div> 
			<span id="error2">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="save2" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>

<div id="add_action3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Not Interested</h4> 
			</div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				<div class="col-md-12">  				
				 <p>
				 <label class="control-label"> Reason for not Interested</label> 
				<textarea type="text" class="form-control" id="comment3" value="" ></textarea>
				</p>
			</div>
			</div> 
			<span id="error3">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="save3" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>

<!-- END CONTAINER -->
<script language="javascript">
$(function(){ 
//loadMessages('sent');
var c="<?=$x?>";
$("#total_customers").val(c);
$("#add_new").click(function()
{  
	$("#email").val(''); $("#contact_person").val(''); $("#name").val(''); $("#phone").val(''); $("#remarks").val('');
	$("#add_msg").empty();
	$("#add_customer").modal('show'); 
});
 
 
 $("#submit").click(function(){   
	var contact_person=$("#contact_person").val();
	var amount=$("#amount").val();
	var name=$("#nameSearch").val();
	var phone=$("#phone").val(); 
	var id=$("#edit_id").val() 
	var email=$("#email").val() 
	var remarks=$("#remarks").val();
	var email_type=$("#email_type").val();
	var location=$("#location").val();
	if(!name){ $("#add_msg").html("<font color='red'> Company Name  is Required</font>");  $("#name").focus(); return false;}
	//if(!contact_person){ $("#add_msg").html("<font color='red'>Contact Person is Required</font>");  $("#contact_person").focus(); return false;}
	if(!phone){ $("#add_msg").html("<font color='red'>Phone Number is Required</font>"); $("#phone").focus(); return false;}
	if(!email){ $("#add_msg").html("<font color='red'>Email is Required</font>"); $("#email").focus(); return false;}
	if(! email){ }else
	 {
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { alert("Email provided is invalid");   $("#email").focus(); return false;}
	 }
	 if(!phone){ $("#add_msg").html("<font color='red'>Mobile Number is Required</font>"); $("#phone").focus(); return false;}
	
	$("#add_msg").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/add_customer",
			type:"POST",
			async:false,
			data:
			{
				'edit_id':id,  
				'contact_person':contact_person, 
				'company':name,
				'phone':phone,
				'email':email,
				'location':location,
				'email_type':email_type,
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{
					if(id){ }else{
						$("#code").val(''); $("#amount").val(''); $("#name").val(''); $("#phone").val(''); $("#remarks").val('');
					}
					$("#add_msg").html("<font color='green'> Data saved successfully </font>");
						setTimeout(function(){
						  $("#add_msg").empty();
						  $("#add_new").modal('hide');
						 window.location.replace("<?=base_url();?>customers/");
                      }, 2500);
				}
				else
				{
					$("#add_msg").html("<font color='red'> Customer details <b> Not </b> added. Check if customer with the same details exists </font>"); 
				} 
			} 
	})
 
 });
  
  
  $("#save_demo").click(function(){
 	  
	var assign_to=$("#assign_to").val();
	var date=$("#Demodate").val(); 
	var time=$("#time").val();
	var id=$("#ActionId").val();
	var customer_name=$("#customer_name").val();
	var remarks=$("#remarks").val(); 
	if(!date){ $("#demo_msg").html("<font color='red'> Demo date Required</font>");  $("#name").focus(); return false;}
	if(!time){ $("#demo_msg").html("<font color='red'> Time Required</font>");  $("#name").focus(); return false;}
	if(!assign_to){ $("#demo_msg").html("<font color='red'> Person Assigned  is Required</font>");  $("#name").focus(); return false;}
	 
	$("#demo_msg").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/add_demo_request",
			type:"POST",
			async:false,
			data:
			{
				'id':id,   
				'customer_name':customer_name,   
				'demo_date':date,
				'time':time,
				'assign_to':assign_to,
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					$("#assign_to").val(''); $("#Demodate").val('');  $("#remarks").val('');
					$("#demo_msg").html("<font color='green'> Data saved successfully </font>");
					setTimeout(function(){
					  $("#demo_msg").empty();
					  $("#add_new").modal('hide');
					 window.location.replace("<?=base_url();?>customers/");
					}, 2500);
				}
				else
				{
					$("#demo_msg").html("<font color='red'> Record  <b> Not </b> saved. </font>"); 
				} 
			} 
	})
 
});
  
  
   $("#save2").click(function(){     
	var date=$("#date_to_call_again").val(); 
	 
	var id=$("#ActionId").val();
	var remarks=$("#comment2").val(); 
	if(!date){ $("#error2").html("<font color='red'> Date Required</font>");  $("#date_to_call_again").focus(); return false;}
	   
	$("#error2").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/call_again",
			type:"POST",
			async:false,
			data:
			{
				'id':id,   
				'date':date,  
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					$("#date_to_call").val('');  $("#comment2").val('');
					$("#error2").html("<font color='green'> Data saved successfully </font>");
					setTimeout(function(){
					  $("#add_msg").empty();
					  $("#add_action2").modal('hide');
					 window.location.replace("<?=base_url();?>customers/");
					}, 2500);
				}
				else
				{
					$("#error2").html("<font color='red'> Record  <b> Not </b> saved. </font>"); 
				} 
			} 
	})
 
 });
  
   $("#save3").click(function(){     
	 
	var id=$("#ActionId").val();
	var remarks=$("#comment3").val(); 
	if(!remarks){ $("#error3").html("<font color='red'>Reason why not interested </font>");  $("#comment2").focus(); return false;}
	  
	$("#error3").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/not_interested",
			type:"POST",
			async:false,
			data:
			{
				'id':id,    
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					  $("#comment3").val('');
					$("#error3").html("<font color='green'> Data saved successfully </font>");
					setTimeout(function(){
					  $("#error3").empty();
					  $("#add_action3").modal('hide');
					 window.location.replace("<?=base_url();?>customers/");
					}, 2500);
				}
				else
				{
					$("#error3").html("<font color='red'> Record  <b> Not </b> saved. </font>"); 
				} 
			} 
	})
 
 });
  
   
 
   $("#save3").click(function(){     
	 
	var id=$("#id").val();
	var remarks=$("#comment3").val(); 
	if(!remarks){ $("#error3").html("<font color='red'>Reason why not interested </font>");  $("#comment2").focus(); return false;}
	  
	$("#error3").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/not_interested",
			type:"POST",
			async:false,
			data:
			{
				'id':id,    
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					  $("#comment3").val('');
					$("#error3").html("<font color='green'> Data saved successfully </font>");
					setTimeout(function(){
					  $("#error3").empty();
					  $("#add_action3").modal('hide');
					 window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
					$("#error3").html("<font color='red'> Record  <b> Not </b> saved. </font>"); 
				} 
			} 
	})
 
 });
  
  
  
 });
 
 
 function deleteCustomer(id)
 {
	 var c=confirm("Delete this customer?");
	 if(c==true)
	 {
		  $("#err").html("<font color='#006699'> Please waiting...</font>"); 
		  
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/deleteCustomer/"+id,
			type:"POST",
			async:false, 
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					 $("#err").html("<font color='green'> Record removed successfully </font>");
					setTimeout(function(){
					  $("#err").empty(); 
					  window.location.reload();
					 //window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
					alert(" Record Not removed. </font>"); 
				} 
			} 
	})
	 }
	 else
	 {
		return c;  
	 }
 }
 
 
 
 function updateStatus(id,customer_name)
 {  
   $("#ActionId").val(id);
   var v=$("#action_"+id).val(); 
   $("#customer_name").val(customer_name); 
   if(v=="Call Again")
   {
	   $("#add_action2").modal('show');
   }
   else if(v=="No Response")
   {
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/no_response",
			type:"POST",
			async:false,
			data:
			{
				'id':id    
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					 setTimeout(function(){
					  window.location.replace("<?=base_url();?>customers/");
					}, 2500);
				}
				else
				{
				   alert('Record not updated');
				} 
			} 
	})
   }
   else if(v=="Not Interested")
   {
	    $("#add_action3").modal('show');
   }
   else
   {
	   $("#add_action").modal("show");
   }
 }
 
 
 function edit(id,name,phone,contact_person,email,remarks,location)
 {  
	$("#title").html('<font> Edit Customer Details </font>');
	$("#edit_id").val(id);$("#contact_person").val(contact_person); $("#location").val(location); $("#email").val(email); $("#name").val(name); $("#phone").val(phone); $("#remarks").val(remarks);
	$("#add_customer").modal('show'); 
 } 
 
 	$(document).ready(function() {
		 
    var printCounter = 0;
 
    // Append a caption to the table before the DataTables initialisation
    $('#sample_editable').append('<caption style="caption-side: bottom">End of Report</caption>');
 
    $('#sample_editable').DataTable({
         dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5 ]
                }
            },
            'colvis'
        ]
    } );
} );

</script> 