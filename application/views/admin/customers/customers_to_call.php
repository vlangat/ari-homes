 <?php $i=0; foreach($customer_info->result() as $p){ $i++; }?>
 <!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Customers </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Calls </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar"> 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th> 
							<th> Status </th>
							<th> Company  </th>
							<th> Contact Person</th> 
							<th> Email</th> 
							<th> Phone No</th>  
							<th> Date to Call </th>		 
							<th> Action </th>  
						</tr>
					</thead>
					<tbody>
						 <?php $x=1;
						 foreach($customer_info->result() as $p){  $total=0;?>
						<tr>
							<td><?=$x?> </td>
							<td> 
							<?php if($p->comment=="demo"){?> 
							<span class="badge badge-warning">&nbsp;&nbsp;</span> Demo
							<?php }else if($p->comment=="call Again")
							{?>
								<span class="badge badge-info">&nbsp;&nbsp;</span>
					  <?php }
							else if($p->comment=="no response")
							{?>
								<span class="badge badge-danger">&nbsp;&nbsp;</span> No Response
						<?php }else if($p->comment=="not interested"){?>
								<span class="badge badge-danger">&nbsp;&nbsp;</span>
							 
							<?php }else{?>
								<span class="badge badge-success">&nbsp;&nbsp;</span>
								To Call
							<?php }?>
							</td>
							<td><?=$p->company?></td>
							<td><?=$p->contact_person?></td> 
							<td><?=$p->email?> </td>
							<td><?=$p->phone?> </td>  
							<td> <?=$p->date_to_call?> </td> 
							<td>
							<select class="form-control" id="action_<?=$p->id?>" onchange="updateStatus('<?=$p->id?>','<?=$p->company?>')">
								<option>--select--</option>
								<option>Demo</option>
								<option>Call Again</option>
								<option>No Response</option>
								<option>Not Interested</option>
							</select>	 
							</td> 
						</tr>
						 <?php $x++; //$total=$total+$amount;
						 }
						 ?>  
						 
					</tbody>
				</table>
				  
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
  

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div> 
    
<div id="add_action" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Schedule  Demo  </h4>
				<input type="hidden" class="form-control" id="id" value="" > 
				<input type="hidden" class="form-control" id="customer_name" value="" > 
			</div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				<div class="col-md-12">  
				 <p>
				<div class="form-group"> 
					<label class="control-label"> Demo Date </label>
					<input type="text" required placeholder="<?php echo date('d/m/Y')?>" class="form-control input-xxlarge date-picker" data-date-format="dd/mm/yyyy" data-date-start-date="-0d" value="<?=date('d/m/Y')?>" data-date-format="dd/mm/yyyy"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Demodate" name="Demodate" />
				</div> </p>
				<p>
			 <div class="form-group">
				<label > Time: </label> 
				<input type="text" id='time' class="form-control timepicker timepicker-24"  placeholder="<?=date('H:i')?>">
				<p>   </p>		
			</div>
			 </p>
			 <p>
			 <label class="control-label"> Assign To </label> 
		 <select id="assign_to" class="form-control">
			<?php foreach($users->result() as $r){?>
			
			    <option value="<?=$r->email?>"><?=$r->first_name?> <?=$r->middle_name?> <?=$r->last_name?></option>
			     
			<?php }?>
			
			</select>
			</p>
			 <p>
			 <label class="control-label"> Your Comments (Optional)</label> 
			<textarea type="text" class="form-control" id="remarks" value="" ></textarea>
			</p>
		</div>
		</div>  <span id="add_msg">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="submit" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>


<div id="add_action2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Call Again</h4> 
			</div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				<div class="col-md-12">  
				 <p>
				<div class="form-group"> 
					<label class="control-label">Date to Call Again </label>
					<input type="text" required class="form-control  input-xxlarge date-picker" value="<?=date('m/d/Y')?>" data-date-format="dd/mm/yyyy"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="date_to_call_again" name="Demodate" />
				</div>
				</p>				
				 <p>
				 <label class="control-label"> Your Comments (Optional)</label> 
				<textarea type="text" class="form-control" id="comment2" value="" ></textarea>
				</p>
			</div>
			</div> 
			<span id="error2">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="save2" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>

<div id="add_action3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Not Interested</h4> 
			</div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				<div class="col-md-12">  				
				 <p>
				 <label class="control-label"> Reason for not Interested</label> 
				<textarea type="text" class="form-control" id="comment3" value="" ></textarea>
				</p>
			</div>
			</div> 
			<span id="error3">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="save3" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>
<!-- END CONTAINER -->
<script language="javascript">
$(function(){ 
//loadMessages('sent');
var c="<?=$x?>";
$("#total_customers").val(c);
$("#add_new").click(function()
{  
	$("#email").val(''); $("#contact_person").val(''); $("#name").val(''); $("#phone").val(''); $("#remarks").val('');
	$("#add_msg").empty();
	$("#add_customer").modal('show'); 
});
 
 
 $("#submit").click(function(){    
	var assign_to=$("#assign_to").val();
	var date=$("#Demodate").val(); 
	var time=$("#time").val();
	var id=$("#id").val();
	var customer_name=$("#customer_name").val();
	var remarks=$("#remarks").val(); 
	if(!date){ $("#add_msg").html("<font color='red'> Demo date Required</font>");  $("#name").focus(); return false;}
	if(!time){ $("#add_msg").html("<font color='red'> Time Required</font>");  $("#name").focus(); return false;}
	if(!assign_to){ $("#add_msg").html("<font color='red'> Person Assigned  is Required</font>");  $("#name").focus(); return false;}
	 
	$("#add_msg").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/add_demo_request",
			type:"POST",
			async:false,
			data:
			{
				'id':id,   
				'customer_name':customer_name,   
				'demo_date':date,
				'time':time,
				'assign_to':assign_to,
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					$("#assign_to").val(''); $("#Demodate").val('');  $("#remarks").val('');
					$("#add_msg").html("<font color='green'> Data saved successfully </font>");
					setTimeout(function(){
					  $("#add_msg").empty();
					  $("#add_new").modal('hide');
					 window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
					$("#add_msg").html("<font color='red'> Record  <b> Not </b> saved. </font>"); 
				} 
			} 
	})
 
 });
  
  $("#save2").click(function(){     
	var date=$("#date_to_call_again").val(); 
	 
	var id=$("#id").val();
	var remarks=$("#comment2").val(); 
	if(!date){ $("#error2").html("<font color='red'> Date Required</font>");  $("#date_to_call_again").focus(); return false;}
	   
	$("#error2").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/call_again",
			type:"POST",
			async:false,
			data:
			{
				'id':id,   
				'date':date,  
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					$("#date_to_call").val('');  $("#comment2").val('');
					$("#error2").html("<font color='green'> Data saved successfully </font>");
					setTimeout(function(){
					  $("#add_msg").empty();
					  $("#add_action2").modal('hide');
					 window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
					$("#error2").html("<font color='red'> Record  <b> Not </b> saved. </font>"); 
				} 
			} 
	})
 
 });
  
   $("#save3").click(function(){     
	 
	var id=$("#id").val();
	var remarks=$("#comment3").val(); 
	if(!remarks){ $("#error3").html("<font color='red'>Reason why not interested </font>");  $("#comment2").focus(); return false;}
	  
	$("#error3").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/not_interested",
			type:"POST",
			async:false,
			data:
			{
				'id':id,    
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					  $("#comment3").val('');
					$("#error3").html("<font color='green'> Data saved successfully </font>");
					setTimeout(function(){
					  $("#error3").empty();
					  $("#add_action3").modal('hide');
					 window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
					$("#error3").html("<font color='red'> Record  <b> Not </b> saved. </font>"); 
				} 
			} 
	})
 
 });
  
  
  
 });
 
 
 function updateStatus(id,customer_name)
 {  
   $("#id").val(id);
   var v=$("#action_"+id).val(); 
   $("#customer_name").val(customer_name); 
   if(v=="Call Again")
   {
	   $("#add_action2").modal('show');
   }
   else if(v=="No Response")
   {
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/no_response",
			type:"POST",
			async:false,
			data:
			{
				'id':id    
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					 setTimeout(function(){
					  window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
				   alert('Record not updated');
				} 
			} 
	})
   }
   else if(v=="Not Interested")
   {
	    $("#add_action3").modal('show');
   }
   else
   {
	   $("#add_action").modal("show");
   }
 }
 
 function edit(id,name,phone,contact_person,email,remarks)
 {  
	$("#title").html('<font> Edit Customer Details </font>');
	$("#edit_id").val(id);$("#contact_person").val(contact_person); $("#email").val(email); $("#name").val(name); $("#phone").val(phone); $("#remarks").val(remarks);
	$("#add_customer").modal('show'); 
 } 
 
</script>