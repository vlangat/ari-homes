<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Paid Packages </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <?=strtoupper($pay_mode)?></span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar">
 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th>
							<th> Name  </th>
							<th> Phone Number </th> 
							<th> Payment Method</th> 
							<th> Receipt/Transaction Code</th> 
							<th> Amount </th>
							<th> Date  </th>   
							<th> Status </th> 
						</tr>
					</thead>
					<tbody>
						 <?php $x=1;
						 foreach($payment_info->result() as $p){
							 $name=""; $email="";
							 $status="Success"; $color="green";
						if($p->audit_number==2){ $status="Pending"; $color="red"; }
						 $total=0;?>
						<tr>
							<td><?=$x?> </td>
							<td>
							<?php foreach($subscriber_info->result() as $s)
							{
								 if($s->id==$p->user_info_id)
								 {  
									$name=$s->first_name." ".$s->middle_name." ".$s->last_name; 
									$email=$s->email; 
								 }
							}
							echo $name;
							 ?> 
							
							</td>
							<td> 
							<?=$email?>
							</td>
							<td> <?=$p->payment_method?> </td>
							<td> <?=$p->payment_method_code?> </td>
							<td> <?=$amount=($p->paid_amount)?> </td>
							<td> <?=$p->date_paid?>  </td> 				 
							<td> 
								<a   href="javascript:;" style="color:<?=$color?>" onclick="activate('<?=$p->id?>','<?=$p->user_info_id?>','<?=$p->company_code?>')" title="Click to change status"><b> <?=$status?> </b> </a>
							</td> 
						</tr>
						 <?php $x++; $total=$total+$amount;
						 }
						 ?>  
						 
					</tbody>
				</table>
				 
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div> 
    
<div id="activate_user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title"> Activate User </h4>
			</div>
			<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12">  
			 <p>
			 <input type="hidden" value="" id="activation_id">
			 <input type="hidden" value="" id="user_id">
			 <input type="hidden" value="" id="company_code">
			 Are you sure you want to make changes on selected transaction?
		   </p>
</div>
</div>  <span id="error_msg">   </span>
</div>
<div class="modal-footer" >
		<input type="submit" class="btn green"  id="submit" value="Yes"> 
		<button type="button" data-dismiss="modal" class="btn btn-outline dark" > No </button>
</div> 
</div>
</div>
</div>
</div>
        <!-- END CONTAINER -->
<script language="javascript">
    $(function(){ 
//loadMessages('sent');

 
 $("#submit").click(function(){  

     var activation_id=$("#activation_id").val(); 
     var user_id=$("#user_id").val(); 
     var company_code=$("#company_code").val(); 
	if(!activation_id){   return false;} 
	if(!company_code){   return false;} 
	$.ajax(
	{ 
			url:"<?=base_url();?>admin/activate_subscriber/",
			type:"POST",
			async:false,
			data:{
				'id':activation_id,
				'user_id':user_id,
				'company_code':company_code
			},			
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{
						$("#error_msg").html("<font color='green'> Subscriber "+obj.msg+" successfully </font>");
						setTimeout(function(){ 
						$("#error_msg").empty();
						$("#activate_user").modal('hide');
						window.location.replace("<?=base_url();?>admin/paid_packages/<?=$pay_mode?>");
					}, 2000);
				}
				else
				{
					 $("#error_msg").html("<font color='red'> User was Not "+obj.msg+" successfully </font>"); 
				} 
			} 
	})
 
 });
  
  
  
 });
 
 
 function activate(id,user_id,company_code){  
	$("#title").html('<font> Activate User </font>');
    $("#user_id").val(user_id); 
    $("#activation_id").val(id); 
    $("#company_code").val(company_code); 
	$("#activate_user").modal('show'); 
 } 
 
</script>