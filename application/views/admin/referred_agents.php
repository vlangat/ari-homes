<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
	<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#"> Agents </a>
		<i class="fa fa-circle"> </i>
	</li>
	<li>
		<span> Referred Agents </span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->

<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR --> 
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
	 
<div class="portlet light ">
	<div class="col-md-12" style="background:#006699;padding:6px;">
		<font color="#ffffff"> Agents/Landlords </font>
	</div>	
 
<div class="portlet-body">

<div class="tab-content"> 
<div   id="tab_1_4">
	<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit">
			 <div class="portlet-body" style="min-height:350px"> 
			 
			 <input type="hidden" value='' id="package_type"> 
			 <input type="hidden" value='' id="total_users">
		<table class="table table-striped table-hover table-bordered"   id="sample_editable_1">
			<thead>
			<tr><th> &nbsp; </th> <th> Name  </th> <th>  Phone  </th> <th> Email  </th> <th> Company  </th> <th> Date Added  </th><th> Status  </th> </tr>
			</thead>
			<?php $x=1; $y="";foreach($agents->result() as $u){?>
			<tbody>
			<tr>    
					<td><?=$x?> </td>
					<td>
                         <?=ucfirst(strtolower($u->name))?> 						
					</td> 
					<td>
						<?=$u->phone?>	
					</td> 
                    <td>
						<?=$u->email?>	
					</td> 
					<?php  $status="<font color='red'> pending</font>";$company="";
					foreach($tenants->result() as $t){ if($t->id_passport_number==$u->added_by){ $status="<font color='green'> Matched </font>"; }	}  
					foreach($agents_details->result() as $a){ if($a->company_email==$u->email){  $company=$a->company_name;}	}?> 
					 <td> <?=$company?> </td> 
					 <td> <?=$u->date_added?> </td> 
					 <td> <?=$status?> </td>  
			</tr>	
 <?php $x++; } ?>	
</tbody>			
</table>	
 	 
		</div>
	</div>
</div> 
</div>
</div>
 
</div>   
        <?php if($this->session->flashdata('temp')){
				$msg=$this->session->flashdata('temp');
				echo '<div class = "alert alert-success alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button> <font color="green">'. $msg. '</font> </div>'; 	
				} 	
		?>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div> 
 
<!-- END CONTENT -->
 
<!-- END CONTAINER -->
 
    </body>

</html>
<script type="text/javascript">
  
 $(document).ready(function()
 { 
	getAgents();
	 checkPrivilege(); 
 });
 
$(function()
{ 
	//$('#responsive_2').modal('hide');
	$("#add_btn").append("<a data-toggle='modal' href='javascript:;' id='add_new' class='btn green'><i class='fa fa-plus'></i> Add New User</a>");  
	 
});

$("#add_new").click(function(){ 
	  $("#responsive_2").modal("show");
});
  
  
  $("#save").click(function(){     
	var email=$("input[name='email']").val();  
	var phone=$("input[name='phone']").val();
	var name=$("input[name='agent_name']").val(); 
	if(!name){ $("#error").html("<font color='red'>  Enter agent name </font>"); $("#agent_name").focus(); return false;}
	if(!email){ $("#error").html("<font color='red'>  Enter email address </font>");  $("#email").focus(); return false;}
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#error").html("<font color='red'> Email provided is not a valid e-mail address"); $("#email").focus(); return false;}
	if(!phone){ $("#error").html("<font color='red'>  Enter phone number </font>");  $("#phone").focus(); return false;}
	$("#errorr").html("<font color='green'>Saving...please wait</font> ");
	$.ajax(
	  {
		url:"<?=base_url();?>Auth/referAgent",
		type:"POST",  
		data:{
			'phone':phone,
			'email':email,
			'name':name 
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#amount").val('');
			       $("#error").html("<font color='green'>   Thank you for referring "+name+" to ARI. We shall contact and sign him up for the service </font>");
				 setTimeout(function()
				{ 
					//window.location="<?=base_url();?>auth/tenant";
                },3000); 
			 }
			 else
			 {
				$("#error").html("<font color='red'> It seems your Agent/Landlord already exists  </font>");
				//window.location="<?=base_url();?>auth/tenant";				
			 }
		}
  })
 });
 
 
</script>