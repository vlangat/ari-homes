<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Users </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Log Activity </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	
	<div class="col-md-12">
	<div class="col-md-12" style="background:#006699;padding:6px;">
				<font color="#ffffff"> Audit Trail </font>
		</div>
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar" style="min-height:400px">
<table class="table table-striped table-hover table-bordered" id="table">
					<thead>
						<tr> 
							<th> #</th> 
							<th> Date Created </th> 
							<th> User Name  </th> 
							<th> IP Address </th> 
							<th> Message </th> 
							<th> Status </th>  
						</tr>
					</thead>
					<tbody id="logs">
					<td colspan='6' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>");
		
					</tbody>
</table>
			 
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
	/*    var table= $('#table').DataTable({
              destroy: true,
              responsive: true,
               "ajax":{
                   url:'/tables/audit_trail',
                   type: 'get'
               },
               language: {
                    searchPlaceholder: "Search records.."
                }
    	});*/
	});
	
	
	
function audits()
{
		var content=""; 
		$("#logs").html("<tr><td colspan='6' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>");
		$.ajax({
		url:'<?=base_url();?>tables/audit_trail/', 		
		type: 'POST', 
		async:false, 
		success:function (data)
		{  
			var obj = JSON.parse(data); 
			var data = obj.data; var count=0; var count=parseInt(data.length);$("#countr").val(count);  
            
			if(count >0){ 
				for(var i=0; i<data.length; i++)
				{
					var p = data[i];    
					content=content+"<tr><td>"+(i+1)+"</td> "+
					"<td>"+p['create_date']+"</td> <td>"+p['user_id']+"</td><td>"+p['ip_address']+"</td> <td>"+p['message']+"</td> <td>"+p['status']+"</td></tr>";
					count++;
				} 
				$("#logs").html(content); 
			}
			else{
					$("#logs").html("<tr><td colspan='8' align='center'>No data available</td></tr>");
				} 
			$('#table').DataTable();
		}
	});
 
}
  
 $(document).ready(
 function(){   
	audits();
 });
</script>