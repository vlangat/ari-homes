 <?php foreach($data->result() as $row){	} ?>
 <?php foreach($property->result() as $p){	} ?>
 <?php foreach($tenants->result() as $t){  	} ?>
 <?php foreach($rent->result() as $r){  	} ?>
 <?php foreach($suppliers->result() as $s){  	} ?>
 <?php foreach($subscription->result() as $sub){  	} ?>
 <?php foreach($users->result() as $u){  	} ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
	<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="index.html">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#"> Users </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Privileges </span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		 <!-- PORTLET MAIN -->
		<?php //foreach($profile as $rows){}?>
 
	</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
	 
<div class="portlet light ">
			<div class="col-md-12" style="background:#006699;padding:6px;">
				<font color="#ffffff"> Privileges </font>
		</div>	
 
<div class="portlet-body">

<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
 
	<!-- END PERSONAL INFO TAB --> 
<div   id="tab_1_4">
	<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit ">
			 <div class="portlet-body">
			 <form action="<?=base_url();?>admin/assign_privilege" method="post">
			 <input type="hidden" value="<?=$user_id?>" name="user_id">
			<table   class="table table-striped table-hover table-bordered" >
			<thead>
			<tr><th> &nbsp; </th>  <th> Add </th> <th> Edit  </th> <th> View </th> <th> Delete </th> </tr>
			</thead>
			<tbody>
			<tr>
				<td> 
					<strong>Property </strong>
					</td>  
					 
					  <td>  
					 <input type="checkbox"  name="p_addPrivilege"  <?php if($p->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="p_editPrivilege" <?php if($p->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="p_viewPrivilege" <?php if($p->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="p_deletePrivilege" <?php if($p->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Tenants </strong>
					</td> 
					 
					  <td>  
					 <input type="checkbox"  name="t_addPrivilege"  <?php if($t->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="t_editPrivilege" <?php if($t->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="t_viewPrivilege" <?php if($t->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="t_deletePrivilege" <?php if($t->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Rent </strong>
					</td> 
					 
					  <td>  
					 <input type="checkbox"  name="r_addPrivilege"  <?php if($r->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="r_editPrivilege" <?php if($r->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="r_viewPrivilege" <?php if($r->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="r_deletePrivilege" <?php if($r->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>			
			<tr>
					<td> 
						<strong>Suppliers </strong>
					</td> 
					 
					  <td>  
					 <input type="checkbox"  name="s_addPrivilege"  <?php if($s->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="s_editPrivilege" <?php if($s->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="s_viewPrivilege" <?php if($s->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="s_deletePrivilege" <?php if($s->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>			
			<tr>
				   <td> 
					<strong>Subscriptions </strong>
					</td> 
					  <td>  
					 <input type="checkbox"  name="sub_addPrivilege"  <?php if($sub->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="sub_editPrivilege" <?php if($sub->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="sub_viewPrivilege" <?php if($sub->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="sub_deletePrivilege" <?php if($sub->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>	
<tr>
				   <td> 
					<strong>Users </strong>
					</td> 
					  <td>  
					 <input type="checkbox"  name="users_addPrivilege"  <?php if($u->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="users_editPrivilege" <?php if($u->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="users_viewPrivilege" <?php if($u->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="users_deletePrivilege" <?php if($u->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>				
 </tbody>
</table>	
<p>  </p>		
<div class="form-group">
		<input type="submit"  value="Save"  id="save_btn" class="btn green"  /> 
</div>
</form>
					</div>
				</div>
			</div>
			
			
</div>
</div> <p id="error">   </p>
</div>   
        <?php if($this->session->flashdata('temp')){
				$msg=$this->session->flashdata('temp');
				echo '<div class = "alert alert-success alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button> <font color="green">'. $msg. '</font> </div>'; 	
				} 	
		?>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>  

<div id="success" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Venit Message</h4>
			</div> 
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
			 
					<p id="msg">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div> 
</div> 
</div>  
<!-- END CONTENT --> 
 
</body> 
</html>


<script type="text/javascript">
 
 $(function()
{  
 
 var msg="<?=$msg?>";
 if(!msg)
 {
	 
 }else{
	 $("#msg").html("<font> "+msg+" </font>");
	 $('#success').modal('show');
 }
 
});
</script>