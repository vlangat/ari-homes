<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Users </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  <?=ucfirst(strtolower($status))?> </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
  
<?php $u=0; $new=0; foreach($users->result() as $user){
		$u++; 
		$now=time(); $diff_time=$now-strtotime($user->registration_date);  $days=floor($diff_time/(60*60*24));
		if($days<=7)
		{ 
			$new++;  
		}
}
?> 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar">
 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th>
							<th> Name  </th>
							<th> Company  </th> 
							<th> Email</th>  
							<th> Mobile </th>  
							<th> Date Registered </th> 
							<th> Package  </th> 
							<th> Last Login </th> 
							<th> Status </th> 
							<th> &nbsp; </th> 
							<th> &nbsp; </th> 
							<th> &nbsp; </th> 
						</tr>
					</thead>
					<tbody>
						 <?php $i=1; foreach($users->result() as $user){ 
 if($user->testAccount =="1"){  
							  $user_type="Free"; $package_id=0; $x=0; $package_type="";
								foreach($paid_packages->result() as $p){ 
									if($p->company_code==$user->company_code){
									 $package_id=$p->package_id; $x++;
										}
									}
									
									foreach($packages->result() as $pac){  if($pac->id==$package_id){  $package_type=$pac->package_type; 	} }
									
							//if($package_id==1){ $user_type="Premium"; } if($package_id==2){ $user_type="Custom"; }
							//if($x !=0){ continue;}	
							$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
							$enct =base64_encode(do_hash($user->id . $salt,'sha512') . $salt);							
							?>
						<tr>
							<td>  <?=$i?> </td>
							<td><?=$user->first_name?> <?=$user->last_name?></td>
							<td><?php $company_name="";
							foreach($company_info->result() as $c)
							{ 
								if($user->company_code==$c->company_code)
								{
									 $company_name=$c->company_name ;
								}
							}
							?>
							<?=$company_name?>
							</td>
							<td> <?=$user->email?> </td>
							<td> <?=$user->mobile_number?> </td>
							   
							<td> <?=$user->registration_date?> </td>
							<td> <?php echo $package_type;	 ?> </td> 
							<td> <?=$user->last_login?> </td>
							 <?php $status="Active"; $color="green";
								if($user->user_enabled==2){
									$status="Inactive"; $color="red"; 
								}?>
							<td>
								<a href="javascript:;"  onclick="activate('<?=$user->id?>','<?=$user->user_enabled?>')" id="activate" style="color:<?=$color?>"> <?=$status?> </a>
							</td> 
							<td>
								<a href="<?=base_url();?>admin/user_privileges/<?=$enct?>" style="color:blue"> Privileges </a>
							</td> 
							<td>
								<a href="javascript:;" style="color:blue" onclick="reset_account('<?=$user->id?>','<?=$user->company_code?>')"> Reset Account </a>
							</td> 
							<td>
								<a href="javascript:;" style="color:blue" onclick="testAccount('<?=$user->id?>')"> Not Test Account? </a>
							</td> 
						</tr>
						 <?php $i++; 
							 }
						 }?>  
						 
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
 <div id="reset_account" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title"> Activate User </h4>
			</div>
			<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				
			<div class="col-md-12">  
						 <p>
						 <input type="hidden" value="" id="activation_id">
						 <input type="hidden" value="" id="user_id">
						 <input type="hidden" value="" id="company_code">
						 Are you sure you want to reset password for selected account?
					   </p>
			</div>
			</div>  <span id="error_msg">   </span>
			</div>
			<div class="modal-footer" >
					<input type="submit" class="btn green"  id="submit" value="Yes"> 
					<button type="button" data-dismiss="modal" class="btn btn-outline dark" > No </button>
			</div> 
			</div>
</div>
</div>
</div>

<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
	<i class="icon-login"></i>
</a>

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

<script language="javascript">
 
 function activate(id, status)
	{ 
	var msg="Deactivate";	
	if(status==2){ msg="Activate";}
	
	var x=confirm("Are you you to "+msg+" this user?");
	if(x==true){  
	  	if(status==1){ status=2;}else{ status=1;}		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/activate/"+id+"/"+status,
		 type:"POST",
		 async:false,
		 success:function(data)
		 {  
			var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				//$("#error_message").html(" <font color='green'> <i class='fa fa-check'></i> Changes made successfully </font>");
				setTimeout(function(){
				location.reload();
			 },2000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				alert(" Changes to the user details not made ");
				return false;
			 }
		 }
		  }
		 )	
	}
else{ return false;}	
	}
	
	function reset_account(user_id,company_code)
{  
	$("#title").html('<font> Reset Account </font>'); 
	$("#user_id").val(user_id);  
    $("#company_code").val(company_code);  
	$("#reset_account").modal('show'); 
} 
 
$(function(){  
	$("#submit").click(function(){     
	var user_id=$("#user_id").val(); 
	var company_code=$("#company_code").val();  
	if(!company_code){   return false;} 
	 
	$.ajax(
	{ 
			url:"<?=base_url();?>admin/reset_account/",
			type:"POST",
			async:false,
			data:
			{  
				'user_id':user_id,
				'company_code':company_code
			},			
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					 setTimeout(function()
					 {
						location.reload();
					 },2000);
				} 
			}	 
	})
 });
    
});
 
 
  function testAccount(id)
	{   
	 var c=confirm('Remove this user from Test Account back to live account?'); 
	 if(c==false){ return false;}
		//var id=	$("#id_no").val();
        var status=1;//$("#testStatus").val();		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/markTestAccount/"+id+"/"+status,
		 type:"POST",
		 async:false,
		 success:function(data)
		 { 
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				$("#error_message").html(" <font color='green'> <i class='fa fa-check'></i> Changes made successfully </font>");
              alert('Account successfully activated as a Live Account');
			  setTimeout(function(){
			  location.reload();
			 },3000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				$("#error_message").html(" <font color='red'><i class='fa fa-close'></i>  Changes to the user details not made </font>");
				return false;
			 }
		 }
		  }
		 )	  
}
</script>