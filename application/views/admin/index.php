<!DOCTYPE html>
 
<html lang="en"> 
    <head>
        <meta charset="utf-8" />
        <title>ARI Homes  | Admin Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES --> 
        <link href="<?=base_url();?>template/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=base_url();?>template/theme/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?=base_url();?>template/theme/assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?=base_url();?>images/favicon.png" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
      <!--  <div class="logo">
            <a href="index.html">
                <img src="<?=base_url();?>images/Logo-Ari-CS.png" height="50" alt="" /> </a>
        </div>-->
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
			<!--<form  class="login-form" >-->
<?php  echo form_open_multipart('admin/login/','onSubmit=""');?>
<h3 class="form-title font-green">Administrator Login</h3>
<?php if($this->session->flashdata('temp')){
	$msg=$this->session->flashdata('temp');
	echo '<div class="alert alert-danger">
                    <button class="close" data-close="alert" >  </button>
                    <span> '. $msg.'</span>
          </div> 
		 '; 
		}
?>

	<div class="form-group">
		<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
		<label class="control-label visible-ie8 visible-ie9">Username</label>
		<input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" required placeholder="Username" id="username" value="<?=$this->session->flashdata('temp_userId')?>" name="email" /> 
		</div>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9">Password</label>
		<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" required placeholder="Password" id="password" name="password" /> </div>
	 <div class="form-group">
		<p> <font  id="captcha_image_reset"><label for="captcha">   <?=$this->session->userdata('image');?>   </font>
	  <font align="left"> &nbsp;  <a href='<?=base_url();?>admin/' style="color:#006699"> Refresh  </a> image </font> </label></p>
		<input type="hidden"  name="captcha_name"  id="captcha_name_reset" class="form-control" value='<?=$this->session->userdata('image');?>'/>
		<input type="hidden"  name="captcha_code" id="captcha_val_reset" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
		<input type="text" required autocomplete="off" name="userCaptcha"  id="user_Captcha" class="form-control" placeholder="Enter the  above numbers" value=""/>
	 
	</div>		
	<div class="form-actions">
		<button type="submit" class="btn green uppercase" id="login">  &nbsp; &nbsp; Login  &nbsp; &nbsp; </button> 
	 </div>
	  
<?php echo form_close(); ?>	
<!-- END LOGIN FORM --> 
</div>
 
<div id="reset_pass" class="modal fade" tabindex="-1" aria-hidden="true">
 <?php  echo form_open_multipart('admin/changePassword/', 'onSubmit="return change()"');?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h4 class="modal-title">Change your  Password </h4>
	</div> 
	<div class="modal-body" style="">  
		<input type="hidden"  id="curr_password" name="curr_password" value="" class="form-control" />  
		<input type="hidden"  name="email" value="<?=$this->session->flashdata('email');?>" class="form-control"/>  
		<input type="hidden"  id="user_info_id" name="user_info_id" value="<?=$this->session->flashdata('user_info_id');?>" class="form-control"/>  
		<div class="form-group">
			<label class="control-label"> New Password</label>
			<input type="password" required id="new_pass" name="new_pass"  class="form-control" />
		</div>
		<div class="form-group">
			<label class="control-label"> Verification Code (Sent via SMS)</label>
			<input type="text" required autocomplete="off" name="code" id="code"   class="form-control" onchange="checkChars('code')" />
		</div>

	  <div class="form-group">
			<p> <font  id="captcha_image_reset"><label for="captcha">   <?=$this->session->userdata('image');?>   </font>
		  <font align="left"> &nbsp; Can't read the image?  Click <a href='javascript:;' style="color:#006699" onclick="getCaptcha()">here </a> </font> </label></p>
		<input type="hidden"  name="captcha_name"  id="captcha_name_reset" class="form-control" value='<?=$this->session->userdata('image');?>'/>
		<input type="hidden"  name="captcha_code" id="captcha_val_reset" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
		 <input type="text" required autocomplete="off" name="userCaptcha"  id="user_Captcha" class="form-control" placeholder="Enter the  above numbers" value=""/>
   		 
	</div>		
			
        </div> 
			<div class="modal-footer"><font style="float:left" id="reset_error"> <?=$this->session->flashdata('temp')?>  </font>
				<input type="submit"  class="btn green"value="Submit"/> 
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
			</div> 
		 
<?php echo form_close(); ?>
</div> 
</div>
</div>

<div class="copyright"> <?php echo date("Y");?> &copy; Ari Limited </div>
        <!--[if lt IE 9]>
<script src="<?=base_url();?>template/theme/assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url();?>template/theme/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/login.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>

<script language="javascript">
$(document).ready(function()
{  
	 
	window.history.forward(-1);
	var reset_pass="<?=$this->session->flashdata('reset_pass')?>";
    var password_resetting="<?=$this->session->flashdata('password_resetting')?>";
    var user_info_id="<?=$this->session->flashdata('user_info_id')?>"; 
	 if(!password_resetting){ 
		return false; 		
	 }
	 else if(user_info_id)
	 {
		 $("#user_info_id").val(user_info_id);
		$("#reset_pass").modal('toggle'); 
	 }
	 else
	 {  
		$("#curr_password").val(reset_pass);
		$("#reset_pass").modal('toggle'); 
	 }		
});


 function change()
	{  
		var email="<?php echo $this->session->userdata('email');?>";
		var curr_pass=$("#curr_password").val();
		var new_pass=$("#new_pass").val();
		var code=$("#code").val();  	
	   if(!new_pass){  $("#reset_error").html("<font color='red'> Enter new password </font>"); $("#new_pass").focus(); return false;}else{ $("#reset_error").empty();}
	   if(!code){	$("#reset_error").html("<font color='red'> Enter verification code </font>"); $("#code").focus(); return false;}else{ $("#reset_error").empty();}
		return true;
	}
   
</script>