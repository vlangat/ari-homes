<?php  foreach($partner_details->result() as $row){ $partner_code=$row->partner_code; }
$t=0; $total=0; $this_month=0; $last_month=0; $l_month=0; 
foreach($subscribers->result() as $rows)
		{
			if($rows->token_no==$partner_code)
			{
			$amount=$rows->paid_amount; $t=$t+$amount; 
			 $month=date("m",strtotime($rows->date_paid));
			 if(date("m")==10||date("m")==11||date("m")==12)
			 {
				 $l_month=date("m")-1; 
				 if($l_month==9){ $l_month='0'.$l_month;}
				 if(date("m",strtotime($rows->date_paid))==$l_month)
					{ 
						$last_month=$last_month+$amount; 
					}
			 }else if(substr(date('m'),1)==1){
					$l_month=12;
					if(date("m",strtotime($rows->date_paid))==$l_month)
					{ 
						$last_month=$last_month+$amount; 
					}
			 }else{
					$l_month=substr(date('m'),1)-1;
					if(date("m",strtotime($rows->date_paid))=='0'.$l_month)
					{ 
						$last_month=$last_month+$amount; 
					}
			 }
			    
             //if($l_month<0){ $l_month=0-$l_month;}			 
			 if($month==date("m"))
				{ 
					$this_month=$this_month+$amount;
				}
				  
			}
			 
		}  
		 $t=$t-((16/100)*$t);
		 $this_month=$this_month-((16/100)*$this_month);
		 $last_month=$last_month-((16/100)*$last_month);
?>
<!-- BEGIN CONTAINER -->
<div class="page-container">  
		<!-- BEGIN PAGE CONTENT BODY -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMBS -->
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="index.html"> Home </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">Partners</a>
						<i class="fa fa-circle"></i>
					</li>  				
					<li><?php  foreach($partner_details->result() as $row){ $partner_code=$row->partner_code; echo $p_name=$row->first_name." ".$row->last_name;} ?>
						<span>  </span>
					</li> 
			</ul>
		
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
<div class="col-lg-3">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value="<?=15/100*$t?>">0</span>
				<small class="font-green-sharp">		</small>
			</h3>
			<small> Total Earnings </small>
		</div>
		<div class="icon">
			<i class="fa fa-money"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:;" class="progress-bar progress-bar-success green-sharp">
				<span class="sr-only"> </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title">Total  Earnings </div>
			 <div class="status-number">  </div> 
		</div>
	</div>
</div>
</div>
<div class="col-lg-5">
<div class="dashboard-stat2">
	<div class="display">
		<div class="number">
			<h3 class="font-red-haze">
				<span data-counter="counterup" data-value="<?=$x=15/100*$last_month?>"> <?=0?> </span>
			</h3>
			<small> Last Month </small>
		</div>
		<div class="icon">
			<i class="fa fa-dollar">	</i>   
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:" class="progress-bar progress-bar-success red-haze">
				<span class="sr-only">   </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title">  Last Month Earnings </div>
			<div class="status-number">  <?=''?>     </div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-4 ">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-purple-soft" style="color:green">
				<span style="color:green" data-counter="counterup" data-value="<?=15/100*$this_month?>"><?=0?></span>
			</h3>
			<small>   This Month </small>
		</div>
		<div class="icon">
			<i class="fa fa-money"> </i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:;" class="progress-bar progress-bar-success purple-soft">
				<span class="sr-only"> </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> <?=date("M");?> Total Earnings </div>
			<div class="status-number">   </div>
		</div>
	</div>
</div>
</div>
</div>
 
<div class="page-content-inner">
	<div class="inbox">
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div> 
			

<div class="row"> 
<div class="col-md-12" style="background:#006699;padding:8px;">
	<font color="#ffffff"> Partner  </font>
	
</div>  

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit " style="min-height:400px;">
	<div class="portlet-body">
	<h5 style="color:#006699"> Partner Name: <b> <?=strtoupper($p_name)?> </b> Parner Token No: <b>  <?=$partner_code?> </b> </h5> 
	 
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
		<thead>
			<tr><th> Customer </th> <th> Package   </th> <th> Date Registered </th> <th> Amount Paid </th> <th> Earnings </th> </tr>
		</thead>
	<tbody>
	  <?php
	$amount=0; $total=0; $count=0; $name=""; $t=0; $sms_cost=0;
	foreach($subscriptions->result() as $rows)
		{   
		if($rows->token_no==$partner_code) 
			{ 
            $user=$rows->user_info_id; $package_id=$rows->package_id; $invoice_no=$rows->invoice_no;
			  
			 foreach($subscribers->result() as $q){ if($invoice_no==$q->invoice_no && $q->package_id !=$package_id &&$q->token_no==$partner_code){ $sms_cost=$sms_cost+$q->paid_amount; } }
			 $amount=($sms_cost+$rows->paid_amount)-(16/100*($sms_cost+$rows->paid_amount));
			 foreach($subscriber_info->result() as $r)
			 {
				 if($user==$r->id)
					{ 
						$name=$r->first_name." ".$r->middle_name." ".$r->last_name;
						
					}						
			 }	

		foreach($packages->result() as $p)
			 {
				 if($package_id==$p->id)
					{ 
						$package_type=$p->package_type; 
					}						
			 }
           $sms_cost=0;			 
		 ?> 
	 <tr>
		 <td> <?=$name?> </td>  <td> <?php if($package_type=='sms'){ echo $package_type;}else {echo $package_type.'/sms';}?> </td> <td> <?=$rows->date_paid?></td> <td> <?=$amount?> </td>  <td> <?php echo $t=$amount*(15/100);?> </td>
	 </tr>
	 
	 <?php  $count++;   $total=$total+$t;
			}
			
		} 
	 
	  ?>
	 </tbody>
</table>
	
	<table width="100%">
	 <tr> <td colspan="6"><td></tr>
	 <tr>
		<td>   </td> <td colspan="5" align="right">
			Total Amount: KES. <?php echo '<font color="blue">'.$total;?> </font> <br/>  
		</td> 
	 </tr>
	 </table>
	 
	 
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
          
        </div>
        <!-- END CONTAINER -->
 
    </body>

</html>

<script language="javascript">
$(document).ready(function()
{ 
$("#save").click(function()
	{   
	 
		var fname=	$("#fname").val();
		var lname=	$("#lname").val();
		var email=	$("#email").val();
        var company=$("#company").val();	
        var phone=$("#phone").val();	
        var alt_phone=$("#alt_phone").val();	
        var national_id=$("#national_id").val();	
        var kra=$("#kra").val();	
        if(!(email)||!(phone)||!(kra)||!(national_id)){ $("#error_message").html("<font color='red'> Fill all the required inputs </font>"); return false; }		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/addPartner/",
		 type:"POST",
		 async:false,
		 data:{
			 'fname':fname,
			 'lname':lname,
			 'company':company,
			 'email':email,
			 'phone':phone,
			 'alt_phone':alt_phone,
			 'kra':kra,
			 'id':national_id
		 },
		 success:function(data)
		 { 
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				$("#error_message").html(" <font color='green'> <i class='fa fa-check'></i> Partner registered successfully </font>");
				setTimeout(function(){
			  location.reload();
			 },3000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				$("#error_message").html(" <font color='red'><i class='fa fa-close'></i>  Partner Not registered successfully </font>");
				return false;
			 }
		 }
		  }
		 )	  
});

});

</script>