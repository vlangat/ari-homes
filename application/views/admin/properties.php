<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="#">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Users </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  <?=ucfirst(strtolower($status))?> </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar">
 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr> 
		<th> #</th>
                 <th> Name</th>
		<th> Property </th>
		<th> Type </th>
		<th> Total Units </th>
		<th> No of Tenants </th>
		<th> Location </th> 
		<th> Floors </th>
		<th> Car Spaces </th>
		<th> &nbsp;   </th> 
	</tr>
</thead>
<tbody>
<?php $x=1;foreach($property->result() as $row):?>
	<tr><?php $total_units=0;?>
		 <td><?=$x?></td>  <td> 	<?php
			foreach($user_info->result() as $c){ 
				if($row->company_code==$c->company_code){
					 echo ucfirst(strtolower($c->first_name)).' '.ucfirst(strtolower($c->middle_name)).' '.ucfirst(strtolower($c->last_name)) ;
				}
			}?>
		</td>
		<td> 	<?=$row->property_name?> 	</td>
		<td> 	<?=$row->property_type?> 	</td> 
		<td> 	<?php foreach($units->result() as $r){ if($row->id==$r->property_id){  $total_units=$total_units+$r->total_units; } }  echo $total_units?> 		</td>
		<td><?php $t=0;
			foreach($tenants->result() as $ten)
			{ 
				if($row->id==$ten->property_id)
				{
					 $t++;
				}
			}
			echo $t;
			?> 
		</td>
		<td> 	<?=$row->location?> 	</td> 
		<td> 	<?=$row->floors?> 			</td> 
		<td class="center"><?=$row->car_spaces?></td>
		<td class="center">
		<?php if($row->audit_number==1){?>
			<a href="javascript:;" onclick="delete_property('<?=$row->id?>')">
			<i class="fa fa-trash"> </i>  
				Remove   
			</a>
		<?php }
		else if($row->audit_number==2){?>
			<a href="javascript:;" onclick="add_property('<?=$row->id?>')">
			<i class="fa fa-plus"> </i>  
				Add   
			</a>
		<?php } ?>		
		</td>
	</tr>   
<?php $x++; endforeach;?> 
</tbody>
</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
	<i class="icon-login"></i>
</a>

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->


<div id="warning" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title"><b  style="font-size:18px;color:brown"> <i class="fa fa-info"> </i>  Warning Message</b></h4>
			</div>
			<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12">  
   
			<p>
				Are you sure you want to  perform  selected action on this property? 
			</p>
			<input type="hidden" id="del_id">
		</div>
		</div>
</div>
<div class="modal-footer" >  
	<button type="button"   id="confirm_del" class="btn green">Yes</button>
	<button type="button" data-dismiss="modal" class="btn default">No</button>
</div>
</div>

</div>
</div>
</div>

<script language="javascript">
$(function(){
	$("#confirm_del").click(function(){
	var id=$("#del_id").val(); 
	$.ajax({
		url:"<?=base_url();?>property/remove_property/"+id,
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;
			if(obj.result=="ok")
			{  
						setTimeout(function(){ 
							$('#warning').modal('hide');
							location.reload();
                      }, 1500);
		       
			}
			else
			{
				alert('Not deleted Try again later');
				return false;
			}
		}
	 })
	 });
	 
	 
});


function delete_property(id)
 {  
	$("#del_id").val(id);
	$("#warning").modal('show');
 }	
 
function add_property(id)
 {  
	var msg="Undo Delete";	 
	var x=confirm("Are you you to return  this property?");
	if(x==true){   	 
		$.ajax({
		 url:"<?=base_url();?>admin/add_property/"+id,
		 type:"POST",
		 async:false,
		 success:function(data)
		 {  
			var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 setTimeout(function(){
				location.reload();
			 },2000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				alert(" Changes to the property was  not made Try again later ");
				return false;
			 }
		 }
		  }
		 )	
	}
else{
		return false;
	}	
}
 	
</script>