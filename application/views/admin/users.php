<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Users </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  <?=ucfirst(strtolower($status))?> </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
  
<?php $u=0; $new=0; foreach($users->result() as $user){
		$u++; 
		$now=time(); $diff_time=$now-strtotime($user->registration_date);  $days=floor($diff_time/(60*60*24));
		if($days<=7)
		{ 
			$new++;  
		}
}
?> 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar">
 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th>
							<th> Name  </th>
							<th> Company Name</th> 
							<th> Email</th>  
							<th> Mobile </th>  
							<th> Date Registered </th> 
							<th> Package  </th> 
							<th> Last Login </th> 
							<th> Status </th> 
						</tr>
					</thead>
					<tbody>
						 <?php $x=1; foreach($users->result() as $user){ ?>
							 
							 <?php $user_type="Free"; $package_id=0;
								foreach($packages->result() as $p){ 
									if($p->company_code==$user->company_code){
									 $package_id=$p->package_id;
										}
									}
							if($package_id==1){ $user_type="Professional"; }if($package_id==2){ $user_type="Enterprise"; }
							if($status =="free" && $user_type !="Free"){ continue;}							
							?>
						<tr>
							<td> <?=$x?> </td>
							<td><?=$user->first_name?> <?=$user->last_name?></td>
							<td><?php $company_name="";
							foreach($company_info->result() as $c)
							{ 
								if($user->company_code==$c->company_code)
								{
									 $company_name=$c->company_name ;
								}
							}
							?>
							<?=$company_name?>
							</td>
							<td> <?=$user->email?> </td>
							<td> <?=$user->mobile_number?> </td>
							   
							<td> <?=$user->registration_date?> </td>
							<td> <?php echo $user_type;	 ?> </td> 
							<td> <?=$user->last_login?> </td>
							 <?php $status="Active"; $color="green";
								if($user->user_enabled==2){
									$status="Inactive"; $color="red"; 
								}?>
							<td>
								<a href="javascript:;" style="color:<?=$color?>"> <?=$status?> </a>
							</td> 
						</tr>
						 <?php $x++; 
						 
						 }?>  
						 
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
	<i class="icon-login"></i>
</a>

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->