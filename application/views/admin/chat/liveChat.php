<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
   
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE CONTENT BODY -->
	<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="index.html">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Support</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Live Chats</span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->  
<div class="page-content-inner">
<div class="inbox">
<div class="row">
 
<div class="col-md-12">
<div class="inbox-body">
 <hr/> 
<div class="inbox-header">
	<h1 class="pull-left"> <font id="smss_title" size="3"> <font> </h1>
	 <font color="#006699"><strong> Live Chats     </strong> </font>  
	
 
</div>
	<div class="inbox-content">  
  
<table  style="font-family:verdana" class="table table-striped table-hover table-bordered" id="sample_editable_1">
	<thead>
		<tr>  <th> # </th> <th> User Name </th> <th> Company Name </th> <th> Email </th> <th> Phone </th>   <th> Last seen  </th></tr>
	</thead>
	  <tbody>
	  <?php $x=1; foreach($users->result() as $user){
							 if($user->testAccount==1){
		// continue;
	 }  
	 $salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
	 $enct =base64_encode(do_hash($user->email . $salt,'sha512') . $salt);?>
						<tr onclick="response('<?=$enct?>')">
							<td><?=$x?></td>
							<td><?=$user->first_name?> <?=$user->last_name?></td>
							<td><?php $company_name="";
							foreach($company_info->result() as $c){ 
							if($user->company_code==$c->company_code){
								 $company_name=$c->company_name ;
							}
							}
							?>
							<?=$company_name?>
							</td>
							<td> <?=$user->email?> </td>
							<td> <?=$user->mobile_number?> </td>
				 
							<td> <?php if($user->logout==1){ echo $user->last_login; } else{ echo '<i style="color:green;font-size:12px;" class="fa fa-circle"></i> Active now';}?> </td>
							 
						</tr>	
				<?php $x++;} ?>			
			</tbody>
		</table>  
		</div>
	</div>
</div>
</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT --> 
 
<!-- END CONTAINER -->
<script type="text/javascript">

function validate()
{
var value=$("#delete").val();
if(!value){ return false;}	
var chks = document.getElementsByName('checkbox[]');
var hasChecked = false;
for (var i = 0; i < chks.length; i++)
{
if (chks[i].checked)
{
hasChecked = true;
break;
}
}
if (hasChecked == false)
{
alert("Please select at least one message.");
//jAlert('Please select at least one message to perform the action');
return false;
}
$("#confirmModal").modal('show');
return true;
}
  
 
 
$(function(){
	
 $("#compose_msg").click(function(){
	  $("#compose_sms").modal('show');
  });
  $("#confirmDelete").click(function(){
		var favorite = [];
		$.each($("input[name='checkbox[]']:checked"), function(){            
			favorite=($(this).val()); 
			deleteMessage(favorite);
		});
	   location.reload();
 });

 }); 
 
 function deleteMessage(id)
 {
	 
	 $.ajax({
			url:"<?=base_url();?>support/removeMessages/"+id,
			type:"post",
			success: function(data){ 
			var obj = JSON.parse(data);
			if (obj.result=="ok"){ 	 
                //jAlert('There was an error in deleting the messages');
			}
			else
			{ 
				jAlert('There was an error in performing the action');
			}
	}	
}	
)	
}
 
function send(){  	
	 
	var to=$("#to").val();	 
	var body=$("#message").val(); 
	var subject=$("#subject").val();  
	if(!subject){ $("#error").html("<font color='red'> Subject Required  </font>"); $("#subject").focus(); return false;}
	if(!to){ $("#error").html("<font color='red'>Select Receipient  </font>"); $("#to").focus(); return false;}
	if(!body){ $("#error").html("<font color='red'>Message is empty.  </font>"); $("#message").focus(); return false;}
	//if(/^[a-zA-Z0-9- ]*$/.test(body) == false){ $("#message").focus(); $("#error").html("<font color='red'>Message content   have illegal characters </font>");  return false; } else{$("#error").empty();} 
    $("#error").html("<font color='#006699'> Sending message waiting...</font>"); 
	$.ajax({ 
			url:"<?=base_url();?>support/compose_message",
			type:"POST",
			async:false,
			data:
			{   
				'to':to,   
				'subject':subject,
				'body':body
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{
					$("#to").val('');
					$("#message").val(''); 
					$("#subject").val('');
					$("#error").html("<font color='#006699'>  Message sent successfully </font>");
					setTimeout(function()
						{
							location.reload(); 
						}, 2500); 
				}
				else{
					$("#error").html("<font color='red'> Message not sent! Try again later  </font>");
				}
					
			} 
		}) 
}
 
 
function response(id)
{  	 
	 
	window.location="<?=base_url();?>support/startLiveChat/"+id; 
}
function check_if_all(value)
{  	 
     if(value=="all")
	 { 
		  $("#to").val('all');
		 document.getElementById('to').disabled = true;
		 
	 }
 
}

function getTime(timestamp,id){
//var parseTimestamp = function(timestamp) {
	var d = new Date( timestamp * 1000 ), // milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		timeString;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}

	timeString = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
 
	 return timeString;
}

</script>

