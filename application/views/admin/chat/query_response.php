<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>ARI Homes | Real Estate Management</title>
	 
	<!-- http://bootsnipp.com/snippets/4jXW -->
	<!--<link rel="stylesheet" href="assets/css/chat.css" />-->
	
<style>
 *{padding:0;margin:0;}

body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px; 
}

.float{
	position:fixed;
	width:100px;
	height:50px;
	bottom:20px;
	right:30px;
	background-color:#0C9;
	color:#FFF;font-size:12px;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #000;
}
 

.my-float{
	margin-top:22px;
}
 
</style>

<script type="text/javascript">	  
		$( document ).ready ( function () {
			$('#msg_block').show();
			//$('#success').modal('show');
			$('#nickname').keyup(function() {
				var nickname = $(this).val();
				
				if(nickname == ''){
					//$('#msg_block').hide();
				}else{
					$('#msg_block').show();
				}
			});
			
			// initial nickname check
			$('#nickname').trigger('keyup');
		});
		
</script>

</head>
<body>



<div class="container">

    <div class="row">
		<p>  </p> 
		 <input type="hidden" value="<?=$tokenId?>" class="form-control" name="token_id">
		 <input type="hidden" value="<?=$receiver?>" class="form-control" name="receiver">
		 <div  id="hide" style="right:2%;" >
		<div style="padding-bottom:8px;color:#fff;background:#006699;">
				  Recent Conversations    
		 </div> 
		
			<div class="panel panel-primary" id="scroll" style="overflow-y:scroll; height:400px">
	 
<hr/>
			 <div class="panel-body" >
				<ul class="chat" id="received" style="list-style-type:none;background:#fff;z-index:-1">
					
					
				</ul>
			</div>
			<div class="panel-footer">
				<div class="clearfix">
					<!--<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">
								Nickname:
							</span>-->
							<input id="nickname" value="<?=$this->session->userdata('first_name')?>" type="hidden" class="form-control input-sm" placeholder="Nickname..." />
						<!--</div>
					</div>-->
					<div class="col-md-12" id="msg_block">
						<div class="input-group">
							<input id="message" type="text" style="height:34px"class="form-control" placeholder="Type your message here..." />
							 <span class="input-group-btn">
								&nbsp; <button class="btn btn-warning btn-sm" id="submit"> Send</button>
							</span>
							<font style="float:right" class='btn red' type="reset" onclick='closeChat()'>
	<!--<i class='fa fa-close'> </i>--> Close Coversation
</font>  
						</div>
						<input id="counter" type="hidden" value="0">
					</div>
				</div>
			</div>
		</div>
    </div>
   </div>
 <!--
<a href="#" class="float">
	<i class="fa fa-wechat my-float" style="font-size:24px"></i> Support
</a>-->
</div>



</body>
</html>

<div class="modal fade" id="success" tabindex="-1" role="basic" aria-hidden="true">
	
	<div class="modal-body"><div class="modal-header" id="modal-header">  </div>
	<div class="row">
	<div class="col-md-12">  
		 
		<div class="panel-body">
				 
			</div>
		 
		</div>
	</div>
</div>
<div class="modal-footer" ><center>  
		<button type="button" data-dismiss="modal" id="got_it" class="btn green ">&nbsp;&nbsp;&nbsp; Got It  &nbsp;&nbsp;&nbsp; </button>
		</center>
</div>
<!-- /.modal-dialog -->
</div>


<script type="text/javascript">

var request_timestamp = 0;

var setCookie = function(key, value) {
	var expires = new Date();
	expires.setTime(expires.getTime() + (5 * 60 * 1000));
	document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

var getCookie = function(key) {
	var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
	return keyValue ? keyValue[2] : null;
}

var guid = function() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

if(getCookie('user_guid') == null || typeof(getCookie('user_guid')) == 'undefined'){
	var user_guid = guid();
	setCookie('user_guid', user_guid);
}


// https://gist.github.com/kmaida/6045266
var parseTimestamp = function(timestamp) {
	var d = new Date( timestamp * 1000 ), // milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		timeString;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}

	timeString = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
		
	return timeString;
}
var token="<?=$tokenId?>";
var receiver="<?=$receiver?>";
var sendChat = function (message, callback) {
	$.getJSON('<?php echo base_url(); ?>api/send_admin_message?token=' + token + '&receiver=' + receiver + '&message=' + message + '&nickname=' + $('#nickname').val() + '&guid=' + getCookie('user_guid'), function (data){
		callback();
	});
}


var append_chat_data = function (chat_data) {
	chat_data.forEach(function (data) {
		var is_me = "<?=$this->session->userdata('email');?>"; //data.guid == getCookie('user_guid');
		var photo=data.photo;
		if(!photo){ photo="me.jpg";}
		if(data.chatType==1){
			var html = '<li class="right clearfix" style="background:beige;padding:10px;">';
			html += '	<span class="chat-img pull-right">';
			html += '		<img  height="50" width="50"  src="<?=base_url();?>media/' + photo + '" alt="User Avatar" class="img-circle" />';
			html += '	</span>';
			html += '	<div class="chat-body clearfix">';
			html += '		<div class="header">';
			html += '			<small class="text-muted ">Sent on  ' + parseTimestamp(data.timestamp) + '</small>';
			html += '			&nbsp; <strong class="pull-right primary-font">' + data.nickname + ' &nbsp;</strong> ';
			html += '		</div> ';
			html += '		 <p style="text-align:left"> ' + data.message + '</p>';
			html += '	</div>';
			html += '</li><li> &nbsp;</li>';
		}else{
		  photo="ARI_Homes_Logo.png";
			var html = ' <li class="left clearfix" style="background:#fff;padding:1px;">';
			html += '	<span class="chat-img pull-left">';
			html += '		<img height="40" width="40" src="<?=base_url();?>images/login/' + photo+ '" alt="User Avatar" class="img-circle" />';
			html += '	 <strong class="primary-font"> &nbsp; ' + data.nickname + '</strong></span> <br/><br/>';
			html += '	<div class="chat-body clearfix" style="text-align:left;padding:10px;">';
			html += '	<div class="header">';
			//html += '	<strong class="primary-font"> ' + data.nickname + '</strong>';
			html += '<small class="text-muted"> Sent on ' + parseTimestamp(data.timestamp) + '</small>';
			//html += '			<small class="pull-right primary-font">' + parseTimestamp(data.timestamp) + '</small>';
			html += '</div>';
			html += '<p style="text-align:left">' + data.message + '</p>';
			html += '</div>';
			html += '</li><li> &nbsp;</li>';
		}
		$("#received").html( $("#received").html() + html);
	});
  
	$('#received').animate({ scrollTop: $('#received').height()}, 1000);
}


var update_chats = function () {
	if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
		var offset = 60*15;  // 15min
	
		request_timestamp = parseInt( Date.now() / 1000 - offset );
	}
	var tokenId="<?php echo $tokenId; ?>";
	var count =$("#counter").val();  
		if(parseInt(count)==0)
		{  
	       $.getJSON('<?php echo base_url(); ?>api/get_customer_messages?tokenId='+tokenId, function (data){
		append_chat_data(data);	

		var newIndex = data.length-1;
		if(typeof(data[newIndex]) != 'undefined'){ 
			request_timestamp = data[newIndex].timestamp;
		}
	});    
		}
	else if(parseInt(count)==5){ 
  $.getJSON('<?php echo base_url(); ?>api/get_customer_messages?tokenId='+tokenId+'&timestamp='+request_timestamp, function (data){
		append_chat_data(data);	

		var newIndex = data.length-1;
		if(typeof(data[newIndex]) != 'undefined'){
			request_timestamp = data[newIndex].timestamp;
		}
	});      
}
}

$('#minimize').click(function (e) {
	$("#scroll").hide();  
$(".float").show(); 	
});

$('.float').click(function (e) {
	$(".float").hide(); 
	$("#scroll").show();  
});

$('#submit').click(function (e) {
	e.preventDefault(); 
	var $field = $('#message');
	var data = $field.val();

	$field.addClass('disabled').attr('disabled', 'disabled');
	sendChat(data, function (){
		$field.val('').removeClass('disabled').removeAttr('disabled');
	});
});

$('#message').keyup(function (e) {
	if (e.which == 13) {
		$('#submit').trigger('click');
	//	$("#counter").val(0);
		scroll();
	}
});

setInterval(function (){
	  update_chats();
	  notify();
	  $("#counter").val(5);
	scroll();
}, 6000);

function scroll()
{
	var myDiv = document.getElementById("scroll");
	myDiv.scrollTop = myDiv.scrollHeight; 
	 //alternative you can use this to make scroll bar always at bottom
	 //$("#scroll").animate({ scrollTop: $(document).height() }, "fast");
}
function closeChat()
{ 
  
  var c=confirm("Close conversation?");
  if(c==true){
   var tokenId="<?=$tokenId;?>"; 
	$.ajax({
	url:'<?=base_url();?>support/changeStatus/'+tokenId, 
	type: 'POST',
	data:{'tokenId':tokenId},		
	success:function (data)
	{ 
		 
		var obj = JSON.parse(data);
		if(obj.result=="ok")
		{  
			alert("Conversation closed successfully!");
			window.location="<?=base_url();?>support/chat";
		}
		else
		{
			alert("Conversation not closed!");
		}
	}
})  

}else{ return false;}

} 
  
</script>