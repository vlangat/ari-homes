<?php  
ini_set('max_execution_time',1800);
?> 
<div class="page-container">
		
<style> 
/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px; padding:150px; text-align:center;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(images/loader-64x/Preloader_2.gif) center no-repeat #fff;
}
</style>
 <script>
	 // Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
	    
		<!-- BEGIN PAGE CONTENT BODY -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMBS -->
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="<?=base_url();?>"> Home </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						 Financial Reports 
						<i class="fa fa-circle"></i>
					</li>  				
					<li>
					 Agent Income Statement <span>  </span>
					</li> 
			</ul>
		
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="inbox">
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div  style="min-height:400px"> 
<div class="row"> <div class="se-pre-con"> <i class="fa fa-spinner spin"></i><font color="#006699" size="4"> Loading...please wait</font></div>
<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong> &nbsp; Agent Income Statement  </strong> </font> 
</div>
<div id="wait" style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;"><img src='demo_wait.gif' width="64" height="64" /><br>Loading..</div>

<div class="col-md-12">  &nbsp;  </div>
	 
<div  class="portlet-body" style="font-size:11px;width:100%" id="print_content" > 
<div class="col-md-12"  style="border:0px solid #bbb; font-size:11px">
<p>   </p>
<?php 
$email=$this->session->userdata('email');
$first_name=$this->session->userdata('first_name'); 
$total=0;
?>
<table width="100%" id="" cellspacing="0" cellpadding="12" class="table table-striped table-hover table-bordered">
		<tr>
			<td colspan="3"> 
				<strong>  <?=$company_name?> </strong>  
			</td>
		</tr>
		<tr> 
			<td>
				<strong>  Location: </strong>   <?=ucfirst(strtolower($company_location))?>	
			</td>
			<td>  </td>
			<td>  &nbsp; </td>
		</tr>  
		<tr> 
			<td>
				<strong> Year: </strong>   <?=$year?>  	
			</td>
			<td>  &nbsp;  </td>
			<td> <strong> Date :</strong>  <?=date('d/m/Y')?> </td>
		</tr>
		 
</table>

<table class="table table-striped table-hover table-bordered" border="1"  cellspacing="0" width="100%">
<thead>
	<tr>
		<th> Item </th> 
		<th> Jan </th>  
		<th> Feb </th>  
		<th> Mar </th>  
		<th> Apr </th>  
		<th> May </th>  
		<th> Jun </th>  
		<th> Jul </th>  
		<th> Aug </th>  
		<th> Sep </th>  
		<th> Oct </th>  
		<th> Nov </th>  
		<th> Dec </th>     
	</tr>
</thead>
<tbody id="data">

<tr><td> <strong> Income </strong> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td>  </tr>  
		<tr><td> Management Fees </td> <td id="jan">0 </td><td id="feb">0</td><td id="mar">0</td><td id="apr">0</td><td id="may">0</td><td id="jun">0</td><td id="jul">0</td> <td id="aug">0</td><td id="sep">0</td><td id="oct">0</td><td id="nov">0</td><td id="dec">0</td></tr>  
		<tr>
		<td> <strong> Expenses </strong> </td><td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td>  </tr>    </tr> 
		<tr>
			<td> Business Expenses </td> <td id="jan2">0</td><td id="feb2">0</td><td id="mar2">0</td><td id="apr2">0</td><td id="may2">0</td><td id="jun2">0</td><td id="jul2">0</td> <td id="aug2">0</td><td id="sep2">0</td><td id="oct2">0</td><td id="nov2">0</td><td id="dec2">0</td>
		</tr>
		<tr>
		<td> 
			<strong> Gross Income </strong> </td> <td id="gross_income_jan">0</td><td id="gross_income_feb">0</td><td id="gross_income_mar">0</td><td id="gross_income_apr">0</td><td id="gross_income_may">0</td><td id="gross_income_jun">0</td><td id="gross_income_jul">0</td><td id="gross_income_aug">0</td><td id="gross_income_sep">0</td><td id="gross_income_oct">0</td><td id="gross_income_nov">0</td><td id="gross_income_dec">0</td>
		</tr>  
		
		<tr> <td> &nbsp; </td><td colspan="12"> </td> </tr> 
		<tr><td> <strong> Total <?=$year?> </strong> </td> <td colspan="12">  <strong id="total">0</strong></td> </tr>	
</tbody>		
</table>
 
<p>   </p>
 

</div>
</div>
<div class="col-md-12">
 <a  href="javascript:;" class="btn green"  id="btn_receipt" >  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
 <!--<a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  
-->
 &nbsp; &nbsp; 
	<a href="javascript:;" id="myButtonControlID" onclick="clickme()" class="btn grey" style="color:brown"><i class="fa fa-file-excel-o" style="font-size:18px;"></i>&nbsp;<b>Export to Excel</b></a>
	<br/>
<!--
 <a  href="javascript:;" class="btn grey"  id="importCSV">  <i class="fa fa-file-excel-o" style="font-size:18px;"></i><b>  Export to Excel  </b> &nbsp;</a>  
-->
 <font style="" id="error1"> </font>
</div>  
	 	 
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
		</div>
		<!-- END PAGE CONTENT BODY -->
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
  
</div>
</div>
</div>
</div> 
<!-- END CONTAINER -->
 <div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> ARI Homes  Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div> 
   

<script language="javascript">
$(document).ready(function()
{  
  getData(); 
});


 
 $("#btn_receipt").click( function () {
	 var y="<?php echo $year;?>";  
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Agent Income Statement </title>');
            printWindow.document.write('</head><body>');
            printWindow.document.write('<h4>AGENT INCOME STATEMENT YEAR '+y+'</h4><hr/>'+divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }); 
 		
 $("#btn_email").click( function () {  
		var divContents = $(".portlet-body").html();   
        var email="<?=$email?>";  
        var fname="<?=$first_name?>";  
        var from="<?=$company_name?>"; 
		if(!email || email==""){ 
		$("#err").html("<font color='red'> Email not sent. Email address is empty. </font>");
		$("#success").modal('show');
		return false;}
		$("#err").html("<font color='#006699'> Sending email...please wait</font>");
		$("#success").modal('show');
		$.ajax({
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Income Statement", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
					$("#err").html("<font color='green'>Income Statement sent to   <u>"+email+"</u></font>");
					$("#success").modal('show');
				}
				else
				{  
					$("#err").html("<font color='red'> Income Statement  not sent to   <u>"+email+"</u></font>");
					$("#success").modal('show');
				}
				
			}
			
 })
 
});


function getData()
{ 
 
	$.ajax({
				'url':"<?=base_url();?>financial/profit_loss_account",
				'type':"POST",
				async:false, 
				success:function(data)
				{ 
					var obj=JSON.parse(data);
					var data = obj.data;
					$("#jan").html(obj.jan);$("#feb").html(obj.feb);$("#mar").html(obj.mar);$("#apr").html(obj.apr);$("#may").html(obj.may);$("#jun").html(obj.jun);   
					$("#jul").html(obj.jul);$("#aug").html(obj.aug);$("#sep").html(obj.sep);$("#oct").html(obj.oct);$("#nov").html(obj.nov);$("#dec").html(obj.dec); 
					$("#jan2").html(obj.jan2);$("#feb2").html(obj.feb2);$("#mar2").html(obj.mar2);$("#apr2").html(obj.apr2);$("#may2").html(obj.may2);$("#jun2").html(obj.jun2);   
					$("#jul2").html(obj.jul2);$("#aug2").html(obj.aug2);$("#sep2").html(obj.sep2);$("#oct2").html(obj.oct2);$("#nov2").html(obj.nov2);$("#dec2").html(obj.dec2); 
					$("#gross_income_jan").html(parseInt(obj.jan)-parseInt(obj.jan));$("#gross_income_feb").html(parseInt(obj.feb)-parseInt(obj.feb2));
					$("#gross_income_jan").html(parseInt(obj.jan)-parseInt(obj.jan));$("#gross_income_feb").html(parseInt(obj.feb)-parseInt(obj.feb2));
					$("#gross_income_may").html(parseInt(obj.may)-parseInt(obj.may2));$("#gross_income_jun").html(parseInt(obj.jun)-parseInt(obj.jun2));
					$("#gross_income_jul").html(parseInt(obj.jul)-parseInt(obj.jul2));$("#gross_income_aug").html(parseInt(obj.aug)-parseInt(obj.aug2));
					$("#gross_income_sep").html(parseInt(obj.sep)-parseInt(obj.sep2));$("#gross_income_oct").html(parseInt(obj.oct)-parseInt(obj.oct2));
					$("#gross_income_nov").html(parseInt(obj.nov)-parseInt(obj.nov2));$("#gross_income_dec").html(parseInt(obj.dec)-parseInt(obj.dec2));
					$("#total").html(obj.total);  
				}
				
	});			
	
	
}

$("#importCSV").click(function () {  
	var year="<?=$year?>";    
	$("#error1").html("<font color='brown'>Exporting document Please wait...</font>");
	 	 
	 setTimeout(function() { $("#error1").html("<font color=''> </font>"); }, 6000);
	 
	 window.location="<?=base_url();?>financial/profit_lossCSV/"+year; 
}); 
 
function clickme(e) {
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=print_content]').html()));
   // e.preventDefault();
}
</script>