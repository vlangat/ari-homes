<?php  
ini_set('max_execution_time',1800);
?> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
	<!-- BEGIN PAGE CONTENT BODY -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMBS -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?=base_url();?>"> Home </a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					 Financial Reports 
					<i class="fa fa-circle"></i>
				</li>  				
				<li>
				Property Report <span>  </span>
				</li> 
			</ul> 
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="inbox">
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div  style="min-height:400px"> 
<div class="row">
	 
<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong> &nbsp; Property Report  </strong> </font> 
	   </div>
	   <div class="col-md-12">  &nbsp;  </div>	   
	   <?php $logo=""; $company_name=""; foreach($company_details->result() as $c){ if($c->company_code==$this->session->userdata('company_code')){ $company_name=$c->company_name; $logo=$c->logo;} }?>
			 
<form action="<?=base_url();?>financial/property_report" method="post">
<div class="col-md-5">
	<div class="form-group">
				<?php foreach($property->result() as $rows){ }?>
				<label class="control-label">Select Property </label>
				<select class="form-control" name="property" id="property" required>  
					<option value=""> select property  </option>
					<?php foreach($property->result() as $row):?> 
						<option value="<?=$row->id?>"> <?=$row->property_name?>  </option>
					<?php endforeach;?>
				</select>
	</div> 
</div>
<div class="col-md-5">

<div class="form-group">
 <label class="control-label">Select Month </label>
		   <select class="form-control" name="month" id="month"> 
		   <option value="01"> Jan  </option> 		   
		   <option value="02"> Feb  </option> 		   
		   <option value="03"> Mar  </option> 		   
		   <option value="04"> April  </option> 		   
		   <option value="05"> May  </option> 		   
		   <option value="06"> June  </option> 		   
		   <option value="07"> July  </option> 		   
		   <option value="08"> Aug  </option> 		   
		   <option value="09"> Sep  </option> 		   
		   <option value="10"> Oct  </option> 		   
		   <option value="11"> Nov  </option> 		   
		   <option value="12"> Dec  </option> 		   
			</select>
			</div>
</div>
<div class="col-md-2">
 <label class="control-label">&nbsp; </label>
   <br/> 
 <button type="submit" class="btn green" > Generate report </button>
 
 </div>
</form>

<?php   if($prop_report=="")
 {
	   
 }
 else{ $property_rent_vat=0; $vat_management_fee=0;
	 $property_id =""; $property_name=""; $lr_no=""; $location=""; $landlord_name="-"; $email=""; $bank="-"; $bank_branch="-"; $acc_no="-"; $landlord_name="-"; $management_fee=0;
	foreach($prop_report->result() as $p)
	{
		$property_name=$p->property_name; $property_id=$p->id; $vat_management_fee=$p->vat_management_fee;$management_fee=$p->management_fee; $property_rent_vat=$p->property_rent_vat; $lr_no=$p->lr_no; $location=$p->location;
		if($management_fee==""){ $management_fee=0;}
		if($property_rent_vat==""){ $property_rent_vat=0;}
		if($vat_management_fee==""){ $vat_management_fee=0;}
		foreach($landlord->result() as $l){ 
		if($p->landlord==$l->id){
			$landlord_name=$l->full_name;
			$acc_no=$l->bank_acc; $bank=$l->bank_name; $email=$l->email;$bank_branch=$l->bank_branch;
			} 
		}
	}
	 ?>
<div  class="portlet-body"  style="font-size:11px;"> 
<div class="col-md-12"  style="border:0px solid #bbb;font-size:11px;">
<p>   </p>
<table  id="" cellspacing="0" cellpadding="12" class="table table-striped table-hover table-bordered">
		 
		<tr> 
		    <td> 
				<strong> Property:</strong> <?=$property_name?>  
			</td>
			<td>
				<strong> LR/No: </strong>  <?=$lr_no?>    	
			</td>
			<td>  <strong>  Location: </strong>  <?=$location?>  </td> 
		</tr>
		<tr> 
			<td>
				<strong> Landlord: </strong>  <?= ($landlord_name)?>  	
			</td>
			<td>  <strong>  Account No: </strong>  <?=$acc_no?> </td>
			<td> <strong> Bank :</strong>  <?=$bank?> &nbsp; <strong>Branch: </strong>  <?=$bank_branch?> </td>
		</tr>
		<tr> 
			<td>
				<strong> Total Units: </strong>  <?=$total_units?>	
			</td>
			<td>  <strong>  Occupied Units: </strong>  <?=$total_tenants?>	 </td>
			<td> <strong> Vacant Units :</strong> <?=$total_units-$total_tenants;?> </td>
		</tr>
		<tr> 
			<td>
				<strong> Month: </strong>  <?=date('F', mktime(0, 0, 0, $month, 1));?>
 
			</td>
			<td>  <strong>  Year: </strong>  <?=date('Y')?> </td>
			<td> <strong> Date :</strong>  <?=date('d/m/Y')?> </td>
		</tr>
		 
</table>


<table class="table table-striped table-hover table-bordered" border="1" width="100%" cellspacing="0" cellpadding="4">
<thead>
	<tr>
		<th> Unit No</th> 
		<th> Tenant</th>
		<th> Balance B/F </th>
		<th> Expected Rent </th> 
		<th> Received Rent </th> 
		<th> Balance C/F </th>  
		<th> &nbsp;  </th>  
	</tr>
</thead>
<tbody>
		<?php  $obj2=new rent_class();
		$balance_brought=0;$balance=0; $total_rent_collect=0; $paid=0; $expected_amount=0; $e=0; $pd=0; $bf=0;
		
		foreach($tenants->result() as $t){  
		$tenant_id=$t->id; 
		$e=$obj2->expected_pay($tenant_id, $month); 
		$total_rent_collect=$total_rent_collect+$obj2->get_receipt_rent($tenant_id, $month);
		if($t->tenant_deleted==2 && $e==0){
			 continue;
		}
		?>
		
		<tr><td> <?=$t->house_no?> </td> 
		<td> 
		<?php $middle_name=""; $last_name="";
			if($t->middle_name=="" || $t->middle_name=="0"){ $middle_name="";} else{ $middle_name=$t->middle_name; }
			if($t->last_name=="" || $t->last_name=="0"){ $last_name="";} else{ $last_name=$t->last_name; }
			$other_names=ucfirst(strtolower($middle_name))." ".ucfirst(strtolower($last_name));?>
		<?=$t->first_name .' '. $other_names;?>
		</td>
		<td><?=$bf=$obj2->balance_forwarded($tenant_id,$month); $balance_brought=$balance_brought+$obj2->balance_forwarded($tenant_id,$month);?> </td><td><?php echo $e; $expected_amount=$expected_amount+$e;?> </td><td> <?php $paid=$paid+$obj2->paid_rent($tenant_id, $month); echo $pd=$obj2->paid_rent($tenant_id, $month);?> </td><td> <?=$b=$bf+$e-$pd; $balance=$balance+$b;?> </td>
        <td>&nbsp; </td>
		</tr>
		<?php }?>		 	
		<tr><td> <strong> Sub Total Rent Collected </strong> </td><td>   </td><td>  <strong><?=$balance_brought?>  </strong></td> <td>  <strong><?=$expected_amount?>   </strong> </td><td>  <strong> <?=$paid?>  </strong> </td><td>  <strong> <?=$balance?>  </strong> </td> </tr>	
		<?php if($property_rent_vat>0){?>
		<tr><td> <strong> VAT (<?=$property_rent_vat?>%) </strong> </td> <td> </td> <td> </td><td> </td>
		<!--<td> KES <?//=($property_rent_vat/100)*$paid?>   </td>-->
		<td>  
		<?php 
		   
		$vat_amount=($total_rent_collect*100)/($property_rent_vat+100); 
		//$vat_amount=100/($property_rent_vat+100)*$paid; 
		 
		//echo "KES ".$y=round($property_rent_vat/100*$vat_amount);
		echo "KES ".$y=round($total_rent_collect-$vat_amount);
		?>   
		</td>
		
		<td>  &nbsp;  </strong></td> <td>  <strong> &nbsp; </strong> </td> </tr>	
		<?php }?>
		</tbody>
	</table>
<p>   </p>
<p> <font size="4"> <strong> Less Deductions </strong> </font> </p>
<table class="table table-striped table-hover table-bordered"  border="1" width="50%" cellspacing="0" cellpadding="4">
<thead>
	<tr>
		<th>Item </th> 
		<th>Amount</th>
	</tr>
</thead>
<tbody>
<tr><td> Management Fee (<?=$management_fee?>%) </td> <td><?=$m=$management_fee/100*$total_rent_collect?> </td> </tr>
<?php $m_vat=$vat_management_fee/100*$m; 
if($vat_management_fee >0){
?>

<tr><td> VAT (<?=$vat_management_fee?>%) on Management Fee  </td> <td> <?=$m_vat?> </td> </tr>

<?php }?>
<tr><td> Property Expense </td> <td> <?=$ex=$obj2->property_expense($property_id, $month);?> </td> </tr>
<tr><td> Reimbursement Deposits </td> <td> <?=$reim=$obj2->reimbursement($property_id, $month);?> </td> </tr>
<tr><td> <strong> Total Deductions </strong> </td> <td>  <strong> <?=$deducts=$m+$m_vat+$ex+$reim?>  </strong> </td>  </tr> 
 
<tr><td> <strong> Amount Payable = [Sub total Rent collected]- [Total Deductions] </strong> </td> <td> <strong> <?=$paid-$deducts?> </strong> </td>  </tr>
</tbody>
</table>

</div>
</div>
<div class="col-md-12">
 <a  href="javascript:;" class="btn green"  id="btn_receipt" >  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
 <a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  
<a  href="javascript:;" class="btn grey"  id="importCSV" >  <i class="fa fa-file-excel-o" style="font-size:18px;"></i><b>  Export to Excel  </b> &nbsp;</a>  
 <font style="" id="error1"> </font>
 </div>  
<?php } ?> 	 
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
          
        </div>
        </div>
      </div>
      </div> 
        <!-- END CONTAINER -->
 
   <div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> ARI Homes Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
	</div>
</div>
        
<div id="sendMail" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> ARI Homes Message </b></h5>
				<hr/>
					<p>
					    This will Email this report to <font id="email_id"> </font>
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="send_email" class="btn green">&nbsp; Yes &nbsp; </button>
			 <button type="button" data-dismiss="modal" class="btn default">&nbsp; Cancel  &nbsp; </button> 
		</center>
	</div>
</div>
   

<script language="javascript">
$(document).ready(function()
{ 
  var p="<?php echo $property_id;?>";   
  $("#property").val(p); 
  var m="<?php echo $month;?>";   
  $("#month").val(m);   
});

var month="<?=strtoupper(date('F', mktime(0, 0, 0, $month, 1)));?>"; 
 var email="<?=$email?>";  
var fname="<?=$landlord_name?>";  
var from="<?=$company_name?>";
var email="<?=$email?>"; 
 var h="";
 $("#btn_receipt").click( function () {
	 $.ajax({
			'url':"<?=base_url();?>rent/getPrintHeader",
			'type':"POST",
			async:false, 
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
			       h=(obj.msg);
				}
				
			}
			
	 }); 
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Property Report </title>');
            printWindow.document.write('</head><body>'+h+'<hr/><center><h3>PROPERTY REPORT FOR THE MONTH OF '+month+'</h3></center><hr/>');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }); 


 $("#btn_email").click( function () {  
		if(!email){ 
		$("#err").html("<font color='red'> Email not sent. Landlord email address is empty. </font>");
		$("#success").modal('show');
		return false;}		
		 $("#email_id").html(fname+" <u> "+email+" </u>");
		 document.getElementById('send_email').disabled = false;
		
		$("#sendMail").modal('show');
 });
 
 $("#send_email").click( function () {  
		var divContents = $(".portlet-body").html();   
		if(!email || email==""){ 
		$("#err").html("<font color='red'> Email not sent. Landlord email address is empty. </font>");
		$("#success").modal('show');
		return false;}
		$("#error").html("<font color='#006699'> Sending email...please wait</font>");
		//$("#success").modal('show');
		$.ajax({
			'url':"<?=base_url();?>payment/sendRentStatement/statement",
			'type':"POST",
			async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':'Property Report', 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   $("#error").html("");
					$("#sendMail").modal('hide');
					$("#err").html("<font color='green'> Property Report sent to <u>"+email+"</u></font>");
					$("#success").modal('show');
				}
				else
				{  $("#sendMail").modal('hide');
					 $("#error").html("");
					 $("#err").html("<font color='red'> Property Report not sent to   <u>"+email+"</u></font>");
					$("#success").modal('show');
				}
				
			}
		}) 
 });
		
		
$("#importCSV").click( function () {  
	var prop_id="<?=$property_id?>";
	var month="<?=$month?>";  
	$("#error1").html("<font color='brown'>Exporting document Please wait...</font>");
	 	 
	 setTimeout(function() { $("#error1").html("<font color=''> </font>"); }, 10000);
	 
	 window.location="<?=base_url();?>financial/property_reportCSV/"+prop_id+"/"+month; 
});

 
</script>