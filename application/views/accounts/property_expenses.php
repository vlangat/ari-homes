<!-- BEGIN PAGE CONTENT BODY -->
<script language='JavaScript'>  

function validate()
	{   
		var supplier=$("#supplier").val();
		var pay_mode=$("#pay_mode").val();
		var amount=$("#amount").val();
		var receipt_no=$("#receipt_no").val();
		var pay_date=$("#pay_date").val(); 
		if(supplier==""||supplier==null||supplier=="No results"){ $("#error1").html("<font color='red'> Please enter at least 1 Supplier  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
		if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
		 
	}
	
	function validate_edit()
	{   
		var s_name=$("#s_name").val();
		var receipt_no=$("#s_receipt_no").val();
		var amount=$("#s_amount").val();  
		var description=$("#s_description").val();  
		if(s_name==""||s_name==null){ $("#edit_error").html("<font color='red'> Please enter at least 1 Supplier  </font>");$("#s_name").focus();return false;}
		 if(amount==""||amount==null){ $("#edit_error").html("<font color='red'> Amount field is empty </font>");$("#s_amount").focus(); return false;}
		if(/^[a-zA-Z0-9- ]*$/.test(amount) == false){ $("#s_amount").focus(); $("#edit_error").html("<font color='red'>Amount contains illegal characters </font>");  return false; }else{$("#edit_error").empty();}
	if(/^[a-zA-Z0-9- ]*$/.test(name) == false){ $("#edit_user_name").focus(); $("#edit_error").html("<font color='red'>Name  should not have unallowed characters </font>");  return false; } else{$("#edit_error").empty();} 
	if(/^[a-zA-Z0-9- ]*$/.test(receipt_no) == false){ $("#s_receipt_no").focus(); $("#edit_error").html("<font color='red'>Receipt No should not have unallowed characters </font>");  return false; } else{$("#edit_error").empty();} 
	if(/^[a-zA-Z0-9- ]*$/.test(description) == false){ $("#s_description").focus(); $("#edit_error").html("<font color='red'>Description contains unallowed characters </font>");  return false; } else{$("#edit_error").empty();} 
	
	return confirm("Update details for "+s_name+"?");
	}
	
</script>
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Expenses  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Property Expenses </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="col-md-12"    style="background:#1bb968;padding:6px;">
				&nbsp; <font color="#ffffff"> Property Expenses  </font>
			</div>	
		 <div class="col-md-12">  &nbsp;  </div>	
	<div>
<span>   </span>
<!-- <?=base_url();?>rent/save_expenses/property_expenses-->
<!--<form action="#" method="post" onsubmit="return false">-->
<div class="row">

<div class="col-md-4">   
<input  class="form-control" type="hidden"   value="1" name="expense_type" >
<input  class="form-control" type="hidden" id="edit_id" value="" name="edit_id" >
				<div class="form-group">
						<label class="control-label">Property   </label>
						   <select class="selectpicker" required="required" name="property" data-live-search="true"  id="property"  title="Select property...">  
								 <?php foreach($property->result() as $row):?>
									
									<option value="<?=$row->id?>"> <?=$row->property_name?>  </option>
								<?php endforeach;?>
							</select>
								<span>   </span>
					<label id="error1">  </label>
				</div>
</div>
<div class="col-md-4">   
<input class="form-control" type="hidden" id="edit_id" value="" name="edit_id"/>
	<div class="form-group">
			<label> Supplier </label> <br/> 
					<select  class="selectpicker"  data-live-search="true" name="from"   id="supplier"  onchange="clearMsg()" onblur="validate_char('supplier','error1')"  title="Select supplier...">
						 <?php foreach($suppliers->result() as $row){?>
								<option value="<?=$row->id?>">
									<?=$company=$row->company_name;?>
								</option>
						 <?php } ?>
					</select> 
					<span>   </span>
		<label id="error2">  </label>
	</div>
</div>
<div class="col-md-4">     
			<div class="form-group">
					<label>Invoice Number (Optional) </label>
					 <input type="text" class="form-control" onchange="clearMsg()" name="invoice" onblur="validate_char('invoice','error3')" id="invoice"  value="">
					 
				 <label id="error3">  </label>
			</div>
</div>
 
</div>
<div class="row">	

<div class="col-md-4"> 
				
				<div class="form-group">
						<label> Amount </label>
						<input type="text" class="form-control" onkeypress="return checkIt(event)"  name="amount"   id="amount"  value=""   onchange="clearMsg()" >
					 <label id="error4">  </label>
				</div> 
				
</div>
 
<div class="col-md-4"> 
		<div class="form-group">
				<label> Payment Mode </label>
				<select class="form-control"   name="pay_mode" id="pay_mode" onchange="disable_method(this.value)">
					<option value=""> Not selected </option>
					<option> Cash </option>
					<option> Cheque </option>
					<option> Mpesa </option>
					<option> Bank Receipt </option>
				</select>
			 <label id="error5">  </label>
		</div>  
</div>  
	<div class="col-md-4"> 		

			<div class="form-group">
					<label> Mpesa, Cheque   Number </label>
					<input type="text" class="form-control"  name="receipt_no"   id="receipt_no" onchange="validate_char('receipt_no','error5')" >
				 <label id="error6">  </label>
			</div>
			

		</div>
 
   <div class="col-md-12">  
			<div class="form-group">
					<label> Payment For </label>
					<textarea class="form-control"  name="pay_for" id="pay_for" rows="2" onchange="validate_char('pay_for','error7')"></textarea>
				 <label id="error7">  </label>
			</div>
		</div>
	 
	<div class="col-md-3">   
			<button  class="btn green" id="pay_expense" onclick="pay_expense()" <?=$disabled;?>>  Submit </button> <p>
			</div>
	<div class="col-md-6"> 
		<p id="save_msg"> 	</p>
				 
	</div>
</div> 

</form>
</div> 
 
 <!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit ">
	<!--<div class="portlet-body">-->
	<hr/>
 <h4><strong>  Suppliers Payment Details </strong> </h4>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr> 
		<th> #</th> 
		<th> Date </th>
		<th> Property </th>
		<th> Supplier </th>
		<th> Invoice No </th>
		<th> Payment for </th>
		<th> Payment Mode </th>
		<th> Amount </th> 
		<th> &nbsp; </th> 
		<th> &nbsp; </th> 
	</tr>
</thead>
<tbody>
   <?php $i=1; foreach($paid_expenses->result() as $row){?>
		<tr><td><?=$i?></td><td>  <?=$row->date_paid?> </td><td> <?php $propertyName=""; foreach($property->result() as $r){  if($row->property_id==$r->id){$propertyName=$r->property_name;}}  echo $propertyName;?> </td><td> <?=$row->company_name?> </td> <td>  <?=$row->supplier_invoice_no?></td> <td> <?=$row->description ?> </td>  <td> <?=$row->payment_mode?> </td> <td>  <?=$row->amount?> </td> <td> <a href="javascript:;" onclick="viewPayment('<?=$row->id?>','<?=$row->company_name?>')"> <i class="fa fa-edit"> </i> Edit </a> </td> 
    <td> <a href="javascript:;" onclick="deletePayment('<?=$row->id?>')"> <i class="fa fa-trash"> </i> Remove </a> </td>
   <?php 
	$i++; 
	}
?>
</tbody>
</table> 

<!--</div>-->
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END CONTENT --> 
<!-- END CONTAINER -->

<div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-header">
	<b> Success </b> 
	</div>
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p>
				<?=$this->session->flashdata('temp');?>
				</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="600" aria-hidden="true">
        
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Register Supplier </b> </font></h4>
		   
	</div>
<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12">  
	<p> 
		<label class="control-label"> Company Name  </label>
		<input   class="form-control" type="text" id="name" name="name"/> </p>
		<p> 
		<label class="control-label"> Contact Person Name  </label>
		<input   class="form-control" type="text" id="contact_person" name="contact_person"/> </p>
		<p>
		<label class="control-label">Email </label>
			<input   class="form-control" type="email" id="email" name="email"> </p>
		 
		<label class="control-label">Phone No </label>
			<input   class="form-control" type="text" id="phone"  name="phone"> </p>
		 
		<p>
		<label class="control-label"> Product Description </label>
		 
		<textarea class="form-control" id="description" name="description" >  </textarea>
		</p>
</div>
</div>  <span id="status_msg">   </span>
</div>
<div class="modal-footer" >
		<input type="submit" class="btn green"  id="submit" <?=$disabled;?> value="Save"> 
		<button type="button" data-dismiss="modal" class="btn btn-outline dark" > Cancel </button>
</div> 
</div>
</div>
 	
 <!-- responsive -->
<div id="add_new" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
	 <b> Specify other Payment </b>
	 </div>
	 <div class="modal-body">
			<div class="row">
			<div class="col-md-12"> 
			<p>
			<label class="control-label">Name  </label>
			<input   class="form-control" type="text" placeholder="" id="amenity_name"> </p>
			<p>
			</div>
		</div>
	</div> 
<div class="modal-footer" > <span id="amenity_error" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_new_one" <?=$disabled;?>> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>



<div id="edit_payment" class="modal fade" tabindex="-1" data-width="800" aria-hidden="true"> 
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Payment Details for  <font id="for">  </font></b> </font></h4>
	 </div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<form action="#" method="post" onsubmit="return false">
			<div class="row">
			
				<div class="col-md-6">
				<p> <label class="control-label"> Name </label> <br/>
							<input   class="form-control" type="hidden" id="editing_id" name="edit_id" >
							<input   class="form-control" type="hidden" id="edit_property_id" name="property_id" >
							<input   class="form-control" type="text"  value="" name="from" id="s_name" readonly> 
								 
						</p>
						<p> <label class="control-label"> Amount Paid </label>
							<input   class="form-control" type="text" name="amount" id="s_amount">
							 
						</p> 
						<p> <label class="control-label">Payment Method </label>
								<select class="form-control" type="text"  name="s_pay_mode" id="payment_method" onchange="disable_method(this.value)">
									<option value="Cash">Cash </option>
									<option value="Cheque">Cheque </option>
									<option value="Mpesa">Mpesa </option>
									<option value="Bank Receipt"> Bank Receipt </option>
								</select> 
						</p>
				</div>
				<div class="col-md-6">   
					 <p> <label class="control-label">Invoice  No </label>
							<input   class="form-control" type="text" readonly name="receipt_no"  id="s_invoice">
							 
						</p> 
						 
						<p> <label class="control-label">Supplier's Invoice No </label> 
							   <input   class="form-control" type="text"   name="invoice" id="supplier_invoice">
						 </p>
						<p> <label class="control-label"> Payment Method Code </label>
							<input   class="form-control" type="text"   name="receipt_no" id="s_pay_method_code">
						</p> 						
				</div>
				<div class="col-md-12"> 
				<p> <label class="control-label"> Payment For </label>
							<textarea class="form-control"  name="pay_for" id="s_pay_for"> </textarea>
						</p>
				</div> 
				 <div class="col-md-12"> 
					<label id="status_message">   </label>
				 </div>
		<div class="modal-footer" >
		<center> 
			<button type="submit" class="btn green" onclick="done_edit()" <?=$disabled;?>> Save Changes </button>
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
	</center>
</div>	
</form> 	 
</div>			
	</div>	
</div>		 
</div>		 
	
<script language="javascript">
 
function validate_char(id,identifier)
{
	clearMsg();
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		$("#"+identifier).html("<font color='red'> Input   contains Illegal characters </font>"); 
		document.getElementById(id).value="";
		//$("#success").modal('toggle');  
        return false;
		}else
		{
			$("#"+identifier).empty(); 
		}
    
}
  	  
function deletePayment(id)
  { 
	var c=confirm("Delete this record?");
	if(c==true){
	$.ajax(
	{
		url:"<?=base_url();?>financial/remove_expenses",
		type:"POST", 
        data:{'id':id},		
		success:function(data)
		{
			 
			 var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 { 
		       setTimeout(function()
				{
					 location.reload(); 
                },2000);  
			 }
			 else
			 {
				alert("Data not removed"); 
			 }
		}})		
	}
	else
	{
		return false;
	}	
}
  
  
$(function(){ 
    $("#supplier" ).autocomplete({
        source: '<?=base_url();?>tenants/search_supplier/?',
		selectFirst: true
    });  
}); 


$(document).ready(function (){ 
 var success_msg="<?=$this->session->flashdata('temp')?>";
 if(!success_msg){  }else{ $("#success").modal('show'); }
 $("#add_new_one").click(function(){ 
 var name=$("#amenity_name").val();
 if(!name){ $("#amenity_error").html("<font color='red'> Item name required </font>"); $("#amenity_name").focus(); return false;}
  $.ajax(
  {
		url:"<?=base_url();?>payment/add_new_amenity",
		type:"POST",  
		data:
			{
				'amenity_name':name
			},
		success:function(data)
		{  
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 { 
				 $("#amenity_error").html("<font color='green'> Item added successfully </font>");
				  setTimeout(function(){
						pay_for(); 
						$("#pay_for").val(obj.id);
						$("#amenity_error").empty(); 
						$("#amenity_name").val(''); 
						$('#add_new').modal('hide'); 
						 
                      }, 2000); 
					      
			 }
			 else
			 { 
				$("#amenity_error").html("<font color='red'> Not added. Try again later </font>");
			 }

		}			 
  })
 
 });
   
});
 
  
  function add_other(value)
  {
	  if(value=="other")
	  {
		$("#add_new").modal('show');
	  }
  }
 
function viewPayment(id,name)
{
   
$.ajax(
{
	url:"<?=base_url();?>rent/get_paid_expenses/"+id,
	type:"POST",  
	success:function(data)
	{
		$("#pay_for").empty();
		 var obj=JSON.parse(data); 
		 if(obj.result=="ok")
		 {   
			 var data=obj.data; 
			 for(var i=0;i<=data.length; i++)
			 {
				 var supplier=data[i];
				 var property_id=supplier['property_id'];
				 var no=supplier['id'];
				 var date=supplier['date_paid'];
				 var pay_mode=supplier['payment_mode'];  
				 var invoice=supplier['invoice_no'];  
				 var amount=supplier['amount']; 
				 var payment_for=supplier['description'];
				 var transaction_no=supplier['supplier_invoice_no'];
				// var description=supplier['description'];
				 var payment_method_code=supplier['payment_method_code'];
				 $("#s_name").val(name);
				 $("#editing_id").val(no);
				 $("#for").html(name.toUpperCase());  
				 $("#s_amount").val(amount); 
				$("#edit_property_id").val(property_id); 
				$("#payment_method").val(pay_mode); 
				  $("#s_pay_for").val(payment_for);
				 $("#supplier_invoice").val(transaction_no);
				 $("#s_invoice").val(invoice); 
				 $("#s_pay_method_code").val(payment_method_code); 
				 // $("#s_description").val(description);
				 $("#edit_error").empty();
				 if(pay_mode=="Cash")
				 {
					 document.getElementById('s_pay_method_code').disabled = true;
				 }
				 else{
					 document.getElementById('receipt_no').disabled = false;
			         document.getElementById('s_pay_method_code').disabled = false;
				 }
				$("#edit_payment").modal('show');
			 }
		 }
	}
  
  
})
}
  
   function pay_expense()
  {
	   //document.getElementById('pay_expense').disabled = true;	 
		var property=$("#property").val();    
		var pay_mode=$("#pay_mode").val();    
		var amount=$("#amount").val();    
		var receipt_no=$("#receipt_no").val();    
		var invoice=$("#invoice").val();    
		var supplier=$("#supplier").val();    
		var description=$("#pay_for").val();  
		if(!property){ $("#error1").html("<font color='red'> Please select at least 1 property  </font>");return false;} else{ $("#error1").html("");}
		if(!supplier){ $("#error2").html("<font color='red'> Please enter at least 1 Supplier  </font>");return false;}else{ $("#error2").html("");}
		//if(!invoice){ $("#error3").html("<font color='red'> Please enter Invoice No  </font>");return false;} else{ $("#error3").html("");}
		if(!amount){ $("#error4").html("<font color='red'> Enter Amount  </font>");return false;}else{ $("#error4").html("");}
		if(!pay_mode){ $("#error5").html("<font color='red'> Please select payment mode </font>");return false;}else{ $("#error5").html("");}
		if(!receipt_no & pay_mode !="Cash"){ $("#error6").html("<font color='red'> Receipt Number required </font>");return false;}else{ $("#error6").html("");}
		//if(!pay_mode_code){ $("#error7").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;} else{ $("#error7").html("");}
	   
    $("#save_msg").html("<font color='green' size='3'>Saving data...please wait</font>");
	$.ajax(
	{
		url:"<?=base_url();?>rent/save_expenses/",
		type:"POST",
			"data":{
				'edit_id':'',
				'from':supplier,
				'property':property,
				'invoice':invoice,
				'receipt_no':receipt_no,
				'pay_mode':pay_mode,
				'expense_type':1,
				'pay_for':description,
				'amount':amount
			},		
		success:function(data)
		{
			 
			 var obj=JSON.parse(data);   
			 if(obj.result=="ok")
			 {   
				setTimeout(function()
				{
					$("#save_msg").html("<font color='green' size='3'>"+obj.msg+"</font>"); 
					location.reload(); 
                },2500); 
				 
			 }
			 else
			 {
				 $("#save_msg").html("<font color='green' size='3'>"+obj.msg+"</font>");
				 document.getElementById('pay_expense').disabled = false;	 
			 }
		}  
	  
  })
  }
  function done_edit()
  {
	   
		var edit_id=$("#editing_id").val();    
		var property=$("#edit_property_id").val();    
		var pay_mode=$("#payment_method").val();    
		var pay_mode_code=$("#s_pay_method_code").val();      
		var amount=$("#s_amount").val();  
		var invoice=$("#supplier_invoice").val();    
		var supplier=$("#s_name").val();   
		var description=$("#s_pay_for").val();  
		//if(!property){ $("#status_message").html("<font color='red'> Please select at least 1 property  </font>");return false;} else{ $("#status_message").html("");}
		if(!supplier){ $("#status_message").html("<font color='red'> Please enter at least 1 Supplier  </font>");return false;}else{ $("#status_message").html("");}
		if(!invoice){ $("#status_message").html("<font color='red'> Please enter Invoice No  </font>");return false;} else{ $("#status_message").html("");}
		if(!amount){ $("#status_message").html("<font color='red'> Enter Amount  </font>");return false;}else{ $("#status_message").html("");}
		if(!pay_mode){ $("#status_message").html("<font color='red'> Please select payment mode </font>");return false;}else{ $("#status_message").html("");}
		if(!pay_mode_code & pay_mode !="Cash"){ $("#status_message").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;} else{ $("#status_message").html("");}
		  
    $("#status_message").html("<font color='green' size='3'>Saving data...please wait</font>");
	$.ajax(
	{
		url:"<?=base_url();?>rent/save_expenses/",
		type:"POST",
			"data":{
				'edit_id':edit_id,
				'from':supplier,
				'property':property,
				'invoice':invoice,
				'receipt_no':pay_mode_code,
				'pay_mode':pay_mode,
				'expense_type':1,
				'pay_for':description,
				'amount':amount
			},		
		success:function(data)
		{
			 
			 var obj=JSON.parse(data);   
			 if(obj.result=="ok")
			 {   
				setTimeout(function()
				{
					$("#status_message").html("<font color='green' size='3'>"+obj.msg+"</font>"); 
					location.reload(); 
                },2500); 
				 
			 }
			 else
			 {
				 $("#status_message").html("<font color='red' size='3'>"+obj.msg+"</font>");
			 }
		}  
	  
  })
  }
 
  
  function disable_method(value)
  { 
  	 $("#error1").empty();
	 $("#error2").empty();
	 $("#error3").empty();
	 $("#error4").empty();
	 $("#error5").empty();
   
	//var value=document.getElementById("pay_mode").value;
	 if(value=="Cash")
	  {
		document.getElementById('error5').value ="";
		document.getElementById('receipt_no').value ="--";
		document.getElementById('receipt_no').disabled = true;
		document.getElementById('s_pay_method_code').disabled = true;
	  }
	  else
	  {
			document.getElementById('receipt_no').value ="";
			document.getElementById('receipt_no').disabled = false;
			document.getElementById('s_pay_method_code').disabled = false;
	  }  
  }

  function clearMsg()
  {
	 $("#error1").empty();
	 $("#error2").empty();
	 $("#error3").empty();
	 $("#error4").empty();
	 $("#error5").empty();
  }
</script> 
