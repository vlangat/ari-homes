<?php  $tenant_name="";
foreach($tenant_details->result() as $t){  $tenant_id=$t->tenant_id; $category_id=$t->property_unit_id;  } 
?>
 <?php $month=$t->month; $year=$t->year; 
 $this_year=date('Y'); $this_month=date('m');
 ?>  
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Payment </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Invoice </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
<div class="row"> 
	<div class="col-md-12"    style="background:#1bb968;padding:6px;">
		<font color="#ffffff"> Invoice </font> 
	</div>	 
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit"> 
<div class="portlet light ">  
<div class="row">
 <div class="col-md-12"> &nbsp;&nbsp;  </div>  
 <div class="col-md-12">
<?php if($year ==$this_year && $month==$this_month){?> 
<div class="col-md-2"> 
		<a onclick="add_item()"  class="btn green"> <i class="fa fa-plus"></i> Add Items  &nbsp; </a>
</div> 	 
 
<div class="col-md-2">   
	<a onclick="edit_remove_item()"  class="btn red">   Modify Items  &nbsp;  </a>
	&nbsp;&nbsp; 
</div> 			
 <?php } ?>
<div class="col-md-2"> 
	<a  href="javascript:;" class="btn grey"  id="btn_receipt"> &nbsp;&nbsp;&nbsp; <i class="fa fa-print" style="font-size:18px;color:#006699"></i><b>  Print  </b>&nbsp;&nbsp;&nbsp;</a> 
</div>
<div class="col-md-2"> 
	<a href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  
</div>
</div>
<div class="col-md-8"> 
   <div class="portlet light">  
		<div class="portlet-body" style="font-size:14px;width:100%;padding:5px">
		<?php 	
			$logo=""; $tenant_company_name="";$company_web=""; $invoice_no=""; $category_id=""; $priority=1; $mobile_number1=""; $slogan=""; $mobile_number2=""; $description=""; $company_name=""; $address=""; $country="";$company_email=""; $town="";$postal_code=""; $mobile_number=""; $company_location=""; 
			foreach($company_details->result() as $c)
			{
				 if($c->company_code==$this->session->userdata('company_code'))
				 { 
					$company_name=$c->company_name; $company_web=$c->company_web; $company_location=$c->location;
					$logo=$c->logo; $description=$c->description; $company_email=$c->company_email; $postal_code=$c->postal_code;
					$address=$c->address; $mobile_number=$c->mobile_number; $town=$c->town; $country=$c->country;
				    $mobile_number1=$c->mobile_one; $mobile_number2=$c->mobile_two;$slogan=$c->slogan;
				 } 
			}
		 
		if($logo ==""){ $logo="ari_company_logo.png"; }  ?> 
		<div>
		<table border="0" class="table table-striped table-hover" width="100%" cellpadding="8" cellspacing="0">
		<tr>
			<td>
				<img id="my_file" src="<?=base_url();?>media/<?=$logo?>" height="75" width="150" style="display:block;max-width:230px;max-height:95px;width: auto;height: auto;" alt="<?=strtoupper($company_name)?>">
			</td>
			<?php if($company_name !=""){ ?>
			<td>
				<h4><strong><?=$company_name?> </strong></h4>
				<h5><?=$description?></h5>  
				<h5><?=$company_location?></h5>  	 
				<h5><?=$company_web?></h5>   
				<h5>&nbsp;</h5>   
			</td> 
			<td> 
			<h5><strong> Cell:</strong>	<?=$mobile_number?></h5> 
			<h5><strong>Email:</strong>	<?=$company_email?></h5>
				<h5><?=$address."-".$postal_code.",";?></h5>
				<h5><?php if($town !=""){ echo ucfirst(strtolower($town)).", ".ucfirst(strtolower($country));}?></h5>
			<?php if($mobile_number1 !="" || $mobile_number2 !=""){?>
			<h5><strong> Tel:</strong>	<?=$mobile_number1?>,<?=$mobile_number2?></h5> 
			<?php }?>
			</td>
			<?php } ?>
		</tr>
		</table>
		</div> 
		
		<?php $p_amount=0; foreach($transactions->result() as $s)
			{ $p_amount=$p_amount+$s->amount;}
		       $diff=($invoice_amount-$p_amount);
		  
		if($diff >0){ $paid="UNPAID"; $color="red";}
		else{ $paid="PAID"; $color="green";} 
		?>
	<h4><strong> Invoice for <?=date("F", strtotime($t->date_generated)); ?> <?=$t->year?>  <font color="<?=$color?>"> <?=$paid?> </font></strong></h4>	 
	<table border="0" class="table table-striped table-hover" style="font-size:12px;padding:1px;border:1px solid lightgrey" cellpadding="0" cellspacing="0" width="100%"> 
			     <tr> 
					<td align="left">
						<font>  <strong> Invoice No: </strong><?php echo $invoice_no=$t->invoice_no; ?></font> 
					</td> 
					 <td>   &nbsp; </td> 
					<td align="right">
						<strong>   Date: </strong>  <?=$t->date_generated;?> &nbsp;
					</td>
				 </tr>
		 <tr>
		<td align="left">  	
				<p> <strong> Being invoice for </strong><br/> <?=$t->property_name;?>
				 <br/> <?php echo $t->category_name;?>
				 <br/> <?php echo "House No ".$t->house_no?> 
				</p> 
		
		</td>
		<td>   &nbsp; &nbsp; </td> 
		  <td align="right">
			<?php 
			$tenant_type=$t->tenant_type; 
			if($tenant_type=="residential")
			{  
		        $phone=$t->mobile_number; $email=$t->tenant_email;
				 $tenant_name=$t->first_name ; 
				$tenant_company_name=$t->company_name; 
			}
		 else{
				$phone=$t->company_mobile; $tenant_name=$t->company_name; $company_name="";
				$email=$t->company_email;
			 }
			?>
				<ul style="font-size:14px; list-style-type:none">
					<li><strong> Invoice To:</strong></li>  
					<li> <?=$tenant_name?> </li>
					<li> <?=$email?> </li>
					<li> <?=$phone?> </li> 
					<li> &nbsp;   </li> 
				</ul>
			 
		</td>
	</tr>	 
	<tr> <td> &nbsp;  </td><td> &nbsp;  </td><td> &nbsp;  </td>  </tr> 
	<tr  bgcolor="#DFDAD9"> 
		<td>  <strong> Date   </strong>  </td><td>   <strong>  Particulars </strong>    </td>  <td align="right">  <strong>   Amount &nbsp;&nbsp;  </strong>  </td>
	</tr>
	  <?php $i=0; $total=0;
//	if(!empty($rent_items)){
	foreach($rent_items as $row)
	{
	?>
	<tr>
	<td> <?=$t->date_generated?>  </td> 
	<td> 
	<?=$row['payment_type'];   if($row['description'] !=""){ echo " (". $row['description'].")"; }?>   
	</td> 
	<td align="right">
		<?php echo number_format($row['payment_type_value']); 
		$priority=$row['priority']; 
		$total=$total+$row['payment_type_value'];?> 
		&nbsp;&nbsp;
	 </td> 
	</tr>
	<?php 
	 
	$i++;
	//}
$priority=$priority+1;	
$total_items=$i;

}
?>
<?php 
	if($total<=0){?>
	 <tr>  <td colspan="3" align="center"> <font color="red"> No Items to pay for </font></td> </tr>
	 <?php } ?>
<tr> <td>  </td>  <td align="right"><strong>Total </strong></td> <td id="total" align="right"> <b><?=number_format($total)?> </b></td> </tr>

</tbody>
</table> 
<p>   </p>
<?php 
$amount1=0; $amount2=0; $amount3=0; $amount4=0;
foreach($aging_report as $a){
	 
			$amount1=$a['thirty_and_less'];
			$amount2=$a['more_than_thirty'];
			$amount3=$a['more_than_sixty'];
			$amount4=$a['more_than_ninety'];  
			 
}
?>
<p> &nbsp; </p>
<p> &nbsp; </p>
<table border="0" id="table_aging" class="table table-striped table-hover" style="font-size:12px;padding:1px;border:1px solid lightgrey" cellpadding="0" cellspacing="0" width="100%"> 
		<tr> 
			<th> 1-30 Days Past Due</th> <th>31-60 Days Past Due</th><th>61-90 Days Past Due </th> <th> Over 90 Days Past Due</th><th> Amount Due </th>
		</tr>
		<tr><td> <?=number_format($amount1)?> </td><td>  <?=number_format($amount2)?> </td><td>  <?=number_format($amount3)?> </td><td>  <?=number_format($amount4)?> </td><td>  <?=number_format(($amount1+$amount2+$amount3+$amount4))?> </td> </tr>		
</table> 
<p> &nbsp; </p>
<h4><b> Transactions</b></h4>
<table border="0" id="table_aging" class="table table-striped table-hover" style="font-size:12px;padding:1px;border:1px solid lightgrey" cellpadding="0" cellspacing="0" width="100%"> 
		<tr> 
			<th> Transaction Date</th> <th>Pay Mode </th> <th> Transaction ID </th><th> Amount </th></tr>
			<?php $paid_amount=0; $x=0;
			foreach($transactions->result() as $tr)
			{   
				?>
				<tr><td> <?=$tr->date_paid?> </td><td>  <?=$tr->payment_mode?> </td><td>  <?=$tr->payment_mode_code?> </td><td>  <?php echo number_format($tr->amount); $paid_amount=($paid_amount+$tr->amount);?> </td> </tr>		
			  <?php 
			  $x++; 
			  
		    }
			if($x==0){ echo '<tr><td align="center" colspan="4"> No Related Transactions Found </td></tr>';}
			?>
			<tr><td colspan="2"> </td><td> <b> Balance </b></td><td> <b><?=number_format(($total-$paid_amount))?></b></td></tr>
</table> 
<p> 
<h5><i><strong> Invoice created by: </strong><?=ucfirst(strtolower($this->session->userdata('first_name')))." " .ucfirst(strtolower($this->session->userdata('last_name')));?></i></font></h5>

</p>
 <p> <center> <i> <?=$slogan?>  </i></center></p>
</div>
 </div>
 </div>
<p>  <hr/> </p>
 
 
</div>
</div>
</div> 
</div> 
</div>
<!-- END EXAMPLE TABLE PORTLET--> 

</div>
<!-- END PAGE CONTENT INNER -->
</div> 
<!-- END PAGE CONTENT BODY --> 


  
  <div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> Venit Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
   


 <div id="sendMail" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Email Tenant Message </b></h5>
				<hr/>
					<p>
					    This Receipt will be sent as an Email to <font id="email_id"> </font>
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="send_email" class="btn green">&nbsp; Yes &nbsp; </button>
			 <button type="button" data-dismiss="modal" class="btn default">&nbsp; Cancel  &nbsp; </button> 
		</center>
	</div>
</div>
   
   
<div id="add_new" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			 
			<h5><b> Add  Invoice Items </b></h5>
			<hr/>
			<p>
			<label class="control-label">Item Name </label>
				<input   class="form-control" type="hidden" id="priority" value="<?=$priority+1?>">
				<input   class="form-control" type="text" placeholder="" id="item_name">
			</p>
			<p>
			<label class="control-label">Amount </label>
				<input   class="form-control" type="text"  onkeypress="return checkIt(event)"  value="" id="item_amount">
			</p> 
			<!--<p>
			<label class="control-label">Description </label>
				<input   class="form-control" type="text"  value="" id="description"/>
			</p> -->
			
			</div>
		</div> 
	</div> 
<div class="modal-footer"> <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_new_one"> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>

  <div id="modal_edit_remove_items" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12"> 
			<h5> <b>  Edit/Remove  Items </b> </h5>
			<hr/>
				<p>
				<table class="table table-striped table-hover table-bordered"  >
			<?php $f=0;	foreach($rent_items as $row)
					{
						if($row['is_one_time']==1){
							//$invoice_no=$row['invoice_no'];
						?>
					<tr> 
						<td> <?=$row['payment_type']?> </td>
						<td> <?=$row['payment_type_value']?> </td> 
						<td><font color="green"  onclick="editItem('<?=$row['id'];?>','<?=$row['payment_type'];?>','<?=$row['payment_type_value'];?>','<?=$row['description'];?>')"><a href="javascript:;" style="color:green"><i class="fa fa-edit"></i> Edit</font> </a> </td>
						<td><font color="red"  onclick="removeItem('<?=$row['id'];?>','<?=$row['payment_type_value'];?>','')"><a href="javascript:;" style="color:red"><i class="fa fa-remove"></i> Remove</font> </a> </td>
					</tr>
			<?php $f++;  
			}
			}
			if($f==0){ echo "<tr> <td colspan='4'><font color='red'>No items to edit/remove, you can only edit/remove items you added! </font> <td></tr>"; }
			      
			?>
				</table>
				</p> 
			</div>
		</div> 
	</div> 
	<div class="modal-footer" >  
		<center> <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button> </center>
	</div>
</div>
   
<div id="editItemModal" class="modal fade" tabindex="-1" data-width="600">
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
			<h5> <b> Edit Invoice Item details </b> </h5>
			<p>
				<label class="control-label"> Item </label>
				<input   class="form-control"  type="text"    id="edit_item"> 
				<input   class="form-control"  type="hidden"  id="edit_id">   
			</p>
			<p>
				<label class="control-label"> Amount </label>
				<input   class="form-control" required="required" type="text" id="edit_amount" onkeypress="return checkIt(event)" >
				<input   class="form-control" type="hidden" id="initial_amount"/>
				<span id="error3"> 			</span>				
			</p> 
			<p>
				<label class="control-label"> Description </label>
				<input   class="form-control" required="required" type="text" id="edit_description">
				<span id="error3"> 			</span>				
			</p> 
			<!-- <p>
				<label class="control-label"> Priority </label> -->
				<input type="hidden"  value="1" id="edit_priority" class="form-control" > 
				<input type="hidden"  value="" id="edit_invoice_no" class="form-control" > 
			<!--</p> -->
			</div>
		</div>
		 <p id="error_edit" style="float:left">  </p><br/><br/>
	</div>
	
<div class="modal-footer" >
	<button type="submit" class="btn green" id="done_edit" > Save </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
</div>
</div>
	 
<!-- END CONTAINER -->
<script language="javascript">
 
 $("#btn_receipt").click( function () {
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=1000');
            printWindow.document.write('<html><head><title>  Invoice </title>');
            printWindow.document.write('<style>th, td { border-bottom: 1px solid #ddd;} #table_aging th,#table_aging td{border:1px solid #bbb;}</style> </head><body style="border:1px solid #ddd; margin-left: auto;margin-right: auto; width:80%">');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
		 
var email="<?=$email?>";  
var fname="<?=$tenant_name?>";  
var from="<?=$company_name?>"; 
     
		
 $("#btn_email").click(function ()
		{ 
		   if(!email){ 
		   alert('Email address is empty! Please add tenant email first');
		   return false;
		   }
		   $("#email_id").html(fname+" <u> "+email+" </u>");
		   document.getElementById('send_email').disabled = false;
			$("#sendMail").modal('show');
		});
		
$("#add_new_one").click(function(){ 
var name=$("#item_name").val();
var amount=$("#item_amount").val();
var description=$("#description").val();
var vat=$("#vat").val();
var tenant_id="<?=$tenant_id?>";
var checkedValue = $('#vat:checked').val();
var category_id = "<?=$category_id?>"; 
var invoice_no = "<?=$invoice_no?>";  
var is_created = "<?=$is_created?>"; 
 
var priority = $('#priority').val();
vat=checkedValue;
 
if(!name){$("#status_message").html("<font color='red'> Item Name Required </font>"); $("#item_name").focus(); return false;}
if(!amount || parseInt(amount) <= 0 ){$("#status_message").html("<font color='red'> Enter the Amount </font>"); $("#item_amount").focus(); return false;}
//if(!description){$("#status_message").html("<font color='red'> Description Required </font>"); $("#description").focus(); return false;}

$.ajax(
  {
		//url:"<?=base_url();?>rent/new_paymentItem",
		url:"<?=base_url();?>invoice/new_paymentItem",
		type:"POST",  
		data:{
				'name':name,
				'amount':amount,
				//'description':description,
				'tenant_id':tenant_id,
				'invoice_no':invoice_no,
				'category_id':category_id,
				'priority':priority, 
				'is_created':is_created 
			},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#status_message").html("<font color='green'> Item added successfully </font>");
				  setTimeout(function(){
								$('#add_new').modal('hide');
                               $("#status_message").empty();
                      }, 2000); 
					  document.location.reload(true);
			 }
			 else{
				 $("#status_message").html("<font color='red'> "+obj.msg+"</font>");
			 }

		}			 
  })
 
 });	
		
   $("#send_email").click( function () {
		document.getElementById('send_email').disabled = true; 	   
		var divContents = $(".portlet-body").html();   
       
		$("#error").html("<font color='#006699'> Sending invoice...please wait</font>");
		//$("#success").modal('show'); 
		 
		$.ajax({
			//'url':"<?=base_url();?>payment/sendRentStatement",
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			 async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Invoice", 
				'message':"Attached is your Invoice ", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
					$("#error").html("<font color='green'> Invoice sent successfully</font>");
					//$("#success").modal('show');
					setTimeout(function()
						{ 
							$("#error").empty();
							$("#sendMail").modal('hide');
						}, 2500);
				}
				else
				{  
					$("#error").html("<font color='red'> Invoice was not sent to <u>"+email+"</u></font>");
					document.getElementById('send_email').disabled = false;
				}
				
			}
			
 })
 
});


$(function () {
  
$.fn.vAlign = function(){
    return this.each(function(i){
    var ah = $(this).height();
    var ph = $(this).parent().height();
    var mh = Math.ceil((ph-ah) / 2);
    $(this).css('margin-top', mh);
    });
};

$('#my_file').vAlign();
		});
		
function add_item()
{  
	$("#add_new").modal('show');
}

function edit_remove_item()
{  
	$("#modal_edit_remove_items").modal('show');
}

	
function removeItem(id,amount,table)
{  
	var tenant_id="<?=$tenant_id?>"; 
	var invoice="<?=$invoice_no?>"; 
	if(!id || id==""){alert("You cannot remove the selected Item!"); return false;}
		var c=confirm("Remove the selected Item?");
		if(c==true){ $.ajax({
				 //  url:"<?=base_url();?>rent/removePaymentItem/"+table,
				   url:"<?=base_url();?>invoice/removePaymentItem/"+table,
				   type:"POST", 
				   data:{'id':id,'tenant_id':tenant_id,'amount':amount,'invoice_no':invoice},
				   async:false, 
				   success:function(data)
				   {   
					   var obj=JSON.parse(data);
					   if(obj.result=="ok")
					   {  		 
							  document.location.reload(true);	 
					   } else{ return false;}
				   } 
				});
			}

}
	
	
function editItem(id,payment_type,payment_type_value,description)
 { 
	 $("#edit_id").val(id); 
	 $("#edit_item").val(payment_type); 
	 $("#initial_amount").val(payment_type_value); 
	 $("#edit_amount").val(payment_type_value); 
	 $("#edit_description").val(description);     
	 $("#editItemModal").modal('show');
	 
 }
 
 $("#done_edit").click(function(){ 
	
	var id=$("#edit_id").val();  
	var item=$("#edit_item").val();  
	var invoice_no="<?=$invoice_no?>";//$("#invoice_no").val();  
	var amount=$("#edit_amount").val(); 
	var initial_amount=$("#initial_amount").val(); 
	var description=$("#edit_description").val();    
	if(!item){ $("#status_msg").html("<font color='red'>Input invoice item name</font>"); $("#item").focus(); return false;}	
	if(!amount){ $("#pricing_value").focus(); return false;}	
	if(amount<=0){ $("#status_msg").html("<font color='red'>Item amount must be greater than 0</font>"); $("#pricing_value").focus(); return false;}	 
	 
	$.ajax({
		url:"<?=base_url();?>invoice/update_invoice_item",
		type:"POST",
		data:
		{  
			'id':id,
			'item':item,
			'amount':amount, 
			'initial_amount':initial_amount, 
			'invoice_no':invoice_no,
			'description':description
		},
		success:function(data)
		{  
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				$("#error_edit").html("<font color='green'> Item updated successfully</font>"); 
				//show_pricing_details(invoice_no); 
				 setTimeout(function(){
							 
                      }, 4500); 
					  document.location.reload(true);
			}
			else
			{
				$("#error_edit").html("<font color='red'> "+obj.msg+"</font>");  
			}  
		}
	})
	
  });
  
</script>
