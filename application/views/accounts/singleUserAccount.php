 <?php foreach($data->result() as $row){ } ?>
  <?php 
  $company_name="";$company_logo="ari_company_logo.png"; $company_email="";   $postal_address=""; $zip_code=""; $company_mobile_number="";  $mobile_number1="";  $mobile_number2=""; $c_country=""; $city_town="";
  $company_web="";
  
	$company_description=""; $slogan="";
   foreach($company->result() as $companyData) 
  {
	  $c_country=$companyData->country; $company_name=$companyData->company_name; 
	  $city_town=$companyData->town;$company_logo=$companyData->logo;  $postal_address=$companyData->address; $zip_code=$companyData->zip_code;
	  $company_email=$companyData->company_email; $company_web=$companyData->company_web; $company_mobile_number=$companyData->mobile_number;
   $mobile_number1=$companyData->mobile_one;  $mobile_number2=$companyData->mobile_two;   $company_description=$companyData->description;
 $slogan=$companyData->slogan; $location=$companyData->location;

 } ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">  
	<!-- BEGIN PAGE BREADCRUMBS -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="<?=base_url();?>property">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="#"> Account</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Profile</span>
		</li>
	</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		 <!-- PORTLET MAIN -->
		<?php //foreach($profile as $rows){}?>
		<div class="portlet light profile-sidebar-portlet ">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				<img src="<?=base_url();?>media/<?=$company_logo?>" class="img-responsive" alt="profile photo" >

				</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">
			 <?=ucfirst(strtolower($row->first_name))?> <?=ucfirst(strtolower($row->last_name))?> 
			   
		</div> 
				<div class="profile-usertitle-job"><font size="1">   </font></div>
			</div>
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<!--<li>
						<a data-toggle="modal" href="#responsive_2">
							Add Users
					</a>       
					</li>-->
					 
				</ul>
			</div>
			<!-- END MENU -->
		<!-- END PORTLET MAIN -->
	</div>
	</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
<div class="col-md-12">
<div class="portlet light ">
<div class="portlet-title tabbable-line">
<div class="caption caption-md">
	<i class="icon-globe theme-font hide"></i>
	<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
</div>
<ul class="nav nav-tabs">
	<li class="active">
		<a href="#tab_1_1" data-toggle="tab" onclick="clear_msg()">Personal Info</a>
	</li>
	<?php //if($this->session->userdata('user_type')=="2") {
	?>
	<li>
		<a href="#tab_1_4" data-toggle="tab" onclick="clear_msg()"> Company Details </a>
	</li> 
	<?php 
	//}
	?>
	<!--<li>
		<a href="#tab_1_2" data-toggle="tab" onclick="clear_msg()">Change Pic</a>
	</li>-->
	<li>
		<a href="#tab_1_3" data-toggle="tab" onclick="clear_msg()">Change Password</a>
	</li> 
	
</ul>
</div>
<div class="portlet-body">
<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
	<div class="tab-pane active" id="tab_1_1">
		<form role="form" action="#">
			<div class="form-group">
				<label class="control-label">First Name</label>
				<input type="text" id="first_name" value="<?=$row->first_name;?>" class="form-control" /> </div>
			<div class="form-group">
				<label class="control-label">Middle Name</label>
				<input type="text" id="middle_name" value="<?=$row->middle_name;?>" class="form-control" /> </div>
				<div class="form-group">
				<label class="control-label">Last Name</label>
				<input type="text" id="last_name" value="<?=$row->last_name;?>" class="form-control" /> </div>
			<div class="form-group">
				<label class="control-label"> Mobile Number </label>
				<input type="text"  id="phone" value="<?=$row->mobile_number?>"  maxlength="10" class="form-control" /> </div>
			<div class="form-group">
<label class="control-label">Country</label>
<select class="form-control" name="country" id="country"> 
						<option><?php echo $row->country;?> </option> 
                        <option value="AF">Afghanistan</option>
                        <option value="AL">Albania</option>
                        <option value="DZ">Algeria</option>
                        <option value="AS">American Samoa</option>
                        <option value="AD">Andorra</option>
                        <option value="AO">Angola</option>
                        <option value="AI">Anguilla</option>
                        <option value="AR">Argentina</option>
                        <option value="AM">Armenia</option>
                        <option value="AW">Aruba</option>
                        <option value="AU">Australia</option>
                        <option value="AT">Austria</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="BS">Bahamas</option>
                        <option value="BH">Bahrain</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BB">Barbados</option>
                        <option value="BY">Belarus</option>
                        <option value="BE">Belgium</option>
                        <option value="BZ">Belize</option>
                        <option value="BJ">Benin</option>
                        <option value="BM">Bermuda</option>
                        <option value="BT">Bhutan</option>
                        <option value="BO">Bolivia</option>
                        <option value="BA">Bosnia and Herzegowina</option>
                        <option value="BW">Botswana</option>
                        <option value="BV">Bouvet Island</option>
                        <option value="BR">Brazil</option>
                        <option value="IO">British Indian Ocean Territory</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BI">Burundi</option>
                        <option value="KH">Cambodia</option>
                        <option value="CM">Cameroon</option>
                        <option value="CA">Canada</option>
                        <option value="CV">Cape Verde</option>
                        <option value="KY">Cayman Islands</option>
                        <option value="CF">Central African Republic</option>
                        <option value="TD">Chad</option>
                        <option value="CL">Chile</option>
                        <option value="CN">China</option>
                        <option value="CX">Christmas Island</option>
                        <option value="CC">Cocos (Keeling) Islands</option>
                        <option value="CO">Colombia</option>
                        <option value="KM">Comoros</option>
                        <option value="CG">Congo</option>
                        <option value="CD">Congo, the Democratic Republic of the</option>
                        <option value="CK">Cook Islands</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CI">Cote d'Ivoire</option>
                        <option value="HR">Croatia (Hrvatska)</option>
                        <option value="CU">Cuba</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="DK">Denmark</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="EC">Ecuador</option>
                        <option value="EG">Egypt</option>
                        <option value="SV">El Salvador</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="ER">Eritrea</option>
                        <option value="EE">Estonia</option>
                        <option value="ET">Ethiopia</option>
                        <option value="FK">Falkland Islands (Malvinas)</option>
                        <option value="FO">Faroe Islands</option>
                        <option value="FJ">Fiji</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GF">French Guiana</option>
                        <option value="PF">French Polynesia</option>
                        <option value="TF">French Southern Territories</option>
                        <option value="GA">Gabon</option>
                        <option value="GM">Gambia</option>
                        <option value="GE">Georgia</option>
                        <option value="DE">Germany</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GR">Greece</option>
                        <option value="GL">Greenland</option>
                        <option value="GD">Grenada</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GU">Guam</option>
                        <option value="GT">Guatemala</option>
                        <option value="GN">Guinea</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="HT">Haiti</option>
                        <option value="HM">Heard and Mc Donald Islands</option>
                        <option value="VA">Holy See (Vatican City State)</option>
                        <option value="HN">Honduras</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HU">Hungary</option>
                        <option value="IS">Iceland</option>
                        <option value="IN">India</option>
                        <option value="ID">Indonesia</option>
                        <option value="IR">Iran (Islamic Republic of)</option>
                        <option value="IQ">Iraq</option>
                        <option value="IE">Ireland</option>
                        <option value="IL">Israel</option>
                        <option value="IT">Italy</option>
                        <option value="JM">Jamaica</option>
                        <option value="JP">Japan</option>
                        <option value="JO">Jordan</option>
                        <option value="KZ">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="KI">Kiribati</option>
                        <option value="KP">Korea, Democratic People's Republic of</option>
                        <option value="KR">Korea, Republic of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LV">Latvia</option>
                        <option value="LB">Lebanon</option>
                        <option value="LS">Lesotho</option>
                        <option value="LR">Liberia</option>
                        <option value="LY">Libyan Arab Jamahiriya</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="MO">Macau</option>
                        <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                        <option value="MG">Madagascar</option>
                        <option value="MW">Malawi</option>
                        <option value="MY">Malaysia</option>
                        <option value="MV">Maldives</option>
                        <option value="ML">Mali</option>
                        <option value="MT">Malta</option>
                        <option value="MH">Marshall Islands</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MU">Mauritius</option>
                        <option value="YT">Mayotte</option>
                        <option value="MX">Mexico</option>
                        <option value="FM">Micronesia, Federated States of</option>
                        <option value="MD">Moldova, Republic of</option>
                        <option value="MC">Monaco</option>
                        <option value="MN">Mongolia</option>
                        <option value="MS">Montserrat</option>
                        <option value="MA">Morocco</option>
                        <option value="MZ">Mozambique</option>
                        <option value="MM">Myanmar</option>
                        <option value="NA">Namibia</option>
                        <option value="NR">Nauru</option>
                        <option value="NP">Nepal</option>
                        <option value="NL">Netherlands</option>
                        <option value="AN">Netherlands Antilles</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NZ">New Zealand</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NE">Niger</option>
                        <option value="NG">Nigeria</option>
                        <option value="NU">Niue</option>
                        <option value="NF">Norfolk Island</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="NO">Norway</option>
                        <option value="OM">Oman</option>
                        <option value="PK">Pakistan</option>
                        <option value="PW">Palau</option>
                        <option value="PA">Panama</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PY">Paraguay</option>
                        <option value="PE">Peru</option>
                        <option value="PH">Philippines</option>
                        <option value="PN">Pitcairn</option>
                        <option value="PL">Poland</option>
                        <option value="PT">Portugal</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="QA">Qatar</option>
                        <option value="RE">Reunion</option>
                        <option value="RO">Romania</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="KN">Saint Kitts and Nevis</option>
                        <option value="LC">Saint LUCIA</option>
                        <option value="VC">Saint Vincent and the Grenadines</option>
                        <option value="WS">Samoa</option>
                        <option value="SM">San Marino</option>
                        <option value="ST">Sao Tome and Principe</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SN">Senegal</option>
                        <option value="SC">Seychelles</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SG">Singapore</option>
                        <option value="SK">Slovakia (Slovak Republic)</option>
                        <option value="SI">Slovenia</option>
                        <option value="SB">Solomon Islands</option>
                        <option value="SO">Somalia</option>
                        <option value="ZA">South Africa</option>
                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                        <option value="ES">Spain</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="SH">St. Helena</option>
                        <option value="PM">St. Pierre and Miquelon</option>
                        <option value="SD">Sudan</option>
                        <option value="SR">Suriname</option>
                        <option value="SJ">Svalbard and Jan Mayen Islands</option>
                        <option value="SZ">Swaziland</option>
                        <option value="SE">Sweden</option>
                        <option value="CH">Switzerland</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="TW">Taiwan, Province of China</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TZ">Tanzania, United Republic of</option>
                        <option value="TH">Thailand</option>
                        <option value="TG">Togo</option>
                        <option value="TK">Tokelau</option>
                        <option value="TO">Tonga</option>
                        <option value="TT">Trinidad and Tobago</option>
                        <option value="TN">Tunisia</option>
                        <option value="TR">Turkey</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TC">Turks and Caicos Islands</option>
                        <option value="TV">Tuvalu</option>
                        <option value="UG">Uganda</option>
                        <option value="UA">Ukraine</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                        <option value="UM">United States Minor Outlying Islands</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="VU">Vanuatu</option>
                        <option value="VE">Venezuela</option>
                        <option value="VN">Viet Nam</option>
                        <option value="VG">Virgin Islands (British)</option>
                        <option value="VI">Virgin Islands (U.S.)</option>
                        <option value="WF">Wallis and Futuna Islands</option>
                        <option value="EH">Western Sahara</option>
                        <option value="YE">Yemen</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
</select>
</div>
<div class="form-group">
<label class="control-label">City/Town</label>
<input type="text" class="form-control" value="<?=$row->town?>" name="town" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"   onchange="checkChars('Town Name')" id="town">  

</div>
<div class="form-group">
	<label class="control-label">Postal Address</label>
	<input type="text"  placeholder="P.O.Box 123" value="<?=$row->address?>" class="form-control"  name="postal_address" id="postal_address"onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" /> 
</div>  
<div class="form-group">
	<label class="control-label">ZIP Code </label>
	<input type="text"   maxlength="10"placeholder="" value="<?=$row->zip_code?>" class="form-control"  name="zip_code" id="zip_code"onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" /> 
</div> 
<div class="margiv-top-10">
	<a href="javascript:;" id="update_details" class="btn green"> Save Changes </a>
	<!--<a href="javascript:;" class="btn default"> Cancel </a>-->
</div>
		</form>
</div> 
	<!-- END PERSONAL INFO TAB -->
	<!-- CHANGE AVATAR TAB -->
	<div class="tab-pane" id="tab_1_2"> 
		<!--<form action="changePhoto" role="form" enctype="form-multipart" >-->
		<?php echo form_open_multipart('auth/changePhoto');?>
			<div class="form-group">
				<div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						<img src="<?=base_url();?>media/<?php echo $this->session->userdata('photo');?>" alt="" /> </div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
					<div>
						<span class="btn default btn-file">
							<span class="fileinput-new"> Select image </span>
							<span class="fileinput-exists"> Change </span>
							<input type="file" name="userfile[]"  accept="gif|jpg|png"> </span>
						<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
					</div>
				</div>
				<div class="clearfix margin-top-10">
					 </div>
			</div>
			<div class="margin-top-10">
				<button href="javascript:;" type="submit" class="btn green"> Save changes </button>
				<!--<button href="javascript:;" type="reset" class="btn red"> Cancel </button>-->
				 
			</div>
		<!--</form>-->
		<?php echo form_close();?>
</div>
<!-- END CHANGE AVATAR TAB -->
<!-- CHANGE PASSWORD TAB -->
<div class="tab-pane" id="tab_1_3">
	<form action="#">
		<div class="form-group">
			<label class="control-label">Current Password</label>
			<input type="password" id="curr_password" class="form-control" /> </div>
		<div class="form-group">
			<label class="control-label">New Password</label>
			<input type="password" id="new_pass" class="form-control" /> </div>
		<div class="form-group">
			<label class="control-label">Re-type New Password</label>
			<input type="password" id="confirm_pass" class="form-control" /> </div>
		<div class="margin-top-10">
			<a href="javascript:;" class="btn green" id="change_pass"> Change Password </a>
			<!--<a href="javascript:;" class="btn default"> Cancel </a>-->
		</div>
	</form>
</div>
<!-- END CHANGE PASSWORD TAB -->
 
 
<div class="tab-pane" id="tab_1_4">
	<!-- BEGIN EXAMPLE TABLE PORTLET-->
	<form action="<?=site_url('auth/updateCompanyDetails');?>"  enctype="multipart/form-data" method="post" onsubmit="return input_validation()">

 
<div class="portlet light portlet-fit ">
<div class="row">
<div class="col-md-4">
<div class="form-group">
		<label class="control-label">Company Name</label>
			<input type="text"   value="<?=$company_name?>" class="form-control" name="company_name" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"  onchange="checkChars('Company Name')" id="Company Name"/>
 </div>
 </div>
 <div class="col-md-4">
<div class="form-group">
	<label class="control-label"> Email Address</label>
	<input type="email"   value="<?=$company_email?>" class="form-control" name="email" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"/> 
</div>
</div> 
<div class="col-md-4">
<div class="form-group">
	<label class="control-label"> Company Website</label>
	<input type="text"   value="<?=$company_web?>" class="form-control" name="company_web" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"/> 
</div>
</div>

<div class="col-md-4"> 
<div class="form-group">
	<label class="control-label">Mobile Number 1</label>
	<input type="text" onkeypress="return checkIt(event)" maxlength="10"   value="<?=$company_mobile_number?>" class="form-control"  name="phone" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"/> 
</div>
</div>

<div class="col-md-4"> 
<div class="form-group">
	<label class="control-label">Mobile Number 2 </label>
	<input type="text" onkeypress="return checkIt(event)" maxlength="10"   value="<?=$mobile_number1?>" class="form-control"  name="mobile_number1" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"/> 
</div>
</div>
<div class="col-md-4"> 
<div class="form-group">
	<label class="control-label">Telephone No</label>
	<input type="text" onkeypress="return checkIt(event)" maxlength="10"   value="<?=$mobile_number2?>" class="form-control"  name="mobile_number2" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"/> 
</div>
</div>


<div class="col-md-4">
 <div class="form-group">
	<label class="control-label">Postal Address</label>
	<input type="text"    placeholder="P.O.Box 123" value="<?=$postal_address?>" class="form-control"  name="postal_address"onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" /> 
</div> 
</div> 
<div class="col-md-4">
<div class="form-group">
	<label class="control-label">Postal Code</label>
	<input type="text"  placeholder="" value="<?=$companyData->postal_code?>" class="form-control"  name="postal_code" id="postal_code"onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" /> 
</div> 
</div> 
<div class="col-md-4">
<div class="form-group">
	<label class="control-label">City/Town</label>
	<input type="text" class="form-control" value="<?=$city_town?>" name="town" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"   onchange="checkChars('Town Name')" id="Town Name">  
</div>
</div>


<div class="col-md-4">
<div class="form-group">
<label class="control-label">Country</label>
<select class="form-control" name="country"> 
	<option> <?=$c_country?> </option>
	<option value="">Country</option>
                        <option value="AF">Afghanistan</option>
                        <option value="AL">Albania</option>
                        <option value="DZ">Algeria</option>
                        <option value="AS">American Samoa</option>
                        <option value="AD">Andorra</option>
                        <option value="AO">Angola</option>
                        <option value="AI">Anguilla</option>
                        <option value="AR">Argentina</option>
                        <option value="AM">Armenia</option>
                        <option value="AW">Aruba</option>
                        <option value="AU">Australia</option>
                        <option value="AT">Austria</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="BS">Bahamas</option>
                        <option value="BH">Bahrain</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BB">Barbados</option>
                        <option value="BY">Belarus</option>
                        <option value="BE">Belgium</option>
                        <option value="BZ">Belize</option>
                        <option value="BJ">Benin</option>
                        <option value="BM">Bermuda</option>
                        <option value="BT">Bhutan</option>
                        <option value="BO">Bolivia</option>
                        <option value="BA">Bosnia and Herzegowina</option>
                        <option value="BW">Botswana</option>
                        <option value="BV">Bouvet Island</option>
                        <option value="BR">Brazil</option>
                        <option value="IO">British Indian Ocean Territory</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BI">Burundi</option>
                        <option value="KH">Cambodia</option>
                        <option value="CM">Cameroon</option>
                        <option value="CA">Canada</option>
                        <option value="CV">Cape Verde</option>
                        <option value="KY">Cayman Islands</option>
                        <option value="CF">Central African Republic</option>
                        <option value="TD">Chad</option>
                        <option value="CL">Chile</option>
                        <option value="CN">China</option>
                        <option value="CX">Christmas Island</option>
                        <option value="CC">Cocos (Keeling) Islands</option>
                        <option value="CO">Colombia</option>
                        <option value="KM">Comoros</option>
                        <option value="CG">Congo</option>
                        <option value="CD">Congo, the Democratic Republic of the</option>
                        <option value="CK">Cook Islands</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CI">Cote d'Ivoire</option>
                        <option value="HR">Croatia (Hrvatska)</option>
                        <option value="CU">Cuba</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="DK">Denmark</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="EC">Ecuador</option>
                        <option value="EG">Egypt</option>
                        <option value="SV">El Salvador</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="ER">Eritrea</option>
                        <option value="EE">Estonia</option>
                        <option value="ET">Ethiopia</option>
                        <option value="FK">Falkland Islands (Malvinas)</option>
                        <option value="FO">Faroe Islands</option>
                        <option value="FJ">Fiji</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GF">French Guiana</option>
                        <option value="PF">French Polynesia</option>
                        <option value="TF">French Southern Territories</option>
                        <option value="GA">Gabon</option>
                        <option value="GM">Gambia</option>
                        <option value="GE">Georgia</option>
                        <option value="DE">Germany</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GR">Greece</option>
                        <option value="GL">Greenland</option>
                        <option value="GD">Grenada</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GU">Guam</option>
                        <option value="GT">Guatemala</option>
                        <option value="GN">Guinea</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="HT">Haiti</option>
                        <option value="HM">Heard and Mc Donald Islands</option>
                        <option value="VA">Holy See (Vatican City State)</option>
                        <option value="HN">Honduras</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HU">Hungary</option>
                        <option value="IS">Iceland</option>
                        <option value="IN">India</option>
                        <option value="ID">Indonesia</option>
                        <option value="IR">Iran (Islamic Republic of)</option>
                        <option value="IQ">Iraq</option>
                        <option value="IE">Ireland</option>
                        <option value="IL">Israel</option>
                        <option value="IT">Italy</option>
                        <option value="JM">Jamaica</option>
                        <option value="JP">Japan</option>
                        <option value="JO">Jordan</option>
                        <option value="KZ">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="KI">Kiribati</option>
                        <option value="KP">Korea, Democratic People's Republic of</option>
                        <option value="KR">Korea, Republic of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LV">Latvia</option>
                        <option value="LB">Lebanon</option>
                        <option value="LS">Lesotho</option>
                        <option value="LR">Liberia</option>
                        <option value="LY">Libyan Arab Jamahiriya</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="MO">Macau</option>
                        <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                        <option value="MG">Madagascar</option>
                        <option value="MW">Malawi</option>
                        <option value="MY">Malaysia</option>
                        <option value="MV">Maldives</option>
                        <option value="ML">Mali</option>
                        <option value="MT">Malta</option>
                        <option value="MH">Marshall Islands</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MU">Mauritius</option>
                        <option value="YT">Mayotte</option>
                        <option value="MX">Mexico</option>
                        <option value="FM">Micronesia, Federated States of</option>
                        <option value="MD">Moldova, Republic of</option>
                        <option value="MC">Monaco</option>
                        <option value="MN">Mongolia</option>
                        <option value="MS">Montserrat</option>
                        <option value="MA">Morocco</option>
                        <option value="MZ">Mozambique</option>
                        <option value="MM">Myanmar</option>
                        <option value="NA">Namibia</option>
                        <option value="NR">Nauru</option>
                        <option value="NP">Nepal</option>
                        <option value="NL">Netherlands</option>
                        <option value="AN">Netherlands Antilles</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NZ">New Zealand</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NE">Niger</option>
                        <option value="NG">Nigeria</option>
                        <option value="NU">Niue</option>
                        <option value="NF">Norfolk Island</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="NO">Norway</option>
                        <option value="OM">Oman</option>
                        <option value="PK">Pakistan</option>
                        <option value="PW">Palau</option>
                        <option value="PA">Panama</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PY">Paraguay</option>
                        <option value="PE">Peru</option>
                        <option value="PH">Philippines</option>
                        <option value="PN">Pitcairn</option>
                        <option value="PL">Poland</option>
                        <option value="PT">Portugal</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="QA">Qatar</option>
                        <option value="RE">Reunion</option>
                        <option value="RO">Romania</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="KN">Saint Kitts and Nevis</option>
                        <option value="LC">Saint LUCIA</option>
                        <option value="VC">Saint Vincent and the Grenadines</option>
                        <option value="WS">Samoa</option>
                        <option value="SM">San Marino</option>
                        <option value="ST">Sao Tome and Principe</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SN">Senegal</option>
                        <option value="SC">Seychelles</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SG">Singapore</option>
                        <option value="SK">Slovakia (Slovak Republic)</option>
                        <option value="SI">Slovenia</option>
                        <option value="SB">Solomon Islands</option>
                        <option value="SO">Somalia</option>
                        <option value="ZA">South Africa</option>
                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                        <option value="ES">Spain</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="SH">St. Helena</option>
                        <option value="PM">St. Pierre and Miquelon</option>
                        <option value="SD">Sudan</option>
                        <option value="SR">Suriname</option>
                        <option value="SJ">Svalbard and Jan Mayen Islands</option>
                        <option value="SZ">Swaziland</option>
                        <option value="SE">Sweden</option>
                        <option value="CH">Switzerland</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="TW">Taiwan, Province of China</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TZ">Tanzania, United Republic of</option>
                        <option value="TH">Thailand</option>
                        <option value="TG">Togo</option>
                        <option value="TK">Tokelau</option>
                        <option value="TO">Tonga</option>
                        <option value="TT">Trinidad and Tobago</option>
                        <option value="TN">Tunisia</option>
                        <option value="TR">Turkey</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TC">Turks and Caicos Islands</option>
                        <option value="TV">Tuvalu</option>
                        <option value="UG">Uganda</option>
                        <option value="UA">Ukraine</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                        <option value="UM">United States Minor Outlying Islands</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="VU">Vanuatu</option>
                        <option value="VE">Venezuela</option>
                        <option value="VN">Viet Nam</option>
                        <option value="VG">Virgin Islands (British)</option>
                        <option value="VI">Virgin Islands (U.S.)</option>
                        <option value="WF">Wallis and Futuna Islands</option>
                        <option value="EH">Western Sahara</option>
                        <option value="YE">Yemen</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
</select>
</div>
</div>

<div class="col-md-8">
<div class="form-group">
	<label class="control-label"> Physical Location</label>
	<input type="text" class="form-control" name="location" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" value="<?=$location?>"/> 
</div>
</div>
<div class="col-md-12">
<div class="form-group">
	<label class="control-label"> Company Slogan </label>
	<input type="text" class="form-control" name="slogan" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" value="<?=$slogan?>"/> 
</div>
</div>

 <!--<div class="col-md-6">
<div class="form-group">
	<label class="control-label">ZIP Code </label>
	<input type="text"   maxlength="10"placeholder="" value="" class="form-control"  name="zip_code"onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" /> 
</div> 
</div> -->

<div class="col-md-12">
<div class="form-group">
	<label class="control-label">Description of your Services</label>
	<textarea rows="2" name="description" class="form-control"><?=$company_description?></textarea>
</div>
</div>
<div class="col-md-12">
<div class="form-group">
<label class="control-label"> Logo</label>
	<div class="fileinput-new thumbnail"  style="width: 100px; height: 80px;">
		  
		<img  src="<?=base_url();?>media/<?=$company_logo?>"  alt="Logo" /> 
	 
	</div>
	<div>
			<input type="file"  name="photo" id="company_logo" value=""> 
			<input type="hidden"  id="no_of_files" name="no_of_files" value=""> 
	</div>
 
</div>
</div>
<div class="col-md-6">
	<div class="margiv-top-10">
		<input type="submit" class="btn green" value="Save Changes"> 
	</div>
</div>
</form> 
 
</div>
</div>
	
			
			
</div>
</div> <p id="error">   </p>
</div>       
<p>  &nbsp; </p>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->

</div>

</div>
   
</div> 
</div>  
    
	
	 <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;"><i class="fa fa-exclamation-circle" style="font-size:24px"> </i> &nbsp; Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="success_msg">
				   <?php if($this->session->flashdata('temp')){ echo $this->session->flashdata('temp');}else{ echo "No Changes made";}  ?>
				   
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
<!-- END CONTENT -->
 
<!-- END CONTAINER --> 
 
<script type="text/javascript">
 
function input_validation()
{
	var x=$('#company_logo').val();
	var no_of_files=0;
     if(!x||x==""){ }else{ no_of_files=1; }	
	$("#no_of_files").val(no_of_files); 
	var email=$("#email").val();
	var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#error").html("<font color='red'> Email provided is not a valid e-mail address");   $("#email").foucus(); return false;}
		return false;
}

$(function()
{  

var temp_msg="<?php echo $this->session->flashdata('temp');?>";  
if(temp_msg==""){   } else { $("#data_saving_success").modal('show'); }
	$("#change_pass").click(function()
	{  
	 
		var curr_pass=$("#curr_password").val();
		var new_pass=$("#new_pass").val();
		var confirm_pass=$("#confirm_pass").val();  	
	   if(curr_pass||new_pass||confirm_pass){ } else{$("#error").html("<font color='red'> Empty fields not required </font>"); return false;}
		if(new_pass == confirm_pass){ }else{  $("#error").empty();$("#error").html("<font color='red'> Password does not match. Try again </font>");  $("#pass2").val('');$("#pass2").focus(); return false;}
		 
		  $.ajax({
		 url:"<?=base_url();?>auth/updatePassword/",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'curr_password':curr_pass,
		   'new_pass':new_pass 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){
				 $("#confirm_pass").val('');$("#new_pass").val(''); $("#curr_password").val('');
				 $("#success_msg").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'> Password changed successfully  </font>"); 
                $("#data_saving_success").modal('show'); 	 
			 }
			 else{
				 $("#success_msg").html("<font color='red'>Password not changed. Check if current password is correct</font>"); 
			 $("#data_saving_success").modal('show'); 
			 }
		 }
		 
		  }
		 )	  
});

$("#update_details").click(function()
	{  
	    
		var fname=$("#first_name").val();
		var mname=$("#middle_name").val();
		var lname=$("#last_name").val();   	  	
		var phone=$("#phone").val();  	  	
		var town=$("#town").val();  	  	
		var country=$("#country").val();  	  	
		var zip_code=$("#zip_code").val();  	  	
		var postal_address=$("#postal_address").val();  	  	
		if(fname){ } else{$("#error").html("<font color='red'> First Name required </font>"); $("#first_name").focus(); return false;}
		if(lname){ } else{$("#error").html("<font color='red'>Last Name required </font>"); $("#last_name").focus(); return false;}
		if(phone){ } else{$("#error").html("<font color='red'> Phone Number required </font>"); $("#phone").focus(); return false;}
		 $("#error").html("<font color='blue'> Saving please wait...</font>");
		$.ajax({
		 url:"<?=base_url();?>auth/updateProfile",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'mname':mname, 
		   'lname':lname, 
		   'phone':phone, 
		   'town':town, 
		   'country':country, 
		   'address':postal_address, 
		   'zip_code':zip_code  
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){ 
				 $("#success_msg").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>Profile details updated successfully  </font>"); 
                //getAgents();
				$("#error").empty();
                $("#data_saving_success").modal('show'); 				
			 }
		else{
			$("#success_msg").html("<font color='red'> Changes not saved. You have made no changes to save </font>"); 
			$("#error").empty();
			$("#data_saving_success").modal('show'); 
			}
		 }
		 
		  }
		 )	 
});

});

function clear_msg()
{
	$("#error").empty();
}
</script>