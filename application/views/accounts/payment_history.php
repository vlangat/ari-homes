<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="index.html"> Rent </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Paid</span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<div class="row">
<div class="col-md-12">
	<div class="portlet light portlet-fit ">
	<div class="portlet-body">
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr> 
							<th> Date </th>
							<th> Amount </th> 
							<th> Payment mode </th> 
						</tr>
					</thead>
					<tbody>
					<?php foreach($pay_history->result() as $row){?>
						<tr> <td> <?=$row->date_paid?></td> <td> <?=$row->amount?> </td>  <td><?=$row->payment_mode?></td> </tr> 
					<?php } ?>						
					</tbody>
				</table>
				<?php foreach($pay_history->result() as $row){  $no=$row->id;}?>
				<!--<a  href="javascript:;" class="btn grey" onclick="payment_history('<?//=$no?>')" > <b> <i class="fa fa-print"></i> Preview/Print </b> </a> 
			--></div>
		</div>
	</div>
</div>
	</div>  
	</div>  
	
	<!-- responsive -->
    <div id="responsive" class="modal fade" tabindex="-1" data-width="600" aria-hidden="true">
         
            <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
           <h4 class="modal-title">	<b> Rent Payment History </b></h4> 
	     </div>
		<div class="modal-body" >
		<div id="print">
		<div class="col-md-12"> 
		<div class="col-md-6">
			<p id="tenant_name">    </p>
			<p id="phone">   </p>
			<p id="email">   </p>
		</div>
		<div class="col-md-6">
			<p id="property_name">    </p>
			 <p> Floor: <font id="floor">   </font> Room: <font id="room"> </font> </p> 
			<p id="category">   </p>
		</div>
		
		</div>
		<div class="portlet light portlet-fit ">
		<div class="portlet-body">
			<div  >
				<table width="100%" border="1" style="text-align:center">
					<thead>
						<tr>
							<th> Date </th>
							<th> Amount </th>
							<th> Payment mode </th> 
						</tr>
					</thead>
					<tbody id="payment_details">
						 
						 
						 
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</div>
<div class="modal-footer" >
<!--<button type="button" class="btn red" onclick="window.print();"> Print </button>
<button type="button" class="btn green"> Email </button>-->
<a  href="javascript:;" class="btn grey" id="btn_print" > <b> <i class="fa fa-print"></i> Print </b> </a>
<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
</div>
  
</div>
</div>   

	<script language="javascript">
 
 $("#btn_print").click( function () {
            var divContents = $("#print").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Payment History </title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
		
 function payment_history(id){ 
	  
		$.ajax({
		"url":"<?=base_url();?>rent/payment_history1/"+id,
        "type":"post",
		"success":function(data)
			{
			var obj = JSON.parse(data);  
			var data = obj.data;
			var content="";
            for(var i=0; i<data.length; i++)
			{
				var property = data[i]; 
				$("#tenant_name").html((property['from']).toUpperCase());  
				$("#date").html(property['date_paid']); 
				content = content+ ("<tr> <td> "+property['date_paid']+" </td> <td> "+property['amount']+"</td> <td> "+property['payment_mode']+" </td> </tr>");
			}
				$("#payment_details").empty();
				$("#payment_details").append(content);
				 getData(id);
			if(id==""){return false;}
			else{
					$("#responsive").modal('show');
				}
		} 
	}); 	
} 	

function getData(id){	
$.ajax({
		"url":"<?=base_url();?>tenants/getData/"+id,
        "type":"post",
		"success":function(data)
		{
			 
			var obj = JSON.parse(data); 
			 
			var data = obj.data;
			var content="";
            for(var i=0; i<data.length; i++)
			{
                    var property = data[i];  
					$("#category").html((property['unit_category']).toUpperCase());
					$("#email").html(property['email']);
					$("#phone").html(property['mobile_no']); 
					$("#house").html(property['house_no']); 
					$("#floor").html(property['floor_no']); 
					$("#room").html(property['house_no']); 
					$("#property_name").html(property['property_name'].toUpperCase()); 
			 } 
		}
	});
}
</script>