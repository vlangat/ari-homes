<?php $m=date('m');$this_month=date('F', mktime(0, 0, 0, $m, 1));?>
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#"> Rent </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>UnPaid Rent</span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value=""> <?php echo $expected_pay;?> </span>
				<small class="font-green-sharp"> </small>
			</h3>
			<small> <?php echo $this_month;?> Expected Rent </small>
		</div>
		<div class="icon">
			<i class="fa fa-money" style="color:#006699"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php $e=$expected_pay; if($expected_pay==0){$e=1;}  echo $pr=$paid/$e*100;?>%;" class="progress-bar progress-bar-success green-sharp">
				<!--<span class="sr-only">70% Occupation</span>-->
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Collection </div>
			<div class="status-number"> <?php $e=$expected_pay; if($expected_pay==0){$e=1; } echo $pr=round($paid/$e*100,0); if($pr>100){ echo '100';} ?> % </div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value=""> <?php echo $paid;?> </span>
				<small class="font-green-sharp"> </small>
			</h3>
			<small> <?php echo $this_month; ?> Rent Paid </small>
		</div>
		<div class="icon">
			<i class="fa fa-money" style="color:#006699"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php $e=$expected_pay; if($expected_pay==0){$e=1;}  echo $pr=$paid/$e*100;?>%;" class="progress-bar progress-bar-success green-sharp">
				<!--<span class="sr-only">70% Occupation</span>-->
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Collection </div>
			<div class="status-number"> <?php $e=$expected_pay; if($expected_pay==0){$e=1; } echo $pr=round($paid/$e*100,0); if($pr>100){ echo '100';} ?> % </div>
		</div>
	</div>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-red-haze">
				<span data-counter="counterup" data-value="1349"> <?php    $unpaid=$rent_not_paid; if($unpaid<0){ $pr=100; echo '0';} else{ echo $unpaid;} ?> </span>
			</h3>
			<small><?php echo $this_month; ?> Rent Unpaid</small>
		</div>
		<div class="icon">
			<i class="fa fa-money" style="color:red">     </i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php echo 100-$pr;?>%;" class="progress-bar progress-bar-success red-haze">
				<!--<span class="sr-only">30% change</span>-->
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Over due </div>
			<div class="status-number"> <?php echo round(100-$pr,0);?> %</div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-purple-soft" style="color:green">
				<span style="color:green" data-counter="counterup" data-value="276">  <?php   $overpaid=$paid-($expected_pay); if($overpaid>0){ if($expected_pay==0){$expected_pay=1;}  echo $overpaid;  $percent=round($overpaid/$expected_pay*100,0);}else{ echo '0';$percent=0;}?> </span>
			</h3>
			<small> <?php echo $this_month;?> Rent Overpaid </small>
		</div>
		<div class="icon">
			<i class="fa fa-money" style="color:green">  </i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php echo $percent; ?>%;" class="progress-bar progress-bar-success purple-soft">
				<!--<span class="sr-only">10% change </span>-->
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Prepaid </div>
			<div class="status-number"> <?php echo $percent; ?> % </div>
		</div>
	</div>
</div>
</div>
</div>
          <div class="row">
			<div class="col-md-12">
				<div class="col-md-12" style="background:#1bb968;padding:8px;">
						<font color="#ffffff"> Tenants </font>
				</div>
			
			</div>
		</div>
<div class="row">
	<div class="col-md-12">
 
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit ">
	<div class="portlet-body">
		<div class="table-toolbar">
		</div>
<table class="table table-striped table-hover table-bordered" id="table1">
<thead>
	<tr>
		 
		<th> # </th>
		<th> Name </th>
		<th> Mobile </th>
		<th> Email </th>
		<th> Property </th>
		<th> Unit   </th> 
		<th> Rent </th> 
		<th> &nbsp; </th> 
	</tr>
</thead>
<tbody id="unpaid_rent">
   <tr><td colspan='8' align='center'>Loading data...Please wait</td></tr> 
				
</tbody>
</table>
 
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>       
                    
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
<!-- responsive -->
    <div id="responsive" class="modal fade" tabindex="-1" data-width="600" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
           <h4 class="modal-title">	 Rent Payment History </h4> 
	     </div>
		<div class="modal-body" >
		<div class="col-md-12"> 
		<div class="col-md-6">
			<p id="tenant_name">    </p>
			<p id="phone">   </p>
			<p id="email">   </p>
		</div>
		<div class="col-md-6">
			<p id="property_name">    </p>
			<p id="floor">   </p>
			<p id="category">   </p>
		</div>
		
		</div>
		<div class="portlet light portlet-fit ">
		<div class="portlet-body">
			<div class="table-scrollable">
				<table class="table table-bordered table-hover">
					<thead>
					
						<tr>
							<th> Date </th>
							<th> Amount </th>
							<th> Payment mode </th> 
						</tr>
					</thead>
					<tbody id="payment_details">
						 
						 
						 
					</tbody>
				</table>
			</div>
		</div>
	</div>
		<div class="modal-footer" >
		<button type="button" class="btn red" onclick="window.print();"> Print </button>
		<button type="button" class="btn green"> Email </button>
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
		</div>
  
</div>
</div> 
</div> 
</div> 

<script type="text/javascript">

function unpaidRent()
{
		var content=""; 
		$("#paid_rent").html("<tr><td colspan='6' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>");
		$.ajax({
		url:'<?=base_url();?>tables/paid_rent/', 		
		type: 'POST', 
		async:false, 
		success:function (data)
		{  
			var obj = JSON.parse(data); 
			var data = obj.data; var count=0; var count=parseInt(data.length);$("#countr").val(count);  
            
			if(count >0){ 
				for(var i=0; i<data.length; i++)
				{
					var p = data[i];  var tenant_name="";  var floor=p['floor_no']; 
					if(floor==0  || floor =="0"){ floor_no="";}else{ floor_no="Floor No "+p['floor_no'];}
					
					var enct=p['enct']; if(p['tenant_type']=="residential"){ 
							tenant_name=p['first_name'];
						}else { tenant_name=p['company_name']; }  
					content=content+"<tr><td>"+(i+1)+"</td> "+
					"<td>"+tenant_name+"</td> <td>"+p['mobile_number']+"</td> <td>"+p['email']+"</td> <td>"+p['property_name']+"</td> <td>"+p['house_no']+" "+floor_no+"</td> <td>"+p['amount']+"</td>"+
					"<td class='center'> <a data-toggle='modal'   href='<?=base_url();?>rent/statement/"+enct+"' ><i class='fa fa-file-o'> </i> View Statement</a> </td> </tr>";
					count++;
				} 
				$("#unpaid_rent").html(content); 
			}
			else{
					$("#unpaid_rent").html("<tr><td colspan='8' align='center'>No data available</td></tr>");
				} 
			$('#table1').DataTable();
		}
	});
 
}
  
 $(document).ready(
 function(){   
	unpaidRent();
 });
</script>