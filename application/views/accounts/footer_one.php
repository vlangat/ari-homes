<!-- BEGIN FOOTER -->
<div class="row">
		<p>  &nbsp; </p>
		
		 <div  id="hide" style="position:fixed;top:38%;height:500px;bottom:0%;left:70%;right:2%;" >
		<div style="padding-bottom:8px;color:#fff;background:#006699;">
				<span class="glyphicon glyphicon-comment">  </span>
				Chat with  us 
				 <a href="javascript:;" style="float:right;color:#fff;font-size:20px" id="minimize" title="minimize"> &nbsp; - &nbsp; </a>
		</div> 
		 
			<div class="panel panel-primary" id="scroll" style="position:fixed;background:#fff; top:38%; bottom:0%;left:70%;right:2%;overflow-y:scroll;min-height:200px">
			 <div class="panel-body">
				<ul class="chat" id="received" style="list-style-type:none;background:#fff;z-index:-1">
					
					
				</ul>
			</div>
			
			<div class="panel-footer">
				<div class="clearfix">
					<!--<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">
								Nickname:
							</span>-->
							
							<input id="nickname" value="<?=$this->session->userdata('first_name')?>" type="hidden" class="form-control input-sm" placeholder="Nickname..." />
						
						<input id="counter" value="0" type="text"/>
						<!--</div>
					</div>-->
					<div class="col-md-12" id="msg_block">
						<div class="input-group">
							<input id="message" type="text" class="form-control input-sm" placeholder="Type your message here..." />
							<span class="input-group-btn">
								<button class="btn btn-warning btn-sm" id="submit">Send</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
	
<a href="#" class="float">
	<i class="fa fa-wechat my-float" style="font-size:24px"></i> Support
</a>

<!-- BEGIN INNER FOOTER -->
<div class="page-footer">
<div class="container"> ARI Limited &copy;  <?php echo date("Y");?> 
<h5> Phone: 0725 992 355 &nbsp;  Email: <a href="mailto:support@ari.co.ke">support@ari.co.ke </a> </h5>
</div>
</div>
<div class="scroll-to-top">
<i class="icon-arrow-up"></i>
</div>
	<!-- END INNER FOOTER -->
	<!-- END FOOTER -->
    <!--[if lt IE 9]> 
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url();?>multifiles/prism.js"></script>
	<script src='<?=base_url();?>multifiles/jquery.form.js' type="text/javascript" language="javascript"></script>
	<script src='<?=base_url();?>multifiles/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
	<script src='<?=base_url();?>multifiles/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
     <script>
  /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1942730-1', 'fyneworks.com');
  ga('send', 'pageview');*/

</script> 
<script type="text/javascript">
/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-1942730-1']);
_gaq.push(['_setDomainName', 'fyneworks.com']);
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();*/
</script>



<script type="text/javascript">	  
		$( document ).ready ( function () {
			$('#msg_block').show();
			//$('#success').modal('show');
			$('#nickname').keyup(function() {
				var nickname = $(this).val();
				
				if(nickname == ''){
					//$('#msg_block').hide();
				}else{
					$('#msg_block').show();
				}
			});
			
			// initial nickname check
			$('#nickname').trigger('keyup');
		});
		
</script>


        <!-- BEGIN THEME LAYOUT SCRIPTS -->
       <!-- END THEME LAYOUT SCRIPTS -->
		<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
		 
        <!-- BEGIN PAGE LEVEL SCRIPTS --> 
		<script src="<?=base_url();?>template/theme/assets/pages/scripts/table-datatables-editable.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>template/theme/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
         
		<script src="<?=base_url();?>template/js/dropzone.js" type="text/javascript"></script>
		<script src="<?=base_url();?>template/js/dropzone2.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
		 <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
		<!--Begin Footer items for To do--> 
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript">
        </script>
		
	 
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS --> 
		<script src="<?=base_url();?>js/autocomplete.js"></script>
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
         <!-- BEGIN THEME LAYOUT SCRIPTS --> 
    </body>

</html>

<script type="text/javascript">

var request_timestamp = 0;

var setCookie = function(key, value) {
	var expires = new Date();
	expires.setTime(expires.getTime() + (5 * 60 * 1000));
	document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

var getCookie = function(key) {
	var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
	return keyValue ? keyValue[2] : null;
}

var guid = function() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

if(getCookie('user_guid') == null || typeof(getCookie('user_guid')) == 'undefined'){
	var user_guid = guid();
	setCookie('user_guid', user_guid);
}


// https://gist.github.com/kmaida/6045266
var parseTimestamp = function(timestamp) {
	var d = new Date( timestamp * 1000 ), // milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		timeString;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}

	timeString = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
		
	return timeString;
}

var sendChat = function (message, callback) {
	$.getJSON('<?php echo base_url(); ?>api/send_message?message=' + message + '&nickname=' + $('#nickname').val() + '&guid=' + getCookie('user_guid'), function (data){
		callback();
	});
}

var append_chat_data = function (chat_data) {
	chat_data.forEach(function (data) {
		var is_me = "<?=$this->session->userdata('email');?>"; //data.guid == getCookie('user_guid');
		var photo=data.photo;
		if(!photo){ photo="me.jpg";}
		if(chatType==1){
			var html = '<li class="right clearfix" style="background:beige;padding:1px;">';
			html += '	<span class="chat-img pull-right">';
			html += '		<img  height="50" width="50"  src="<?=base_url();?>images/' + photo + '" alt="User Avatar" class="img-circle" />';
			html += '	</span>';
			html += '	<div class="chat-body clearfix">';
			html += '		<div class="header">';
			html += '			<small class="text-muted ">Sent on  ' + parseTimestamp(data.timestamp) + '</small>';
			html += '			&nbsp; <strong class="pull-right primary-font">' + data.nickname + ' &nbsp;</strong> ';
			html += '		</div> ';
			html += '		 <p style="text-align:left"> ' + data.message + '</p>';
			html += '	</div>';
			html += '</li><li> &nbsp;</li>';
		}else{
		  photo="ARI_Homes_Logo.png";
			var html = ' <li class="left clearfix" style="background:#fff;padding:1px;">';
			html += '	<span class="chat-img pull-left">';
			html += '		<img height="60" width="60" src="<?=base_url();?>images/login/' + photo+ '" alt="User Avatar" class="img-circle" />';
			html += '	 <strong class="primary-font"> &nbsp; ' + data.nickname + '</strong></span> <br/><br/>';
			html += '	<div class="chat-body clearfix" style="text-align:left;padding:5px;">';
			html += '	<div class="header">';
			//html += '	<strong class="primary-font"> ' + data.nickname + '</strong>';
			html += '<small class="text-muted"> Sent on ' + parseTimestamp(data.timestamp) + '</small>';
			//html += '			<small class="pull-right primary-font">' + parseTimestamp(data.timestamp) + '</small>';
			html += '</div>';
			html += '<p style="text-align:left">' + data.message + '</p>';
			html += '</div>';
			html += '</li><li> &nbsp;</li>';
		}
		$("#received").html( $("#received").html() + html);
	});
  
	$('#received').animate({ scrollTop: $('#received').height()}, 1000);
}

var update_chats = function () {
	if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
		var offset = 60*15; // 15min 
		request_timestamp = parseInt( Date.now() / 1000 - offset );
	}
		var count =$("#counter").val();  //alert(count); 
	/*	if(parseInt(count)==5)
		{  
	        
		}
	else{  */
	$.getJSON('<?php echo base_url(); ?>api/get_messages?timestamp=' + request_timestamp, function (data){
		 append_chat_data(data);	
			 
		var newIndex = data.length-1;
		if(typeof(data[newIndex]) != 'undefined'){
			request_timestamp = data[newIndex].timestamp;
		}
	}); 
 //  }
	
}

$('#minimize').click(function (e) {
	$("#scroll").hide(); 
	$("#hide").hide();
$(".float").show(); 	
});

$('.float').click(function (e) {
	$(".float").hide(); 
	$("#scroll").show(); 
	$("#hide").show(); 
});

$('#submit').click(function (e) {
	e.preventDefault(); 
		var msg=$('#message').val();
		if(!msg){ $('#message').focus(); return false;}
	var $field = $('#message');
	var data = $field.val();

	$field.addClass('disabled').attr('disabled', 'disabled');
	sendChat(data, function (){
		$field.val('').removeClass('disabled').removeAttr('disabled');
	});
});

$('#message').keyup(function (e) {
	if (e.which == 13) {
		var msg=$('#message').val();
		$("#counter").val(0);
		if(!msg){ $('#message').focus(); return false;}
		$('#submit').trigger('click');
		
		scroll();
	}
});

setInterval(function (){
	var tokenId = "<?php echo $this->session->userdata('tokenId')?>";   
	update_chats(); 
	if(!tokenId){ $("#counter").val(5);}else{ $("#counter").val(0); }  
	scroll();
}, 2500);



$(function(){  
	$("#scroll").hide();
	$("#hide").hide();  
});

 function scroll()
  {
		var myDiv = document.getElementById("scroll");
		myDiv.scrollTop = myDiv.scrollHeight; 
		 //alternative you can use this to make scroll bar always at bottom
		 //$("#scroll").animate({ scrollTop: $(document).height() }, "fast");
  }
</script>