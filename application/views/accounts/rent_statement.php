<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Rent  </span>
	<i class="fa fa-circle"> </i>
</li>
<li>
	<span> Rent Statement</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
 
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title">
		 
<form action="<?=base_url();?>rent/rent_statement" method="post" onsubmit="return validate()">	 
<div class="row">
	<div class="col-md-4">  
		<div class="form-group">    
				<label class="control-label col-md-4">From </label>
				 <input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value="<?=date("m/d/Y")?>"  placeholder=""  name="from" />
			
					<!-- /input-group -->
				</div> 
			<label id="error1">					</label>  
	</div> 
	<div class="col-md-4">  
		<div class="form-group">    
				<label class="control-label col-md-4">To </label>
				 <input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value="<?=date("m/d/Y")?>"  placeholder=""  name="to" />
		</div> 
			<label id="error1">					</label>  
	</div> 	 		 
	<div class="col-md-4"> 
	<label class="control-label col-md-12"> &nbsp;&nbsp; </label>
		<div class="form-group">   
				 <button type="submit" class="btn green"> Search</button>
		</div>
	</div> 
</div> 
</form>
  
<hr/>
 <!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
	
<div id="prnt">
<table    class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr> 
		<th align="left"> Date </th>  
		<th align="left"> Tenant </th>  
		<!--<th align="left"> Receipt No </th>  
		<th align="left"> Payment Mode </th>   
		<th align="left"> Mode No </th>  -->
		<th align="left"> Description </th> 
		<th align="left"> Debit </th>  
		<th align="left"> Credit </th>  
		<th align="left"> Balance </th> 
	</tr>
</thead>
<tbody> 
<?php 
		$credit=0; $debit=0; $curr_bal=0; $total=0;
		$tenant_name="";
		foreach($statement->result() as $row){
		if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
		if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
		  
		$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
		$enct =base64_encode(do_hash($row->tenant_id . $salt,'sha512') . $salt);		  
		$enct2 =base64_encode(do_hash($row->receipt_no . $salt,'sha512') . $salt);		  
	?>

	<tr>
		  <td> <?=$row->date_paid?>    </td>  <td> <?php $tenant_type=$row->tenant_type; if($tenant_type=="residential"){ $tenant_name=$row->last_name." ".$row->middle_name." ".$row->first_name;}else{ $tenant_name=$row->company_name;}  ?> <a href="<?=base_url();?>rent/statement/<?=$enct?>"> <?=$tenant_name?></a> </td> <!-- <td>   </td>--<td> <?//=$row->payment_mode?> </td> <td> <?//=$row->payment_mode_code?>--> </td> <td> <?=$row->description?> &nbsp; <?php if($row->type=="d"){  echo $row->receipt_no;  }else{ ?><a href="<?=base_url();?>rent/printReceipt/<?=$i="0"?>/<?=$enct?>/<?=$enct2?>">  <?=$row->receipt_no?>  </a> <?php }?> </td> <td> <?php if($row->type=="d"){echo $row->amount;}else{ echo '-';}?> </td>  <td> <?php if($row->type=="c"){echo $row->amount;}else{ echo '-';}?> </td>  <td> <?php   if(($debit-$credit)<0){echo 0-$curr_bal;}else { echo $curr_bal; }?> </td> 
	</tr>
	
<?php   }?>	
</tbody>
</table>
<hr/>
<center> <p><b>  Closing Balance  </b>  Ksh: <?=$debit-$credit; ?> </p> </center>
</div>
<!--
 <a  href="javascript:;" class="btn green"  id="btn_receipt" >  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
-->		 
</div> 
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div>   
<!-- END CONTAINER -->
<script language="javascript">
 
 $("#btn_receipt").click( function () {
	var divContents = $("#prnt").html();
	var printWindow = window.open('', '', 'height=400,width=800');
	printWindow.document.write('<html><head><title>  Statement </title>');
	printWindow.document.write('</head><body >');
	printWindow.document.write(divContents);
	printWindow.document.write('</body></html>');
	printWindow.document.close();
	printWindow.print();
        });
</script>
