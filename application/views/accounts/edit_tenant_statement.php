<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		var tenant=$("#tenants").val();
		var pay_mode=$("#pay_mode").val();
		var receipt_no=$("#receipt_no").val();
		var amount=$("#amount").val();
		var pay_date=$("#pay_date").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
	//	if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		 if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
		 
	}
	function validate_edit()
	{    
		var name=$("#edit_user_name").val();
		var pay_mode=$("#edit_pay_mode").val();
		var amount=$("#edit_amount").val();
		var receipt_no=$("#edit_receipt_no").val();
		var pay_date=$("#edit_pay_date").val(); 
		if(name==""||name==null){ $("#edit_error1").html("<font color='red'> Tenant Name   is empty </font>");$("#edit_user_name").focus(); return false;}
		if(amount==""||amount==null){ $("#edit_error4").html("<font color='red'> Amount field is empty </font>"); $("#edit_amount").focus(); return false;}
		if(pay_date==""||pay_date==null){ $("#edit_error3").html("<font color='red'> Date field is empty</font>"); $("#edit_pay_date").focus(); return false;}
		//if(receipt_no==""||receipt_no==null){ $("#edit_error2").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>"); $("#edit_receipt_no").focus(); return false;}
		return confirm("Update details for "+name+"?");
	}
	</script>
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Rent  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Edit Tenant Payment</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title">
				
		<div class="col-md-12" style="background:#006699;padding:6px;">
			<font color="#ffffff"><strong> &nbsp;  Modify Tenants Transactions </strong> </font> 
	   </div>
	   <div class="col-md-12">  &nbsp;  </div>	
	 
<div class="row">
 
	<div class="col-md-6">  
						<div class="form-group"> 
						<input   class="form-control" type="hidden" value="" id="edit_id" name="edit_id">
								<label> Tenant Name </label><br/>
								<select  class="selectpicker"  data-live-search="true" name="tenant"   id="tenants"  title="Select Tenant...">
									 <?php foreach($tenants->result() as $row){?>
											<option value="<?=$row->id?>">
												<?php $company=$row->company_name; $name=$row->first_name." " . $row->middle_name." ".$row->last_name; if($company ==""){ echo $name.",". $row->property_name.",". $row->house_no; }else{ echo $row->company_name.",". $row->property_name.",". $row->house_no;;}?>
											</option>
									 <?php } ?>
								</select> <br/>
								<label id="error1">					</label>  
					</div>
						
						<div class="form-group">
								<label> Payment Method </label>
								<select class="form-control"  name="pay_mode" id="pay_mode"  onchange="disable_method()" >
									<option value=""> Not selected </option>
									<option> Cash </option>
									<option> Cheque </option>
									<option> Mpesa </option>
									<option> Bank Receipt </option>
								</select>
							 <label id="error3">  </label>
						</div>
				 
				<p> </p> 
				</div> 
		
 
	<div class="col-md-6">    
					 <div class="form-group">
								<label> Amount </label>
								<input type="number" class="form-control" required name="amount"   id="amount"  value="0" min="1"  max="25000000" onchange="setData()">
							 <label id="error2">  </label>
						</div> 
						
					 <div class="form-group">
								<label> Mpesa, Cheque or Receipt Number </label>
								<input type="text" class="form-control"   name="receipt_no"   id="receipt_no" onchange="validate_char('receipt_no')">
							 <label id="error5">  </label>
						</div>
		 </div>
		
   </div> 
   
	<div class="row"> 
			 <div class="col-md-6">    
				 <div class="form-group"> <label> Transaction Type </label>
							<select class="form-control"  name="type" id="type" >
								<option value=""> Not selected </option>
								<option value="c"> Credit </option>
								<option value="d"> Debit </option> 
							</select> 
							<label id="error6">  </label>
				</div>  	 
		 </div> 
<div class="col-md-6">    
			 <div class="form-group">
						<label> Description </label>
						<textarea name="description" class="form-control"  cols="60" id="description"></textarea>
					  
				</div> 
						 
		 </div> 

		 

</div>		 
	<div class="row">  
	<div class="col-md-3">  
				 <div class="form-group">
				 <button  type="submit" class="btn green" id="update">  &nbsp; Update Changes  &nbsp;  </button>
				</div>
						
		 </div> 
		 <div class="col-md-6"> 
		 <div class="form-group">
					<p id="status_message">  </p>
                        </div> 
		 </div>
	 
</div> 

 


 
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div>
</div>
<!---Edit payment-->

 
	

<!--->
 <!-- responsive -->
 


<div id="success" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b style="font-size:20px;color:green">    Warning Message </b></h5>
				<hr/>
				<p id="success_msg">
				   
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
 <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">    Success Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="status_msg">
				  
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 
function validate_char(id)
{
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		$("#success").modal('toggle');  
        return false;
		}
    
}

$(document).ready(function () {   
  
       //$("#data_saving_success").modal('toggle'); 	
 
	$("#update").click(function(){ 
	var tenant=$("#tenant").val();
	var description=$("#description").val();
	var pay_mode=$("#pay_mode").val(); 
	var amount=$("#amount").val();
	var amount=$("#amount").val();
	var tenant=$("#tenants").val(); 
	var type=$("#type").val(); 
	var receipt_no=$("#receipt_no").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
	//	if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		 if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
		 if(type==""||type==null){ $("#error6").html("<font color='red'> Transaction type required </font>");return false;}
		 if(amount<=0){ $("#error2").html("<font color='red'> Amount must be greater than zero(0) </font>"); $("#amount").foucus(); }
  $.ajax(
  {
		url:"<?=base_url();?>rent/edit_tenant_transact",
		type:"POST",  
		data:{
			'tenant':tenant,
			'receipt_no':receipt_no,
			'description':description,
			'amount':amount,
			'type':type,
			'pay_mode':pay_mode
		    },
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				$("#status_msg").html("<font color='green'> Payment updated  successfully </font>");
				$("#tenant").val('');$("#error1").empty();$("#error2").empty();$("#error3").empty();$("#error4").empty();$("#error5").empty();$("#error6").empty();
				$("#description").val('');
				$("#pay_mode").val(''); 
				$("#amount").val('');
				$("#amount").val('');
				$("#tenants").val(''); 
				$("#type").val(''); 
				$("#receipt_no").val('');
				 $('#data_saving_success').modal('show');
				   
			 }
			 else{
				 $("#status_message").html("<font color='red'> Not updated. Try again later </font>");
			 }

		}			 
  })
 
 });

 });
  
  
  
 function disable_method()
  { 
	var value=document.getElementById("pay_mode").value;
	 if(value=="Cash")
	  {
		 
	document.getElementById('error5').value ="";
	document.getElementById('receipt_no').value ="--";
	document.getElementById('receipt_no').disabled = true;
      
	  }
	  else
	  {
			document.getElementById('receipt_no').value ="";
			document.getElementById('receipt_no').disabled = false;
	  }  
}
</script> 