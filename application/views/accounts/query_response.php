<?php $count1=0;$count2=0;$count3=0;$count4=0;?>
<div class="page-container">
<!-- BEGIN CONTENT --> 
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	
	<li>
		<a href="<?=base_url();?>"> Home </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#"> Support </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span> Response </span>
	</li>
	
</ul>

<div class="page-content-inner">
<div class="inbox">
<div class="row"> 

<div class="col-md-12">
  
<div class="inbox-body"  id="scroll" style="height:500px; width:100%;overflow-y:scroll;padding-top:10px;" >
 <div class="col-md-12" style="background:#006699;padding:6px;">
			<font color="#ffffff"><strong> &nbsp; Queries   </strong> </font> 
	   </div>
  <?php
  $id="";
  foreach($queries->result() as $r){ $id=$r->id; $sub=$r->subject; $desc=$r->description; }?>
	<table class="table" border="1"> 
		<tr> <th> Date </th> <th> BY </th> <th> Subject </th> <th> Details </th> <th> Status </th> </tr>
		<tr>
			<td> <?=$r->date_sent?></td>
			<td> <?=$r->name?></td>
			<td> <?=$sub?> </td>
			<td> <?=$desc?> </td>
			<td> 
				<?php  
				$msg="Pending";
				$admin_id=$this->session->userdata('admin_id');
				$color="red"; $t_color="#fff"; $status=$r->status; if($status==1){     } else if($status==2){ $msg="Solved"; $color="green";  }  ?>
				<font style="background:<?=$color?>;color:<?=$t_color?>;">
				<?php if($admin_id==""){  echo $msg;   }else {?>
					<select style="color:#000;background:;" name='status' onchange="change_status('<?=$r->id?>',this.value)">
						<option> <?=$msg?> </option> 
						<option value='1'> Pending </option> 
						<option value='2'> Solved </option> 
					</select>
				<?php } ?> </font> 
			</td>
		</tr>
	</table>
  <?php foreach($responses->result() as $resp)
			{ 
			    $response_title="Customer Response";
				if($admin_id==""){ $response_title="My Response";}
				if($resp->response_type==2){ $response_title="Support Response"; }
				 					
				?>
				<div class="col-md-12" style="background:#006699;padding:6px;">
					<font color="#ffffff"><strong> &nbsp; <?=$response_title?>   </strong> </font> 
				</div> 
		<table class="table" border="1">  
			<tr>
			 
				<td><strong>  <?=ucfirst(strtolower($resp->name));?> </strong> &nbsp; &nbsp; &nbsp;   <?=$resp->response_date;?>   <p>     </p><?=$resp->response_message;?>  </td>
				 
			</tr>
		</table>
	<?php  
		}	 
	?>
	</table>

 <form action="<?=base_url();?>support/reply_response" method="post" onsubmit="return validate()">
 <hr/> 
 <input type="hidden" value="<?=$id?>" class="form-control" name="response_id">

<p>   </p>
<div class='inbox-form-group'> <label class='control-label'> Reply   </label>
<textarea class='inbox-editor inbox-wysihtml5 form-control' id='message' name='message' rows='6'></textarea>
</div>
<div>
	<p id="counter"> </p> <p id="error"> </p>
    </div> 
      
		<button class='btn green' onclick='send()'>
	<i class='fa fa-check'></i> Post Reply </button>  
	</form>
	<hr/>
	<!--<div class="inbox-content">  -->

	 
	<!--	</div>-->
	</div>
</div>
</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div> 

 <div id="confirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Confirm Deletion</h4>
			</div>
			<div class="modal-body">
				<p>  Do you want to remove the selected Messages? </p>
			</div>
			<div class="modal-footer">
				<button class="btn default" data-dismiss="modal" aria-hidden="true">No</button>
				<button data-dismiss="modal" id="confirmDelete" class="btn blue">Yes</button>
			</div>
		 
</div>

 
<!-- END CONTAINER -->
<script type="text/javascript">
 
$(function(){
	scroll();
 
});

function validate(status)
{  	  
	var body=$("#message").val();  
	if(!body){ $("#error").html("<font color='red'>Reply message is required  </font>"); $("#message").focus(); return false;}
	//if(/^[a-zA-Z0-9- ]*$/.test(body) == false){ $("#message").focus(); $("#error").html("<font color='red'>Message contains illegal characters </font>");  return false; } else{$("#error").empty();} 
    return true;
 
}
 function change_status(id,status)
  {
	  var id2="<?=substr(md5(date("d")),0,16)?>";  
		  $.ajax({
		url:'<?=base_url();?>support/change_query_status/'+id+"/"+status, 
		type: 'POST',
		data:{'id':id,'status':status},		
		success:function (data)
		{ 
			 
			var obj = JSON.parse(data);
			if(obj.result=="ok")
			{  
				//document.location.reload(true);
				window.location="<?=base_url();?>support/admin_response/"+id+"/"+id2;
			}
		}
	}) 
  } 
  
 function scroll()
  {
		var myDiv = document.getElementById("scroll");
		myDiv.scrollTop = myDiv.scrollHeight; 
		 //alternative you can use this to make scroll bar always at bottom
		 //$("#scroll").animate({ scrollTop: $(document).height() }, "fast");
  }
 </script> 