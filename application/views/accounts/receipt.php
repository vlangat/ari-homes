<!-- BEGIN PAGE CONTENT BODY -->

<?php 
		$credit=0; $debit=0; $curr_bal=0; $total=0; $receipt="";
		/* foreach($statement->result() as $row){
	     if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
		 if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
		
		 }*/
		   
	?>
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Payment </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Receipt   </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">
	
	<div class="col-md-12"    style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Receipt </font> 
	</div>		 
  
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
<div class="portlet light ">  
<div class="row"> 
<div class="col-md-8"> 
   <div class="portlet light">  
		<div class="portlet-body" style="font-size:12px;width:100%">
		<?php 	
		$logo=""; $tenant_company_name="";$company_web=""; $mobile_number1=""; $slogan=""; $mobile_number2=""; $description=""; $company_name=""; $address=""; $country="";$company_email=""; $town="";$postal_code=""; $mobile_number=""; $company_location=""; 
			 foreach($company_details->result() as $c)
			 {
				 if($c->company_code==$this->session->userdata('company_code'))
				 { 
					$company_name=$c->company_name; $company_web=$c->company_web; $company_location=$c->location;
					$logo=$c->logo; $description=$c->description; $company_email=$c->company_email; $postal_code=$c->postal_code;
					$address=$c->address; $mobile_number=$c->mobile_number; $town=$c->town; $country=$c->country;
				    $mobile_number1=$c->mobile_one; $mobile_number2=$c->mobile_two;$slogan=$c->slogan;
				 } 
			}
		 
		foreach($tenant_details->result() as $t){  } 
		if($logo ==""){ $logo="ari_company_logo.png"; } ?> 
		<div>
		<table border="0" class="table table-striped table-hover" width="100%" cellpadding="8" cellspacing="2">
		<tr>
			<td>
				<img id="my_file" src="<?=base_url();?>media/<?=$logo?>" height="75" width="150" style="display:block;max-width:230px;max-height:95px;width: auto;height: auto;" alt="<?=strtoupper($company_name)?>">
			</td>
			<?php if($company_name !=""){ ?>
			<td>
				<h4><strong><?=$company_name?> </strong></h4>
				<h5><?=$description?></h5>  
				<h5><?=$company_location?></h5>  	 
				<h5><?=$company_web?></h5>   
				<h5>&nbsp;</h5>   
				
			</td> 
			<td>
				
			<h5><strong> Cell:</strong>	<?=$mobile_number?></h5> 
			<h5><strong>Email:</strong>	<?=$company_email?></h5>
				<h5><?=$address."-".$postal_code.",";?></h5>
				<h5><?php if($town !=""){ echo ucfirst(strtolower($town)).", ".ucfirst(strtolower($country));}?></h5>
			<?php if($mobile_number1 !="" || $mobile_number2 !=""){?>
			<h5><strong> Tel:</strong>	<?=$mobile_number1?>,<?=$mobile_number2?></h5> 
			<?php }?>
			</td>
			<?php } ?>
		</tr>
		</table>
		</div> 
		 
	<table border="0"  class="table table-striped table-hover" style="font-size:12px;padding:1px;border:1px solid lightgrey" cellpadding="0" cellspacing="0" width="100%"> 
			<tr> 
					<td align="left">
						<font>  <strong> Receipt No: </strong><?=$receipt_no?></font> 
					</td> 
					 <td>   &nbsp; </td> 
					<td align="right">
						<strong>   Date: </strong>  <?=$date_paid;?> &nbsp;
					</td>
				 </tr>
				 <tr><td colspan="3"> Being payment for <strong><?=$t->property_name .",". $t->category_name .", House No ".$t->house_no?></strong></td></tr>
		<tr>
		<td align="left">  	
		<ul style="font-size:13px;padding-left:0px;list-style-type:none">
<!--			style="font-size:13px;padding-left:0px;list-style-type:none">-->
			<li><strong>Paid By:</strong></li>  
			<li>	<?php $tenant_type=$t->tenant_type; if($tenant_type=="residential"){

			 $other_names="";
		 echo $tenant_name=($t->first_name ." ".$other_names); 
		 $tenant_company_name=$t->company_name; }else{ echo $tenant_name=$t->company_name; $company_name="";}
			
		 ?>	
		 </li>
			<li>	<?=$email=$t->tenant_email?>  </li>
			<li>	<?=$tenant_company_name?>  	</li>   
			<li>	&nbsp; 	</li>   
		</ul>
		</td>
		<td>   &nbsp; &nbsp; </td> 
		  <td  align="right">
			<?php if($company_name ==""){ 
				$landlord_name=""; $address="";
				foreach($user_info->result() as $u)
				{ 
				$email=$u->email; $landlord_name=$u->first_name ." ". $u->middle_name ." ".$u->last_name;
				$address=$u->address; $phone=$u->mobile_number; 
				} 
				?>
				<ul style="font-size:13px; list-style-type:none">
					<li><strong> Paid to:</strong></li>  
					<li> <?=$landlord_name?> </li>
					<li> <?=$email?> </li>
					<li> <?=$phone?> </li>
					<li> <?=strtoupper($address);?> </li> 
					<li> &nbsp;   </li> 
				</ul>
			<?php } ?>
		</td>
	</tr>	
	<tr>
			<td> 
				 <strong> Amount Received:</strong> KES <?=number_format($amount)?>  
			</td> 
			<td> 
				<strong> Payment Method:</strong>   <?=$payment_mode?>  
			</td> 
			<td>
				<strong> Payment No: </strong>  <?php  if($payment_mode=="Cash"){ $payment_mode_code="";  }else if($payment_mode_code =="0"){   $payment_mode_code="";} else{ echo $payment_mode_code; }?>   	
			</td> 
	</tr>
		<tr> <td> &nbsp;  </td><td> &nbsp;  </td><td> &nbsp;  </td>  </tr> 
		<tr  bgcolor="#DFDAD9"> 
			<td>   <strong>  Particulars </strong>    </td> <td>  </td> <td align="right">  <strong>   Amount &nbsp;&nbsp;  </strong>  </td>
		</tr>
	 
		 <?php 	$item_cost=0; 	$d=0;
		foreach($receipt_items->result() as $r){ ?>
		<tr height="20"> 
		<td> <?php 
		echo $r->payment_type ." ". $r->description;
		?>
		</td> <td>  </td> <td align="right"> KES  <?=number_format($r->paid_amount)?></td>
		</tr>
		<?php 
		 $item_cost=$item_cost+$r->paid_amount;  
		}  
		$sub_total=$amount; 
		$overpay=0;  
		 if(($item_cost)  < $amount)
		 { 
			//$overpay=$sub_total-$item_cost; 
		 }
		 if($balance <0){  $overpay=$sub_total-$item_cost;  }else{    }
		 ?>
		<tr> <td> </td> <td>  </td><td align="right"> <b> Sub Total </b>&nbsp; KES  <?=number_format($item_cost);?>  </td></tr> 
		
		<?php if($balance<0 && $overpay >0){?>
				<tr> <td>  </td> <td>  </td> <td align="right"> <b> Over-payment</b> &nbsp;  KES  <?=number_format($overpay)?></td></tr>
		<?php }?>
		<tr> <td> </td> <td>  </td> <td align="right">   <strong> Total </strong>    &nbsp;  KES <?=number_format($amount);?>   </b>  </td></tr>
		<tr><td> &nbsp; </td><td>  &nbsp; </td><td> &nbsp;</td> </tr>
		<tr><td> </td> <td>  </td> <td align="right"><strong> Balance</strong>   &nbsp;  KES <?=number_format($balance); ?>     </td></tr>
	</table>
  
<p> 
	<h5><i><strong> Served by: </strong><?=ucfirst(strtolower($this->session->userdata('first_name')))." " .ucfirst(strtolower($this->session->userdata('last_name')));?></i> <font style="float:right"><strong> Sign  &amp;  Stamp </strong> </font></h5>
	 
	
</p>
<p> &nbsp; </p>
<p> &nbsp; </p>
<!--<p> <i> Rent is payable on or before <?//=$t->expected_pay_day?> of every payment month </i></p>-->
<p> <center> <i> <?=$slogan?>  </i></center></p>
</div>
 
<p>  <hr/> </p>
 
<a  href="javascript:;" class="btn grey"  id="btn_receipt" > &nbsp;&nbsp;&nbsp; <i class="fa fa-print" style="font-size:18px;color:#006699"></i><b>  Print  </b>&nbsp;&nbsp;&nbsp;</a> 
&nbsp;
<a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  

		 </div>
		</div>
 
</div>
</div>
</div> 
</div> 
</div>
<!-- END EXAMPLE TABLE PORTLET--> 

</div>
<!-- END PAGE CONTENT INNER -->
</div> 
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
  <div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> Venit Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
   


 <div id="sendMail" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Email Tenant Message </b></h5>
				<hr/>
					<p>
					    This Receipt will be sent as an Email to <font id="email_id"> </font>
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="send_email" class="btn green">&nbsp; Yes &nbsp; </button>
			 <button type="button" data-dismiss="modal" class="btn default">&nbsp; Cancel  &nbsp; </button> 
		</center>
	</div>
</div>
   
      
        <!-- END CONTAINER -->
<script language="javascript">
 
 $("#btn_receipt").click( function () {
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=1000');
            printWindow.document.write('<html><head><title>  Receipt </title>');
            printWindow.document.write('<style>th, td { border-bottom: 1px solid #ddd;}</style> </head><body style="border:1px solid #ddd; margin-left: auto;margin-right: auto; width:80%">');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
		
	
		
  
 
		var email="<?=$email?>";  
        var fname="<?=$tenant_name?>";  
        var from="<?=$company_name?>"; 
      
		
 $("#btn_email").click(function ()
		{ 
		   if(!email){ 
		   alert('Email address is empty! Please add tenant email first');
		   return false;
		   }
		   $("#email_id").html(fname+" <u> "+email+" </u>");
		   document.getElementById('send_email').disabled = false;
			$("#sendMail").modal('show');
		});
		
   $("#send_email").click( function () {
document.getElementById('send_email').disabled = true; 	   
		var divContents = $(".portlet-body").html();   
       
		$("#error").html("<font color='#006699'> Sending email...please wait</font>");
		//$("#success").modal('show'); 
		 
		$.ajax({
			//'url':"<?=base_url();?>payment/sendRentStatement",
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			 async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Rent Receipt", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
					$("#error").html("<font color='green'> Receipt   sent   successfully</font>");
					//$("#success").modal('show');
					setTimeout(function()
						{ 
							$("#error").empty();
							$("#sendMail").modal('hide');
						}, 2500);
				}
				else
				{  
					$("#error").html("<font color='red'> Receipt   not sent to   <u>"+email+"</u></font>");
					document.getElementById('send_email').disabled = false;
				}
				
			}
			
 })
 
});
		
$(function () {
$.fn.vAlign = function(){
    return this.each(function(i){
    var ah = $(this).height();
    var ph = $(this).parent().height();
    var mh = Math.ceil((ph-ah) / 2);
    $(this).css('margin-top', mh);
    });
};

$('#my_file').vAlign();
		});
</script>
