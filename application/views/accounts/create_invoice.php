<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		document.getElementById('save').disabled=false;	
		var tenant=$("#tenants").val();
		var pay_mode=$("#pay_mode").val();
		var receipt_no=$("#receipt_no").val();
		var amount=$("#amount").val();
		var pay_date=$("#pay_date").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
	//	if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		 if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
			document.getElementById('save').disabled=true;	 
	}
	function validate_pay()
	{   
		document.getElementById('receive').disabled=false;		
		var pay_mode=$("#pay_mode").val();
		var amount=$("#amount").val();
		var receipt_no=$("#receipt_no").val();
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>"); $("#amount").focus(); return false;}else{ $("#error2").empty();}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}else{ $("#error3").empty();}
		if(receipt_no=="")
		{
			if(pay_mode == "Cash"){   }else{$("#error5").html("<font color='red'> Receipt/Cheque No  required </font>");  $("#receipt_no").focus(); return false;} 
		}
		$("#error2").empty(); $("#error3").empty(); $("#error5").empty(); 
		document.getElementById('receive').disabled=true;
		return true; 
	}
	
</script>

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Invoice  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Created Invoice</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
	<div class="portlet light ">
		<div class="portlet-title"  style="min-height:300px"> 
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong> &nbsp;   Created Invoices </strong> </font> 
		</div> 
<div class="col-md-12">  &nbsp;  </div> 
<div class="row">
<div class="col-md-12"> 
<div class="form-group"> 
<form action="<?=base_url();?>rent/receiveRent" method="post" onsubmit="return validate()">
<input class="form-control" type="hidden" value="" id="edit_id" name="edit_id"/>
		<label> Invoice To </label><br/>
		<!--<select  class="selectpicker"  data-live-search="true" name="from"  onchange="getBalance(this.value)" id="tenants"  title="Select Tenant...">
		-->
		<select  class="selectpicker"  data-live-search="true" name="from"   onchange="getInvoice()" id="tenants"   title="Select Tenant...">
			 <?php $rent_frequency=""; $priority=1;
			 foreach($tenants->result() as $row){?>
					<option value="<?=$row->id?>">
						<?php $company=$row->company_name;
						$rent_frequency=$row->rent_frequency; 
						$middle_name=""; $last_name=""; $other_names="";
			 $name=$row->first_name; if($company ==""){ echo $name.", ". $row->property_name.", ". $row->house_no; }else{ echo $row->company_name.", ". $row->property_name.",". $row->house_no;}?>
					</option>
			 <?php } 
			 if($rent_frequency==0){ $rent_frequency="";}
			 ?>
		</select> <br/>
		<label id="error1">	 	</label>  
</form>
</div>
</div>  
 <?php 
 $tenant_property_name=""; $tenant_name="";$tenant_room_no=""; $tenant_floor=""; $tenant_mobile_number=""; $tenant_email=""; $total=0; $tenant_id=""; $category_id="";
 //if(!empty($received_rent)){
 if(!empty($received_rent)){ 
	 foreach($selected_tenant->result() as $r)
	 {
			$tenant_name=$r->first_name;
			$total=0; $tenant_id=$r->id;   $tenant_room_no=$r->house_no; $tenant_floor=$r->floor_no; 
			$tenant_email=$r->tenant_email; $tenant_mobile_number=$r->mobile_number; $tenant_property_name=$r->property_name; $category_id=$r->property_unit_id; 
			$tenant_company_name=$r->company_name;
			if($r->tenant_type=="commercial"){ $tenant_name=$tenant_company_name; }
	}
  
 }   
 ?> 
 
 
<form action="<?=base_url();?>rent/receive_pay" method="post" enctype="multipart/form-data"  onsubmit="return validate_pay()">
<div class="col-md-12"> 
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
		<thead>
			<tr> <input type="hidden" id="invoice_no" >
				<th> # </th>  
				<th>Item  </th>  
				<th>Amount </th> 
				<th>Description</th> 
				<th colspan="2">Action</th>  
			</tr>
		</thead>
		<tbody id="pricing_details">
			<td colspan="5" align="center">  No records found </td>  
		</tbody>
	</table>
	<p>	<font size="3"> Total Amount: Ksh.<b> <font id="pricing_total"> 0 </font> </b> </font> </p>
		<button   class="btn green" id="add_new"> <i class="fa fa-plus"> </i> Add Invoice Item </a> </button><font id="error_empty_category"></font>
		
 
 
</div>
 
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div> 
</div> 
<!---Edit payment-->

 <!-- pricing details modal -->
<div id="pricing_modal" class="modal fade" tabindex="-1" data-width="800" data-height="400" >
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12"> 
					<h5><b> Add invoice items for the Invoice No <font id="invoice"></font></b> </h5>  
					 <input class="form-control" value="1" type="hidden" id="item_no">  
					<table class="table" id="sample_editable_1">
					 <tr>
						<td> 
						 <label> Item: </label>
							<input type="text"  class="form-control" id="item"/>
						</td>   
					<td><label> Amount: </label> <input   class="form-control"  type="number"  id="pricing_value" value="" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" >  </td>
					<td><label> Description:</label> <input   class="form-control" type="text"  id="description" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" >  </td>
					 
					<!--<td> Priority:-->
						<input   class="form-control" type="hidden"  value="1" min="0" max="1000000000" id="priority" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" > 
					<!--</td> --> 
					<td> <label> &nbsp; </label> <input type="submit" class="btn green" id="save_pricing" value="Save Item">   
					</td>
					<td colspan="3"> 
							<font id="vat_notification">  </font>
					</td>
					</tr>
					
					</table>
					<p id="status_msg">   </p>
				</div>
				</div>
				
<div class="row">			
<div class="col-md-12" style="overflow-y:scroll;max-height:520px">			
	<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
		<thead>
			<tr> 
				<th> #</th>  
				<th> Item Name</th>  
				<th> Payment Amount  </th> 
				<th> Description  </th> 
				<th colspan="2"> Action  </th> 
			</tr>
		</thead>
		<tbody id="pricing_data">
			<td colspan="5" align="center">  No records found </td>  
		</tbody>
	</table>
		<p> Total Amount: <font id="total_price"> 0 </font>  </p>
		 
			</div>
		</div>
		
</div>
<div class="modal-footer" >
<font id="invoice_link"> </font>   
 <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button> 
</div>
</div>
<!-- end  -->

<div id="warning" class="modal fade" tabindex="-1" data-width="400">
<div class="modal-header">
	<b style="font-size:20px;color:brown">Warning Message </b> 
</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p>
				  <font color='red' id="warning_msg">  </font>
				</p> 
				</div>
			</div>    
	</div>
	<div class="modal-footer" > 
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Ok</button>
	</div> 
</div>

<div id="deleteItemModal" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5> <b  style="font-size:18px;color:brown"> Confirm Delete Action </b> </h5>
				<hr/>	
				<p>
					Are you sure you want to remove this invoice item? 
				</p> 
				<input type="hidden" id="del_item_id">
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button"  onclick="confirm_delete_item()" class="btn green">Yes</button>
		<button type="button" data-dismiss="modal" class="btn default">No</button>
	</div>
</div>

<div id="editItemModal" class="modal fade" tabindex="-1" data-width="600">
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
			<h5> <b> Edit Invoice Item details </b> </h5>
			<p>
				<label class="control-label"> Item </label>
				<input   class="form-control"  type="text"  readonly id="edit_item"> 
				<input   class="form-control"  type="hidden"   readonly id="edit_id">   
			</p>
			<p>
				<label class="control-label"> Amount </label>
				<input   class="form-control" required="required" type="number" id="edit_amount" > <!--onkeypress="return checkIt(event)"-->
				<span id="error3"> 			</span>				
			</p> 
			<p>
				<label class="control-label"> Description </label>
				<input   class="form-control" required="required" type="text" id="edit_description">
				<span id="error3"> 			</span>				
			</p> 
			<!-- <p>
				<label class="control-label"> Priority </label> -->
				<input type="hidden"  value="1" id="edit_priority" class="form-control" > 
				<input type="hidden"  value="" id="edit_invoice_no" class="form-control" > 
			<!--</p> -->
			</div>
		</div>
		 <p id="error_edit" style="float:left">  </p><br/><br/>
	</div>
	
<div class="modal-footer" >
	<button type="submit" class="btn green" id="done_edit" > Save </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
</div>
</div>
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 
 function input_validation()
{
	var property_name=$("#property_name").val();
	var town=$("#town").val();
	var country=$("#Country").val();
	var street=$("#street").val();
	var floors=$("#floors").val();
	var location=$("#Location").val(); 
	// if(!property_name||!country||!street||!town||!floors||!location){  return false; }
	
}
$("#add_new").click(function()
	{  
		$("#status_msg").empty();
		var tenant_id=$("#tenants").val();  
		if(!tenant_id){ 	$("#warning_msg").html("<font color='red'> Please select tenant to proceed </font>");$("#warning").modal('show'); $("tenants").focus(); return false;}

		$("#pricing_modal").modal('show');
});

function getInvoice()
	{  
	 $("#status_msg").empty();
	 $.ajax({
				url:"<?=base_url();?>invoice/getNewInvoiceNo/",
				type:"POST", 
				async:false,
				success:function(data)
				{  	  
					var obj=JSON.parse(data);
					var data = obj.data; 
				    $("#invoice_no").val(obj.invoice_no);
					$("#invoice").html(obj.invoice_no); 
				}
			}); 
	}
 
 $("#save_pricing").click(function(){ 
	var item=$("#item").val(); 
	var tenant_id=$("#tenants").val(); 
	var item_no=$("#item_no").val(); 
	var amount=$("#pricing_value").val(); 
	var description=$("#description").val(); 
	var priority=$("#priority").val();    
	var invoice_no=$("#invoice_no").val();    
	if(!item){ $("#status_msg").html("<font color='red'>Input invoice item name</font>"); $("#item").focus(); return false;}	
	if(!amount){ $("#pricing_value").focus(); return false;}	
	if(amount<=0){ $("#status_msg").html("<font color='red'>Item amount must be greater than 0</font>"); $("#pricing_value").focus(); return false;}	 
	if(!priority){ $("#priority").focus(); return false;}
    var my_url="<?=base_url();?>invoice/viewNewInvoice/";
	$.ajax({
		url:"<?=base_url();?>invoice/insert_invoice_item",
		type:"POST",
		data:
		{ 
			'tenant_id':tenant_id,
			'item':item,
			'amount':amount, 
			'description':description, 
			'priority':priority,
			'invoice_no':invoice_no 
		},
		success:function(data)
		{  
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				$("#status_msg").html("<font color='green'> Invoice Item added successfully</font>");
				$("#priority").val(parseInt(priority,10)+1);
				$("#item_no").val(parseInt(item_no,10)+1); 
				 show_pricing_details(invoice_no);
				 $("#item").val(''); $("#pricing_value").val(''); $("#description").val(''); 
				$("#invoice_link").html('<a href="'+my_url+obj.en_invoice_no+'" class="btn blue"> <i class="fa fa-check"> </i> Finish </a>'); 
			}
			else
			{
				$("#status_msg").html("<font color='red'> "+obj.msg+"</font>");  
			}
		}
	})
	
  });
  
  function show_pricing_details(invoice_no){
		var content="";	  
		$("#pricing_data").empty();
		var tenant_id=$("#tenants").val(); 
		$("#pricing_data").html("<tr> <td colspan='6' align='center'> <b>  Loading...please wait </b> </td> </tr>");
		$.ajax({
		url:'<?=base_url();?>invoice/get_invoice_details/'+invoice_no+"/"+tenant_id, 
		type: 'POST', 
		success:function (data)
		{ 
			var obj = JSON.parse(data); 
			var data = obj.data;
			if(obj.result=="ok")
			{ 
				
				var total=0; var total_deposit=0;
				for(var i=0; i<data.length; i++)
				{
					var pricing = data[i];
					var deposit=0;	
					 
					total=total+parseInt(pricing['payment_type_value']);  
					content=content+ "<tr>"+
						"<td>"+(parseInt(i)+1)+"</td><td>"+pricing['payment_type']+"</td>  <td> "+pricing['payment_type_value']+"</td>   <td> "+pricing['description']+"</td> <td class='center'> <a data-toggle='modal' href='javascript:;' onclick=\"edit_invoice_item('"+pricing['id']+"','"+pricing['invoice_no']+"','"+pricing['payment_type']+"','"+pricing['payment_type_value']+"','"+pricing['priority']+"','"+pricing['description']+"')\"><i class='fa fa-edit'></i> Edit </a></td> <td class='center'><a data-toggle='modal' onclick=\"delete_pricing('"+pricing['id']+"')\" href='#' class='btn red' > <i class='fa fa-trash-o'> </i> Remove </a></td>  </tr>";
				}
				
				$("#pricing_data").empty();
				$("#total_price").html(total);
				$("#pricing_total").html(total);
                $("#pricing_data").html(content); 
                $("#pricing_details").html(content);  
									
			}
			else
			{
				$("#pricing_data").html("<tr><td colspan='5'> No payment details added</td>  </tr>");
				$("#pricing_details").html("<tr><td colspan='5'> No payment details added</td>  </tr>");
			}
		}
	});
	return false;
}
 
 function delete_pricing(id)
 { 
	$("#del_item_id").val(id);
	$("#deleteItemModal").modal('show');	
	 
 }
 
function confirm_delete_item()
	{  
	var item_no=$("#item_no").val();
	var id=$("#del_item_id").val(); 
	var invoice_no=$("#invoice_no").val();
	 $.ajax({
		url:"<?=base_url();?>invoice/remove_invoice_item/"+id,
		type:"POST", 
		success:function(data)
		{  
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				$("#status_msg").html("<font color='green'> Invoice item removed successfully</font>");
				$("#item_no").val(parseInt(item_no,10)-1); 
				$("#deleteItemModal").modal('hide');
				show_pricing_details(invoice_no); 
			}
			else
			{
				$("#status_msg").html("<font color='red'> "+obj.msg+"</font>");  
			}
		}
	})
} 

function edit_invoice_item(id,invoice_no,payment_type,payment_type_value,priority,description)
 { 
	 $("#edit_id").val(id); 
	 $("#edit_item").val(payment_type); 
	 $("#edit_amount").val(payment_type_value); 
	 $("#edit_description").val(description); 
	 $("#edit_priority").val(priority);    
	 $("#edit_invoice_no").val(invoice_no);    
	 $("#editItemModal").modal('show');
	 
 }
 
 $("#done_edit").click(function(){
	  
 
	var id=$("#edit_id").val();  
	var item=$("#edit_item").val();  
	var invoice_no=$("#invoice_no").val();  
	var amount=$("#edit_amount").val(); 
	var description=$("#edit_description").val();    
	if(!item){ $("#status_msg").html("<font color='red'>Input invoice item name</font>"); $("#item").focus(); return false;}	
	if(!amount){ $("#pricing_value").focus(); return false;}	
	if(amount<=0){ $("#status_msg").html("<font color='red'>Item amount must be greater than 0</font>"); $("#pricing_value").focus(); return false;}	 
	 
	$.ajax({
		url:"<?=base_url();?>invoice/update_invoice_item",
		type:"POST",
		data:
		{  
			'id':id,
			'item':item,
			'amount':amount, 
			'description':description
		},
		success:function(data)
		{  
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				$("#error_edit").html("<font color='green'> Item updated successfully</font>"); 
				 show_pricing_details(invoice_no); 
			}
			else
			{
				$("#error_edit").html("<font color='red'> "+obj.msg+"</font>");  
			}
		}
	})
	
  });
    

</script> 