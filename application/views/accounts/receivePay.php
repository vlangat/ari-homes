<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		document.getElementById('save').disabled=false;	
		var tenant=$("#tenants").val();
		var pay_mode=$("#pay_mode").val();
		var receipt_no=$("#receipt_no").val();
		var amount=$("#amount").val();
		var pay_date=$("#pay_date").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
	//	if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		 if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
			document.getElementById('save').disabled=true;	 
	}
	function validate_pay()
	{   
		document.getElementById('receive').disabled=false;		
		var pay_mode=$("#pay_mode").val();
		var amount=$("#amount").val();
		var receipt_no=$("#receipt_no").val();
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>"); $("#amount").focus(); return false;}else{ $("#error2").empty();}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}else{ $("#error3").empty();}
		if(receipt_no=="")
		{
			if(pay_mode == "Cash"){   }else{$("#error5").html("<font color='red'> Receipt/Cheque No  required </font>");  $("#receipt_no").focus(); return false;} 
		}
		$("#error2").empty(); $("#error3").empty(); $("#error5").empty(); 
		document.getElementById('receive').disabled=true;
		return true; 
	}
	
</script>

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Rent  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Receive Payment</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title"  style="min-height:300px"> 
				<div class="col-md-12" style="background:#1bb968;padding:6px;">
					<font color="#ffffff"><strong> &nbsp;  Tenant Rent Payment </strong> </font> 
				</div> 
<div class="col-md-12">  &nbsp;  </div> 
<div class="row">
<div class="col-md-12">   
<div class="form-group"> 
<form action="<?=base_url();?>rent/receiveRent" method="post" onsubmit="return validate()">
<input class="form-control" type="hidden" value="" id="edit_id" name="edit_id"/>
		<label> Select Tenant </label><br/>
		<!--<select  class="selectpicker"  data-live-search="true" name="from"  onchange="getBalance(this.value)" id="tenants"  title="Select Tenant...">
		-->
		<select  class="selectpicker"  data-live-search="true" name="from"  onchange="this.form.submit()" id="tenants"  title="Select Tenant...">
			 <?php $rent_frequency=""; $priority=1;
			 foreach($tenants->result() as $row)
			 {?>
					<option value="<?=$row->id?>">
						<?php $company=$row->company_name; $rent_frequency=$row->rent_frequency;  
						$name=$row->first_name; if($company ==""){ echo $name.", ". $row->property_name.", ". $row->house_no; }else{ echo $row->company_name.", ". $row->property_name.",". $row->house_no;;}?>
					</option>
			<?php 
			} 
			 if($rent_frequency==0){ $rent_frequency="";}
			 ?>
		</select> <br/>
		<label id="error1">	 	</label>  
</form>
</div>
</div>  
 <?php 
 $tenant_property_name=""; $tenant_name="";$tenant_company_name="";$tenant_room_no=""; $tenant_floor=""; $tenant_mobile_number="";
 $tenant_email=""; $total=0; $tenant_id=""; $category_id="";
 //if(!empty($received_rent)){
 if(!empty($received_rent)){ 
	 foreach($selected_tenant->result() as $r)
	 {
			$tenant_name=$r->first_name;
			$tenant_id=$r->id;   $tenant_room_no=$r->house_no; $tenant_floor=$r->floor_no; 
			$tenant_email=$r->tenant_email; $tenant_mobile_number=$r->mobile_number; $tenant_property_name=$r->property_name;
			$category_id=$r->property_unit_id; $tenant_company_name=$r->company_name;
			if($r->tenant_type=="commercial"){ $tenant_name=$tenant_company_name; }
	}
  
 }   
 ?> 
<div class="col-md-12">   
<div class="col-md-6"> 
	<div class="col-md-12"> &nbsp; </div>
	<div class="col-md-12"  style="color:#ffffff;background:#32c5d2;padding:5px;min-height:40px">
		<label> &nbsp;  </label>
		<label>
		   <font size="3"><?php if($tenant_name !="" ){$tenant_name=$tenant_name.",";} echo $tenant_name . $tenant_property_name;?>  </font>
		</label>
	</div>
</div>
   
<div class="col-md-3">  
		<label> Balance Carried Forward </label>
			<input type="number" class="form-control" readonly name="prev_bal"   id="prev_bal"  value="0" min="1" >
		<label>  </label>  
</div> 	
<div class="col-md-3">  
			<label> Expected Amount Today </label>
			<input type="number" class="form-control" readonly name="expected_amount"   id="expected_amount"  value="0" min="1">
		 <label>  </label>
</div> 
 			
</div>	 
   
 
 
<form action="<?=base_url();?>rent/receive_pay" method="post" enctype="multipart/form-data"  onsubmit="return validate_pay()">
<div class="col-md-12"> 
<table class="table table-striped table-hover table-bordered"  id="ssample_editable_1">
<thead>
	<tr>
		<th> Items </th> 
		<th> Date </th>
		<!--<th> Description </th>--> 
		<th> Amount Due </th>   
		<th> Amount to Pay </th>   
		<th> Balance </th>   
	</tr>
</thead>
<tbody>
<?php  $i=1;  $total_items=0; $total=0;
if(!empty($rent_items)){
	foreach($rent_items as $row)
	{
	?>
	<tr>
	<td> <input type="checkbox" onchange="setTotalPay()" checked name="checkedItems[]" value="<?=$i?>" id="checkedItems<?=$i?>" /> &nbsp;
	<?=$row['payment_type']." ".$row['description'];?> 
	<input type="hidden" value="<?=$row['payment_type_value']?>" name="itemList_<?=$i?>" id="itemList_<?=$i?>"/>
	</td>
	<td> <?=date('d/m/Y')?>  </td>
	 <?php    $priority=$row['priority'];  $total=$total+$row['payment_type_value'];?> 
	 
	<td> <?=$row['payment_type_value']?> </td>
	<td>  	
		<input type="hidden" name="pay_item_<?=$i?>" value="<?=$row['payment_type'];?>">
		<input type="hidden" name="description_<?=$i?>" value="<?=$row['description'];?>">
		<input type="hidden" name="pricing_id_<?=$i?>" value="<?=$row['id'];?>">
		<input type="hidden" name="is_deposit_<?=$i?>" value="<?=$row['is_deposit'];?>">  
		<input type="number" min="0" max="<?=$row['payment_type_value']?>" class="form-control" onchange="setTotalPay('<?=$i?>')" onkeypress="return checkIt(event)" value="<?=$row['payment_type_value'];?>" maxlength="<?=$row['payment_type_value']?>" id="item_<?=$i?>" name="item_<?=$i?>">
	</td> 
	<td><font id="balance_<?=$i?>"> 0 </font></td>  
	  
	</tr>
	<?php 
	 
	$i++;
	}
$priority=$priority+1;	
$total_items=$i;
} 
if($total_items <1 && $tenant_id !=""){?>
	 <tr>  <td colspan="5" align="center"> <font color="red"> No Items to pay for, selected tenant has a Balance of KES 0 </font></td> </tr>
	 <?php } ?>
<tr> <td> Total </td><td> &nbsp;  </td> <td> <?=$total?> </td><td id="total"> <?=$total?> </td> <td id="total_balance">0 </td></tr>

</tbody>
</table>

</div>

 
<div class="col-md-3">  
			 <div class="form-group">
			 <font id="unlock"><a onclick="unlockInputs()"  class="btn green"> <i class="fa fa-edit"></i> Edit Item Amount  &nbsp;  </a> </font>
			</div>
					
</div>  
<div class="col-md-12">  	
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
		<font color="#ffffff"><strong> &nbsp;  Receive Payment </strong> </font> 
	</div> 
</div>  
<div class="col-md-12">  &nbsp; </div>

<div class="col-md-3">    
	<div class="form-group">
		<label> Amount Received</label>
			<input type="text" class="form-control" required name="amount" onchange="allocateAmount()"  onkeypress="return checkIt(event)"    id="amount"  value="" onchange="setData()">
			<input type="hidden" class="form-control" name="from" value="<?=$tenant_id?>" id="from"/>
			<input type="hidden" class="form-control" name="items_paid" value="<?=$i-1?>"/>
			<input type="hidden" class="form-control" name="tenant_id" value="<?=$tenant_id?>" id="tenant_id"/>
			<input   class="form-control" type="hidden" id="category_id" name="category_id" value="<?=$category_id?>">
			
			<label id="error2">  </label>
	</div>  
</div> 
<div class="col-md-3"> 
			<div class="form-group">
					<label> Payment Method </label>
					<select class="form-control"  name="pay_mode" id="pay_mode"  onchange="disable_method()" >
						<option value=""> Not selected </option>
						<option> Cash </option>
						<option> Cheque </option>
						<option> Mpesa </option>
						<option> Bank Receipt </option>
					</select>
				 <label id="error3">  </label>
			</div> 
	<p> </p> 
</div>				
<div class="col-md-3"> 
		 <div class="form-group">
					<label> Mpesa, Cheque or Receipt Number </label>
					<input type="text" class="form-control"   name="receipt_no"   id="receipt_no" onchange="validate_char('receipt_no')">
				 <label id="error5">  </label>
		 </div>
</div>
<div class="col-md-3"> 
<div class="form-group">
		<label> Bank Account</label> 
		<select class="form-control" name="bank" id="bank"> 
			<!--landlords lists here-->
		</select>
	 <label id="error6">  </label>
</div>
</div> 
</div>

<div class="row">
<div class="col-md-4"> 
	<label>  Attach a Receipt image  </label>
	<input type="file" name="userfile[]" class="form-control"> 
</div> 
<div class="col-md-12">  &nbsp; </div> 
<div class="col-md-12">   
	<button  type="submit" id="receive" class="btn red">Receive Rent</button> 	
  <p> &nbsp;  </p>
 </div>
<input type="hidden" name="edit_flag" id="edit_flag" value="0">  
<input type="hidden" name="edit_amount" value="0">  
</form>

</div>
 <?php 
// } 
 ?>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div> 
<!---Edit payment-->

 
<div id="add_new" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			 
			<h5><b> Add Other Items </b></h5>
			<hr/>
			<p>
			<label class="control-label">Item Name </label>
				<input   class="form-control" type="hidden" id="priority" value="<?=$priority+1?>">
				<input   class="form-control" type="text" placeholder="" id="item_name">
			</p>
			<p>
			<label class="control-label">Amount </label>
				<input   class="form-control" type="text"  onkeypress="return checkIt(event)"  value="" id="item_amount">
			</p> 
			<!--<p>
			<label class="control-label">Description </label>
				<input   class="form-control" type="text"  value="" id="description"/>
			</p> -->
			
			</div>
		</div> 
	</div> 
<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_new_one"> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>

  
<div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
<div class="modal-header">
	<b style="font-size:20px;color:green">Success Message </b> 
</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="err">
				   <?php if($this->session->flashdata('temp')){ echo $this->session->flashdata('temp');}  ?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<!--<button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> -->
		&nbsp;
	</div> 
</div>
 
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 
function validate_char(id)
{
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		$("#success").modal('toggle');  
        return false;
		}
    
}

$(document).ready(function () { 
  getBankDetails();
  checkPrivilege();
 
  var v="<?=$i?>";  
  for(var x=1; x<v;x++){  
  document.getElementById('item_'+x).disabled=true;
  
  }       
  var saving_success="<?php echo $this->session->flashdata('temp');?>";
  var tenant_id="<?php echo $tenant_id;?>";  
  var tenant_name="<?php echo $tenant_name;?>";
   
  if(tenant_name ==""){
	  document.getElementById('receive').disabled = true;  
	  document.getElementById('amount').disabled = true;  
	  }
  getBalance(tenant_id);
 if(!saving_success)
 { }
else{
		$("#data_saving_success").modal('toggle');
		setTimeout(function()
		{
			 $("#data_saving_success").modal('hide'); 
		}, 2000);  
	}
	
});
 
function disable_method()
{ 
	var value=document.getElementById("pay_mode").value;
	 if(value=="Cash")
	  {  
		document.getElementById('error5').value ="";
		document.getElementById('receipt_no').value ="--";
		document.getElementById('receipt_no').disabled = true;
		$('#bank').empty();
		document.getElementById('bank').disabled = true;  
	  }
	  else
	  {
			document.getElementById('receipt_no').value ="";
			document.getElementById('receipt_no').disabled = false;
			document.getElementById('bank').disabled = false;
			getBankDetails();
	  }  
}

function getBalance(val)
{   
$.ajax({
   url:"<?=base_url();?>rent/getTenantBalance/"+val,
   type:"POST", 
   async:false, 
   success:function(data)
   {  
	   var obj=JSON.parse(data);
	   if(obj.result=="ok")
	   {  		 
			 
			$("#expected_amount").val(obj.balance);	 	
			$("#prev_bal").val(obj.balance_forwarded);		
			$("#rent").val(obj.rent); 	
			//$("#expected_amount").val(obj.expected_pay);		
			$("#tenants").val(val);	
            		
	   }
	   else
	   {
		   
		   
	   }
   }

})

}
 
 
function setTotalPay(id)
{  

    var total_items="<?=$total_items?>";
	var total_a="<?=$total?>"; 
	total_items=parseInt(total_items);  
	total_a=parseInt(total_a);
	var a=0; var i=0; var count=0; 	 var v=0;
	for(i=1;i<total_items; i++)
	{ 
		v=parseInt($("#item_"+i).val()); 
		var checkedValue =$("#checkedItems"+i).is(':checked');
		if(checkedValue==true){  count=count+1; a=a+v; } 
	}   
	var b=0; var r=0; b=parseInt($("#itemList_"+id).val());	r=parseInt($("#item_"+id).val());
	var diff=0; diff=b-r;	r=$("#balance_"+id).html(diff); 
	$("#total").html(a);
	$('input[name="edit_amount"]').val(a);
	$("#itemsPaid").html(count);
	$("#amount").val(a); 
	$("#bal").html(total_a-a);
	$("#total_balance").html(total_a-a);
	var flag=$("#edit_flag").val();  
 /*  if(flag==0){
	 $("#edit_flag").val(1);
   }else{    $("#edit_flag").val(0);

   }	*/
	
}
 
function checkPrivilege()
 {  
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/3",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			 
			if(obj.add==0){ 
			   	document.getElementById('save').disabled=true;	  
			}
			if(obj.view==0){ 
			for(var x=1; x<i;x++){   $("#view_"+x).html("<font color='' onclick=\"alert('You have no privilege to view')\"> <a href='#' > View Receipt </a></font>"); }
              		  
			}
			  
			if(obj.delete==0){ 
			    
			   //document.getElementById('confirm').disabled=true;   
			}
		}
	 })
 }  
 
  function getBankDetails(){ 
     $("#msg").empty();  
		$.ajax({
		url:'<?=base_url();?>tenants/get_data/bank', 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data);  
			var data = obj.data; 
			 if(obj.result=="ok")
			{ 
				var name=""; 
				$("#bank").empty();
				$("#bank").append("<option value=''>Select Bank</option>");
				for(var i=0; i<data.length; i++)
				{
					 	var l = data[i];
						var acc_name=l['acc_name'];
						var owner=l['owner'];
						var acc_no=l['acc_no'];
						var bank_name=l['bank_name'];  
						$("#bank").append("<option value='"+l['id']+"'>"+acc_name+", "+acc_no+"</option>"); 
				
				}
			  
			} 
			 
		}
	}); 
	 
} 
 
 function unlockInputs()
 {
   var v="<?=$i?>";  
   var flag=$("#edit_flag").val();  
   if(flag==0){
	   for(var x=1; x<v;x++){ document.getElementById('item_'+x).disabled=false; }
       document.getElementById('amount').disabled=true;	   
   }else{
	    
		for(var i=1; i<v;i++){ document.getElementById('item_'+i).disabled=true; } 
		document.getElementById('amount').disabled=false;
   }	   
    if(flag==0){
	 $("#edit_flag").val(1);
   }else{    $("#edit_flag").val(0);

   }	 
   $("#unlock").html('<a onclick="unlockInputs()" class="btn green"> <i class="fa fa-edit"></i> Edit  Amount Received  &nbsp; </a> &nbsp;  &nbsp;  &nbsp; ');   
   setTotalPay();
 }
 
function allocateAmount()
{     
	var total_items="<?=$i?>";var total_a="<?=$total?>"; var amount=$("#amount").val(); var b=0;
	if(amount>=total_a){$("#total_balance").html((total_a-amount));}else{  $("#total_balance").html((total_a-amount)); }		
	for(i=1;i<total_items; i++){  
	var checkedValue =$("#checkedItems"+i).is(':checked');
	var f=$("#edit_flag").val();  
	if(parseInt(f)==1){
		$("#unlock").html('<a onclick="unlockInputs()" class="btn green"> <i class="fa fa-edit"></i> Edit  Amount Received  &nbsp; </a> &nbsp;  &nbsp;  &nbsp; ');   
	}
	if(checkedValue==true){   
		b=parseInt($("#itemList_"+i).val());   
		if(b<=amount)
		{ 
		  amount=amount-b; $("#item_"+i).val(b); $("#balance_"+i).html(0);	
		}else{  b=b-amount; $("#item_"+i).val(amount); amount=0; $("#balance_"+i).html(b);}
		 
		$("#total").html($("#amount").val());			
		} 
	} 
}
  
</script> 