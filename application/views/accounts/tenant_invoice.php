<script src="<?=base_url();?>js/jquery-1.12.4.js"></script> 
<script type="text/javascript" charset="utf8" src="<?=base_url();?>js/jquery.dataTables.min.js"></script>
  
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Rent</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Invoice </a>
		<i class="fa fa-circle"></i>
	</li> 
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="row">
		<div class="col-md-12">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit ">
	<div class="portlet-body">
		<div class="row" style="min-height:500px"> 
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"> List of Invoices</font>
		</div> 
		<div class="col-md-12"> &nbsp;  </div>	
	 
 
<form name="form1" method="post" action="<?=site_url();?>tenants/viewTenant" onSubmit="return validate();">
 <?php $bal=0;$x=1; $i=0;
?>
<table class="table table-striped table-hover table-bordered" id="invoice_tbl">
<thead>
	<tr>
	<th> No </th>
	<th> Inv No  </th>
	<th> Date  </th>
	<th> Tenant  </th>
	<th> Property </th>
	<th> Unit Name </th>
	<!--<th> House No </th> -->
	<th> Amount </th>
	<th> View  </th>
	<th> Edit </th> 
	<th> Delete </th> 
	</tr>
</thead>
<tbody id="tbody">
 <tr><td colspan='10' align='center'> <font color='green'>Loading data...please wait</font> </td></tr> 
		
</tbody>
</table>
<!-- <a data-toggle="modal"    class="btn red"  id="collect" href="" > Collect Rent </a>-->
</form>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
</div>
<!-- END CONTAINER -->


<div id="schedule_invoice" class="modal fade" tabindex="-1" data-width="500">
<div class="modal-header">
	<b style="font-size:20px;color:green">Schedule  Invoices </b> 
</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-4">  
			<div class="form-group">
				<label class="control-label"> Day to Generate</label>
				<select class="form-control" name="day" id="day_to_generate">
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
				<option>5</option>
				<option>6</option>
				<option>7</option>
				<option>8</option>
				<option>9</option>
				<option>10</option>
				<option>11</option>
				<option>12</option>
				<option>13</option>
				<option>14</option>
				<option>15</option>
				<option>16</option>
				<option>17</option>
				<option>18</option>
				<option>19</option> 
				<option>20</option>
				<option>21</option>
				<option>22</option>
				<option>23</option>
				<option>24</option>
				<option>25</option>
				<option>26</option>
				<option>27</option>
				<option>28</option> 
			</select>
			</div>  
				<p>  </p>
				</div>
				
				<div class="col-md-4"> 
					<div class="form-group">
					<label class="control-label"> Automatic</label>
						<select class="form-control" name="automatic" id="automatic">
							<option value="no">No</option>
							<option value="yes">Yes</option>
						</select>
					</div>
				</div>  
				<div class="col-md-3"> 
					<div class="form-group">
					<label class="control-label"> Email Invoice</label>
						<select class="form-control" name="email" id="email">
							<option value="no">No</option>
							<option value="yes">Yes</option>
						</select>
					</div>
				</div>
				
			</div>    
	</div>
	<div class="modal-footer" > <span id="err_schedule" style="float:left"> </span>
	<button type="submit" class="btn green" onclick="saveInvoiceSettings()"> Save Changes</button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
</div> 
</div>


<div id="generate_invoice" class="modal fade" tabindex="-1" data-width="400">
<div class="modal-header">
	<b style="font-size:20px;color:green">Generate New Invoices </b> 
</div>
	 <div class="modal-body">
				<div class="row">
					<div class="col-md-12">  
						<p>
						  Click Generate button below to generate invoices for the next month
						</p>
						<p id="err_invoice">  </p>
					</div>
				</div>    
	</div>
	<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" onclick="generateInvoice()"> Generate</button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div> 
</div>

 <!-- RESPONSIVE MODAL -->
<div id="rent_collection" class="modal fade" tabindex="-1" aria-hidden="true"  data-width="400">
  
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Rent Collection Details </b> </font></h4>
		  <span id="rent_for">  </font>
	</div>
<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12"> 
		<p>
			<label class="control-label"> Amount (KES) <font color='red'> * </font> </label> 
			<input   class="form-control"   type="text" id="amount" name="amount"> 
			<input   class="form-control"   type="hidden" id="id_no"> 
			<input   class="form-control"   type="hidden" id="name"> 
			<input   class="form-control"   type="hidden" id="property_name"> 
			<input   class="form-control"   type="hidden" id="floor_no"> 
			<input   class="form-control"   type="hidden" id="house_no">			
		</p>
	<p> 
		<label class="control-label">Payment Method  <font color='red'> * </font></label>
		<select class="form-control" name="mode" id="mode"> 
			<option> Cash </option>
			<option> Mpesa </option>
			<option> Cheque </option>
			<option> Bank Receipt </option>
		</select>
	</p>
  
		<p>
		<label class="control-label">Mpesa, Cheque, Bank receipt Number  <font color='red'> * </font> </label>
		<input   class="form-control" type="text" id="transaction_no" name="transaction_no"/> </p>
		<p>
 
		<p>
		<label class="control-label">Paid on</label>
		<input   class="form-control" type="text" id="date" placeholder="<?php echo date('Y-m-d');?>" value="<?php echo date('Y-m-d');?>" name="paid_on"> </p>
		<p>
 
		<p>
		<label class="control-label"> Description </label>
		 
		<textarea class="form-control" id="description" name="description" >  </textarea>
		</p>
</div>
</div>  <span id="status_msg">   </span>
</div>
<div class="modal-footer" >
		<input type="submit" class="btn green" id="save_data" value="Save">
		<button type="submit" class="btn red"> Save &amp; Print Receipt </button>
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div> 
</div>
</div>
 <!---END OF RESPONSIVE MODAL-->
 <div id="confirmModal" class="modal fade" style="min-height:220px">
		<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Confirm Invoice Removal</h4>
		</div>
		<div class="modal-body">
				<p> <font color="#006699"> Do you want to delete the selected Invoice? </font> </p>
				 <p id="err_remove">   </p> 
		</div>
		<div class="modal-footer">
			<input type="hidden" id="deleteId" value="">
			<input type="hidden" id="deleteStatus" value="0">
					<button data-dismiss="modal" id="confirmDelete" class="btn blue">Yes</button>
			<button class="btn default" data-dismiss="modal" aria-hidden="true">No</button>
		</div> 
</div>   
      
 
<script language="javascript">
$(function(){   
	get_invoice();
 });
 $(document).ready(
 function(){   
	//get_invoice();
 });
function validate()
{	
var formobj = document.forms[0];
var counter = 0;
for (var j = 0; j < formobj.elements.length; j++)
{
    if (formobj.elements[j].type == "checkbox")
    {
        if (formobj.elements[j].checked)
        {
            counter++;
        }
    }       
}
if(counter==0){  
    alert("Please select at least one.");
	return false;
 }
else if(counter>1)
{
 alert("Please select one tenant.");
	return false;	
}

	/*var chks = document.getElementsByName('checkbox[]');
	var hasChecked = false;
	for (var i = 0; i < chks.length; i++)
	{
		if (chks[i].checked)
		{
		hasChecked = true;
		break;
	}
	}
	if (hasChecked == false)
	{
	alert("Please select at least one.");
	return false;
	}*/
	 
	return true;
}
function deleteReceipt(id,status)
{  
   
$("#reason_error").html("");
$("#deleteId").val(id);
$("#deleteStatus").val(status);
$("#confirmModal").modal('toggle'); 

}
 
$(document).ready(function(){ 
 
 checkPrivilege();
 var i=("#countr").val(); 
 if(i<=0){ $("#view_tenant").hide(); }
 
	$("#collect").click(function(){ 
	validate();

	var id = [];
	$.each($("input[name='checkbox[]']:checked"), function(){            
	id=($(this).val());
	}); 
	 
		$.ajax({
		"url":"<?=base_url();?>tenants/getTennantDetails/"+id,
        "type":"post",
		"success":function(data)
			{
				var obj = JSON.parse(data); 
				var data = obj.data;
				for(var i=0; i<data.length; i++)
				{
					var tennant = data[i]; 
					$("#id_no").val(id);
					var name=tennant['first_name']+" "+tennant['middle_name']+" "+tennant['last_name'];
					var property=tennant['property_name'];
					var house=tennant['house_no'];
					var floor=tennant['floor_no'];
					$("#property_name").val(property);   
					$("#house_no").val(house);   
					$("#floor_no").val(floor);
					$("#rent_for").html(name); 
                                        if(tennant['first_name']||tennant['last_name']||tennant['middle_name']){  }else{name=tennant['company_name'];   $("#rent_for").html(name);}  
					$("#name").val(name); 

 
				}
 
				 if(id==""){return false;}else{
				        $("#rent_collection").modal('show');
                                     }
			}
	});
		
 
}); 	

 $("#save_data").click(function(){  
	 var id=$("#id_no").val();
	 var name=$("#name").val(); 
	 var amount=$("#amount").val(); 
	 var mode=$("#mode").val(); 
	 var transaction_no=$("#transaction_no").val(); 
	 var property_name=$("#property_name").val(); 
	 var floor_no=$("#floor_no").val(); 
	 var house_no=$("#house_no").val();
	 var business_no=0; 
	 var paid_on=$("#date").val();
	 var description=$("#description").val();
	 if(! amount || ! transaction_no || ! mode){  $("#status_msg").html("<font color='red'> Fill in all the required inputs </font>");  return false;}
	 $("#status_msg").html("<font color='blue'> Updating....</font>");
	$.ajax(
	{
		url:"<?=base_url();?>rent/payment/",
		type:"POST",
		async:false,
		data:
		{
		'id':id,
		'name':name,
		'transaction_no':transaction_no,
		'business_no':business_no,
		'property_name':property_name,
		'house_no':house_no,
		'floor_no':floor_no,
		'amount':amount,
		'mode':mode,
		'paid_on':paid_on,
		'description':description
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 { 
                          $("#transaction_no").val('');
		           $("#amount").val('');
		           $("#description").val('');
					 $("#status_msg").html("<i class='fa fa-check'> </i><font color='green'> Changes Saved </font>");
					 setTimeout(function(){
								 $('#rent_collection').modal('hide');
                                                                  $("#status_msg").empty();
                      }, 2000); 
			 }
			 else
			 {
				 //$("#status_msg").html(" <font color='red'> Not updated. You have made no changes to save </font>");
			 }
		}
		
	})
 });
 

 
});
  
   
 $("#confirmDelete").click(function(){ 
	var id=$("#deleteId").val();  
	var status=$("#deleteStatus").val();  
	$("#err_remove").html("<font color='red'> Please wait...</font>");
	$.ajax({
   url:"<?=base_url();?>invoice/remove_invoice/"+id+"/"+status,
   type:"POST",  
   async:false,  
   success:function(data)
   {  
	   var obj=JSON.parse(data);
	   if(obj.result=="ok")
	   {  		$("#err_remove").html("<font color='green'>Invoice Removed successfully!</font>");   
			 setTimeout(function(){
				$('#confirmModal').modal('hide');
				document.location.reload(true);	 
				  }, 2000); 
				 
	   } else{ return false;}
   } 
}); 
});
function getSettings()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>invoice/get_settings",
		type:"POST", 
		async:false,
		success:function(data)
		{  
			var obj=JSON.parse(data);  
			var data = obj.data;  
			  
			$("#day_to_generate").val(obj.day);
			$("#email").val(obj.email);
			$("#automatic").val(obj.automatic);
			
			 
			 
		}
	 })
 } 
 
 function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/2",
		type:"POST", 
		async:false,
		success:function(data)
		{  
			var obj=JSON.parse(data);  
			var data = obj.data;  
			 
			if(obj.view==0){ 
				document.getElementById('view_tenant').disabled = true;  
			}
			 
		}
	 })
 } 
 
 function generateInvoice()
 { 
	$("#err_invoice").html("<font color='blue'>Generating invoice...</font>");
	$.ajax({
		url:"<?=base_url();?>invoice/generateInvoice",
		type:"POST", 
		async:false,
		success:function(data)
		{  
			var obj=JSON.parse(data);  
			var data = obj.data;   
			if(obj.result=="ok")
			{ 
				$("#err_invoice").html("<font color='green'>Invoices generated successfully</font>");
				setTimeout(function(){
                            $("#err_invoice").empty(); 
							$('#generate_invoice').modal('hide');
							
                      }, 3000); 
					  //document.location.reload(true);
					  window.location.href = "<?=base_url();?>invoice/tenantInvoice";
			}
		   else{  $("#err_invoice").html("<font color='red'>Invoices not generated, invoice for next month exists!</font>"); }
		}
	 });
 } 
  
 function saveInvoiceSettings()
 { 
   var day_to_generate=$("#day_to_generate").val();
   var email=$("#email").val();
   var automatic=$("#automatic").val();
	$("#err_schedule").html("<font color='blue'>saving changes...</font>");
	$.ajax({
		url:"<?=base_url();?>invoice/update_schedule_settings",
		type:"POST", 
		async:false, 
		data:
		{
		'day':day_to_generate,
		'email':email,
		'automatic':automatic
		},
		success:function(data)
		{  
			var obj=JSON.parse(data);  
			var data = obj.data;   
			if(obj.result=="ok")
			{ 
				$("#err_schedule").html("<font color='green'>Invoices settings saved successfully</font>");
			 setTimeout(function(){
                            $("#err_schedule").empty(); 
							$('#schedule_invoice').modal('hide');
							
                      }, 3000);  
					//  window.location.href = "<?=base_url();?>invoice/tenantInvoice"; 
			}
		   else{  $("#err_schedule").html("<font color='red'>Failed to save invoice settings!</font>"); }
		}
	 });
 } 
 
function getChecked(){ 
 var formobj = document.forms[0];

var counter = 0;
for (var j = 0; j < formobj.elements.length; j++)
{
    if (formobj.elements[j].type == "checkbox")
    {
        if (formobj.elements[j].checked)
        {
            counter++;
        }
    }       
}

alert('Total Checked = ' + counter);
}

$(function(){
      getSettings();
		var x=document.URL;
		var y="<?=base_url();?>invoice/tenantInvoice#generate";
        if (x==y) { 
 		    $("#generate_invoice").modal({backdrop: "static"}); 
        }else{ 
		  
		var v="<?=base_url();?>invoice/tenantInvoice#schedule";
        if (x==v)
		{  
			$("#schedule_invoice").modal({backdrop: "static"}); 
		} 
		}
}); 
  
$(document).ready(function() {  
    $('#invoice_tbl').DataTable(/*{dom: 'Bfrtip',
	buttons: [
                     
        {
            extend: 'copyHtml5',
            exportOptions: {
                columns: [ 0, ':visible' ]
            }
        },
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: ':visible'
            }
        },
        {
            extend: 'pdfHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 5 ]
            }
        },{
            extend: 'print',
            exportOptions: {
                columns: [ 0, 1, 2, 5 ]
            }
        },
    ]}*/);
});

function get_invoice()
{ 
		var content=""; 
		$("#tbody").html("<tr><td colspan='10' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>");
		$.ajax({
		url:'<?=base_url();?>tables/invoice', 
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate', 
			'Pragma': 'no-cache', 
			'Expires': '0'
         },		
		type: 'POST', 
		async:false, 
		success:function (data)
		{  
			 var obj = JSON.parse(data); 
			var data = obj.data; var count=0; var count=parseInt(data.length);$("#countr").val(count);  
           if(count<=0){ $("#view_tenant").hide(); } 
			if(count >0){ 
				for(var i=0; i<data.length; i++)
				{
					
					var p = data[i];  var tenant_name=""; var floor="";  
					var enct=p['enct']; var enct_invoice=p['enct_invoice']; floor_no=p['floor_no'];  var status=parseInt(p['status']);
		            if(floor==0  || floor =="0"){ floor_no="";}else{ floor_no="Floor No "+p['floor_no'];}
					if(status ==1){ 
					var v="<td> <a data-toggle='modal' id='payment_history'  href='<?=base_url();?>invoice/viewNewInvoice/"+enct_invoice+"' class='btn green'> <i class='fa fa-eye'></i></a>"+
					"</td><td> <a data-toggle='modal' id='payment_history'  href='<?=base_url();?>invoice/viewNewInvoice/"+enct_invoice+"' class='btn default'> <i class='fa fa-edit'></i></a>"+
					"</td>"; 
					}
				else{ 
					var v="<td> <a data-toggle='modal' id='payment_history'  href='<?=base_url();?>invoice/viewTenantInvoice/"+enct+"' class='btn green'> <i class='fa fa-eye'></i>   </a> </td>"+
						"<td> <a data-toggle='modal' id='payment_history'  href='<?=base_url();?>invoice/viewTenantInvoice/"+enct+"' class='btn default'> <i class='fa fa-edit'></i>   </a></td>";
				  } 
			content=content+"<tr><td>"+(i+1)+"</td>"+
					"<td>"+p['invoice_no']+"</td> <td>"+p['date_generated']+"</td><td>"+p['first_name']+"</td> <td>"+p['property_name']+"</td><td> "+p['house_no']+" "+floor_no+"</td> <td>"+p['amount']+"</td>"+
					""+v+""+ 
					"<td class='center'> <a data-toggle='modal'onclick='deleteReceipt("+p['id']+")' href='javascript:;' class='btn red'><i class='fa fa-trash'> </i> Delete</a> </td> </tr>";
					count++;
				} 
				$("#tbody").html(content); 
			}
			else{
					$("#tbody").html("<tr><td colspan='10' align='center'>No tenants available</td></tr>");
				} 
			$('#invoice_tbl').DataTable(); 
		}
	});
 
}
</script>  