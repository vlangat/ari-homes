<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		var tenant=$("#tenants").val();
		var pay_mode=$("#pay_mode").val();
		var receipt_no=$("#receipt_no").val();
		var amount=$("#amount").val();
		var pay_date=$("#pay_date").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
	//	if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		 if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
	}
	
	function validate_edit()
	{    
		var name=$("#edit_user_name").val();
		var pay_mode=$("#edit_pay_mode").val();
		var amount=$("#edit_amount").val();
		var receipt_no=$("#edit_receipt_no").val();
		var pay_date=$("#edit_pay_date").val(); 
		if(name==""||name==null){ $("#edit_error1").html("<font color='red'> Tenant Name   is empty </font>");$("#edit_user_name").focus(); return false;}
		if(amount==""||amount==null){ $("#edit_error4").html("<font color='red'> Amount field is empty </font>"); $("#edit_amount").focus(); return false;}
		if(pay_date==""||pay_date==null){ $("#edit_error3").html("<font color='red'> Date field is empty</font>"); $("#edit_pay_date").focus(); return false;}
		//if(receipt_no==""||receipt_no==null){ $("#edit_error2").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>"); $("#edit_receipt_no").focus(); return false;}
		if(/^[a-zA-Z0-9- ]*$/.test(amount) == false){ $("#edit_amount").focus(); $("#edit_error4").html("<font color='red'>Amount contains illegal characters </font>");  return false; }else{$("#edit_error4").empty();}
		if(/^[a-zA-Z0-9- ]*$/.test(name) == false){ $("#edit_user_name").focus(); $("#edit_error1").html("<font color='red'>Name  should not have special characters </font>");  return false; } else{$("#edit_error1").empty();} 
		if(/^[a-zA-Z0-9- ]*$/.test(receipt_no) == false){ $("#edit_receipt_no").focus(); $("#edit_error2").html("<font color='red'>Receipt No should not have special characters </font>");  return false; } else{$("#edit_error2").empty();} 
	 return confirm("Update details for "+name+"?");
	}
</script>

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Rent  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Deposits</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  
<div class="row">
	
	<div class="col-md-12"   style="min-height:400px">  
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title">
				
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong>&nbsp; Deposits </strong> </font> 
	   </div>
	   <div class="col-md-12">  &nbsp;  </div>	
	 
<form action="<?=base_url();?>rent/deposits" method="post" >
						<div class="form-group"> 
						<input   class="form-control" type="hidden" value="" id="edit_id" name="edit_id">
								<label> Select Tenant </label><br/>
								<select  class="selectpicker"  data-live-search="true" name="tenant_id"  onchange="this.form.submit(this.value)" id="tenants"   >
									 <option value=""> None  </option>
									 <?php 
									 $tenant_id=""; $audit_number=1;
									 foreach($tenants->result() as $row)
									 {
									    foreach($refunded_deposits->result() as $r)
										 { 
											 if($row->id == $r->tenant_id){
												 $tenant_id=$r->tenant_id;  
											 break;
											 }  
										 } 
 									 if($row->id != $tenant_id){ ?>
										<option value="<?=$row->id?>">  
											<?php $company=$row->company_name; $name=$row->first_name; if($company ==""){ echo $name.", ". $row->property_name .", ". $row->house_no; }else{ echo $row->company_name .", ". $row->property_name .", ". $row->house_no;}?>
										</option>
									<?php
										}
									 }
									 ?>
								</select> <br/>
								<label id="error1"> </label>  
					</div>
						
</form>					
		 


 <?php $x=0; $total=""; $property_category_id=""; $property_id="";
 	  
if($deposits==""){
	   
 }else{
	$name=""; $property_name="";  $category_name=""; $mail=""; $phone=""; $room="";

	foreach($tenants->result() as $t)
	{ 
	if($t->id==$selected_tenant){
		$name=$t->first_name;
		$property_name=$t->property_name; 
		$property_id=$t->property_id; 
		 $category_name=$t->category_name;
		}		 
	}
	?>
	 
<?php echo '<table class="table table-striped table-hover table-bordered" >
<thead>
	<tr>
		 
		<th> Item </th>
		<th> Amount </th>   
	</tr>
</thead>
<tbody>';
 $property_category_id=$r->property_unit_id;
 foreach($deposits->result() as $d){
		 if($d->paid_amount >0)
		{
			$x++;
?>

<tr>  <td> <?=$d->payment_type?></td><td> <?=$d->paid_amount?> </td></tr>

 
		<?php
		$total=$total+$d->paid_amount;
		} 
  }
 
 if($x==0){
	 
	 echo '<tr>
	 <td colspan="2" align="center"> <font color="red" > No records found! It seems that the selected tenant has not cleared deposits </font> </td> </tr>';
 }
 else
 {
	 echo  '<tr><td> <strong> Total  </strong> </td>  <td><strong>'.$total.' </strong></td> </tr>';
 }
 
 
  
 
 ?>

</tbody>
</table>
	
<table class="table">
<tr>
<td>  
    
<strong> Amount Refunded</strong> 
<p><input   class="form-control" type="text" required name="amount" value="" onkeypress="return checkIt(event)"> 
<input   class="form-control" type="hidden" name="tenant_id" value="<?=$selected_tenant?>"> 
<input   class="form-control" type="hidden" name="property_id" value="<?=$property_id?>"> </p>

</td>
<td>
<p>
<div class="form-group">
						<br/>
	<button type="submit" class="btn red" <?=$disabled;?> id="save_changes"> Save Changes  </button>	

	</div>
	</p>
	 
</div>
</td>
</tr>
<tr>

<td colspan="2"> 
 <span id="error">  </span>
</td>
</tr>
</table> 
 
<?php } ?>

<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong> &nbsp;  Refunded Deposits </strong> </font> 
	   </div>
<div class="col-md-12">  &nbsp;  </div>	
	  
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr> 
		<th> # </th>
		<th> Date </th>
		<th> Tenant </th>
		<th> Property </th>
		<th> Unit No </th>
		<th> Total Deposits </th>
		<th> Returned amount </th>
		<th> Edit </th>   
		<th> Delete </th>   
	</tr>
</thead>
<tbody>
<?php $i=1;
  foreach($refunded_deposits->result() as $d){
	  $tenant_id=$d->tenant_id;
	  if($d->audit_number==2){ continue; }
	  ?>
<tr> <td> <?=$i?> </td> 
 <td> <?=$d->date_refunded?></td> 
<td> 
	<?php  
		 $t_name=$d->first_name;
		 if($d->tenant_type=="commercial"){ $t_name=$d->company_name; }
		  $t_unit=$d->house_no;
		 
 
	?>
	<?=$t_name?> 
</td>
<td>	<?=$d->property_name?> </td>
<td>	<?=$d->house_no?> </td>
<td> 
 
<?php
  $total_d=0; 
  
	foreach($tenant_deposits->result() as $dep){
		if($dep->tenant_id==$tenant_id)
		{
			$x++; 
			$total_d=$total_d+$dep->payment_type_value;
		}  
 
  }
 
  ?>
<?=$total_d;?>
</td><td> <?=$d->amount?> </td> 
<td> <a href="javascript:;" onclick="edit_deposit('<?=$d->no?>','<?=$d->amount?>','<?=$t_name?>')"> <i class="fa fa-edit"> </i> Edit </a> </td>
<td> <a href="javascript:;" onclick="remove_deposit('<?=$d->no?>')"> <i class="fa fa-edit"></i>  Remove </a> </td>
</tr> 

<?php  $total_d=0;
 $i++;
 } ?>
</tbody>
</table>
 
</div>
<p>  </p>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>  
</div>  
</div>   
<!---Edit payment-->


<!--->
 <!-- responsive -->
<div id="edit_deposit_modal" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			 
			<h5><b> Edit Deposit  </b></h5>
			<hr/>
			<p><label class="control-label">Tenant Name </label> 
			<input   class="form-control" type="text" readonly required id="edit_name" value=""> </p>
			<p>
			<label class="control-label">Amount</label>
			<input   class="form-control" type="text"  id="d_amount" value="" onkeypress="return checkIt(event)">  
			<input   class="form-control" type="hidden"  id="edit_deposit_id" value=""> 
			<p>
			</div>
		</div> 
	</div> 
<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="done_edit" <?=$disabled;?>> Save </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
</div>
</div>


<div id="success" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b style="font-size:20px;color:green">    Warning Message </b></h5>
				<hr/>
				<p id="success_msg">
				   
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
 <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">    Success Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="">
				   <?php if($this->session->flashdata('temp')){ echo $this->session->flashdata('temp');}  ?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<!--<button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> -->
		&nbsp;
	</div> 
</div>
 
 
<div id="delete_confirmation" class="modal fade" tabindex="-1" data-width="400">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b> Remove Deposit </b></h5>
				<p id="del_status"> Confirm removal of this deposit? </p>
				<input type="hidden" id="confirm_id">
			</div>
		</div>
	</div>
<div class="modal-footer" >
	<button type="button" class="btn green" id="confirm_del" <?=$disabled?>> Yes </button>
	<button type="button" data-dismiss="modal"  class="btn red"> No </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>
 
 
 <div id="deposit_confirmation" class="modal fade" tabindex="-1" data-width="400">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b> Confirm Deposit</b></h5>
				<p> This Tenant has Balance of Ksh. <font id="deposit_bal"></font>. Do you want to refund deposit? </p>
				 
			</div>
		</div>
	</div>
<div class="modal-footer" > 
	<button type="button" data-dismiss="modal"  class="btn green"> Yes </button>
	<button type="button"  class="btn red" id="cancel"> No </button>
 </div>
</div>
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 
function validate_char(id)
{
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		$("#success").modal('toggle');  
        return false;
		}
    
}

 function edit_deposit(id,value,name)
  { 
		$("#d_amount").val(value);
		$("#edit_deposit_id").val(id);
		$("#edit_name").val(name);
		$("#edit_deposit_modal").modal('show');
	  
  }
  
  function remove_deposit(id)
  {  
		$("#confirm_id").val(id);
		$("#delete_confirmation").modal('show'); 
  } 
  
$(document).ready(function (){ 
 var t="<?php echo $selected_tenant;?>";   
 var x="<?=$x;?>"; 
  if(x==0){
	  //document.getElementById('save_changes').disabled=true;
}	
 getBalance(t);
  $("#tenants").val(t);  
  checkPrivilege();
 
  var saving_success="<?php echo $this->session->flashdata('temp');?>";
 if(!saving_success)
 { }
else{
	$("#data_saving_success").modal('toggle');
	setTimeout(function()
						{
						 $("#data_saving_success").modal('hide'); 
						}, 2000);  
	}
	
	$("#done_edit").click(function(){ 
	var amount=$("#d_amount").val();
	var id=$("#edit_deposit_id").val();
 if(!amount){ $("#status_message").html("<font>   Amount field cannot be empty </font>"); $("#d_amount").focus();  return false;}
  $.ajax(
  {
		url:"<?=base_url();?>financial/edit_deposit",
		type:"POST",  
		data:{
			'id':id,
			'amount':amount
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#status_message").html("<font color='green'> Edit done successfully </font>");
				  setTimeout(function(){
								$('#edit_deposit_modal').modal('hide');
                               $("#status_message").empty();
                      }, 2000); 
					 // document.location.reload(true);
					  window.location="<?=base_url();?>rent/deposits";
			 }
			 else{
				 $("#status_message").html("<font color='red'> Not edited. Try again later </font>");
			 }

		}			 
  })
 
 });

 
 $("#save_changes").click(function(){     
	var property_id=$("input[name='property_id']").val();  
	var tenant_id=$("input[name='tenant_id']").val();
	var amount=$("input[name='amount']").val(); 
	if(!amount){ $("#error").html("<font color='red'>  Enter the amount to refund </font>"); $("input[name='amount']").focus(); return false;}
 $.ajax(
  {
		url:"<?=base_url();?>rent/refund_deposits",
		type:"POST",  
		data:{
			'tenant_id':tenant_id,
			'property_id':property_id,
			'amount':amount 
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#amount").val('');
			       $("#error").html("<font color='green'>  "+obj.msg+" </font>");
				 setTimeout(function()
				{ 
					window.location="<?=base_url();?>rent/deposits";
                },3000); 
			 }
			 else
			 {
				$("#error").html("<font color='red'>   "+obj.msg+"  </font>"); 
			 }
		}
  })
 });
 
$("#cancel").click(function(){  
window.location="<?=base_url();?>rent/deposits";
});

$("#confirm_del").click(function(){  
var id=$("#confirm_id").val();
 $.ajax(
  {
		url:"<?=base_url();?>financial/remove_deposit",
		type:"POST",  
		data:{
			'id':id 
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#status_message").html("<font color='green'> Deposit removed successfully </font>");
				  setTimeout(function(){
								//$('#edit_deposit_modal').modal('hide');
                               $("#status_message").empty();
                      }, 2000); 
					  //document.location.reload(true);
					  window.location="<?=base_url();?>rent/deposits";
			 }
			 else{
				 $("#status_message").html("<font color='red'> Not removed. Try again later </font>");
			 }

		}			 
  })
 
 });
 
 });
  
 
function getBalance(t)
 {  
	$.ajax({
		url:"<?=base_url();?>rent/getTenantBalance/"+t,
		type:"POST",
		sync:false,
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				if(obj.balance>0)
				{
					$("#deposit_bal").html(obj.balance); 
					$("#deposit_confirmation").modal("show"); 
				}
				else
				{
				 
				}
			}
		}
		
	});
	
 }
 
 function checkPrivilege()
 {  
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/3",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			 
			if(obj.add==0){ 
			   	document.getElementById('save').disabled=true;	  
			}
			if(obj.view==0){ 
			//for(var x=1; x<i;x++){   $("#view_"+x).html("<font color='' onclick=\"alert('You have no privilege to view')\"> <a href='#' > View Receipt </a></font>"); }
              		  
			}
			  
			if(obj.delete==0){ 
			    
			   //document.getElementById('confirm').disabled=true;   
			}
		}
	 })
 } 
</script> 