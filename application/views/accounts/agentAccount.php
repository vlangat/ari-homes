 <?php foreach($data->result() as $row) {} ?>
 <?php foreach($company->result() as $companyData) {} ?>
 <div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="#">Account</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>User Profile</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-md-12">
<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar">
<!-- PORTLET MAIN -->
<?php //foreach($profile as $rows){}?>
<div class="portlet light profile-sidebar-portlet ">
	<!-- SIDEBAR USERPIC -->
	<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				<img src="<?=base_url();?>images/<?php echo $this->session->userdata('photo');?>" class="img-responsive" alt="profile pic" >
 
				</div>
				<div class="profile-usertitle">
				<div class="profile-usertitle-name">
			 <?=ucfirst(strtolower($row->first_name))?> <?=ucfirst(strtolower($row->last_name))?> 
			   
		</div>
				<!--<div class="profile-usertitle-job"> <?//=$row->role?> </div>-->
				 <div class="profile-usertitle-job"><font size="1"><?=$row->company_name?> </font></div> 
			</div>
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li>
						<a data-toggle="modal" href="#responsive_2">
							Add Users
					</a>
					</li>
					 
				</ul>
			</div>
			<!-- END MENU -->
	<!-- SIDEBAR MENU -->
 
</div>
<!-- END PORTLET MAIN -->
</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
	<div class="col-md-12">
		<div class="portlet light ">
			<div class="portlet-title tabbable-line">
				<div class="caption caption-md">
					<i class="icon-globe theme-font hide"></i>
					<span class="caption-subject font-blue-madison bold uppercase"> Company  </span>
				</div>
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
					</li>
					<li>
						<a href="#tab_1_4" data-toggle="tab"> Company Details </a>
					</li>
					<li>
						<a href="#tab_1_2" data-toggle="tab"> Profile Pic </a>
					</li>
					<li>
						<a href="#tab_1_3" data-toggle="tab"> Change Password </a>
					</li>
					<li>
						<a href="#tab_1_5" data-toggle="tab">  Users  </a>
					</li> 
				</ul>
			</div>
<div class="portlet-body">
<div class="tab-content">
<!-- PERSONAL INFO TAB -->
<div class="tab-pane active" id="tab_1_1">
<?php //foreach($profile as $row){  }?>
<?php// echo form_open('Auth/userProfile');?> 
<!--<form role="form" method="post" action="Auth/userProfile">-->
<?php  foreach($data->result() as $row){  }?>
<div class="form-group">
<label class="control-label">First Name </label>
<input type="text"   class="form-control" id="first_name" value="<?=$row->first_name?>" /> </div>
<div class="form-group">
<label class="control-label">Last Name</label>
<input type="text"   class="form-control" id="last_name"  value="<?=$row->last_name?>"/> </div>
<div class="form-group">
<label class="control-label">Mobile Number</label>
	<input type="text" onkeypress="return checkIt(event)" maxlength="11" value="<?=$row->phone?>" class="form-control"  id="phone"/> 
</div> 
<div class="margiv-top-10">
<input type="submit"  id="update_details"class="btn green" value=" Save Changes">

</div>
</form>
</div>
<!-- END PERSONAL INFO TAB -->
<!-- CHANGE AVATAR TAB -->
<div class="tab-pane" id="tab_1_2">
<?php echo form_open_multipart('auth/changePhoto/company_details');?>
<!-- <form action="#" role="form">-->
<div class="form-group">
<div class="fileinput fileinput-new" data-provides="fileinput">
	<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
		<img src="<?=base_url();?>images/<?php echo $this->session->userdata('photo');?>" alt="" /> </div>
	<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
	<div>
		<span class="btn default btn-file">
			<span class="fileinput-new"> Select image </span>
			<span class="fileinput-exists"> Change </span>
			<input type="file" name="photo"  accept="gif|jpg|png"> </span>
		<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
	</div>
</div>
</div>
<div class="margin-top-10">
<input type="submit" class="btn green" value="Upload"/>
<a href="javascript:;" class="btn default"> Cancel </a>
</div>
<?php echo form_close();?>
</div>
<!-- END CHANGE AVATAR TAB -->
<!-- CHANGE PASSWORD TAB -->
<div class="tab-pane" id="tab_1_3">
<?php //echo form_open_multipart('Auth/changePass');?>
<form action="#">
		<div class="form-group">
			<label class="control-label">Current Password</label>
			<input type="password" id="curr_password" class="form-control" /> </div>
		<div class="form-group">
			<label class="control-label">New Password</label>
			<input type="password" id="new_pass" class="form-control" /> </div>
		<div class="form-group">
			<label class="control-label">Re-type New Password</label>
			<input type="password" id="confirm_pass" class="form-control" /> </div>
		<div class="margin-top-10">
			<a href="javascript:;" class="btn green" id="change_pass"> Change Password </a>
			<a href="javascript:;" class="btn default"> Cancel </a>
		</div>
	</form>
</div>
<!-- END CHANGE PASSWORD TAB -->

<div class="tab-pane" id="tab_1_4">
	<!-- BEGIN EXAMPLE TABLE PORTLET-->
<?php echo form_open_multipart('auth/updateCompanyDetails');?>
<div class="portlet light portlet-fit ">
<div class="form-group">
		<label class="control-label">Company Name</label>
			<input type="text"   value="<?=$companyData->company_name?>" class="form-control" name="company_name" />
 </div>
<div class="form-group">
	<label class="control-label"> Email Address</label>
	<input type="email"   value="<?=$companyData->email?>" class="form-control" name="email" /> 
</div>
<div class="form-group">
<label class="control-label">Country</label>
<select class="form-control" name="country"> 
	<option> <?=$companyData->country?> </option>
	<option> Kenya   </option>
	<option> Uganda   </option>
	<option> Tanzania   </option>
</select>
</div>
<div class="form-group">
<label class="control-label">Agency City/Town</label>
<select class="form-control" name="town"> 
	<option>  <?=$companyData->town?>  </option>
	<option>  Nairobi  </option>
	<option>  Mombasa  </option>
	<option>  Dare salaam  </option>
	<option>  Kampala </option>
</select>

</div>
<!--<div class="form-group">
	<label class="control-label">Phone Number</label>
	<input type="text" onkeypress="return checkIt(event)" maxlength="10"placeholder="0710 030 000"value=" "class="form-control"  name="phone"/> 
</div>-->
<div class="form-group">
	<label class="control-label">Mobile Number</label>
	<input type="text" onkeypress="return checkIt(event)" maxlength="15"placeholder="0712 345 678" value="<?=$companyData->phone?>" class="form-control"  name="phone"/> 
</div>

<div class="form-group">
<label class="control-label">Agency Logo</label>
	<div class="fileinput-new thumbnail"  style="width: 100px; height: 80px;">
		<img  src="<?=base_url();?>images/<?=$companyData->photo?>"  alt="Logo" /> 
	</div>
	<div>
			<input type="file"  name="photo" value=""> 
	</div>
 
</div>
<div class="margiv-top-10">
<input type="submit" class="btn green" value=" Save Changes">

</div>
<!--</form>-->
<?php echo form_close();?>
</div>
	</div>
	
	<div class="tab-pane" id="tab_1_5">
										   <!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet light portlet-fit ">
					
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="btn-group">
									   
										<a data-toggle="modal" href="#responsive_2" class="btn green"> Add New User
											<i class="fa fa-plus"></i>
											</a>
									</div>
								</div>
							</div>
						</div>
						<!--<table class="table table-striped table-hover table-bordered" id="sample_editable_1">-->
						<table class="table table-striped table-hover table-bordered"  >
							<thead>
								<tr>
									<th> First Name </th>
									<th> Last Name </th>
									<th> Phone </th>
									<th> Email </th>
									<th> Status </th>
								</tr>
							</thead>
								<tbody  id="agents"> 
								
								</tbody>
						</table>
					</div>
				</div>
			</div>
</div>
 
</div>
<p id="error">    </p>
    
        <?php if($this->session->flashdata('temp')){
				$msg=$this->session->flashdata('temp');
				echo '<div class = "alert alert-success alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button> <font color="green">'. $msg. '</font> </div>'; 	
				} 	
		?>

</div>
</div>
</div>
</div>
</div> 
<!-- END PROFILE CONTENT -->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div> 
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
 <div id="responsive_2" class="modal fade" tabindex="-1" aria-hidden="true">
<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" id="task">Add User </font></h4>
	</div>
<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div  class="form-horizontal">
				<div class="form-group">
					<div class="col-md-12">
					First Name:
					<input type="text" id="fname"class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off"required="required" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					Second Name:
					<input type="text" id="lname"class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off"required="required" />
					</div>
				</div>
			<div class="form-group">
			<div class="col-md-12">
				Email:
				<input type="email" id="email"class="form-control todo-taskbody-tasktitle" required="required" autocomplete="off"required="required" placeholder="Email"/>
			</div>
	</div>
		<div class="form-group">
			<div class="col-md-12">    
				</div>
			<input type="hidden" id="added_by"class="form-control todo-taskbody-tasktitle"value="<?php echo $this->session->userdata('id');?>" placeholder="">
		</div>
	</div>
	</div>
</div>
</div><font id="add_user"> </font>
<div class="modal-footer"> 
<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
<input type="submit" id="add_new_user" class="btn green"value="Save Changes">
</div>   
</div>
</div> 
<script type="text/javascript">
function checkPass()
{
var pass1=document.getElementById("pass1");   
var pass2=document.getElementById("pass2");
var message=document.getElementById('confirmMessage');   
if(pass1.value==pass2.value)
{
//pass2.style.backgroundColor = '#66cc66';
pass2.style.backgroundColor = 'lightblue';
message.style.color = 'green';
message.innerHTML="Password match <img src='../images/tick.jpg' height='15'width='15'/>"	
}
else
{
	pass2.style.backgroundColor ='#ff6666';
	message.style.color = 'red';
	message.innerHTML="Password do not match <img src='../images/error.jpg' height='10'width='15'/>"
}

} 
</script>
 

<SCRIPT LANGUAGE="JavaScript">
$(function(){
	getAgents();
$("#update_details").click(function()
	{  
		var fname=$("#first_name").val();
		var lname=$("#last_name").val();
		var company=$("#company").val();  	  	
		var phone=$("#phone").val();   
		if(fname&&phone&&lname){ } else{$("#error").html("<font color='red'> Empty fields not required </font>"); return false;}
		$("#error").html("<font color='blue'> Saving please wait...</font>");
		$.ajax({
		 url:"<?=base_url();?>auth/updateProfile",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'lname':lname, 
		   'phone':phone, 
		   'company':company 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){ 
				 $("#error").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>Profile details updated successfully  </font>"); 
                getAgents();	 
			 }
		else{
				$("#error").html("<font color='red'> Error occured. Check if details provided are correct </font>");return false;
			}
		 }
		 
		  }
		 )	  
});

$("#change_pass").click(function()
	{  
	 
	var curr_pass=$("#curr_password").val();
	var new_pass=$("#new_pass").val();
	var confirm_pass=$("#confirm_pass").val();  	
	if(curr_pass||new_pass||confirm_pass){ } else{$("#error").html("<font color='red'> Empty fields not required </font>"); return false;}
	if(new_pass == confirm_pass){ }else{  $("#error").empty();$("#error").html("<font color='red'> Password does not match. Try again </font>");  $("#pass2").val('');$("#pass2").focus(); return false;}
	if(curr_pass == new_pass){ $("#error").empty();$("#error").html("<font color='red'> New Password cannot be same as old password.</font>");  $("#pass2").focus(); return false; }else{  }

	  $.ajax({
	 url:"<?=base_url();?>auth/changePassword/",
	 type:"POST",
	 async:false,
	 data:
	 {
	   'curr_password':curr_pass,
	   'new_pass':new_pass 
	 },
	 success:function(data)
	 {
		 var obj=JSON.parse(data);
		 if(obj.result=="ok"){
			 $("#confirm_pass").val('');$("#new_pass").val(''); $("#curr_password").val('');
			 $("#error").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'> Password changed successfully  </font>"); 
				 
		 }
		 else{
			 
			 $("#error").html("<font color='red'> Password not changed. Check if current password is correct</font>");return false;
		 }
	 }
	 
	  }
	 )	  
});

$("#add_new_user").click(function()
	{  
		var email=$("#email").val();
		var fname=$("#fname").val();
		var lname=$("#lname").val();  	  	
		if(email&&fname&&lname){ } else{$("#add_user").html("<font color='red'> Empty fields not required </font>"); return false;}
		var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#add_user").html("<font color='red'> Email provided is not a valid e-mail address"); return false;}
		$("#add_user").html("<font color='blue'> Adding new user....</font>");
		$.ajax({
		 url:"<?=base_url();?>auth/addUser",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'lname':lname, 
		   'email':email 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){
				 $("#fname").val('');$("#lname").val(''); $("#email").val('');
				  
				 $("#add_user").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>"+obj.msg+"  </font>"); 
                getAgents();	 
			 }
			 else{
				 $("#add_user").html("<font color='red'> "+obj.msg+"</font>");return false;
			 }
		 }
		 
		  }
		 )	  
});

});

function getAgents()
{
	var i=0;
	$.ajax({
		url:"<?=base_url();?>auth/loadAgents",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				 var content="";
				 $("#agents").empty();
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					var status="";
					var agent=data[i]; 
					 if(agent['user_set_status']==1){ status="<font color='green' title='Click to Deactivate'>Active</font>";} else{ status="<font color='red' title='Click to activate'> Deactivated </font>";}
					content=content+"<tr><td> "+agent['first_name']+"</td> <td> "+agent['last_name']+"</td><td> "+agent['phone']+"</td><td> "+agent['email']+"</td><td><a   onclick=\"remove_user("+agent['id']+","+agent['user_set_status']+")\"> " +status+" </a></td></tr>";
					
				 }
				 $("#agents").html(content); 
			}
		}
		
		
	})
	
}

function checkIt(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        status = "This field accepts numbers only."
        return false
    }
    status = ""
    return true
}
</SCRIPT>
