<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper"> 
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
	<div class="container">
		<!-- BEGIN PAGE BREADCRUMBS -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Property</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Landlord Details </span>
			</li>
	</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row"> 
<div class="col-md-12">
<!-- BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
	<div class="row">
		<div class="col-md-12">
<div class="portlet light ">		 
<div class="portlet-body">
	<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
<div class="tab-pane active" name="tab_1_1">
<?php if($msg !="")
{
/*echo '<div class = "alert alert-success alert-dismissable" style="background:lightgreen">
   <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	  &times;
   </button>
	
   <font color="#006699">'. $msg. '</font>
</div>'; 	*/
}
?>
 

<div class="row"> 
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff">  Landlord Section </font>
	</div>		
 
<div class="col-md-12">  &nbsp;   </div>	
<div class="col-md-12"><button class="btn red" onclick="add_new_landlord()"> <i class="fa fa-plus"> </i> Add New Landlord </button>
 <hr/>   </div>	
 <div class="row"> 
<div class="col-md-12">
<div class="col-md-6">
<div class="form-group">
		<label class="control-label">Select Landlord </label>
		   <select class="form-control" name="landlord" required id="landlord"> 
		   <option value=""> None  </option>
					<?php foreach($landlords->result() as $row):?> 
					<option value="<?=$row->id ?>"><?=ucfirst(strtolower($row->full_name))?>  </option>
					<?php endforeach;?> 
		   
			</select>
			<label id="error1">  </label>
</div>

</div>
<div class="col-md-6">
	<div class="form-group">
			
			<input type="hidden" id="id_no" value="">
			<label class="control-label"> Assign Property </label>
			<select class="form-control" required name="property_id" id="property_id">
					<option value=""> None  </option>
					<?php foreach($property->result() as $row):?> 
					<option value="<?=$row->id ?>"><?=$row->property_name?>  </option>
					<?php endforeach;?> 
			</select>
			<label id="error2">  </label>
	</div>  
	 
</div>
 <div class="col-md-12">
 
 <input type="submit" <?=$disabled?> value="Save Details"  id="save" class="btn green"  /> 
</div>
 
</div>

</div> 
</div>
 
<hr/>
<div class="col-md-12"> &nbsp;  </div>
<table  class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>  
							<th> #  </th>
							<th> Landlord </th> 
							<th> Id No </th> 
							<th> Phone </th> 
							<th> Email </th> 
							<th> View Properties </th>  
							<th> Edit </th> 
							<th> Remove </th>  
						</tr>
					</thead>
					<tbody>
					<?php $i=1;foreach($landlords->result() as $row){
						//$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
						//$enct =base64_encode(do_hash($row->id . $salt,'sha512') . $salt);
						$landlord_id = $this->encrypt->encode($row->id);		
		              $enct=str_replace(array('+', '/', '='), array('-', '_', ''), $landlord_id);
		    
						?>
						<tr>
						<td><?=$i?></td>
						<td> <?=ucfirst(strtolower($row->full_name))?> </td> 
						<td> <?=$row->id_no?> </td> 
						<td> <?=$row->phone?> </td> 
						<td> <?=$row->email?> </td> 
						<td> <a href="<?=base_url();?>tenants/landlord_property/<?=$enct?>"><i class="fa fa-file-o"> </i> View Properties</a> </td> <td> <a href="javascript:;" onclick="getLandlordDetails('<?=$row->id?>')" ><i class="fa fa-edit"> </i> Edit </a> </td> <td> <a href="javascript:;" onclick="delete_landlord('<?=$row->id?>')" ><i class="fa fa-trash"> </i> Remove </a> </td> </tr> 
					<?php  $i++; } 
					?>						
					</tbody>
				</table>
  
 
</div>
</div>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
</div>
</div>
<!-- END CONTAINER --> 

 <div id="landlord_details" class="modal fade" tabindex="-1" data-width="800">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> <i class="fa fa-plus"></i> Add New Landlord </b></h5>
				<hr/>
 
<div class="col-md-12">  
<input type="hidden" name="landlord_id" value="">
<div class="form-group">
			<label class="control-label"> Full Name </label>
			<input type="text" required  class="form-control"   name="landlord_fname" id="First Name" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"   onchange="checkChars('First Name')"/> 
		</div>

</div>	
<!--<div class="col-md-4">  
<div class="form-group">
			<label class="control-label"> Middle Name </label>
			<input type="text"  required class="form-control" placeholder="Middle Name (optional)" name="landlord_mname" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Middle Name" onchange="checkChars('Middle Name')"/> 
		</div>

</div>	
 <div class="col-md-4">  
<div class="form-group">
			<label class="control-label">Last Name</label>
			<input type="text" required  class="form-control"   name="landlord_lname" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Last Name" onchange="checkChars('Last Name')"/> 
		</div>

</div>	-->
 
 <div class="col-md-4">  
<div class="form-group">
			<label class="control-label">Id Number</label>
			<input type="text" onkeypress="return checkIt(event)"  required class="form-control" maxlength="8" name="landlord_id_no" id="landlord_id_no" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Id Number" onchange="checkChars('Id Number')"/> 
		</div>

</div>
 <div class="col-md-4">  
<div class="form-group">
			<label class="control-label">Email Address</label>
			<input type="email" required  class="form-control"  placeholder="Email Address (optional)" name="landlord_email" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Email" onchange="return validateMail()"/> 
		</div>

</div>
 <div class="col-md-4">  
<div class="form-group">
			<label class="control-label"> Phone Number</label>
			<input type="text"  onkeypress="return checkIt(event)"  required maxlength="13" class="form-control" name="landlord_phone" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Phone Number" onchange="checkChars('Phone Number')"/> 
		</div>

</div>

 <div class="col-md-4">  
<div class="form-group">
			<label class="control-label"> Bank Acc No</label>
			<input type="text" onkeypress="return checkIt(event)"   placeholder="Bank Acc No (optional)" required class="form-control" name="bank_acc" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Bank Account" onchange="checkChars('Bank Account')"/> 
		</div>

</div>

<div class="col-md-4">  
<div class="form-group">
			<label class="control-label"> Bank Name </label>
			<input type="text"  placeholder="Bank Name (optional)" required class="form-control" name="bank_name" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Bank Name" onchange="checkChars('Bank Name')"/> 
		</div>

</div>
<div class="col-md-4">  
	<div class="form-group">
				<label class="control-label"> Branch</label>
				<input type="text" required   placeholder="Branch (optional)" class="form-control" name="bank_branch" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Branch" onchange="checkChars('Branch')"/> 
	</div>

</div>
 <div class="col-md-12"> <p id="msg">  </p> </div>
				</div>
				
			</div>
			
	</div>
	<div class="modal-footer"> 
		<center>  
			<button type="submit"   class="btn green"  <?=$disabled?> value="Save Details"  onclick="save_data()">&nbsp; Save &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn grey">&nbsp; Close  &nbsp; </button> 
		</center>
	</div>
	 
</div>

<div id="delete_landlord" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Confirm Message </b></h5>
				<hr/>
					<p>
					   By accepting to remove this Landlord, you will not be able to view His/Her details later. Proceed?
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="confirm_del" class="btn green" <?=$disabled?>>&nbsp; Yes &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn red">&nbsp; No  &nbsp; </button> 
		</center>
	</div>
</div>

<div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">   Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="success_msg">
				  
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
  

<script type="text/javascript">
 
function validateMail() {
     
    var x = $("#Email").val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
     $("#Email").val('');

        return false;
    }
}
function delete_landlord(id)
	{ 
	        $("#id_no").val(id);
			$("#delete_landlord").modal('show');
	 
	}
	
function add_new_landlord()
{  
 $("#msg").empty();
               $("input[name='landlord_id']").val('');
				$("input[name='landlord_fname']").val('');
				//$("input[name='landlord_mname']").val('');
				//$("input[name='landlord_lname']").val('');
				$("input[name='landlord_id_no']").val('');
				$("input[name='email']").val('');
				$("input[name='landlord_email']").val('');
				$("input[name='landlord_phone']").val('');
				$("input[name='bank_acc']").val('');
				$("input[name='bank_name']").val('');
				$("input[name='bank_branch']").val('');
				$("#landlord_details").modal('show');
 
} 
	
 function getLandlordDetails(id){ 
     $("#msg").empty();
		$.ajax({
		url:'<?=base_url();?>tenants/getLandlordDetails/'+id, 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data);  
			var data = obj.data; 
			if(obj.result=="ok")
			{
				 
				for(var i=0; i<data.length; i++)
				{
						var l = data[i];
						var fname=l['full_name'];
						//var mname=l['middle_name'];
						//var lname=l['last_name'];
						var id_no=l['id_no'];
						var bank_acc=l['bank_acc'];
						var bank_name=l['bank_name'];
						var bank_branch=l['bank_branch'];
						var email=l['email'];
						var phone=l['phone'];
				}
			  
				$("input[name='landlord_id']").val(id);
				$("input[name='landlord_fname']").val(fname);
				//$("input[name='landlord_mname']").val(mname);
				//$("input[name='landlord_lname']").val(lname);
				$("input[name='landlord_id_no']").val(id_no);
				$("input[name='email']").val(email);
				$("input[name='landlord_email']").val(email);
				$("input[name='landlord_phone']").val(phone);
				$("input[name='bank_acc']").val(bank_acc);
				$("input[name='bank_name']").val(bank_name);
				$("input[name='bank_branch']").val(bank_branch);
				$("#landlord_details").modal('toggle'); 
			}
			else
			{
				$("input[name='landlord_id']").val('');
				$("input[name='landlord_fname']").val('');
				//$("input[name='landlord_mname']").val('');
				//$("input[name='landlord_lname']").val('');
				$("input[name='landlord_id_no']").val('');
				$("input[name='email']").val('');
				$("input[name='landlord_email']").val('');
				$("input[name='landlord_phone']").val('');
				$("input[name='bank_acc']").val('');
				$("input[name='bank_name']").val('');
				$("input[name='bank_branch']").val('');
				 
			}
		}
	});
	 
} 
 
 $(document).ready(function(){   
  checkPrivilege();   
  var msg="<?php echo $msg;?>";   
  var saving_success="<?php echo $msg;?>";
 if(!saving_success)
 {

 }
else{
	 
       $("#data_saving_success").modal('toggle'); 	
   }
 
});	 
 
 $("#save").click(function()
	{   
	var property_id=$("#property_id").val();  
	var landlord_id=$("#landlord").val();   
	if(!landlord_id){$("#error1").html("<font color='red'>   Select Landlord </font>");  return false;} else{ $("#error1").empty(); }
	if(!property_id){$("#error2").html("<font color='red'>   Select Property to allocate </font>");  return false;} else{ $("#error2").empty();}
	$.ajax({
		"url":"<?=base_url();?>tenants/assign_landlord_property/",
		"data":
		{
			'property_id':property_id,
			'landlord_id':landlord_id
		},
        "type":"post",
		"success":function(data)
		{  
			 var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
				  $("#success_msg").html("<font color='green'>  "+obj.msg+" </font>"); 
				 $("#data_saving_success").modal('show');				  
				  setTimeout(function()
				{
					$("#data_saving_success").modal('hide'); 
					location.reload(); 
                },3000);  
			 }
			 else
			 {
				$("#success_msg").html("<font color='red'>   "+obj.msg+". Try again later</font>");
				$("#data_saving_success").modal('show');
			 }
		}
		
	})
	
 	});
	
	$("#confirm_del").click(function()
	{  
	//$("#delete_landlord").modal('hide');
	var id=$("#id_no").val();  
	$.ajax({
		"url":"<?=base_url();?>tenants/removeLandlord/"+id,
        "type":"post",
		"success":function(data)
		{
			var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
				 location.reload(); 
			 }
			 else
			 {
				$("#msg").html("<font color='red'> Error! Landlord Not removed. Try again later</font>");
				$("#success").modal('show');
			 }
		}
		
	})
	
 	});
	
	
	
function save_data()
{ 
	var id=$("input[name='landlord_id']").val();      
	var fname=$("input[name='landlord_fname']").val();
	var id_no=$("input[name='landlord_id_no']").val();
	var bank_acc=$("input[name='bank_acc']").val();
	var bank_name=$("input[name='bank_name']").val();
	var bank_branch=$("input[name='bank_branch']").val();
	var email=$("input[name='landlord_email']").val();
	var phone=$("input[name='landlord_phone']").val(); 
	if(!fname){ $("#msg").html("<font color='red'>   Landlord Name required </font>");  	return false;}
	if(!id_no){$("#msg").html("<font color='red'>   National Id required </font>"); 	return false;}
    if(!phone){ $("#msg").html("<font color='red'>  Phone Number required </font>"); 	return false;}
	$.ajax({
		"url":"<?=base_url();?>tenants/add_landlord",
        "type":"post",
		"data":{
			'landlord_id':id,
			'landlord_fname':fname, 
			'landlord_id_no':id_no,
			'bank_acc':bank_acc,
			'bank_name':bank_name,
			'bank_branch':bank_branch,
			'landlord_email':email,
			'landlord_phone':phone
			},
		"success":function(data)
		{
			var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
			     $("#msg").html("<font color='green'><i class='glyphicon glyphicon-ok'></i>  Landlord details saved successfully</font>");
				if(!id){
					$("input[name='landlord_id']").val('');
				$("input[name='landlord_fname']").val('');
				//$("input[name='landlord_mname']").val('');
				//$("input[name='landlord_lname']").val('');
				$("input[name='landlord_id_no']").val('');
				$("input[name='email']").val('');
				$("input[name='landlord_email']").val('');
				$("input[name='landlord_phone']").val('');
				$("input[name='bank_acc']").val('');
				$("input[name='bank_name']").val('');
				$("input[name='bank_branch']").val('');
				
				}
				else  
				{ 
			//return false;
			}
				 
				setTimeout(function()
				{
					$("#landlord_details").modal('hide'); 
					location.reload(); 
                },3000); 
			 }
			 else
			 {
				$("#msg").html("<font color='red'>  "+obj.msg+"</font>");
				 
			 }
		}
		
	})
	return false; 
}


function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/1",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			if(obj.add==0)
			{  
				 document.getElementById('save').disabled = true; 
		      //$("#save_btn").html("<font color='' onclick=\"alert('You have no privilege to add property')\"> Save Details </font>"); 
			} 
		 
		}
	 })
 } 
 

$(function(){ 
$("#landlord_id_no").keypress(function(){  
	var str = $("#landlord_id_no").val();
    var char_length = str.length; 
	 
	 if(char_length==8){ return false;} 
  }); 
  
  }); 
</script>
