<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		document.getElementById('save').disabled=false;	
		var tenant=$("#tenants").val();
		var pay_mode=$("#pay_mode").val();
		var receipt_no=$("#receipt_no").val();
		var amount=$("#amount").val();
		var pay_date=$("#pay_date").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
	//	if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		 if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
			document.getElementById('save').disabled=true;	 
	}
	function validate_edit()
	{    
		var name=$("#edit_user_name").val();
		var pay_mode=$("#edit_pay_mode").val();
		var amount=$("#edit_amount").val();
		var receipt_no=$("#edit_receipt_no").val();
		var pay_date=$("#edit_pay_date").val(); 
		if(name==""||name==null){ $("#edit_error1").html("<font color='red'> Tenant Name   is empty </font>");$("#edit_user_name").focus(); return false;}
		if(amount==""||amount==null){ $("#edit_error4").html("<font color='red'> Amount field is empty </font>"); $("#edit_amount").focus(); return false;}
		if(pay_date==""||pay_date==null){ $("#edit_error3").html("<font color='red'> Date field is empty</font>"); $("#edit_pay_date").focus(); return false;}
		//if(receipt_no==""||receipt_no==null){ $("#edit_error2").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>"); $("#edit_receipt_no").focus(); return false;}
		if(/^[a-zA-Z0-9- ]*$/.test(amount) == false){ $("#edit_amount").focus(); $("#edit_error4").html("<font color='red'>Amount contains illegal characters </font>");  return false; }else{$("#edit_error4").empty();}
	if(/^[a-zA-Z0-9- ]*$/.test(name) == false){ $("#edit_user_name").focus(); $("#edit_error1").html("<font color='red'>Name  should not have special characters </font>");  return false; } else{$("#edit_error1").empty();} 
	if(/^[a-zA-Z0-9- ]*$/.test(receipt_no) == false){ $("#edit_receipt_no").focus(); $("#edit_error2").html("<font color='red'>Receipt No should not have special characters </font>");  return false; } else{$("#edit_error2").empty();} 
	
		
		return confirm("Update details for "+name+"?");
	}
	
</script>

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Rent  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Receive Water Payment</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title"> 
				<div class="col-md-12" style="background:#1bb968;padding:6px;">
					<font color="#ffffff"><strong> &nbsp;  Receive Water Payment </strong> </font> 
				</div> 
<div class="col-md-12">  &nbsp;  </div> 
<div class="row">
<form action="<?=base_url();?>water/receiveWaterPay" method="post" onsubmit="return validate()">
<div class="col-md-12">  
<div class="form-group"> 
<input class="form-control" type="hidden" value="" id="edit_id" name="edit_id">
		<label> Select Tenant </label><br/>
		<select  class="selectpicker"  data-live-search="true" name="from"  onchange="getBalance(this.value)" id="tenants"  title="Select Tenant...">
			 <?php $rent_frequency="";
			 foreach($tenants->result() as $row){?>
					<option value="<?=$row->id?>">
						<?php $company=$row->company_name;$rent_frequency=$row->rent_frequency; $name=$row->first_name." " . $row->middle_name." ".$row->last_name; if($company ==""){ echo $name.",". $row->property_name.",". $row->house_no; }else{ echo $row->company_name.",". $row->property_name.",". $row->house_no;;}?>
					</option>
			 <?php } 
			 if($rent_frequency==0){ $rent_frequency="";}
			 ?>
		</select> <br/>
		<label id="error1">	 	</label>  
</div>
</div>  
 
<div class="col-md-4">  
	<div class="form-group">
		<label>Previous Reading </label>
			<input type="number" class="form-control" readonly name="prev_reading"   id="prev_reading"  value="0" min="1" >
		<label>  </label>
	</div> 
</div>
	
<div class="col-md-4"> 
		   <div class="form-group">
					<label> Current Reading </label>
					<input type="number" class="form-control" readonly name="curr_reading"   id="curr_reading"  value="0" min="1">
				 <label>  </label>
			</div> 
						
</div>	 
<div class="col-md-4"> 
	<div class="form-group">
			<label> Expected Amount  </label>
			<input type="number" class="form-control" readonly name="expected_amount"   id="expected_amount"  value="0" min="1">
		 <label>  </label>
	</div> 
						
</div>	
<div class="col-md-4">    
		 <div class="form-group">
					<label> Amount Received</label>
					<input type="number" class="form-control" required name="amount"   id="amount"  value="0" min="1"  max="25000000" onchange="setData()">
				 <label id="error2">  </label>
			</div> 
			
</div>

<div class="col-md-4"> 
			<div class="form-group">
					<label> Payment Method </label>
					<select class="form-control"  name="pay_mode" id="pay_mode"  onchange="disable_method()" >
						<option value=""> Not selected </option>
						<option> Cash </option>
						<option> Cheque </option>
						<option> Mpesa </option>
						<option> Bank Receipt </option>
					</select>
				 <label id="error3">  </label>
			</div> 
	<p> </p> 
</div>				
<div class="col-md-4"> 
		 <div class="form-group">
					<label> Mpesa, Cheque or Receipt Number </label>
					<input type="text" class="form-control"   name="receipt_no"   id="receipt_no" onchange="validate_char('receipt_no')">
				 <label id="error5">  </label>
		 </div>
</div>

</div> 
	<div class="row">  
		<div class="col-md-3">  
				 <div class="form-group">
				 <button  type="submit" id="save" class="btn green" <?=$disabled;?> >  &nbsp; SAVE  &nbsp;  </button>
				</div> 		
		 </div> 
		 <div class="col-md-6"> 
		<div class="form-group">
					<b style="color:brown"> <?php if($this->session->flashdata('temp')){ 
					//echo $this->session->flashdata('temp');
					}?> </b>
		</div> 
		 </div>
	</form>
</div> 

 
 
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div> 
<!---Edit payment-->


<!--->
 <!-- responsive -->
<div id="add_new" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			 
			<h5><b> Specify other Payment </b></h5>
			<hr/>
			<p>
			<label class="control-label">Name/Description </label>
			<input   class="form-control" type="text" placeholder="electricity" id="amenity_name"> </p>
			<p>
			</div>
		</div> 
	</div> 
<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_new_one" <?=$disabled;?>> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>


<div id="success" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b style="font-size:20px;color:green">    Warning Message </b></h5>
				<hr/>
				<p id="success_msg">
				   
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
 <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">    Success Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="">
				   <?php if($this->session->flashdata('temp')){ echo $this->session->flashdata('temp');}  ?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<!--<button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> -->
		&nbsp;
	</div> 
</div>
 
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 
function validate_char(id)
{
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		$("#success").modal('toggle');  
        return false;
		}
    
}

$(document).ready(function () { 

  checkPrivilege();
  var saving_success="<?php echo $this->session->flashdata('temp');?>";
if(!saving_success) {  }
else{
		$("#data_saving_success").modal('toggle');
		setTimeout(function()
		{
			 $("#data_saving_success").modal('hide'); 
		},4000);  
	}
	
$("#add_new_one").click(function(){ 
var name=$("#amenity_name").val();
if(!name){ $("#status_message").html("<font> Name required </font>"); $("#amenity_name").focus();  return false;}
  $.ajax(
  {
		url:"<?=base_url();?>payment/add_new_amenity",
		type:"POST",  
		data:{
			'amenity_name':name
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#status_message").html("<font color='green'> Added successfully </font>");
				  setTimeout(function(){
								$('#add_new').modal('hide');
                               $("#status_message").empty();
                      }, 2000); 
					  document.location.reload(true);
			 }
			 else{
				 $("#status_message").html("<font color='red'> Not added. Try again later </font>");
			 }

		}			 
  })
 
 });

 });
  
function add_other(value)
{
  if(value=="other")
  {
	$("#add_new").modal('show');
  }
}
  
function disable_method()
{ 
	var value=document.getElementById("pay_mode").value;
	 if(value=="Cash")
	  {  
		document.getElementById('error5').value ="";
		document.getElementById('receipt_no').value ="--";
		document.getElementById('receipt_no').disabled = true;  
	  }
	  else
	  {
			document.getElementById('receipt_no').value ="";
			document.getElementById('receipt_no').disabled = false;
	  }  
}

function getBalance(val)
{  
$.ajax({
   url:"<?=base_url();?>water/getWaterReading/"+val,
   type:"POST", 
   async:false, 
   success:function(data)
   {
	   var obj=JSON.parse(data);
	   if(obj.result=="ok")
	   {  		 
			setRentFrequency(val);
			$("#expected_amount").val(obj.balance);		
			$("#prev_reading").val(obj.prev_reading);		
			$("#curr_reading").val(obj.curr_reading); 	
	   }
	   else { }
   } 
})
}

function setRentFrequency(val)
{  
 $.ajax({
   url:"<?=base_url();?>water/getRentFrequency/"+val,
   type:"POST", 
   async:false, 
   success:function(data)
   { 
	   var obj=JSON.parse(data);
	   if(obj.result=="ok")
	   {  		 
			$("#rent_frequency").html("<font>"+obj.rent_frequency+"</font>");	 
	   } 
   } 
})
 
}


function checkPrivilege()
 {  
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/3",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			 
			if(obj.add==0){ 
			   	document.getElementById('save').disabled=true;	  
			}
			if(obj.view==0){ 
			for(var x=1; x<i;x++){   $("#view_"+x).html("<font color='' onclick=\"alert('You have no privilege to view')\"> <a href='#' > View Receipt </a></font>"); }
              		  
			}
			  
			if(obj.delete==0){ 
			    
			   //document.getElementById('confirm').disabled=true;   
			}
		}
	 })
 } 
</script> 