<!-- BEGIN PAGE CONTENT BODY -->

<?php 
		$credit=0; $debit=0; $curr_bal=0; $total=0;
		 
		   
	?>
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Payment </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Water Receipt   </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">
	
	<div class="col-md-12"    style="background:#006699;padding:6px;">
				<font color="#ffffff">Water Payment Receipt </font> 
	</div>		  
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
<div class="portlet light ">  
<div class="row">
 
<div class="col-md-9"> 
   <div class="portlet light">  
		<div class="portlet-body" ><!--ari_company_logo.png-->
		<div style=" padding:1px;border:1px solid lightgrey">
			 <?php $logo="Logo-Ari-CS.PNG"; $tenant_name=""; $email=""; $company_name="";  $amount=0; $item_cost=0; $payment_mode=""; $payment_mode_code=""; $balance=0; $receipt_no=""; $date_paid="";
			  if($logo==""||$logo==0){ $logo="Logo-Ari-CS.PNG"; }
			   foreach($receipt->result() as $r){ 
			   $tenant_id=$r->tenant_id; $receipt_no=$r->receipt_no; $date=$r->date_paid;$month=$r->month;$year=$r->year;
			   $amount=$r->amount;$balance=$r->balance; $payment_mode=$r->payment_mode; $payment_mode_code=$r->payment_mode_code;
			   }
			   foreach($tenants->result() as $t){ 
			    $tenant_type=$t->tenant_type; if($tenant_type=="residential")
				{ 
				 $tenant_name=($t->last_name." ".$t->middle_name." ".$t->first_name); $company_name=$t->company_name; 
				 $email=$t->tenant_email;
				}
				 else{  $tenant_name=$t->company_name; $company_name="";}
				 
			   }
				 ?>	</li>
			  
		<div>
		 
		<center><img id="my_file" src="<?=base_url();?>media/<?=$logo?>" height="70" width="140" style=" display:block;max-width:230px;max-height:95px;width: auto;height: auto;" alt="<?=strtoupper($company_name)?>"> 
		    <p>  </p>
		<ul style="font-size:14px; list-style-type:none">
			<li> <?php if($company_name==""){ $company_name=$this->session->userdata('first_name')." ".$this->session->userdata('last_name');}else{  $company_name;} echo strtoupper($company_name);?> </li>
				<li> <?=strtolower($this->session->userdata('email'));?> </li>
				<li> <?=strtoupper($this->session->userdata('phone'));?> </li>
			<li> <?=strtoupper($this->session->userdata('address'));?> </li> 
		</ul>
		</center>
		</div>
		<p>  </p>
		 <center><h3>WATER RECEIPT</h3></center>
		 <hr/>
	<table border="0"  class="table table-striped table-hover"  cellpadding="0" cellspacing="0" width="100%"> 
	
				<tr> 
					 <td align="left">  <font>  <strong> Amount: KES </strong><?=$amount?></font>  </td> 
					<td >
						<font>  <strong> Receipt No: </strong><?=$receipt_no?></font> 
					</td> 
					 
					 <td><font>  <strong> Date Paid: </strong><?=$date?></font> </td>  
				 </tr>
				 <tr> 
					 <td align="left">  <font>  <strong>Payment Mode: </strong><?=$payment_mode?></font>  </td> 
					<td colspan="2">
						<font><strong>Payment Transaction Code: </strong><?=$payment_mode_code?></font> 
					</td> 
					   
				 </tr>
		<tr>
		<td align="left" colspan="2">  	
			<ul style="font-size:16px;padding-left:1px;list-style-type:none">
	           <!--style="font-size:13px;padding-left:0px;list-style-type:none">-->
				<li>	&nbsp; 	</li>  
				<li><strong>Paid By:</strong></li>  
				<li>	 <?=$tenant_name?>	</li>
				<li>	&nbsp; 	</li> 
				<li>	House No: <?=$t->house_no?>	&nbsp; Floor: <?=$t->floor_no?>	</li>
				<li>	&nbsp; 	</li>   
				<li>	Email : <?=$t->tenant_email?> &nbsp; Phone : <?=$t->mobile_number?> </li> 
				   
			 </ul>
		</td> 
		<td>   &nbsp; &nbsp; </td>
		 
		<!--<td  align="right">
			<ul style="font-size:13px; list-style-type:none">
				<li><strong> Paid to:</strong></li>  
			 
				<li> &nbsp;   </li> 
			</ul>
		</td>-->
	</tr>	
	</table>
	<table border="0" width="100%" class="table table-striped table-hover" style="text-align:center">
	<tr>
			<th> 
				 <strong> Prev Reading Date</strong>   
			</th> 
			<th> 
				<strong> Prev Meter Reading</strong>     
			</th>
			<th> 
				 <strong> Curr Reading Date</strong>   
			</th> 
			<th> 
				<strong> Curr Meter Reading</strong>     
			</th> 
			<!--<th> 
				<strong> Consumption</strong>     
			</th>
			<th> 
				<strong> Price/Unit</strong>     
			</th><th> 
				<strong> Amount</strong>     
			</th>--> 
			  
	</tr>
		  
		 <tr height="20"> <td><font id="prev_reading_date">0</font> </td> <td> <font id="prev_reading">0</font> </td> 
		 <td><font id="curr_reading_date">0</font></td> 
			<td> <font id="curr_reading">0</font> </td>
			<!--<td> <font id="consumption1"></font> </td> 
			<td> <font id="unit_cost1"></font> </td>  
			<td align="center"> <font id="total_cost1"></font></td>-->
		</tr>
	 </table>
	 <table border="0" width="100%" class="table table-striped table-hover">
		<tr>
			<th>Consumption &nbsp; </th> 
			<th>Price/Unit </th> 
			<th align="center">  Total Cost</th>
		 </tr>
		  
		<tr><td align="center"> <font id="consumption">0</font>  </td><td align="center">   <font id="unit_cost">0</font></td><td align="center"><font id="total_cost">0</font></td></tr>
		<tr><td colspan="3"> &nbsp; </td></tr> 
		<tr><td> &nbsp; </td><td>  &nbsp; </td> 
			<td > <strong> Balance as at <?=$date?>&nbsp;</strong> KES <?=$balance?> </td>
		 </tr>
	 </table>
  
<p> 
<!--<h5><strong> Served by: 
<?//=ucfirst(strtolower($this->session->userdata('first_name')));?>  
</strong> </h5>-->
<h6>Printed on <?php echo date('d-m-Y H:i');?></h6>  </p>
</div>
</div>
 
<p> &nbsp; </p>
 
<a  href="javascript:;" class="btn grey"  id="btn_receipt" > &nbsp;&nbsp;&nbsp; <i class="fa fa-print" style="font-size:18px;color:#006699"></i><b>  Print  </b>&nbsp;&nbsp;&nbsp;</a> 
&nbsp;
<a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  

		 </div>
		</div>
 
</div>
</div>
</div> 
</div> 
</div>
<!-- END EXAMPLE TABLE PORTLET--> 

</div>
<!-- END PAGE CONTENT INNER -->
</div> 
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
  <div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> Venit Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
   


 <div id="sendMail" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Email Tenant Message </b></h5>
				<hr/>
					<p>
					    This Receipt will be sent as an Email to <font id="email_id"> </font>
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="send_email" class="btn green">&nbsp; Yes &nbsp; </button>
			 <button type="button" data-dismiss="modal" class="btn default">&nbsp; Cancel  &nbsp; </button> 
		</center>
	</div>
</div>
   
      
        <!-- END CONTAINER -->
<script language="javascript">
 	
$(document).ready(function () {
 getData();
});
 $("#btn_receipt").click( function () {
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=1000');
            printWindow.document.write('<html><head><title>  Receipt </title>');
            printWindow.document.write('<style>th, td { border-bottom: 1px solid #ddd;}</style> </head><body style="margin-left: auto;margin-right: auto; width:80%">');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
		

  
 
  
        var email="<?=$email?>";  
        var fname="<?=$tenant_name?>";  
        var from="<?=$company_name?>"; 
      
		
 $("#btn_email").click( function ()
		{ 
		   if(!email){ 
		   alert('Email address is empty! Please add tenant email first');
		   return false;
		   }
		   $("#email_id").html(fname+" <u> "+email+" </u>");
		   document.getElementById('send_email').disabled = false;
			$("#sendMail").modal('show');
		});
		
   $("#send_email").click( function () {
document.getElementById('send_email').disabled = true; 	   
		var divContents = $(".portlet-body").html();   
       
		$("#error").html("<font color='#006699'> Sending email...please wait</font>");
		//$("#success").modal('show'); 
		 
		$.ajax({
			//'url':"<?=base_url();?>payment/sendRentStatement",
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			 async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Water Receipt", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
					$("#error").html("<font color='green'> Receipt   sent   successfully</font>");
					//$("#success").modal('show');
					setTimeout(function()
						{ 
							$("#error").empty();
							$("#sendMail").modal('hide');
						}, 2500);
				}
				else
				{  
					$("#error").html("<font color='red'> Receipt   not sent to   <u>"+email+"</u></font>");
					document.getElementById('send_email').disabled = false;
				}
				
			}
			
 })
 
});
	


   function getData()
   { 
	   var month="<?=$month?>";  
      var year="<?=$year?>";  
     var id="<?=$tenant_id?>";  
     $.ajax({
	   "url":"<?=base_url();?>water/getWaterReading/"+id+"/"+month+"/"+year,
	   "type":"POST",
	   async:false, 
	   success:function(data)
	   {
		  var obj=JSON.parse(data); 
        $("#curr_reading_date").html(""+obj.curr_reading_date);		  
          $("#curr_reading").html(""+obj.curr_reading);	  
          $("#prev_reading_date").html(""+obj.curr_reading_date);		  
          $("#prev_reading").html(""+obj.prev_reading);	
          //$("#bal").html(""+obj.balance);
			var consumption=0; var total_cost=0;
			consumption=(parseInt(obj.curr_reading))-(parseInt(obj.prev_reading));		  
			total_cost=consumption*parseInt(obj.unit_cost);		  
          $("#consumption").html(""+consumption);	
          $("#unit_cost").html(""+obj.unit_cost);	
          $("#total_cost").html(""+total_cost);	
	   }
   })
   
    
   }
$(function () {
$.fn.vAlign = function(){
    return this.each(function(i){
    var ah = $(this).height();
    var ph = $(this).parent().height();
    var mh = Math.ceil((ph-ah) / 2);
    $(this).css('margin-top', mh);
    });
};

$('#my_file').vAlign();
		});
</script>
