 <script language="javascript">
$(function() {
  
	  $("#searchName").autocomplete({
      source: '<?=base_url();?>tenants/search_supplier'
    }); 
 $("#searchName").autocomplete("option", "appendTo", "#modal-body" );

});
</script>
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#"> Home </a>
		<i class="fa fa-circle"></i>
	</li>
     <li>
		<a href="#"> Expenses </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>  Suppliers  </span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<div class="row">
<div class="col-md-12">
	   
	<div class="portlet light portlet-fit ">
	<div class="portlet-body"  style="min-height:500px"> 
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Suppliers  </font>
		</div>		 
			<p> &nbsp; </p> 
			 <input type="hidden" value="0" id="countr">
				<table  class="table table-striped table-hover table-bordered" id="table1">
					<thead>
						<tr>  
                             <th> #  </th>
							<th> Company Name </th> 
							<th> Phone </th> 
							<th> Email </th>  
							<th> Action </th> 
							<th> &nbsp; </th> 
						</tr>
					</thead>
					<tbody id="t_data">
					<tr><td colspan='6' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>						
					</tbody>
				</table>
			 
	            <div  >
							<a href="javascript:;" class="btn green"  onclick="new_supplier()" >  ADD NEW SUPPLIER  </a>
						</div> 
			 
			</div>
		</div>
	</div>
</div>
	</div>  
	</div>  
	
	<div id="edit_supplier" class="modal fade" tabindex="-1" data-width="600" aria-hidden="true"> 
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Edit Supplier <font id="for"> </font></b> </font></h4>
		   
	</div>
<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12">  
	<p> 
		<label class="control-label"> Company Name <font color="red"> * </font> </label>
		<input   class="form-control" type="hidden" id="edit_id" name="edit_id"/> </p>
		<input   class="form-control" type="text" id="name" name="name"  onchange="validate_char('name')"/> </p>
		<!--<p> 
		<label class="control-label"> Contact Person Name  </label>
		<input   class="form-control" type="text" id="contact_person" name="contact_person" onchange="validate_char('contact_person')" /> </p>
		<p>-->
		<label class="control-label">Email </label>
			<input   class="form-control" type="email" id="email" name="email" onchange="validate_email('email')"> </p>
		 
		<label class="control-label">Phone No <font color="red"> * </font></label>
			<input   class="form-control" type="text" id="phone"  maxlength="10" onkeypress="return checkIt(event)"  name="phone" onchange="validate_char('phone')"> </p>
		 
		<!--<p>
		<label class="control-label"> Product Description </label>
		 
		<textarea class="form-control" id="description" name="description" onchange="validate_char('description')">  </textarea>
		</p>-->
</div>
</div>  <span id="status_msg">   </span>
</div>
<div class="modal-footer" >
		<input type="submit" class="btn green"  id="edit" <?=$disabled?>  value="Save"> 
		<button type="button" data-dismiss="modal" class="btn btn-outline dark" > Cancel </button>
</div> 
</div>
</div>
 
<div id="responsive" class="modal fade" tabindex="-1" data-width="600" aria-hidden="true">
        
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" color="#006699" > <b> Register Supplier </b> </font></h4>
	 </div>
<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12" id="modal-body">  
	<p> 
		<label class="control-label"> Name/Company <font color="red"> * </font> </label>
		<input   class="form-control" type="text" id="searchName" name="name" onchange="validate_char('searchName')"/> </p>
		 <label class="control-label">Email </label>
			<input   class="form-control" type="email" id="add_email" name="email" onchange="validate_email('add_email')"> </p>
		    
		<label class="control-label">Phone No <font color="red"> * </font></label>
			<input   class="form-control" type="number" id="add_phone" maxlength="10" name="phone" onchange="validate_char('add_phone')"> </p>
		 
</div>
</div>  <span id="add_msg">   </span>
</div>
<div class="modal-footer" >
		<input type="submit" class="btn green" id="add_supplier" <?=$disabled?>  value="Save"> 
		<button type="button" data-dismiss="modal" class="btn btn-outline dark" > Cancel </button>
</div> 
</div>
</div>
 
 <input type="hidden" value="0" id="delete_items">
 
 <script language="javascript">
  
function validate_char(id)
{ 
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		//$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		document.getElementById(id).style.backgroundColor = "#ffd6cc";
		$("#add_msg").html("<font color='red'> The highlighted Input field had illegal characters. Re-enter the value</font>"); 
		   document.getElementById(id).focus();
        return false;
		}
		else
		{
			$("#add_msg").empty();
			document.getElementById(id).style.backgroundColor = "#fff";
		} 
}


    function edit_supplier(id){
		$("#status_msg").empty();	
		$("#add_msg").empty();
 	
	$.ajax(
	{
		url:"<?=base_url();?>rent/showSupplier/"+id,
		type:"POST",  
		success:function(data)
		{ 
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {  
				 var data=obj.data;
				 for(var i=0;i<=data.length; i++)
				 {
					 var supplier=data[i];
					 var name=supplier['company_name'];
					 var no=supplier['id'];
					 var contact_person=supplier['contact_person_name'];
					 var phone=supplier['mobile_no'];  
					 var email=supplier['email'];  
					 var service=supplier['product_description'];  
					  $("#name").val(name); 
					 $("#for").html(name); 
					 $("#contact_person").val(contact_person);
					 $("#phone").val(phone); 
					$("#email").val(email);
					$("#edit_id").val(no);
					$("#description").val(service);
					$('#edit_supplier').modal('show'); 
				 }
			 }
		}
	  
	  
  })
	
	 
	 } 
	 
function delete_supplier(id){
	 
	 var a=$("#delete_items").val();
	 if(a==0){
		alert('You have no privilege to remove suppliers!'); 
		return false;
	     }
	 
		 var d="<?=$disabled?>";
		if(d=="disabled")
		{
			alert("You cannot perform this action! Ensure your subscription is active");
			return false;
		}else{
		var conf= confirm("Are you sure you want to remove this Supplier?");
			if(conf==true)
			{
				$.ajax(
			{
				url:"<?=base_url();?>tenants/remove_supplier/"+id,
				type:"POST",  
				success:function(data)
				{ 
					 var obj=JSON.parse(data);
					 if(obj.result=="ok")
					 {  
				  
						location.reload();
					 } 
					 else
					 {
						 alert('Record not removed! Try again later');
					 }
				}
			  
			  
		  })
	
	 }
	}
}

$(document).ready(function () { 
 checkPrivilege();
 
$("#edit").click(function(){ 
$("#add_msg").empty();  
  var company_name=$("#name").val(); 
	 var id=$("#edit_id").val(); 
	 var email=$("#email").val(); 
	 var phone=$("#phone").val();   
	 //var contact_person=$("#contact_person").val();  
	 var contact_person='';  
	// var description=$("#description").val();  
	 var description=''; 
$("#status_msg").empty();	 
if(! company_name){ $("#name").focus();  return false;}
if(! company_name || ! phone){  $("#status_msg").html("<font color='red'> Fill in all the required fields marked with * </font>");  return false;}
 if(! email){}else
	 {
		 var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { alert("Email provided is invalid");   $("#email").focus(); return false;}
	 }

	$("#status_msg").html("<font color='blue'> Saving data...</font>"); 
	$.ajax(
	{
		url:"<?=base_url();?>tenants/add_supplier",
		type:"POST",
		async:false,
		data:
		{
		'id':id,  
		'company_name':company_name,  
		'contact_person':contact_person,  
		'email':email,  
		'phone':phone,  
		'description':description 
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 { 
                          //$("#transaction_no").val('');
		          // $("#amount").val('');
		          // $("#description").val('');
					 $("#status_msg").html("<i class='fa fa-check'> </i><font color='green'> Supplier details updated successfully  </font>");
					 setTimeout(function(){
								$('#responsive').modal('hide');
                               $("#status_msg").empty();
                      }, 3000); 
					  document.location.reload(true);
			 }
			 else
			 {
				 $("#status_msg").html(" <font color='red'> Data was not saved! "+obj.msg+" </font>");
			 }
		}
		
	})	 
	
 });
 
 
$("#add_supplier").click(function(){  
	 var company_name=$("#searchName").val(); 
	 var email=$("#add_email").val(); 
	 var phone=$("#add_phone").val();  
	 var contact_person='';//$("#add_contact_person").val();  
	 var description='';//$("#add_description").val();
	$("#add_msg").empty();
if(! company_name || ! phone){  $("#add_msg").html("<font color='red'> Fill in all the required fields marked with *  </font>");  return false;}
	 if(! email){ }else
	 {
		 var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { alert("Email provided is invalid");   $("#add_email").focus(); return false;}
	 }
	 $("#add_msg").html("<font color='blue'> Saving data...</font>");
	$.ajax(
	{
		url:"<?=base_url();?>tenants/add_supplier",
		type:"POST",
		async:false,
		data:
		{
		'id':'',  
		'company_name':company_name,  
		'contact_person':contact_person,  
		'email':email,  
		'phone':phone,  
		'description':description
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 { 
					$("#transaction_no").val('');
					$("#amount").val('');
					$("#description").val('');
					$("#add_msg").html("<i class='fa fa-check'> </i><font color='green'> Supplier details saved  </font>");
					 setTimeout(function(){
								$('#responsive').modal('hide');
                               $("#add_msg").empty();
                      }, 3000); 
					  document.location.reload(true);
			 }
			 else
			 {
				  $("#add_msg").html("<font color='red'> Supplier details not  saved! Company Name/Email already exist  </font>");
				 //$("#status_msg").html(" <font color='red'> Not updated. You have made no changes to save </font>");
			 }
		}
		
	})	 
	
 });
 
 
 });
 
function new_supplier()
  { $("#status_msg").empty();$("#add_msg").empty();
	   $("#responsive").modal('show');
	  
  } 
 
function validate_email(id)
 {
	 
		var email=	$("#"+id).val(); 
	   if(email){ } else{$("#"+id).focus(); return false;}
      var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { alert("Email provided is invalid"); $("#"+id).focus(); return false;}
 }
 
function checkPrivilege()
 { 
	 var i=$("#countr").val();
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/4",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;   
			if(obj.add==0)
			{  
				 document.getElementById('add_supplier').disabled = true; 
				
		      //$("#save_btn").html("<font color='' onclick=\"alert('You have no privilege to add property')\"> Save Details </font>"); 
			} 
			if(obj.view==0){ 
				//document.getElementById('pricinng_property_name').disabled = true;  
			}
			if(obj.edit==0){  
				for(var x=1; x<i;x++){   $("#edit_"+x).html("<font color='' onclick=\"alert('You have no privilege to edit')\"> <a href='#' ><i class='fa fa-edit'> </i> Edit   </a></font>"); }
              
				 document.getElementById('edit').disabled = true;  
			}
		 
			 $("#delete_items").val(obj.delete);  
				//document.getElementById('delete_btn').disabled = true;   
		 
		}
	 })
 } 
 
 
function suppliers()
{
		var content=""; 
		$("#t_data").html("<tr><td colspan='6' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>");
		$.ajax({
		url:'<?=base_url();?>tables/suppliers/',  		
		type: 'POST', 
		async:false, 
		success:function (data)
		{   
			var obj = JSON.parse(data);  
			var data = obj.data; var count=0; var count=parseInt(data.length);$("#countr").val(count);  
            
			if(count >0){ 
				for(var i=0; i<data.length; i++)
				{
					var p = data[i];  var tenant_name="";  					 
					var enct=p['enct']; 
					content=content+"<tr><td>"+(i+1)+"</td> "+
					"<td>"+p['company_name']+"</td> <td>"+p['mobile_no']+"</td><td>"+p['email']+"</td>"+
					" <td id='edit_"+i+"'> <a href='javascript:;'  onclick='edit_supplier("+p['id']+")' ><i class='fa fa-edit'> </i> Edit </a> </td> <td> <a href='javascript:;' onclick='delete_supplier("+p['id']+")' ><i class='fa fa-trash'> </i> Delete </a> </td>  </tr>";
					 count++;
				} 
				$("#t_data").html(content); 
			}
			else{
					$("#t_data").html("<tr><td colspan='8' align='center'>No data available</td></tr>");
				} 
			$('#table1').DataTable();
		}
	});
 
}
  
 $(document).ready(
 function(){   
	suppliers();
 });
</script>  