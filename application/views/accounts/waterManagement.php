<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper"> 
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
	<div class="container">
		<!-- BEGIN PAGE BREADCRUMBS -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Tenants</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Water Management </span>
			</li>
	</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row"> 
<div class="col-md-12">
<!-- BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
	<div class="row">
		<div class="col-md-12">
<div class="portlet light ">		 
<div class="portlet-body">
	<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
<div class="tab-pane active" name="tab_1_1">
<?php 
 

if($msg !="")
{
/*echo '<div class = "alert alert-success alert-dismissable" style="background:lightgreen">
   <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	  &times;
   </button>
	
   <font color="#006699">'. $msg. '</font>
</div>'; 	*/
}

  
?>


<div class="row"> 
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff">  Water Management	</font>
	</div>		
 
<div class="col-md-12">  &nbsp;  </div>	
<form action="<?=site_url('water/');?>" enctype="multipart/form-data" method="post"> 
  
<div class="col-md-4">
	<div class="form-group">
			<input type="hidden" name="landlord_id" value="">
			
			<label class="control-label"> Select Property </label>
			<select class="selectpicker"  data-live-search="true"  name="property_id" id="property" onChange="this.form.submit(this.value)" >
					<option value=""> None  </option>
					<?php foreach($property->result() as $row){?> 
					<option value="<?=$row->id ?>"><?=$row->property_name?> </option>
					<?php  }?> 
			</select>
	</div>  
</div>
<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> 	Price per unit (KES)</label>
			<input type="number" class="form-control" min="0" id="water_unit_cost" onchange="updateWaterUnitCost('<?=$selected_property?>')" value="<?=$water_unit_cost?>" /> 
		<span id="errorMsg">  </span>
		</div>
</div>

<div class="col-md-4">
	<label class="control-label"> &nbsp; &nbsp;</label>
<div class="form-group">

		<a href="javascript:;" class="btn red" onClick="add_new_reading()">  Close/Open New Month </a>
</div>
</div> 
 
</form>
 
<div class="col-md-12"> &nbsp; <hr/>  </div>
<form action="<?=site_url('water/waterReading');?>" enctype="multipart/form-data" method="post"> 
 <div class="col-md-12">
 <input type="hidden" name="id_no" value="<?=$id?>">
 <input type="hidden" name="water_unit_cost" value="<?=$water_unit_cost?>">
<table  class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>   
							<th> Unit No </th>
							<th> Tenant </th>
							<th> Previous Reading  </th> 
							<th> Current Reading (<?=date('d/m/Y')?>)</th>    
							<th> Consumption  </th>    
						</tr>
					</thead>
					<tbody>
					<?php $i=1;foreach($tenants->result() as $row){
						$curr_reading=0; $prev_reading=0;
						foreach($water->result() as $r){   if($row->id==$r->tenant_id){ $prev_reading=$r->previous_reading; $curr_reading=$r->current_reading;} }
						?> 
			  <tr>
			  <td> <?=$row->house_no?> </td>
			  <td> <input type="hidden"  name="<?='id'.$i?>" value="<?=$row->id?>"> <?=$row->first_name. '  '. $row->middle_name.' '.$row->last_name?></td>
			  <td> 
			  <input type="number" class="form-control" <?php if($prev_reading>0){ echo 'readonly';} ?> name="<?='prev_reading'.$i?>"  value="<?=$prev_reading?>"> </td>
			    
				<td>
					<div class="form-group"> 
						<input type="number" min="0" name="<?='curr_reading'.$i?>" maxlength="3" onkeypress="return checkIt(event)" value="<?=$curr_reading?>" class="form-control" > </td>	</div> 
				</td>
				<td> <?php echo $curr_reading-$prev_reading;?></td>
			</tr> 
					<?php  $i++; }?>						
					</tbody>
				</table> 
 <?php if($i>1){?>
<div class="form-group">
	<input type="hidden"value="<?php echo $i-1;?>"name="total_number">
		<input type="submit" <?=$disabled?> value="Save Records"  id="save" class="btn green"/> 
</div>
 <?php }?>
</form>

</div>
   
</div>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
</div>
</div>
<!-- END CONTAINER --> 

 <div id="delete_landlord" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Confirm Message </b></h5>
					<p id="contents">
					   By accepting to remove this Landlord, you will not be able to view His/Her details later. Proceed?
					</p>
				</div>
			</div>
			
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="confirm_del" class="btn green" <?=$disabled?>>&nbsp; Yes &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn red">&nbsp; No  &nbsp; </button> 
		</center>
	</div>
</div>
 <div id="add_new_reading" class="modal fade" tabindex="-1" data-width="600">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Warning Pop Message </b></h5>
				<hr/>
					<p>
					   The current reading will be re-set to Zero (0) to allow this month's reading entry. Proceed?
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" onclick="confirm_new_reading()" class="btn green" <?=$disabled?>>&nbsp; Yes &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn red">&nbsp; No  &nbsp; </button> 
		</center>
	</div>
</div>

  <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">   Water Readings Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="">
				   <?php  echo $msg;?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
  

<script type="text/javascript">
 
function validateMail() {
     
    var x = $("#Email").val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
     $("#Email").val('');

        return false;
    }
}
function delete_landlord(id)
	{ 
	        $("#id_no").val(id);
			$("#delete_landlord").modal('show');
	 
	} 
	
	function add_new_reading(id)
	{
	        $("#id_no").val(id);
			$("#add_new_reading").modal('show');
	 
	} 
 function confirm_new_reading()
 { 
		$.ajax({
		url:'<?=base_url();?>water/add_new_reading/', 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data);  
			var data = obj.data; 
			if(obj.result=="ok")
			{
			   window.location.replace("<?=base_url();?>tenants/waterManagement");
			}
			else
			{
				$("#error").html('<font color="red"> Reading was not re-set successfully! This is done once a month </font>');
			}
		}});
		
} 
 
$(document).ready(function(){ 
 
  checkPrivilege();   
  var msg="<?php echo $msg;?>";   
  var prop="<?php echo $selected_property;?>";   
  $("#property").val(prop);
  var saving_success="<?php echo $msg;?>";
	if(!saving_success || saving_success=="")
 {

 }
else{
       $("#data_saving_success").modal('toggle');   
    } 
});	 
 
	
  
	
function updateWaterUnitCost(id)
 { 
	var unit_cost=$("#water_unit_cost").val();
 $.ajax({
	 url:"<?=base_url();?>water/updateWaterUnitCost",
	 type:"POST",
	 async:false,
	 data:{
		 'property_id':id,
		 'unit_cost':unit_cost
		},
	 success:function(data)
	 {
		var obj=JSON.parse(data);
        if(obj.result=="ok")
		{  
			 setTimeout(function() { $("#errorMsg").html("<font color='green'><b>"+obj.msg+"</b> </font>"); }, 1000);
			  setTimeout(function() { $("#errorMsg").html(""); }, 5000);
			 
		}
		else{
			 setTimeout(function() { $("#errorMsg").html("<font color='red'><b>"+obj.msg+"</b> </font>"); }, 1000);
			 setTimeout(function() { $("#errorMsg").html(""); }, 5000);
			  
		}		
	 }
	 
	 });	 
	 
 } 

 function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/1",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			if(obj.add==0)
			{  
				 document.getElementById('save').disabled = true; 
		      //$("#save_btn").html("<font color='' onclick=\"alert('You have no privilege to add property')\"> Save Details </font>"); 
			} 
		 
		}
	 })
 } 
 		
</script>
