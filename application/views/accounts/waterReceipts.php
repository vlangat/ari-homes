<script>
function validate_edit()
	{    
		var name=$("#edit_user_name").val();
		var pay_mode=$("#edit_pay_mode").val();
		var amount=$("#edit_amount").val();
		var receipt_no=$("#edit_receipt_no").val();
		var pay_date=$("#edit_pay_date").val(); 
		if(name==""||name==null){ $("#edit_error1").html("<font color='red'> Tenant Name   is empty </font>");$("#edit_user_name").focus(); return false;}
		if(amount==""||amount==null){ $("#edit_error4").html("<font color='red'> Amount field is empty </font>"); $("#edit_amount").focus(); return false;}
		if(pay_date==""||pay_date==null){ $("#edit_error3").html("<font color='red'> Date field is empty</font>"); $("#edit_pay_date").focus(); return false;}
		//if(receipt_no==""||receipt_no==null){ $("#edit_error2").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>"); $("#edit_receipt_no").focus(); return false;}
		if(/^[a-zA-Z0-9- ]*$/.test(amount) == false){ $("#edit_amount").focus(); $("#edit_error4").html("<font color='red'>Amount contains illegal characters </font>");  return false; }else{$("#edit_error4").empty();}
	if(/^[a-zA-Z0-9- ]*$/.test(name) == false){ $("#edit_user_name").focus(); $("#edit_error1").html("<font color='red'>Name  should not have special characters </font>");  return false; } else{$("#edit_error1").empty();} 
	if(/^[a-zA-Z0-9- ]*$/.test(receipt_no) == false){ $("#edit_receipt_no").focus(); $("#edit_error2").html("<font color='red'>Receipt No should not have special characters </font>");  return false; } else{$("#edit_error2").empty();} 
	
		
		var c= confirm("Update details for "+name+"?");
		if(c==true)
		{ 
			doneEdit();
		}
		else{  return false;}
	}
	</script>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper"> 
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
	<div class="container">
		<!-- BEGIN PAGE BREADCRUMBS -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Tenants</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Water Receipts </span>
			</li>
	</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row"> 
<div class="col-md-12">
<!-- BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
	<div class="row">
		<div class="col-md-12">
<div class="portlet light ">		 
<div class="portlet-body">
	<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
<div class="tab-pane active" name="tab_1_1">
<?php 
 

if($msg !="")
{
/*echo '<div class = "alert alert-success alert-dismissable" style="background:lightgreen">
   <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	  &times;
   </button>
	
   <font color="#006699">'. $msg. '</font>
</div>'; 	*/
}

  
?>


<div class="row"> 
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff">  Water Payment Receipts	</font>
	</div>		
 
<div class="col-md-12">  &nbsp;  </div>	
<form action="<?=site_url('water/waterReceipts/');?>" enctype="multipart/form-data" method="post"> 
  
<div class="col-md-12">
	<div class="form-group">
			<input type="hidden" name="landlord_id" value="">
			
			<label class="control-label"> Select Property </label>
			<select class="selectpicker"  data-live-search="true"  name="property_id" id="property" onChange="this.form.submit(this.value)" >
					<option value=""> None  </option>
					<?php foreach($property->result() as $row){?> 
					<option value="<?=$row->id ?>"><?=$row->property_name?> </option>
					<?php 
					if(!empty($water_unit_cost)){ }else{$water_unit_cost=$row->water_unit_cost;}
					}?> 
			</select>
	</div>  
</div>
  
<?php $salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
	$enct =base64_encode(do_hash($selected_property . $salt,'sha512') . $salt);?> 
	 
</form> 
<div class="col-md-12"> &nbsp; <hr/>  </div>
<form action="<?=site_url('water/waterReading');?>" enctype="multipart/form-data" method="post"> 
 <div class="col-md-12">
 <input type="hidden" name="id_no" value="<?=$id?>">
<table  class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>   
							<th> # </th>
							<th> Date Paid </th>
							<th> Name </th>
							<th> Amount </th> 
							<th> Payment Mode</th>      
							<th> Payment Mode Code</th>      
							<th> Balance(KES.)</th>    
							<th> &nbsp;  </th>    
							<th> &nbsp;  </th>    
						</tr>
					</thead>
					<tbody>
				<?php $i=1; $amount=0; $payment_mode=""; $name=""; $payment_mode_code=""; $balance=0;
					foreach($water->result() as $r){
                      
					$amount=$r->amount;  $payment_mode=$r->payment_mode; $payment_mode_code=$r->payment_mode_code; $balance=$r->balance; 
					foreach($tenants->result() as $row){
						if($row->id==$r->tenant_id){ $name=$row->first_name. '  '. $row->middle_name.' '.$row->last_name;}
					}
					?> 
			  <tr>
			  <td> <?=$i?> </td>
			  <td> <?=$r->date_paid?> </td>
			  <td>  <?=$name?> </td>
			  <td>  <?=$amount?>  </td> 
				<td> <?=$payment_mode?> </td>
				<td> <?=$payment_mode_code;?> </td> 
				<td> <?=$balance?> </td> 
<?php $salt = sha1('2ab'); $salt = substr($salt, 0, 10); $enct =base64_encode(do_hash($r->id . $salt,'sha512') . $salt);?>				
				<td> <a href="<?=base_url();?>water/waterReceipt/<?=$enct?>"> <i class="fa fa-file-o"></i> View Receipt </a></td>
			    <td id="edit_<?=$i?>">
					<a href="javascript:;" onclick="editPayment('<?=$r->id?>','<?=$name?>')"><i class="fa fa-edit"> </i> Edit</a>
				</td>
			</tr> 
			<?php  $i++; }?>						
			</tbody>
		</table>  
</div>
  </form> 
</div>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
</div>
</div>
<!-- END CONTAINER --> 
 <div id="edit_payment" class="modal fade" tabindex="-1" data-width="800" aria-hidden="true"> 

	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Edit Receipt Details </b> </font></h4>
	 </div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
			<!--<form action="water/editWaterReceipt" method="post" onsubmit="return validate_edit()">
				-->
				<div class="col-md-6">
				<p> <label class="control-label"> Name </label> <br/>
							<input   class="form-control" type="hidden" id="editing_id" name="edit_id" >
							<input   class="form-control" type="hidden" id="editing_tenant_id" name="tenant_id" >
							<input   class="form-control" type="text" name="from"   id="edit_user_name" readonly onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" >
							<input   class="form-control" type="hidden" name="selected_property" id="selected_property"    value="<?=$selected_property?>" >
									   
								 
								<label id="edit_error1">					</label> 
						</p> 
						<p> <label class="control-label"> Payment Method </label>
								<select class="form-control" type="text"  name="pay_mode" id="edit_pay_mode" onchange="disable_method()">
									<option value="Cash"> Cash </option>
									<option value="Cheque"> Cheque </option>
									<option value="Mpesa"> Mpesa </option>
									<option value="Bank Receipt"> Bank Receipt </option>
								</select> 
						<label> </label> 
						</p>
						
						<p> 
						 <label class="control-label"> Date </label>
							<input type="text" required readonly class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value=""   onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" name="date" id="edit_pay_date">
							<label id="edit_error3">					</label> 
						</p>
					</div>
					<div class="col-md-6">	
						<p> <label class="control-label"> Amount Paid </label>
							<input   class="form-control" type="text" name="amount"  id="edit_amount" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);">
							<label id="edit_error4">					</label> 
						</p> 					
						 <p> <label class="control-label">Receipt/Cheque No </label>
							<input   class="form-control" type="text" name="receipt_no"  id="edit_receipt_no" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);">
							<label id="edit_error2">					</label>  
						</p> 
							 	 
					</div> 
		</div> 
		<div class="modal-footer" ><center> <span id="status_message" style="float:left"> </span>
	<button type="submit" onclick="validate_edit()" class="btn green" <?=$disabled;?>> Save Changes </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
	</center>
</div>	
<!--</form>--> 	 
</div>			
	</div>	

	</div> 
	
 <div id="add_new_reading" class="modal fade" tabindex="-1" data-width="600">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Warning Pop Message </b></h5>
				<hr/>
					<p>
					   The current reading will be re-set to Zero (0) to allow this month's reading entry. Proceed?
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" onclick="confirm_new_reading()" class="btn green" <?=$disabled?>>&nbsp; Yes &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn red">&nbsp; No  &nbsp; </button> 
		</center>
	</div>
</div>

  <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">   Water Readings Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="">
				   <?php  echo $msg;?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
  

<script type="text/javascript">
 
function validateMail() {
     
    var x = $("#Email").val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
     $("#Email").val('');

        return false;
    }
}
function delete_landlord(id)
	{ 
	        $("#id_no").val(id);
			$("#delete_landlord").modal('show');
	 
	} 
	
	function add_new_reading(id)
	{
	        $("#id_no").val(id);
			$("#add_new_reading").modal('show');
	 
	} 
 function confirm_new_reading()
 { 
		$.ajax({
		url:'<?=base_url();?>tenants/add_new_reading/', 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data);  
			var data = obj.data; 
			if(obj.result=="ok")
			{
			   window.location.replace("<?=base_url();?>tenants/waterManagement");
			}
			else
			{
				$("#error").html('<font color="red"> Reading was not re-set successfully! This is done once a month </font>');
			}
		}});
		
} 
 
$(document).ready(function(){ 
 
  checkPrivilege();   
  var msg="<?php echo $msg;?>";   
  var prop="<?php echo $selected_property;?>";   
  $("#property").val(prop);
  var saving_success="<?php echo $msg;?>";
	if(!saving_success || saving_success=="")
 {

 }
else{
       $("#data_saving_success").modal('toggle');   
    } 
});	 
 
 $("#confirm_del").click(function()
	{  
	//$("#delete_landlord").modal('hide');
	var id=$("#id_no").val();  
	$.ajax({
		"url":"<?=base_url();?>tenants/removeLandlord/"+id,
        "type":"post",
		"success":function(data)
		{
			var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
				 location.reload(); 
			 }
			 else
			 {
				$("#message").html("<font color='red'> Error! Landlord Not removed. Try again later</font>");
				$("#success").modal('show');
			 }
		}
		
	})
	
 	});
	
  
	
function updateWaterUnitCost(id)
 { 
	var unit_cost=$("#water_unit_cost").val();
 $.ajax({
	 url:"<?=base_url();?>tenants/updateWaterUnitCost",
	 type:"POST",
	 async:false,
	 data:{
		 'property_id':id,
		 'unit_cost':unit_cost
		},
	 success:function(data)
	 {
		var obj=JSON.parse(data);
        if(obj.result=="ok")
		{  
			 setTimeout(function() { $("#errorMsg").html("<font color='green'><b>"+obj.msg+"</b> </font>"); }, 1000);
			  setTimeout(function() { $("#errorMsg").html(""); }, 5000);
			 
		}
		else{
			 setTimeout(function() { $("#errorMsg").html("<font color='red'><b>"+obj.msg+"</b> </font>"); }, 1000);
			 setTimeout(function() { $("#errorMsg").html(""); }, 5000);
			  
		}		
	 }
	 
	 });	 
	 
 } 

 function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/1",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			if(obj.add==0)
			{  
				 document.getElementById('save').disabled = true; 
		      //$("#save_btn").html("<font color='' onclick=\"alert('You have no privilege to add property')\"> Save Details </font>"); 
			} 
		 
		}
	 })
 } 
 		
function doneEdit()
  {
	    var pay_mode=$("#edit_pay_mode").val();
		var date=$("#edit_pay_date").val();
		var amount=$("#edit_amount").val();  
		var no=$("#editing_id").val();  
		var tenant_id=$("#editing_tenant_id").val();  
		var code=$("#edit_receipt_no").val();  
		var selected_property=$("#property").val();  
		  
	  $.ajax(
		{
		url:"<?=base_url();?>water/editWaterReceipt",
		type:"POST", 
		data:{
			 'edit_id':no,
		     'tenant_id':tenant_id,
			 'pay_mode':pay_mode, 
		     'receipt_no':code,
		      'date':date,
			  'amount':amount, 
			  'selected_property':selected_property 			
		   },
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				  $("#status_message").html("<font color='green'>Changes made successfully</font>");
				   setTimeout(function(){ 
                      }, 2000); 
					  document.location.reload(true);
			 }
			 else{
				  $("#status_message").html("<font color='red'>Changes was not made successfully</font>");
			 }
			 
			 
		}
	})
  }	
		
function editPayment(id,name)
  {
	    disable_method();
	  $.ajax(
		{
		url:"<?=base_url();?>water/payment_details/"+id,
		type:"POST", 
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 var data=obj.data;
				for(var i=0; i<=data.length; i++)
				{
					var payment=data[i];
					var tenant_name=name;
					var no=payment['id'];  
					var amount=payment['amount'];
					var tenant_id=payment['tenant_id'];
					var pay_mode=payment['payment_mode']; 
					var code=payment['payment_mode_code'];  
					var date=payment['date_paid'];   
					$("#edit_pay_mode").val(pay_mode);
					$("#edit_pay_date").val(date);
					$("#edit_amount").val(amount);  
					$("#editing_id").val(no);  
					$("#editing_tenant_id").val(tenant_id);  
					$("#edit_receipt_no").val(code);  
					$("#edit_user_name").val(tenant_name);  
					$("#edit_payment").modal('show');					
				}
				disable_method();
			 }
			 else
			 {
				alert('You cannot edit this receipt'); 
			 }
		}
  }
)
}

 function disable_method()
  { 
	var value=document.getElementById("edit_pay_mode").value;  
	 if(value=="Cash")
	  {
		 
	document.getElementById('edit_error2').value ="";
	document.getElementById('edit_receipt_no').value ="--";
	document.getElementById('edit_receipt_no').disabled = true;
      
	  }
	  else
	  {
			document.getElementById('edit_receipt_no').value ="";
			document.getElementById('edit_receipt_no').disabled = false;
	  }  
}  
</script>
