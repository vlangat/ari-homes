<div class="page-content">
<div class="container">

<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Tenants</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">All Tenant</a>
		<i class="fa fa-circle"></i>
	</li> 
</ul>

<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="row">
		<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
	<div class="portlet light portlet-fit ">
	<div class="portlet-body">
	<div class="row" style="min-height:500px;">  
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
		<font color="#ffffff"> All Tenants </font>
	</div> 
<div class="col-md-12"> &nbsp;  </div>	 
<form name="form1" method="post" action="<?=site_url();?>tenants/viewTenant" onSubmit="return validate();">
<input type="submit" class="btn green" id="view_tenant"  value="View /Edit Tenants Details"> 
<hr/>
<input type="hidden" id="countr"/>
<table class="table table-striped table-hover table-bordered" id="table1" >
<thead>
	<tr>
	<th> # </th>
	<th> Tenant  </th>
	<th> Property </th>
	<th> Unit Category </th>
	<th> House No </th>
	<th> Floor </th>
	<th> Mobile No </th>
	<th> Rent Due </th>
	<th> Statement </th>
	</tr>
</thead>
<tbody id="tenant_details" style="min-height:500px;">
 <tr><td colspan='9' align='center'> <font color='green'> Loading please wait...</font> </td></tr> 
				
</tbody>
</table>
</form>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
 
<!-- RESPONSIVE MODAL -->
<div id="rent_collection" class="modal fade" tabindex="-1" aria-hidden="true"  data-width="400">
  
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Rent Collection Details </b> </font></h4>
		  <span id="rent_for">  </font>
	</div>
<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12"> 
		<p>
			<label class="control-label"> Amount (KES) <font color='red'> * </font> </label> 
			<input   class="form-control"   type="text" id="amount" name="amount"> 
			<input   class="form-control"   type="hidden" id="id_no"> 
			<input   class="form-control"   type="hidden" id="name"> 
			<input   class="form-control"   type="hidden" id="property_name"> 
			<input   class="form-control"   type="hidden" id="floor_no"> 
			<input   class="form-control"   type="hidden" id="house_no">			
		</p>
	<p> 
		<label class="control-label">Payment Method  <font color='red'> * </font></label>
		<select class="form-control" name="mode" id="mode"> 
			<option> Cash </option>
			<option> Mpesa </option>
			<option> Cheque </option>
			<option> Bank Receipt </option>
		</select>
	</p>
  
		<p>
		<label class="control-label">Mpesa, Cheque, Bank receipt Number  <font color='red'> * </font> </label>
		<input   class="form-control" type="text" id="transaction_no" name="transaction_no"/> </p>
		<p>
 
		<p>
		<label class="control-label">Paid on</label>
		<input   class="form-control" type="text" id="date" placeholder="<?php echo date('Y-m-d');?>" value="<?php echo date('Y-m-d');?>" name="paid_on"> </p>
		<p>
 
		<p>
		<label class="control-label"> Description </label>
		 
		<textarea class="form-control" id="description" name="description" >  </textarea>
		</p>
</div>
</div>  <span id="status_msg">   </span>
</div>
<div class="modal-footer" >
		<input type="submit" class="btn green" id="save_data" value="Save">
		<button type="submit" class="btn red"> Save &amp; Print Receipt </button>
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div> 
</div>
</div>
 <!---END OF RESPONSIVE MODAL-->
 
 
<script language="javascript">
function validate()
{	
var formobj = document.forms[0];
var counter = 0;
for (var j = 0; j < formobj.elements.length; j++)
{
    if (formobj.elements[j].type == "checkbox")
    {
        if (formobj.elements[j].checked)
        {
            counter++;
        }
    }       
}
if(counter==0){  
    alert("Please select at least one.");
	return false;
 }
else if(counter>1)
{
 alert("Please select one tenant.");
	return false;	
}

return true;

}


$(document).ready(function(){
 checkPrivilege();  

	$("#collect").click(function(){ 
	validate();

	var id = [];
	$.each($("input[name='checkbox[]']:checked"), function(){            
	id=($(this).val());
	}); 
	 
		$.ajax({
		"url":"<?=base_url();?>tenants/getTennantDetails/"+id,
        "type":"post",
		"success":function(data)
			{
				var obj = JSON.parse(data); 
				var data = obj.data;
				for(var i=0; i<data.length; i++)
				{
					var tennant = data[i]; 
					$("#id_no").val(id);
					var name=tennant['first_name']+" "+tennant['middle_name']+" "+tennant['last_name'];
					var property=tennant['property_name'];
					var house=tennant['house_no'];
					var floor=tennant['floor_no'];
					$("#property_name").val(property);   
					$("#house_no").val(house);   
					$("#floor_no").val(floor);
					$("#rent_for").html(name); 
                                        if(tennant['first_name']||tennant['last_name']||tennant['middle_name']){  }else{name=tennant['company_name'];   $("#rent_for").html(name);}  
					$("#name").val(name); 

 
				}
 
				 if(id==""){return false;}else{
				        $("#rent_collection").modal('show');
                                     }
			}
	});
		
 
}); 	

 $("#save_data").click(function(){  
	 var id=$("#id_no").val();
	 var name=$("#name").val(); 
	 var amount=$("#amount").val(); 
	 var mode=$("#mode").val(); 
	 var transaction_no=$("#transaction_no").val(); 
	 var property_name=$("#property_name").val(); 
	 var floor_no=$("#floor_no").val(); 
	 var house_no=$("#house_no").val();
	 var business_no=0; 
	 var paid_on=$("#date").val();
	 var description=$("#description").val();
	 if(! amount || ! transaction_no || ! mode){  $("#status_msg").html("<font color='red'> Fill in all the required inputs </font>");  return false;}
	 $("#status_msg").html("<font color='blue'> Updating....</font>");
	$.ajax(
	{
		url:"<?=base_url();?>rent/payment/",
		type:"POST",
		async:false,
		data:
		{
		'id':id,
		'name':name,
		'transaction_no':transaction_no,
		'business_no':business_no,
		'property_name':property_name,
		'house_no':house_no,
		'floor_no':floor_no,
		'amount':amount,
		'mode':mode,
		'paid_on':paid_on,
		'description':description
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 { 
                          $("#transaction_no").val('');
		           $("#amount").val('');
		           $("#description").val('');
					 $("#status_msg").html("<i class='fa fa-check'> </i><font color='green'> Changes Saved </font>");
					 setTimeout(function(){
								 $('#rent_collection').modal('hide');
                                                                  $("#status_msg").empty();
                      }, 2000); 
			 }
			 else
			 {
				 //$("#status_msg").html(" <font color='red'> Not updated. You have made no changes to save </font>");
			 }
		}
		
	})
 });
 
 
});
  
  
  function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/2",
		type:"POST", 
		async:false,
		success:function(data)
		{  
			var obj=JSON.parse(data);  
			var data = obj.data;  
			 
			if(obj.view==0){ 
				document.getElementById('view_tenant').disabled = true;  
			}
			 
		}
	 })
 } 
 
function getChecked(){ 
 var formobj = document.forms[0];

var counter = 0;
for (var j = 0; j < formobj.elements.length; j++)
{
    if (formobj.elements[j].type == "checkbox")
    {
        if (formobj.elements[j].checked)
        {
            counter++;
        }
    }       
}

alert('Total Checked = ' + counter);
}

function get_tenants()
{
		var content=""; 
		$("#tenant_details").html("<tr><td colspan='9' align='center'> <font color='green'>Loading data...please wait</font> </td></tr>");
		$.ajax({
		url:'<?=base_url();?>tables/tenants/', 
  headers: {
     'Cache-Control': 'no-cache, no-store, must-revalidate', 
     'Pragma': 'no-cache', 
     'Expires': '0'
   },		
		type: 'POST', 
		async:false, 
		success:function (data)
		{  
			var obj = JSON.parse(data); 
			var data = obj.data; var count=0; var count=parseInt(data.length);$("#countr").val(count);    
           if(count<=0){ $("#view_tenant").hide(); } 
			if(count >0){ 
				for(var i=0; i<data.length; i++)
				{
					var p = data[i];  var tenant_name=""; var floor="";
					var enct=(p['enct']); 
					if(p['tenant_type']=="residential"){ 
					tenant_name=p['first_name'];
					}else {tenant_name=p['company_name']; } 
					floor_no=p['floor_no'];					
		            if(parseInt(floor_no) == 0){ floor_no="";} 
					content=content+"<tr><td><input name='checkbox[]' type='checkbox' id='checkbox[]' value='"+p['id']+"'></td> "+
					"<td>"+tenant_name+"</td> <td>"+p['property_name']+"</td> <td>"+p['category_name']+"</td><td> "+p['house_no']+"</td><td> "+floor_no+"</td> <td>"+p['mobile_number']+"</td>  <td>"+p['rent_due']+"</td>"+
					"<td class='center'> <a data-toggle='modal'  id='payment_history'  href='<?=base_url();?>rent/statement/"+enct+"' ><i class='fa fa-file-o'> </i> View Statement</a> </td> </tr>"+
					count++;
				} 
				 $("#tenant_details").html(content); 
			}
			else{
					$("#tenant_details").html("<tr><td colspan='9' align='center'>No tenants available</td></tr>");
				} 
			$('#table1').DataTable();
		}
	});
 
}
 
 $(document).ready(
 function(){   
	get_tenants();
 });
</script> 