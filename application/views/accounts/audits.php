<div class="content-top">
    <div class="box box-primary">
        <div class="box-header with-border">
            <br>
            <h1 class="box-title"><i class="fa fa-edit"></i> List of my audits</h1>
            <div class="pull-right padding">
                <a href="/manager/index/new_audit" class="btn btn-danger"><i class="fa fa-plus"></i> New Audit</a> &nbsp;&nbsp;&nbsp;
                <a href="javascript:void(0);" class="btn btn-info"><i class="fa fa-eye"></i> View</a> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
            </div>

        </div>

        <div class="box-body">
            <table class="table table-striped table-hover table-bordered" id="table">
					<thead>
						<tr>
							<th> #  </th>      
							<th> Date Created </th> 
							<th> User Name  </th> 
							<th> Email </th> 
							<th> Message </th> 
							<th> Status </th>  
						</tr>
					</thead><tbody></tbody>
            </table>
        </div>
        <br>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	    var table= $('#table').DataTable({
              destroy: true,
              responsive: true,
               "ajax":{
                   url:'/tables/audits',
                   type: 'get'
               }
               language: {
                    searchPlaceholder: "Search records.."
                },
                scrollY:        "320px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         true,
                
    	});

	});
</script>