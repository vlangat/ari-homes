<?php $count1=0;$count2=0;$count3=0;$count4=0;?>
<div class="page-container">
<!-- BEGIN CONTENT -->
 
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE CONTENT BODY -->
	<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="<?=base_url();?>">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		Support
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Queries</span>
	</li>
</ul>
 
<div class="page-content-inner">
<div class="inbox">
<div class="row"> 

<div class="col-md-12">
  <div class="col-md-12" style="background:#006699;padding:6px;">
			<font color="#ffffff"><strong> &nbsp; Support    </strong> </font> 
	   </div>
<div class="inbox-body">

<div class="inbox-header">
	<h1 class="pull-left"> <font id="smss_title" size="3"> <font> </h1>
	 
</div>
			 <form action="<?=base_url();?>support/send_query" method="post" onsubmit="return validate()">
		 <h4 class="modal-title"> <strong>  Make a New Query  </strong> </h4>
		 <hr/>
		 
         <input type="hidden" value="" class="form-control" id="msg_id">
    <div>
     <label > Subject: </label> 
        <div> 
	 		<input type="text" class="form-control" id="subject" name="subject">
        </div>
    </div>   
	<p>   </p>
    <div class='inbox-form-group'> <label class='control-label'> Description   </label>
        <textarea class='inbox-editor inbox-wysihtml5 form-control' id='message' name='message' rows='6'></textarea>
    </div>
    <div>
	<p id="counter"> </p> <p id="error"> </p>
    </div> 
      
		<button class='btn green' onclick='send()'>
	<i class='fa fa-check'></i> Submit Query </button>  
	</form>
	<hr/>
	<!--<div class="inbox-content">  -->

	<table  style="font-family:verdana" class="table table-striped table-hover table-bordered" id="sample_editable_1">
		<thead>
			<tr>  <th> # </th>  <th> Date </th> <th> Subject </th> <th> Description  </th>  <th> Status </th>   </tr>
		</thead>
		<tbody>
			  <?php  $x=1; foreach($queries->result() as $r){?>
					<tr onclick="response(<?=$r->id?>)"> <td><?=$x?> </td> <td> <?=$r->date_sent?> </td> <td> <?=$r->subject?>   </td>  <td>  <?=substr($r->description,0,18).'...'?> </td> <td>  <?php $msg="Pending"; $t_color="#fff"; $color="red";  $status=$r->status; if($status==1){ $msg="Pending"; $color="red"; }else if($status==2){ $msg="Solved"; $color="green"; }else if($status==3){ $msg="New"; $color="yellow"; $t_color="red"; }    ?> <font style="background:<?=$color?>;color:<?=$t_color?>"> <?=$msg?> </font> </td>  </tr>
			  <?php $x++; } ?>
		</tbody>
	</table> 
	<!--	</div>-->
	</div>
</div>
</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div> 

 <div id="confirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Confirm Deletion</h4>
			</div>
			<div class="modal-body">
				<p>  Do you want to remove the selected Messages? </p>
			</div>
			<div class="modal-footer">
				<button class="btn default" data-dismiss="modal" aria-hidden="true">No</button>
				<button data-dismiss="modal" id="confirmDelete" class="btn blue">Yes</button>
			</div>
		 
</div>

 
<!-- END CONTAINER -->
<script type="text/javascript">
function validate(status)
{  	 
	var subject=$("#subject").val(); 
	var body=$("#message").val();  
	if(!subject){ $("#error").html("<font color='red'> Subject is Required </font>"); $("#subject").focus(); return false;}
	if(!body){ $("#error").html("<font color='red'>Query is empty. Write your query description </font>"); $("#message").focus(); return false;}
	//if(/^[a-zA-Z0-9- ]*$/.test(subject) == false){ $("#subject").focus(); $("#error").html("<font color='red'>Subject contains   illegal characters </font>");  return false; } else{$("#error").empty();} 
	//if(/^[a-zA-Z0-9- ]*$/.test(body) == false){ $("#message").focus(); $("#error").html("<font color='red'>Message contains illegal characters </font>");  return false; } else{$("#error").empty();} 
    return true;
 
}

function response(id,id2)
{  	 
	var id2="";
	 window.location="<?=base_url();?>support/response/"+id+"/"+id2;
 
}
 </script>