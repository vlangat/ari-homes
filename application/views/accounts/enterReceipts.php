<div class="page-content">
<div class="container">

<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Tenants</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Receipts</a>
		<i class="fa fa-circle"></i>
	</li> 
</ul>

<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="row">
		<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
	<div class="portlet light portlet-fit ">
	<div class="portlet-body">
	<div class="row" style="min-height:500px;">  
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
		<font color="#ffffff"> Receive Payment </font>
	</div> 
<div class="col-md-12"> &nbsp;  </div>	 
<form name="form1" method="post" action="<?=site_url();?>rent/receiptEntry" onsubmit="progress(this)">
  <input type="hidden" value="<?=$msg?>" name="message">
<table class="table table-striped table-hover table-bordered" id="table1" >
<thead>
	<tr>
	<th> # </th>
	<th> Tenant  </th>
	<th> Property </th>
	<th> Unit Category </th> 
	<th> House No </th> 
	<th> Expected Rent </th> 
	<th> Amount Received</th> 
	<th> PayMode</th> 
	<th> Receipt No</th> 
	</tr>
</thead>
<tbody  style="min-height:500px;">
 <?php $x=1; foreach($tenants->result() as $r){?>
 <tr>
 <td><?=$x?></td>
 <td><?=$r->first_name?></td>
 <td><?=$r->property_name?></td>
 <td align='center'><?=$r->category_name?></td> 
 <td align='center'><?=$r->house_no?></td>
 <td><?=$r->rent_due?></td>
 <td align='center'>
	<input type="hidden" value="<?=$r->id?>" name="id_<?=$x?>"> 
	<input type="number" name="amount_<?=$x?>" value="" class="form-control">
 </td>
 <td>
	<select  name="pay_mode_<?=$x?>" class="form-control">
		<option value=""> Select  </option>
		<option> Cash </option>
		<option> Cheque </option>
		<option> Mpesa </option>
		<option> Bank Receipt </option>
	</select>
 </td>
  <td>
  <input type="text" class="form-control" name="receipt_no_<?=$x?>"/>
 </td>
 
 </tr> 
 <?php $x++; } ?>			
</tbody>
</table>
<input type="hidden" name="total_number" value="<?=$x?>">
&nbsp; &nbsp; <input type="submit" name="submit" value="Save Changes" class="btn btn-success">
</form>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT --> 
<div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
<div class="modal-header">
	<b style="font-size:20px;color:green">Success Message </b> 
</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="err">
				    <?php echo $msg; ?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<!--<button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> -->
		&nbsp;
	</div> 
</div>
 
 
<script language="javascript"> 
$(document).ready(function(){  
  var saving_success="<?php echo $msg;?>";
 
  if(saving_success =="")
 {  }
else{  
		$("#data_saving_success").modal('toggle');
		setTimeout(function()
		{
			 $("#data_saving_success").modal('hide'); 
		}, 3000);  
	}
}); 
	function progress()
	{
		$("#err").html("<font color='blue'>Saving, please wait...</font>");
		$("#data_saving_success").modal('toggle'); 
	}
  
</script> 