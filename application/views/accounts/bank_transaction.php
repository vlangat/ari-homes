<!-- BEGIN PAGE CONTENT BODY -->
<script>
 
	function validate_edit()
	{    
		var name=$("#edit_user_name").val();
		var pay_mode=$("#edit_pay_mode").val();
		var amount=$("#edit_amount").val();
		var receipt_no=$("#edit_receipt_no").val();
		var pay_date=$("#edit_pay_date").val(); 
		if(name==""||name==null){ $("#edit_error1").html("<font color='red'> Tenant Name   is empty </font>");$("#edit_user_name").focus(); return false;}
		if(amount==""||amount==null){ $("#edit_error4").html("<font color='red'> Amount field is empty </font>"); $("#edit_amount").focus(); return false;}
		if(pay_date==""||pay_date==null){ $("#edit_error3").html("<font color='red'> Date field is empty</font>"); $("#edit_pay_date").focus(); return false;}
		//if(receipt_no==""||receipt_no==null){ $("#edit_error2").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>"); $("#edit_receipt_no").focus(); return false;}
		if(/^[a-zA-Z0-9- ]*$/.test(amount) == false){ $("#edit_amount").focus(); $("#edit_error4").html("<font color='red'>Amount contains illegal characters </font>");  return false; }else{$("#edit_error4").empty();}
		if(/^[a-zA-Z0-9- ]*$/.test(name) == false){ $("#edit_user_name").focus(); $("#edit_error1").html("<font color='red'>Name  should not have special characters </font>");  return false; } else{$("#edit_error1").empty();} 
		if(/^[a-zA-Z0-9- ]*$/.test(receipt_no) == false){ $("#edit_receipt_no").focus(); $("#edit_error2").html("<font color='red'>Receipt No should not have special characters </font>");  return false; } else{$("#edit_error2").empty();} 
		return confirm("Update details for "+name+"?");
	}
</script>

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Rent  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Bank Account Transactions</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN EXAMPLE TABLE PORTLET-->

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title"> 
			<?php $owner=""; $acc_name=""; $bank_name=""; $branch="";
		foreach($bank_details->result() as $rows){
		$owner=$rows->owner; $acc_name=$rows->acc_name;$acc_no=$rows->acc_no; $bank_name=$rows->bank_name; $branch=$rows->branch;
		}
?>			 
<div class="row">
<div class="col-md-12" style="background:#1bb968;padding:1px;">
	<h5> <font color="#ffffff">  &nbsp; &nbsp; Bank Transactions for Account No <b> <?=$acc_no?></b>, Account Name <b> <?=$acc_name?></b>, Branch <b> <?=$branch?></b> </font></h5>
</div>

<p> &nbsp; </p>  
 
 <form> 	  
 <div class="col-md-12">  
<table class="table table-striped table-hover table-bordered"  id="sample_editable_1">
<thead>
	<tr>
		<th>#</th> 
		<th> Date </th>
		<th> Amount Received </th>
		<th> Payment Method</th>
		<th> Transaction Code</th>  
				
	</tr>
</thead>
<tbody>
   
  <?php $x=1;  $total=0; 
foreach($tenant_transaction->result() as $r){
	$payment_mode_code="";
	foreach($bank_details->result() as $rows){
		$owner=$rows->owner; $acc_name=$rows->acc_name;
	if($rows->id==$r->bank_account_id){ $total=$total+$r->amount; 
	if($r->payment_mode_code==""){  $payment_mode_code="--";}else{ $payment_mode_code=$r->payment_mode_code;} 
		
		?>
	<tr>
		<td><?php echo $x?> </td>  
		<td class="center"> <?=$r->date_paid?>  </td> 
		<td> <?=$r->amount?>  </td> 
		<td>  <?php  echo $r->payment_mode; ?> </td>  
		<td>  <?php  echo $payment_mode_code; ?> </td>
	 </tr>
	<?php $x++;  
	} ?>
	  
	<?php
	}
}?>
	</tbody>
</table> 
 
 </form>
 <p> &nbsp; </p>
</div> 
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div> 
<!---Edit payment-->


<div id="edit_bank" class="modal fade" tabindex="-1" data-width="800" aria-hidden="true">  
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Edit Bank Details </b> </font></h4>
	 </div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
			<form action="<?=base_url();?>rent/receive_pay" method="post" onsubmit="return validate_edit()">
				<div class="col-md-6">
				<p> <label class="control-label"> Name </label> <br/>
							<input   class="form-control" type="hidden" id="editing_id" name="edit_id" >
							<input   class="form-control" type="hidden" id="editing_tenant_id" name="tenant_id" >
							<label id="edit_error1"></label> 
				</p> 
				<p> <label class="control-label"> Payment Method </label>
						<select class="form-control" type="text"  name="pay_mode" id="edit_pay_mode" onchange="disable_method()">
							<option value="Cash"> Cash </option>
							<option value="Cheque"> Cheque </option>
							<option value="Mpesa"> Mpesa </option>
							<option value="Bank Receipt"> Bank Receipt </option>
						</select> 
				<label> </label> 
				</p> 
				<p> 
				 <label class="control-label"> Date </label>
					<input type="text" required class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value=""   onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" name="date" id="edit_pay_date">
					<label id="edit_error3">					</label> 
				</p>
			</div>
			<div class="col-md-6">	
				<p> <label class="control-label"> Amount Paid </label>
					<input   class="form-control" type="text" name="amount"  id="edit_amount" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);">
					<label id="edit_error4">					</label> 
				</p> 					
				 <p> <label class="control-label">Receipt/Cheque No </label>
					<input   class="form-control" type="text" name="receipt_no"  id="edit_receipt_no" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);">
					<label id="edit_error2"> </label>  
				</p> 
							 	 
			</div> 
		</div> 
		<div class="modal-footer" ><center> <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" <?=$disabled;?>> Save Changes </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
	</center>
</div>	
</form> 	 
</div>			
	</div>	

</div> 
	

<!--->
 <!-- responsive -->
<div id="add_new" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			 
			<h5><b> Specify other Payment </b></h5>
			<hr/>
			<p>
			<label class="control-label">Name/Description </label>
			<input   class="form-control" type="text" placeholder="electricity" id="amenity_name"> </p>
			<p>
			</div>
		</div> 
	</div> 
<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_new_one" <?=$disabled;?>> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>


<div id="success" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b style="font-size:20px;color:green">    Warning Message </b></h5>
				<hr/>
				<p id="success_msg">
				   
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
<div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">    Success Message </b> 
		</div>
	<div class="modal-body">
					<div class="row">
					<div class="col-md-12"> 
					 
					<p id="">
					   <?php if($this->session->flashdata('temp')){ echo $this->session->flashdata('temp');}  ?>
					</p>
					</div>
				</div>    
	</div>
	<div class="modal-footer" >  
		<!--<button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> -->
		&nbsp;
	</div> 
</div>
 
<!-- END CONTENT --> 
<!-- END CONTAINER --> 