<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Payment </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Receipt  </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">

	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
			<div class="portlet-title">
			<div class="col-md-12" style="background:#006699;padding:6px;">
				<font color="#ffffff"> Payment Receipt </font>
	</div>
 
<div class="col-md-2" >
</div>
<div class="col-md-8" >
   <div class="portlet light "> 
   
		<div class="portlet-body"> 
<?php $sms=0; $total_sms=0; $value1=0; $bal=0;$credit=0; $sms_cost=0; $value2=0; $paid_cost=0; $users=0; $cost=0; $package_name=""; $years=0; $package_cost=0; $vat=0; foreach($packages->result() as $p){ if($package_id==$p->id){  $package_name=$p->package_name; $value=$p->amount;} }?>
	
<?php   $users=0;  
  foreach($paid_packages->result() as $d){ 
		 foreach($packages->result() as $pac)
		 {
		 if($d->package_id==$pac->id){  
				 if($pac->package_type=='sms')
				 { 
					$total_sms=$pac->value;  $sms_cost=$pac->amount;
				 }
				 else
				 {   
					 $package_name=$pac->package_name; 
					 $years=$pac->value; $users=$d->paid_users;
					 $cost=$pac->amount; $paid_cost=$d->paid_amount;
					 $bal=$d->balance;
				 }  
				} 
		}
	}
	
	if($total_sms==0)
	{
		foreach($paid_sms->result() as $p)
		 {
			$id=$p->package_id; 
			foreach($packages->result() as $s)
			 {
				if($p->package_id==$s->id)
				{  
				 if($s->package_type=='sms')
				 { 
					$total_sms=$s->value;  $sms_cost=$s->amount;
				 } 
				}
			}
		}
  }
?>
			 
		<table border="0" id="success_payment"  width="90%" class="table table-striped table-hover"> 
			<tr>
				<td>
					<img src="<?=base_url();?>images/ARI-Logo.png" width="150" > 
					<h4> <strong> Receipt Number <?=$transaction_code ?> </strong> </h4>	
				</td> 
				<td>  </td>
				<td align="right">
				&nbsp; 
				</td>
			</tr>  
			<tr>
			<td>
			<p>  </p>		
			<ul style="font-size:14px;padding:4px;list-style-type:none">
			<li>	<strong> Paid By:	</strong></li> 
			<li>	<?=strtoupper($this->session->userdata('first_name'))?> <?=strtoupper($this->session->userdata('last_name'))?>	</li>
			<li>	<?=$this->session->userdata('email')?>	</li>
			<li>	<?=$this->session->userdata('company_name')?>	</li>
			<li>	<?=$this->session->userdata('address')?>	</li>
			<li>	<?=$this->session->userdata('phone')?>	</li> 
			 </ul>
		</td>
		<td> </td> 
		<td> 
		<ul style="font-size:14px;text-align:right; list-style-type:none">
			<li> <strong> Paid To: </strong></li>
			<li>  ARI Limited </li>
			<li>  P.O.BOX 5710-00100 </li>
			<li>  Nairobi, Kenya. </li>
			<li>  +254 725 992 355 </li> 
			<li>  AACC Building 1<sup>st</sup> Floor</li>
			<li>  Waiyaki Way, Nairobi. </li> 
		</ul>
		 
		</td>
	</tr>	
	<tr>
		<td><strong> Amount Received: </strong> <?=$amount=$paid_cost+$sms_cost+(0-$bal)?> </td>	<td> <strong>Payment Method: </strong><?=strtoupper($pay_mode)?>  </td>	<td align="right"> <strong> Date: </strong> <?=$transaction_date?>  </td>	
	</tr>	
	</table>
	
	<table border="0" class="table table-striped table-hover" id="success_payment"  width="90%"  style="border:1px solid lightgrey"> 
		<tr height="45" bgcolor="#DFDAD9"> 
			<td>  <strong>  Particulars </strong>  </td> <td align="right"> <strong>   Amount &nbsp;&nbsp;  </strong>  </td>
		</tr> 
		<?php if($years>0 && $users>0)
		      {?>
		<tr height="40"> <td> <?=$years. ' Year(s), '.$users.' User(s)';?> </td> <td align="right"> Ksh <?=$paid_cost;?></td></tr>
		<?php }else if($total_sms<=0 && $users>0){ ?>
		<tr height="40"> <td> Package Renewal( <?=$package_name;?>) </td> <td align="right"> Ksh <?=$value1=$paid_cost ;?></td></tr>
		
		<?php }  
		 if($total_sms >0)
		 {?>
		<tr height="40"> <td> <?=$total_sms;?> SMS </td> <td align="right">Ksh   <?=$value2=$sms_cost;?></td></tr>
		 <?php
		 }?>
		<tr  ><td colspan="2">   </td>  </tr>
		<tr height="40"> <td> </td> <td align="right"> <b> Sub Total </b> &nbsp;  Ksh   <?=($sub_total=$paid_cost+$sms_cost);?>  </td></tr> 
		<tr height="40"> <td>  </td> <td align="right"> <b> 16.00% VAT TAX  </b> &nbsp;   Ksh   <?=$vat=16/100*($paid_cost)?>  </td></tr>
		<tr height="20"> <td> </td> <td align="right"> <b> Credit </b> &nbsp;    Ksh <?=$credit?>  </td></tr>
		
		<tr  > <td> </td> <td align="right"> <strong>  Total </strong>  &nbsp;   Ksh   <?=($sub_total+$credit);?>      </td></tr>
		<tr> <td>  </td>   <td align="right">  <strong> Balance </strong>   &nbsp;  KES <?=$balance=$sub_total+$credit-$amount; ?>     </td></tr>
	
	</table>
	<p> </p>
			 
			 </div>
		 
			 <a  href="javascript:;" class="btn grey"  id="btn_receipt" >  <b> <i class="fa fa-print"></i> Print  </b> &nbsp;</a>   
		 </div>
		</div>
		<div class="col-md-2" >
		
		</div>
			 </div>
			 </div>
			 </div> 
		 
		</div> 
	</div>
<!-- END EXAMPLE TABLE PORTLET--> 
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->         
<!-- END CONTAINER -->
<script language="javascript">
 
 var doc = new jsPDF();
var specialElementHandlers = {
'#editor': function (element, renderer) {
return true;
}
};

$(document).ready(function() {
$('#download').click(function () { 
doc.fromHTML($('.pormtlet-body').html(), 15, 15, {
'width': 170,
'elementHandlers': specialElementHandlers
});
doc.save('invoice.pdf');
});
}); 
 
 $("#btn_receipt").click( function () {
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Invoice </title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
</script>
