<!-- BEGIN PAGE CONTENT BODY -->
<script>
 
	function validate_edit()
	{    
		var name=$("#edit_user_name").val();
		var pay_mode=$("#edit_pay_mode").val();
		var amount=$("#edit_amount").val();
		var receipt_no=$("#edit_receipt_no").val();
		var pay_date=$("#edit_pay_date").val(); 
		if(name==""||name==null){ $("#edit_error1").html("<font color='red'> Tenant Name   is empty </font>");$("#edit_user_name").focus(); return false;}
		if(amount==""||amount==null){ $("#edit_error4").html("<font color='red'> Amount field is empty </font>"); $("#edit_amount").focus(); return false;}
		if(pay_date==""||pay_date==null){ $("#edit_error3").html("<font color='red'> Date field is empty</font>"); $("#edit_pay_date").focus(); return false;}
		//if(receipt_no==""||receipt_no==null){ $("#edit_error2").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>"); $("#edit_receipt_no").focus(); return false;}
		if(/^[a-zA-Z0-9- ]*$/.test(amount) == false){ $("#edit_amount").focus(); $("#edit_error4").html("<font color='red'>Amount contains illegal characters </font>");  return false; }else{$("#edit_error4").empty();}
		if(/^[a-zA-Z0-9- ]*$/.test(name) == false){ $("#edit_user_name").focus(); $("#edit_error1").html("<font color='red'>Name  should not have special characters </font>");  return false; } else{$("#edit_error1").empty();} 
		if(/^[a-zA-Z0-9- ]*$/.test(receipt_no) == false){ $("#edit_receipt_no").focus(); $("#edit_error2").html("<font color='red'>Receipt No should not have special characters </font>");  return false; } else{$("#edit_error2").empty();} 
		return confirm("Update details for "+name+"?");
	}
</script>

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Rent  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Bank Details</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN EXAMPLE TABLE PORTLET-->

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title"> 
				 
<div class="row">
<div class="col-md-12"    style="background:#1bb968;padding:1px;">
	<font color="#ffffff"> <h4> &nbsp; &nbsp; Bank Details </h4> </font>
</div>	 
	 <p>  &nbsp; </p> 
	<!--<h4><strong>  Received Payment Details </strong> </h4>-->
	 
 <div class="col-md-4">  
	<div class="form-group">
		<label> Account Name  </label>
		<input   class="form-control" type="hidden" id="edit_id" name="edit_id" >
		
			<input type="text" class="form-control" name="acc_name"   id="acc_name" onchange="validate_char('acc_name')" >
		<label>  </label>
	</div> 
</div>
<div class="col-md-4"> 
	<div class="form-group">
			<label>  Account Number  </label>
			<input type="text" class="form-control" name="acc_no"   id="acc_no"  value=""  onchange="validate_char('acc_no')" >
		 <label>  </label>
	</div> 				
</div>	
	
<div class="col-md-4"> 
		   <div class="form-group">
					<label> Owner</label>
					<select class="form-control" name="owner" id="owner"> 
					<!--landlords lists here-->
					</select>
				 <label id="error3">  </label>
			</div> 		
</div>	 
<div class="col-md-4">    
		 <div class="form-group">
				<label> Bank </label> 
				<input type="text" class="form-control" required name="bank"   id="bank"  onchange="validate_char('bank')" value="">
				 <label id="error2">  </label> 
			</div>  
</div>
<div class="col-md-4"> 
			<div class="form-group">
					<label> Branch </label>
					<input type="text" class="form-control"  name="branch" id="branch"  onchange="validate_char('branch')"  > 
				 <label id="error3">  </label> 
			</div> 
	<p> </p> 
</div>				
<div class="col-md-4"> 
		 <div class="form-group">
				<label> Description (Optional) </label>
					<input type="text" class="form-control" autocomplete="off"   name="description"   id="description" onchange="validate_char('description')">
				 <label id="error5">  </label>
		 </div>
</div>
 	 
<div class="col-md-6">  
		 <div class="form-group">
			<button  type="submit" id="save" class="btn green" <?=$disabled;?> >  &nbsp; Save Record  &nbsp;  </button>
			<input  type="reset"   class="btn red" value="Reset" />
		</div> 		
</div> 
<div class="col-md-6"> 
	<div class="form-group">
			<b> <font id="add_error"></font> <?php if($this->session->flashdata('temp')){ 
			//echo $this->session->flashdata('temp');
			}?> </b>
	</div> 
</div>
 
 	 
 <form  method="post" action="<?=base_url();?>rent/bank_transactions/" onsubmit="return checkedBoxes()"> 	 
 <p> &nbsp;   </p> 
 <div class="col-md-12">  
<table class="table table-striped table-hover table-bordered"  id="sample_editable_1">
<thead>
	<tr>
		<th>  #</th> 
		<th> Date </th>
		<th> Account Name </th>
		<th> Account Number</th>
		<th> Bank</th> 
		<th> Branch</th> 
		<th> Owner</th>  
		<th> Total</th> 
		<th> &nbsp; </th> 
	</tr>
</thead>
<tbody>
   
  <?php $x=1;  $total=0; foreach($bank_details->result() as $rows): ?>
	<tr>
		<td>  <input name="checkbox[]" type="checkbox" id="checkbox[]" onchange="setItemId('<?=$rows->id?>')" value="<?=$rows->id; ?>">  </td>
		<td class="center"> <?=$rows->date_added?>  </td>
		<td> 
		<?php 	$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
				$enct =base64_encode(do_hash($rows->id . $salt,'sha512') . $salt);
				echo $rows->acc_name;   
		   ?>
		</td> 
		<td>  <?=$rows->acc_no?>   </td> 
		<td>  <?=$rows->bank_name?>	</td> 
		<td>  <?=$rows->branch?>   </td> 
		<td>  <?=$rows->owner?>   </td>    
		<td id="edit_<?=$x?>"><?php 
		foreach($tenant_transaction->result() as $r){ if($rows->id==$r->bank_account_id){ $total=$total+$r->amount;}}
		echo $total;
		?></td> 
		<td><a href="#" onclick="editBank('<?=$rows->id?>')"><i class="fa fa-edit"></i> Edit</a> </td> 
	</tr>
	<?php $x++; $total=0; endforeach;?>
	</tbody>
</table> 
<input type="hidden" id="itemId" name="account_id" value=""/>
<button type="submit" class="btn green"> View Transaction</button> 
 </form>
 <p> &nbsp; </p>
</div> 
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div> 
<!---Edit payment-->


<div id="edit_bank" class="modal fade" tabindex="-1" data-width="800" aria-hidden="true">  
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Edit Bank Details </b> </font></h4>
	 </div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
			<form action="<?=base_url();?>rent/receive_pay" method="post" onsubmit="return validate_edit()">
				<div class="col-md-6">
				<p> <label class="control-label"> Name </label> <br/>
							<input   class="form-control" type="hidden" id="editing_id" name="edit_id" >
							<input   class="form-control" type="hidden" id="editing_tenant_id" name="tenant_id" >
							<label id="edit_error1"></label> 
						</p> 
						<p> <label class="control-label"> Payment Method </label>
								<select class="form-control" type="text"  name="pay_mode" id="edit_pay_mode" onchange="disable_method()">
									<option value="Cash"> Cash </option>
									<option value="Cheque"> Cheque </option>
									<option value="Mpesa"> Mpesa </option>
									<option value="Bank Receipt"> Bank Receipt </option>
								</select> 
						<label> </label> 
						</p>
						
						<p> 
						 <label class="control-label"> Date </label>
							<input type="text" required class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value=""   onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" name="date" id="edit_pay_date">
							<label id="edit_error3">					</label> 
						</p>
</div>
			<div class="col-md-6">	
						<p> <label class="control-label"> Amount Paid </label>
							<input   class="form-control" type="text" name="amount"  id="edit_amount" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);">
							<label id="edit_error4">					</label> 
						</p> 					
						 <p> <label class="control-label">Receipt/Cheque No </label>
							<input   class="form-control" type="text" name="receipt_no"  id="edit_receipt_no" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);">
							<label id="edit_error2">					</label>  
						</p> 
							 	 
			</div> 
		</div> 
		<div class="modal-footer" ><center> <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" <?=$disabled;?>> Save Changes </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
	</center>
</div>	
</form> 	 
</div>			
	</div>	

	</div> 
	

<!--->
 <!-- responsive -->
<div id="add_new" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			 
			<h5><b> Specify other Payment </b></h5>
			<hr/>
			<p>
			<label class="control-label">Name/Description </label>
			<input   class="form-control" type="text" placeholder="electricity" id="amenity_name"> </p>
			<p>
			</div>
		</div> 
	</div> 
<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_new_one" <?=$disabled;?>> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>


<div id="success" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b style="font-size:18px;color:brown">    Warning Message </b></h5>
				<hr/>
				<p id="success_msg">
				   
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
 <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">    Success Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="">
				   <?php if($this->session->flashdata('temp')){ echo $this->session->flashdata('temp');}  ?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<!--<button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> -->
		&nbsp;
	</div> 
</div>
 
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 function checkedBoxes()
{	
 
 var counter = $('input:checkbox:checked').length; 
 
if(counter==0){  
$("#success_msg").html('<font size="2" color="red"> Please select one row </font>');
$("#success").modal('toggle');
    //alert("Please select one row.");
	return false;
 }
else if(counter>1)
{
	$("#success_msg").html('<font size="2" color="red"> Please select only one row </font>');
$("#success").modal('toggle');
 //alert("Please select only one row.");
	return false;	
} 
	return true; 
}

 
function validate_char(id)
{
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		$("#success").modal('toggle');  
        return false;
		}
    
}

$(document).ready(function () { 
disable_method();
getLandlordDetails();
  checkPrivilege();
  var saving_success="<?php echo $this->session->flashdata('temp');?>";
 if(!saving_success)
 { }
else{
	$("#data_saving_success").modal('toggle');
	setTimeout(function(){
						 $("#data_saving_success").modal('hide'); 
                      }, 2000); 
      	
	}
$("#save").click(function(){ 
 
	var edit_id=$("#edit_id").val();
	var name=$("#acc_name").val();
	var acc_no=$("#acc_no").val();
	var bank_name=$("#bank").val();
	var bank_branch=$("#branch").val();
	var owner=$("#owner").val();
	var acc_no=$("#acc_no").val();
	var desc=$("#description").val();
	
 if(!name){ $("#add_error").html("<font color='red'> Account Name required </font>"); $("#acc_name").focus();  return false;}
 if(!acc_no){ $("#add_error").html("<font color='red'> Account Number required </font>"); $("#acc_no").focus();  return false;}
 if(!owner){ $("#add_error").html("<font color='red'> Owner Name  required </font>"); $("#owner").focus();  return false;}
  if(!bank_name){ $("#add_error").html("<font color='red'> Bank Name required </font>"); $("#bank").focus();  return false;}
 if(!bank_branch){ $("#add_error").html("<font color='red'> Bank Branch  required </font>"); $("#branch").focus();  return false;}
 $("#add_error").html("<font color='green'> Saving data...</font>");
 $.ajax(
  {
		url:"<?=base_url();?>tenants/bank_details",
		type:"POST",  
		data:{
			'edit_id':edit_id,
			'acc_name':name,
			'acc_no':acc_no,
			'bank_branch':bank_branch,
			'bank_name':bank_name,
			'description':desc,
			'owner':owner
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				 $("#add_error").html("<font color='green'> Data saved successfully </font>");
				  setTimeout(function(){
								$('#add_new').modal('hide');
                               $("#add_error").empty();
                      }, 2000); 
					  document.location.reload(true);
			 }
			 else{
				 $("#add_error").html("<font color='red'> Not added. Try again later </font>");
			 }

		}			 
  })
 
 });

 });
  
  function add_other(value)
  {
	  if(value=="other")
	  {
		$("#add_new").modal('show');
	  }
  }
   
  function setItemId(value)
  { 
		$("#itemId").val(value); 
  }
   
  function editBank(id){ 
     $("#msg").empty();
		$.ajax({
		url:'<?=base_url();?>tenants/getBankDetails/'+id, 
		type: 'POST', 
		success:function (data)
		{  
			var obj = JSON.parse(data);  
			var data = obj.data; 
			if(obj.result=="ok")
			{  
				for(var i=0; i<data.length; i++)
				{
					var l = data[i];  
					var acc_name=l['acc_name'];
					var acc_no=l['acc_no'];
					var bank_branch=l['branch'];   
					var bank_name=l['bank_name'];   
					var desc=l['description'];   
					var owner=l['owner'];   
					$("#edit_id").val(id);
					$("#acc_name").val(acc_name);
					$("#acc_no").val(acc_no);
					$("#bank").val(bank_name);
					$("#branch").val(bank_branch);
					$("#owner").val(owner); 
					$("#description").val(desc);
				}
			  
			} 
		}
	});
	 
} 
    
  function getLandlordDetails(){ 
     $("#msg").empty();
		$.ajax({
		url:'<?=base_url();?>tenants/get_data/landlord', 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data);  
			var data = obj.data; 
			if(obj.result=="ok")
			{
				var name=""; 
				$("#owner").empty();
				$("#owner").append("<option value=''> Please select </option>"); 
				for(var i=0; i<data.length; i++)
				{
						var l = data[i];
						var fname=l['full_name'];
						//var mname=l['middle_name'];
						//var lname=l['last_name']; 
						name=fname;  
				$("#owner").append("<option>"+name+"</option>"); 
				
				}
			  
			} 
		}
	});
	 
} 
 
 function disable_method()
  { 
	var value=document.getElementById("edit_pay_mode").value;  
	 if(value=="Cash")
	  {
		 
	document.getElementById('edit_error2').value ="";
	document.getElementById('edit_receipt_no').value ="--";
	document.getElementById('edit_receipt_no').disabled = true;
      
	  }
	  else
	  {
			document.getElementById('edit_receipt_no').value ="";
			document.getElementById('edit_receipt_no').disabled = false;
	  }  
}

function checkPrivilege()
 { 
	 var i="<?=$x?>";
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/3",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			 
			if(obj.add==0){ 
			   	document.getElementById('save').disabled=true;	  
			}
			if(obj.view==0){ 
			for(var x=1; x<i;x++){   $("#view_"+x).html("<font color='' onclick=\"alert('You have no privilege to view')\"> <a href='#' > View Receipt </a></font>"); }
              		  
			}
			 if(obj.edit==0){  
				for(var x=1; x<i;x++){   $("#edit_"+x).html("<font color='' onclick=\"alert('You have no privilege to edit')\"> <a href='#' ><i class='fa fa-edit'> </i> Edit   </a></font>"); }
             
			}
			if(obj.delete==0){ 
			    
			   //document.getElementById('confirm').disabled=true;   
			}
		}
	 })
 } 
</script> 