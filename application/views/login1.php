<!DOCTYPE html>
<html lang="en">
     <head profile="http://www.w3.org/2005/10/profile">
<link rel="icon" 
      type="image/png" 
      href="<?=base_url();?>images/login/favicon.png">
        <meta charset="utf-8" />
        <title>ARI Homes | Real Estate Management</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        
         <link href="<?=base_url();?>template/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?=base_url();?>template/theme/assets/pages/css/coming-soon.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
		
		   <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
		 <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <!--<!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
         <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="<?=base_url();?>css/jquery-ui.css">
        <link rel="shortcut icon" href="<?=base_url();?>images/login/favicon.png" /> 
        <style> a{color:#ffffff;text-decoration:underline;}a:hover{color:#fff;text-decoration:none;}</style>
       
<script type='text/javascript'>
function validate()
{
    var TCode = document.getElementById('company').value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) {

        alert('Company name field contains illegal characters');
       document.getElementById('company').value="";
        return false;
    }

    return true;     
}
</script>

 <style> ul li h3{ font-family:sans-serif; font-size: 18px; font-style: normal; font-variant: normal; font-weight:200; } </style>
</head>
    <!-- END HEAD -->
<body>
	
<?php if(empty($data)){  $first_name=""; $phone=""; $last_name="";$user_type=""; $email=""; $company="";  }
else {
		foreach($data->result() as $row)
		{
			$first_name=$row->first_name; $phone=$row->mobile_number; $last_name=$row->last_name;$user_type=$row->user_type;$email=$row->email;
		}  
	}
?> 
	
<div class="container" style="color:#fff;">
<div class="row">
 <div class="col-md-6">
 <h3>     <!---logo here--->  </h3>
 </div>
<div class="col-md-6">   
 <h4> &nbsp; &nbsp;<strong> Log in  </strong> </h4>
</div>
</div>
<div class="row">
<div class="col-md-6">
	<img class="todo-userpic pull-left" src="<?=base_url();?>images/login/ARI_Homes_Logo.png" height="80" >
</div>  

<div class="col-md-6">
	<?php  echo form_open_multipart(auth/login/', 'onSubmit="return login()"');?>
	<div class="col-md-6">
		<input class="form-control" type="email" id="username"  name="username"  placeholder="Email" value="<?php echo $this->session->flashdata('temp_email')?>" required="required"  />
	</div>
	<div class="col-md-4">
		<input  class="form-control" type="password" autocomplete="off" placeholder="Password " required="required" id="login_pass" autocomplete="off"   name="password" />
	</div>
	<div class="col-md-2">
		<input class="form-control" type="submit" style="background:#36c6d3;color:#ffffff" value="Log In"/>
		 
	</div>
	<div class="col-md-12">
		<br/><font>
			<a data-toggle="modal" id="btnnewproject" href="#responsive"> Forgot your password? </a>
		</font>
	</div>
<?php echo form_close(); ?>					
</div> 

</div>
<div class="row">
<div class="col-md-6 coming-soon-content">
 
	<h3><strong> Agent or Landlord? Pata Rent Bila Stress  </strong>  </h3>
	<!--<h3> <strong>   Jipange na...</strong>   </h3>-->
	<p>   </p>
	<ul style="list-style-type:;margin:2em;line-height:25px;padding-left:0px;"> 
	 
		<li> <h3> Know your agency fee </h3></li>
		 
		<li> <h3>  Manage your landlords </h3></li>
		 
		<li> <h3>  View Monthly rentals income statements </h3> </li>
	 
		<li> <h3>  Send tenants Invoices, Receipts and SMS </h3> </li>
		 
		 
		<li style="list-style-type:none;"> <img src="<?=base_url();?>images/Guarantee.png" height="120" > &nbsp; <img src="<?=base_url();?>images/premium.png" height="120" ></li>
	
	</ul> 
	
	<div class="col-md-12">
	<ul  style="list-style-type:none;">
		<li> &nbsp;  </li> 
	</ul>
	Copyright &copy;  ARI <?php echo date("Y");?> &nbsp;  Phone: +254 725 992 355 / +254 736 822 568 &nbsp;

	</div>
</div>     
	
<div class="col-md-6 coming-soon-countdown"> 
<?php if($this->session->flashdata('temp')){
	$msg=$this->session->flashdata('temp');
	$error_msg=$this->session->flashdata('error_msg');
	$bgcolor="success"; $text_color="#000"; if($error_msg=="1"){ $text_color="red"; $bgcolor="danger";}
	echo '<div class = "alert alert-'.$bgcolor.' alert-dismissable" style="color:#000;background:;">
	   <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		  <font  style="float:right;font-size:18px;color:#fff;">   X   </font>
	   </button> 
   <font color="'.$text_color.'">'. $msg. '</font>
</div>';
 
}
?>
   <?php  echo form_open_multipart('auth/signUp/', 'onSubmit="return sign_up()"');?>
	<h3 class="font-greenn"> <b> Create an Account </b> <font size="4"> Start Your FREE 30 Days Trial </font></h3>
	<div class="form-group">
	   
	<div class="row">
	<div class="col-md-6"><p>
		<input placeholder="First Name" id="fname" name="first_name" class="form-control" type="text" onchange="checkChars('fname')"> </p>
	</div>
	<div class="col-md-6">
		<p><input placeholder="Last Name" id="lname" name="last_name" class="form-control" type="text" onchange="checkChars('lname')"> </p>
	</div>
  
   
		<div class="col-md-6">
		<div class="form-group">
			<p>
				<select class="form-control"  id="type" name="user_type" placeholder="Register As"> 
					<option value=""> --Register As--  </option> 
					<option value="1">Landlord</option>
					<option value="2">Business (Estate Agent)</option>
				<!--	<option value="3">Tenant </option>-->
				</select>
			</p>
			</div>
		</div>  
	<div class="col-md-6">
		<p><input placeholder="Company Name (Optional)" id="company" name="company" class="form-control" type="text"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
  		
 	 
	<div class="col-md-6">
		<p><input placeholder="Email" id="email"  name="email" class="form-control" type="email"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
	 <div class="col-md-6">
			<p><input type="number" autocomplete="off" name="phone"   id="phone" class="form-control" placeholder="Phone Number" value=""/></p>
	 </div>
 
 
		<div class="col-md-12"> 
		 <p><input placeholder="Password" id="pass" name="password" class="form-control" type="password"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
			
		 </div>
		<div class="col-md-6"> 	 					 
		 
			<font ><label for="captcha"  id="captcha_image">   <?=$this->session->userdata('image');?>  </label> </font>
			<font style="float:right"><a href='javascript:;' onclick="getCaptcha()"> <i class="fa fa-refresh"></i> Refresh image  </a> </font> 
		 
		</div> 
		
			<div class="col-md-6"> 
				<div class="form-group">	
			<input type="text" autocomplete="off" name="userCaptcha"  id="user_Captcha" class="form-control"   placeholder="Enter the text shown above" value=""/>
				<input type="hidden"  name="captcha_name"  id="captcha_name" class="form-control" value='<?=$this->session->userdata('image');?>'/>
				<input type="hidden"  name="captcha_code" id="captcha_val" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
				</div> 
			</div> 
		<div class="col-md-12"> <font id="error">   </font> <hr/> </div>
		<div class="col-md-6"> 
				<div class="form-group">
				<input  id="status" name="status" value="0" class="form-control" type="hidden"/> 
				<label class="check">
				<input type="checkbox" id="tnc" name="tnc"/> Agree to the
				<a href="#terms_n_condition" data-toggle="modal"> Terms & conditions </a>
				</label> 
				
				</div>
			</div> 
	<div class="col-md-6">
		 <div class="form-group">
            <button type="submit" id="register-submit-btn"  style="color:#000;background:lightgreen" class="btn btn-default uppercase"> Submit </button>
         </div>
	</div>
	<div class="col-md-6">
	  &nbsp;
	</div>
		<?php echo form_close(); ?>
	</div>
   </div>
</div>
   
</div>
</div> 
 
		
<div id="reset_pass" class="modal fade" tabindex="-1" aria-hidden="true">
 <?php  echo form_open_multipart('auth/changePassword/', 'onSubmit="return change()"');?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h4 class="modal-title">Change your  Password </h4>
	</div> 
	<div class="modal-body" style="">  
		<input type="hidden"  id="curr_password" name="curr_password" value="" class="form-control" />  
		<input type="hidden"  name="email" value="<?=$this->session->flashdata('email');?>" class="form-control"/>  
		<div class="form-group">
			<label class="control-label"> New Password</label>
			<input type="password" required id="new_pass" name="new_pass"  class="form-control" />
		</div>
		<div class="form-group">
			<label class="control-label"> Verification Code (Sent via SMS)</label>
			<input type="text" required autocomplete="off" name="code" id="code"   class="form-control" onchange="checkChars('code')" />
		</div>

	  <div class="form-group">
			<p> <font  id="captcha_image_reset"><label for="captcha">   <?=$this->session->userdata('image');?>   </font>
		  <font align="left"> &nbsp; Can't read the image?  Click <a href='javascript:;' style="color:#006699" onclick="getCaptcha()">here </a> </font> </label></p>
		<input type="hidden"  name="captcha_name"  id="captcha_name_reset" class="form-control" value='<?=$this->session->userdata('image');?>'/>
		<input type="hidden"  name="captcha_code" id="captcha_val_reset" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
		 <input type="text" required autocomplete="off" name="userCaptcha"  id="user_Captcha" class="form-control" placeholder="Enter the  above numbers" value=""/>
   		 
	</div>		
			
        </div> 
			<div class="modal-footer"><font style="float:left" id="reset_error"> <?=$this->session->flashdata('temp')?>  </font>
				<input type="submit"  class="btn green"value="Submit"/> 
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
			</div> 
		 
<?php echo form_close(); ?>
</div> 
     

<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true" data-width="500" data-height="200">
  <?php  echo form_open_multipart('auth/getPassword/', 'onSubmit="return get_pass()"');?>
	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">  Enter your E-mail address below to reset your password </h5>
	</div>
	<div class="modal-body">
		 <div class="row">
		 <div class="col-md-12"> 	
		<p>
             <input class="form-control placeholder-no-fix"required="required" id="reset_pass_email" type="email" autocomplete="off" placeholder="Email" name="email" /> 
		</p> 
		 </div>
		<div class="col-md-4">
		 <font  id="captcha_image_forgot"><label for="captcha">   <?=$this->session->userdata('image');?> </label> </font> 
		 
		</div>
		<div class="col-md-8"> 	
				<input type="hidden"  name="captcha_name"  id="captcha_name_forgot" class="form-control" value='<?=$this->session->userdata('image');?>'/>
				<input type="hidden"  name="captcha_code" id="captcha_val_forgot" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
				<input type="text" autocomplete="off" required name="userCaptcha"  id="user_Captcha" class="form-control" placeholder="Enter the numbers" value=""/>
			</p> 
		  </div>
	<div class="col-md-12"> 	
		 
		 <font align="left">  Can't read the image?  &nbsp; <a href='javascript:;' onclick="getCaptcha()" style="color:#006699"><i class="fa fa-refresh"></i> Refresh </a> </font>  
	</div>
	</div>	  
    </div>
    <div class="modal-footer"> <font id="reset_status">   </font>
		<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            <input type="submit"  name="submit"class="btn green"value="Submit">     
    </div> 
	
	<?php echo form_close(); ?>
</div> 


<div id="activateAcc" class="modal fade" tabindex="-1" aria-hidden="true" data-width="800" data-height="700">
   <?php 
    echo form_open_multipart('auth/signUp/', 'onSubmit="return activate()"');
   ?>
   <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">  Complete the details on this form to activate Account</h5>
	</div>
	<div class="modal-body">
		
	<div class="form-group">
	   
	<div class="row">
	<div class="col-md-6"><p>
		<input placeholder="First Name" id="a_fname" name="first_name" class="form-control" type="text" onchange="checkChars('fname')"> </p>
	</div>
	<div class="col-md-6">
		<p><input placeholder="Last Name" id="a_lname" name="last_name" class="form-control" type="text" onchange="checkChars('lname')"> </p>
	</div> 
		<div class="col-md-6">
		<div class="form-group">
			<p>
				<select class="form-control"  id="a_type" name="user_type" placeholder="Register As"> 
					<option value=""> --Register As--  </option>
					<option value="1">Landlord</option>
					<option value="2">Business (Estate Agent)</option>
					<!--<option value="3">Tenant </option>-->
				</select>
			</p>
			</div>
		</div>  
	<div class="col-md-6">
		<p><input placeholder="Company Name (Optional)" id="a_company" name="company" class="form-control" type="text"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
  		 
	<div class="col-md-6">
		<p><input placeholder="Email" id="a_email"  name="email" class="form-control" type="email"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
	 <div class="col-md-6">
			<p><input type="number" autocomplete="off" name="phone"   id="a_phone" class="form-control" placeholder="Phone Number" value="<?=$phone?>"/></p>
	 </div> 
	<div class="col-md-12"> 
	 <p><input placeholder="Password" id="a_pass" name="password" class="form-control" type="password"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
		
	</div>
	<div class="col-md-6"> 	  
		<font><label for="captcha"  id="a_captcha_image">   <?=$this->session->userdata('image');?>  </label> </font>
		<font style="float:right"><a href='javascript:;' style="color:blue" onclick="getCaptcha()"> <i class="fa fa-refresh"></i> Refresh  </a> </font> 
	 
	</div> 
		
			<div class="col-md-6"> 
				<div class="form-group">	
			<input type="text" autocomplete="off" name="userCaptcha"  id="a_user_Captcha" class="form-control"   placeholder="Enter the text shown " value=""/>
				<input type="hidden"  name="captcha_name"  id="a_captcha_name" class="form-control" value='<?=$this->session->userdata('image');?>'/>
				<input type="hidden"  name="captcha_code" id="a_captcha_val" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
				</div> 
			</div> 
		<div class="col-md-12"> <font   id="a_error">   </font> <hr/> </div>
		<div class="col-md-12"> 
				<div class="form-group">
				<input  id="a_status" name="status" value="0" class="form-control" type="hidden"/> 
				<label class="check">
				<input type="checkbox" id="a_tnc" name="tnc"/> Agree to the
			<a href="#terms_n_condition" style="color:blue" data-toggle="modal"> Terms & conditions </a>
				</label> 
				
				</div>
			</div>  
	</div>
   </div>  
    </div>
    <div class="modal-footer"> <font id="a_reset_status">   </font>
	
	 <input type="submit" value="Submit" id="a_register-submit-btn" class="btn btn-success uppercase">  
        
		<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                
    </div> 
	<?php 
	 echo form_close();
	?> 
	</form>
</div> 

<div id="terms_n_condition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" class="modal fade" tabindex="-1" aria-hidden="true" style="width: 800px; height: 600px;margin-left: -400px; " data-width="1000" data-height="1200">
   
	<div class="modal-header">
	     <span  data-dismiss="modal" style="float:right;cursor:pointer" title="Close">   <font size="3"color="#000"><i class="fa fa-close"></i></font>   </span>
                
                <h4 class="modal-title"><b> ARI Homes Terms and Conditions</b> </h4>
	</div>
	<div class="modal-body" style="height:450px;overflow-y:scroll;">
		 
			<!--<object data="<?=base_url();?>media/ARI Homes Terms and Conditions 02062017.pdf" type="application/pdf" style="width:100%;padding:0px;height:440px">
				<iframe src="https://docs.google.com/viewer?url=your_url_to_pdf&embedded=true"></iframe>
			</object>-->
 <p> These terms and conditions outline the rules and regulations for the use of ARI Homes Real Estate management Solution (“The Application”) www.ari.co.ke website. </p>
	
	<p>ARI Limited is located at:</p>
	<p> Suite 9, 1<sup> th </sup> Floor | AACC | Sir Francis Ibiam House | Waiyaki Way, Westlands | Nairobi, Kenya  </p>
	<p> By accessing this website we assume you accept these terms and conditions in full. Do not continue to use the application if you do not accept all of the terms and conditions.</p>
	<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: "Client", “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. "The Company", “Ourselves”, “We”, “Our” and "Us", refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves “The Application” refers to www.ari.co.ke All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services/products, in accordance with and subject to, prevailing law of Kenya. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same. </p>
	<p> <strong>Accounts</strong></p>
	<p>When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service. </p>
	<p> You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service. </p>
	<p> You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account. </p>
	
	<p> <strong> Termination </strong></p>
	<p> We may terminate or suspend access to the application and your access to the application immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. </p>
	<p> All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability. </p>
	<p> We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. </p>
	<p> Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service. </p>
	<p> All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability. </p>
	<p> <strong> Governing Law </strong> </p>
	<p> These Terms shall be governed and construed in accordance with the laws of KENYA, without regard to its conflict of law provisions. </p>
	<p>  Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>
	<p> <strong> Changes </strong>   </p>
	<p> We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days’ notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion. </p>
	<p> 
By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.
	</p>
	<p> <strong> Cookies </strong>  </p>
	<p>  We employ the use of cookies. By using The Application's website you consent to the use of cookies in accordance with ARI Limited’s privacy policy. </p>
	<p> Most of the modern day interactive web sites use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate / advertising partners may also use cookies. </p>
	<p> <strong> License </strong>  </p>
	<p>  Unless otherwise stated, ARI Limited and/or its licensors own the intellectual property rights for all material on The Application. All intellectual property rights are reserved. You may view and/or print pages from The Application for your own personal use subject to restrictions set in these terms and conditions.  </p>
	<p> You must not:  </p>
	<p> <ul>
<li> Republish material from <a href="<?=base_url()?>" style="color:blue">www.ARI.co.ke</a> </li>
<li> Sell, rent or sub-license material from <a style="color:blue"href="<?=base_url()?>">www.ARI.co.ke</a>  </li>
<li>  Reproduce, duplicate or copy material from <a style="color:blue" href="<?=base_url()?>"> www.ARI.co.ke</a> </li>
<li>  Redistribute content from The Application (unless content is specifically made for redistribution) from <a href="<?=base_url()?>" style="color:blue">www.ARI.co.ke</a>  </li>
</ul>
	</p>
	<p> <strong> User Information </strong>  </p>
	<p> 
	<ol>
	<li> This Agreement shall begin on the date hereof 4th May 2016.</li> 
	<li> Certain parts of this website offer the opportunity for users to post and exchange opinions, information, material, data and comments in areas of the website. ARI Limited does not screen, edit, publish or review information prior to their appearance on the website and information do not reflect the views or opinions of ARI Limited, its agents or affiliates. Information reflect the view and opinion of the person who posts such view or opinion. To the extent permitted by applicable laws ARI Limited shall not be responsible or liable for the information or for any loss cost, liability, damages or expenses caused and or suffered as a result of any use of and/or posting of and/or appearance and loss of the information on this website.</li> 
	<li> ARI Limited reserves the right to monitor all information and to remove any information which it considers in its absolute discretion to be inappropriate, offensive or otherwise in breach of these Terms and Conditions.</li> 
	<li> You warrant and represent that:</li> 
	<li> You are entitled to post the information on our website and have all necessary licenses and consents to do so;</li> 
	<li> The Information do not infringe any intellectual property right, including without limitation copyright, patent or trademark, or other proprietary right of any third party;</li> 
	<li> The Information do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material or material which is an invasion of privacy</li> 
	<li>The information will not be used to solicit or promote business or custom or present commercial activities or unlawful activity. </li> 
	<li> You hereby grant to <strong> ARI Limited </strong> a non-exclusive royalty-free license to use, reproduce, edit and authorize others to use, reproduce and edit any of your information in any and all forms, formats or media.</li> 
	  
	</ol>
	</p>
	<p> <strong> Hyperlinking to our Content </strong>  </p>
	 
	<ol>
		<li> 
		We may consider and approve in our sole discretion other link requests from the following types of organizations:  </p>
	    <ol style="list-style-type:lower-alpha;">
		<li>Commonly-known consumer and/or business information sources, social media sites </li>
		<li> Community sites; </li>
		<li>Associations or other groups representing charities, including charity giving sites, </li>
		<li> Online directory distributors</li>
		<li> Educational institutions and trade associations</li>
		<li>Government agencies </li>
		<li> Search engines</li>
		<li>News organizations </li>
		 
		</ol>
		</li>
		<li> We will approve link requests from these organizations if we determine that: (a) the link would not reflect unfavorably on us or our accredited businesses (for example, trade associations or other organizations representing inherently suspect types of business, such as work-at-home opportunities, shall not be allowed to link); (b)the organization does not have an unsatisfactory record with us; (c) the benefit to us from the visibility associated with the hyperlink outweighs the absence of ARI Limited; and (d) where the link is in the context of general resource information or is otherwise consistent with editorial content in a newsletter or similar product furthering the mission of the organization. </li>
		<li> These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and it products or services; and (c) fits within the context of the linking party's site. </li>
		<li> If you are among the organizations listed in paragraph 2 above and are interested in linking to our website, you must notify us by sending an e-mail to support@ari.co.ke. Please include your name, your organization name, contact information (such as a phone number and/or e-mail address) as well as the URL of your site, a list of any URLs from which you intend to link to our Web site, and a list of the URL(s) on our site to which you would like to link. Allow 2-3 weeks for a response. </li>
		<li> Approved organizations may hyperlink to our Web site as follows:
		<ul>
		<li>By use of the uniform resource locator (Web address) being linked to</li>
		<li>No use of (our company name)’s logo or other artwork will be allowed for linking absent a trademark license agreement.</li>
		 
		</ul>
		</li>
		<li> Iframes<br/> <p>Without prior approval and express written permission, you may not create frames around our Web pages or use other techniques that alter in any way the visual presentation or appearance of our Web site. </li>
		<li> Content Liability<br/><p> We shall have no responsibility or liability for any content appearing on your Web site. You agree to indemnify and defend us against all claims arising out of or based upon your Website. No link(s) may appear on any page on your Web site or within any context containing content or materials that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p> </li>
		<li>   Reservation of Rights <br/> <p> We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Web site. You agree to immediately remove all links to our Web site upon such request. We also reserve the right to amend these terms and conditions and it’s linking policy at any time. By continuing to link to our Web site, you agree to be bound to and abide by these linking terms and conditions. </p></li>
		<li>   Removal of links from our website<br/><p> If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.</p>
		<p> Whilst we endeavor to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.</p>
		</li>
		<li> Links To Other Web Sites<br/><p> Our Service may contain links to third-party web sites or services that are not owned or controlled by ARI Limited.</p>
         <p> ARI Limited has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that ARI Limited shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or
		 services available on or through any such web sites or services.</p>
		 <p>We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.
		 </p>
		 </li> 
	</ol>
	</p>
	<p> <strong> Disclaimer </strong>  </p>
	<p> To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill). Nothing in this disclaimer will:
	<ul>
	<li> limit or exclude our or your liability for death or personal injury resulting from negligence; </li>
	<li> limit or exclude our or your liability for fraud or fraudulent misrepresentation; </li>
	<li>  limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
	<li> exclude any of our or your liabilities that may not be excluded under applicable law.  </li>
 
	</ul>
	</p>
	<p>The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.</p>
	<p>To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature. </p>
	<p> <strong> Contacting us </strong>  </p>
	<p>  If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at: </p>
	<p> <h4> ARI Limited</h4> </p>
	<p> <h4> www.ari.co.ke</h4> </p>
	<p> Suite 9, 1<sup>th</sup> Floor | AACC | Sir Francis Ibiam House | Waiyaki Way, Westlands | Nairobi, Kenya  </p>
	<p> +254 725 992355, +254 735 412002  </p>
	<p> support@ari.co.ke </p>
 
	 
    </div>  
	 <div class="modal-footer"> <font id="a_reset_status">   </font>
	 <center>
		<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
       </center>         
    </div> 
</div> 



<!-- BEGIN CORE PLUGINS --> 
<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
 <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/theme/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/theme/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url();?>template/theme/assets/global/plugins/countdown/jquery.countdown.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/theme/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?=base_url();?>template/theme/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=base_url();?>template/theme/assets/pages/scripts/coming-soon.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/theme/assets/pages/scripts/coming-soon.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>

<script language="javascript">
 
$(document).ready(function()
{    
	window.history.forward(-1);
	var reset_pass="<?=$this->session->flashdata('reset_pass')?>";
    var password_resetting="<?=$this->session->flashdata('password_resetting')?>";
	 if(!password_resetting){ 
		return false; 		
	 }
	 else
	 {  
		$("#curr_password").val(reset_pass);
		$("#reset_pass").modal('toggle'); 
	 }		
});

</script>

<script language="javascript"> 



function getCaptcha() 
	{  
		 
		 $.ajax({
		 url:"<?=base_url();?>auth/getCaptcha/",
		 type:"POST",
		 async:false, 
		 success:function(data)
		 {
			 var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
					$("#captcha_image").html(obj.image);
					$("#captcha_image_forgot").html(obj.image);
					$("#captcha_image_reset").html(obj.image);
					$("#captcha_name").val(obj.image);				 
					$("#captcha_name_forgot").val(obj.image);				 
					$("#captcha_name_reset").val(obj.image);				 
					$("#captcha_val").val(obj.val);  
					$("#captcha_val_forgot").val(obj.val);  
					$("#captcha_val_reset").val(obj.val);  

			 }
			 else{
					$("#captcha_image").html(obj.image);
					$("#captcha_image_forgot").html(obj.image);
					$("#captcha_image_reset").html(obj.image);
					$("#captcha_name").val(obj.image);				 
					$("#captcha_name_forgot").val(obj.image);				 
					$("#captcha_name_reset").val(obj.image);				 
					$("#captcha_val").val(obj.val);  
					$("#captcha_val_forgot").val(obj.val);  
					$("#captcha_val_reset").val(obj.val);  
				 }
		 }
		 })	  
} 

  function sign_up()
	{    
		var pass =	$("#pass").val();
		var fname =	$("#fname").val();
		var lname =	$("#lname").val();   	
		var email =	$("#email").val();   	
		var type =	$("#type").val();  
		var partner_code=$("#partner").val(); 
		var company=$("#company").val(); 
		var status =$("#status").val(); 
		var phone =$("#phone").val(); 
		var captcha_val =$("#captcha_val").val(); 
		var user_Captcha =$("#user_Captcha").val();
		var captcha_name=$("#captcha_name").val(); 
		if(!fname){  $("#error").html("<font color='red'> Provide your First Name</font>"); $("#fname").focus();   return false;   } else{ $("#error").empty();}
		if(!lname){    $("#error").html("<font color='red'> Provide your Last Name</font>"); $("#lname").focus(); return false;   } else{ $("#error").empty();} 
		if(!type){    $("#error").html("<font color='red'> Select register as</font>");  $("#type").focus(); return false; }  else{ $("#error").empty();} 
		if(type==2 && company==""){    $("#error").html("<font color='red'>  Company Name required if need to sign up as Agent</font>");  $("#company").focus(); return false; } else{ $("#error").empty();}  
		if(!email){   $("#error").html("<font color='red'> Provide your Email Address </font> ");  $("#email").focus(); return false;   }else{ $("#error").empty();}
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#error").html("<font color='red'> Invalid E-mail address"); $("#email").focus(); return false;}	
		if(!phone){   $("#error").html("<font color='red'> Provide your phone Number </font> ");  $("#phone").focus(); return false;   }else{ $("#error").empty();}
		if(phone.length <10){   $("#error").html("<font color='red'> Re-check your phone number </font> ");  $("#phone").focus(); return false;   }else{ $("#error").empty();}
		if(!pass){   $("#error").html("<font color='red'> Provide your password </font> ");  $("#pass").focus(); return false;   }else{ $("#error").empty();}
		if(pass.length<6){$("#error").html("<font color='red'> Password should be at least 6 characters </font>"); return false;}else{ $("#error").empty();}
		
		var x = document.getElementById("tnc").checked;
		if(!user_Captcha){   $("#error").html("<font color='red'>Enter the captcha text  ");  $("#user_Captcha").focus(); return false;   }else{ $("#error").empty();}
		if(captcha_val ==user_Captcha){ } else{ $("#error").html("<font color='red'>Wrong  captcha text. Please re-enter  ");  $("#user_Captcha").focus(); return false;}
		if(x==false){$("#error").html("<font color='red'> Accept terms and conditions to proceed"); $("#tnc").focus(); return false;}else{ $("#error").empty();}
		return true;
	} 
	
	function activate()
	{    
		var pass =	$("#a_pass").val();
		var fname =	$("#a_fname").val();
		var lname =	$("#a_lname").val();   	
		var email =	$("#a_email").val();   	
		var type =	$("#a_type").val();  
		var partner_code=$("#a_partner").val(); 
		var company=$("#a_company").val(); 
		var status =$("#a_status").val(); 
		var phone =$("#a_phone").val(); 
		var captcha_val =$("#a_captcha_val").val(); 
		var user_Captcha =$("#a_user_Captcha").val();
		var captcha_name=$("#a_captcha_name").val(); 
		if(!fname){  $("#a_error").html("<font color='red'> Provide your First Name</font>"); $("#a_fname").focus();   return false;   } else{ $("#a_error").empty();}
		if(!lname){    $("#a_error").html("<font color='red'> Provide your Last Name</font>"); $("#a_lname").focus(); return false;   } else{ $("#a_error").empty();} 
		if(!type){    $("#a_error").html("<font color='red'> Select register as</font>");  $("#a_type").focus(); return false; }  else{ $("#a_error").empty();} 
		if(type==2 && company==""){    $("#a_error").html("<font color='red'>  Company Name required if need to sign up as Agent</font>");  $("#a_company").focus(); return false; } else{ $("#a_error").empty();}  
		if(!email){   $("#a_error").html("<font color='red'> Provide your Email Address </font> ");  $("#a_email").focus(); return false;   }else{ $("#a_error").empty();}
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#a_error").html("<font color='red'> Invalid E-mail address"); $("#a_email").focus(); return false;}	
		if(!phone){   $("#a_error").html("<font color='red'> Provide your phone Number </font> ");  $("#a_phone").focus(); return false;   }else{ $("#a_error").empty();}
		if(phone.length <10){   $("#a_error").html("<font color='red'> Re-check your phone number </font> ");  $("#a_phone").focus(); return false;   }else{ $("#a_error").empty();}
		if(!pass){   $("#a_error").html("<font color='red'> Provide your password </font> ");  $("#a_pass").focus(); return false;   }else{ $("#a_error").empty();}
		if(pass.length<6){$("#a_error").html("<font color='red'> Password should be at least 6 characters </font>"); return false;}else{ $("#a_error").empty();}
		
		var x = document.getElementById("a_tnc").checked;
		if(!user_Captcha){   $("#a_error").html("<font color='red'>Enter the captcha text  ");  $("#a_user_Captcha").focus(); return false;   }else{ $("#a_error").empty();}
		if(captcha_val ==user_Captcha){ } else{ $("#a_error").html("<font color='red'>Wrong  captcha text. Please re-enter  ");  $("#a_user_Captcha").focus(); return false;}
		if(x==false){$("#a_error").html("<font color='red'> Accept terms and conditions to proceed"); $("#a_tnc").focus(); return false;}else{ $("#a_error").empty();}
		return true;
	} 
	
	
  function get_pass()
	{   
	 
		var email=	$("#reset_pass_email").val(); 
		if(email){ } else{$("#reset_pass_email").focus(); return false;}
		var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#reset_status").html("<font color='red'> Email provided is invalid"); return false;}
		return true; 
	}
	
 
 function change()
	{  
		var email="<?php echo $this->session->userdata('email');?>";
		var curr_pass=$("#curr_password").val();
		var new_pass=$("#new_pass").val();
		var code=$("#code").val();  	
	   if(!new_pass){  $("#reset_error").html("<font color='red'> Enter new password </font>"); $("#new_pass").focus(); return false;}else{ $("#reset_error").empty();}
	   if(!code){	$("#reset_error").html("<font color='red'> Enter verification code </font>"); $("#code").focus(); return false;}else{ $("#reset_error").empty();}
		return true;
	}
	
function login() 
	{   
	  
		var password=$("#login_pass").val();
		var username=$("#username").val(); 
		//var userCaptcha=$("#userCaptcha").val();

		var email=username; 
		$("#login_msg").html("");
		if(username){ } else{$("#login_msg").html("<font color='red'> Please Enter username  </font>");$("#username").focus();  return false;}
		if(!password){ $("#login_msg").html("<font color='red'> Please provide your password </font>");$("#login_pass").focus(); return false; }
		//if(!userCaptcha || userCaptcha==""){ $("#userCaptcha").html("<font color='red'> Enter Image text  </font>");$("#userCaptcha").focus(); return false; }

		$("#login_msg").empty();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#login_msg").html("<font color='red'> Invalid E-mail address"); return false;}
		$("#login_msg").html("<div class = 'alert alert-success alert-dismissable' style='background:lightgreen;opacity:1.0;'>"+
		"<button type = 'button' class = 'close' data-dismiss = 'alert' aria-hidden = 'true'>&times;"+
		"</button> <font color='#006699'> Authenticating Please wait... </font></div>");

}

$(function()
{    
//var activate="<?=$this->session->flashdata('activate');?>"; 
	var fname="<?=$first_name?>"; 
	var lname="<?=$last_name?>"; 
	var email="<?=$email?>"; 
	var user_type="<?=$user_type?>";  
	var company="<?=$company?>";  
	var phone="<?=$phone?>";  
	if(!email||email==null)
	{
	  return false;
	}
	else
	{ 
		 $("#a_fname").val(fname);
		 $("#a_lname").val(lname);   	
		 $("#a_email").val(email);   	
		 $("#a_phone").val(phone);   	
		 $("#a_company").val(company); 
		 $("#a_type").val(user_type); 
		 $("#a_status").val(1); 
		 document.getElementById('a_fname').disabled = true;
		 document.getElementById('a_lname').disabled = true;
		 document.getElementById('a_phone').disabled = true;
		 document.getElementById('a_company').disabled = true;
		 document.getElementById('a_type').disabled = true;
		 document.getElementById('a_email').disabled = true;
		 $('#activateAcc').modal('show');
	}
 

});

/*
 
$(function(){
 
		var x=document.URL;
        if (x=="https://ari.co.ke/auth/#signup") { 
 		    $("#signup").modal({backdrop: "static"}); 
        } 
		
		$("#create_acc").click(function()
		{
			$("#signup").modal({backdrop: "static"}); 
		});
		
		$("#sign_up_page").click(function()
		{
			$("#signup").modal({backdrop: "static"}); 
		});
		
       $("#close").click(function()
		{
			window.location = "https://ari.co.ke/auth/";
		});
	}); 
*/	
 
</script>
