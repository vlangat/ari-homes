<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller {
 
  var $user_ip=null; 
public function __construct(){
		 parent::__construct();
		$this->load->library('email');
		$this->load->helper('email'); 
		$this->load->helper('security');
		$this->load->library('session');
		$this->load->model("Admin_model");
		$this->load->library('AfricasTalkingGateway');  
		$this->load->model('Admin_model');
		$this->load->model('Authentication_model');
		$this->load->model('Property_model');
		$this->load->model('Tenant_model');
		$this->load->model('Rent_model');
		$this->load->library('sendSms'); 
		$this->load->library('Captcha');
		$this->load->library('Encrypte');
	    date_default_timezone_set('Africa/Nairobi');
		header('Access-Control-Allow-Origin: *');
}
 
public function index($transactions="")
{ 
	$data['customer_info']=$this->Admin_model->getData('customers',array('audit_number'=>1)); 
	$data['demos']=$this->Admin_model->getData('customer_calls',array('comment'=>'demo')); 
	$data['users']=$this->Admin_model->getData('account',$condition=array('audit_number'=>1));
	$this->load->view('admin/header');
	$this->load->view('admin/customers/index',$data);
	$this->load->view('admin/footer');
	
}
public function calls()
{ 
	$data['customer_info']=$this->Admin_model->getData('customer_calls'); 
	$data['users']=$this->Admin_model->getData('account',$condition=array('audit_number'=>1,'level'=>2), $order_by="id",$asc_desc="desc");
	$this->load->view('admin/header');
	$this->load->view('admin/customers/customers_to_call',$data);
	$this->load->view('admin/footer');
	
}

public function demo()
{ 
	$data['customer_info']=$this->Admin_model->getData('customer_calls',array('comment'=>'demo')); 
	$this->load->view('admin/header');
	$this->load->view('admin/customers/customers_to_demo',$data);
	$this->load->view('admin/footer');
	
}
	
	public function no_response()
	{
		$id=$this->input->post('id');
		$data=array('comment'=>'no response'); 
		if($id !="")
		{
			$this->Admin_model->update_data($table="calls", array('customer_id'=>$id),$data);
			echo  json_encode(array('result'=>"ok"));
		}
		else
		{
			echo json_encode(array('result'=>"false"));
		} 
	}
		
		
	public function call_again()
	{
		$id=$this->input->post('id');
		$data=array( 
		'date_to_call'=>$this->input->post('date'), 
		'called'=>'no', 
		'comment'=>'call again', 
		'additional_comment'=>$this->input->post('remarks') 
		);
		if($id !="")
		{
			$this->Admin_model->update_data($table="calls", array('customer_id'=>$id),$data);
			echo  json_encode(array('result'=>"ok"));
		}
			else
			{
				echo json_encode(array('result'=>"false"));
			} 
		}
	public function not_interested()
	{
		$id=$this->input->post('id');
		$data=array(   
		'additional_comment'=>$this->input->post('remarks') 
		);
		if($id !="")
		{
			$this->Admin_model->update_data($table="calls", array('customer_id'=>$id),$data);
			echo  json_encode(array('result'=>"ok"));
		}
			else
			{
				echo json_encode(array('result'=>"false"));
			} 
		}
	
	public function remarksAfterDemo()
	{
		$id=$this->input->post('id');
		$data=array('remarks'=>$this->input->post('remarks'));
		 if($id !="")
		{
			$this->Admin_model->update_data($table="customers", array('id'=>$id),$data);
			echo  json_encode(array('result'=>"ok"));
		}
		else
		{
			echo json_encode(array('result'=>"false"));
		} 
	}
	
	public function add_demo_request()
	{
		$id=$this->input->post('id');
		$assign_to=$this->input->post('assign_to');
		$assign_to_name="";
		$customer_name=$this->input->post('customer_name');
		$q=$this->Admin_model->getData('account',$condition=array('email'=>$assign_to), $order_by="id",$asc_desc="desc");
		foreach($q->result() as $r){ $assign_to_name=$r->first_name.' '.$r->middle_name.' '.$r->last_name;}
		$data=array( 
		'demo_date'=>$this->input->post('demo_date'), 
		'demo_time'=>$this->input->post('time'), 
		'called'=>'yes', 
		'comment'=>'demo', 
		'additional_comment'=>$this->input->post('remarks'), 
		'demo_by'=>$assign_to_name
		);
		if($id !="")
		{
			$this->Admin_model->update_data($table="calls", array('customer_id'=>$id),$data);
			$date=$this->input->post('demo_date').' '.$this->input->post('time');
			$this->notifyAssigned($assign_to,$date,$customer_name,$assign_to_name);
			echo  json_encode(array('result'=>"ok"));
		}
			else
			{
				echo json_encode(array('result'=>"false"));
			} 
	}
	
	public function deleteCustomer($deleteId="")
	{
		$id=$this->input->post('id');
		if($deleteId =="")
		{ 
			$this->Admin_model->delete_data($table="calls", array('customer_id'=>$id));
			$q=$this->Admin_model->get_data($table="calls", array('customer_id'=>$id));
			if($q->num_rows()==0)
			{
			 echo  json_encode(array('result'=>"ok",'data'=>1));
			}
			else
			{
			echo json_encode(array('result'=>"false",'data'=>0));
			}

		}
		else
		{
			$this->Admin_model->delete_data($table="customers", array('id'=>$deleteId));
			$this->Admin_model->delete_data($table="calls", array('customer_id'=>$deleteId));
			$q=$this->Admin_model->get_data($table="customers", array('customer_id'=>$deleteId));
			if($q->num_rows() ==0)
			{
			 echo  json_encode(array('result'=>"ok",'data'=>1));
			}
			else
			{
			echo json_encode(array('result'=>"false",'data'=>0));
			} 
		} 
	}
	
	public function add_customer()
	{
		$edit_id=$this->input->post('edit_id');
		$data=array(
		'company'=>$this->input->post('company'),
		'phone'=>$this->input->post('phone'), 
		'email'=>$this->input->post('email'), 
		'contact_person'=>$this->input->post('contact_person'),
		'remarks'=>$this->input->post('remarks'), 
		'date_added'=>date('Y-m-d H:i:s'));
		$mail_to_send=$this->input->post('email_type');
		if($edit_id=="")
		{
			$query=$this->Admin_model->getData($table="company_info", array('company_email'=>$this->input->post('email')));
			if($query->num_rows()<1)
			{ 
				$p=$this->Admin_model->get_data($table="customers", array('email'=> $this->input->post('email'),'company'=> $this->input->post('company')));
				if($p->num_rows()<1)
				{
					$this->Admin_model->insert_data($table="customers", $data);
				}
				$q=$this->Admin_model->getData($table="customers", array('email'=> $this->input->post('email'),'company'=> $this->input->post('company')));
				if($q->num_rows()>0)
				 {
					 foreach($q->result() as $r) { $id=$r->id;}
					 $date_to_call="";
					  
				$date=date_create(date('Y-m-d H:i'));
		        date_add($date,date_interval_create_from_date_string(1 ."days"));
				$date_to_call=date_format($date,"Y-m-d H:i");  
				$this->Admin_model->insert_data($table="calls", array('customer_id'=>$id,'called'=>'no','date_to_call'=>$date_to_call,'comment'=>'','demo_date'=>'','demo_by'=>''));
				if($mail_to_send=="email_1"){
					
					
				$this->email_one($this->input->post('email'), $receipient_name=$this->input->post('contact_person'));
					echo  json_encode(array('result'=>"ok",'data'=>1));
				 }
				 
				 }
				else
				{
					echo json_encode(array('result'=>"false",'data'=>0));
				}
			}
			else
			{
				echo json_encode(array('result'=>"false",'data'=>0));
			} 
		}
		else
		{
			 $this->Admin_model->update_data($table="customers", array('id'=>$edit_id), $data);
			 echo  json_encode(array('result'=>"ok",'data'=>1));
			  
		}	
		
	}
	
 
  public function email_one($to, $receipient_name)
  {
	  $subject="Online Real Estate Management Solution";
	  $body='<font size="3">Dear '.$receipient_name.',</font>
		<p> Are you having difficulties in: </p> 
		<ul>
			<li>Knowing your agency fee</li>
			<li>Managing your landlords </li>
			<li>Viewing Monthly rentals income statements </li>
			<li>Sending tenants Invoices, Receipts and SMS </li>
			<li>Recording and Receiving Rental payments </li>
		</ul>
		<p> Welcome to ARI Homes Real Estate Management Solution, an easy, efficient and paperless online system  that automates operations across all your branches. </p> 
		<p> <a href="www.arihomes.co.ke">CLICK HERE</a> or visit the online portal at www.arihomes.co.ke to start your FREE 30 Days Trial. <br/>
		Attached is  a brief outlook of how the solution works.  I would be happy to arrange a DEMO for you. <br/>
		Thanks,</p> 
		<p><font size="3" align="left"> '.$this->session->userdata('first_name').'</p> <br/>'.$this->getFooter();
	    
	 	$this->sendMail($to,$body,$subject,$from="ARI HOMES","ARI_Homes_26052017.ppt");
	 
  }
  
  public function changePhoto($table="")
	{
		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('photo'))
		{
			$error = array('error' => $this->upload->display_errors());
			$status=$this->session->set_flashdata('temp',' <font color="red">There was an error in saving Image </font> ');	 
		}
		else
		{
		$condition=array('id'=>$this->session->userdata('id'));
		$upload_data = $this->upload->data(); 
		$file_name =   $upload_data['file_name'];
		if($table==""){$table="user_info";}
		$this->Admin_model->uploadPhoto($file_name,$condition,$table);
		$data = array('upload_data' => $this->upload->data());
                $this->session->set_userdata('photo', $file_name);
		$status=$this->session->set_flashdata('temp','Photo uploaded successfully');
		}
         
		redirect(base_url() ."admin/account#tab_1_2");
             
	}
////end of upload function
 
  public function changeCompanyLogo()
	{
		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('photo'))
		{
			$error = array('error' => $this->upload->display_errors());
			return;//$status=$this->session->set_flashdata('img_info',' <font color="red"> There was an error in saving Image </font> ');	 
		}
		else
		{
		$condition=array('company_id'=>$this->session->userdata('company_id'));
		$upload_data = $this->upload->data(); 
		$file_name =   $upload_data['file_name'];
		$file_name.$table="company_details";
		$this->Authentication_model->uploadPhoto($file_name,$condition,$table);

		$data = array('upload_data' => $this->upload->data());
               //$this->session->set_userdata('photo', $file_name);
		$status=$this->session->set_flashdata('temp','Changes made successfully');
		}
		 redirect(base_url() ."Auth/agent#tab_1_4");
	}
	 
public function sms()
{
	$username   = "daguindd";
	$apikey     = "bbf2a40b99f48da7d75d94bb044addb5bdcd164a531d0b085d196c2cb590bf29"; 
		
// Specify the numbers that you want to send to in a comma-separated list
// Please ensure you include the country code (+254 for Kenya in this case)
//$recipients = "+254725992355,";
 $recipients="";
 $count=0; 
 
foreach($this->input->post('to') as $value) {  $recipients=$recipients."+254".substr($value,1).","; $count++;}
 
// And of course we want our recipients to know what we really do
$message    = $this->input->post('body'); 
// Create a new instance of our awesome gateway class
$gateway    = new AfricasTalkingGateway($username, $apikey);

// Any gateway error will be captured by our custom Exception class below, 
// so wrap the call in a try-catch block

try 
{ 
  // Thats it, hit send and we'll take care of the rest. 
 $results = $gateway->sendMessage($recipients, $message);
		 
 foreach($results as $result) {
 
	 $cost=$result->cost; 
   } 
  /******get sms balance */
  $data = $gateway->getUserData();
  $balance=$data->balance;
  /**** end of balance ***/ 
  $pages=$this->input->post('pages');
  $sent_sms=$count*$pages; 
   
 $this->saveMessage($message,$recipients,$type=$this->input->post('message_type'),$sent_sms);
  
}
catch ( AfricasTalkingGatewayException $e )
{
  //echo "Encountered an error while sending: ".$e->getMessage();
  echo json_encode(array('result'=>"false",'data'=>0));
}

}
 
	
public function hashId($enct="", $table="",  $condition="",$searchItem="")
{

			if($condition==""  || $table=="" ||  $enct=="" ||  $searchItem=="")
			{ 
				 $id=""; 
			}
			else{
			$q=$this->Authentication_model->get_data($table, $condition); 
			if($q->num_rows()>0)
			 {
			   foreach($q->result() as $r)
			   {
				$id=$r->$searchItem; 
				$s = sha1('2ab'); $s = substr($s, 0, 10); 
				$e =base64_encode(do_hash($id . $s,'sha512') . $s);
				if($e==$enct)
				{  
					$id=$r->$searchItem;   break;
				} 
				else{ $id=""; }
			   } 
			  
			 }
			} 
			
			return  $id; 
}



 public function getFooter()
 {
	 $footer='<div><br/>
		<p   align="left"> Thanks, <br/>  
		 Team ARI  Homes 
		</p>
		 
			<font size="2"> <center> &copy; '.date("Y").' ARI Homes   </center> </font>
			<center> <font> <a href="mailto:support@ari.co.ke">support@ari.co.ke </a> | +254 789 502 424 | +254 789 732 828 </font> </center> 
		   <center><font size="2"> 1 <sup>st </sup> Floor | All Africa Council of Churches | Sir Francis Ibiam House | Waiyaki Way, Westlands, Nairobi  <br/>
			 <a href="'.base_url().'media/ARI Homes Terms and Conditions 02062017.pdf">Terms </a>  </font>
			 </center>
			 
		</div>';
			return $footer;
 }
 
 public function notifyAssigned($to="",$date="",$customer="",$receipient_name="")
 {
	$body='<font size="3">Dear '.$receipient_name.',</font>
		 
		<p> 
		You have been assigned by '.$this->session->userdata('first_name').' to demonstrate ARI Homes Real Estate management system to '.$customer.' on '.$date.' for more information contact Team ARI Homes support@ari.co.ke.  </p> 
		 </p> 
		 <br/>'.$this->getFooter(); 
	 	$this->sendMail($to,$body,$subject="Demo Assignment",$from="ARI HOMES");	  
 }
 
 
 public function getCustomers()
 {  
 $search=$this->input->get('term');
	if($search !="")
	{
	  $query = $this->Admin_model->get_data($table="customers", array('audit_number'=>1),$order_by="",$search);
		foreach($query->result() as $r)
		{
			$arr_result[]=$r->company;
		}
	   echo json_encode($arr_result); 
		 //echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		 //echo json_encode($q->result_array());*/
	}
	else
	{
		echo json_encode(array('result'=>"false",'data'=>0));
	}  
 }
 
 public function sendMail($to,$body,$subject,$from,$fileName="")
    {
        
		$this->load->library('email');
		$this->email->from('support@ari.co.ke',$from);
		$this->email->to($to);
	 
		if($fileName !="")
		{ 
				$this->email->attach($_SERVER["DOCUMENT_ROOT"].'/media/'.$fileName);
		} 
		$this->email->subject($subject);
		$this->email->message($body);	
		$this->email->set_mailtype("html");
		$this->email->send();
		//echo $this->email->print_debugger();
	 //$this->email->print_debugger(); 
	}

}