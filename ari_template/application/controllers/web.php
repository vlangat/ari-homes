<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web extends CI_Controller {
	public function __construct()
	{
		parent::__construct();    
		$this->load->library('Encrypte');
		$this->load->library('Captcha');
		$this->load->helper('security');   
        date_default_timezone_set('Africa/Nairobi');
        $this->session->set_flashdata('temp','');
	}
 
	public function index()
	{
	   $cap=new Captcha();
	   $data['image']=$cap->index(); 
	   $this->load->view('web/header');
	   $this->load->view('web/index',$data);
	   $this->load->view('web/footer');
	}
	
	public function pricing()
	{
	   $this->load->view('web/header');
	   $this->load->view('web/pricing');
	   $this->load->view('web/footer');
	}
	public function about()
	{
	   $this->load->view('web/header');
	   $this->load->view('web/about');
	   $this->load->view('web/footer');
	}
	public function contact()
	{
	   $this->load->view('web/header');
	   $this->load->view('web/contact');
	   $this->load->view('web/footer');
	}
	public function how_it_works()
	{
	   $this->load->view('web/header');
	   $this->load->view('web/how_it_works');
	   $this->load->view('web/footer');
	}
	
	
}