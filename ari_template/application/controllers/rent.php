<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rent extends CI_Controller {
	public function __construct()
	{
		parent::__construct();   
		$this->load->model("Rent_model");
		$this->load->model("Tenant_model"); 		
		$this->load->model("Payment_model"); 		
		$this->load->model("Property_model");
		$this->load->library('Expired');
		$this->load->library('Encrypte');
		$this->load->helper('security');
		$this->load->library('rent_class');  
		$this->load->library('pdf');
        date_default_timezone_set('Africa/Nairobi'); 
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry(); 		
	}
 
	public function paid()
	{
		$data['paid_rent']=$this->Rent_model->get_paid_rent($table="tenant_transaction",  array('tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_transaction.type'=>'c','tenant_transaction.audit_number'=>1)); 
		$data['expected_rent']=$this->Rent_model->get_paid_rent($table="tenant_transaction",  array('tenant_property_units.audit_number'=>1,'tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_transaction.type'=>'d','tenant_transaction.audit_number'=>1)); 
		$data['tenant_details']=$this->Rent_model->getData($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code')));
		$data['unpaid_rent']=$this->Rent_model->unpaid_rent(array('tenant_property_units.audit_number'=>1,'company_code'=>$this->session->userdata('company_code')));  
		$this->load->view('accounts/header');
		$this->load->view('accounts/paid_rent',$data);  
		$this->load->view('accounts/footer');
	}
		
	public function unpaid()
	{
		$data['paid_rent']=$this->Rent_model->get_paid_rent($table="tenant_transaction",  array('tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_transaction.type'=>'c','tenant_transaction.audit_number'=>1)); 
		$data['expected_rent']=$this->Rent_model->get_paid_rent($table="tenant_transaction",  array('tenant_property_units.audit_number'=>1,'tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_transaction.type'=>'d','tenant_transaction.audit_number'=>1)); 
		//$data['expected_rent']=$this->Rent_model->getData($table="tenant_pricing"); 
		$data['unpaid_rent']=$this->Rent_model->unpaid_rent(array('tenant_property_units.audit_number'=>1,'company_code'=>$this->session->userdata('company_code')));  
		$data['tenant_details']=$this->Rent_model->getData($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/unpaid_rent', $data);
		$this->load->view('accounts/footer');
	}

public function payment()
	{ 
		$id_no=$this->input->post('id');
		$amount=$this->input->post('amount');
		$data=array(
				'id_no'=>$this->input->post('id'),
				'property_allocated'=>$this->input->post('property_name'),
				'house_no'=>$this->input->post('house_no'),
				'floor_no'=>$this->input->post('floor_no'),
				'company_code'=>$this->session->userdata('company_code'),
				'name'=>$this->input->post('name'),
				'transaction_no'=>$this->input->post('transaction_no'),
				'business_no'=>$this->input->post('business_no'),
				'amount'=>$this->input->post('amount'),
				'payment_mode'=>$this->input->post('mode'),
				'paid_on'=>$this->input->post('paid_on'),
				'month'=>date("m"),
				'description'=>$this->input->post('description') 
				); 
	if($this->Rent_model->payment($data))
		{
			$arrears=0;
			$q=$this->Rent_model->checkArrears($id_no);
			foreach($q->result() as $row){ $arrears=$row->current_arrears;} 
			if($arrears>$amount){$new_arrears=$arrears-$amount;}else{ $new_arrears=$amount-$arrears; }
			 if($this->Rent_model->updateArrears($id_no,$new_arrears))
			 {   
			      echo json_encode(array('result'=>"ok",'data'=>1));
			 }
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}  		
	}
 
public function receivePayment()
	{
		$data['tenants']=$this->Tenant_model->showTenants(array('unit_property_details.audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant.tenant_status'=>1));
		//$data['rent']=$this->Rent_model->paidRent($condition=array('company_code'=>$this->session->userdata('company_code'),'month'=>date("m")));
		$data['received_rent']=$this->Rent_model->received_pay(array('tenant_transaction.audit_number'=>1,'tenant_transaction.type'=>'c','tenant_property_units.company_code'=>$this->session->userdata('company_code')));
		$data['payment_for']=$this->Rent_model->payment_for();
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/receive_payment',$data);
		$this->load->view('accounts/footer');
	}
  	
 public function statement($enct="")
	{ 
	    if($enct !=""){   $id=$this->input->post('id'); } 
		$data['id']=$enct;
		$hashed=new Encrypte();
		$id=$hashed->hashId($enct, $table="tenant",  $condition=array('audit_number'=>1),$searchItem="id");		
		$data['tenant_details']= $this->Rent_model->getData("tenant_property_units",$condition=array('id'=>$id)); 
		$data['units_details']= $this->Rent_model->getData($table="unit_property_details"); 
		$data['statement']= $this->Rent_model->get_statement($condition=array('tenant_id'=>$id)); 
		
		if($this->input->post('from') !="" && $this->input->post('to') !="" )
		{ 
			$from=$this->input->post('from'); 
			$to=$this->input->post('to');
			$data['statement']=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1),$from,$to);
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1),$from,$to);
			if($q->num_rows()<=0){  $data['tenant_details']= $this->Rent_model->getData("tenant_property_units",$condition=array('id'=>$id));  }
		    
		} 
		
		
	 $bal=0;$curr_bal=0; $credit=0; $debit=0; 
	   if($id==""){ redirect(base_url() ."rent/paid"); }else{
		$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id));
		if($q->num_rows()>0){
			foreach($q->result() as $row)
			{
				if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
				if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
					
			}
			$bal=$debit-$credit;
			$this->Rent_model->update_data($table="current_account", array('balance'=>$bal), array('tenant_id'=>$id));
			$data['rent_due']=$this->Rent_model->getData($table="current_account",  array('tenant_id'=>$id));
			$data['users']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'user_added_by'=>0), $table="user_info");
			$data['company_info']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')), $table="company_info");
			$this->load->view('accounts/header');
			$this->load->view('accounts/statement',$data);
			$this->load->view('accounts/footer');
			 }
		else{
				redirect(base_url() ."rent/paid"); 
			}			 
		 }   
	}

public function rent_statement($id="")
	{ 
		
		$bal=0;$curr_bal=0; $credit=0; $debit=0; 
		if($this->input->post('from') !="" && $this->input->post('to') !="")
		{   
			$from=$this->input->post('from'); 
			$to=$this->input->post('to');
			$data['statement']=$this->Rent_model->get_statement($condition=array('company_code'=>$this->session->userdata('company_code'),'tenant_transaction_view.audit_number'=>1),$from,$to); 
		 }
		else{  
		if($id=="")
		{
			$data['statement']= $this->Rent_model->get_statement($condition=array('unit_property_details.company_code'=>$this->session->userdata('company_code'),'tenant_transaction_view.audit_number'=>1));
		}else{
				$data['id']=$id;
				$data['statement']= $this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1));  
				$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id,'tenant_transaction_view.audit_number'=>1));
					if($q->num_rows()>0)
					{
							foreach($q->result() as $row)
							{
								if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
								if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
									
							}
					}
		 
			$bal=$debit-$credit;
			$this->Rent_model->update_data($table="current_account", array('balance'=>$bal), array('tenant_id'=>$id));
			$data['rent_due']=$this->Rent_model->getData($table="current_account",  array('tenant_id'=>$id));   
			}
		}	

			$this->load->view('accounts/header');
			$this->load->view('accounts/rent_statement',$data);
			$this->load->view('accounts/footer');		
	}
	
	
	 public function receive_pay()
	{
		$bal=0; $curr_bal=0; $edit_amount=0;  $payment_type_bal=0;  $temp_val=0; $item_bal=0; $credit=0; $debit=0; $tenant_transaction_id=0;
		$edit_id=$this->input->post('edit_id');
		$tenant_id=$this->input->post('tenant_id');
		$amount=$this->input->post('amount'); 
		$pay_from=$this->input->post('from');
		 if($edit_id==""){  
		//check tenant Payment balance. Unpaid amount
		$check_bal=$this->Rent_model->getData($table="tenant_transaction" , array('tenant_id'=>$pay_from));
		 if($check_bal->num_rows()>0){ foreach($check_bal->result() as $balance){ if($balance->type=="c"){$credit=$credit+$balance->amount;}else if($balance->type=="d"){ $debit=$debit+$balance->amount;} } } 
		 if($credit>=$debit){ $curr_bal=(0-$credit-$debit);} else if($credit<$debit){$curr_bal= $debit-$credit;}
 		 //end
 		$mode=$this->input->post('receipt_no');
 		if(!$mode){ $mode="";}
		$data=array(
		'tenant_id'=>$this->input->post('from'), 
		'payment_mode'=>$this->input->post('pay_mode'), 
		'payment_mode_code'=>$mode, 
		'date_paid'=>date('m/d/Y'),
		'description'=>'Receipt',		
		'amount'=>$this->input->post('amount'),   
		'receipt_no'=>'A'.date("m").date("d").rand(10,1210).date("y")."L",   
		'type'=>'c',
        'balance'=>($curr_bal-($this->input->post('amount')))		
		); 
 			
			$this->Rent_model->insert_data($table="tenant_transaction", $data);
			$c=$this->Rent_model->getData($table="tenant_transaction" , array('tenant_id'=>$pay_from,'date_paid'=>date('m/d/Y')), $order="id");
			foreach($c->result() as $rows){  $tenant_transaction_id=$rows->id;}
			
			$salt = sha1('2ab'); $salt = substr($salt, 0, 10); $enct =base64_encode(do_hash($tenant_transaction_id . $salt,'sha512') . $salt);
			
			$q=$this->Rent_model->getData($table="tenant", array('id'=>$this->input->post('from')));
			foreach($q->result() as $r){ $unit_id=$r->property_unit_id;	}
			$query=$this->Rent_model->getData($table="pricing_details" , array('unit_id'=>$unit_id),$order="priority");//get items to be paid from pricing_table
			if($query->num_rows()>0){
			foreach($query->result() as $row)
			{
			 $payment_type=$row->payment_type;   $pricing_id=$row->id;  $value=$row->payment_type_value;
			//check current balance for a given item eg Rent, Electricity
			 $r=$this->Rent_model->getData($table="tenant_payment", array('tenant_id'=>$pay_from,'pricing_details_id'=>$pricing_id), $order="id");
			 if($r->num_rows()>0){foreach($r->result() as $b){ $payment_type_bal=$b->balance;} }else{$payment_type_bal=0;} 			 
			 $value=$value+$payment_type_bal; //Sum balance and expected amount
			 if($amount>=$value)
			 {  
				$temp_val=$bal;
				if($curr_bal<=0){ $temp_val=0-$bal; }
				$this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$pay_from,'tenant_transaction_id'=>$tenant_transaction_id,'pricing_details_id'=>$pricing_id,'paid_amount'=>$value,'balance'=>$temp_val,'date_paid'=>date("Y-m-d H:i:s")));
				$amount=$amount-$value;
			 }
			 else if($value>$amount)
			 { 
				$value=$amount;
				//$temp_val=$bal; 
				if($curr_bal<=0){ $temp_val=0-$bal;}
				$temp_val=$value-$amount;
				//if($curr_bal<=0){$item_bal=0-$value;} else{ $item_bal=$value;}
				$this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$pay_from,'tenant_transaction_id'=>$tenant_transaction_id,'pricing_details_id'=>$pricing_id,'paid_amount'=>$value,'balance'=>$temp_val,'date_paid'=>date("Y-m-d H:i:s")));
				$amount=0;
			 } 
			 else if($amount==0)
			 {
				 $this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$pay_from,'tenant_transaction_id'=>$tenant_transaction_id,'pricing_details_id'=>$pricing_id,'paid_amount'=>0,'balance'=>$value,'date_paid'=>date("Y-m-d H:i:s")));
				 //break;
			 }
		}
		//$this->Rent_model->update_data($table="tenant_transaction",array('balance'=>$amount), array('tenant_id'=>$this->input->post('from')));
		$this->Rent_model->update_data($table="current_account",array('balance'=>$this->get_balance($pay_from)), array('tenant_id'=>$pay_from));
		$this->session->set_flashdata('temp','Payment details saved successfully');
		//redirect(base_url().'rent/receivePayment');
		redirect(base_url().'rent/printReceipt/'.$enct); 
		//echo json_encode(array('result'=>"ok",'data'=>1));
 }
		else
		{
			$this->session->set_flashdata('temp','Payment was not saved successfully. Try again later');
			//echo json_encode(array('result'=>"false",'data'=>0));
			//redirect(base_url().'rent/receivePayment'); 
			
			redirect(base_url().'rent/printReceipt/'.$enct); 
		}  
	}
	else{
		$initial_amount=0; $initial_balance=0; 
		//check tenant Payment balance. Unpaid amount  
		$b=$this->Rent_model->getData($table="tenant_transaction" , array('id'=>$edit_id));
		foreach($b->result() as $row){ $tenant_id=$row->tenant_id; $initial_balance=$row->balance; $initial_amount=$row->amount; }
		
		 $check_bal=$this->Rent_model->getData($table="tenant_transaction" , array('tenant_id'=>$tenant_id));
		 if($check_bal->num_rows()>0){ foreach($check_bal->result() as $balance){ if($balance->type=="c"){$credit=$credit+$balance->amount;}else if($balance->type=="d"){ $debit=$debit+$balance->amount;} } } 
		 if($credit>=$debit){ $curr_bal=(0-$credit-$debit);} else if($credit<$debit){$curr_bal= $debit-$credit;}
 		 $edit_amount=$this->input->post('amount');
		 
		 if($edit_amount>=$initial_amount)
		 {
			 $edit_bal=$initial_balance-($edit_amount-$initial_amount);
		 }
		 else
		 {
			 $edit_bal=$initial_balance+($initial_amount-$edit_amount);
		 }
		 //end
		   
		$data=array('payment_mode'=>$this->input->post('pay_mode'), 
		'payment_mode_code'=>$this->input->post('receipt_no'),
		'date_paid'=>$this->input->post('date'),
		'balance'=>$edit_bal,
		'amount'=>$this->input->post('amount')  
		);		
		 
		$this->Rent_model->update_data($table="tenant_transaction",$data, $condition=array('id'=>$edit_id,'tenant_id'=>$tenant_id));
		$tenant_transaction_id=$edit_id;
		$t=$this->Rent_model->getData($table="tenant" , array('id'=>$tenant_id));
		foreach($t->result() as $r){ $unit_id=$r->property_unit_id; }
		
		$query=$this->Rent_model->getData($table="pricing_details" , array('unit_id'=>$unit_id),$order="priority");//get items to be paid from pricing_table
			if($query->num_rows()>0){
		   $this->Rent_model->delete_data($table="tenant_payment", array('tenant_transaction_id'=>$edit_id) );
			foreach($query->result() as $row)
			{
			 $payment_type=$row->payment_type;   $pricing_id=$row->id;  $value=$row->payment_type_value;
			//check current balance for a given item eg Rent, Electricity
			 $r=$this->Rent_model->getData($table="tenant_payment", array('tenant_id'=>$this->input->post('from'),'pricing_details_id'=>$pricing_id), $order="id");
			 if($r->num_rows()>0){foreach($r->result() as $b){ $payment_type_bal=$b->balance;} }else{$payment_type_bal=0;} 			 
			 $value=$value+$payment_type_bal; //Sum balance and expected amount
			 if($amount>=$value)
			 {  
				$bal=0;
				$temp_val=$bal;
				if($curr_bal<=0){ $temp_val=0-$bal;}  
				$this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$tenant_id,'tenant_transaction_id'=>$tenant_transaction_id,'pricing_details_id'=>$pricing_id,'paid_amount'=>$value,'balance'=>$temp_val,'date_paid'=>date("Y-m-d H:i:s")));
				$amount=$amount-$value;
			 }
			 else if($value>$amount)
			 { 
				$value=$amount;
				$temp_val=$bal;
				if($curr_bal<=0){ $temp_val=0-$bal;}  
				//if($curr_bal<=0){$item_bal=0-$value;} else{ $item_bal=$value;}
				$this->Rent_model->insert_data($table="tenant_payment", array('tenant_id'=>$tenant_id,'tenant_transaction_id'=>$tenant_transaction_id,'pricing_details_id'=>$pricing_id,'paid_amount'=>$value,'balance'=>$temp_val,'date_paid'=>date("Y-m-d H:i:s")));
				$amount=0;
			 } 
			 if($amount<=0)
			 {
				 break;
			 }
		}
		$this->session->set_flashdata('temp','Data Updated successfully');
		redirect(base_url().'rent/receipts');
	}
	}
}

public function printReceipt($id="",$tenant_id="",$receipt="")
	{
		$hashed=new Encrypte();
		//$id=$hashed->hashId($id, $table="tenant_transaction",  $condition=array('audit_number'=>1),$searchItem="id");		
		$id=$hashed->hashId($id, $table="tenant_transaction",  $condition=array('audit_number >'=>0),$searchItem="id");		
		$current_reading=0; $previous_reading=0; $water_cost=0; $balance=0;
	 if($tenant_id=="")
		{   
			$q=$this->Rent_model->getData($table="tenant_transaction", array('id'=>$id));
		}else if($id=="")
		{ 
			//$tenant_id=$hashed->hashId($tenant_id, $table="tenant",  $condition=array('audit_number'=>1),$searchItem="id");		
			$tenant_id=$hashed->hashId($tenant_id, $table="tenant",  $condition="",$searchItem="id");		
			//$receipt_no=$hashed->hashId($receipt, $table="tenant_transaction",  $condition=array('audit_number'=>1),$searchItem="receipt_no");		
			$receipt_no=$hashed->hashId($receipt, $table="tenant_transaction",  $condition="", $searchItem="receipt_no");		
		    
			$q=$this->Rent_model->getData($table="tenant_transaction", array('tenant_id'=>$tenant_id,'receipt_no'=>$receipt_no));
		}
		  if($q->num_rows()>0)
		{
			foreach($q->result() as $row){  $tenant_transaction_id=$row->id; $balance=$row->balance; $payment_mode=$row->payment_mode;$amount=$row->amount; $receipt_no=$row->receipt_no;$payment_mode_code=$row->payment_mode_code; $tenant_id=$row->tenant_id; $date=$row->date_paid;$pay_for='internet';}
			$this->session->set_userdata(array('amount'=>$amount,'date_paid'=>$date,'pay_for'=>$pay_for,'payment_mode'=>$payment_mode,'receipt_no'=>$receipt_no));
			$data['amount']=$amount; $data['date_paid']=$date; $data['pay_for']=$pay_for;$data['balance']=$balance; $data['payment_mode']=$payment_mode; $data['receipt_no']=$receipt_no; $data['payment_mode_code']=$payment_mode_code; 
			$data['tenant_details']=$this->Rent_model->getData($table="tenant", array('id'=>$tenant_id));
			$data['company_details']=$this->Rent_model->getData($table="company_info");
			$data['receipt_items']=$this->Rent_model->get_receipt($tenant_transaction_id);
			$data['statement']= $this->Rent_model->get_statement($condition=array('tenant_id'=>$tenant_id)); 
			$data['current_balance']=$this->get_balance($tenant_id);
			$data['balance']=$balance;
            $month=date("m",strtotime($date));			
            $year=date("Y",strtotime($date));			
			$water_cost=$this->getWaterBill($tenant_id,$month,$year);
			$query=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$tenant_id,'month'=>$month,'year'=>$year));
		   if($query->num_rows()>0)
			  {
				foreach($query->result() as $row)
				{
					 $current_reading=$row->current_reading; $previous_reading=$row->previous_reading; 
				}
			  }
			$data['water_cost']=$water_cost; 
			$data['current_reading']=$current_reading; $data['previous_reading']=$previous_reading; 
			$data['receipt_no']=$receipt_no;  
			$this->load->view('accounts/header');
			$this->load->view('accounts/receipt', $data);
			$this->load->view('accounts/footer'); 
		 
		} 
		else{
				redirect(base_url().'rent/receivePayment');
			}    
	}
	
 
	
	public function edit_tenant_transact()
	{
		$data=array('tenant_id'=>$this->input->post('tenant'),
		'payment_mode'=>$this->input->post('pay_mode'), 
		'payment_mode_code'=>$this->input->post('receipt_no'),
		'date_paid'=>date('d-m-Y'),
		'amount'=>$this->input->post('amount') ,
		'type'=>$this->input->post('type'),
		'description'=>$this->input->post('description') 
		); 
		$this->Rent_model->insert_data($table="tenant_transaction", $data);
		if($this->db->affected_rows()>0)
		{ 
			$bal=$this->get_balance($this->input->post('tenant'));
			$this->Rent_model->update_data($table="current_account",array('balance'=>$bal), $condition=array('tenant_id'=>$this->input->post('tenant')));
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else 
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	
  public function save_expenses($page="property_expenses")
	{
		$edit_id=$this->input->post('edit_id');
		$id=$this->input->post('from');
		$property_id=$this->input->post('property');
		$expense_type=$this->input->post('expense_type');
		$invoice=$this->input->post('invoice');
		$receipt=$this->input->post('receipt_no'); $system_invoice='A'.date("m").date("d").rand(123,1200).date("y")."R";
		if($this->input->post('receipt_no')=="" || $receipt==""){ $receipt="";} 
		if($this->input->post('pay_mode')=="Cash"){ $receipt="";}
		$data=array('supplier_id'=>$id,
		'payment_mode'=>$this->input->post('pay_mode'),
		'expense_type'=>$expense_type,
		'description'=>$this->input->post('pay_for'),
		'supplier_invoice_no'=>$invoice,
		'invoice_no'=>$system_invoice,
		'property_id'=>$property_id,
		'company_code'=>$this->session->userdata('company_code'),
		'payment_method_code'=>$receipt,
		'date_paid'=>date('Y-m-d'),
		'amount'=>$this->input->post('amount')  
		); 
		if($edit_id==""){ 
			if($this->Rent_model->insert_data($table="expenses",$data))
			{ 
				$msg='Payment information saved successfully';
				//redirect(base_url().'rent/'.$page);
				 echo json_encode(array('result'=>"ok",'msg'=>$msg));
			}
			else
			{
				$msg='Payment was not saved successfully. Try again later';
				 echo json_encode(array('result'=>"false",'msg'=>$msg));
			 //redirect(base_url().'rent/'.$page);
			} 
		}
	else
	{//expense_type_id'=>$this->input->post('pay_for'),
	 $this->Rent_model->update_data($table="expenses",
		$data=array('payment_mode'=>$this->input->post('pay_mode'),'payment_method_code'=>$receipt, 'date_paid'=>date('m/d/Y'), 'amount'=>$this->input->post('amount') ,
		'description'=>$this->input->post('pay_for'),'supplier_invoice_no'=>$invoice),
		array('id'=>$edit_id));
		$msg='Data Updated successfully';
	  echo json_encode(array('result'=>"ok",'msg'=>$msg));
	 //redirect(base_url().'rent/'.$page);
	}		
	}
	
	
	public function payment_details($id)
	{
		$q=$this->Rent_model->getData($table="tenant_transaction", array('id'=>$id));
		if($q->num_rows()>0)
		{ 
		 
			 echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else 
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
 
	 
	
	public function deposits()
	{
		$id=$this->input->post('tenant_id');
		$data['pricing_details']=""; 
		$data['selected_tenant']=$id; 
		$data['pay_details']=$this->Property_model->get_data($table="tenant_pricing");
		$data['price_details']=$this->Tenant_model->get_data($table="pricing_details");
		$data['tenant_deposits']=$this->Tenant_model->get_data($table="tenant_deposit");
		$data['tenants']=$this->Tenant_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code')));
		//$this->Tenant_model->showTenants(array('company_code'=>$this->session->userdata('company_code'),'tenant.tenant_status'=>1));
		if(empty($id))
		{
				
		}
		else
		{ 
		//$data['rent']=$this->Rent_model->paidRent($condition=array('company_code'=>$this->session->userdata('company_code'),'month'=>date("m")));
		 $data['tenant_details']=$q=$this->Tenant_model->showTenants(array('id'=>$id));
         foreach($q->result() as $row){ $type=$row->tenant_type;} 
		 $data['property']=$this->Tenant_model->allocatedProperty($condition=array('id'=>$id));
		 $data['properties']=$this->Tenant_model->allocatedProperty(array('company_code'=>$this->session->userdata('company_code')));
		 $data['payment_details']=$this->Property_model->get_data($table="tenant_pricing", array('tenant_id'=>$id));
		 $data['categories']=$this->Property_model->get_data($table="unit_property_details", array('company_code'=>$this->session->userdata('company_code')));
		 $data['pricing_details']=$this->Tenant_model->get_data($table="pricing_details");
		 $data['deposits']=$this->Tenant_model->get_data($table="tenant_deposit");
		}
		
		$data['refunded_deposits']=$this->Tenant_model->get_data($table="refunded_deposits_list", $condition=array('company_code'=>$this->session->userdata('company_code')), $order_by="no");
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/deposits',$data);
		$this->load->view('accounts/footer');
	}
  	
  	
  	public function refund_deposits()
	{  
		$id=$this->input->post('tenant_id');
		$data=array(
		'tenant_id'=>$id,
		'property_id'=>$this->input->post('property_id'),
		'amount'=>$this->input->post('amount'),
		'date_refunded'=>date('Y-m-d')
		);
		$d=$this->Rent_model->getData($table="refunded_deposits",array('tenant_id'=>$id));
		if($d->num_rows()>0)
		{
			//$this->session->set_flashdata('temp','Deposit not refunded  Ensure that Tenant has not  been previously refunded');
			echo json_encode(array('result'=>"false",'msg'=>'Deposit cannot be refunded more than once'));
		}
		else{
				if($this->Rent_model->insert_data($table="refunded_deposits",$data))
				{ 
					//$this->session->set_flashdata('temp','Deposit refunded successfully');
					echo json_encode(array('result'=>"ok",'msg'=>'Deposit refunded successfully'));
					//redirect(base_url().'rent/'); 
				}
			}	 
		//$this->deposits(); 
	}
	
	
	public function receipts()
	{
		$data['tenants']=$this->Tenant_model->showTenants(array('unit_property_details.audit_number'=>1, 'company_code'=>$this->session->userdata('company_code')));
		//$data['tenants']=$this->Tenant_model->showTenants(array('unit_property_details.audit_number'=>1, 'company_code'=>$this->session->userdata('company_code'),'tenant.tenant_status'=>1));
		//$data['received_rent']=$this->Rent_model->received_pay(array('tenant_transaction.audit_number'=>1,'tenant_transaction.type'=>'c','tenant_property_units.company_code'=>$this->session->userdata('company_code')));
		$data['received_rent']=$this->Rent_model->received_pay(array('tenant_transaction.type'=>'c','tenant_property_units.company_code'=>$this->session->userdata('company_code')));
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/rent_receipts',$data);
		$this->load->view('accounts/footer');
	}
 
	public function edit_tenant_statement()
	{
		$data['tenants']=$this->Tenant_model->showTenants(array('company_code'=>$this->session->userdata('company_code'),'tenant.tenant_status'=>1));
		//$data['rent']=$this->Rent_model->paidRent($condition=array('company_code'=>$this->session->userdata('company_code'),'month'=>date("m")));
		$data['received_rent']=$this->Rent_model->received_pay(array('tenant_transaction.type'=>'c','tenant_property_units.company_code'=>$this->session->userdata('company_code')));
		$data['payment_for']=$this->Rent_model->payment_for();
		$this->load->view('accounts/header');
		$this->load->view('accounts/edit_tenant_statement',$data);
		$this->load->view('accounts/footer');
	}
	
	
	public function property_expenses()
	{
		$data['disabled']=$this->disabled;
		$data['property']=$this->Rent_model->getData($table="property", $condition=array('company_code'=>$this->session->userdata('company_code')), $order_by="id",$asc_desc="DESC");
		$data['suppliers']=$this->Rent_model->getData($table="suppliers", $condition=array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id",$asc_desc="DESC");
		$data['expenses']=$this->Rent_model->getData($table="expense_type",$condition=array('company_code'=>$this->session->userdata('company_code')));
		$data['paid_expenses']=$this->Rent_model->getExpenses(array('expense_type'=>1,'expenses.audit_number'=>1,'suppliers.company_code'=>$this->session->userdata('company_code')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/property_expenses',$data);
		$this->load->view('accounts/footer');
	}
	
	public function business_expenses()
	{
		$data['disabled']=$this->disabled;
		$data['property']=$this->Rent_model->getData($table="property", $condition=array('company_code'=>$this->session->userdata('company_code')), $order_by="id",$asc_desc="DESC");
		$data['suppliers']=$this->Rent_model->getData($table="suppliers", $condition=array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id",$asc_desc="DESC");
		$data['expenses']=$this->Rent_model->getData($table="expense_type",$condition=array('company_code'=>$this->session->userdata('company_code')));
		$data['paid_expenses']=$this->Rent_model->getExpenses(array('expense_type'=>2,'expenses.audit_number'=>1,'suppliers.company_code'=>$this->session->userdata('company_code')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/business_expense',$data);
		$this->load->view('accounts/footer');
	}
	
  
  public function suppliers()
	{
		$data['disabled']=$this->disabled;
		$data['suppliers']=$this->Rent_model->getData($table="suppliers", $condition=array('supplier_deleted'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id",$asc_desc="DESC");
		$this->load->view('accounts/header');
		$this->load->view('accounts/suppliers',$data);
		$this->load->view('accounts/footer');
	}
	
	public function showSupplier($id)
	{
		$q=$this->Rent_model->getData($table="suppliers", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code')));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}

public function expense_statement($id="")
	{ 
		 $data['suppliers']=$this->Rent_model->getData($table="suppliers", $condition=array('company_code'=>$this->session->userdata('company_code')), $order_by="id",$asc_desc="DESC");
		  
		if($this->input->post('from') !="" &&  $this->input->post('to') !="")
		{   
			$from=$this->input->post('from'); 
			$to=$this->input->post('to');
			$data['statement']=$this->Rent_model->get_expense_statement($condition=array('company_code'=>$this->session->userdata('company_code')),$from,$to); 
		 }
		else{  
				if($id=="")
				{
					$data['statement']= $this->Rent_model->get_expense_statement(array('company_code'=>$this->session->userdata('company_code')));
				}
				else
				{
					$data['id']=$id;
					$data['statement']= $this->Rent_model->get_expense_statement($condition=array('company_code'=>$this->session->userdata('company_code'),'supplier_id'=>$id));  
					$q=$this->Rent_model->get_expense_statement($condition=array('company_code'=>$this->session->userdata('company_code'),'supplier_id'=>$id));
				}
			}	
			$this->load->view('accounts/header');
			$this->load->view('accounts/expense_statement',$data);
			$this->load->view('accounts/footer');		
	}
	
	public function paid_expenses()
	{
		$q=$this->Rent_model->getData($table="expense_type",$condition=array('company_code'=>$this->session->userdata('company_code')), $order="id", $asc_desc="asc");
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
	
	
	public function getPrevBalance($id="", $par2="")
	{
		    $credit=0; $debit=0; $prev_bal=0; $rent=0;
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id));
			if($id=="")
			{
				echo json_encode(array('result'=>"false"));
			}
			else
			{
			if($q->num_rows()>0)
				{
					$count=$q->num_rows();
					foreach($q->result() as $row)
					{
						if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
						if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
						//if($count==2){  $prev_bal=$debit-$credit; }	
						if($row->type=='d'){  $rent=$row->amount; }
						$count--;
					} 
				}
			}
		
		//$return_val= $prev_bal;
		$return_val= $rent;
		if($par2==""){ } else{  $return_val=$rent; }
		
		 
		return $return_val;
	}
	
	
	public function getTenantBalance($id="")
	{
		$bal=0;
		if($id=="")
		{
			echo json_encode(array('result'=>"false"));
		}
		else
		{
			$bal=$this->get_balance($id);
			echo json_encode(array('result'=>"ok",'balance'=>$bal,'rent'=>$this->getPrevBalance($id),'expected_pay'=>$bal+$this->getPrevBalance($id)));
		}
	}
	
	
	public function get_paid_expenses($id="")
	{
		$q=$this->Rent_model->getData($table="expenses",$condition=array('id'=>$id));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
	public function get_balance($id)
	{
		 $credit=0; $debit=0; 
		 $check_bal=$this->Rent_model->getData($table="tenant_transaction" , array('tenant_id'=>$id));
		 if($check_bal->num_rows()>0){ foreach($check_bal->result() as $balance){ if($balance->type=="c"){$credit=$credit+$balance->amount;}else if($balance->type=="d"){ $debit=$debit+$balance->amount;} } } 
		 //if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit){$curr_bal= $debit-$credit;}
		 $curr_bal= $debit-$credit;
		 return  $curr_bal;
	}

	public function update_receipt()
	{ 
		  $total_pay=0; $curr_bal=0; $water_cost=0;
		  $query=$this->Tenant_model->showTenants(array('company_code'=>$this->session->userdata('company_code'),'tenant_deleted'=>1,'tenant_status'=>1));
		  if($query->num_rows()>0)
		  {
			foreach($query->result() as $row)
			{
				$tenant_id=$row->id; $unit_id=$row->property_unit_id;
				$q=$this->Tenant_model->get_data($table="current_account", array('tenant_id'=>$tenant_id));
				foreach($q->result() as $e){ $expiry_date=$e->expected_pay_date; $curr_bal=$e->balance;}
				$now=time(); 
				$water_cost=$this->getWaterBill($tenant_id);
				$diff_time=strtotime($expiry_date)-$now; 
				$days=floor($diff_time/(60*60*24));  
					if($days<0)
					{  
						$data=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id));	
						if($data->num_rows()>0)
						{ 
							$t=$this->Property_model->get_data($table="tenant_pricing", array('property_category_id'=>$unit_id,'tenant_id'=>$tenant_id));
							foreach($t->result() as $d){$total_pay=$total_pay+$d->payment_type_value;}
						} 
					 $total_pay=$total_pay+$water_cost;//update total pay= rent+water_cost
					 $invoice='A'.date("m").date("d").rand(10,1210).date("y")."L";     
					 $this->Rent_model->insert_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'date_paid'=>date("m/d/Y"),'amount'=>$total_pay,'payment_mode'=>'','payment_mode_code'=>$invoice,'description'=>'Rent', 'payment_mode_code'=>'','type'=>'d'));
					 $expected_pay_date=$this->get_next_pay($tenant_id); 
					 $this->Rent_model->update_data($table="current_account", array('balance'=>$total_pay+$curr_bal,'expected_pay_date'=>$expected_pay_date),  array('tenant_id'=>$tenant_id));
						//send email to tenant if required 
					}  
				}
			 
			 echo json_encode(array('result'=>"ok",'data'=>1));
		  }
		  else
		  {
				echo json_encode(array('result'=>"false",'data'=>0));  
		  } 
	}
	
	
	public function get_next_pay($tenant_id){
		$expiry_date=date_create(date("Y-m-d"));//$day=""; $rent_frequency="Monthly";
		$day=0;		
		$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
		if($q->num_rows()>0)
		{
			$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
			foreach($q->result() as $r){ $rent_frequency=$r->rent_frequency;}
			$s=$this->Tenant_model->get_data($table="current_account", array('tenant_id'=>$tenant_id));
			foreach($s->result() as $i){ $expiry_date=$i->expected_pay_date;}		
			if($rent_frequency=="Monthly")
			{
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(30 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			 else if($rent_frequency=="Daily"){
				  $date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(1 ."days"));
		       $day=date_format($date,"Y-m-d"); 
			} 
			else if($rent_frequency=="Quarterly"){
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(90 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Yearly"){
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(1 ."years"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Weekly"){
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(7 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
		}
		 return  $day;
		
	}
	
	public function expected_pay_day($tenant_id)
	{
		$day=""; $days_of_year=date("z", mktime(0,0,0,12,31,date('Y'))) + 1;
		$month = date("m");	 $year = date("Y"); $days_of_month=date("t",mktime(0,0,0,$month,1,$year)); 
		$expiry_date=date_create(date("Y-m-d"));
		//$day=""; $rent_frequency="Monthly"; 
		$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
		if($q->num_rows()>0)
		{
			$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
			foreach($q->result() as $r){ $date_added=$r->date_added; $rent_frequency=$r->rent_frequency; $expiry_date=$r->expected_pay_day;}
			$now=date("d");
			if($rent_frequency=="Monthly")
			{
				
				$date_added=date_create($date_added);
		        date_add($date_added,date_interval_create_from_date_string($days_of_month-$now+$expiry_date  ."days"));
		        $day=date_format($date_added,"Y-m-d");  
			}
			else if($rent_frequency=="Daily"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(1 ."days"));
				$day=date_format($date,"Y-m-d"); 
			} 
			else if($rent_frequency=="Quarterly"){
				$date=date_create($expiry_date);
		        date_add($date,date_interval_create_from_date_string(90-$now+$expiry_date ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Yearly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string($days_of_year-$now+$expiry_date."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Weekly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(7 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
		}
		 return  $day; 
		
}
	
public function getWaterBill($tenant_id="", $month="", $year="")
	{ 
		  if($month==""){ $month=date("m");}  if($year==""){ $year=date("Y"); }
		  $cost=0; $current_reading=0; $previous_reading=0; $water_unit_cost=0;
		  $query=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$tenant_id,'month'=>$month,'year'=>$year));
		  $q=$this->Tenant_model->get_data($table="tenant_property_units", array('id'=>$tenant_id));
		  
		  if($query->num_rows()>0)
		  {
			foreach($query->result() as $row)
			{
				$tenant_id=$row->tenant_id; $current_reading=$row->current_reading; $previous_reading=$row->previous_reading;
			foreach($q->result() as $w) { $water_unit_cost=$w->water_unit_cost; }
			}
		  }
		  $cost=($current_reading-$previous_reading)*$water_unit_cost;
		  
		  return  $cost;  
	}		
		 
		 
	
}
?>