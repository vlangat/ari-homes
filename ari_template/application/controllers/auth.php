<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Auth extends CI_Controller {
 
 var $user_ip=null; 
 var $ari=null;
public function __construct(){
		 parent::__construct();
		$this->load->library('email');
		$this->load->helper('email');
		$this->load->helper('security');
		$this->load->helper('file');
		$this->load->library('session'); 
		$this->load->model("Authentication_model");
		$this->load->model("Payment_model");
		$this->load->model("Admin_model");
		$this->load->library('Expired');
		$this->load->library('Encrypte');
		$this->load->library('sendSms');
		$this->load->library('Captcha');
		$this->load->helper(array('form', 'url'));
		$this->load->library('AfricasTalkingGateway');
		date_default_timezone_set('Africa/Nairobi');
		$this->user_ip=$this->input->ip_address();
		$obj=new Expired();
       $this->disabled=$obj->check_subscription_expiry(); 
 $this->ari="ARI";
      
}
public function index()
{
	$cap=new Captcha();
	$data['image']=$cap->index();
	
	$this->load->view('web/header');
	$this->load->view('web/index',$data); 
	$this->load->view('web/footer');
	
}
	
public function logout()
{
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
$this->session->sess_destroy();
//$this->load->view('login'); 
 redirect(base_url().'auth/index');
}

 public function getCaptcha()
	{ 
	    $cap=new Captcha();
		$image=""; $val=rand();
	    $image=$cap->index();
	    $val=$cap->getVal();
		
		if($image !="")
		{ 
			echo json_encode(array('result'=>'ok','image'=>$image,'val'=>$val));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','image'=>'','val'=>$val));
		} 	 
   }
   
   
 public function referAgent()
  {
		$name=$this->input->post('name');
		$email=$this->input->post('email');
		$phone=$this->input->post('phone');
		$national_id="";
		$p=$this->Authentication_model->get_data($table="user_info", array('id'=>$this->session->userdata('id')));
		if($p->num_rows()>0){ foreach($p->result() as $s){ $national_id=$s->national_id;} }
		$q=$this->Authentication_model->get_data($table="user_info", array('email'=>$email));
		if($q->num_rows()>0){ echo json_encode(array('result'=>'false')); }
		
		else{
			$q=$this->Authentication_model->get_data($table="referred_agents", array('email'=>$email));
			if($q->num_rows()>0){ echo json_encode(array('result'=>'false')); }
			else{
			if($this->Authentication_model->insert_data($table="referred_agents", array('name'=>$name,'email'=>$email,'phone'=>$phone,'added_by'=>$national_id,'date_added'=>date('d-m-Y H:i'),'status'=>1)))
			{
				//send sms and email
				$from="ARI Homes";
				$body='<font size="2"> Dear	'.$name.',<br/> your tenant '.ucfirst(strtolower($this->session->userdata('first_name'))).' has requested you to use ARI Homes Property Management System to manage rent collection. <br/>
				To sign up go to <a href="www.ari.co.ke"> www.arihomes.co.ke </a> <br/></font>';
				if($email==""){ }
				else
				{
					$this->sendMail($email,$body.$this->getFooter(),$subject="ARI Homes",$from);
				}
				$obj=new sendSms();
				$obj->send_sms($receipients=$phone,$message="Dear ".$name.", your tenant ".ucfirst(strtolower($this->session->userdata('first_name')))." has requested you to use ARI Homes Property Management System to manage rent collection. To sign up go to www.ari.co.ke",$from=$this->ari);
				echo json_encode(array('result'=>'ok'));
			}
			else
			{
				echo json_encode(array('result'=>'false'));
			} 
		}
	}
  }
  
	public function checkId()
	{
		$q=$this->Authentication_model->get_data($table="enter", array('user_info_id'=>$this->session->userdata('id')));
		if ($q->num_rows() >0) 
		{ 
			foreach($q->result() as $r){  $id=$r->sessionId;}
			
			echo json_encode(array('result'=>"ok",'sessId'=>$id));
		}
		else
		{
		echo json_encode(array('result'=>"false",'sessId'=>0));	
		} 
	}
	
	public function login()
	{ 
		 $email=$this->input->post('username');
		 $password=$this->input->post('password');
	   /*$captcha_code=$this->input->post('captcha_code');
		 $userCaptcha=$this->input->post('userCaptcha');
		 $fileName=$this->input->post('captcha_name');
		 
		if(strcmp(strtoupper($userCaptcha),strtoupper($captcha_code)) != 0) 
		{  
		    $this->session->set_flashdata('temp',"You have provided wrong image text!");
			redirect(base_url() ."auth/");
			return false;
		}*/
		//$auth_password=do_hash($password, 'sha512');
		$salt="";		
		$q=$this->Authentication_model->get_data($table="account", array('email'=>$email,'user_enabled'=>1));
		if ($q->num_rows() >0) 
		{ 
			foreach($q->result() as $r){  $salt=$r->ref_time; $user_type=$r->user_type; } 
		}
	    $auth_password = base64_encode(do_hash($password . $salt,'sha512') . $salt); 
	    $check_if_deactivated=$this->Authentication_model->login(array('email'=>$email,'user_enabled'=>2));
		$check_if_exist=$this->Authentication_model->login(array('email'=>$email,'user_enabled'=>1,'verify'=>$auth_password));
		
		if ($check_if_deactivated->num_rows() >0) 
		{  
	        $this->session->set_flashdata('error_msg','1');
			$msg="Account Deactivated, Contact your system administrator";
			$this->session->set_flashdata('temp_email',$email);
			$this->session->set_flashdata('temp',$msg); 
			$path=$_SERVER['DOCUMENT_ROOT'].'/captcha'; 
			delete_files($path, true); 
		 redirect(base_url() ."auth/index");
		}
		else if ($check_if_exist->num_rows() <=0) 
		{  
	        $this->session->set_flashdata('error_msg','1');
			$msg="Wrong username or password";
			$this->session->set_flashdata('temp_email',$email);
			$this->session->set_flashdata('temp',$msg); 
			$path=$_SERVER['DOCUMENT_ROOT'].'/captcha'; 
			delete_files($path, true); 
		 redirect(base_url() ."auth/index");
		}
		else
		{
			$id="";
			foreach($check_if_exist->result() as $row){
				$user=$row->first_name; 
				$session_id=md5(date('Y-m-d H:i')); 
				$this->session->set_userdata('first_name',$row->first_name); 
				$this->session->set_userdata('middle_name',$row->middle_name); 
				$this->session->set_userdata('last_name',$row->last_name); 
				$this->session->set_userdata('id_review','');
				$this->session->set_userdata('id',$row->id); 
				$this->session->set_userdata('session_id',$session_id); 
				$this->session->set_userdata('tenant_sign_up_id', "");
				//$this->session->set_userdata('company_name',$row->company_name);
				$this->session->set_userdata('company_code', $row->company_code);
				$this->session->set_userdata('user_type', $row->user_type);
				$this->session->set_userdata('address', $row->address);
				$this->session->set_userdata('email', $row->email);
				$this->session->set_userdata('phone', $row->mobile_number);
				$this->session->set_userdata('photo', $row->photo);
				$id=$row->id;  
				$this->session->set_flashdata('error_msg','0');
				$this->Authentication_model->update_data($table="enter", array('user_info_id'=>$row->id), array('sessionId'=>$session_id,'last_login'=>date('Y-m-d H:i:s')));
				//$user_ip=$this->input->ip_address();
				$this->Admin_model->insert_data($table="login_ip_history", array('user_id'=>$row->id,'login_time'=>date('Y-m-d H:i:s'),'ip_address'=>$this->user_ip));
			 } 
			$national_id="";  $tenant_id="";
			$path=$_SERVER['DOCUMENT_ROOT'].'/captcha'; 
			delete_files($path, true); 
			 
			if($user_type==3)
			{  
				$this->session->set_userdata('tenant_sign_up_id', $id);
				$q=$this->Authentication_model->get_data($table="user_info", array('id'=>$id));
				foreach($q->result() as $r){  $national_id=$r->national_id;	}
				  
					$this->session->set_userdata('national_id', $national_id);
					if($national_id=="")
					{
						$enct="";
					}
					else
					{
						$p=$this->Authentication_model->get_data($table="tenant", array('id_passport_number'=>$national_id));
						foreach($p->result() as $r){  $tenant_id=$r->id;  }
						
						$salt = sha1('2ab'); $salt = substr($salt, 0, 10);  
						$enct =base64_encode(do_hash($tenant_id . $salt,'sha512') . $salt);
					}					 
					 redirect(base_url() ."tenantSignUp/statement/".$enct); 
			}
			else
			{ 
				$this->session->set_userdata('tenant_sign_up_id', "");
			    redirect(base_url() ."property");
			}
		}   
	}
	
	public function signUp($new_user="")
	{
		$fileName=$this->input->post('captcha_name');
		$status=$this->input->post('status');
		if($status==1){ $this->acceptAccount(); return; }
		$mail=$this->input->post('email'); 
		$password=$this->input->post('password'); 
		$mobile_number=$this->input->post('phone'); 
		$company_name=$this->input->post('company');
		$userCaptcha=$this->input->post('userCaptcha'); 
		$captcha_code=$this->input->post('captcha_code');   
		if(strcmp(strtoupper($userCaptcha),strtoupper($captcha_code)) != 0) 
		{ 
			$msg=$this->session->set_flashdata('temp','You have provided incorrect Image Text');
			redirect(base_url() ."auth/");
			return false;		   
		}
		$activation_code=md5($mail); 
		$salt = sha1(rand());
        $salt = substr($salt, 0, 10);  
		$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		while($check_salt->num_rows() >0)
		{
			$salt = sha1(rand());
			$salt = substr($salt, 0, 10); 
			$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		}
		
		$password2 =base64_encode(do_hash($password . $salt,'sha512') . $salt);
		//$password2=do_hash($password,'sha512');  
		$code=rand(100,12000000);
		$check_companyId=$this->Authentication_model->checkCompany(array('company_code'=>$code));
		 
		while($check_companyId->num_rows() >0)
		{
			$code=rand(100,12000000);
			$check_companyId=$this->Authentication_model->checkCompany(array('company_code'=>$code));
		}
		 
		$check_if_exist=$this->Authentication_model->proof_user(array('email'=>$mail));
		if ($check_if_exist->num_rows() >0) 
		{ 
			$path=$_SERVER['DOCUMENT_ROOT'].'/captcha'; 
			delete_files($path, true); 
			$this->session->set_flashdata('error_msg','1');
			$msg="Account for the provided Email already exists. Please use a different email address";
		}
		else
		{
		$date=date_create(date("Y-m-d"));
		date_add($date,date_interval_create_from_date_string(30 ."days"));
		$expiry_date=date_format($date,"Y-m-d");	
		$data=array(
		'first_name'=>$this->input->post('first_name'),
	  	'last_name'=>$this->input->post('last_name'),	 
	  	'middle_name'=>'',	 
	  	'company_code'=>$code, 
		'mobile_number'=>$mobile_number,
		'registration_date'=>date('Y-m-d'),
		'acc_expiry_date'=>$expiry_date,
		'subscription_date'=>date('Y-m-d'),
		'email'=>$this->input->post('email') 
		);
		
	    $this->Authentication_model->signUp($data);
		$q=$this->Authentication_model->get_data($table="user_info", array('email'=>$this->input->post('email')));
		foreach($q->result() as $r){ 	$id=$r->id;	}
		//$this->Authentication_model->insert_data($tabla="enter", array('user_type'=>$this->input->post('user_type'),'user_info_id'=>$id,'verify'=>$password2,'activation'=>$activation_code,'ref_time'=>$salt,'user_enabled'=>2));
		if($this->Authentication_model->insert_data($tabla="enter", array('user_type'=>$this->input->post('user_type'),'user_info_id'=>$id,'verify'=>$password2,'activation'=>$activation_code,'ref_time'=>$salt,'user_enabled'=>2)))
		{
			$this->insertPrivilege($id, $val=1);
			if($this->input->post('user_type')==2)
			{ 
				$this->Authentication_model->insert_data($tabla="company_info", array('company_email'=>$this->input->post('email'),'company_name'=>$company_name,'mobile_number'=>$mobile_number,'user_info_id'=>$id,'company_code'=>$code)); 
			}
			
		//update package current table
			$this->Payment_model->expected_pay_date($table="package_current_account", array('company_code'=>$code), array('balance'=>0,'company_code'=>$code,'user_info_id'=>$id,'expected_pay_date'=>$expiry_date));
		//$this->Authenticate->UpdateAccountDetails($mail);
		$message=", FREE 30 Days Trial";
		if($this->input->post('user_type')==3){  $message=""; } 
		$body='
		<html>
		<div style="width:90%;padding:10px;background:ffffff;">
		<div style="padding-left:10px;line-height:20px">
		<center> <img  src="'.base_url().'images/login/ARI_Homes_Logo.png" height="60"></center>
		<p> <font size="3" align="left"> Dear  '.ucfirst(strtolower($this->input->post('first_name'))).',</font><br/>
		 Thank you for signing up for, ARI Homes property management solution'.$message.'.<br/>
		Activate your account by clicking on the button below.
		 </p>
		<br/>
		<center>
		<button style="background:brown;border: 0; height: 50px; width: 200px;"> <a href="'.base_url().'auth/activation/'.$activation_code.'" style="text-decoration:none;color:#ffffff">
		 Activate My Account</a></button>  </center>
		<h4 align="left"> Having Trouble? </h4>
		<p> If the above button does not work try copying and paste this link into your browser  </p> 
		<p   style="background:;color:#006699"> '.base_url().'auth/activation/'.$activation_code.'  </p>
		 <br/>'.$this->getFooter().'
		 </div>
		</div>
		</center>
		</body>';
		$to=$this->input->post('email');
		$from='ARI Homes';
		$subject='Account Activation';
		$this->sendMail($to,$body,$subject,$from);
		$this->session->set_flashdata('error_msg','0');
		$msg="Thank you for signing up with ARI Homes. Please check the link sent to your email (inbox or spam folder) to activate your account";
		 $this->Authentication_model->log_activity($user=$this->input->post('email'), $ip=$this->user_ip, $message="created account",$status=1);
		$path=$_SERVER['DOCUMENT_ROOT'].'/captcha'; 
		delete_files($path, true); 
		}
		else
		{
			$this->session->set_flashdata('error_msg','1');
			$msg="Account not created. Try again later";
			$this->Authentication_model->log_activity($user=$mail, $ip=$this->user_ip, $message="Tried to create account with Email ".$mail,$status=2);
			 
		}
	} 
	
	$this->session->set_flashdata('temp',$msg);
	redirect(base_url() ."auth/");	
}

public function activation($code="")
 {
	 if($code==""){  
	 $status=$this->session->set_flashdata('temp','Account activation was not successfully. Please ensure that you have clicked the link provided in your mail');
	 redirect(base_url() ."auth/");
	 } 
	 //$q=$this->Authentication_model->activation($code);
	 if($this->Authentication_model->activation($code))
	 {
		$this->session->set_flashdata('error_msg','0'); 
		$status=$this->session->set_flashdata('temp','Account activated successfully. You can now login');	
	 }
	 else
	 {
		 $this->session->set_flashdata('error_msg','1');
		 $status=$this->session->set_flashdata('temp','Account activation was not successfully. Please ensure that you have clicked the link provided in your mail');
	 }
		redirect(base_url() ."auth/");
		 
}
//forget password
public function getPassword()
	{   
		$captcha_code=$this->input->post('captcha_code');
		$userCaptcha=$this->input->post('userCaptcha');
		$fileName=$this->input->post('captcha_name');
		 
		if(strcmp(strtoupper($userCaptcha),strtoupper($captcha_code)) != 0) 
		{  
	       $this->session->set_flashdata('error_msg','1');
		   $this->session->set_flashdata('temp',"You have provided wrong image text!");
			redirect(base_url() ."auth/");
			return false;
		}
		$now=time();
		$email=$this->input->post('email'); 
		if($email==""){   redirect(base_url() ."auth/"); return false;  }    
		
		$check=$this->Authentication_model->login(array('email'=>$email));
		if($check->num_rows()>0)
		{  
			$phone="";
	        foreach($check->result() as $r){ $user_info_id=$r->id; $name=$r->first_name; $phone=$r->mobile_number;}
			$pass=do_hash($email,'sha512');
			$code=$pass;  $ref_no=rand(10000,100000);
			$data=$this->Authentication_model->getPassword($user_info_id,$pass,$reset_ref_no=md5($ref_no));
			 
			$to=$email;
			$phone="+254".substr($phone,1);
			$body='<html> 
			<div style="width:90%;padding:10px;background:#ffffff;">
			<div style="padding-left:10px;line-height:25px">
			 <center> <img  src="'.base_url().'images/login/ARI_Homes_Logo.png" height="60"></center>
			<p> <font align="left"  > Dear '.$name.',</font></p>
			 <p>
				A password reset had been requested for your account, click on the link below to reset your password. If you did not make this request, ignore this email. This link will expire in 24 hours
			 </p>
			 <p>
			   To accept changes please  click on the button below. </p><br/>
			<center>
		<button style="background:#006699;height: 50px; width: 200px; border: 0;"> <a href="'.base_url().'auth/reset/'.$pass.'/'.$now.'" style="text-decoration:none;color:#ffffff">Reset Password </a></button></center>
			<h4 align="left">   Having Trouble? </h4>
			<p> If the above button does not work try copying and paste this link into your browser  </p> 
			<font color="#006699"> <b> '.base_url().'auth/reset/'.$pass.'/'.$now.' </b> </font>
			 '.$this->getFooter().'
			</div>
			</div>
			</body>
		</html>'; 
		$from="ARI Homes";
		$subject='Password Reset'; 
	    $this->sendMail($to,$body,$subject,$from);
		$this->Authentication_model->log_activity($user=$email, $ip=$this->user_ip, "Reset password",$status=1);
		$message=$ref_no." is your ARI Homes verification code";  
		$obj=new sendSms();
		$obj->send_sms($receipients=$phone,$message,$from=$this->ari);
 	$msg=$this->session->set_flashdata('temp','Password sent to your email. Please check your inbox or spam folder');
		$this->session->set_flashdata('error_msg','0');
		redirect(base_url() ."auth/"); 
		}
		else
		{
			$this->Authentication_model->log_activity($user=$email, $ip=$this->user_ip, $message="Tried to reset password with email ".$email,$status=2);
			$msg=$this->session->set_flashdata('temp','The Email you have provided does not exist or account is not active. Please create account if not yet!');
			$this->session->set_flashdata('error_msg','1');
			redirect(base_url() ."auth/");
	   }  
 }
 
public function reset($code="",$expiry_time="")
{
	if($code=="" || $expiry_time==""){ redirect(base_url() ."auth/"); }
	$now=time();
	$diff=$now-$expiry_time; $days=floor($diff/(60*60*24));
	if($days>0) 
	{  
	    $this->session->set_flashdata('error_msg','1');
			$status=$this->session->set_flashdata('temp','The link has expired. Click on forget password link to reset your password again');
			redirect(base_url() ."auth/index");
	}
	else{
		$check=$this->Authentication_model->proof_user(array('reset_password_code'=>$code));
		if($check->num_rows()>0)
		{
			foreach($check->result() as $row){ $email=$row->email;}
			$this->Authentication_model->reset($code);
			$this->session->set_flashdata('reset_pass',$code);
			$this->session->set_flashdata('email',$email);
			$this->session->set_flashdata('error_msg','0');
			$status=$this->session->set_flashdata('password_resetting','Password Reset successful');
			
		}
		else
		{  
	          $this->session->set_flashdata('error_msg','1');
			  $status=$this->session->set_flashdata('temp','Password was not reset. Please ensure that you have clicked the link provided in your mail');
		}
	 	redirect(base_url() ."auth/");		
	}
}

public function changePassword()
	{
	     
    $captcha_code=$this->input->post('captcha_code');
	$userCaptcha=$this->input->post('userCaptcha');
     //$fileName=$this->input->post('captcha_name'); 
	if(strcmp(strtoupper($userCaptcha),strtoupper($captcha_code)) != 0) 
		{  
	      // $this->session->set_flashdata('error_msg','1');
		  // $this->session->set_flashdata('temp',"You have provided wrong image text!");
		   $this->session->set_flashdata(array('error_msg'=>'1','temp'=>'You have provided wrong image text!'));
			redirect(base_url() ."auth/index");
			return false;
		}
		
		$oldpass=$this->input->post('curr_password'); 
		$newpass=$this->input->post('new_pass');
		$code=$this->input->post('code');
		$mail=$this->input->post('email');  

		$code=md5($code);
		$salt = sha1(rand());
		$salt = substr($salt, 0, 10);  
		$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		while($check_salt->num_rows() >0)
		{
			$salt = sha1(rand());
			$salt = substr($salt, 0, 10); 
			$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		}
		$newpass=base64_encode(do_hash($newpass . $salt,'sha512') . $salt);
		 
		$this->Authentication_model->update_data("enter", array('reset_password_code'=>$oldpass,'reset_ref_no'=>$code),$data=array('verify'=>$newpass,'ref_time'=> $salt));
        $q=$this->Authentication_model->get_data("enter", array('reset_password_code'=>$oldpass,'reset_ref_no'=>$code));
		if($q->num_rows()>0)
		{
			$user_mail=$mail; $name="";
			$q=$this->Authentication_model->get_data("account",array('reset_password_code'=>$oldpass));
		    if($q->num_rows()>0){  foreach($q->result() as $r){   $name=$r->first_name; $user_mail=$r->email; } }
			
			$this->Authentication_model->update_data($table="enter", array('reset_password_code'=>$oldpass), array('reset_password_code'=>'','reset_ref_no'=>''));
			$this->Authentication_model->log_activity($user=$mail, $ip=$this->user_ip, $message="Changed  password",$status=1);
			 
			$body='<div style="line-height:20px"><img src='.base_url().'images/login/ARI-Logo"><p> Dear '.$name.', <br/> The password for your ARI Account '.$user_mail.' was just changed on '.date('d-m-Y H:i'). '.&nbsp;
			Do you recognize this activity? If this was not you please contact ARI Homes administrator. <br/> Contact our support team using the email: <a href="mailto:support@ari.co.ke">support@ari.co.ke </a>  in case of any questions</p>
			 </div>'.$this->getFooter();
			
		//	$msg=$this->session->set_flashdata('temp','Password changed successfully');
		//	$this->session->set_flashdata('error_msg','0'); 
			 $this->session->set_flashdata(array('error_msg'=>'0','temp'=>'Password changed successfully'));
			$this->sendMail($to=$user_mail,$body,$subject="Password Reset",$from="ARI Homes");
		 
			redirect(base_url() ."auth/index");
		}
		else
		{
			$this->Authentication_model->log_activity($user=$mail, $ip=$this->user_ip, $message="Failed to change reset password",$status=2);
		//	$msg=$this->session->set_flashdata('temp','Password  not changed. Try again later');
		//$this->session->set_flashdata('error_msg','1'); 
			 $this->session->set_flashdata(array('error_msg'=>'1','temp'=>'Password  not changed. Try again later'));
		 	redirect(base_url() ."auth/index");
		}   
		
	 
	}
  

	public function profile()
	{
		$data['company']=$this->Authentication_model->get_data($table="company_info", array('company_code'=>$this->session->userdata('company_code')));
		$data['data']=$this->Authentication_model->get(array('email'=>$this->session->userdata('email'),'id'=>$this->session->userdata('id'),'company_code'=>$this->session->userdata('company_code')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/singleUserAccount',$data);
		$this->load->view('accounts/footer');
	}
 
	public function userPrivileges($id="", $tokeId="")
	{
		$value=0; 
		$hashed=new Encrypte();
		$id=$hashed->hashId($id, $table="user_info",  $condition=array('audit_number'=>1),$searchItem="id");		
		$q=$this->Authentication_model->get_data($table="user_info", array('id'=>$id,'company_code'=>$this->session->userdata('company_code')));
		if($q->num_rows()<=0)
		{
		     redirect(base_url() ."auth/users");	
		}
		$data['data']=$this->Authentication_model->get_data($table="account", array('email'=>$this->session->userdata('email'),'id'=>$this->session->userdata('id'),'company_code'=>$this->session->userdata('company_code')));
		
		$p=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>1));
		if($p->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>1,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['property']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>1));
		$t=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>2));
		if($t->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>2,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['tenants']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>2));
		
		$r=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>3));
		if($r->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>3,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['rent']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>3));
		
		$s=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>4));
		if($s->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>4,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['suppliers']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>4));
		
		$sub=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>5));
		if($sub->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>5,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['subscription']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>5));
		$u=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>6));
		
		if($u->num_rows()<=0)
		{
			$this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>6,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	    }
		$data['users']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$id,'resource_id'=>6));
		$data['disabled']=$this->disabled;
		$data['user_id']=$id;
		$data['msg']="";
		$this->load->view('accounts/header');
		$this->load->view('accounts/userPrivileges',$data);
		$this->load->view('accounts/footer');
	}
	
	public function assign_privilege($page="userPrivileges",$folder="accounts")
	{
		$data['msg']="";
		$user_id=$this->input->post('user_id');
		$addProperty=$this->input->post('p_addPrivilege');
		$editProperty=$this->input->post('p_editPrivilege');
		$deleteProperty=$this->input->post('p_deletePrivilege');
		$viewProperty=$this->input->post('p_viewPrivilege');
	    $q=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>1,'user_id'=>$user_id));		
		if($q->num_rows()>0){
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>1,'user_id'=>$user_id), array('addPrivilege'=>$addProperty,'editPrivilege'=>$editProperty,'viewPrivilege'=>$viewProperty,'deletePrivilege'=>$deleteProperty));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>1,'user_id'=>$user_id,'addPrivilege'=>$addProperty,'editPrivilege'=>$editProperty,'viewPrivilege'=>$viewProperty,'deletePrivilege'=>$deleteProperty));		
		 }
		$addTenant=$this->input->post('t_addPrivilege');
		$editTenant=$this->input->post('t_editPrivilege');
		$deleteTenant=$this->input->post('t_deletePrivilege');
		$viewTenant=$this->input->post('t_viewPrivilege');
		 $r=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>2,'user_id'=>$user_id));		
		if($r->num_rows()>0){
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>2,'user_id'=>$user_id), array('addPrivilege'=>$addTenant,'editPrivilege'=>$editTenant,'viewPrivilege'=>$viewTenant,'deletePrivilege'=>$deleteTenant));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>2,'user_id'=>$user_id,'addPrivilege'=>$addTenant,'editPrivilege'=>$editTenant,'viewPrivilege'=>$viewTenant,'deletePrivilege'=>$deleteTenant));		
		 }
		$addRent=$this->input->post('r_addPrivilege');
		$editRent=$this->input->post('r_editPrivilege');
		$deleteRent=$this->input->post('r_deletePrivilege');
		$viewRent=$this->input->post('r_viewPrivilege'); 	
	    $s=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>3,'user_id'=>$user_id));		
		if($s->num_rows()>0){
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>3,'user_id'=>$user_id),array('addPrivilege'=>$addRent,'editPrivilege'=>$editRent,'viewPrivilege'=>$viewRent,'deletePrivilege'=>$deleteRent));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>3,'user_id'=>$user_id,'addPrivilege'=>$addRent,'editPrivilege'=>$editRent,'viewPrivilege'=>$viewRent,'deletePrivilege'=>$deleteRent));		
		 }
		 
		$addSupplier=$this->input->post('s_addPrivilege');
		$editSupplier=$this->input->post('s_editPrivilege');
		$deleteSupplier=$this->input->post('s_deletePrivilege');
		$viewSupplier=$this->input->post('s_viewPrivilege');
		$s=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>4,'user_id'=>$user_id));		
		if($s->num_rows()>0)
		{
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>4,'user_id'=>$user_id),array('addPrivilege'=>$addSupplier,'editPrivilege'=>$editSupplier,'viewPrivilege'=>$viewSupplier,'deletePrivilege'=>$deleteSupplier));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>4,'user_id'=>$user_id,'addPrivilege'=>$addSupplier,'editPrivilege'=>$editSupplier,'viewPrivilege'=>$viewSupplier,'deletePrivilege'=>$deleteSupplier));		
		 }
		$addSubscription=$this->input->post('sub_addPrivilege');
		$editSubscription=$this->input->post('sub_editPrivilege');
		$deleteSubscription=$this->input->post('sub_deletePrivilege');
		$viewSubscription=$this->input->post('sub_viewPrivilege');
		 
		$t=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>5,'user_id'=>$user_id));		
		if($t->num_rows()>0){
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>5,'user_id'=>$user_id),array('addPrivilege'=>$addSubscription,'editPrivilege'=>$editSubscription,'viewPrivilege'=>$viewSubscription,'deletePrivilege'=>$deleteSubscription));		
		 }
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>5,'user_id'=>$user_id,'addPrivilege'=>$addSubscription,'editPrivilege'=>$editSubscription,'viewPrivilege'=>$viewSubscription,'deletePrivilege'=>$deleteSubscription));		
		 }
		 
		$addUsers=$this->input->post('users_addPrivilege');
		$editUsers=$this->input->post('users_editPrivilege');
		$deleteUsers=$this->input->post('users_deletePrivilege');
		$viewUsers=$this->input->post('users_viewPrivilege');
		
		//$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>5,'user_id'=>$user_id), array('addPrivilege'=>$addSubscription,'editPrivilege'=>$editSubscription,'viewPrivilege'=>$viewSubscription,'deletePrivilege'=>$deleteSubscription));		    
	    $u=$this->Authentication_model->get_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id));		
		if($u->num_rows()>0)
		{
			$this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id), array('addPrivilege'=>$addUsers,'editPrivilege'=>$editUsers,'viewPrivilege'=>$viewUsers,'deletePrivilege'=>$deleteUsers));
		}
		 else
		 {
			$this->Authentication_model->insert_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id,'addPrivilege'=>$addUsers,'editPrivilege'=>$editUsers,'viewPrivilege'=>$viewUsers,'deletePrivilege'=>$deleteUsers));
		}
		
		//if($this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id), array('addPrivilege'=>$addUsers,'editPrivilege'=>$editUsers,'viewPrivilege'=>$viewUsers,'deletePrivilege'=>$deleteUsers)))
		if($this->Authentication_model->update_data($table="user_privileges", array('resource_id'=>6,'user_id'=>$user_id), array('addPrivilege'=>$addUsers,'editPrivilege'=>$editUsers,'viewPrivilege'=>$viewUsers,'deletePrivilege'=>$deleteUsers)))
		 {
			$data['msg']="User privileges saved successfully";
		}
		else
		{
			$data['msg']="No changes made to user privileges";
		}
		 
		$data['property']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>1));
		$data['tenants']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>2));
		$data['rent']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>3));
		$data['suppliers']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>4));
		$data['subscription']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>5));
		$data['users']=$this->Authentication_model->get_data($table="user_privileges",array('user_id'=>$user_id,'resource_id'=>6));
		$data['data']=$this->Authentication_model->get(array('company_code'=>$this->session->userdata('company_code'),'id <>'=>$this->session->userdata('id')));
        $data['disabled']=$this->disabled;
		$data['user_id']=$user_id;
		$this->load->view('accounts/header');
		if($folder=="payments")
		{
			redirect(base_url().'auth/users/');
		}else
		{
			$this->load->view($folder.'/'.$page,$data);
		}
		$this->load->view('accounts/footer'); 
	}
	
	
	public function agent()
	{
		$data['data']=$this->Authentication_model->get(array('email'=>$this->session->userdata('email'),'id'=>$this->session->userdata('id'),'company_code'=>$this->session->userdata('company_code')));
		$data['company']=$this->Authentication_model->getCompanyDetails(array('company_email'=>$this->session->userdata('email') ,'company_code'=>$this->session->userdata('company_code')));
		$this->load->view('accounts/header');
		$this->load->view('accounts/agentAccount', $data);
		$this->load->view('accounts/footer');
	}
	
	
  public function updateProfile()
	{
		 $data=array(
		 'first_name'=>$this->input->post('fname'), 
		 'middle_name'=>$this->input->post('mname'),
		 'last_name'=>$this->input->post('lname'),
		 'address'=>$this->input->post('address'), 
		 'town'=>$this->input->post('town'), 
		 'country'=>$this->input->post('country'), 
		 'zip_code'=>$this->input->post('zip_code'), 
		 'mobile_number'=>$this->input->post('phone')
			);
		//$this->Authentication_model->updateProfile($condition=array('id'=>$this->session->userdata('id'),'email'=>$this->session->userdata('email')),$data);
		if($this->Authentication_model->updateProfile($condition=array('id'=>$this->session->userdata('id'),'email'=>$this->session->userdata('email')),$data))
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="updated profile information",$status=1);
		  
			echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','data'=>0));
		}
		 	
  }

  public function updatePassword()
	{
		$oldpass=$this->input->post('curr_password'); 
		$newpass=$this->input->post('new_pass');   
		$salt ="0"; 
		$q=$this->Authentication_model->get_data($table="enter", array('user_info_id'=>$this->session->userdata('id')));
		if ($q->num_rows() >0) 
		{ 
			foreach($q->result() as $r){ $salt=$r->ref_time;	} 
		}
		$salt = substr($salt, 0, 10);

		$newpass=base64_encode(do_hash($newpass . $salt,'sha512') . $salt); 
		$oldpass=base64_encode(do_hash($oldpass . $salt,'sha512') . $salt); 
		$q=$this->Authentication_model->get_data($table="enter", array('user_info_id'=>$this->session->userdata('id'),'verify'=>$oldpass));
	if($q->num_rows()>0)
		{
			//$this->Authentication_model->changePass(array('user_info_id'=>$this->session->userdata('id')),$newpass,$salt); 
			if($this->Authentication_model->changePass(array('user_info_id'=>$this->session->userdata('id')),$newpass,$salt))
			{
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="changed password ",$status=1);
		  
				echo json_encode(array('result'=>'ok','data'=>1));
			}
			else
			{
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed update password",$status=2);
		  
			  echo json_encode(array('result'=>'fail','data'=>0));
			}
		}
	else
	{
		echo json_encode(array('result'=>'fail','data'=>0));
	}	
		
  }
  
  public function updateCompanyDetails()
	{
		
	 $data=array( 
			 'company_email'=>$this->input->post('email'),
			 'country'=>$this->input->post('country'), 
			 'town'=>$this->input->post('town'), 
			 'company_name'=>$this->input->post('company_name'), 
			 'address'=>$this->input->post('postal_address'), 
			 'zip_code'=>$this->input->post('zip_code'), 
			 'mobile_number'=>$this->input->post('phone')
			);
		 //$this->Authentication_model->updateCompanyProfile($condition=array('company_code'=>$this->session->userdata('company_code')),$data);
		 $files=$this->input->post('no_of_files');  
		if ($files==0) { }else {  $this->changeCompanyLogo(); }
		if($this->input->post('email')=="")
		{
						  $status=$this->session->set_flashdata('temp','Update failed! Provide your company email to update profile');
				 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to change company information",$status=2);
				  
				  redirect(base_url() ."auth/profile#tab_1_4");

		}else{
		 if($this->Authentication_model->updateCompanyProfile($condition=array('company_code'=>$this->session->userdata('company_code')),$data))
		{ 
	        $status=$this->session->set_flashdata('temp','Changes updated successfully');
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="update company profile information",$status=1);
		  
		  redirect(base_url() ."auth/profile#tab_1_4");
			//echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
		  
		 $status=$this->session->set_flashdata('temp','Changes not saved successfully');
		 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to change company information",$status=2);
		  
		  redirect(base_url() ."auth/profile#tab_1_4");
		  //echo json_encode(array('result'=>'fail','data'=>0));
		} 
	}
  }


  public function loadAgents()
	{ 
		$q=$this->Authentication_model->get(array('company_code'=>$this->session->userdata('company_code'),'id <>'=>$this->session->userdata('id')));
		if($q->num_rows()>0)
		{
		 
			echo json_encode(array('result'=>'ok','data'=>$q->result_array()));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','data'=>0));
		}
		 
  }
  
  public function checkUsers()
	{
		$q=$this->Payment_model->get_data(array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code')),$table="paying_for_users");
		if($q->num_rows()>0)
		{
		 
			echo json_encode(array('result'=>'ok','data'=>$q->result_array()));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','data'=>0));
		}
	
	}
	
public function addUser()
	{
	    
	$email=$this->input->post('email');
	$phone=$this->input->post('phone');
	$first_name=ucfirst(strtolower($this->input->post('fname')));
	$data=array('email'=>$this->input->post('email'),'mobile_number'=>$phone,'first_name'=>$this->input->post('fname'),'last_name'=>$this->input->post('lname'),'user_added_by'=>$this->session->userdata('id'),'registration_date'=>date('Y-m-d'),'company_code'=>$this->session->userdata('company_code'));
	$check_if_exist=$this->Authentication_model->proof_user(array('email'=>$email));
	if ($check_if_exist->num_rows() >0) 
		{ 
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="tried to add new user email ".$email,$status=2);
			echo json_encode(array('result'=>'false','data'=>0,'msg'=>'Email provided already exists. Please use a different email address' )); 
		}
	else
	{ 
		$u=$this->Authentication_model->get_data($table="package_payment", $condition=array('company_code'=>$this->session->userdata('company_code'),'flag'=>1,'audit_number'=>1,'remaining_users >'=>0), $order_by="id", $asc_desc="ASC" ,$limit=1);
		$remaining_users=0; $paid_users=0; $expiry_date="0000-00-00"; $years=0; $transaction_id="";
			if($u->num_rows()>0)
			{
				
				foreach($u->result() as $q)
				{  
					
					$paid_users=$q->paid_users; $remaining_users=$q->remaining_users;  
					$subscription_date=$q->date_paid;  $years=$q->years;  $transaction_id=$q->id;  
					$date=date_create(date($subscription_date));
					 
					date_add($date,date_interval_create_from_date_string($years ."years"));
					$expiry_date=date_format($date,"Y-m-d");
				}
			} 
			
		 if($remaining_users <=0)
		 {  
		 // return false
			echo json_encode(array('result'=>'false','data'=>0,'msg'=>'User not added. Please subscribe to add more users' )); 
		 }
		 else{
 
	$this->Authentication_model->addUser($data);
	$query=$this->Authentication_model->login(array('email'=>$email));
	if($query->num_rows()>0){
		$flag=1;
		$remaining_users=$remaining_users-1;
		if($remaining_users==0){ $flag=0;}
		$u=$this->Authentication_model->update_data($table="package_payment", $condition=array('company_code'=>$this->session->userdata('company_code'),'id'=>$transaction_id,'flag'=>1,'audit_number'=>1), array('company_code'=>$this->session->userdata('company_code'),'flag'=>$flag,'remaining_users'=>$remaining_users));
		
		
		foreach($query->result() as $row) { $id=$row->id; }
	    $this->insertPrivilege($id);
		//$expiry_date="0000-00-00"; 
		//$q=$this->Authentication_model->get_data($table="user_info", array('id'=>$this->session->userdata('id')));
		//if($q->num_rows()>0){ foreach($q->result() as $r) { $expiry_date=$r->acc_expiry_date; $subscription_date=$r->subscription_date; } }
		
		$this->Payment_model->update_data($condition=array('id'=>$id), $table="user_info",array('acc_expiry_date'=>$expiry_date,'subscription_date'=>$subscription_date));
		
		$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
                $check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		while($check_salt->num_rows() >0)
		{
			$salt = sha1(rand());
			$salt = substr($salt, 0, 10); 
			$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		}
        $userId=$id;
		$id =base64_encode(do_hash($id . $salt,'sha512') . $salt);
		//$account_id =base64_encode(do_hash($account_id . $salt,'sha512') . $salt);
		$user_added_by =base64_encode(do_hash($this->session->userdata('id') . $salt,'sha512') . $salt);
		$company_code =base64_encode(do_hash($this->session->userdata('company_code') . $salt,'sha512') . $salt);
		
		$to=$email;
		$from="ARI Homes";
		$subject="Account activation";
		$body='<html> 
		<div style="width:90%;padding:10px;background:ffffff;">
		<div style="padding-left:10px;line-height:25px">
		 <center> <img  src="'.base_url().'images/login/ARI_Homes_Logo.png" height="60"></center>
		<p>  Dear '.ucfirst(strtolower($first_name)).',</p>
		 <p> You were added to ARI Homes Property Management system  by '.ucfirst(strtolower($this->session->userdata('first_name'))).'( Email: '.$this->session->userdata('email').') on '. date("d-m-Y").'
		 </p>
		 <p>
		 Please activate your account by clicking on the button below. </p> <br/>
		 <center><button style="background:#006699; border: 0; height: 50px; width: 200px;"> 
		 <a href="'.base_url().'auth/userActivation/'.$id.'/'.$user_added_by.'/'.$company_code.'" style="text-decoration:none;color:#ffffff"> Activate My Account</a></button>  </center>
		<h4 align="left">  Having Trouble? </h4>
		<p> If the above button does not work try copying and paste this link into your browser  </p> 
		<font color="#006699">   '.base_url().'auth/userActivation/'.$id.'/'.$user_added_by.'/'.$company_code.'  </font>
		 '.$this->getFooter().'
		</div>
		</center>
		</body>';
				
		$this->sendMail($to,$body,$subject,$from); 
		$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="added new user to the system. User Email: ".$email,$status=1);
		  
		echo json_encode(array('result'=>'ok','data'=>1,'user_id'=>$userId,'msg'=>'User added successfully. Email notification sent to the user' )); 
	 } 
		 
		else
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="tried to add new user",$status=2);
		  
			echo json_encode(array('result'=>'false','data'=>0,'msg'=>'User not added. Please try again' )); 
		} 
	}
}

}
	
  public function removeUser($id="", $status="")
  {
	  if($id==""){ echo json_encode(array('result'=>'fail','data'=>0)); }else{
	  if($status==2){ $status=1;}else if($status==1){ $status=2;}else{  echo json_encode(array('result'=>'fail','data'=>0)); return; }   
	  
	 $this->Authentication_model->update_data("enter",$condition=array('user_info_id'=>$id),$data=array('user_enabled'=>$status));
	 $q=$this->Authentication_model->get_data("account", $condition=array('id'=>$id,'user_enabled'=>$status));
	  if($q->num_rows()>0)
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="removed user id ".$id,$status=1);
		  
			echo json_encode(array('result'=>'ok','data'=>1));
		}
		else
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to removed user id ".$id,$status=2);
		  
			echo json_encode(array('result'=>'fail','data'=>0));
		}
		
	  }
  }
//-----activate user added by admin

public function userActivation($id="",$added_by="",$account_id="")
{
	$this->session->sess_destroy();
	$hashed=new Encrypte();
	$id=$hashed->hashId($id, $table="user_info",  $condition=array('audit_number'=>1),$searchItem="id");
	$added_by=$hashed->hashId($added_by, $table="user_info",  $condition=array('audit_number'=>1),$searchItem="id");
	$account_id=$hashed->hashId($account_id, $table="user_info",  $condition=array('audit_number'=>1),$searchItem="company_code");
	 if($id=="")
	 {  
		 $status=$this->session->set_flashdata('temp','Activation was not success. Try again later');
		 redirect(base_url() ."auth/");
	 }
	$q=$this->Authentication_model->get_data($table="enter", array('user_info_id'=>$id));
	if($q->num_rows()>0)
	{
		redirect(base_url() ."auth/");
	}
	$user_type=1; $company="";
	$q=$this->Authentication_model->proof_user(array('id'=>$added_by));if($q->num_rows()>0){ foreach($q->result() as $r){$user_type=$r->user_type;}}
	$c=$this->Authentication_model->get_data($table="company_info", array('company_code'=>$account_id));
	if($c->num_rows()>0)
	{
		foreach($c->result() as $r){ $company=$r->company_name;}
	}
	$data['company']=$company;
	//$this->Authentication_model->insert_data($tabla="enter", array('user_info_id'=>$id,'password'=>'','activation'=>1,'user_type'=>$user_type,'deactivated'=>1,'user_enabled'=>1));
	if($this->Authentication_model->insert_data($tabla="enter", array('user_info_id'=>$id,'verify'=>'','activation'=>1,'user_type'=>$user_type,'deactivated'=>1,'user_enabled'=>1)))
	{
		 
		 $data['data']=$this->Authentication_model->proof_user(array('id'=>$id)); 
		 $this->session->set_flashdata('error_msg','0');
		 $status=$this->session->set_flashdata('temp','Welcome to ARI Homes Property Management system.Please complete the form below');	
		 $this->load->view('login',$data); 
	}
	else
	{  
        $this->session->set_flashdata('error_msg','1');
		$status=$this->session->set_flashdata('temp','Activation was not success. Please click the link again');	 
		redirect(base_url() ."auth/");
	}
}
//end of activation

public function acceptAccount()
	{
	$email=$this->input->post('email');
	$password=$this->input->post('password');
	$company=$this->input->post('company'); 
	$fname=ucfirst(strtolower($this->input->post('first_name')));
	$lname=ucfirst(strtolower($this->input->post('last_name')));
	//$activation_code=do_hash($email,'sha512');
	//$pass=do_hash($password,'sha512'); 
	$salt = sha1(rand()); $salt = substr($salt, 0, 10);
        $check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		while($check_salt->num_rows() >0)
		{
			$salt = sha1(rand());
			$salt = substr($salt, 0, 10); 
			$check_salt=$this->Authentication_model->get_data($table="enter", array('ref_time'=>$salt));
		}

	$pass=base64_encode(do_hash($password . $salt,'sha512') . $salt); 
	if($this->Authentication_model->acceptAccount($email,$pass,$salt))
		{
			$this->session->set_flashdata('error_msg','0');
			$msg=$this->session->set_flashdata('temp','Thank you for signing up with ARI Homes. Your account is now active, you can login');
			redirect(base_url() ."auth/");
		} 
		else
		{ 
			$this->session->set_flashdata('error_msg','1');
			$msg=$this->session->set_flashdata('temp','Account was not activated successfully');
			redirect(base_url() ."auth/");
		}  
	
	}
	
  public function changePhoto($table="")
	{ 
		$config['upload_path'] = 'media/'; 
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '5000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
        $this->upload->initialize($config);
		$targetDir='./media/';   //directory name
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);
		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i]; 
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			//$this->upload->initialize($this->set_upload_options());
			$this->upload->do_upload();
			//get file name
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
			$targetFile = $targetDir.date("Y-m-d-H:i")."_".$fileName; 
			$fileName=date("Y-m-d-H:i")."_".$fileName;
			if(move_uploaded_file($_FILES['userfile']['tmp_name'],$targetFile))
			{
			   $condition=array('id'=>$this->session->userdata('id'));
               if($table==""){$table="user_info";}
				$this->Authentication_model->update_data($table ,$condition,$data=array('photo'=>$fileName));
				//$data = array('upload_data' => $this->upload->data());
				$this->session->set_userdata('photo', $fileName);
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="change profile photo",$status=1);
				$status=$this->session->set_flashdata('temp','Photo uploaded successfully');
			}
			else
			{
				$status=$this->session->set_flashdata('temp',' <font color="red">There was an error in saving Image </font>');	 
			}
			
		$fileName = implode(',',$images); 
	}
	redirect(base_url() ."auth/profile#tab_1_2");
	}
////end of upload function


  public function changeCompanyLogo()
	{
		$config['upload_path'] = './media/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		//$this->load->library('upload', $config);
		 $this->upload->initialize($config); //use initialize  when upload library is set to autoload
		if ( ! $this->upload->do_upload('photo'))
		{
			$error = array('error' => $this->upload->display_errors());
			return;//$status=$this->session->set_flashdata('img_info',' <font color="red"> There was an error in saving Image </font> ');	 
		}
		else
		{
		$condition=array('company_code'=>$this->session->userdata('company_code'));
		$upload_data = $this->upload->data(); 
		$file_name =   $upload_data['file_name'];
		$table="company_info";
		$this->Authentication_model->update_data($table="company_info", $condition,array('logo'=>$file_name)); 
		$data = array('upload_data' => $this->upload->data());
               //$this->session->set_userdata('photo', $file_name);
			   $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="changed company logo ",$status=1);
		  
		$status=$this->session->set_flashdata('temp','Changes made successfully');
		}
		 redirect(base_url() ."auth/profile#tab_1_4");
	}

 
/*	public function subscription_expiry_date()
	{
		date_default_timezone_set('Africa/Nairobi'); 
		$disabled=""; $expiry_date=""; 
		$subscription=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_payment", $order_by="id");
		$expiring_date=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
		$days=0;		
		
		if($expiring_date->num_rows()<=0)
		{
			$disabled="";  
		}
		else{
			
			foreach($expiring_date->result() as $e){ $expiry_date=$e->expected_pay_date;  }
		    foreach($subscription->result() as $row)
			{
				 $package_id=$row->package_id;  $date_paid=$row->date_paid;
			}
			if(!empty($date_paid))
			{
				$now=time();
				$total_days=strtotime($expiry_date)-strtotime($date_paid);
				$total_days=floor($total_days/(60*60*24));
				$diff_time=strtotime($expiry_date)-$now;
				$days=floor($diff_time/(60*60*24)); 
			} 		
		   
	}
	 	
		 	echo json_encode(array('days'=>$days));
}*/

public function subscription_expiry_date()
	{
		date_default_timezone_set('Africa/Nairobi'); 
		$disabled=""; $expiry_date=""; 
		$subscription=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_payment", $order_by="id");
		$p=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
		$expiring_date=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'id'=>$this->session->userdata('id')), $table="user_info");
		$days=0;		
		
		if($p->num_rows()<=0)
		{
			$disabled="";  
		}
		else{
			
			foreach($expiring_date->result() as $e){ $expiry_date=$e->acc_expiry_date;  }
		  /*foreach($subscription->result() as $row)
			{
				$package_id=$row->package_id;  $date_paid=$row->date_paid;
			}*/
			//if(!empty($date_paid))
			//{
				 $now=time();
				 //$now=date('Y-m-d');
				 $total_days=strtotime($expiry_date)-strtotime($now);
				$total_days=floor($total_days/(60*60*24));
				$diff_time=strtotime($expiry_date)-$now;
				$days=floor($diff_time/(60*60*24)); 
			//} 		
		   
	}
	 	
	echo json_encode(array('days'=>$days));
}


public function users()
	{
	    $data['users']=$this->Authentication_model->get_data($table="account", array('company_code'=>$this->session->userdata('company_code')));
		$data['disabled']=$this->disabled; 
		$data['msg']=""; 
		$balance=0;
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		if($package_bal->num_rows()>0){ foreach($package_bal->result() as $b){ $balance=$b->balance; }  }
		$data['balance']=$balance;
		$data['success']=0;
		$this->load->view('accounts/header');
		$this->load->view('payments/renewSubscription', $data);
		$this->load->view('accounts/footer');
	}


public function checkPrivilege($resourceId="")
{
	$userId=$this->session->userdata('id');
	$q=$this->Authentication_model->get_data($table="user_privileges", array('user_id'=>$userId,'resource_id'=>$resourceId));
	$addPrivilege=0; $editPrivilege=0; $viewPrivilege=0; $deletePrivilege=0; $updatePrivilege=0;
	if($q->num_rows()>0)
		{
		  foreach($q->result() as $row)
			{
				$addPrivilege=$row->addPrivilege;
				$editPrivilege=$row->editPrivilege;
				//$updatePrivilege=$row->updatePrivilege;
				$viewPrivilege=$row->viewPrivilege;
				$deletePrivilege=$row->deletePrivilege;
			}
		  
		}  
		echo  json_encode(array('add'=>$addPrivilege,'edit'=>$editPrivilege,'view'=>$viewPrivilege,'delete'=>$deletePrivilege));
}


public function insertPrivilege($id="", $value=0)
{
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>1,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>2,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>3,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>4,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>5,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
	   $this->Authentication_model->insert_data($tabla="user_privileges", array('user_id'=>$id,'resource_id'=>6,'addPrivilege'=>$value,'editPrivilege'=>$value,'deletePrivilege'=>$value,'viewPrivilege'=>$value));
		
	
}
 
 public function getFooter()
 {
	 $footer='<br/>
		<p   align="left"> Thanks, <br/>  
		 Team ARI  Homes 
		</p>
		 <div>
			<font size="2"> <center> &copy; '.date("Y").' ARI Homes   </center> </font>
			<center> <font> <a href="mailto:support@ari.co.ke">support@ari.co.ke </a> | +254 789 502 424 | +254 789 732 828 </font> </center> 
		   <center><font size="2"> 1 <sup>st </sup> Floor | All Africa Council of Churches | Sir Francis Ibiam House | Waiyaki Way, Westlands, Nairobi  <br/>
			 
			 <a href="'.base_url().'media/ARI Homes Terms and Conditions 02062017.pdf">Terms </a> </font>
			 </center>
			 
			</div>';
			return $footer;
 }
 public function sendMail($to,$body,$subject,$from)
    {
		$this->load->library('email');
		$this->email->from('support@ari.co.ke',$from);
		$this->email->to($to); 
		$this->email->subject($subject);
		$this->email->message($body);	
		$this->email->set_mailtype("html");
		$this->email->send();
		 //echo $this->email->print_debugger();
		$this->email->print_debugger();

	}
}
?>