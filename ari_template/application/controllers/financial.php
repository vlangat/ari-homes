<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financial extends CI_Controller {
	public function __construct()
	{
		parent::__construct();   
		$this->load->model("Rent_model");
		$this->load->model("Tenant_model"); 		
		$this->load->model("Payment_model"); 		
		$this->load->model("Property_model");
		$this->load->library('Expired');
		$this->load->helper('security');
		$this->load->library('rent_class');  
		$this->load->library('pdf');
        date_default_timezone_set('Africa/Nairobi'); 
		$obj=new Expired();  
		$this->disabled=$obj->check_subscription_expiry(); 		
	}

	public function property_report()
	{
		$total_units=0; $total_tenants=0;
		$id=$this->input->post('property');
		$month=$this->input->post('month');
		if($month==""){ $month=date('m');}
		$data['company_details']=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['property']=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		if($id==""){
			$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						$id=$row->id;
					}
				}
				$data['prop_report']=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
			
			}
		else{ 
		$data['prop_report']=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
		$data['property']=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		
		}
		$q=$this->Property_model->get_data($table="unit_property_details" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$r=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants_payment']=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'))); 
		$data['landlord']=$this->Property_model->get_data($table="landlord" ,array('company_code'=>$this->session->userdata('company_code'))); 
		foreach($q->result() as $row){ $total_units=$total_units+$row->total_units;   }
		foreach($r->result() as $rows){ $total_tenants=$total_tenants+1;   }
		$data['total_units']=$total_units; $data['total_tenants']=$total_tenants; $data['month']=$month;
		
		$this->load->view('accounts/header');
		$this->load->view('financial/property_report',$data);  
		$this->load->view('accounts/footer');
	}
		
	 
	public function landlord_report()
	{
		$total_units=0; $total_tenants=0;
		$id=$this->input->post('landlord');
		$month=$this->input->post('month');
		if($month==""){ $month=date('m');}
		$data['company_details']=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		 if($id==""){
			$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						$id=$row->landlord;
					}
				}
				$data['landlord_report']=$this->Property_model->get_data($table="property" ,array('landlord'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
			
			}
		else{ 
		$data['landlord_report']=$this->Property_model->get_data($table="property" ,array('landlord'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		}
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'landlord'=>$id)); 
		$data['tenants_payment']=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'))); 
		$data['landlords']=$this->Property_model->get_data($table="landlord" ,array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'))); 
		$data['landlord_details']=$this->Property_model->get_data($table="landlord" ,array('audit_number'=>1,'id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		$data['month']=$month; $data['selected_landlord']=$id; 
		$this->load->view('accounts/header');
		$this->load->view('financial/landlord_report',$data);  
		$this->load->view('accounts/footer');
	}
	
	public function property_expense()
	{
		$total_units=0; $total_tenants=0;
		$id=$this->input->post('property');
		$month=$this->input->post('month');
		if($month==""){ $month=date('m');}
		$data['company_details']=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['property']=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		if($id==""){
			$q=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code')));
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						$id=$row->id;
					}
				}
				$data['prop_report']=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
			
			}
		else{ 
		$data['prop_report']=$this->Property_model->get_data($table="property" ,array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'id'=>$id)); 
		 }
		$q=$this->Property_model->get_data($table="unit_property_details" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$r=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id)); 
		$data['tenants_payment']=$this->Property_model->get_data($table="tenant_transaction_view", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'))); 
		$data['landlord']=$this->Property_model->get_data($table="landlord" ,array('company_code'=>$this->session->userdata('company_code'))); 
		foreach($q->result() as $row){ $total_units=$total_units+$row->total_units;   }
		foreach($r->result() as $rows){ $total_tenants=$total_tenants+1;   }
		$data['total_units']=$total_units; $data['total_tenants']=$total_tenants; $data['month']=$month;
		 
		$data['suppliers']=$this->Property_model->get_data($table="suppliers" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['expenses']=$this->Property_model->get_data($table="expenses" ,array('property_id'=>$id,'expense_type'=>1)); 
		$this->load->view('accounts/header');
		$this->load->view('financial/property_expense',$data);  
		$this->load->view('accounts/footer');
	}
	
	public function profit_loss()
	{
		$year=$this->input->post('year');
		$month=$this->input->post('month');
		if($year==""){ $year=date('Y');}
		if($month==""){ $month=date('m');}
		$data['user_info']=$this->Property_model->get_data($table="user_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['company_details']=$this->Property_model->get_data($table="company_info" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['property']=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		 
		$data['year']=$year; $data['month']=$month; 
		$data['suppliers']=$this->Property_model->get_data($table="suppliers" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['expenses']=$this->Property_model->get_data($table="expenses" ,array('expense_type'=>2,'company_code'=>$this->session->userdata('company_code'))); 
		$this->load->view('accounts/header');
		$this->load->view('financial/profit_loss',$data);  
		$this->load->view('accounts/footer');
	}
	
	public function business_expense()
	{
		$year=$this->input->post('year');
		$month=$this->input->post('month');
		if($year==""){ $year=date('Y');}
		if($month==""){ $month=date('m');}
		$data['user_info']=$this->Property_model->get_data($table="user_info",array('company_code'=>$this->session->userdata('company_code'))); 
		$data['company_details']=$this->Property_model->get_data($table="company_info",array('company_code'=>$this->session->userdata('company_code'))); 
		$data['property']=$this->Property_model->get_data($table="property" ,array('company_code'=>$this->session->userdata('company_code'))); 
		 
		$data['year']=$year; $data['month']=$month; 
		$data['suppliers']=$this->Property_model->get_data($table="suppliers" ,array('company_code'=>$this->session->userdata('company_code'))); 
		$data['expenses']=$this->Property_model->get_data($table="expenses" ,array('expense_type'=>2,'company_code'=>$this->session->userdata('company_code'))); 
		
		$this->load->view('accounts/header');
		$this->load->view('financial/business_expense',$data);  
		$this->load->view('accounts/footer');
	}
		
	 
	public function edit_deposit()
	{
		if($this->Rent_model->update_data($table="refunded_deposits", $data=array('amount'=>$this->input->post('amount')), $condition=array('audit_number'=>1,'no'=>$this->input->post('id'))))
		 {
		 
			echo json_encode(array('result'=>'ok','status'=>1));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','status'=>0));
		}
		
	}
	
	public function remove_deposit()
	{
		if($this->Rent_model->update_data($table="refunded_deposits", $data=array('audit_number'=>2), $condition=array('audit_number'=>1,'no'=>$this->input->post('id'))))
		{
			echo json_encode(array('result'=>'ok','status'=>1));
		}
		else
		{
			echo json_encode(array('result'=>'fail','status'=>0));
		}
		
	}
	
	public function remove_expenses()
	{
		if($this->Rent_model->update_data($table="expenses", $data=array('audit_number'=>2), $condition=array('audit_number'=>1,'id'=>$this->input->post('id'))))
		$q=$this->Rent_model->getData($table="expenses", $data=array('audit_number'=>2,'id'=>$this->input->post('id')));
		if($q->num_rows()>0)
		 {
		   echo json_encode(array('result'=>'ok','status'=>1));
		 }
		else
		{
		  echo json_encode(array('result'=>'fail','status'=>0));
		} 
	}
	
 
}