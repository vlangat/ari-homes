<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	var $tokenId=null;
	public function __construct()
	{
		parent::__construct(); 
		$this->load->library('email');
		$this->load->helper('email');
		$this->load->helper('security'); 
		$this->load->library('session');  
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Africa/Nairobi');
		$this->load->model('Chat_model');
	}
	
	
	public function send_message()
	{
		$message = $this->input->get('message', null); 
		//$nickname =$this->session->userdata('first_name');  
		$email =$this->session->userdata('email');  
		$guid = $this->input->get('guid', '');
		$token_id="";
		$q=$this->Chat_model->get_data($table="chat_messages",array('email'=>$email,'chatType'=>1,'status'=>'open'));
		if($q->num_rows()<=0){ $token_id=rand(1000,20000); }else{ foreach($q->result() as $r){$token_id=$r->tokenId;}}
		$this->tokenId=$token_id;$this->session->set_userdata('tokenId',$token_id);
		$data = array(
			'message'	=> (string) $message, 
			'email'	=> (string) $email,
			'guid'		=> (string)	$guid,
			'timestamp'	=> time(),
			'chatType'=>1,
			'status'=>'open',
			'tokenId'=>$this->tokenId
		);
		$this->Chat_model->add_message($data);
		
		$this->_setOutput($message);
	}
	
	public function send_admin_message()
	{
	    $token_id="";
		$message = $this->input->get('message', null); 
		$token_id = $this->input->get('token', null); 
		$chatType = 2; 
		//$nickname =$this->session->userdata('first_name');  
		$email =$this->session->userdata('email');  
		$guid = $this->input->get('guid', '');
		if($token_id==""){
		$q=$this->Chat_model->get_data($table="chat_messages",array('email'=>$email,'chatType'=>1,'status'=>'open'));
		if($q->num_rows()<=0){ $token_id=rand(1000,20000); }else{ foreach($q->result() as $r){$token_id=$r->tokenId;}}
		$this->tokenId=$token_id;$this->session->set_userdata('tokenId',$token_id);
		}
		$data = array(
			'message'	=> (string) $message, 
			'email'	=> (string) $email,
			'guid'		=> (string)	$guid,
			'timestamp'	=> time(),
			'chatType'=>$chatType,
			'status'=>'open',
			'tokenId'=>$token_id
		);
		$this->Chat_model->add_message($data);
		
		$this->_setOutput($message);
	}
	
	
	public function get_messages()
	{
		$timestamp = $this->input->get('timestamp', null);
		$tokenId=$this->session->userdata('tokenId');		
		if($tokenId !="")
		{  
			$messages = $this->Chat_model->get_messages($timestamp,$tokenId); 
		  
		$this->_setOutput($messages);
		}
	}
	
	public function get_prev_messages()
	{    $tokenId="";
		    $q=$this->Chat_model->get_data($table="chat_messages",array('email'=>$this->session->userdata('email'),'chatType'=>1,'status'=>'open'),$order_by="id",$asc_desc="ASC",$limit="1");
			if($q->num_rows()>0){  foreach($q->result() as $r){  $tokenId=$r->tokenId; } }
			 
			$messages = $this->Chat_model->fetch_recent_messages(array('status'=>'open','tokenId'=>$tokenId));
		  
		  $this->_setOutput($messages);
		
	}
	
	public function get_customer_messages()
	{
	
		$timestamp = $this->input->get('timestamp', null);
		$tokenId=$this->input->get('tokenId', null);		
		if($tokenId !="")
		{  
			$messages = $this->Chat_model->get_messages($timestamp,$tokenId); 
		}
		else{   
		$q=$this->Chat_model->get_data($table="chat_messages",array('email'=>$this->session->userdata('email'),'chatType'=>1,'status'=>'open'),$order_by="id",$asc_desc="DESC",$limit="1");
			if($q->num_rows()>0){  foreach($q->result() as $r){  $tokenId=$r->tokenId; } }
			 
			$messages = $this->Chat_model->fetch_recent_messages(array('status'=>'open','tokenId'=>$tokenId));
		    } 
		 
		  $this->_setOutput($messages);
    }
    
    
	public function get_admin_messages()
	{
		$timestamp = $this->input->get('timestamp', null);
		$tokenId=$this->input->get('tokenId');		
		if($timestamp !=""){
			 $messages = $this->Chat_model->get_messages($timestamp,$tokenId); 
		   }else
		   {
			$messages = $this->Chat_model->fetch_recent_messages(array('tokenId'=>$tokenId));    
		   }
		//$messages = $this->Chat_model->fetch_recent_messages(); 
		$this->_setOutput($messages);
	}
	
	
	private function _setOutput($data)
	{
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		
		echo json_encode($data);
	}
}

?>