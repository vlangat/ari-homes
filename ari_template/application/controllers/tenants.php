<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tenants extends CI_Controller {
	var $disabled="";
	 var $user_ip=null; 
	public function __construct()
	{
		parent::__construct();  
		$this->load->model("Authentication_model");
		$this->load->model("Property_model"); 
		$this->load->model("Tenant_model"); 
		$this->load->model("Rent_model"); 
		$this->load->model("Payment_model"); 
		$this->load->library('upload');
		$this->load->library('Expired');
		$this->load->helper('security');
		$this->load->library('rent_class');
		$this->load->library('Encrypte');
		date_default_timezone_set('Africa/Nairobi');
		$this->user_ip=$this->input->ip_address();
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry();
	}
	public function index()
	{
		 //$data['tenants']=$this->Tenant_model->showTenants(array('tenannt.audit_number'=>1,'unit_property_details.company_code'=>$this->session->userdata('company_code')));
		$data['tenants']=$this->Tenant_model->get_data(array('tenant_property_units'),array('audit_number'=>1,'tenant_status'=>1,'company_code'=>$this->session->userdata('company_code')), $order_by="id");
		$data['current_acc']=$this->Property_model->get_data($table="current_account", array('audit_number'=>1));
		$this->load->view('accounts/header');
		$this->load->view('accounts/viewTenants',$data);
		$this->load->view('accounts/footer');
	}
	
 
  public function viewTenant($tenant_id="", $msg="")
	{
		 $id="";  
		 $data['disabled']=$this->disabled;
		 if($tenant_id=="" && !empty($this->input->post('checkbox'))){ foreach($this->input->post('checkbox') as $value) {  $id=$value; } }else{  $id=$tenant_id;}
		 if($id==""){ $id=$this->input->post('id'); }
		 if($id==""){ $this->index(); return; }
		 $data['msg']=$msg; 
		 $data['tenant_details']=$q=$this->Tenant_model->showTenants(array('id'=>$id,'tenant.audit_number'=>1));
         foreach($q->result() as $row){$type=$row->tenant_type;} 
		 $data['property']=$this->Tenant_model->allocatedProperty($condition=array('id'=>$id));
		 $data['properties']=$this->Tenant_model->allocatedProperty(array('company_code'=>$this->session->userdata('company_code')));
		 $data['categories']=$this->Property_model->get_data($table="unit_property_details", array('company_code'=>$this->session->userdata('company_code')));
		 $data['payment_details']=$this->Property_model->get_data($table="tenant_pricing", array('tenant_id'=>$id));
		 $data['paid_rent']=$this->Rent_model->getData($table="tenant_transaction",  array('tenant_id'=>$id,'type'=>'c')); 	 
		 $data['pricing_details']=$this->Tenant_model->get_data($table="pricing_details");
		 $data['deposits']=$this->Tenant_model->get_data($table="tenant_deposit");
		 $data['current_acc']=$this->Property_model->get_data($table="current_account", array('audit_number'=>1));
		 $data['documents']=$this->Tenant_model->get_data($table="media", array('tenant_id'=>$id,'deleted'=>1));
         if($type=="residential"){ $page="viewSingleTenant";} else{ 	$page="viewCommercialTenant";	}
		 $this->load->view('accounts/header');
		 $this->load->view('accounts/'.$page,$data);
		 $this->load->view('accounts/footer');  
	}
			 
	public function viewCommercialTenant($tenant_id="", $msg="")
	{
		 if($tenant_id=="" && !empty($this->input->post('checkbox'))){ foreach($this->input->post('checkbox') as $value) {  $id=$value; } }else{  $id=$tenant_id;}
		 if($id==""){ $id=$this->input->post('id'); }
		 if($id==""){ $this->index(); return; }
		 
		$data['msg']=$msg; 
		$data['disabled']=$this->disabled;
		$data['tenant_details']=$q=$this->Tenant_model->showTenants(array('id'=>$id,'tenant.audit_number'=>1)); 
		$data['property']=$this->Tenant_model->get_data($table="property"); 
		$data['properties']=$this->Tenant_model->allocatedProperty();
		$data['categories']=$this->Property_model->get_data($table="unit_property_details", array('company_code'=>1000));
		$data['payment_details']=$this->Property_model->get_data($table="tenant_pricing", array('tenant_id'=>$id));	
		$data['documents']=$this->Tenant_model->get_data($table="media", array('tenant_id'=>$id,'deleted'=>1));
		$this->load->view('accounts/header');
		$this->load->view('accounts/viewCommercialTenant',$data);
		$this->load->view('accounts/footer');  
	}
 
	public function getTennantDetails($id="")
	{ 
	     if($id==""){ redirect(base_url().'tenants/'); }
		$data=$this->Tenant_model->showTenants(array('no'=>$id,'tenant.audit_number'=>1));
		if($data->num_rows()>0)
		{
		   echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}         
	  else
		 {
			echo json_encode(array('result'=>"false",'data'=>0));
		 }
                 
	}

	public function getCompanyDetails($id)
	{ 
		$data=$this->Tenant_model->showCommercialTenants(array('no'=>$id));
		if($data->num_rows()>0)
		{
		  echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	public function getData($id="")
	{ 
	   if($id==""){ redirect(base_url().'tenants/'); }
		$data=$this->Tenant_model->showCommercialTenants(array('no'=>$id));
		if($data->num_rows()>0)
		{
		 
			 echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
		}
		else
		{
			$data=$this->Tenant_model->showTenants(array('no'=>$id,'tenant.audit_number'=>1));
			if($data->num_rows()>0)
			{
			  echo json_encode(array('result'=>"ok",'data'=>$data->result_array()));
			}
			else { echo json_encode(array('result'=>"false",'data'=>0)); }
		}
	}
	
	public function individual($id="",$msg="",$tenant_id="", $status="0")
	{
	    $hashed=new Encrypte();
	    $id=$hashed->hashId($id, $table="property",  $condition=array('audit_number'=>1),$searchItem="id");
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['package_account']=$this->Property_model->get_data($table="package_current_account",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['property']=$this->Tenant_model->allocatedProperty(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if(!empty($id))
		{
			$data['property']=$this->Tenant_model->allocatedProperty(array('id'=>$id));
		}
		$data['category']=$this->Property_model->getCategory();
        $data['msg']=$msg;  $data['status']=$status;  $data['tenant_id']=$tenant_id;
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/single_tenants',$data);
		$this->load->view('accounts/footer');
	}
	public function commercial($id="", $msg="",$tenant_id="", $status="0")
	{
		
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['package_account']=$this->Property_model->get_data($table="package_current_account",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['property']=$this->Tenant_model->allocatedProperty(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['msg']=$msg; $data['status']=$status;  $data['tenant_id']=$tenant_id;
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('accounts/commercialTenant',$data);
		$this->load->view('accounts/footer');
	}
	
	public function waterManagement($msg="",$id="")
	{ 
		if($id=="")
		{
			$id=$this->input->post('property_id');
		}
		if($id=="")
		{
			$q=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		    if($q->num_rows()>0)
		    { $x=0;
			   foreach($q->result() as $r)
			   { 
					if($x==1){break;} $id=$r->id; $x++;
			   } 
			} 
		}
		$data['disabled']=$this->disabled;
		$data['msg']=$msg;
		$data['selected_property']=$id;
		$data['id']=$id;
		$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['water']=$this->Property_model->get_data($table="water_management",array('audit_number'=>1,'month'=>date('m'),'year'=>date('Y')));
		$data['property']=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if($id !="")
		{
			$data['tenants']=$this->Property_model->get_data($table="tenant_property_units",array('company_code'=>$this->session->userdata('company_code'),'property_id'=>$id,'audit_number'=>1));
		}
		 
		$this->load->view('accounts/header');
		$this->load->view('accounts/waterManagement',$data);
		$this->load->view('accounts/footer'); 
	}
	
	public function waterReading($msg="")
	{
		$total=$this->input->post('total_number'); 
		$month=date('m');$year=date('Y');
		for($x=1;$x<=$total;$x++)
		{
			$id=$this->input->post('id'.$x.'');
			$curr_reading=$this->input->post('curr_reading'.$x.'');
			$prev_reading=$this->input->post('prev_reading'.$x.'');
			$q=$this->Tenant_model->get_data($table="water_management", array('tenant_id'=>$id,'month'=>$month,'year'=>$year));
			$s=""; $r="";
			if($q->num_rows()<=0)
			{ 
				$s=$this->Tenant_model->insert_data($table="water_management",array('current_reading_date'=>date('Y-m-d'),'current_reading'=>$curr_reading,'previous_reading'=>$prev_reading,'tenant_id'=>$id,'month'=>$month,'year'=>$year));
			}
			else
			{
				
				$r=$this->Tenant_model->update_data($table="water_management", $condition=array('tenant_id'=>$id), array('current_reading_date'=>date('Y-m-d'),'current_reading'=>$curr_reading,'previous_reading'=>$prev_reading));
			}
		}
		if($s||$r)
		{
			$msg="Data saved successfully";
		}
		 	
	 $this->waterManagement($msg,$this->input->post('id_no')); 
	}
	
	
	public function add_new_reading($msg="")
	{
		$c=json_encode(array('result'=>"false",'data'=>0));
		$q=$this->Tenant_model->get_data($table="tenant_property_units", $condition=array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if($q->num_rows()>0)
		{
			foreach($q->result() as $r)
			{
				$id=$r->id;  
				$query=$this->Tenant_model->get_data($table="water_management", $condition=array('tenant_id'=>$id), $order_by="id", $asc_desc="DESC");
				foreach($query->result() as $s)
				{ 
					$curr_reading=$s->current_reading; $prev_reading=$s->previous_reading;   $month=$s->month; $year=$s->year;
					 
					if($this->Tenant_model->update_data($table="water_management", $condition=array('tenant_id'=>$id,'month'=>$month,'year'=>$year), array('current_reading'=>$curr_reading,'previous_reading'=>$prev_reading)))
					 
					$y=$this->Tenant_model->get_data($table="water_management", $condition=array('tenant_id'=>$id,'month'=>date('m'),'year'=>date('Y')), $order_by="id", $asc_desc="DESC");
					if($y->num_rows()>0)
					{
						if($this->Tenant_model->update_data($table="water_management", $condition=array('tenant_id'=>$id,'month'=>date('m'),'year'=>date('Y')), array('current_reading'=>$curr_reading,'previous_reading'=>$prev_reading)))
						{
							$c=json_encode(array('result'=>"false",'data'=>1));
						}
					}
					else
					{
						if($this->Tenant_model->insert_data($table="water_management", $data=array('tenant_id'=>$id,'previous_reading'=>$curr_reading,'current_reading'=>0,'previous_reading_date'=>date('Y-m-d'),'current_reading_date'=>date('Y-m-d'),'month'=>date('m'),'year'=>date('Y'))))
				        {
							$c=json_encode(array('result'=>"ok",'data'=>1));
						}
					}
				} 
			}       
			  
		} 
		echo $c; 
	}
	
	public function landlord($msg="")
	{
		$data['disabled']=$this->disabled;
		$data['msg']=$msg;
		$data['property']=$this->Property_model->get_data($table="property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		$data['landlords']=$this->Property_model->get_data($table="landlord",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1),$order_by="id");
		$this->load->view('accounts/header');
		$this->load->view('accounts/landlord',$data);
		$this->load->view('accounts/footer');  
	}
	

	public function assign_landlord_property()
	{
		$msg=""; 
		$property_allocated=$this->input->post('property_id');  
		$landlord_id=$this->input->post('landlord_id'); 
		if($property_allocated=="")
		{
			$msg="Data not updated successfully"; 
			echo json_encode(array('result'=>"false",'msg'=>$msg));
		}
		else
		{
			$this->Tenant_model->update_data($table="property", $condition=array('id'=>$property_allocated), array('landlord'=>$landlord_id));
			$msg="Data   updated successfully"; 
			echo json_encode(array('result'=>"ok",'msg'=>$msg));
		} 	
	}
	
	public function add_landlord()
	{ 
	     $landlord_id=$this->input->post('landlord_id');
		 $email=$this->input->post('landlord_email');
		 $data=array(
			'company_code'=>$this->session->userdata('company_code'),
			'first_name'=>$this->input->post('landlord_fname'),
			'middle_name'=>$this->input->post('landlord_mname'),
			'last_name'=>$this->input->post('landlord_lname'),
			'phone'=>$this->input->post('landlord_phone'),
			'email'=>$this->input->post('landlord_email'),
			'bank_name'=>$this->input->post('bank_name'),
			'bank_acc'=>$this->input->post('bank_acc'),
			'bank_branch'=>$this->input->post('bank_branch'),
			'id_no'=>$this->input->post('landlord_id_no')
			);
		
       if($landlord_id==""){	
       	$c=$this->Tenant_model->get_data($table="landlord", array('id_no'=>$this->input->post('landlord_id_no'),'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1)); 
		if($c->num_rows()>0)
	    { 
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new landlord ",$status=2);
			
	        echo json_encode(array('result'=>"false",'msg'=>'Landlord records already exist!'));
		}
		else{
	   $this->Tenant_model->insert_data($table="landlord", $data);
	   $q=$this->Tenant_model->get_data($table="landlord", $data);
	   if($q->num_rows()>0)
	    { 
				$msg="Landlord details saved successfully";
				 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="added new landlord ",$status=1);
			
				echo json_encode(array('result'=>"ok",'msg'=>$msg));
		 		
		}
		else{
			  $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new landlord ",$status=1);
			
				$msg="Landlord details not saved ";	
                echo json_encode(array('result'=>"false",'msg'=>$msg));				
			}
		}
	   }
		else
		{
			$this->Tenant_model->update_data($table="landlord", $condition=array('id'=>$landlord_id), $data);
		 
			 $msg="Landlord details updated successfully";
			 echo json_encode(array('result'=>"ok",'msg'=>$msg));
		}
		//$this->landlord($msg);
    }

	public function dealocate_landlord($property_id="", $landlord_id="")
	{
		if($property_id==""){  echo json_encode(array('result'=>"false",'data'=>0)); }  
		if($this->Property_model->update_data($table="property", array('id'=>$property_id,'landlord'=>$landlord_id,'audit_number'=>1), array('landlord'=>'')))
		{
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	
	public function removeLandlord($id="")
	{
		if($id==""){  echo json_encode(array('result'=>"false",'data'=>0)); }
		 
		if($this->Property_model->update_data($table="landlord", array('id'=>$id,'audit_number'=>1), array('audit_number'=>2)))
		{
			$this->Property_model->update_data($table="property", array('landlord'=>$id,'audit_number'=>1), array('landlord'=>''));	
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
public function landlord_property($id="")
{
		$enct=$id;
		$data['msg']="";
		$hashed=new Encrypte();
	    $id=$hashed->hashId($enct, $table="landlord",  $condition=array('company_code'=>$this->session->userdata('company_code')),$searchItem="id");		
		if($id==""){ redirect(base_url() ."tenants/landlord"); }
        $q=$this->Tenant_model->get_data($table="landlord", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		if($q->num_rows()<=0)
		 {
			redirect(base_url() ."tenants/landlord");
		 } 
		$data['disabled']=$this->disabled;
		$data['landlords']=$this->Tenant_model->get_data($table="landlord", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		$data['properties']=$this->Tenant_model->get_data($table="property", $condition=array('landlord'=>$id,'company_code'=>$this->session->userdata('company_code'))); 
		$this->load->view('accounts/header');
		$this->load->view('accounts/landlord_property',$data);
		$this->load->view('accounts/footer'); 	
}


public function singleTenant()
	{ 
		$property_allocated=$this->input->post('property'); 
		$unit_id=$this->input->post('unit_id'); 
		$national_id=$this->input->post('id_number');  
		$total_payment=$this->input->post('total_payment');  
		$lease_period=$this->input->post('lease_period');  
		if($lease_period==""){ $lease_period=0;}
		$payCode=$this->getPayCode();
		$data=array(
		'first_name'=>$this->input->post('fname'),
		'middle_name'=>$this->input->post('mname'),
		'last_name'=>$this->input->post('lname'),
		'mobile_number'=>$this->input->post('mobile_no'),
		'tenant_email'=>$this->input->post('email'),
		'id_passport_number'=>$this->input->post('id_number'), 
        'tenant_type'=>'residential',   
		'property_unit_id'=>$unit_id, 
		'house_no'=>$this->input->post('houseno'),
		'floor_no'=>$this->input->post('floor'),
		'parking_allocated'=>$this->input->post('car_spaces'),    
		'lease_period'=>$lease_period,
		'rent_frequency'=>$this->input->post('rent_frequency'),
		'expected_pay_day'=>$this->input->post('pay_day'),
		'date_added'=>$this->input->post('date_registered'),
		'added_by'=>$this->session->userdata('company_code'),		
		'delete_date'=>'',		
		'tenant_payId'=>$payCode		
		);
		
		$files=$this->input->post('no_of_files');   
		//$this->Tenant_model->insert_data($table="tenant", $data);
		$s=$this->Tenant_model->get_data($table="tenant", $data);
		if($s->num_rows()>0){ $this->individual($property_allocated,$msg="Tenant details already exist!",$tenant_id="", $status="0"); return; }
		if($this->Tenant_model->insert_data($table="tenant", $data)){ 
		$q=$this->Tenant_model->get_data($table="tenant", $condition=array('tenant_email'=>$this->input->post('email'),'id_passport_number'=>$this->input->post('id_number')));
			foreach($q->result() as $row){ $id=$row->id;}  
			$this->Tenant_model->insert_data($table="current_account", array('tenant_id'=>$id,'balance'=>0));
			$this->update_payment_info($id,$unit_id,$deposits=$this->input->post('total_deposits'));
			//echo json_encode(array('result'=>"ok",'data'=>$national_id)); 
			if ($files==0){  }else {  $this->do_upload($id,$national_id); }
		  
			//$this->session->set_userdata('tenant_status','Tenant details saved successfully.');
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add single tenant ",$status=1);
				
			$this->individual($property_allocated,$msg="Tenant details saved successfully",$tenant_id=$id, $status="1");
			
		
		}
		else
		{
		$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new tenant ",$status=2);
		//echo json_encode(array('result'=>"false",'data'=>0));
		$this->session->set_userdata('tenant_status','Tenant details Not saved successfully. Try again');
		$this->individual($property_allocated,$msg="Tenant details Not saved successfully. Try again",$tenant_id="", $status="0");
		  
		}   
	}
	
	public function commercialTenant()
	{
		$property_allocated=$this->input->post('property');  
		$unit_id=$this->input->post('unit_id');  
		$business_no=$this->input->post('business_no');
		$lease_period=$this->input->post('lease_period');  
		if($lease_period==""){ $lease_period=0;}
		$payCode=$this->getPayCode();
		$data=array(
		'company_name'=>$this->input->post('company'),
		//'company_code'=>$this->session->userdata('company_id'),
		'business_number'=>$this->input->post('business_no'),
		'tenant_email'=>$this->input->post('email'),
		'company_email'=>$this->input->post('email'),
		'company_mobile'=>$this->input->post('mobile'),
        'tenant_type'=>'commercial', 
		'business_activity'=>$this->input->post('activity'),
		'first_name'=>$this->input->post('contact_person'),
		'mobile_number'=>$this->input->post('phone_no'),  
		'property_unit_id'=>$unit_id,
		'house_no'=>$this->input->post('houseno'),
		'floor_no'=>$this->input->post('floor'),
		'parking_allocated'=>$this->input->post('car_spaces'), 
		'lease_period'=>$lease_period,
		'rent_frequency'=>$this->input->post('rent_frequency'),
		'expected_pay_day'=>$this->input->post('pay_day'),
		'added_by'=>$this->session->userdata('company_code'), 
		'date_added'=>$this->input->post('date_registered'),		
		'id_passport_number'=>'',		
		'delete_date'=>'',
		'tenant_payId'=>$payCode
		);
		$id=$this->input->post('business_no'); 
		$files=0;
		$files=$this->input->post('no_of_files'); 
		
		$s=$this->Tenant_model->get_data($table="tenant", $data);
		if($s->num_rows()>0){ $this->commercial($id,$msg="Tenant details already exist!",$tenant_id="",$status="0"); return; }
		
		//$this->Tenant_model->insert_data($table="tenant", $data);
		if($this->Tenant_model->insert_data($table="tenant", $data)){
           $q=$this->Tenant_model->get_data($table="tenant",$condition="");
			foreach($q->result() as $row){ $id=$row->id; }
			$this->Tenant_model->insert_data($table="current_account", array('tenant_id'=>$id,'balance'=>0));
			$this->update_payment_info($id,$unit_id, $deposits=$this->input->post('total_deposits')); 
			if ($files==0){    } else{ $this->do_upload($id,$business_no); }
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add commercial tenant ",$status=1);
			
			$this->commercial($id,$msg="Tenant details saved successfully.",$tenant_id=$id,$status="1");
		  
		}
		else
		{
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new tenant ",$status=2);
			$this->commercial($id,$msg="Tenant details Not saved successfully. Try again",$tenant_id="",$status="0");	
		}   
	}
	
	public function update_tenant()
	{
		$property_allocated=$this->input->post('property');
		$unit_id=$this->input->post('category');
		//$q=$this->Property_model->get_data($table="property", array('id'=>$property_allocated));
		//foreach($q->result() as $row){ $name=$row->property_name; } 
		$id=$this->input->post('id_no'); 
		$national_id=$this->input->post('national_id'); 
		$userfile=$this->input->post('userfile');
		$data=array(
		'first_name'=>$this->input->post('fname'),
		'middle_name'=>$this->input->post('mname'),
		'last_name'=>$this->input->post('lname'),
		'mobile_number'=>$this->input->post('mobile_no'),
		'tenant_email'=>$this->input->post('email'),
		'id_passport_number'=>$this->input->post('national_id'), 
		'property_unit_id'=>$unit_id,
		//'property_name'=>$name,
		//'unit_category'=>$this->input->post('category'),
		'house_no'=>$this->input->post('houseno'),
		'floor_no'=>$this->input->post('floor'),
		'parking_allocated'=>$this->input->post('car_spaces'), 
		'lease_period'=>$this->input->post('lease_period'),
		'rent_frequency'=>$this->input->post('rent_frequency'),
		'expected_pay_day'=>$this->input->post('pay_day'),
		'date_added'=>$this->input->post('date_registered') 
		);
		$files=0; 
		$files=$this->input->post('no_of_files');  
	 	if($this->Tenant_model->updateTenant(array('id'=>$id),$data))
		{  
			if ($files==0) { }else { $this->do_upload($id,$national_id); }
			//echo json_encode(array('result'=>"ok",'data'=>1));
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="edit  tenant id ".$id,$status=1);
			
			$this->viewTenant($id,$msg="Tenant details successfully updated ");  		
		}
		else
		{ 
			$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to edit tenant id ".$id,$status=2);
			//echo json_encode(array('result'=>"false",'data'=>0));
			$this->viewTenant($id,$msg="Tenant details not updated . Try again later");
		} 
		 
	}
	
	public function update_Commercial_tenant()
	{
		$property_allocated=$this->input->post('property');
		$unit_id=$this->input->post('category'); 	
		$id=$this->input->post('id_no'); 
		$national_id=$this->input->post('national_id');  
		if($national_id==""){$national_id=$this->input->post('mobile_no');}
		$userfile=$this->input->post('userfile');
		$data=array(
		'company_name'=>$this->input->post('company_name'), 
		'company_mobile'=>$this->input->post('mobile_no'),
		'first_name'=>$this->input->post('contact_person'),
		'mobile_number'=>$this->input->post('phone_no'),
		'company_email'=>$this->input->post('email'),
		'business_number'=>$this->input->post('business_no'),
		'business_activity'=>$this->input->post('business_activity'),
		'property_unit_id'=>$unit_id, 
		'house_no'=>$this->input->post('houseno'),
		'floor_no'=>$this->input->post('floor'),
		'parking_allocated'=>$this->input->post('car_spaces'), 
		'lease_period'=>$this->input->post('lease_period'),
		'rent_frequency'=>$this->input->post('rent_frequency'),
		'expected_pay_day'=>$this->input->post('pay_day'),
		'date_added'=>$this->input->post('date_registered') 
		);  
		$files=0; 
		 $files=$this->input->post('no_of_files'); 
		if($this->Tenant_model->updateCommercialTenant(array('id'=>$id),$data))
		{  
			if ($files==0){  }else { $this->do_upload($id,$national_id); }
			  $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="edit tenant id ".$id,$status=1);
			
				// echo json_encode(array('result'=>"ok",'data'=>1));
				 $this->viewTenant($id,$msg="Tenant details successfully updated");  		
		}
		else
		{
				 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed edit tenant id ".$id,$status=2);
			//echo json_encode(array('result'=>"false",'data'=>0));
				 $this->viewTenant($id,$msg="Tenant details not  updated. Try again later");
		} 
		 
	}
	

	public function removeTenant($id="")
	{
		if($id==""){  echo json_encode(array('result'=>"false",'data'=>0)); }
		$this->Tenant_model->removeTenant(array('id'=>$id));
		$q=$this->Tenant_model->get_data('tenant',array('id'=>$id,'audit_number'=>2,'tenant_deleted'=>2));
		if($q->num_rows()>0)
		{
			$tenant_id=$id;
			$this->Property_model->update_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2));	
			$this->Property_model->update_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2)); 
			$this->Property_model->update_data($table="tenant_payment", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2)); 	
			$this->Property_model->update_data($table="current_account", array('tenant_id'=>$tenant_id,'audit_number'=>1), array('audit_number'=>2));	
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="removed tenant id ".$tenant_id,$status=1);
			
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to remove tenant id ".$tenant_id,$status=2);
			
			echo json_encode(array('result'=>"false",'data'=>0));
		}
	}
	
	public function add_supplier()
	{
		$edit_id=$this->input->post('id');
		$data=array(
				'company_name'=>$this->input->post('company_name'),  
				'contact_person_name'=>$this->input->post('contact_person'),  
				'product_description'=>$this->input->post('description'),
				'mobile_no'=>$this->input->post('phone'),
				'company_code'=>$this->session->userdata('company_code'),
				'supplier_added_by'=>$this->session->userdata('id'),
				'email'=>$this->input->post('email'),
				'supplier_delete_date'=>null,
				'supplier_deleted_by'=>null
			  ); 
		 if($edit_id=="")
		{ 
	        $q=$this->Tenant_model->get_data($table="suppliers", array('company_code'=>$this->session->userdata('company_code'),'email'=>$this->input->post('email')));
	         $r=$this->Tenant_model->get_data($table="suppliers", array('company_code'=>$this->session->userdata('company_code'),'company_name'=>$this->input->post('company_name')));
			if($q->num_rows()>0){
				echo json_encode(array('result'=>"false",'data'=>0));
			}
			else if($r->num_rows()>0){
				echo json_encode(array('result'=>"false",'data'=>0));
			}
			else{
					if($this->Tenant_model->insert_data($table="suppliers", $data))
					{
					$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="add new supplier ",$status=1);

					echo json_encode(array('result'=>"ok",'data'=>1));
					}
					else
					{
						$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to add new supplier ",$status=2);
						echo json_encode(array('result'=>"false",'data'=>0));
					}
				}
		}
               else
		{
		 
		    if($this->Tenant_model->update_supplier($data,array('id'=>$edit_id)))
		     {
				 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="update details for supplier id ".$edit_id,$status=1);
				 echo json_encode(array('result'=>"ok",'data'=>1));
		     }
           else
		    {
				$this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to update details for supplier id ".$edit_id,$status=2);
				echo json_encode(array('result'=>"false",'data'=>0));
		    }
        }
		
	}
	
	public function getLandlordDetails($id="")
	{
		$q=$this->Tenant_model->get_data($table="landlord", $condition=array('id'=>$id,'company_code'=>$this->session->userdata('company_code'),'audit_number'=>1));
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
	public function showSupplier($id="")
	{
		$q=$this->Rent_model->getData($condition=array('no'=>$id,'company_id'=>$this->session->userdata('company_id'),'supplier_deleted'=>1), $table="expenses");
		if($q->num_rows()>0)
		{
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
	public function remove_supplier($id="")
	{
		if($id==""){  echo json_encode(array('result'=>"false",'data'=>0)); return; }
		$q=$this->Tenant_model->update_supplier($data=array('supplier_deleted'=>2,'audit_number'=>2,'supplier_delete_date'=>date('Y-m-d')), $condition=array('id'=>$id,'supplier_deleted'=>1,'company_code'=>$this->session->userdata('company_code')));
		$s=$this->Rent_model->getData( $table="suppliers",$condition=array('id'=>$id,'supplier_deleted'=>2,'audit_number'=>2,'supplier_delete_date'=>date('Y-m-d')));
	
		if($s->num_rows()>0)
		{
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="remove supplier id ".$id,$status=1);
			
			echo json_encode(array('result'=>"ok",'data'=>1));
		}
		else
		{
			 $this->Authentication_model->log_activity($user=$this->session->userdata('email'), $ip=$this->user_ip, $message="failed to remove supplier id ".$id,$status=1);
			echo json_encode(array('result'=>"false",'data'=>0));
		}
		
	}
	
public function search_supplier()
	{ 
        $search_id=$_GET['term'];//$this->input->post('search_item'); 
		$q=$this->Tenant_model->lookup_supplier($search_id);
		  
		if($q->num_rows()>0)
		{
			foreach($q->result() as $row){
				$arr_result[]=$row->company_name; 
			}
			echo json_encode($arr_result);
		}
		else{
			echo json_encode(array('result'=>'No results'));
		} 
		
    }
	

public function update_payment_info($tenant_id, $unit_id ,$deposits)
	{
		$total_pay=0;
		$data=$this->Property_model->get_data($table="pricing_details", array('unit_id'=>$unit_id));	
		if($data->num_rows()>0)
		{
			foreach($data->result() as $r)
			{ 
				$q=$this->Property_model->get_data($table="tenant_pricing", array('property_category_id'=>$unit_id,'tenant_id'=>$tenant_id,'payment_type'=>$r->payment_type));
				if($q->num_rows()>0)
				{
				 
				}
				else
				{
					$total_pay=$total_pay+$r->payment_type_value;
					$this->Property_model->insert_data($table="tenant_pricing", array('tenant_id'=>$tenant_id,'property_category_id'=>$unit_id,'payment_type'=>$r->payment_type,'payment_type_value'=>$r->payment_type_value,'priority'=>$r->priority,'is_mandatory'=>$r->is_mandatory));
				}
			}
			$this->Property_model->insert_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'date_paid'=>date("m/d/Y"),'amount'=>$deposits,'description'=>'Deposit','payment_mode'=>'','payment_mode_code'=>'','receipt_no'=>'','type'=>'d'));
			$this->Property_model->insert_data($table="tenant_transaction", array('tenant_id'=>$tenant_id,'date_paid'=>date("m/d/Y"),'amount'=>$total_pay,'payment_mode'=>'','description'=>'Rent', 'payment_mode_code'=>'','receipt_no'=>'','type'=>'d'));
			$obj=new rent_class;
			//$expected_pay_day=$this->rent->get_next_pay($tenant_id);
			$expected_pay_day=$obj->expected_pay_day($tenant_id);
			$this->Rent_model->update_data($table="current_account", array('balance'=>$total_pay+$deposits,'expected_pay_date'=>$expected_pay_day),  array('tenant_id'=>$tenant_id));
		} 
	
	}
	
	
	public function getPayCode()
	{ 
		$paycode=1001;
        $q=$this->Tenant_model->get_data($table="tenant", array('tenant_payId'=>$paycode));
		while($q->num_rows() >0)
		{
			$paycode=$paycode+1;
			$q=$this->Tenant_model->get_data($table="tenant",array('tenant_payId'=>$paycode));
		}
		return $paycode;
	}

	
	public function do_upload($id,$national_id=""){  
		$config['upload_path'] = 'media/'; 
		$config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx';
		$config['max_size']	= '5000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
        $this->upload->initialize($config);
		$targetDir='./media/';   //directory name
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);
	for($i=0; $i<$cpt; $i++)
	{
		$_FILES['userfile']['name']= $files['userfile']['name'][$i];
		$_FILES['userfile']['type']= $files['userfile']['type'][$i];
		$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
		$_FILES['userfile']['error']= $files['userfile']['error'][$i]; 
		$_FILES['userfile']['size']= $files['userfile']['size'][$i];
		//$this->upload->initialize($this->set_upload_options());
		$this->upload->do_upload();
		//get file name
		$fileName = $_FILES['userfile']['name'];
		$images[] = $fileName;
		$targetFile = $targetDir.$national_id."_".$fileName; 
		if(move_uploaded_file($_FILES['userfile']['tmp_name'],$targetFile))
		{
			//check if the file exist 
			$check=$this->Property_model->check_file(array('media'=>$national_id."_".$fileName,'tenant_id'=>$id));
			if($check->num_rows()<=0){   
				// upload files to images directiry and also  to the database
				$data=array('media'=>$national_id."_".$fileName,'tenant_id'=>$id,'date_added'=>date("Y-m-d H:i:s"));
				//pass parameter with data to model
				$this->Tenant_model->insert_documents($data); 
			}
			
		}
		}
			$fileName = implode(',',$images); 
			  
	}
 
	
	
}