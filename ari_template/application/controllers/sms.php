<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require APPPATH . '/libraries/AfricasTalkingGateway.php';
class Sms extends CI_Controller
{
  var $username=null;
  var $apikey=null;
  var $gateway=null;
    public function __construct()
    { 
        parent::__construct();
		$this->load->library('AfricasTalkingGateway');
		$this->load->model('Admin_model');
		$this->load->model('Payment_model');
		$this->load->model('Tenant_model');
		date_default_timezone_set('Africa/Nairobi');
		$this->username   = "daguindd";
		//$this->apikey     = "c33e58aae898f33cc54724a57f81985065f51be0473fa8bfc1bf161e12991dbe";
		$this->apikey     = "bbf2a40b99f48da7d75d94bb044addb5bdcd164a531d0b085d196c2cb590bf29";
		$this->gateway= new AfricasTalkingGateway($this->username, $this->apikey);
		   
   }
    
 
public function send_sms($par="")
    { 
		$recipients=""; $count=0; 
		if($par =="")
		{
			foreach($this->input->post('to') as $value) {  $recipients=$recipients."+254".substr($value,1).","; $count++;}
		}
		else
		{
			$recipients=$recipients."+254".substr($this->input->post('to'),1); 
		}
		  
$message = $this->input->post('body');   
 
$q=$this->Admin_model->getData($table="sms_status", array('company_code'=>$this->session->userdata('company_code')));
$remained=0;
if($q->num_rows()<=0)
{
	echo json_encode(array('result'=>"false",'data'=>0));
}
else{
	foreach($q->result() as $r){ $remained=$r->remaining_sms;}
	
	if($remained>=$count){ 
		try 
		{  
		  // Thats it, hit send and we'll take care of the rest. 
		 $data = $this->gateway->getUserData();
		 $balance=substr($data->balance,4);
		 if($balance>0)
		 {
			 $results = $this->gateway->sendMessage($recipients, $message."  - ".$this->input->post('from'),$from="ARI");
		 }
		  else
		  {
			  echo json_encode(array('result'=>"false",'msg'=>'Message not sent, please contact ARI Homes'));
			  return;
		  }	
		 foreach($results as $result) {
			// status is either "Success" or "error message"
			/*echo " Number: " .$result->number;
			echo " Status: " .$result->status;
			echo " MessageId: " .$result->messageId;*/
			/*echo " Cost: "   .*/
			  $cost=$result->cost; 
		    } 
		  /******get sms balance */
		  //$data = $gateway->getUserData();
		  //echo "Balance:" . $data->balance."\n";
		  /**** end of balance ***/ 
		  $pages=$this->input->post('pages');
		  $sent_sms=$count*$pages; 
		  $this->update_sms($sent_sms);
		  if($this->input->post('id')==""){
		  $this->saveMessage($message,$recipients,$type=$this->input->post('message_type'),$sent_sms);
		  }
		  else
		  {
			$this->Admin_model->updateMessageInfo($id=$this->input->post('draft_id'),$data=array('sent_by'=>$this->session->userdata('email'),'name'=>$this->session->userdata('first_name'),'to'=>$recipients,'message_type'=>$this->input->post('message_type'),'message'=>$message));
			echo  json_encode(array('result'=>"ok",'sms_cost'=>$sent_sms,$receipients,'data'=>1));
		  
		  }	  
		  
		}
		catch ( AfricasTalkingGatewayException $e )
		{
		  //echo "Encountered an error while sending: ".$e->getMessage();
		  echo json_encode(array('result'=>"false",'msg'=>'SMS not sent, Error occurred! Please contact ARI Homes '));
		}
}
else
{
	echo json_encode(array('result'=>"false",'msg'=>'SMS not sent! Ensure you have enough sufficient balance '));
}
	

	}
}
 
public function saveMessage($message,$to,$type,$sent_sms)
{
	$name="";
	foreach($this->input->post('to') as $value)
	{  
		$q=$this->Admin_model->get_data($table="tenant_property_units", array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'mobile_number'=>$value)); 
	    foreach($q->result() as $r){   $name=$name.$r->first_name.' '.$r->last_name."  ";}
	}
	$this->Admin_model->sendMail($id="",$data=array('company_code'=>$this->session->userdata('company_code'),'sent_by'=>$this->session->userdata('email'),'to'=>$to,'name'=>$name,'message_type'=>$type,'message'=>$message));
	if($this->db->affected_rows()>0)
	{
		echo  json_encode(array('result'=>"ok",'sms_cost'=>$sent_sms,'receipients'=>$to,'data'=>1));
	}
	else
	{
		echo json_encode(array('result'=>"false"));
   }
}
	
	public function update_sms($sent_sms, $company_code="")
	{       $company_code=$this->session->userdata('company_code');
			if($company_code==""){ $this->session->userdata('company_code'); }
			//$this->Admin_model->update_sms($condition=array('company_code'=>$company_code),$sent_sms);
			if($this->Admin_model->update_sms($condition=array('company_code'=>$company_code),$sent_sms)){
				//echo  json_encode(array('result'=>"ok",'data'=>1));
			}
			else
		    { 
				//echo json_encode(array('result'=>"false",'data'=>0));
		   }
	}

	
public function schedule($page="schedule_sms")
	{
	    $data['messages']=$this->Payment_model->get_data($condition=array('audit_number'=>1), 'scheduled_sms', $order="id"); 
		$data['properties']=$this->Tenant_model->get_data("property",array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'property_deleted'=>1));
		$data['tenants']=$this->Tenant_model->showTenants(array('company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		$data['users']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'user_added_by'=>0), $table="user_info");
		$data['company_info']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')), $table="company_info");
		$this->load->view('accounts/header');
		//$this->load->view('admin/messages',$data);
		$this->load->view('accounts/'.$page,$data);
		$this->load->view('accounts/footer');
		
	}
	
	public function scheduled_sms()
	{
			$id=$this->input->post('id');
			$data=array(
			'property_id'=>$this->input->post('property'),
			'company_code'=>$this->session->userdata('company_code'),
			'message'=>$this->input->post('message'),
			'title'=>$this->input->post('title'),
			'rent_status'=>$this->input->post('status'),
			'date_sent'=>$this->input->post('date'),
			'time_sent'=>$this->input->post('time'),
			'repeat'=>$this->input->post('repeat'),
			'frequency'=>$this->input->post('frequency'),
			'date_created'=>date('d-m-Y')
			);
			if($id !="")
			{ 
			   $this->Admin_model->update_data("scheduled_sms", array('id'=>$id), $data);
			   echo  json_encode(array('result'=>"ok"));
			}else{
				 if($this->Admin_model->insert_data('scheduled_sms',$data))
				 {
					echo  json_encode(array('result'=>"ok"));
				  }
				else
				{ 
					echo json_encode(array('result'=>"false"));
				}
			}
	}
	
	public function removeMessages($id="")
	{
		 $q=$this->Admin_model->getData('scheduled_sms',$condition=array('id'=>$id)); 
	    foreach($q->result() as $row) 
		{ 
			$status=$row->audit_number;
		}
		if($status==1){ $status=2;} else { $status=1;}
		if($this->Admin_model->update_data("scheduled_sms", array('id'=>$id),array('audit_number'=>$status)))
		{
			echo json_encode(array('result'=>"ok"));
		}
		else
		{
			echo json_encode(array('result'=>"false"));
		} 
	} 
	
	public function displayMessages($id="")
	{
		$q=$this->Admin_model->getData('scheduled_sms',$condition=array('id'=>$id)); 
	    if ($q->num_rows()>0) 
		{  
			echo json_encode(array('result'=>"ok",'data'=>$q->result_array()));
		}
		else
		{
			echo json_encode(array('result'=>"false"));
		} 
	}
	
		public function sendScheduledSms()
	{
	    $company_name=""; $name=""; 
		
        $receipient="";	$sent=0;	 
	//	$today=date('m/d/Y H:i');
	$today=date('m/d/Y');
		$q=$this->Admin_model->getData('scheduled_sms',$condition=array('audit_number'=>1)); 
	    if ($q->num_rows()>0) 
		{  
			foreach($q->result() as $r)
			{
			//	$date_sent=$r->date_sent.' '.$r->time_sent;
			    $date_sent=$r->date_sent;
				$status=$r->status; $rent_status=$r->rent_status; $messageId=$r->id;
				$message=$r->message; $propId=$r->property_id; $repeat=$r->repeat; $rent_frequency=$r->frequency; $repeat_date=$r->repeat_date;
			    $company_code=$r->company_code; 
			  
			$users=$this->Payment_model->get_data(array('company_code'=>$company_code,'user_added_by'=>0), $table="user_info");
			$company_info=$this->Payment_model->get_data(array('company_code'=>$company_code), $table="company_info");
			foreach($users->result() as $r)
			{
				  $name=ucfirst(strtolower($r->first_name))." ".ucfirst(strtolower($r->last_name)); 
			}
			foreach($company_info->result() as $c)
			{
				 $company_name=$c->company_name;
			}
	   
		if($company_name !=""){ $name=$company_name; }
		  	 
			$p=$this->Admin_model->getData('tenant_property_units',$condition=array('company_code'=>$company_code,'property_id'=>$propId));
			foreach($p->result() as $row)
			{
				$tenantId=$row->id; $mobile_number=$row->mobile_number; $bal=0;
				if($rent_status=="unpaid")
				{ 
                   $c=$this->Admin_model->getData('current_account',$condition=array('tenant_id'=>$tenantId)); 
				   foreach($c->result() as $w){ $bal=$w->balance; } 
				   	if($bal >0)
					{
						$sent++;
					    $receipient=$receipient."+254".substr($row->mobile_number,1).",";
					}
				}
				else if($rent_status=="all")
				{
					if($mobile_number !="")
					{
						$sent++;
						$receipient=$receipient."+254".substr($row->mobile_number,1).",";
					} 
				}
				 
			} 		
			  
			//$receipient="+254725440356"; 
			if($status==0 && $date_sent==$today)
			{ 
				if($receipient !="")
				{  
					$results = $this->gateway->sendMessage($receipient, $message."  - ".$name,$from="ARI");
					$this->update_sms($sent,$company_code);
					if($repeat=="yes"){
					$now=date("d"); 
					$day=""; $days_of_year=date("z", mktime(0,0,0,12,31,date('Y'))) + 1;
					$month = date("m");	 $year = date("Y"); $days_of_month=date("t",mktime(0,0,0,$month,1,$year)); 
				//	$expiry_date=date_create(date("m/d/Y H:i"));
				    $expiry_date=date_create(date("m/d/Y"));
					if($rent_frequency=="monthly")
					{
						$date_sent=date_create($date_sent);
						date_add($date_added,date_interval_create_from_date_string($days_of_month-$now+$expiry_date  ."days"));
					//	$day=date_format($date_added,"m/d/Y H:i");
					$day=date_format($date_added,"m/d/Y");
					}
					else if($rent_frequency=="daily"){
						$date=date_create($date_sent);
						date_add($date,date_interval_create_from_date_string(1 ."days"));
						$day=date_format($date,"m/d/Y"); 
					} 
					else if($rent_frequency=="weekly"){ 
						$date=date_create($date_sent);
						date_add($date,date_interval_create_from_date_string(7 ."days"));
						$day=date_format($date,"m/d/Y"); 
					}
							$repeat_date=$day; 
							$this->Admin_model->update_data("scheduled_sms",array('id'=>$messageId),array('status'=>1,'repeat_date'=>$repeat_date,'date_sent'=>date('m/d/Y'),'time_sent'=>date('H:i')));
					}
					 
				} 
			}
			else if($status==1 && $repeat_date==$today)
			{
			    $results = $this->gateway->sendMessage($receipient, $message."  - ".$name,$from="ARI");
			    $this->update_sms($sent,$company_code);			   
			}
			 
		}
		
          echo json_encode(array('result'=>"ok"));
		}
		else{ echo json_encode(array('result'=>"false"));}
	}
	
}