<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {
	public function __construct()
	{
		parent::__construct();   
		$this->load->library('form_validation');
		$this->load->model("Rent_model");
		$this->load->model("Payment_model");		
		$this->load->model("Property_model");		
		$this->load->model("Tenant_model");	
		$this->load->library('email');
		$this->load->library('pdf');
		$this->load->helper('email');
		$this->load->helper('security');
        $this->load->helper('file');
		$this->load->library('Expired');
		$this->load->library('Encrypte');
		$this->load->library('pdf_converter/FPDF');
        date_default_timezone_set('Africa/Nairobi');
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry(); 
	   
	}
	
	
	public function sendRentStatement()
	{ 
//require_once('../libraries/fpdf.php');
$body=$this->input->post('body');
$to=$this->input->post('email');
$from=$this->input->post('from');
$salutation='<html><body> 
		<div style="font-size:12px;">
		 
		<p style="line-height:15px"> <h4 align="left"> Dear  '.ucfirst(strtolower($this->input->post('first_name'))).',</h4>
		 <h5> Attached   is your '.$this->input->post('title').'.</h5>
		 
		 </p> ';
$pdf = new PDF(); 
$pdf->SetFont('','U'); 
$pdf->SetFont('helvetica','',12);
// Second page
$pdf->AddPage();
//$pdf->SetLink($link); 
//$pdf->SetLeftMargin(45);
$pdf->SetFontSize(12);
$html ='<style>table, th, td { 
    padding: 1px;
    text-align: left; font-size:12px;
	border-top: 1px solid #000;  
} ul{list-style-type:none;}</style>'.$body;
//<h5>'.$this->input->post('title').'</h5> '.$body;
// Print text using writeHTMLCell() 

   // border: 1px solid black; border-collapse: collapse; 
$pdf->WriteHTML($html);
$fileName='document_'.date('Y-m-d').'_'.date('H-i-s').'.pdf';
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'media/'.$fileName, 'F'); 
	 	
 $this->sendMail($to,$salutation,$subject=$this->input->post('title'),$from,$fileName);
 
  unlink($_SERVER['DOCUMENT_ROOT'].'media/'.$fileName); 
  	echo json_encode(array('result'=>"ok"));
	}
	
/*	public function sendRentStatement()
	{  
	 
	$to=$this->input->post('email');
	 $from=$this->input->post('from');
	 $body=$this->input->post('body');
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
  
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('');
    $pdf->SetTitle('Rent Statement');
    $pdf->SetSubject(' Rent Statement ');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');   
  
    // set default header data
    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));

    $pdf->setFooterData(array(0,64,0), array(0,64,128)); 
  
    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
  
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
  
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
  
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
  
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
  
    // ---------------------------------------------------------    
  
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);   
  
    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('helvetica', '', 12, '', true);   
  
    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage(); 
  
    // set text shadow effect
    $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
  
    // Set some content to print
   $salutation='<html><body> 
		<div style="font-size:12px;">
		 
		<p style="line-height:15px"> <h4 align="left"> Dear  '.ucfirst(strtolower($this->input->post('first_name'))).',</h4>
		 <h5> Attached   is your '.$this->input->post('title').'.</h5>
		 
		 </p> ';
    $html ='<h5> '.$this->input->post('title').' </h5><hr/>'.$body.'
     </div> 	
		 </body> 
		 </html>';
// Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
  
    // ---------------------------------------------------------    
  
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
   // $attachment=$pdf->Output('example_001.pdf', 'E'); 
   $fileName='document_'.date('Y-m-d').'_'.date('H-i-s').'.pdf';
   $pdf->Output($_SERVER['DOCUMENT_ROOT'].'review/media/'.$fileName, 'F');  
if($to !="" && $fileName !=""){	 	
	$this->sendMail($to,$salutation,$subject=$this->input->post('title'),$from,$fileName);
	unlink($_SERVER['DOCUMENT_ROOT'].'review/media/'.$fileName);
	echo json_encode(array('result'=>"ok"));
}
else
{
	 echo json_encode(array('result'=>"false"));
}
    //============================================================+
    // END OF FILE
    //============================================================+
}	
 
 */
	public function package($page="", $item="")
	{
		$balance=0;
		if(!empty($page)){}else {$page="packages";}
		if(!empty($item)){ $data['selected_item']=$item; } else {$data['selected_item']="";}
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		foreach($package_bal->result() as $b){ $balance=$b->balance; } $data['balance']=$balance;
		$data['billing_cycle']=$this->Payment_model->billing_cycle(array('package_type'=>$item));
		$this->load->view("accounts/header");
		$this->load->view("payments/".$page, $data);
		$this->load->view("accounts/footer");
		
	}
	
  public function transactions($page="", $item="")
	{
		$balance=0;
		$data['partner_details']=$this->Payment_model->get_data(array('id'=>$this->session->userdata('id')),"partners");
		$data['subscriber_info']=$this->Payment_model->get_data($condition="", "user_info");
		$data['subscribers']=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1),"package_payment",$order="id",$limit="");
		$data['packages']=$this->Payment_model->get_data($condition="", "package");
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		foreach($package_bal->result() as $b){ $balance=$b->balance; } $data['balance']=$balance;
		$page="transactions";
		$this->load->view('accounts/header'); 
		$this->load->view('accounts/'.$page, $data); 
		$this->load->view('accounts/footer');
	}
	
	public function buy_sms()
	{
		$balance=0;
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		if($package_bal->num_rows()>0){ foreach($package_bal->result() as $b){ $balance=$b->balance; }  }
		$data=array('package'=>'',
		'users'=>0,
		'years'=>0,
		'total_package_bill'=>0, 
		'total_sms'=>$this->input->post('sms'), 
		//'total_sms_cost'=>$this->input->post('total_cost'), 
		'total_sms_cost'=>$this->input->post('sms')*3, 
		'total_sub_total'=>0, 
		'total_sum_total'=>$this->input->post('total_cost'), 
		'total_vat'=>0,'mode'=>'mpesa');  
		$this->session->set_userdata($data);
		$invoice_no='A'.date("m").date("d").rand(100,1000).date("y"); $receipt_no='A'.date("m").date("d").rand(123,1200).date("y")."R";
		$this->session->set_userdata(array('package_balance'=>$balance,'total_sms_cost'=>$this->input->post('total_cost'),'invoice_no'=>$invoice_no,'receipt_no'=>$receipt_no)); 
		$data['total_sms']=$this->input->post('sms'); $data['total_sms_cost']=$this->input->post('sms')*3; $data['package_balance']=$balance; $data['invoice_no']=$invoice_no; $data['receipt_no']=$receipt_no;
		$this->load->view("accounts/header");
		$this->load->view("payments/buy_sms", $data);
		$this->load->view("accounts/footer"); 
	}
	
	public function order_summary($package)
	{
		$balance=0;
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		if($package_bal->num_rows()>0)
		{
			foreach($package_bal->result() as $b){ $balance=$b->balance; }  
		}
		$data['billing_cycle']=$this->Payment_model->billing_cycle(array('package_type'=>$package));
		$data['sms']=$this->Payment_model->billing_cycle(array('package_type'=>'sms'));
		$data['package']=$package;
		$data['credit']=$balance;
		$this->session->set_userdata(array('package'=>$package));   
		$this->load->view("accounts/header");
		$this->load->view("payments/order_summary",$data);
		$this->load->view("accounts/footer");  
	}
	  
	public function payment_checkout()
	{
		$data=array('package'=>$this->input->post('package'),
		'users'=>$this->input->post('users'),
		'years'=>$this->input->post('years'),
		'total_package_bill'=>$this->input->post('total_bill'), 
		'total_sms'=>$this->input->post('total_sms'), 
		'total_sms_cost'=>$this->input->post('total_sms_cost'), 
		'total_sub_total'=>$this->input->post('total_sub_total'), 
		'total_sum_total'=>$this->input->post('total_sum_total'), 
		'total_vat'=>$this->input->post('total_vat')); 
		$this->session->set_userdata($data); 
		$credit=$this->input->post('credit');
		$sms_cost=$this->input->post('total_sms_cost');
		$vat=$this->input->post('total_vat'); 
		$years=$this->input->post('years');
		$data['users']=$this->input->post('users'); $data['years']=$this->input->post('years'); $data['total_package_bill']=$this->input->post('total_bill');
		$data['total_sms_cost']=$sms_cost; $data['total_sub_total']=$this->input->post('total_sub_total'); $data['total_sum_total']=$this->input->post('total_sum_total'); 
		$data['total_vat']=$vat; 
		$data['credit']=$credit;  
		$total=$sms_cost+$vat+($this->input->post('total_sub_total')); 
		$data['total_amount_to_pay']=$total;
		$this->session->set_userdata(array('total_amount_to_pay'=>$total,'invoice_no'=>'A'.date("m").date("d").rand(100,1000).date("y"),'receipt_no'=>'A'.date("m").date("d").rand(123,1200).date("y")."R"));
		$date=date_create(date("Y-m-d"));
		date_add($date,date_interval_create_from_date_string($years ."years"));
		$expiry_date=date_format($date,"Y-m-d");
		//$this->Payment_model->insert_data($data1=array('package_type'=>$this->input->post('package'),'sms'=>$total_sms,'receipt_no'=>$this->session->userdata('receipt_no'),'invoice_no'=>$this->session->userdata('invoice_no'),'amount'=>$total,'date_paid'=>date("Y-m-d"),'expiry_date'=>$expiry_date,'company_name'=>$this->session->userdata('company_name'),'phone'=>$this->session->userdata('phone'),'email'=>$this->session->userdata('email'),'first_name'=>$this->session->userdata('first_name'),'last_name'=>$this->session->userdata('last_name')),$table="payment");
		$this->load->view("accounts/header");
		$this->load->view("payments/payment_checkout",$data);
		$this->load->view("accounts/footer"); 
	} 
	
	public function preferred_pay()
	{
		$balance=0;
		$data['previous_pay']=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_payment");
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		foreach($package_bal->result() as $b){ $balance=$b->balance; }
		$this->session->set_userdata(array('mode'=>$this->input->post('mode'))); 
		$data['mode']=$this->input->post('mode'); 
		//$data['credit']=$this->input->post('credit'); 
		$data['credit']=$balance; 
		$this->session->set_userdata(array('package_balance'=>$balance)); 
		$this->load->view("accounts/header");
		$this->load->view("payments/chosen_payment",$data);
		$this->load->view("accounts/footer");
		
	}
 
	
	public function proof_payment()
	{
		$msg=""; $next_subscription=""; $current_bal=0;
		$e=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
		if($e->num_rows()<=0){   }else{ $expiry_date=""; foreach($e->result() as $e){ $next_subscription=$e->expected_pay_date;  $current_bal=$e->balance;} }
		$transaction_code=$this->input->post('transaction_code'); 
		$transaction_mode=strtoupper($this->input->post('transaction_mode')); 
		$this->session->set_userdata(array('transaction_code'=>$transaction_code,'credit'=>$this->input->post('credit'))); 
		$token=$this->input->post('token'); 
		$years=$this->input->post('years'); 
		$users=$this->session->userdata('users');
		$package=$this->input->post('package');  
		$total_sms=$this->input->post('total_sms');  
		$total=$this->input->post('total_amount_to_pay'); 
		if($token=="")
		{
			$c=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), "package_payment");
				
			if($c->num_rows()<=0)
			{
				$p=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code'),'id'=>$this->session->userdata('id'),'audit_number'=>1), "user_info");
				foreach($p->result() as $r){ $token=$r->partner_code;}
			}			
		}
		if($token==""){
			
		}
		else
		{
			$t=$this->Payment_model->get_data($condition=array('partner_code'=>$token,'audit_number'=>1), "partners");
			if($t->num_rows()<0){ $msg="Invalid Partner Token  "; $token=""; }
		}
		$this->session->set_userdata(array('package_balance'=>$this->input->post('package_bal')));
	    //$package=$this->session->userdata('package');
		$bal=0; $package_id=0;  $pendingAmount=0;
		$a=$this->Payment_model->get_data($condition=array('package_type'=>$package,'value'=>$years), "package");
		if($a->num_rows()>0){ foreach($a->result() as $k){  	$package_id=$k->id;	}	}
		 
				$amount=$this->proof_transaction($transaction_code);
				//
				if($amount !=""){
				if($amount<$total)
				{  
					$g=$this->Payment_model->get_data(array('transaction_no'=>$transaction_code), $table="pesapi_temp_account");
					if($g->num_rows()<=0)
					{
						$this->Payment_model->insert_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'transaction_no'=>$transaction_code,'date_paid'=>date('Y-m-d'),'amount_paid'=>$amount), $table="pesapi_temp_account");
					}
					$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="package_current_account",array('balance'=>($current_bal-$amount)));
					$this->Payment_model->update_data($condition=array('receipt'=>$transaction_code), $table="pesapi_payment",array('status'=>1));
					$bal=$total-$amount;
					echo json_encode(array('result'=>"insufficient",'data'=>($total-$amount),'msg'=>$msg));
				}
				else
				{  
					$bal=$amount-$total;
					$bal=0-$bal; $sms_package_id=0;
					if($total_sms>0)
					{
						$s=$this->Payment_model->get_data(array('package_type'=>'sms','value'=>$total_sms,'amount'=>$this->session->userdata('total_sms_cost')),$table="package");
						if($s->num_rows()>0) { foreach($s->result() as $v ){ $sms_package_id=$v->id; }}
						$this->Payment_model->insert_data($data=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$sms_package_id,'paid_amount'=>$this->session->userdata('total_sms_cost'),'balance'=>0,'credit'=>$pendingAmount,'payment_method_code'=>$transaction_code,'invoice_no'=>$this->session->userdata('invoice_no'),'token_no'=>$token,'flag'=>0,'years'=>0,'paid_users'=>0,'remaining_users'=>0,'payment_method'=>$transaction_mode), $table="package_payment");
					}
					
					$u=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code'),'paid_users <>'=>0,'audit_number'=>1), "package_payment", "id","5");
					$packages=$this->Payment_model->get_data($condition=array('audit_number'=>1), "package", "id");
				    $x=0; $package_type="";
					if($u->num_rows()>0)
					{
						foreach($u->result() as $q)
						{  
						  $package_identifier=$q->package_id; 
						  foreach($packages->result() as $pac)
							{
								$package_type=$pac->package_type;
								if($package_identifier==$pac->id)
								{
									if($package_type=="sms"){   continue;}else{  $x=1; break;}
								}
							}
						 if($x==1){   break; } 
						}
					}	  
					
					$flag=0; $remaining_users=0; 
					if($x==0){ $remaining_users=$users-1;} 
					if($remaining_users >1){ $flag=1;} 
					//get pending amount from pesapi_temp_account
		            $pendingAmount=$this->checkCredit();
					//
					$this->Payment_model->save_payment($condition=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'package_id'=>$package_id,'payment_method_code'=>$transaction_code),$data=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$package_id,'remaining_users'=>$users,'paid_users'=>$this->session->userdata('users'),'flag'=>$flag,'years'=>$years,'paid_amount'=>($total)-$this->session->userdata('total_sms_cost'),'balance'=>$bal,'credit'=>$pendingAmount,'payment_method_code'=>$transaction_code,'invoice_no'=>$this->session->userdata('invoice_no'),'token_no'=>$token,'payment_method'=>$transaction_mode), $table="package_payment");
					$this->Payment_model->insert_data(array('user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'amount'=>$amount,'expected_pay'=>$total,'credit_debit'=>'c'), $table="package_transaction");
					$this->Payment_model->insert_data(array('user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$package_id,'company_code'=>$this->session->userdata('company_code'),'number_of_users'=>$users,'transaction_code'=>$transaction_code), $table="paying_for_users");
					$z=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')),$table="sms_status");
					if($z->num_rows()<=0)
					{ 
						$this->Payment_model->insert_data(array('company_code'=>$this->session->userdata('company_code'),'remaining_sms'=>$total_sms,'sent_sms'=>0), $table="sms_status");
					}
					else 
					{
							foreach($z->result() as $a){ $remaining_sms=$a->remaining_sms; }
							$remaining_sms=$remaining_sms+$this->session->userdata('total_sms');
							$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="sms_status",array('remaining_sms'=>$remaining_sms));
					}
					$this->Payment_model->update_data($condition=array('receipt'=>$transaction_code), $table="pesapi_payment",array('status'=>1));
					$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="pesapi_temp_account",array('audit_number'=>2));
					$remaining_days=$this->checkExpiryDate();
					$date=date_create(date("Y-m-d"));
					if($remaining_days >0)
					{ 
						$date=date_create($next_subscription);
					}
					date_add($date,date_interval_create_from_date_string($years ."years"));
					$expiry_date=date_format($date,"Y-m-d");
					
					 if($x==0)
					 { 
						//update subscription date
						$this->Payment_model->update_data($condition=array('id'=>$this->session->userdata('id')), $table="user_info",array('subscription_date'=>date('Y-m-d'),'acc_expiry_date'=>$expiry_date));
					 } 
					
				    //$this->Payment_model->update_data($condition=array('id'=>$this->session->userdata('id')), $table="user_info",array('subscription_date'=>date('Y-m-d'),'acc_expiry_date'=>$expiry_date));
					 
					$this->Payment_model->expected_pay_date($table="package_current_account", array('company_code'=>$this->session->userdata('company_code')), array('balance'=>$bal,'company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'expected_pay_date'=>$expiry_date));
					$this->TransactionNotification($sms=$this->session->userdata('total_sms'),$users=$this->session->userdata('users'),$package, $transaction_code, $transaction_mode,$amount);
					echo json_encode(array('result'=>"ok",'data'=>1,'msg'=>$msg));
				} 
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0,'msg'=>$msg));
		}  
		
}
	
	
	public function over_pay_transaction()
	{  
		$bal=0; $package_id=0; $transaction_mode=$this->input->post('mode'); $x=0;
		$package=$this->session->userdata('package');
		$mpesa_code=$this->session->userdata('invoice_no'); 
		$total_sms=$this->session->userdata('total_sms');
		$a=$this->Payment_model->get_data($condition=array('package_type'=>$package,'value'=>$this->session->userdata('years')), "package");
		if($a->num_rows()>0){ foreach($a->result() as $k){ $package_id=$k->id;	} }		
		$total=$this->session->userdata('total_amount_to_pay');
		$over_payment=$this->input->post('overpay');
		//$transaction_mode=$this->input->post('transaction_mode');
		$amount=$total; 
		$users=$this->session->userdata('users');
		$years=$this->session->userdata('years'); 
		$this->session->set_userdata(array('paid_amount'=>$amount,'transaction_code'=>$mpesa_code));  
		if($over_payment<=0){
		$bal=$over_payment;
		$sms_package_id=0;
		$u=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), "package_payment");
		$packages=$this->Payment_model->get_data($condition=array('audit_number'=>1), "package");
				    $x=0;
					 
					if($u->num_rows()>0)
					{
						
						foreach($u->result() as $q)
						{  
							$package_identifier=$q->package_id; $package_type=""; 
							foreach($packages->result() as $pac)
							{
									if($package_identifier==$pac->id)
									{   
										$package_type=$pac->package_type;   
									} 
									
									if($x==1){  	break;	}  
									if($package_type == "sms"){    continue;  }else{  $x++; break;}
									
							}
							
						}
					} 
		$flag=0;
		if($x==0){ $users=$users-1;}
		if($users >1){ $flag=1;}
		 
		if($total_sms>0)
		{
			$s=$this->Payment_model->get_data(array('package_type'=>'sms','value'=>$total_sms,'amount'=>$this->session->userdata('total_sms_cost')),$table="package");
			if($s->num_rows()>0) { foreach($s->result() as $v ){ $sms_package_id=$v->id; }}
			$this->Payment_model->insert_data($data=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$sms_package_id,'paid_amount'=>$this->session->userdata('total_sms_cost'),'balance'=>0,'payment_method_code'=>$mpesa_code,'invoice_no'=>$this->session->userdata('invoice_no'),'flag'=>0,'paid_users'=>0,'remaining_users'=>0,'token_no'=>'','payment_method'=>'mpesa'), $table="package_payment");
		}
				
			$this->Payment_model->save_payment($condition=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'package_id'=>$package_id,'payment_method_code'=>$mpesa_code),$data=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$package_id,'remaining_users'=>$users,'paid_users'=>$this->session->userdata('users'),'flag'=>$flag,'years'=>$years,'paid_amount'=>$amount,'balance'=>$bal,'payment_method_code'=>$mpesa_code,'invoice_no'=>$this->session->userdata('invoice_no'),'payment_method'=>'--','token_no'=>''), $table="package_payment");
			
			$this->Payment_model->insert_data(array('user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'amount'=>$amount,'expected_pay'=>$total,'credit_debit'=>'c'), $table="package_transaction");
			$this->Payment_model->insert_data(array('user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$package_id,'company_code'=>$this->session->userdata('company_code'),'number_of_users'=>$users,'transaction_code'=>$mpesa_code), $table="paying_for_users");
			$z=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')),$table="sms_status");
			if($z->num_rows()<=0)
			{
				$this->Payment_model->insert_data(array('company_code'=>$this->session->userdata('company_code'),'remaining_sms'=>$total_sms,'sent_sms'=>0), $table="sms_status");
			}
			else 
			{
				foreach($z->result() as $a){ $remaining_sms=$a->remaining_sms; }
				$remaining_sms=$remaining_sms+$this->session->userdata('total_sms');
				$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="sms_status",array('remaining_sms'=>$remaining_sms));
			}
			$this->Payment_model->update_data($condition=array('receipt'=>$mpesa_code), $table="pesapi_payment",array('status'=>1));
			$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="pesapi_temp_account",array('audit_number'=>2));
			$date=date_create(date("Y-m-d"));
			date_add($date,date_interval_create_from_date_string($years ."years"));
			$expiry_date=date_format($date,"Y-m-d");
			 
			$this->Payment_model->expected_pay_date($table="package_current_account", array('company_code'=>$this->session->userdata('company_code')), array('balance'=>$bal,'company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'expected_pay_date'=>$expiry_date));
			$this->TransactionNotification($sms=$this->session->userdata('total_sms'),$users,$package, $mpesa_code, $transaction_mode,$amount);
			echo json_encode(array('result'=>"ok",'data'=>1)); 
		}
	 else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}  
		
	}
	
	public function add_new_amenity()
	{
		$name=$this->input->post('amenity_name');
		if($this->Payment_model->insert_data($data=array('expense'=>$name,'company_code'=>$this->session->userdata('company_code')), $table="expense_type"))
		{
			$q=$this->Payment_model->get_data(array('expense'=>$name,'company_code'=>$this->session->userdata('company_code')), $table="expense_type");
			foreach($q->result() as $row){ $id=$row->id;}
			echo json_encode(array('result'=>"ok",'id'=>$id, 'data'=>1));
		}
		else
		{
			echo json_encode(array('result'=>"false",'data'=>0));
		}

	} 
	
	public function sms($type="")
	{
		if($type==""){$type="sent";}
		$data['count_messages']=$this->Payment_model->get_data($condition=array('sent_by'=>$this->session->userdata('email'),'company_code'=>$this->session->userdata('company_code'),'read_status'=>1,'delete_status <>'=>3), 'messages'); 
		$data['messages']=$this->Payment_model->get_data($condition=array('message_type'=>$type,'delete_status <>'=>3), 'messages',$order="id"); 
		$data['tenants']=$this->Tenant_model->showTenants(array('company_code'=>$this->session->userdata('company_code'),'tenant_status'=>1));
		$data['users']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'user_added_by'=>0), $table="user_info");
		$data['company_info']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')), $table="company_info");
		$data['sms_status']=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="sms_status");
		$this->load->view('accounts/header');
		//$this->load->view('admin/messages',$data);
		$this->load->view('accounts/bulk_sms',$data);
		$this->load->view('accounts/footer');
		
	}
	
	public function subscription($id="")
	{
		$q=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'audit_number'=>1), $table="package_payment", $order_by="id");
		if($q->num_rows()<=0)
		{
			$data['subscription']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_payment", $order_by="id");
		}else
		{
			$data['subscription']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'audit_number'=>1), $table="package_payment", $order_by="id");
		} 
		$data['total_agents']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1),$table="account");
		$data['paid_users']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="paying_for_users");
		$data['sms_status']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="sms_status");
	    //$data['expiring_date']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
		$data['expiring_date']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'id'=>$this->session->userdata('id')), $table="user_info");
		$data['package_current_account']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
		$data['packages']=$this->Payment_model->get_data($condition="", "package");
		$this->load->view("accounts/header");
		$this->load->view("payments/subscription",$data);
		$this->load->view("accounts/footer");
	}

	public function add_sms()
	{
			$bal=0;$balance=0;
			$total_sms=$this->input->post('sms');
			$sms_cost=$this->input->post('total_cost');
			$total_amount_to_pay=$this->input->post('total_amount_to_pay'); 
			$mpesa_code=$this->input->post('mpesa_code');
			$invoice=$this->input->post('invoice_no');
			$company_code=$this->session->userdata('company_code');

			$this->session->set_userdata(array('transaction_code'=>$mpesa_code,'credit'=>$this->input->post('credit'))); 
				 
			 $c=$this->Payment_model->get_data(array('package_type'=>'sms','value'=>$total_sms,'audit_number'=>1),$table="package");
			 if($c->num_rows()>0) {  foreach($c->result() as $r){ 	$package_id=$r->id;		}  }
			 //get transaction code amount
			 $amount=$this->proof_transaction($mpesa_code);
			//
			if($amount !=""){
			if($amount<$total_amount_to_pay)
			{
				$g=$this->Payment_model->get_data(array('transaction_no'=>$mpesa_code,'audit_number'=>1), $table="pesapi_temp_account");
				if($g->num_rows()<=0)
				{
					$this->Payment_model->insert_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'transaction_no'=>$mpesa_code,'date_paid'=>date('Y-m-d'),'amount_paid'=>$amount), $table="pesapi_temp_account");
				}
				else{
					
				}
				$this->Payment_model->update_data($condition=array('receipt'=>$mpesa_code), $table="pesapi_payment",array('status'=>1));
				echo json_encode(array('result'=>"false",'msg'=>'You have less balance to buy  <b>'.$total_sms.'</b> SMS  Pay  <b>Ksh '.($y=$total_amount_to_pay-$amount).' to buy SMS  </b>','data'=>1));
			}
			else
			{ 
				$remaining_sms=0; $new_total=0; 
				//$bal=0-($amount-($total_amount_to_pay+$balance));
				$bal=0-($amount-$total_amount_to_pay);
				$my_sms=$this->Payment_model->get_data(array("company_code"=>$company_code), $table="sms_status");
				if($my_sms->num_rows()>0)
				{ 
					foreach($my_sms->result() as $s){ $remaining_sms=$s->remaining_sms;} 
				}
				else{
						$this->Payment_model->insert_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'remaining_sms'=>0,'sent_sms'=>0,'package_payment_id'=>$package_id), $table="sms_status");
					}
				 $new_total=$remaining_sms+$total_sms;
				if($this->Payment_model->add_sms($table="sms_status", $condition=array("company_code"=>$company_code),$data=array('remaining_sms'=>$new_total)))
				{      
					$this->Payment_model->save_payment($condition=array('company_code'=>$this->session->userdata('company_code'),'payment_method_code'=>$mpesa_code),$data=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$package_id,'paid_amount'=>$amount,'balance'=>$amount-$total_amount_to_pay,'payment_method_code'=>$mpesa_code,'invoice_no'=>$invoice,'payment_method'=>'mpesa'), $table="package_payment");
					$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="pesapi_temp_account",array('audit_number'=>2));
					$this->Payment_model->update_data($condition=array('receipt'=>$mpesa_code), $table="pesapi_payment",array('status'=>1));
					$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')),$table="package_current_account", array('balance'=>$bal,'company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id')));
					echo json_encode(array('result'=>"ok",'data'=>1));
				}
				 
			}
	}
	else
	{
		echo json_encode(array('result'=>"false",'msg'=>'Ensure that you have provided a correct transaction code'));
	}
} 
	
public function add_sms_overpay()
{
		$bal=0;
		$total_sms=$this->input->post('sms');
		$over_payment=$this->input->post('over_payment');
		$cost=$this->input->post('total_cost');
		$invoice=$this->input->post('invoice_no');
		$company_code=$this->session->userdata('company_code');
		$this->session->set_userdata(array('invoice_no'=>'A'.date("m").date("d").rand(100,1000).date("y")));
		$mpesa_code=$this->session->userdata('invoice_no');
		$this->session->set_userdata(array('transaction_code'=>$mpesa_code,'credit'=>$this->input->post('overpay'),'paid_amount'=>$cost,'company_code'=>$this->session->userdata('company_code'))); 
		$c=$this->Payment_model->get_data(array('package_type'=>'sms','value'=>$total_sms,'audit_number'=>1),$table="package");
		if($c->num_rows()>0) { foreach($c->result() as $r){ $package_id=$r->id;		} }
		if($over_payment<=0)
		{ 
				//$bal=$cost-(0-$this->session->userdata('package_balance'));
				$bal=$cost-(0-$over_payment); 
				$remaining_sms=0; $new_total=0; 
				$my_sms=$this->Payment_model->get_data(array("company_code"=>$company_code), $table="sms_status");
				if($my_sms->num_rows()>0)
				{ 
					foreach($my_sms->result() as $s){ $remaining_sms=$s->remaining_sms;} 
				}
				else{
						$this->Payment_model->insert_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'remaining_sms'=>0,'sent_sms'=>0,'package_payment_id'=>$package_id), $table="sms_status");
					}
				 $new_total=$remaining_sms+$total_sms;
				if($this->Payment_model->add_sms($table="sms_status", $condition=array("company_code"=>$company_code),$data=array('remaining_sms'=>$new_total)))
				{
					$this->Payment_model->save_payment($condition=array('company_code'=>$this->session->userdata('company_code'),'payment_method_code'=>$mpesa_code),$data=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$package_id,'paid_amount'=>$cost,'balance'=>$bal,'payment_method_code'=>$mpesa_code,'invoice_no'=>$invoice,'payment_method'=>'mpesa'), $table="package_payment");
					$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="pesapi_temp_account",array('audit_number'=>2));
					$this->Payment_model->update_data($condition=array('receipt'=>$mpesa_code), $table="pesapi_payment",array('status'=>1));
					$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')),$table="package_current_account", array('balance'=>$bal,'company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id')));
					echo json_encode(array('result'=>"ok",'data'=>1));
				} 
			else
			{
				echo json_encode(array('result'=>"false",'msg'=>'Transaction failed! Please try again later','data'=>0));
			}
		}
		else
		{
			echo json_encode(array('result'=>"false",'msg'=>'Transaction failed! Try again later','data'=>0));
		}
}
 
 public function TransactionNotification($sms, $users, $package, $refno, $mode="MPESA",$amount)
	{ 
			$first_name=ucfirst(strtolower($this->session->userdata('first_name')));
			$email= $this->session->userdata('email'); 
		 
			$to=$email;
			$from="ARI Homes";
			$subject="Package Subscription";
			$body='<html> 
			<div style="width:90%;padding:10px; background:ffffff;">
			<div style="padding-left:10px">
			 <center> <img  src="'.base_url().'images/login/ARI-Logo.png" height="60"></center>
			<p style="line-height:5px"> 
			<p align="left"> Dear '.ucfirst(strtolower($first_name)).',</p>
			  <p> Thank you for choosing ARI Homes as your online property management solution, your payment of KES '.$amount.' has been received, below are the transaction details and attached is your receipt.</p>
			  <p> Amount: KES. '.$amount.'</p>
			  <p> Payment Mode: '.$mode.'  </p>
			  <p> Transaction Code: '.$refno.'  </p>
			  <p> Date: '.date('d-m-Y').'  </p>
			  <p> Package: '.ucfirst(strtolower($package)).'  </p>
			  <p> Users: '.$users.'  </p>
			  <p> SMS: '.$sms. '</p> 
			  '.$this->getFooter().'
			</p>
			</div>
			</div>
			</center>
			</body>'; 	
			$this->sendMail($to,$body,$subject,$from);  	 
}
	 
 public function invoice($receipt_no="")
 {
	    $hashed=new Encrypte();
		$receipt_no=$hashed->hashId($receipt_no, $table="package_payment",  $condition=array('audit_number'=>1),$searchItem="id");	
		if($receipt_no==""){ redirect(base_url() ."property/"); }
		$audit_number="";$mode=""; $package_id=""; $credited=0; $invoice_no=""; $transaction_date=date('d-m-Y');  $balance=0; 
		//$q=$this->Payment_model->get_data($condition=array('invoice_no'=>$invoice_no,'paid_amount'=>$amount),"package_payment");
		$q=$this->Payment_model->get_data($condition=array('id'=>$receipt_no),"package_payment");
		foreach($q->result() as $r){ $audit_number=$r->audit_number; $invoice_no=$r->invoice_no; /*$credited=$r->credit;*/ $balance=$r->balance; $amount=$r->paid_amount; $mode=$r->payment_method; $package_id=$r->package_id; $transaction_date=$r->date_paid; $transaction_code=$r->payment_method_code;  }
		$data['packages']=$this->Payment_model->get_data($condition="", "package"); 
		$data['paid_packages']=$this->Payment_model->get_data($condition=array('audit_number'=>1,'payment_method_code'=>$transaction_code,'paid_users >'=>0),"package_payment");
		$data['paid_sms']=$this->Payment_model->get_data($condition=array('audit_number'=>1,'payment_method_code'=>$transaction_code,'paid_users'=>0),"package_payment");
		//$data['paid_users']=$this->Payment_model->get_data($condition=array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'transaction_code'=>$transaction_code), "paying_for_users");
		$data['audit_no']=$audit_number; $data['credited']=$credited; $data['invoice_no']=$invoice_no; $data['receipt_no']=$receipt_no; $data['amount']=$amount; $data['pay_mode']=$mode; $data['transaction_code']=$transaction_code; $data['package_id']=$package_id;  $data['transaction_date']=$transaction_date;  
		$data['balance']=$balance; 
		$this->load->view('accounts/header'); 
		$this->load->view('accounts/invoice', $data); 
		$this->load->view('accounts/footer'); 
 }
 
 public function checkExpiryDate($id="")
 {
		if($id==""){ $id=$this->session->userdata('id');}
	    $subscription=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_payment", $order_by="id");
		$p=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
		$expiring_date=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'id'=>$id), $table="user_info");
		$days=0;		
		
		if($p->num_rows()<=0)
		{
			 
		}
		else{
			 
			$expiry_date=""; 
			foreach($expiring_date->result() as $e){ $expiry_date=$e->acc_expiry_date;  } 
			
			if($expiry_date=="0000-00-00"){
				
			}
			else{
				  
				$now=time(); 
				$diff_time=strtotime($expiry_date)-$now;
				$days=floor($diff_time/(60*60*24)); 
			 
				}	 
		}		
	 return $days;
}
 
 public function getBillingCycle($package=""){
 
	$q=$this->Payment_model->billing_cycle(array('package_type'=>$package));
	if($q->num_rows()>0)
		{
		 
			echo json_encode(array('result'=>'ok','data'=>$q->result_array()));
		}
		else
		{
		  echo json_encode(array('result'=>'fail','data'=>0));
		}
}

public function user_renewal()
{
		$balance=0;  $x="";
		//get list of selected users and append to $x variable 
		foreach($this->input->post('id') as $key=>$value)
				{ 
				 $x=$x."<input type='hidden' name='id[]' checked value='".$value."'/>";
				} 
		$data['previous_pay']=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_payment");
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		foreach($package_bal->result() as $b){ $balance=$b->balance; } 
		$data['mode']=$this->input->post('mode'); 
		$package=$this->input->post('package_name'); 
		$data['years']=$this->input->post('years');
		$data['users']=$this->input->post('users');
		$data['package_cost']=$this->input->post('package_cost');
		$data['vat']=$this->input->post('vat');
		$data['credit']=$this->input->post('balance');
		$data['total_cost']=$this->input->post('total_cost');
		$data['balance']=$balance; 
		$data['package']=$package;  
		$data['success']=0;
		$data['selected_users']=$x;  //send html with users appended   to renewal_method.php page
		$data['invoice_no']='A'.date("m").date("d").rand(100,1000).date("y"); 
		$this->session->set_userdata(array('package_balance'=>$balance)); 
		$this->load->view("accounts/header");
		$this->load->view("payments/renewal_method",$data);
		$this->load->view("accounts/footer");
	
}


public function renewal()
	{ 
		$total=$this->input->post('amount');
		
		$x=0; $package_bal=0;
          if($total<=0)
		  {
			$this->overpay_renewal();  
		  }
		  else{
			$e=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
			if($e->num_rows()<=0){   }
			else{ $expiry_date=""; foreach($e->result() as $e){ $package_bal=$e->balance; $next_subscription=$e->expected_pay_date;  } }

			$this->session->set_userdata(array('package_balance'=>$package_bal)); 
			$transaction_code=$this->input->post('transaction_code'); 
			$transaction_mode=strtoupper($this->input->post('transaction_mode')); 
			$package=$this->input->post('package_name'); 
			$years=$this->input->post('years');     
			$bal=0; $package_id=0; 
			$a=$this->Payment_model->get_data($condition=array('package_type'=>$package,'value'=>$years), "package");
			if($a->num_rows()>0){ foreach($a->result() as $k){  $package_id=$k->id;	}	}
			// get transaction code amount
			$amount=$this->proof_transaction($transaction_code); 
			// end transaction code amount
			if($amount==""){ $data['msg']="Renewal not success Try again later"; }
				else if($amount<$total)
				{
					$g=$this->Payment_model->get_data(array('transaction_no'=>$transaction_code), $table="pesapi_temp_account");
					if($g->num_rows()<=0)
					{
						$this->Payment_model->insert_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'transaction_no'=>$transaction_code,'date_paid'=>date('Y-m-d'),'amount_paid'=>$amount-$temp_amount), $table="pesapi_temp_account");
					}
					$this->Payment_model->update_data($condition=array('receipt'=>$transaction_code), $table="pesapi_payment",array('status'=>1));
					 $bal=$total-$amount;
					 $data['msg']="Insufficient amount!  Pay KES ".($total-$amount)." to complete   transaction";
					// echo json_encode(array('result'=>"insuficient",'data'=>($total-$amount)));
				}
				else
				{ 	$bal=$amount-$total;
					$bal=0-$bal; $sms_package_id=0; 
					$s=$this->Payment_model->get_data(array('package_type'=>'sms','value'=>$this->session->userdata('total_sms'),'amount'=>$this->session->userdata('total_sms_cost')),$table="package");
					if($s->num_rows()>0) { foreach($s->result() as $v ){ $sms_package_id=$v->id; }}
					//$this->Payment_model->update_data(array('invoice_no'=>$this->session->userdata('invoice_no'),'email'=>$this->session->userdata('email')),$table="payment",$data=array('balance'=>$bal,'token'=>$token,'mpesa_code'=>$transaction_code,'pay_status'=>1));
					$this->Payment_model->save_payment($condition=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'payment_method_code'=>$transaction_code),$data=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$package_id,'paid_amount'=>$amount,'balance'=>$bal,'payment_method_code'=>$transaction_code,'invoice_no'=>$this->session->userdata('invoice_no'),'token_no'=>'','payment_method'=>'mpesa'), $table="package_payment");
					 
					$this->Payment_model->update_data($condition=array('receipt'=>$transaction_code), $table="pesapi_payment",array('status'=>1));
					$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="pesapi_temp_account",array('audit_number'=>2));
					
					$users=$this->input->post('users');
					//start renew users
					foreach($this->input->post('id') as $key=>$value)
					{
						$x++;  
						$remaining_days=$this->checkExpiryDate($value);
						$date=date_create(date("Y-m-d"));
						if($remaining_days >0)
						{ 
					
							$p=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'id'=>$value), $table="user_info");
							if($p->num_rows()>0)
							{
								foreach($p->result() as $e){ $date_to_expire=$e->acc_expiry_date;  } 
							}
							 
							$date=date_create($date_to_expire);
						}
						date_add($date,date_interval_create_from_date_string($years ."years"));
						$expiry_date=date_format($date,"Y-m-d");
						
						 $this->Payment_model->update_data($condition=array('id'=>$value), $table="user_info",array('subscription_date'=>date('Y-m-d'),'acc_expiry_date'=>$expiry_date));
						
						 $this->Payment_model->expected_pay_date($table="package_current_account", array('company_code'=>$this->session->userdata('company_code')), array('balance'=>$bal,'company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'expected_pay_date'=>$expiry_date));
					}
					//end of user renewal
					$this->TransactionNotification($sms=0,$users=$x, $package, $transaction_code, $transaction_mode, $amount);
					//echo json_encode(array('result'=>"ok",'data'=>1,'msg'=>$msg));
					$data['msg']="Thank you! Renewal done successful";
				}
		 
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		if($package_bal->num_rows()>0){ foreach($package_bal->result() as $b){ $balance=$b->balance; }  }
		$data['balance']=$balance;  
		$data['total_users']=$x;
		$data['users']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')),$table="account");
		$data['disabled']=$this->disabled;
		$data['success']=1;
		$this->load->view('accounts/header');
		$this->load->view('payments/renewSubscription', $data);
		$this->load->view('accounts/footer');  
	}	
}	
	 

public function overpay_renewal()
	{ 
		$over_payment=$this->input->post('amount'); 
		$amount=$this->input->post('amount_to_pay'); 
		$transaction_code='A'.date("m").date("d").rand(100,1000).date("y");
		$transaction_mode=strtoupper($this->input->post('transaction_mode')); 
		$package=$this->input->post('package_name'); 
		$years=$this->input->post('years');     
		$bal=0; $package_id=0; 		
		if($over_payment>0)
		{
			$data['msg']="Subscription renewal not success Try again later";
		}
		else if($over_payment<=0){
			$e=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
			$a=$this->Payment_model->get_data($condition=array('package_type'=>$package,'value'=>$years), "package");
			if($a->num_rows()>0){ foreach($a->result() as $k){  $package_id=$k->id;	}	}
			 
				  	$bal=$over_payment;
					$bal=0-$bal; $sms_package_id=0;
					$s=$this->Payment_model->get_data(array('package_type'=>'sms','value'=>$this->session->userdata('total_sms'),'amount'=>$this->session->userdata('total_sms_cost')),$table="package");
					if($s->num_rows()>0) { foreach($s->result() as $v ){ $sms_package_id=$v->id; }}
					 $this->Payment_model->save_payment($condition=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'payment_method_code'=>$transaction_code),$data=array('company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'date_paid'=>date('Y-m-d'),'package_id'=>$package_id,'paid_amount'=>$amount,'balance'=>$bal,'payment_method_code'=>$transaction_code,'invoice_no'=>$this->session->userdata('invoice_no'),'token_no'=>'','payment_method'=>'mpesa'), $table="package_payment");
					 
					$this->Payment_model->update_data($condition=array('receipt'=>$transaction_code), $table="pesapi_payment",array('status'=>1));
					$this->Payment_model->update_data($condition=array('company_code'=>$this->session->userdata('company_code')), $table="pesapi_temp_account",array('audit_number'=>2));
					
					$users=$this->input->post('users'); 
					//start renew users 
					$x=0;
					foreach($this->input->post('id') as $key=>$value)
					{
						$x++;  
						$remaining_days=$this->checkExpiryDate($value);
						$date=date_create(date("Y-m-d"));
						if($remaining_days >0)
						{ 
					
							$p=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'id'=>$value), $table="user_info");
							if($p->num_rows()>0)
							{
								foreach($p->result() as $e){ $date_to_expire=$e->acc_expiry_date;  } 
							}
							 
							$date=date_create($date_to_expire);
						}
						date_add($date,date_interval_create_from_date_string($years ."years"));
						$expiry_date=date_format($date,"Y-m-d");
						
						 $this->Payment_model->update_data($condition=array('id'=>$value), $table="user_info",array('subscription_date'=>date('Y-m-d'),'acc_expiry_date'=>$expiry_date));
						
						 $this->Payment_model->expected_pay_date($table="package_current_account", array('company_code'=>$this->session->userdata('company_code')), array('balance'=>$bal,'company_code'=>$this->session->userdata('company_code'),'user_info_id'=>$this->session->userdata('id'),'expected_pay_date'=>$expiry_date));
					}
					//end of user renewal
					$this->TransactionNotification($sms=0,$users, $package, $transaction_code, $transaction_mode,$amount);
					//echo json_encode(array('result'=>"ok",'data'=>1,'msg'=>$msg));
					$data['msg']="Thank you! Subscription renewal was successful";
		}
		$package_bal=$this->Payment_model->get_data($condition=array('company_code'=>$this->session->userdata('company_code')),"package_current_account");
		if($package_bal->num_rows()>0){ foreach($package_bal->result() as $b){ $balance=$b->balance; }  }
		$data['balance']=$balance;  
		$data['total_users']=$x;
		$data['success']=1;
		$data['users']=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code')),$table="user_info");
		$data['disabled']=$this->disabled;
		$this->load->view('accounts/header');
		$this->load->view('payments/renewSubscription', $data);
		$this->load->view('accounts/footer');  
	}	
	 
	public function proof_transaction($transaction_code="")
	{
		$x=0; $amount="";$bal=0; $temp_amount=0;
		$data['msg']="";
		 
		$q=$this->Payment_model->get_data(array('receipt'=>$transaction_code,'status'=>0), $table="pesapi_payment");
		if($q->num_rows()>0)
		{ 
			foreach($q->result() as $row)
			{
				$temp_amount=0;
				$amount=$row->amount;
				$amount=$amount/100;  
				$total=$this->input->post('amount');
				$this->session->set_userdata(array('paid_amount'=>$amount));
				
				$t=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1),$table="pesapi_temp_account");
				if($t->num_rows()>0)
				{
					foreach($t->result() as $row)
					{
						//$temp_amount=$temp_amount+$row->amount_paid;
						$temp_amount=$temp_amount+$row->amount_paid;
					}
				}
				 
				 $amount=$amount+$temp_amount;
				 
			}
		}
		else
		{
			$amount="";
		}
		return $amount;	
	}
	
	 public function checkCredit()
	 {
		$amount=0; $temp_amount=0;
		$t=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1),$table="pesapi_temp_account");
		if($t->num_rows()>0)
		{
			foreach($t->result() as $row)
			{
				$temp_amount=$temp_amount+$row->amount_paid;
			}
		}
		$amount=$amount+$temp_amount;
			  
		 return $amount; 
	}
	
 public function checkCode()
 {
	  	$transaction_code=$this->input->post('transaction_code'); 
	  	$total=$this->input->post('amount'); 
		$this->session->set_userdata(array('transaction_code'=>$transaction_code)); 
		$temp_amount=0; $bal=0;
		// get transaction code amount
		$amount=$this->proof_transaction($transaction_code);
		//
		if($amount=="")
		{
			echo json_encode(array('result'=>"false",'data'=>0));
			
		}
		else
		{
			if($amount<$total)
				{
					$g=$this->Payment_model->get_data(array('transaction_no'=>$transaction_code), $table="pesapi_temp_account");
					if($g->num_rows()<=0){
						$this->Payment_model->insert_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'transaction_no'=>$transaction_code,'date_paid'=>date('Y-m-d'),'amount_paid'=>$amount-$temp_amount), $table="pesapi_temp_account");
					}
					$this->Payment_model->update_data($condition=array('receipt'=>$transaction_code), $table="pesapi_payment",array('status'=>1));
					$bal=$total-$amount;
					 echo json_encode(array('result'=>"insufficient",'data'=>$bal));
				}
				else
				{
					echo json_encode(array('result'=>"ok",'data'=>0));
				}
			
		}
	 
 }
  
 
 public function request_quote()
 {
	 $name=ucfirst(strtolower($this->session->userdata('first_name'))).' '.ucfirst(strtolower($this->session->userdata('middle_name'))).' '.ucfirst(strtolower($this->session->userdata('last_name')));
	 $email=$this->session->userdata('email');
	 $body='<html><body>
		<div style="width:90%;padding:10px;background:ffffff;">
		<div style="padding-left:10px">
		 
		<p style="line-height:15px">  <font> Our customer has requested for an Enterprise quote with the details below</font>
		 <hr/>
		 <table border="1" width="100%" cellpadding="0" cellspacing="0"> 
		 <tr><td> Name </td> <td>'.$name.'</td> </tr>
         <tr><td> Email </td> <td>'.$this->session->userdata('email').' </td></tr>		
         <tr><td> Phone </td> <td>'.$this->session->userdata('phone').' </td></tr>		
         <tr><td> No of Users </td> <td>'.$this->input->post('users').' </td></tr>		
		</table>
		 </p> </center><br/> 
		  '.$this->getFooter().'</body> 
		 </html>';
	
	 if($email==""){	
			echo json_encode(array('result'=>"false"));	 
	 }
	else
	{
		 $this->sendMail($to="klangat@ari.co.ke",$body,$subject="Enterprise Quote",$from="ARI Homes Support");
		echo json_encode(array('result'=>"ok")); 
	}
 }
 
 /*public function sendRentStatement()
 {
	 $to=$this->input->post('email');
	 $from=$this->input->post('from');
	 $body=$this->input->post('body');
	 $body='<html><body>
		<div style="width:90%;padding:10px;background:ffffff;">
		<div style="padding-left:10px">
		 
		<p style="line-height:15px"> <h4 align="left"> Dear  '.ucfirst(strtolower($this->input->post('first_name'))).',</h4>
		 <h5> See your Rent   Statement below.</h5>
		 <hr/>
		 </p>'.$body.'
		  </center>
		 <img  src="'.base_url().'images/city.png" width="100%">
		 </body> 
		 </html>';
	$this->sendMail($to,$body,$subject="Rent Statement",$from); 
	 
		echo json_encode(array('result'=>"ok",'data'=>1));
	 	
 }*/
 
  
 public function getFooter()
 {
	 $footer='<br/>
		<p align="left"> Thanks, <br/>  
		 Team ARI Homes
		</p>
		  <div>
			<font size="2"> <center> &copy; '.date("Y").' ARI Homes   </center> </font>
			<center> <font> <a href="mailto:support@ari.co.ke">Support@ari.co.ke </a> | +254 789 502 424 | +254 789 732 828 </font> </center> 
		   <center><font size="2"> 1 <sup>st </sup> Floor | All Africa Council of Churches | Sir Francis Ibiam House | Waiyaki Way, Westlands, Nairobi  <br/>
			 
			 <a href="#">Terms </a></font>
			 </center>
			 
			</div>';
			return $footer;
 }
 
 public function sendMail($to,$body,$subject,$from="",$fileName="")
    {
		$this->load->library('email'); 
		$this->email->from('support@ari.co.ke',$from);
		$this->email->to($to); 
		$this->email->subject($subject);
		$this->email->message($body);  
		if($fileName !="")
		{ 
			$this->email->attach($_SERVER["DOCUMENT_ROOT"].'media/'.$fileName); 
		} 
		$this->email->set_mailtype("html");		
		$this->email->send();
		//echo $this->email->print_debugger();
		$this->email->print_debugger();

	}
	  
}