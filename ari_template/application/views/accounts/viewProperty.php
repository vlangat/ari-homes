<!--STATUS--->
<?php    $total_properties=0; foreach($property->result() as $row){$total_properties=$total_properties+1;} 
	$total_units=0; foreach($units->result() as $row){$total_units=$total_units+$row->total_units;} 
	$total_tenants=0; foreach($tenants->result() as $row){$total_tenants++; } 
	$total_rent=0;  foreach($paid_rent->result() as $r)
	{
		 $month=date("m",strtotime($r->date_paid));
		 if($month==date("m"))
			{ 
				$total_rent=$total_rent+$r->amount;
			}
	}
	 
	$expected_rent=0; 
	foreach($rent->result()  as $r)
	{
		 $expected_rent=$expected_rent+$r->payment_type_value; 
	}	
	$t_units=$total_units;  
 ?>
<!--END STATUS---->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="#">Home</a>
	<i class="fa fa-circle"> </i>
</li>
<li>
	<a href="#">Property</a>
	<i class="fa fa-circle"> </i>
</li>
<li>
	<span>View Property </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<?php if($total_units==0){$t_units=1;}$rented=(($total_tenants)/$t_units)*100;?>
<div class="page-content-inner">
<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-green-sharp"> 
						<span data-counter="counterup" data-value="7800"><?php echo $total_properties;?> </span>
						<small class="font-green-sharp"> </small>
					</h3>
					<small> TOTAL PROPERTIES </small>
				</div>
				<div class="icon">
					<i class="icon-home"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: <?php echo round($rented);?>%;" class="progress-bar progress-bar-success green-sharp">
						<span class="sr-only">76% Occupation</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Occupation </div>
					<div class="status-number"><?php echo round($rented);?> %  </div>
				</div>
			</div>
		</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-red-haze">
				<span data-counter="counterup" data-value="1349"> <?php echo $total_units;?></span>
			</h3>
			<small>TOTAL UNITS</small>
		</div>
		<div class="icon">
			<i class="icon-grid"></i>
		</div>
	</div> 
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php echo round($rented,0);?>%;" class="progress-bar progress-bar-success red-haze">
				<span class="sr-only"> 85% change</span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Rented </div>
			<div class="status-number"> <?php echo round($rented);?>%</div>
		</div>
	</div>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-blue-sharp">
						<span data-counter="counterup" data-value="567"> <?php echo $total_tenants;?> </span>
					</h3>
					<small>TOTAL TENANTS</small>
				</div>
				<div class="icon">
					<i class="icon-users"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width:<?php echo round($rented);?>%;" class="progress-bar progress-bar-success blue-sharp">
						<span class="sr-only">45% grow</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Tenants </div>
					<div class="status-number"> <?php echo round($rented);?>%</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-purple-soft"> <?php if($expected_rent==0){$expected_rent=1;} ($total_rent/$expected_rent)*100?>
						<span data-counter="counterup" data-value="276"> <?php echo $total_rent;?>  </span>
					</h3>
					<small> <?php echo date("M");?> Rent Collected </small>
				</div>
				<div class="icon">
				<i class="fa fa-money"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: <?php echo (($total_rent-$expected_rent)/$expected_rent)*100?>%;" class="progress-bar progress-bar-success purple-soft">
						<span class="sr-only">56% change</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> <?php  $per=round((($total_rent)/$expected_rent)*100,0); if($per>100){ echo 'OVER PAID'; $per=$per-100;} else{ echo 'COLLECTION';} ?> </div>
					<div class="status-number"> <?=$per?>% </div>
				</div>
			</div>
		</div>
	</div>
</div>
          <div class="row">
			<div class="col-md-12">
				<div class="col-md-12" style="background:#1bb968;padding:8px;">
						<font color="#ffffff"> Properties </font>
				</div>
			
			</div>
		</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12">
			<div class="form-group">
					<div class="col-md-12">  &nbsp;  </div>
					<a href="<?=base_url();?>property/add" class="btn red"  >Add New property </a> 
			</div>
		</div>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit ">
	<div class="portlet-body">
		<div class="table-toolbar">
		</div>
<form name="form1" method="post" action="<?=site_url();?>property/single" onSubmit="return validate();">
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr> 
		<th> Property </th>
		<th> Type </th>
		<th> Total Units </th>
		<th> Location </th>
		<th> Street </th>
		<th> Floors </th>
		<th> Car Spaces </th>
		<th>  View   </th>
		<th> Edit </th>
		<th> Delete </th>
	</tr>
</thead>
<tbody>
<?php $i=1; foreach($property->result() as $row):?>
	<tr><?php
	$total_units=0;
	$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
	$enct =base64_encode(do_hash($row->id . $salt,'sha512') . $salt);
	?>
		 <td> 	<?=$row->property_name?> 	</td>
		<td> 	<?=$row->property_type?> 	</td> 
		<td> 	<?php foreach($units->result() as $r){ if($row->id==$r->property_id){  $total_units=$total_units+$r->total_units; } }  echo $total_units?> 		</td>
		<td> 	<?=$row->location?> 			</td>
		<td> 	<?=$row->street?> 			</td>
		<td> 	<?=$row->floors?> 			</td> 
		<td class="center"><?=$row->car_spaces?></td>
		<td class="center"  id="view_<?=$i?>"><a href="<?=base_url();?>property/view_property/<?=$enct?>"> <i class="fa fa-file-o"> </i> View </a></td>
		<td class="center" id="edit_<?=$i?>"><a href="<?=base_url();?>property/edit/<?=$enct?>" > <i class="fa fa-edit"> </i>  Edit </a></td>
		<td class="center"  id="delete_<?=$i?>"><a href="javascript:;" onclick="delete_property('<?=$row->id?>')"> <i class="fa fa-trash"> </i>  Remove </a></td>
	</tr>   
<?php $i++; endforeach;?> 
</tbody>
</table>
<!--<a   data-toggle="modal"   href="<?=base_url();?>property/single" class="btn red" > View  </a>-
<input name="view" type="submit" id="view" class="btn red" value="View">  -->
</form>
	</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->

</div>
                    
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
 
<div id="warning" class="modal fade" tabindex="-1" data-width="400">
<div class="modal-body">
			<div class="row">
			<div class="col-md-12"> 
			<h5> <b  style="font-size:18px;color:brown">    Confirm Delete Action </b> </h5>
			<hr/>	
			<p>
				Are you sure you want to remove this property?  This will also disassociate tenants with that property
			</p> 
			<input type="hidden" id="del_id">
			</div>
		</div>
</div>
<div class="modal-footer" >  
	<button type="button"   id="confirm_del"   <?=$disabled?> class="btn green">Yes</button>
	<button type="button" data-dismiss="modal" class="btn default">No</button>
</div>
</div>

<script language="javascript">
function validate()
{
var chks = document.getElementsByName('checkbox[]');
var hasChecked = false;
for (var i = 0; i < chks.length; i++)
{
if (chks[i].checked)
{
hasChecked = true;
break;
}
}
if (hasChecked == false)
{
alert("Please select at least one.");
return false;
}
return true;
}

$(function(){
	checkPrivilege();
	
$("#edit").click(function(){
 validate();
 
 
});	

$("#confirm_del").click(function(){
	var id=$("#del_id").val(); 
	$.ajax({
		url:"<?=base_url();?>property/remove_property/"+id,
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;
			if(obj.result=="ok")
			{  
						setTimeout(function(){ 
							$('#warning').modal('hide');
							location.reload();
                      }, 1500);
		       
			}
			else
			{
				alert('Not deleted Try again later');
				return false;
			}
		}
	 })
	 });
	 
	 
});


function delete_property(id)
 {  
	$("#del_id").val(id);
	 $("#warning").modal('show');
 }	
 
 
 function checkPrivilege()
 { 
	var i="<?=$i?>";
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/1",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data);  
			var data = obj.data;  
			if(obj.edit==0)
			{  
		      for(var x=0; x<i;x++){  $("#edit_"+x).html("<font color='green' onclick=\"alert('You have no privilege to edit property')\"><i class='fa fa-edit'></i> Edit </font>"); }
              			  
			}
			if(obj.delete==0)
			{  
		      for(var x=0; x<=i;x++){  $("#delete_"+x).html("<font color='red' onclick=\"alert('You have no privilege to remove property')\"> <i class='fa fa-trash'></i> Delete </font>"); }
              			  
			}
			if(obj.view==0)
			{  
			 for(var x=0; x<=i;x++){   $("#view_"+x).html("<font color='' onclick=\"alert('You have no privilege to view property')\"> <i class='fa fa-file-o'></i> View</font>"); }	
			}
		}
	 })
 } 
 
</script>