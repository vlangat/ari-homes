 <?php foreach($data->result() as $row) {} ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
	<!-- BEGIN PAGE BREADCRUMBS -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="#">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="#"> Subscriptions </a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Add Users</span>
		</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		 <!-- PORTLET MAIN -->
		<?php //foreach($profile as $rows){}?>
 
	</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
	 
<div class="portlet light " style="min-height:500px">
		<div class="col-md-12" style="background:#006699;padding:6px;">
			<font color="#ffffff">  Add Users  </font>
		</div>	
 
<div class="portlet-body">

<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
 
	<!-- END PERSONAL INFO TAB -->
 
 
<div   id="tab_1_4">
	<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit ">
			 <div class="portlet-body">
					<input type="hidden" value='' id="package_type">
					<input type="hidden" value='' id="total_users">
						<!--<table class="table table-striped table-hover table-bordered" id="sample_editable_1">-->
						<table class="table table-striped table-hover table-bordered"   id="sample_editable_1" >
							<thead>
								<tr>
									<th> First Name </th>
									<th> Last Name </th>
									<th> Phone </th>
									<th> Email </th>
									<th> Status </th>
									<th> &nbsp; </th>
								</tr>
							</thead>
								<tbody  id="angents"> 
								<?php $i=1;
									foreach($data->result() as $r){
										$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
										$enct =base64_encode(do_hash($r->id . $salt,'sha512') . $salt);
										if($disabled=="disabled"){
										$link="#"; 
										}else{ $link=base_url().'Auth/userPrivileges/'.$enct; 
										}
										if($r->user_enabled==1){ $status="<font color='green' title='Click to Deactivate'>Active</font>";} else{ $status="<font color='red' title='Click to activate'> Deactivated </font>";}
										?> 
										<tr><td>  <?=$r->first_name?> </td> <td> <?=$r->last_name?> </td><td> <?=$r->mobile_number?> </td><td> <?=$r->email?> </td><td><a   onclick="remove_user('<?=$r->id?>','<?=$r->user_enabled?>')"> <?=$status?> </a></td><td id="edit_<?=$i?>"><a   href="<?=$link?>"> <i class='fa fa-edit'></i> Edit Permissions </a></td></tr>
										<?php 
										$i++; 
								 }
								 ?>
								</tbody>
						</table>
					<p id="add_btn"> </p>
					</div>
				</div>
			</div>
			
			
</div>
</div> <p id="error">   </p>
</div>   
        <?php if($this->session->flashdata('temp')){
				$msg=$this->session->flashdata('temp');
				echo '<div class = "alert alert-success alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button> <font color="green">'. $msg. '</font> </div>'; 	
				} 	
		?>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
     
 
<!-- END CONTENT -->
 
<!-- END CONTAINER -->
<div id="responsive_2" class="modal fade" tabindex="-1" aria-hidden="true">
<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" id="task"> Add User </font></h4>
	</div>
<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div  class="form-horizontal">
				<div class="form-group">
					<div class="col-md-12">
					First Name:
					<input type="text"  class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off"required="required"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="First_Name" onchange="checkChars('First_Name')"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					Second Name:
					<input type="text" id="Last_Name"  class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off"required="required"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" onchange="checkChars('Last_Name')" />
					</div>
				</div>
			<div class="form-group">
			<div class="col-md-12">
				Email:
				<input type="email" id="email"class="form-control todo-taskbody-tasktitle" required="required" autocomplete="off"required="required" placeholder="Email"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" />
			</div>
	</div>
		<div class="form-group">
			<div class="col-md-12">    
				</div>
			<input type="hidden" id="added_by"class="form-control todo-taskbody-tasktitle"value="<?php echo $this->session->userdata('id');?>" placeholder="">
		</div>
	</div>
	</div>
</div>
</div><font id="add_user"> </font>
<div class="modal-footer"> 
<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
<input type="submit" id="add_new_user" class="btn green"value="Save Changes">
</div>   
</div>
</div> 
  
<!-- responsive -->
<div id="del_user" class="modal fade" tabindex="-1" data-width="400">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b>  Confirm Action </b></h5>
				<p id="del_status"> Are you sure you want to perform this Action? </p>
				
			</div>
		</div>
</div>
<div class="modal-footer" >
<input type="hidden" id="status">
	<button type="button" class="btn green" id="confirm" <?=$disabled?>> Yes </button>
	<button type="button" data-dismiss="modal"  class="btn red"> No </button> 
</div>
</div>
 
 
 <div id="subscribe_users" class="modal fade" tabindex="-1" data-width="600">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b>  Add More Users </b></h5>
				<hr/>
				<p> You need to subscribe to be able to add more users to the system </p>
				<p> Do you want to start the subscription? </p>
				
			</div>
		</div>
</div>
<div class="modal-footer" >

	<button type="button" class="btn green" id="confirm_subscription"> Yes </button>
	<button type="button" data-dismiss="modal"  class="btn red"> No </button> 
</div>
</div>


<div id="privileges" class="modal fade" tabindex="-1" data-width="600">
     <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3"> <b> Assign  Privileges to the user </b> </font></h4>
	</div>
	<div class="modal-body"> 
			 <form action="<?=base_url();?>Auth/assign_privilege/users" method="post">
			 <input type="hidden" value="" name="user_id" id="user_id">
			<table   class="table table-striped table-hover table-bordered" id="sample_editable_1">
			<thead>
			<tr><th> &nbsp; </th> <th> Add </th> <th> Edit  </th> <th> View </th> <th> Delete </th> </tr>
			</thead>
			<tbody>
			<tr>
				<td> 
					<strong>Property </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="p_addPrivilege"    value="1">   
					  </td> 
					 <td>
					 <input type="checkbox"  name="p_editPrivilege"   value="1"> 
					 </td> 
					 <td> 
					 <input type="checkbox"   name="p_viewPrivilege"  value="1">  
					</td> 
					<td> 
					<input type="checkbox"   name="p_deletePrivilege" value="1" > 
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Tenants </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="t_addPrivilege" value="1">  
					  </td> 
					 <td>
					 <input type="checkbox"  name="t_editPrivilege" value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"  name="t_viewPrivilege" value="1">   
					</td> 
					<td> 
					<input type="checkbox"  name="t_deletePrivilege" value="1">  
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Rent </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="r_addPrivilege" value="1">   
					  </td> 
					 <td>
					 <input type="checkbox"  name="r_editPrivilege" value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"  name="r_viewPrivilege" value="1">    
					</td> 
					<td> 
					<input type="checkbox"  name="r_deletePrivilege" value="1">    
					  
					</td>
			</tr>			
			<tr>
					<td> 
						<strong>Suppliers </strong>
					</td>  
					<td>  
						<input type="checkbox"  name="s_addPrivilege" value="1">    
					  </td> 
					 <td>
						<input type="checkbox"  name="s_editPrivilege" value="1">   
					 </td> 
					 <td> 
					 <input type="checkbox"  name="s_viewPrivilege" value="1">  
					</td> 
					<td> 
					<input type="checkbox"  name="s_deletePrivilege" value="1">    
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Subscriptions </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="sub_addPrivilege" value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="sub_editPrivilege" value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"  name="sub_viewPrivilege" value="1">    
					</td> 
					<td> 
					<input type="checkbox"  name="sub_deletePrivilege" value="1">    
					  
					</td>
			</tr>			
 <tr>
				<td> 
					<strong>Users </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="users_addPrivilege" value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="users_editPrivilege" value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"  name="users_viewPrivilege" value="1">    
					</td> 
					<td> 
					<input type="checkbox"  name="users_deletePrivilege" value="1">    
					  
					</td>
			</tr>			
 </tbody>
</table>	
<p>  </p>		
  
</div>
<div class="modal-footer" >
 
	<button type="submit" class="btn green"   <?=$disabled?>> Save  </button>
	<button type="button" data-dismiss="modal"  class="btn red"> Close </button> 
</div>
</form>
</div>

<div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5> <b> Venit Message </b> </h5>
				<hr/>
					<p id="msg">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
   
 <input type="hidden" value="1" id="delete_items"> 
 <input type="hidden" value="1" id="add_items">   
</body>

</html>
<script type="text/javascript">
 $(document).ready(function()
 {
	 
	
	getAgents();
	 checkPrivilege(); 
 }
);
$(function()
{ 
	$('#responsive_2').modal('hide');
	$("#add_btn").append("<a data-toggle='modal' href='javascript:;' id='add_new'    class='btn green'><i class='fa fa-plus'></i> Add New User</a>");  
	
		
var msg="<?=$msg?>";
 if(!msg)
 {
	 
 }else{
	 $("#msg").html("<font> "+msg+" </font>");
	 $('#success').modal('show');
 }
 

$("#confirm_subscription").click(function()
{ 
	var package_name=$("#package_type").val();
	window.location="<?=base_url();?>payment/package/";
});

$("#add_new").click(function()
{ 
      
	 var a=$("#add_items").val();
	 
	var c="<?=$disabled?>";
	if(c=="disabled")
	{
		  alert("You cannot add new user! Ensure your subscription is active");
	}else  if(a==0){
		alert('You have no privilege to add new user!'); 
		return false;
	 }
else{
	var users_limit=$("#total_users").val();
	var i=0;
	var users=0;
	$.ajax({
		url:"<?=base_url();?>Auth/checkUsers",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
                                 
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					var property=data[i]; 
					var package_name=property['package_type']; 
					users=users+property['number_of_users']++;
				 } 
				 
				if(users_limit<users)
				{ 
					$('#responsive_2').modal('show');
				
					
				}else{
					$("#package_type").val(package_name);
						$('#subscribe_users').modal('show');
						
					return false;
				}
				
			}
			else{
				$('#subscribe_users').modal('show');
			}
		}
		
		
	})
	
	}
});

$("#add_new_user").click(function()
	{  
		var email=$("#email").val();
		var fname=$("#First_Name").val();
		var lname=$("#Last_Name").val(); 
	  	
		if(email&&fname&&lname){ } else{$("#add_user").html("<font color='red'> Empty fields not required </font>"); return false;}
		var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#add_user").html("<font color='red'> Email provided is not a valid e-mail address"); return false;}
		$("#add_user").html("<font color='blue'> Adding new user....</font>");
		$.ajax({
		 url:"<?=base_url();?>Auth/addUser",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'lname':lname, 
		   'email':email 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){
				 $("#First Name").val('');$("#Last Name").val(''); $("#email").val('');
				  
				 $("#add_user").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>"+obj.msg+"  </font>"); 
                 getAgents(); 
                $("#user_id").val(obj.user_id); 
					setTimeout(function(){
					$('#add_user').empty();
					$('#responsive_2').modal('hide');
					$("#privileges").modal('show');
				}, 3000);				
					
			 }
			 else{
				 $("#add_user").html("<font color='red'> "+obj.msg+"</font>");return false;
			 }
		 }
		 
		  }
		 )	  
});

$("#confirm").click(function(){ 
	var par1=$("#confirm").val();
	var par2=$("#status").val();  
	$("#del_status").html("<font color='blue'>  Please wait....</font>");
	 $.ajax({
		url:"<?=base_url();?>Auth/removeUser/"+par1+"/"+par2,
		type:"POST",
		async:false ,
		success:function(data)
		{ 
			var obj = JSON.parse(data); 
			if (obj.result=="ok")
			{
			   getAgents();
			   if(par2==1){ msg="de-activated";}else{msg="activated";}
			   $("#del_status").html(" <font color='green'> Agent "+msg+" successfully</font> ");
				setTimeout(function(){
					$('#del_status').empty();
					$('#del_user').modal('hide');
                                     document.location.reload(true);
				}, 3000);
				return false;			   
			}
			else{
				$("#del_status").html(" <font color='red'> Error! Agent not removed </font> "); 
				return false;
			}
			
		}
		})
	
});

checkPrivilege();

});

function getAgents()
{
	var i=0;
	var link="";
	var users=1;
	var c="<?=$disabled?>";
	
	$.ajax({
		url:"<?=base_url();?>Auth/loadAgents",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				 var content="";
				 $("#agents").empty();
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					var status="";
					var agent=data[i]; 
					if(c=="disabled"){ link="#"; }else{ link="<?=base_url();?>Auth/userPrivileges/"+agent['id']; }
					 if(agent['user_enabled']==1){ status="<font color='green' title='Click to Deactivate'>Active</font>";} else{ status="<font color='red' title='Click to activate'> Deactivated </font>";}
					content=content+"<tr><td> "+agent['first_name']+"</td> <td> "+agent['last_name']+"</td><td> "+agent['mobile_number']+"</td><td> "+agent['email']+"</td><td><a   onclick=\"remove_user("+agent['id']+","+agent['user_enabled']+")\"> " +status+" </a></td><td><a   href='"+link+"'> <i class='fa fa-edit'></i> Edit Privileges </a></td></tr>";
					users++;
				 }
				 $("#total_users").val(users); 
				 $("#agents").html(content); 
			}
else
{

$("#total_users").val(users); 
}
		}
		
		
	})
	
}

function remove_user(id,status)
{ 
	
		$("#del_user").modal('toggle');
		$("#confirm").val(id);
		$("#status").val(status); 
		 
}

function checkPrivilege()
 { 
	 var i="<?=$i?>";
	$.ajax({
		url:"<?=base_url();?>Auth/checkPrivilege/6",
		type:"POST", 
		async:false,
		success:function(data)
		{  
			var obj=JSON.parse(data);   
			var data = obj.data;   
			if(obj.add==0)
			{        $("#add_items").val(obj.add); 
				 document.getElementById('add_new_user').disabled = true; 
				 document.getElementById('add_new').disabled = true; 
			 } 
			if(obj.view==0){ 
				//document.getElementById('pricinng_property_name').disabled = true;  
			}
			if(obj.edit==0){  
				 //document.getElementById('edit').disabled = true;  
				 for(var x=0; x<i;x++){  $("#edit_"+x).html("<font color='' onclick=\"alert('You have no privilege to edit users')\"> <a href='javascript:;'><i class='fa fa-edit'></i>Edit Privileges </a></font>"); }
             
			}
			if(obj.delete==0){ 
			 $("#delete_items").val(obj.delete);  
				 document.getElementById('confirm').disabled = true;   
			}
		}
	 })
 } 
 

</script>