<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head profile="http://www.w3.org/2005/10/profile">
<link rel="icon" 
      type="image/png" 
      href="<?=base_url();?>images/login/favicon.png">
<meta charset="utf-8" />
<title>ARI Homes | Real Estate Management</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" /> 
<link href="<?=base_url();?>template/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	   
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
 <link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/css/dropzone.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?=base_url();?>multifiles/prism.css">
  <link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />

<!-- END THEME GLOBAL STYLES -->
 <!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url();?>template/theme/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>css/mycss.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
 <link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END THEME LAYOUT STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS --> 

        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
 <!-- HTML TO PDF PLUGIN --> 
<script src="<?=base_url();?>js/jspdf.min.js" type="text/javascript"></script>
<!-- END OF HTML TO PDF PLUGIN -->   
  <link rel="stylesheet" href="<?=base_url();?>css/bootstrap-select.css">
  <script src="<?=base_url();?>js/bootstrap-select.js"></script> 
<!--Auto complete plugins--> 
<!--<link rel="shortcut icon" href="<?=base_url();?>images/favicon.png" />--> 
 <link rel="stylesheet" href="<?=base_url();?>css/jquery-ui.css">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 <link rel="stylesheet" href="<?=base_url();?>css/chat.css">
<link rel="stylesheet" href="<?=base_url();?>css/my_chat.css">

 <!---End of autocomplete--->
<script type='text/javascript'>
function checkChars(id)
{
   var TCode = document.getElementById(id).value;
   if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false)
   {
		//alert(id+' contains illegal characters');
		$("#error_msg").html("<font color='red'> "+id+" field contains illegal characters.Only [Aa-Zz] or [0-9] required</font>"); 
		 document.getElementById(id).value="";
		$("#text_field_error").modal('toggle'); 
        return false;
   }
    return true;     
}
 
function validateChar(id)
{
   var TCode = document.getElementById(id).value;
   if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false)
   {
	//alert(id+' contains illegal characters');
	//$("#error_msg").html("<font color='red'> "+id+" field contains illegal characters</font>"); 
	document.getElementById(id).value=""; 
	//$("#text_field_error").modal('toggle'); 
        return false;
   }
    return true;     
}

function checkIt(evt)
{  
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        status = "This field accepts numbers only."
        return false
    }
    status = ""
    return true
} 
</SCRIPT>
 
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid page-boxed">
	<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<div class="page-header-top">
			<div class="container">
				<!-- BEGIN LOGO -->
				<div class="page-logo">
					<a href="<?=base_url();?>property/">
						<img src="<?=base_url()?>images/login/ARI_Homes_Logo.png" width="90" height="45" alt="logo" class="logo-default">
					</a>
		</div>
<!-- END LOGO -->
<!-- BEGIN RESPONSIVE MENU TOGGLER -->
<a href="javascript:;" class="menu-toggler"> </a>
<!-- END RESPONSIVE MENU TOGGLER -->
<div class="top-menu"> 
<ul class="nav navbar-nav pull-right">
	<!-- BEGIN NOTIFICATION DROPDOWN -->
<!--<li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
		<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			<i class="icon-bell"> </i>
			<span class="badge badge-default" id="count"> 0 </span>
		</a>
<ul class="dropdown-menu">
	<li class="external">
		<h3>You have
			<strong id="count1"> 0 </strong> latest messages</h3>
		<a href="#"> view all </a>
	</li>
	<li>
		<ul class="dropdown-menu-list scroller" style="height:250px;" data-handle-color="#637283" id="notification">
			 
			 <!---NOTIFICATION IS APPENDED HERE--->
		
	<!--	</ul>
	</li>
</ul>
</li>-->

<!-- END NOTIFICATION DROPDOWN -->
 
<!--<li class="droddown dropdown-separator">
<span class="separator"></span>
</li>-->
 
<!-- END INBOX DROPDOWN -->

<!-- BEGIN TOP NAVIGATION MENU -->
<div class="top-menu">
<ul class="nav navbar-nav pull-right">
 
<!-- BEGIN USER LOGIN DROPDOWN -->
<li class="dropdown dropdown-user dropdown-dark">
	<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
		<img alt="" class="img-circle" src="<?=base_url();?>media/<?php echo $this->session->userdata('photo');?>">
		<span class="username username-hide-mobile">
		<?php
		$id=$this->session->userdata('id');
	  if(!empty($id))
	  {
				echo ucfirst(strtolower($this->session->userdata('first_name')));
	  }
	  else{
		  //redirect(base_url()."manager/account/");
		  }  
	?> </span>
	</a>
	<ul class="dropdown-menu dropdown-menu-default"> 
			<li class=" ">
				<a href="<?=base_url();?>auth/profile"  class="nav-link  "> <i class="icon-user"></i>  My Profile
				   
				</a>
			</li>
 
		<li>
			<a href="<?=base_url();?>auth/logout">
				<i class="icon-key"></i> Log Out </a>
		</li>
	</ul>
</li>
</ul>
</div>
<!-- END USER LOGIN DROPDOWN -->
 
</ul>
</div>
<!-- END TOP NAVIGATION MENU -->
</div>
</div> 
	<?php
	  $id = $this->session->userdata('id');
	  $id_review=$this->session->userdata('id_review');
	  $tenant_signup=$this->session->userdata('tenant_sign_up_id');
	  if($tenant_signup !="")
	  {
		  redirect(base_url()."auth/");
			//////	  
	  }
	  else if($id_review !="")
	  {
	      
	   redirect(base_url()."auth/");   
	  }
	  else if($id =="") 
		{
		    $msg="";
		   // $msg="Your were log out! Login again";
		    $this->session->set_flashdata('error_msg','1');
		    $this->session->set_flashdata('temp',$msg);  
	 	redirect(base_url()."auth");
		}
		
	?>
<!-- END HEADER TOP -->
<!-- BEGIN HEADER MENU --> 
<div class="page-header-menu">
	<div class="container"> 
<!-- BEGIN MEGA MENU -->
<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
<div class="hor-menu  ">
<ul class="nav navbar-nav">
	<li class="menu-dropdown classic-menu-dropdown ">
		<a href="<?=base_url();?>property"> Home 
			<span class="arrow"></span>
		</a>
	</li>
		<li class="menu-dropdown classic-menu-dropdown ">
		<a href="javascript:;"> Property
			<span class="arrow"></span>
		</a>
		<ul class="dropdown-menu pull-left">
			<li class="">
				<a href="<?=base_url();?>property/add" class="nav-link  ">
					  Add Property 
				</a>
			</li>
			
			<li class=" ">
				<a href="<?=base_url();?>property/add#tab_1_3" class="nav-link  ">
				  Unit Details
				</a>
			</li> 
			<li class=" ">
				<a href="<?=base_url();?>property/add#tab_1_4" class="nav-link  ">
				  Payment Details
				</a>
			</li>  
			<li class=" ">
				<a href="<?=base_url();?>tenants/landlord" class="nav-link  ">
					 Landlord Details</a>
			</li>
			<li class=" ">
				<a href="<?=base_url();?>property" class="nav-link  ">
					 View All Properties</a>
			</li>
		</ul>
	</li>
	<li class="menu-dropdown classic-menu-dropdown ">
		<a href="javascript:;"> Tenants
			<span class="arrow"></span>
		</a>
		<ul class="dropdown-menu pull-left">
			<li class="">
				<a href="<?=base_url();?>tenants/individual" class="nav-link  ">
					  Add Residential Tenant 
				</a>
			</li>
			<li class="">
				<a href="<?=base_url();?>tenants/commercial" class="nav-link  ">
					  Add Commercial Tenant 
				</a>
			</li>
			<li class="">
				<a href="<?=base_url();?>tenants/waterManagement" class="nav-link  ">
					 Water Management 
				</a>
			</li>
			<li class="">
				<a href="<?=base_url();?>tenants" class="nav-link  ">
					View All Tenants </a>
			</li> 
		</ul>
	</li>
	
	<li class="menu-dropdown classic-menu-dropdown ">
		<a href="javascript:;"> Rent
			<span class="arrow"></span>
		</a>
		<ul class="dropdown-menu pull-left">
			<li class=" ">
				<a href="<?=base_url();?>rent/receivePayment" class="nav-link  ">
					 Receive Payment
				   
				</a>
			</li>
			<li class=" ">
				<a href="<?=base_url();?>rent/receipts" class="nav-link  ">
					Show Receipts </a>
			</li>
			<li class=" ">
				<a href="<?=base_url();?>rent/paid" class="nav-link  ">
					 Paid Rent </a>
			</li>
			<li class=" ">
				<a href="<?=base_url();?>rent/unpaid" class="nav-link  ">
				  Unpaid Rent
					
				</a>
			</li> 
			  
			<!--<li class="">
				<a href="<?=base_url();?>rent/edit_tenant_statement" class="nav-link  ">
				  Update Payment
					
				</a>
			</li>--> 
			 
		</ul>
	</li> 
 
 <li class="menu-dropdown classic-menu-dropdown ">
		<a href="javascript:;"> Expenses
			<span class="arrow"></span>
		</a>
		<ul class="dropdown-menu pull-left">
			
			<li class=" ">
				<a href="<?=base_url();?>rent/suppliers" class="nav-link  ">
					Add Suppliers
				</a>
			</li> 
			<li class=" ">
				<a href="<?=base_url();?>rent/property_expenses" class="nav-link  ">
					 Property Expense
				</a>
			</li><li class=" ">
				<a href="<?=base_url();?>rent/business_expenses" class="nav-link  ">
					 Business Expense
				</a>
			</li>	
			<li class=" ">
				<a href="<?=base_url();?>rent/deposits" class="nav-link  ">
					 Deposits
				</a>
			</li>	
			<!--<li class=" ">
				<a href="<?=base_url();?>rent/pay_expenses" class="nav-link  ">
					 Record Expense
				</a>
			</li>	
			 <li class=" ">
				<a href="<?=base_url();?>rent/expense_statement" class="nav-link  ">
				    Expense Statement 
				</a>
			</li>--> 
  
		</ul>
</li> 

 <li class="menu-dropdown classic-menu-dropdown ">
		<a href="javascript:;"> Subscriptions
			<span class="arrow"></span>
		</a>
		<ul class="dropdown-menu pull-left"> 
			<li class=" ">
				<a href="<?=base_url();?>payment/package" class="nav-link  ">
				  Buy Package 
				</a>
			</li>
<li class=" ">
				<a href="<?=base_url();?>payment/subscription" class="nav-link  ">
				My Subscription  </a>
			</li>

			<li class=" ">
				<a href="<?=base_url();?>payment/transactions" class="nav-link  ">
				 My Transactions 
				</a>
			</li>
			

			<li class=" ">
				<a href="<?=base_url();?>payment/sms" class="nav-link  ">
					  My SMS 
				</a>
			</li>
			<li class=" ">
				<a href="<?=base_url();?>auth/users/" class="nav-link  ">
				  Manage Users 
				</a>
			</li>
			
			
		</ul>
</li> 

<!--<li class="menu-dropdown classic-menu-dropdown ">
		<a href="<?=base_url();?>support/"> Support
			<span class="arrow"></span>
		</a> 
</li> 
-->
 <li class="menu-dropdown classic-menu-dropdown ">
		<a href="javascript:;"> Financial Reports
			<span class="arrow"></span>
		</a>
		<ul class="dropdown-menu pull-left"> 
			<li class=" ">
				<a href="<?=base_url();?>financial/property_report" class="nav-link  ">
				Property Report  </a>
			</li>
			<li class=" ">
				<a href="<?=base_url();?>financial/landlord_report" class="nav-link  ">
				Landlord Report 
				</a>
			</li>
			<li class=" ">
				<a href="<?=base_url();?>financial/property_expense" class="nav-link  ">
				 Property Expense Report
				</a>
			</li> 
			<li class=" ">
				<a href="<?=base_url();?>financial/business_expense" class="nav-link  ">
				 Business Expense Report
				</a>
			</li> 
			<li class=" ">
				<a href="<?=base_url();?>financial/profit_loss" class="nav-link  ">
				  Agent Income Statement  
				</a>
			</li> 
			
		</ul>
</li> 

<li class="menu-dropdown classic-menu-dropdown ">
		<!--<a href="javascript:;">-- Account
			<!--<span class="arrow"></span>--
		</a> -->
		<!--<ul class="dropdown-menu pull-left">-->
			<?php if($this->session->userdata('user_type')=="Individual"){?>
			<!--<li class=" ">-->
				<a href="<?=base_url();?>auth/profile"  class="nav-link  ">  Account
						
				</a> 
		<?php } ?>
		<?php if($this->session->userdata('user_type')=="Business"){?>			
			<!--<li class=" ">-->
				<a href="<?=base_url();?>auth/agent"  class="nav-link  ">  Account
				</a>
			<!--</li>-->
 
			<?php } ?> 
			<!--</ul> -->
	</li>  							  
</ul>
</div>
<!-- END MEGA MENU -->
</div>
</div>
<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<div id="text_field_error" class="modal fade" tabindex="-1" data-width="450">
	  <div class="modal-body">
			<div class="row">
				<div class="col-md-12"> 
				<h5> <b> Venit Message </b> </h5>
				<hr/>
					<p id="error_msg">
					  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>

<div id="expires" class="modal fade" tabindex="-1" data-width="450">
	  <div class="modal-body">
			<div class="row">
				<div class="col-md-12"> 
				<h5> <b id="header"> Venit Message </b> </h5>
				<hr/>
					<p id="expire_msg">
					  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" id="e_click" class="btn btn-outline dark">OK</button>
	</div>
</div>


<script language="javascript">
	$(function()
	{
	   getSess();
		update_receipt();
		subscription();
		notify();
		$("#e_click").click(function()
		{
			window.location="<?=base_url();?>auth/logout";
		});
		
	});

function update_receipt()
{   
	$.ajax({
		url:"<?=base_url();?>rent/update_receipt",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
			    	 
			}
		}
		
		
	})
	
}
function getSess()
{   
var s="<?php  echo $this->session->userdata('session_id');?>"; 
	$.ajax({
		url:"<?=base_url();?>auth/checkId",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
			   if(s==obj.sessId){
				   
			} 	
else{
	$("#header").html("Logged Out");
	$("#expire_msg").html("Your account is logged in another device!");
	 
	$("#expires").modal({backdrop: "static"}); 
	
}			   
			}
		}
		
		
	})
	
}


 function subscription()
 {
	 $.ajax(
		{
		url:"<?=base_url();?>auth/subscription_expiry_date",
		type:"POST",
		success:function(data)
		{
		var obj = JSON.parse(data);  
		 if (obj.days<=0)
		 {
			//$("#expire_msg").html("<font color='red' size='4'> Your subscription has expired! Please re-new to enjoy our services</font>"); 
		 
		    //$("#expires").modal({backdrop: "static"});
		    //$("#expires").modal('show');
		 
		 }
			 
		}
	})
		 
 }

 function notify()
 {
		var count=0;
		$("#count").html(count);
		$("#count1").html(count); 
		$.ajax(
		{
		url:"<?=base_url();?>support/loadNotifications",
		type:"POST",
		success:function(data)
		{
		//alert(data);
		var id2="<?=substr(md5(date("d")),0,16)?>"; 		
		 var body=""; 
		 var icon='';			 
		 var icon2=''; 	 
		 var obj = JSON.parse(data);
		 if (obj.result=="ok")
		 {    
			var data = obj.data;
			for(var i=0; i<data.length; i++)
			{
				var message = data[i]; 
			 var id=message['id'];
			 var msg=message['subject']; 
			 var stat=message['status'];
			 var isNew=message['status'];
			 var background="";
			 if(isNew==1){ background="#595a5b";}else{ background="";}
			 if(stat=='2'){ icon='label label-sm label-icon label-danger'; icon2='fa fa-bolt';}else{icon='label label-sm label-icon label-info'; icon2='fa fa-bell-o';}
			body+="<li   style='background:"+background+"' ><a href='<?=base_url();?>support/response/"+id+"/"+id2+"'> <span style='background:#595a5b' class='time'> "+message['response_date']+" </span> <span class='details'>"+
				   "<span class='"+icon+"'><i class='"+icon2+"'></i>"+
				 "</span><font color='#ffffff'>"+msg+"</font>  <br/><div style='padding-left:35px'> "+message['description'].substring(0, 18)+"...."+" </div></span> </a> </li> ";
			 
			if(isNew==1){
			count++;   			
			}  
			} 
			 
	$("#notification").html(body);
	$("#count").html(count);
	$("#count1").html(count);		
	$("#notification").animate({ scrollBottom: $(document).height() }, "fast");
 }
 else{$("#count").html(0);  }
 }
 
 })
	 
 }
 
</script>