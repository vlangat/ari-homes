<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Expenses </a></span>
	<i class="fa fa-circle"> </i>
</li>
<li>
	<span> Expense Statement</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
 
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title">
		 
<form action="<?=base_url();?>rent/expense_statement" method="post" onsubmit="return validate()">	 
<div class="row">
	<div class="col-md-4">  
		<div class="form-group">    
				<label class="control-label col-md-4">From </label>
				 <input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value="<?=date("m/d/Y")?>"  placeholder=""  name="from" />
			
					<!-- /input-group -->
				</div> 
			<label id="error1">					</label>  
	</div> 
	<div class="col-md-4">  
		<div class="form-group">    
				<label class="control-label col-md-4">To </label>
				 <input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value="<?=date("m/d/Y")?>"  placeholder=""  name="to" />
		</div> 
			<label id="error1">					</label>  
	</div> 	 		 
	<div class="col-md-4"> 
	<label class="control-label col-md-12"> &nbsp;&nbsp; </label>
		<div class="form-group">   
				 <button type="submit" class="btn green"> Search</button>
		</div>
	</div> 
</div> 
</form>
  
<hr/>
 <!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
	
<div id="prnt">
<table    class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr> 
		<th align="left"> Date </th>  
		<th align="left"> Supplier </th> 			
		<th align="left"> Payment Mode </th>  
		<th align="left"> Payment No </th>   
		<th align="left"> Description </th> 	
		<th align="left"> Amount   </th>   
	</tr>
</thead>
<tbody> 
<?php 
		$total=0;
		$supplier="";
		foreach($statement->result() as $row){
		 $total=$total+$row->amount;
		 
		    
	?>

	<tr>
	 <?php foreach($suppliers->result() as $s){ if($s->id==$row->supplier_id){ $supplier=$s->company_name;} }?>
		  <td> <?=$row->date_paid?>    </td>   <td>  <?=$supplier?> </td>  <td> <?=$row->payment_mode?> </td> <td>  <a href="javascript:;" onclick="viewPayment('<?=$row->id?>','<?=$supplier?>')">   <?=$row->payment_method_code?> </a>  </td> <td> <?=$row->description?>  </td> <td> <?=$row->amount?> </td>
	</tr>
	
<?php   }?>	
</tbody>
</table>
<hr/>
<center> <p><b>  Total  </b>  Ksh: <?=$total; ?> </p> </center>
</div>
<!--
 <a  href="javascript:;" class="btn green"  id="btn_receipt" >  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
	-->	 
</div> 
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div>  


<div id="edit_payment" class="modal fade" tabindex="-1" data-width="800" aria-hidden="true"> 

	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Payment Details for  <font id="for">  </font></b> </font></h4>
	 </div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<form action="<?=base_url();?>rent/save_expenses" method="post" onsubmit="return validate_edit()">
			<div class="row">
			
				<div class="col-md-6">
				<p> <label class="control-label"> Name </label> <br/> 
							<input   class="form-control" type="text"  value="" name="from" id="s_name" readonly> 
								 
						</p>
					<p> <label class="control-label">Invoice/Cheque No </label>
							<input   class="form-control" type="text" readonly name="receipt_no"  id="s_receipt_no">
							 
						</p> 
						
						<p> <label class="control-label">Payment Method </label>
								<select class="form-control" type="text" readonly name="pay_mode" id="s_pay_mode">
									<option value="Cash"> Cash </option>
									<option value="Cheque"> Cheque </option>
									<option value="Mpesa"> Mpesa </option>
									<option value="Bank Receipt"> Bank Receipt </option>
								</select> 
						</p>
				</div>
				<div class="col-md-6">   
					 
						<p> <label class="control-label"> Amount Paid </label>
							<input   class="form-control" readonly type="text" name="amount" id="s_amount">
							 
						</p> 
						<p> <label class="control-label">Payment For </label>
						<select type="text" class="form-control" readonly name="pay_for" id="s_pay_for"> 
							   
						</select> 
						 </p>
						<p> <label class="control-label"> Description </label>
							<textarea class="form-control" readonly id="s_description" name="description"> </textarea>
						</p>
						  
												
				</div>
			</div>	 
		<div class="modal-footer" ><center>  
	<button type="button" data-dismiss="modal" class="btn btn-green">Close</button>
	</center>
</div>	
</form> 	 
</div>			
	</div>	
	</div>		 
<!-- END CONTAINER -->
<script language="javascript">
 function viewPayment(id,name)
  {
	   
	$.ajax(
	{
		url:"<?=base_url();?>rent/get_paid_expenses/"+id,
		type:"POST",  
		success:function(data)
		{
			$("#pay_for").empty();
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {   
				 var data=obj.data;
				 for(var i=0;i<=data.length; i++)
				 {
					 var supplier=data[i];
					 //var name=supplier['supplier_id'];
					 var no=supplier['id'];
					 var date=supplier['date_paid'];
					 var pay_mode=supplier['payment_mode'];
					 var amount=supplier['amount'];
					 var payment_for=supplier['expense_type_id'];
					 var description=supplier['description'];
					 var transaction_no=supplier['payment_method_code'];
					 $("#s_name").val(name);
					 $("#editing_id").val(no);
					 $("#for").html(name.toUpperCase());  
					  $("#s_amount").val(amount);
					 $("#s_pay_mode").val(pay_mode);
					  $("#s_pay_for").val(payment_for);
					 $("#s_receipt_no").val(transaction_no);
					  $("#s_description").val(description);
					 $("#edit_error").empty();
					$("#edit_payment").modal('show');
				 }
			 }
		}
	  
	  
  })
  }
  
  
 
  
 $("#btn_receipt").click( function () {
	var divContents = $("#prnt").html();
	var printWindow = window.open('', '', 'height=400,width=800');
	printWindow.document.write('<html><head><title>  Statement </title>');
	printWindow.document.write('</head><body >');
	printWindow.document.write(divContents);
	printWindow.document.write('</body></html>');
	printWindow.document.close();
	printWindow.print();
        });
</script>
