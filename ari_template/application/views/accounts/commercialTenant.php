<?php $total_tenants=0; foreach($tenants->result() as $r){ $total_tenants++;   }?>
<?php $packages=0; foreach($package_account->result() as $r){  $packages++; }?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->

<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
	<div class="container">
		<!-- BEGIN PAGE BREADCRUMBS -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="<?=base_url();?>">Home</a>
				<i ng-click="" class="fa fa-circle"></i>
			</li>
			<li>
				 Tenant 
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Add Commercial Tenant </span>
			</li>
		</ul>
		<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-md-12">
<!-- BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
	<div class="row">
		<div class="col-md-12">
<div class="portlet light ">		 
<div class="portlet-body">
	<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
<div class="tab-pane active" id="tab_1_1">
<?php if($msg !=""){
				 
			/*							 
				echo '<div class = "alert alert-success alert-dismissable" style="background:lightgreen">
					   <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
						  &times;
					   </button>
						
					   <font color="#006699">'. $msg. '</font>
					</div>'; */	
					}
									
				?>
<div class="row">
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Business  Details </font>
		</div>		
</div>
<div class="col-md-12">  &nbsp;  </div>		
</div>
<?php //echo form_open('index.php/tenants/commercialTenant');?>
<form  action="<?=site_url('tenants/commercialTenant');?>" enctype="multipart/form-data" method="post" onsubmit="return validate()" >
<form role="form" action="#">
<div class="row"> 
<div class="col-md-12">
		<div class="form-group">
			<label class="control-label">Company /Business Name  </label>
			<input type="text"   class="form-control" name="company" id="Company Name" placeholder="Company Name" onchange="checkChars('Company Name')"/>
			</div>
</div>
 
 
<div class="col-md-4">
			<div class="form-group">
			<label class="control-label">Business Number  </label>
			<input type="text"  class="form-control" name="business_no" id="Business No" placeholder="Business No" onchange="checkChars('Business No')"/>
		</div>
</div>
<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> Business Activity </label>
			<input type="text"  class="form-control" placeholder="Business Activity " name="activity" id="Business Activity" onchange="checkChars('Business Activity')"/>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Mobile Number </label>
			<input type="text"   class="form-control" name="mobile" placeholder="Mobile Number" maxlength="10" id="Mobile No" onchange="checkChars('Mobile No')" onkeypress="return checkIt(event)"/>
		</div>
	</div>
 
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> Email </label>
			<input type="email"   class="form-control" placeholder="Email (Optional)" name="email" id="Email" onchange="return validateMail()"/>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> Contact Person Full Name </label>
			<input type="text"  class="form-control" name="contact_person" placeholder="Contact person (Optional)"  id="Contact Person" onchange="checkChars('Contact Person')" />
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> Contact Person Phone Number </label>
			<input type="text"  class="form-control" name="phone_no" placeholder="Contact person Phone (Optional)"  id="Phone No" onkeypress="return checkIt(event)" onchange="checkChars('Phone No')" maxlength="10"/>
		</div>
	</div>
 
</div> 
<div class="row">
	<div class="col-md-12">
			<div class="col-md-12" style="background:#1bb968;padding:6px;">
					<font color="#ffffff"> House/Unit Allocation Details </font>
			</div>			
	</div>
	<div class="col-md-12">  &nbsp;  </div>		
</div> 
 <div class="row"> 
<div class="col-md-4">
<div class="form-group"><?php foreach($property->result() as $rows) {}?>
		<label class="control-label">Allocated Property Name </label>
		   <select class="form-control" required="required" name="property" id="property" onChange="getCategory(this.value)" required>  
				<option value=""> select property  </option>
				<?php foreach($property->result() as $row){?>
					
					<option value="<?=$row->id?>"><?=$row->property_name?>  </option>
				<?php }?>
			</select>
			</div>
		</div>
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">

		<label class="control-label"> Unit Category  </label>
		<input type="hidden" id="unit_id" name="unit_id" />
	
			<select class="form-control" required="required" name="category" id="category" onChange="checkUnitAvailability(this.value)">
				 
			</select> 
			</div>
		</div>
</div>
<div class="col-md-4">
<div class="form-group">
	<div class="form-group">
		<label class="control-label"> Floor </label>
		<input type="hidden"  class="form-control"  id="floors_available"/>
		<input type="number"    class="form-control" placeholder="Floor No (Optional)"  name="floor" min="0" max="1000" value="0" onkeypress="return checkIt(event)" id="Floor No" onchange="checkFloors(this.value)"/>
	</div>
</div>
</div>

</div>	
<div class="row"> 
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> House Number </label>
				<input type="text" placeholder="House No (Optional)"   class="form-control" name="houseno" id="House No" onchange="checkChars('House No')"/>
			</div>
		</div>
</div> 	 
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Parking spaces Allocated </label>
				<input type="hidden"  class="form-control"  id="car_spaces"/>
				<input type="number" max="1000" min="0" placeholder="Parking spaces (Optional)"   class="form-control" value="0" name="car_spaces" id="Parking Spaces Allocated"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"  onkeypress="return checkIt(event)"  onchange="checkAvailability(this.value)"/>
			</div>
		</div>
</div>
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Registration date </label>
				<input type="text" class="form-control  input-xxlarge date-picker" value="<?=date('m/d/Y')?>" data-date-format="dd/mm/yyyy" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"   required name="date_registered" id="datepicker"/>
			</div>
		</div>
</div>
</div> 
 
	
<div class="row">
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Rent Details </font>
				
		</div>
</div>
	<div class="col-md-12">  &nbsp;  </div>		
</div>

<div class="row"> 
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Rent Frequency </label>
				<select class="form-control" required name="rent_frequency" onchange="checkRentFrequency(this.value)">
				<option value=""> Not selected </option>
				<option> Daily </option>
				<option> Weekly </option>
				<option> Monthly</option>
                <option> Quarterly</option>
				<option> Yearly</option>
			</select>
			</div>
		</div>
</div>
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Expected pay day</label>
				<select class="form-control" name="pay_day"  id="pay_day" >
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
				<option>5</option>
				<option>6</option>
				<option>7</option>
				<option>8</option>
				<option>9</option>
				<option>10</option>
				<option>11</option>
				<option>12</option>
				<option>13</option>
				<option>14</option>
				<option>15</option>
				<option>16</option>
				<option>17</option>
				<option>18</option>
				<option>19</option> 
				<option>20</option>
				<option>21</option>
				<option>22</option>
				<option>23</option>
				<option>24</option>
				<option>25</option>
				<option>26</option>
				<option>27</option>
				<option>28</option>
				<option>29</option>
				<option>30</option>
				<option>31</option>
			</select>
			</div>
		</div>
</div>

<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Lease period (Number of years) &nbsp;   </label>
				<input type="text"  placeholder="Optional" class="form-control" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"   onkeypress="return checkIt(event)" name="lease_period"/>
			</div>
		</div>
</div>

</div> 

<div class="row">
<div class="col-md-12">  &nbsp;  </div>	
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Payment Details </font>
				<input type="hidden" id="total_payment" name="total_payment">
		</div>
</div>
	<div class="col-md-12">  &nbsp;  </div>		
</div>

<div class="row" id="pay_details"> 
	 
	 <div class="col-md-12">  &nbsp;  </div>	
	 <!--Payment details embedded here -->
	<div class="col-md-12"> 
		<p> <font color="green"> <strong> Select unit to preview payment details </p> </font> </strong>
	</div>	 
	 
</div> 

<div class="row"> 
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Deposit Details </font>
				<input type="hidden" id="total_deposits" name="total_deposits">
		</div>
</div>
	<div class="col-md-12">  &nbsp;  </div>		
</div>

<div class="row" id="deposit_details"> 
	 
	 <div class="col-md-12">  &nbsp;  </div>	
	 <!--Payment details embedded here -->
	<div class="col-md-12"> 
		<p> <font color="green"> <strong> Select unit to preview deposit details </p> </font> </strong>
	</div>	 
	 
</div>  

 
<div class="row"> 
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Tenant documents Uploads (Id, Passport images e.t.c) </font>
				
		</div>
</div>
		
<div class="col-md-12"> 
  <label for='upload'>  Choose or Drag and drop files here  </label>
	<div  style="border:2px solid grey;color:brown; font-size:18px;min-height:200px">
	<label>  &nbsp;  </label>
	 <input type="hidden" id="no_of_files" name="no_of_files">
        <input id='upload' type="file" style="color:blue;padding:8px;" class="multi" accept="gif|jpg|jpeg|pdf|docx|doc|png|xls|csv" name="userfile[]"  multiple="multiple" />
    </div>
 
</div> 
 
<div class="col-md-12">  &nbsp;  </div>
   
<div class="col-md-3">
<div class="form-group">
		<input type="submit" value="Save Details" id="save" <?=$disabled?> class="btn green"  /> 
</div>
</div>	
<div class="col-md-3">
		<!--<div class="form-group">
				<input type="reset" value="Cancel" class="btn default" />  
		</div>-->
		
</div>
<div class="col-md-3">
&nbsp;
</div>
<div class="col-md-3">
&nbsp;
</div> 

</div>

	</form>
 
<!-- END PERSONAL INFO TAB -->

 
</div>
</div>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
</div>
</div>
<!-- END CONTAINER -->

<!-- responsive -->
        <div id="responsive" class="modal fade" tabindex="-1" data-width="400">
              <div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			<h5><b> Edit Units </b></h5>
			<div class="form-group">

		<p>	<label class="control-label">Category </label>
			<select class="form-control">
				<option>Single Bedroom</option>
				<option>2 Bedroom</option>
				<option>3 Bedroom</option>
				<option>4 Bedroom</option>
				<option>5 Bedroom</option>
			</select>
		</p>
	</div>
			<p>
			<label class="control-label">Total Units </label>
			<input placeholder="2" class="form-control" type="text">
			</p>
			<p>
			<label class="control-label">Number of Baths </label>
			<input  class="form-control" type="text"> </p>
			<p>
                            
			<p>
			<label class="control-label">Full Furnished </label>
			<select class="form-control">
				<option>Yes</option>
				<option>No</option>
			</select>
		</p>
</div>
</div>
</div>
<div class="modal-footer" >
<button type="button" class="btn green"> OK </button>
<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>
 
 <!-- responsive -->
<div id="responsive2" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
	<div class="row">
		<div class="col-md-12">
		<h5><b> Add Category </b></h5>
		<hr/>
		<p>
		<label class="control-label">Category Name</label>
		<input   class="form-control" type="text"> </p>
		<p>
	</div>
	</div>
</div>
<div class="modal-footer" >
<button type="button" class="btn green"> Add </button>
<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>

<div id="success" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Message </b></h5>
				<p id="success_msg">
				   
				</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>

 
  <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	  <form action="<?=base_url();?>tenants/viewTenant" method="post">
	 <div class="modal-header">
 <p style="font-size:18px;color:green">   Add Tenant Message </p> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="">
				   <?php  echo $msg;?>
				</p>
				<input type="hidden" value="<?=$tenant_id?>" name="id">
				</div>
			</div>    
	</div>
	<div class="modal-footer" >
		<center>	
		<?php if($status=="1"){?>
			<button type="submit" class="btn btn-success">View Details</button>
		<?php } ?>	
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		</center>
	</div>
	</form>
</div>

<div id="alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	 
		<div class="modal-content">
			<div class="modal-header"> 
				<h4 class="modal-title">Alert  Message</h4>
			</div>
			<div class="modal-body">
				 <div style="line-height:25px">
				 <p> You have not  subscribed to any package. You can only add two properties and two tenants per property.   </p>   
				 <p> Please purchase a package to add more properties  and tenants </p>   
				 </div>
				<div class="modal-footer">
					<button class='btn green' onclick="window.location='<?=base_url();?>payment/package'" >
					           Subscribe Now
					</button> 
					<button class='btn red' onclick="window.location='<?=base_url();?>property'">
					           Subscribe Later
					</button> 
					 
				</div> 
		</div> 
</div> 
</div> 

<script language="javascript">
var packages="<?=$packages?>"; 
function validateMail() {
     
    var x = $("#Email").val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
     $("#Email").val('');

        return false;
    }
 
} 
function validate()
 {
	 var x=$('input[type="file"]').val();
	$("#no_of_files").val(x.length);
	 var unit_id=$("#unit_id").val(); 
	 if(!unit_id){  $("#success_msg").html("<font size='3' color='red'> Select  Unit with  the  Payment Details. Please add payment details  if you haven't</font>"); $("#success").modal('show'); return false;}
	 var total_payment=$("#total_payment").val();  
	 if(!total_payment){  $("#total_payment").html("<font size='3' color='red'>The selected Unit has no Payment Details. Please add payment details  to proceed</font>"); $("#success").modal('show'); return false;}
 }
 
 
  function getCategory(id){ 
			checkProperty(id); 
	       var prop=$("#property").val();
		   if(!prop){return false;} 
		   $("#category").empty();
			$("#category").append("<option value=''> Select category </option>");		   
		    $.ajax({
			url:'<?=base_url();?>property/getCategory/'+id, 
			type: 'POST', 
			success:function (data)
			{
			var obj = JSON.parse(data); 
			var data = obj.data; 
            for(var i=0; i<data.length; i++)
			{
                    var category = data[i];
					
					$("#category").append("<option value='"+category['property_category_id']+"'> "+category['category_name']+" </option>");
			}
		 }
		});
		return false;
	} 
	
	function checkProperty(id){ 
	if(!id){ $("#category").val(''); document.getElementById('category').disabled = true;  return false;}  
		    $.ajax({
			url:'<?=base_url();?>property/checkProperty/'+id, 
			type: 'POST', 
			success:function (data)
			{ 
				var obj = JSON.parse(data);  			
				$("#car_spaces").val(obj.car_spaces); 
				$("#floors_available").val(obj.total_floors); 
				var units=obj.units_available; 
		     if(parseInt(units,10)<=0)
				{ 
					$("#success_msg").html("<font color='brown'> You currently have  no units left  to allocate tenant. Please select another property or add more units</font>"); 
					$("#category").val("");  
					document.getElementById('category').disabled = true;
					$("#success").modal('toggle');
					//getCategory(id);
					//window.location.replace("<?=base_url()?>tenants/individual");
					return false;
				} 
			else if(parseInt(obj.total_tenants,10)>=2 && packages==0)
				 {  
					  $("#alert").modal({backdrop: "static"}); 
					  $('#alert').modal('show');
					  return false;		   
				 }
			else
				{
					document.getElementById('category').disabled = false;
					document.getElementById('Parking Spaces Allocated').disabled = false; 
					document.getElementById('House No').disabled = false; 
					document.getElementById('Floor No').disabled = false; 
					document.getElementById('category').disabled = false; 
				}
			}
		});
		return false;
	} 
 	
function getPricing(category_id)
{   
	   $("#pay_details").empty();
	   var prop=$("#property").val();  
	   if(!prop){return false;} 
	    $("#unit_id").empty(); 
	    $("#pay_details").empty(); 
		$.ajax({
		url:'<?=base_url();?>property/check_pricing_details/'+prop, 
		type: 'POST',
		data:{'category_id':category_id},		
		success:function (data)
		{
			var obj = JSON.parse(data); 
			var data = obj.data; 
			if(obj.result=="ok")
			{
			var total=0;
			var unit_id="";
			 for(var i=0; i<data.length; i++)
			 {
					 var category = data[i]; 
					$("#pay_details").append("<div class='col-md-4'> <div class='form-group'> <div class='form-group'> <label class='control-label'>"+category['payment_type']+" </label>"+
					" <input type='text' readonly class='form-control' id='item[]' value='"+category['payment_type_value']+"'/>"+
					"</div> </div> </div>");
					total=total+parseInt(category['payment_type_value'],10);
					unit_id=category['unit_id'];
			 }
			 $("#total_payment").val(total);
			 $("#unit_id").val(unit_id);
			 $("#pay_details").append("<div class='col-md-12'><font color='blue'> <b> Total Amount: "+total+" </b> </font> <p> &nbsp; </p></div>");
			getDeposits(unit_id);
			}
			else{  
				 $("#pay_details").html("<div class='col-md-12'>&nbsp;  </div> <div class='col-md-12'><font size='3' color='red'> No payment records found for the selected unit. Please add unit's payment details </font></div> <div class='col-md-12'>&nbsp;  </div>");
			}
			
		}
	});
	return false;
} 
	

function checkUnitAvailability(categorty_id){ 
            
		var prop=$("#property").val();
           if(!categorty_id){  
				$("#pay_details").empty(); $("#pay_details").html("<div class='col-md-12'>  &nbsp;  </div> <div class='col-md-12'><font color='green'> Select Unit category to view payment details</font> </div> <div class='col-md-12'>  &nbsp;  </div>");
				$("#deposit_details").empty(); $("#deposit_details").html("<div class='col-md-12'>  &nbsp;  </div><div class='col-md-12'><font color='green'> Select Unit category to view deposit details </font></div> <div class='col-md-12'>  &nbsp;  </div>");
				$("#total_payment").val();
				$("#unit_id").val(); 
				return false;	
		   }				
		    $.ajax({
			url:'<?=base_url();?>property/checkUnits/'+prop+"/"+categorty_id, 
			type: 'POST', 
			success:function (data)
			{ 
				var obj = JSON.parse(data);  			 
				var units=obj.units_available; 
				 if(parseInt(units,10)<=0)
				 { 
					$("#success_msg").html("<font color='brown'> You currently have  no Units left  to allocate tenant. Please select another category</font>"); 
					$("#category").val("");   
					$("#success").modal('toggle'); 
					 return false;
				 } 
				 else
				 {
					 getPricing(categorty_id);
				 }
			}
		}); 
	} 
 
	
function checkAvailability(input){    			
             var spaces=$("#car_spaces").val(); 
			 if(parseInt(input,10)>parseInt(spaces,10)){  
             $("#success_msg").html("<font color='brown'> You only have <b> "+spaces+"</b> Parking spaces remaining. Please add more parking spaces to the property </font>"); 
			 document.getElementById("Parking Spaces Allocated").value="0";
             $("#success").modal('toggle'); 
			 return false;
			 
			 }
			 else{ return true;}
	} 		
	
	
function checkFloors(input)
	{    			
             var floors=$("#floors_available").val();  
			 if(parseInt(input,10)>parseInt(floors,10))
			 {  
				 $("#success_msg").html("<font color='brown'> You only have  <b> "+floors+"</b> floors in  this property. </font>"); 
				 document.getElementById("Floor No").value="0";
				 $("#success").modal('toggle'); 
				 return false; 
			 }
			else
			{ 
				return true;
			}
	} 		

function getDeposits(unit_id)
{   
	   $("#deposit_details").empty(); 
	   var prop=$("#property").val();  
	   if(!prop){return false;} 
	    $("#deposit_details").empty(); 
		$.ajax({
		url:'<?=base_url();?>property/check_tenant_deposit', 
		type: 'POST',
		data:{'unit_id':unit_id},		
		success:function (data)
		{
			var obj = JSON.parse(data);
			if(obj.result=="ok")
			{  
				var data = obj.data; 
				var total=0; 
				 for(var i=0; i<data.length; i++)
				 {
					var category = data[i]; 
					$("#deposit_details").append("<div class='col-md-4'> <div class='form-group'> <div class='form-group'> <label class='control-label'>"+category['payment_type']+" </label>"+
					" <input type='text' readonly class='form-control'  value='"+category['deposit']+"'/>"+
					"</div> </div> </div>");
					total=total+parseInt(category['deposit'],10); 
				 } 
				 $("#total_deposits").val(total);
				 $("#deposit_details").append("<div class='col-md-12'><font color='blue'> <b> Total Amount: "+total+" </b> </font> <p> &nbsp; </p></div>");
			}
			else
			{  
				 $("#deposit_details").html("<div class='col-md-12'>&nbsp;  </div> <div class='col-md-12'><font size='3' color='red'>  No deposit details   found for the selected unit. Please add unit's payment/deposit details </font></div> <div class='col-md-12'>&nbsp;  </div>");
			}
			
		}
	});
	return false;
} 

$(document).ready(function(){  
	checkPrivilege();
	var tenants="<?=$total_tenants?>";
	var msg="<?php echo $msg;?>";   
	  if(tenants>=4 && packages==0 && msg=="")	 
	  {
		  $("#alert").modal({backdrop: "static"}); 
		  $('#alert').modal('show');
		  return false;
	  }
	 document.getElementById('category').disabled = true;
	document.getElementById('Parking Spaces Allocated').disabled = true; 
	document.getElementById('House No').disabled = true; 
	document.getElementById('Floor No').disabled = true; 
  var saving_success="<?php echo $msg;?>";
 if(!saving_success)
 { }
else{
       $("#data_saving_success").modal('toggle'); 	
}

});

function checkRentFrequency(input)
{    	 
   if(!input){ $("#pay_day").val(1); 	document.getElementById('pay_day').disabled = false;  return false; }
   var tt ="<?=date('m/d/Y')?>"; 
    var date = new Date(tt); 
    var newdate = new Date(date);
	//var someFormattedDate = mm + '/' + dd + '/' + y; 
	//document.getElementById('follow_Date').value = someFormattedDate;
	var day="<?=date('d')?>";
	day=parseInt(day,10);
	if(input==""){ return false;}
	if(input=="Daily"||input=="daily")
	{ 
		newdate.setDate(newdate.getDate() + 1); 
		var dd = newdate.getDate();
		var mm = newdate.getMonth()+1; 
		var y = newdate.getFullYear(); 
		$("#pay_day").val(dd); 
		document.getElementById('pay_day').disabled = true;
	}
	else if(input=="Weekly"||input=="weekly"){
		newdate.setDate(newdate.getDate() + 7); 
		var dd = newdate.getDate();
		var mm = newdate.getMonth()+1; 
		var y = newdate.getFullYear(); 
		//day=parseInt(day,10)+7;
		$("#pay_day").val(dd); 
		document.getElementById('pay_day').disabled = true;
	}	
	else
	{ 
		document.getElementById('pay_day').disabled = false;
	} 	  
} 		


function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/2",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			if(obj.add==0)
			{  
				 document.getElementById('save').disabled = true; 
		      //$("#save_btn").html("<font color='' onclick=\"alert('You have no privilege to add property')\"> Save Details </font>"); 
			} 
		 
		}
	 })
 } 
 	
	
	$(function () {
    $('#datepicker').datepicker({
        format: 'mm/dd/yyyy',
        endDate: '+0d',
        autoclose: true
    });
});
</script>