<?php $bal=0; 
foreach($tenant_details->result() as $row){  }?>
      
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->

<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="index.html">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Tenant</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span> <?=ucfirst(strtolower($row->company_name))?> </span>
	</li>
</ul>
                        <!-- END PAGE BREADCRUMBS -->
<!-- PERSONAL INFO TAB -->
<div class="page-content-inner"> 
<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="color:#ffffff;background:#32c5d2;height:200px">
			<div class="display">
				<div class="number">
				<h4>  <?=ucfirst(strtolower($row->first_name))?> &nbsp; <?=ucfirst(strtolower($row->middle_name))?> &nbsp; <?=ucfirst(strtolower($row->last_name))?></h4>	 
				<h6>  <?=$row->mobile_number?> </h6>
				<span> <?=$row->tenant_email?> </span>
				<h5> <?=$row->property_name?>  </h5>
				</div> 
			</div>
			 
		</div>
	</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:200px">
			<div class="display">
				<div class="number">
					<h3 class="font-red-haze">
						<span data-counter="counterup" data-value="1349"> <?=$row->house_no?> </span>
					</h3>
					<small>House Number</small>
				</div>
				<div class="icon">
					<i class="icon-home"> </i>
				</div>
			</div>
			
			<div class="progress-info">
				<div class="progress">
					<span style="width: 100%;" class="  "> 
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Tenant for  </div>
					<div class="status-number"> <?=$row->lease_period?> years </div>
				</div>
			</div>
		</div>
</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:200px">
			<div class="display">
				<div class="number">
					<h3 class="font-blue-sharp">
						<span data-counter="counterup" data-value="567"> <?=$row->category_name?> </span>
					</h3>
					<small>   Category </small>
				</div>
				<div class="icon">
					<i class="icon-layers"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width:100%" class="">
					 
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Floor </div>
					<div class="status-number"> <?=$row->floor_no?> </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:200px">
			<div class="display">
				<div class="number">
					<h3 class="font-purple-soft">
						<span data-counter="counterup" data-value="276">  
						<?php
					$rent_paid=0; $rent_to_pay=0; 
					foreach($payment_details->result() as $r)
					{
							 $rent_to_pay=$rent_to_pay+$r->payment_type_value; 
					} 
			?>  
			<?php foreach($paid_rent->result() as $pr){
			 	  $rent_paid=$rent_paid+$pr->amount; 
				
			 } 
			 //($rent_to_pay-$rent_paid);
		foreach($current_acc->result() as $acc)
		{
			if($acc->tenant_id==$row->id)
			{
				 $bal=$acc->balance;
			}
		}
		echo $bal;
			 ?>						
					</span>
				</h3>
				<small>  Rent Due </small>
			</div>
				<div class="icon">
					<i class="fa fa-money"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width:100%;" class="">
						 
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Days delayed </div>
					<div class="status-number"> 0 days </div>
				</div>
			</div>
		</div>
	</div>
</div>
 
<div class="portlet light ">		 
<div class="portlet-body">
<div class="tab-content">

<div class="row">
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:5px;">
				<font color="#ffffff"> Business  Details </font>
		</div>		
</div>
<div class="col-md-12">  &nbsp;  </div>		
</div> 
<form action="<?=base_url('tenants/update_Commercial_tenant');?>" enctype="multipart/form-data" method="post" onsubmit="return validate()" >

<div class="row"> 
<div class="col-md-12">
		<div class="form-group">
			<label class="control-label">Company /Business Name</label>
			<input type="text" name="company_name" value="<?=$row->company_name?>"  class="form-control" onkeyUp="validateChar('Business name')" id="Business name"/>
			<input type="hidden"  name="id_no" id="id_no" value="<?=$row->id?>" class="form-control" />
			</div>
</div>

<div class="col-md-4">
			<div class="form-group">
			<label class="control-label">Business Number </label>
			<input type="text"  name="business_no"  value="<?=$row->business_number?>" class="form-control" />
		</div>
</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> Business Activity </label>
			<input type="text" name="business_activity" value="<?=$row->business_activity?>" id="Business activity"onkeyUp="validateChar('Business activity')" class="form-control" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Mobile Number</label>
			<input type="text"  name="mobile_no" value="<?=$row->company_mobile?>" maxlength="12" class="form-control" onkeypress="return checkIt(event)"/>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> Email </label>
			<input type="text" name="email" value="<?=$row->company_email?>"  class="form-control"  />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> Contact Person Full Name </label>
			<input type="text" name="contact_person" value="<?=$row->first_name?>"  onkeyUp="validateChar('Contact person name')" id="Contact person name" class="form-control" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Phone Number </label>
			<input type="text" name="phone_no" value="<?=$row->mobile_number?>"  maxlength="10" class="form-control" onkeypress="return checkIt(event)"/>
		</div>
	</div> 

 
</div>
<div class="row">
	<div class="col-md-12">
			<div class="col-md-12" style="background:#1bb968;padding:5px;">
					<font color="#ffffff"> House/Unit Allocation Details </font>
			</div>			
	</div>
	<div class="col-md-12">  &nbsp;  </div>		
</div> 
 
<div class="row"> 
<div class="col-md-4">
	<div class="form-group">
		<label class="control-label">Allocated Property Name </label>
		   <select name="property" class="form-control" id="property" onChange="getCategory(this.value)">
				<option value="<?=$row->property_id?>"> <?=$x=$row->property_name?>  </option>
				<?php foreach($properties->result() as $row1): ?>
					<?php if($row1->property_name==$row->property_name){continue;}?>
					<option value="<?=$row1->id?>"><?=$row1->property_name?>  </option>
				<?php endforeach;?>
			</select>
	</div>
</div>	
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Unit Category </label> 
			<select class="form-control" name="category" id="category"> 
			<option value="<?=$row->unit_id?>"> <?=$row->category_name?></option>
			<?php foreach($categories->result() as $row2): ?>
					<?php if($row2->category_name==$row->category_name){continue;}?>
					<option value="<?=$row2->unit_id?>"><?=$row2->category_name?>  </option>
				<?php endforeach;?>
			</select>
			
			</div>
		</div>
</div>
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> House Number </label>
				<input type="text"  name="houseno" value="<?=$row->house_no?>" maxlength="8"class="form-control" />
			</div>
		</div>
</div>	 

 
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Floor </label>
				<input type="text" name="floor" value="<?=$row->floor_no?>" onkeypress="return checkIt(event)" class="form-control" />
			</div>
		</div>
</div>
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Parking spaces Allocated </label>
				<input type="hidden"  id="car_spaces" />
				<input type="text" id="Parking Spaces Allocated" name="car_spaces" value="<?=$row->parking_allocated?>" onkeypress="return checkIt(event)" onChange="checkAvailability(this.value)" class="form-control" />
			</div>
		</div>
</div>
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Registration date </label>
				<input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" name="date_registered" placeholder="<?php echo date('d-m-Y');?>" value="<?=$row->date_added?>" class="form-control" />
			</div>
		</div>
</div>  
</div>
<div class="row">
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Rent Details </font>
				
		</div>
</div>
	<div class="col-md-12">  &nbsp;  </div>		
</div>

<div class="row">
 <?php $rent_frequency=$row->rent_frequency;?>
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Rent Frequency </label>
				<select name="rent_frequency" class="form-control" id="rentFrequency" onChange="checkRentFrequency(this.value)">
				<option> Daily </option>
				<option> Weekly </option>
				<option> Monthly</option>
				<option> Yearly</option>
			</select>
			</div>
		</div>
</div>
  
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Expected pay day</label>
				<select name="pay_day" id="pay_day" class="form-control">
				<option><?=$row->expected_pay_day?></option>
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
				<option>5</option>
				<option>6</option>
				<option>7</option>
				<option>8</option>
				<option>9</option>
				<option>10</option>
				<option>11</option>
				<option>12</option>
				<option>13</option>
				<option>14</option>
				<option>15</option>
				<option>16</option>
				<option>17</option>
				<option>18</option>
				<option>19</option> 
				<option>20</option>
				<option>21</option>
				<option>22</option>
				<option>23</option>
				<option>24</option>
				<option>25</option>
				<option>26</option>
				<option>27</option>
				<option>28</option>
				<option>29</option>
				<option>30</option>
				<option>31</option>
			</select>
			</div>
		</div>
</div>

 
<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<label class="control-label"> Lease period (Number of years) </label>
				<input type="text" name="lease_period" value="<?=$row->lease_period?>" onkeypress="return checkIt(event)" class="form-control" />
			</div>
		</div>
</div> 		
</div>
<div class="row">
<div class="col-md-12">  &nbsp;  </div>	
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Payment Details </font>
				
		</div>
</div>
	<div class="col-md-12">  &nbsp;  </div>		
</div>	
<div class="row">
 
<?php $property_category_id="";
foreach($payment_details->result() as $r){
	$property_category_id=$r->property_category_id;
	?>
	<div class="col-md-4">
		<div class="form-group">
			<div class="form-group"> <label class="control-label"> <?=$r->payment_type?></label> 
				<input type="text" readonly  class="form-control" value="<?=$r->payment_type_value?>" /> 
			</div> 
		</div>

	 </div>
<?php } ?>
  
</div>
<div class="row">
<div class="col-md-12">  &nbsp;  </div>	
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Deposit Details </font>
				
		</div>
</div>
	<div class="col-md-12">  &nbsp;  </div>		
</div>
<div class="row">
 
<?php $x=0; 
  foreach($pricing_details->result() as $pricing){ 
if($property_category_id==$pricing->unit_id)
{
	$pricing_details_id=$pricing->id;
	foreach($deposits->result() as $d){
		if($d->pricing_details_id==$pricing_details_id)
		{
			$x++;
?>
	<div class="col-md-4">
		<div class="form-group">
			<div class="form-group"> <label class="control-label"> <?=$pricing->payment_type?></label> 
				<input type="text" readonly class="form-control" value="<?=$d->amount?>" /> 
			</div> 
		</div>

	 </div>
		<?php
		}
	}
 }
 
  }
 
 if($x==0){
	 
	 echo '<div class="col-md-12">
				<div class="form-group">
				<font color="red" size="2"> No deposits details found </font> 
		</div>

	 </div>
		';
 }
?>
  
</div>


<div class="row">
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Tenant documents Uploads (Id, Passport images e.t.c) </font>
				
		</div>
</div>
<div class="col-md-12">  &nbsp;  </div>
<!-- BEGIN PAGE CONTENT INNER --> 
<div class="col-md-8">
	<label for='upload'> Tenant's Documents and files  </label>
<div class="page-content-inner" style="border:2px solid lightgrey;min-height:150px">

 	 
		<table width="100%">
		<?php $i=1; foreach($documents->result() as $doc){?>
		<tr height="40">
		<td>
			<?=$doc->media?>  
		</td>
		<td id="view_<?=$i?>">
			<a href="<?=base_url()?>media/<?=$doc->media?>" target="_blank" class="btn green"> View  </a>  
		</td>
		<td>
			<a href="<?=base_url()?>media/<?=$doc->media?>" class="btn default" download> <span style="color:green;font-size:20px;" class="glyphicon glyphicon-download"></span> Download </a> 
		</td>
		<td>
			<a   onclick="deleteFile('<?php echo $doc->id;?>')" class="btn red"> Delete </a>  
		</td>
	 </tr>
	 <?php $i++;  } ?> 
	 </table>
</div>

</div>
 <div class="col-md-4"> 
   <label for='upload'>Choose or Drag and drop files here</label>
	<div  style="border:2px solid grey;min-height:200px">
        <input type="hidden" id="no_of_files" name="no_of_files">
        <input id='upload' type="file"  class="multi" accept="gif|jpg|pdf|docx|doc|png|xls|csv" name="userfile[]"  multiple="multiple" />
    </div>
 </div>
 
 
<!-- END PAGE CONTENT INNER -->
<div class="col-md-12">  &nbsp;  </div> 
 
<div class="col-md-6"> 
<div class="form-group">
		<input type="submit" value="Save Details" id="done_edit" class="btn green"  <?=$disabled?> /> <a  id="delete" class="btn red" > Remove Tenant </a> 
 </div> 
 </div> 
 <div class="col-md-6">
	 
</div>
 
</div> 
</div>
<?php  // echo form_close(); 
?>
  </form>  
 
<!-- END PERSONAL INFO TAB -->

 
</div>
</div>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->

<!-- responsive -->
        <div id="responsive" class="modal fade" tabindex="-1" data-width="400">
              <div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			<h5><b> Edit Units </b></h5>
			<div class="form-group">

		<p>	<label class="control-label">Category </label>
			<select class="form-control">
				<option>Single Bedroom</option>
				<option>2 Bedroom</option>
				<option>3 Bedroom</option>
				<option>4 Bedroom</option>
				<option>5 Bedroom</option>
			</select>
		</p>
	</div>
			<p>
			<label class="control-label">Total Units </label>
			<input placeholder="2" class="form-control" type="text">
			</p>
			<p>
			<label class="control-label">Number of Baths </label>
			<input placeholder="1" class="form-control" type="text"> </p>
			<p>
                            
			<p>
			<label class="control-label">Full Furnished </label>
			<select class="form-control">
				<option>Yes</option>
				<option>No</option>
			</select>
		</p>
</div>
</div>
</div>
<div class="modal-footer" >
<button type="button" class="btn green"> OK </button>
<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>

<div id="success" class="modal fade" tabindex="-1" data-width="400">
       <div class="modal-body">
			<div class="row">
			<div class="col-md-12"> 
			<h5> <b>  <i class='icon-info'> </i> &nbsp;  Edit Tenant Details Message </b> </h5>
			      <hr/>
				   
					<h5 id="message">      </h5>	 
				 
			</div>
		</div>
		</div>
	<div class="modal-footer" > 
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
<div id="delete_tenant" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Remove Tenant Message</b></h5>
				 <hr/>
					<p>
					   You will not be able to view this tenants later. Proceed?
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="confirm_del" class="btn green" <?=$disabled?>>&nbsp; Yes &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn red">&nbsp; No  &nbsp; </button> 
		</center>
	</div>
</div>

 <div id="responsive2" class="modal fade" tabindex="-1" data-width="400">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b> Delete Confirmation </b></h5>	<hr/>
				<p id="del_status"> Are you sure you want to remove this file? </p>
				<input type="hidden" id="confirm_id">
			</div>
		</div>
	</div>
<div class="modal-footer" >
	<button type="button" class="btn green" id="confirm" <?=$disabled?>> Yes </button>
	<button type="button" data-dismiss="modal"  class="btn red"> No </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>
 
 
 <script type="text/javascript">
 function validate()
 {		
	var x=$('input[type="file"]').val();
	$("#no_of_files").val(x.length); 
 }
 
  //$("#responsive").modal('toggle');
  function getCategory(id){ 
	   var prop=$("#property").val();
	   if(!prop){return false;}
	   //$("#status").empty(); 
		$.ajax({
		url:'<?=base_url();?>property/getCategory/'+id, 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data); 
			var data = obj.data;
			$("#category").empty();
			for(var i=0; i<data.length; i++)
			{
				var category = data[i]; 
				$("#category").append("<option value='"+category['unit_id']+"'> "+category['category_name']+" </option>");
			}
		}
	});
	return false;
	}
	
  $(document).ready(function()
 {   
     var id=$("#property").val();  
	 getCategory(id);
 checkPrivilege();
checkProperty();
	var msg="<?php echo $msg; ?>"; 
	var rentFrequency="<?php echo $rent_frequency;?>"; 
	$("#rentFrequency").val(rentFrequency);
	if(msg==""){
	  //do nothing
	}
	else{
	 //show modal
	 $('#message').html("&nbsp; <font color='green'> <b> "+msg+" </b></font>");
	 $('#success').modal('toggle');   
	}
});


function checkProperty(){ 
var id=$("#property").val(); 
		    $.ajax({
			url:'<?=base_url();?>property/checkProperty/'+id, 
			type: 'POST', 
			success:function (data)
			{ 
			var obj = JSON.parse(data);  			
             $("#car_spaces").val(obj.car_spaces);
              			 
			  return false;
			}
		});
		return false;
	} 
 

	
function checkAvailability(input){    			
             var spaces=$("#car_spaces").val(); 
			 if(parseInt(input,10)>parseInt(spaces,10)){  
             $("#message").html("<font color='brown'> You have only <b> "+spaces+"</b> parking spaces left. Please add more parking spaces to the property </font>"); 
			 document.getElementById("Parking Spaces Allocated").value="<?=$row->parking_allocated?>";
             $("#success").modal('toggle'); 
			 return false;
			 
			 }
			 else{ return true;}
	} 		
 
 
  $("#delete").click(function()
	{ 
			$("#delete_tenant").modal('show');
	 
	});
	$("#confirm").click(function(){
	var tenant_id="<?=$row->id?>";
	var id=$("#confirm").val();
	 $.ajax({
		url:"<?=base_url();?>property/deleteFile/"+id+"/"+tenant_id,
		type:"POST",
		async:false ,
		success:function(data)
		{ 
			var obj = JSON.parse(data); 
			if (obj.result=="ok")
			{
			 
			 window.location.replace("<?=base_url();?>tenants/viewTenant/"+tenant_id);						
			//refreshData();
			}
			else{
				$("#del_status").html(" <font color='red'> Error! File was not removed </font> "); 
				setTimeout(function(){
					$('#responsive2').modal('hide')
				}, 2000);
				return false;
			}
			
		}
		})
	
});

$("#confirm_del").click(function()
	{  
	$("#delete_tenant").modal('hide');
	var id=$("#id_no").val();
	$.ajax({
		"url":"<?=base_url();?>tenants/removeTenant/"+id,
        "type":"post",
		"success":function(data)
		{
			var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
				 location.reload();
				 var property=$("#property").val();
				 window.location.replace("<?=base_url()?>tenants"); 
			 }
			 else
			 {
				$("#message").html("<font color='red'> Error! Tenant Not removed. Try again later</font>");
				$("#success").modal('show');
			 }
		}
		
	})
	
 	});
 
function deleteFile(id)
{ 
		$("#responsive2").modal('toggle');
		$("#confirm").val(id);
		 
}
 
function checkRentFrequency(input)
{    	 
      var day="<?=date('d')?>";
	day=parseInt(day,10);
	if(input==""){ return false;}
	if(input=="Daily"||input=="daily")
	{ 
		day=parseInt(day,10)+1;
		$("#pay_day").val(day); 
		document.getElementById('pay_day').disabled = true;
	}
	else if(input=="Weekly"||input=="weekly"){
		day=parseInt(day,10)+7;
		$("#pay_day").val(day); 
		document.getElementById('pay_day').disabled = true;
	}	
	else
	{ 
		document.getElementById('pay_day').disabled = false;
	} 	  
}

function checkPrivilege()
 { 
	 var i="<?=$i?>";
	$.ajax({
		url:"<?=base_url();?>Auth/checkPrivilege/2",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data);  
			var data = obj.data;  
			 
			if(obj.view==0){ 
			for(var x=1; x<i;x++){   $("#view_"+x).html("<font color='' onclick=\"alert('You have no privilege to view')\"> <a href='#' class='btn green'> View </a></font>"); }
              		  
			}
			 if(obj.edit==0){  
				 document.getElementById('done_edit').disabled = true; 
			}
			if(obj.delete==0){ 
			 
			   document.getElementById('confirm_del').disabled=true;   
			   document.getElementById('confirm').disabled=true;   
			}
		}
	 })
 }
 
</script>