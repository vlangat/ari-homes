 

<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
   
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE CONTENT BODY -->
	<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		 Subscriptions 
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Messages</span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER --> 
<div class="row">
<div class="col-lg-12">
 
</div>

</div>
<div class="page-content-inner">

<div class="inbox">
<div class="row">
 
<div class="col-md-12">
<div class="inbox-body">
<div class="inbox-header">
	<h1 class="pull-left"> <font id="sms_title" size="3"> <font> </h1>
	<input type="hidden" id="out_of" value="10">
	<input type="hidden"  id="start_counter">
	<input type="hidden"  id="total_pages">
	<input type="hidden"  id="heading">
 
</div>
<div class="inbox-content">  <a id="delete" style="float:right"class="btn btn-sm red btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> 
							  <i class="fa fa-trash-o">		</i> Delete 
						</a>
	<div class="btn-group input-actions">
			<a href="javascript:;" id="compose_msg"  data-title="Compose" class="btn green compose-btn btn-block">
			<i class="fa fa-edit"> </i> Schedule New Message
			</a>
						<!--<ul class="dropdown-menu"> 
						   <li>
								<a href="javascript:;" id="delete">
								<i class="fa fa-trash-o">		</i> Delete </a>
							</li>
						</ul>-->
	</div>
	<hr/>
<table class="table table-striped table-hover table-bordered"  id="sample_editable_1" style="font-size:12px;padding:1px;border:1px solid lightgrey" cellpadding="0" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>#</th>
            <th> Property </th>
            <th> Title</th>
            <th> Message</th>
            <th> Date to Sent</th>
            <th> Rent Status</th>
            <th> Status</th> 
            <th> Edit </th> 
            
        </tr>
    </thead>
    <tbody  id="msg_bodyb">
	<?php  $x=1; foreach($messages->result() as $msg){ ?>
	<tr class="<?=$msg->id?>" data-messageid="<?=$msg->id?>" onclick="readMsg('<?=$msg->id?>')">
            <td><?=$x?> </td>
            <td class="inbox-small-cells">
                <input name='checkbox[]' type='checkbox' id='checkbox[]' value='<?=$msg->id?>' class="mail-checkbox" >
			</td>
            
            <td class="view-message hidden-xs"> 
				<?php $name="";  
				foreach($properties->result() as $r)
				{
					if($r->id==$msg->property_id){ echo $name=$r->property_name; break;}else{ 
					echo $name;
					}					
				}
				?> 
			</td>
            <td class="view-message hidden-xs">  <?=$msg->title?> </td>
            <td class="view-message hidden-xs">  <?=$msg->message?> </td>
            <td class="view-message ">  <?=$msg->date_sent?> <?=$msg->time_sent?>   </td>
            <td class="view-message inbox-small-cells">
                 <?=$msg->rent_status?>
            </td> 
            <td class="view-message text-left"> 
				<?php if($msg->status==0){echo 'Pending';}else{ echo 'Sent';}?> 
			</td>
			<td class="view-message text-left"> 
				<a href="#" onclick="editMessage('<?=$msg->id?>')"><i class="fa fa-edit"></i>Edit</a> 
			</td>
         </tr>  
		<?php $x++;  }?>
         
    </tbody>
</table>
<p id="success"> </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>

 <div id="confirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Confirm Deletion</h4>
			</div>
			<div class="modal-body">
				<p>  Do you want to remove the selected Messages? </p>
			</div>
			<div class="modal-footer">
				<button class="btn default" data-dismiss="modal" aria-hidden="true">No</button>
				<button data-dismiss="modal" id="confirmDelete" class="btn blue">Yes</button>
			</div> 
</div>


<div id="compose_sms" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true"  data-width="800" data-height="520">
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	<h4 class="modal-title">Compose New Message</h4>
</div>
<div class="modal-body">
 <input type="hidden" value="" id="msg_id">
    <div>
	<div class="row">
		
	<div class="col-xs-6">
		<label>Property </label> 
        <div> 
		 <select  class='form-control'  data-live-search='true' id='property'>
			 <option value=""> Select Property </option>
				<?php foreach($properties->result() as $row){?>
				<option value="<?=ucfirst(strtolower($row->id))?>">
					<?php $name=$row->property_name;  echo $name;?>
				</option>
				<?php } ?> 
		 </select>
		<p>   </p>		 
        </div>
	</div>
	<div class="col-xs-6">
		<label> Rent Status </label>  
		<select class="form-control" name="status" id="status"> 
			<option value=""> Select </option> 
			<option value="all"> All </option> 
			<option value="unpaid"> Unpaid </option> 
		</select> 
	 	<p>   </p>
	</div>
      <div class="col-xs-12">
	  
	  <div class="form-group">
			<label > Date (Schedule) (dd/mm/yyyy)</label> 
				  <input type="text" placeholder="<?php echo date('m/d/Y')?>" id='date' class="form-control input-xxlarge date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="-0d" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> 
		</div>
		<p>   </p>
		</div>
		<!--<div class="col-xs-6">
			<label > Time: </label> 
			<input type="text" id='time' class="form-control timepicker timepicker-24"  placeholder="<?=date('H:i')?>">
			<p>   </p>		
		</div>-->
		
		<div class="col-xs-6">
		 
			<label > Repeat </label> 
			<div> 
				 <select name="repeat" id="repeat" class="form-control" > 
					<option value="">No</option>			 
					<option value="yes">Yes</option>			 
				 </select> 
				<p>   </p>			
			</div> 
		 </div>
		 <div class="col-xs-6">
		 
		<label > Repeat Frequency </label> 
        <div> 
			 <select name="repeat_frequency" id="repeat_frequency" class="form-control" >
				<option value="">None</option>			 
				<option value="daily">Daily</option>			 
				<option value="weekly">Weekly</option>			 
				<option value="monthly">Monthly</option>	 		 
			 </select> 
			<p>   </p>			
        </div> 
		 </div>
		 
		 <div class="col-xs-12">
			<label > Title: </label> 
			<input type="text" id='title' class="form-control"  value="">
			<p>   </p>		
		</div>
		
		<div class="col-xs-12">
			<div class='inbox-form-group'>
					<label class='control-label'> Message   </label>
					<textarea class='inbox-editor inbox-wysihtml5 form-control' id='text_message' rows=''></textarea>
			</div>
		</div>
		
    </div>   
	<p>   </p>
	  
    <div>
	<p id="counter"> </p> <p id="error"> </p>
    </div> 
<div class="modal-footer">
	<button class='btn green' onclick='send()'>
		<i class='fa fa-check'></i> Save Message </button> 
	<button class="btn default" data-dismiss="modal" aria-hidden="true"> Close </button>
	 
</div>
		 
</div>
</div>

</div> 
<!-- END CONTAINER -->
<script type="text/javascript">

function validate()
{
var chks = document.getElementsByName('checkbox[]');
var hasChecked = false;
for (var i = 0; i < chks.length; i++)
{
if (chks[i].checked)
{
hasChecked = true;
break;
}
}
if (hasChecked == false)
{
//alert("Please select at least one.");
//jAlert('Please select at least one message to perform the action');
alert('Please select at least one message to perform the action');
return false;
}
$("#confirmModal").modal('show');
return true;
}

function discard(){
   $("#to").val('');
   $("#text_message").val('');
   $("#cc").val('');
   $("#subject").val('');
	
}

$("#confirmDelete").click(function(){
				var favorite = [];
				$.each($("input[name='checkbox[]']:checked"), function(){            
					favorite=($(this).val()); 
					deleteMessage(favorite);
				});
	  location.reload();
 });
 
 $("#compose_msg").click(function(){ 
	 $("#msg_id").val(''); $("#property").val(''); $("#repeat").val('');
	 $("#repeat_frequency").val(''); $("#date").val(''); 
	 $("#status").val(''); $("#title").val(''); $("#text_message").val('');
	  $("#compose_sms").modal('show');
  });
 
$("#delete").click(function(){  
validate();

  //return confirm("Are you sure you want to delete the message?");
 
 });

 function deleteMessage(id)
 {
 
	 $.ajax({
			url:"<?=base_url();?>sms/removeMessages/"+id,
			type:"post",
			success: function(data){ 
			var obj = JSON.parse(data);
			if (obj.result=="ok"){ 	 
                //jAlert('There was an error in deleting the messages');
				
			}
			else
			{ 
				jAlert('There was an error in deleting the messages');
			}
	}	
}	
)	
}
 
 function send(){  	
	 
	var id=$("#msg_id").val();
	var property=$("#property").val();
	var repeat=$("#repeat").val();
	var frequency=$("#repeat_frequency").val();
	var date=$("#date").val();
	//var time=$("#time").val();
	var rent_status=$("#status").val();
	var title=$("#title").val();
	var body=$("#text_message").val();  
	 
	if(!property){ 		$("#error").html("<font color='red'>Select property</font>"); $("#property").focus(); return false;}
	if(!rent_status){ 	$("#error").html("<font color='red'>Select rent status</font>"); $("#status").focus(); return false;}
	//if(!repeat){ $("#error").html("<font color='red'>Receipient Number is Required</font>"); $("#to").focus(); return false;}
	//if(!frequency){ $("#error").html("<font color='red'>Receipient Number is Required</font>"); $("#to").focus(); return false;}
	if(!date){ $("#error").html("<font color='red'>Select date</font>"); $("#date").focus(); return false;}
	//if(!time){ $("#error").html("<font color='red'>Select time</font>"); $("#time").focus(); return false;}
	if(!title){ $("#error").html("<font color='red'>Enter message title</font>"); $("#title").focus(); return false;}
	if(!body){ $("#error").html("<font color='red'>Message is empty. Write your message </font>"); $("#text_message").focus(); return false;}
	//if(/^[a-zA-Z0-9-. ]*$/.test(body) == false){ $("#message").focus(); $("#error").html("<font color='red'>Message content   have illegal characters </font>");  return false; } else{$("#error").empty();} 
    $("#error").html("<font color='#006699'> Saving messages... </font>"); 
  
	$.ajax({ 
		url:"<?=base_url();?>sms/scheduled_sms",
		type:"POST",
		async:false,
		data:
		{
			'id':id,  
			'property':property,  
			'status':rent_status,  
			'date':date,  
			//'time':time,  
			'time':'',  
			'title':title,  
			'message':body,  
			'repeat':repeat,  
			'frequency':frequency 
		},
		success:function(data)
		{ 
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				 $("#success").html("<font color='green'> Message saved successfully </font>");
				 $("#text_message").val(''); 
				 location.reload();
			}
			else{
				$("#error").html("<font color='red'> SMS not saved successfully! </font>");
			}
				
		}
	
})
}

function editMessage(id)
{
	$("#error").empty();
$.ajax({
url:"<?=base_url();?>sms/displayMessages/"+id,
type:"post",
success: function(data){ 
var obj = JSON.parse(data);
if (obj.result=="ok"){ 	 
	var data = obj.data;  
   for(var i=0; i<data.length; i++)
   {
	var message = data[i]; 
	var id=message['id'];
	var content=message['message'];
	var property_id=message['property_id'];
	var title=message['title'];
	var rent_status=message['rent_status'];
	//var time=message['time_sent'];
	var subject=message['subject']; 
	var date=message['date_sent']; 
	var repeat=message['repeat']; 
	var frequency=message['frequency']; 
	 
   }		
	 $("#msg_id").val(id); $("#property").val(property_id); $("#repeat").val(repeat);
	 $("#repeat_frequency").val(frequency); $("#date").val(date); 
	 //$("#time").val(time);
	 $("#status").val(rent_status); $("#title").val(title); $("#text_message").val(content);	 
	$("#compose_sms").modal('show');
	//alert(content);
}
}
   })

} 

</script>