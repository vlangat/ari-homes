 <?php 
 $first_name="";
 //foreach($statement->result() as $rows)
  if(is_array($statement)&& count($statement)>0)
  {  
	 foreach($statement->result() as $rows){
	 $tenant_type=$rows->tenant_type; $property_unit_id=$rows->property_unit_id;
	 if($tenant_type=="residential"){ $tenant_name=$rows->last_name." ".$rows->middle_name." ".$first_name=$rows->first_name;}else{ $tenant_name=$rows->company_name;} 
	 $house_no=$rows->house_no; $floor_no=$rows->floor_no; $lease_period=$rows->lease_period; $phone=$rows->mobile_number;  
  
		 
	 }
  }
 else
 {   
	 foreach($tenant_details->result() as $rows)
	 {
		 $tenant_type=$rows->tenant_type; $property_unit_id=$rows->property_unit_id;
		 if($tenant_type=="residential"){ $tenant_name=$rows->last_name." ".$rows->middle_name." ".$first_name=$rows->first_name;}else{ $tenant_name=$rows->company_name;} 
		 $house_no=$rows->house_no; $floor_no=$rows->floor_no; $lease_period=$rows->lease_period; $phone=$rows->mobile_number;  
     } 
 }
 
	
foreach($units_details->result() as $u)
{ 
	 if($property_unit_id==$u->unit_id)
	 { 
		$property_name=$u->property_name;$category_name=$u->category_name; 
	 }
}
if($first_name==""){ $first_name=$tenant_name;}
$name=""; $company_phone_number="";
 foreach($users->result() as $r)
 { 
	 if($r->id==$this->session->userdata('id'))
	 {
	 $name=ucfirst(strtolower($r->first_name))." ".ucfirst(strtolower($r->last_name)); 
	 if($name !=""){

					}
					else
					{
						foreach($company_info->result() as $c)
						{
							 $name=$c->company_name; $company_phone_number=$c->mobile_number;
						} 
					} 
	 }
}  
?>

 <div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Rent </span>
	<i class="fa fa-circle"> </i>
</li>
<li>
	<span> Payment Statement</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  <div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="color:#ffffff;background:#32c5d2;height:150px">
			<div class="display">
				<div class="number">
				<h4>  <?=ucfirst(strtolower($tenant_name))?> </h4>	 
				<span> <?=$email=$rows->tenant_email?> </span>
				<h6>  <?php if($phone==""||$phone=="0"){ $phone=$company_phone_number;}  echo $phone;?>  </h6>
				<h5> <?=$property_name?>  </h5>
				</div> 
			</div>
			 
		</div>
	</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:150px">
			<div class="display">
				<div class="number">
				<!---	<h3 class="font-red-haze">	-->
					<h4 class="font-red-haze">
						<span data-counter="counterup" data-value="1349"> <?=$house_no?> </span>
					</h4>
					<small>House Number</small>
				</div>
				<div class="icon">
					<i class="icon-home"> </i>
				</div>
			</div>
			
			<div class="progress-info">
				<div class="progress">
					<span style="width: 100%;" class="  "> 
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Tenant for  </div>
					<div class="status-number"> <?=$lease_period?> years </div>
				</div>
			</div>
		</div>
</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:150px">
			<div class="display">
				<div class="number">
					<!--<h4 class="font-blue-sharp">-->
					 <h4 class="font-blue-sharp"> 
						<span data-counter="counterup" data-value="567"> <?=$category_name?> </span>
					</h4>
					<small>   Category </small>
				</div>
				<div class="icon">
					<i class="icon-layers"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width:100%" class="">
					 
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Floor </div>
					<div class="status-number"> <?=$floor_no?> </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:150px">
			<div class="display">
				<div class="number">
					<h4 class="font-purple-soft">
						<span data-counter="counterup" data-value="276">   <?php
						$rent_paid=0; $rent_to_pay=0; 
			//	foreach($rent->result() as $r){
				//if($r->tenant_id==$rows->id){ $rent_to_pay=$rent_to_pay+$r->payment_type_value;}
				// }
				
			?>  
			<?php //foreach($paid_rent->result() as $pr){
			 	//if($pr->tenant_id==$rows->id){ $rent_paid=$rent_paid+$pr->amount;}
				
			// }
			$rent=0;
			  foreach($rent_due->result() as $rt){
			 	 $rent=$rt->balance;
				
			 }
			 echo  $rent;//($rent_to_pay-$rent_paid);
						
						?> </span>
					</h4>
					<small>  Rent Due </small>
				</div>
				<div class="icon">
					<i class="fa fa-money"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width:100%;" class="">
						 
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Days delayed </div>
					<div class="status-number"> 0 days </div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title">
		 
<form action="<?=base_url();?>rent/statement/<?=$id?>" method="post" onsubmit="return validate()">	 
<div class="row">
	<div class="col-md-4">  
		<div class="form-group">    
				<label class="control-label col-md-4">From </label>
				 <input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="mm/dd/yyyy" value="<?=date("m/d/Y")?>" id="datepicker"  placeholder=""  name="from" />
			
					<!-- /input-group -->
				</div> 
			<label id="error1">					</label>  
	</div> 
	<div class="col-md-4">  
		<div class="form-group">    
				<label class="control-label col-md-4">To </label>
				 <input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="mm/dd/yyyy"  id="date_picker"  value="<?=date("m/d/Y")?>"  placeholder=""  name="to" />
		</div> 
			<label id="error1">					</label>  
	</div> 	 		 
	<div class="col-md-4"> 
	<input type="hidden" value="<?=$id?>" name="id"/>
	<label class="control-label col-md-12"> &nbsp;&nbsp; </label>
		<div class="form-group">   
				 <button type="submit" class="btn green"> Search</button>
		</div>
	</div> 
</div> 
</form>
  
<hr/>
 <!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
	
<div id="prnt">
 
<table  border="0"  class="table table-striped table-hover" style="font-size:12px"  cellpadding="0" cellspacing="0" width="100%">
<thead>
	<tr> 
		<th align="left"> Date </th> 
		<th align="left"> Receipt No </th>  
		<th align="left"> Pay Mode </th>  
		<th align="left"> Mode No </th>  
		<th align="left"> Description </th>  
		<th align="left"> Debit </th>  
		<th align="left"> Credit </th>  
		<th align="left"> Balance </th> 
	</tr>
</thead>
<tbody> 
<?php 
		$credit=0; $debit=0; $curr_bal=0; $total=0;
		foreach($statement->result() as $row){
		if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
		if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
		   
	?>

	<tr>
		  <td> <?=$row->date_paid?>    </td>  <td> <?=$row->receipt_no?> </td> <td> <?=$row->payment_mode?> </td> <td> <?=$row->payment_mode_code?> </td> <td> <?=$row->description?>  </td> <td> <?php if($row->type=="d"){echo $row->amount;}else{ echo '-';}?> </td>  <td> <?php if($row->type=="c"){echo $row->amount;}else{ echo '-';}?> </td>  <td> <?php   if(($debit-$credit)<0){echo 0-$curr_bal;}else { echo $curr_bal; }?> </td> 
	</tr>
	<!--<tr><td colspan="8"> &nbsp; </td></tr>-->
	
<?php   }?>	
</tbody>
</table>
<p>  </p>
<center> <p style="font-size:12px"><b>  Closing Balance  </b>  Ksh: <?=$debit-$credit; ?> </p> </center>
</div>

 <a  href="javascript:;" class="btn green"  id="btn_receipt" >  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
 <a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  
  
		 
</div> 
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div> 

<div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> Venit Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
  
<!-- END CONTAINER -->

 <div id="sendMail" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Email Tenant Message </b></h5>
				<hr/>
					<p>
					    This Statement shall be sent as an Email to <font id="email_id"> </font>
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="send_email" class="btn green">&nbsp; Yes &nbsp; </button>
			 <button type="button" data-dismiss="modal" class="btn default">&nbsp; Close  &nbsp; </button> 
		</center>
	</div>
</div>


<script language="javascript">
 
 	var property="<?=$property_name?>";
	var tenant="<?=$tenant_name?>";
	var l_name="<?=$name?>";
	var email="<?=$email?>";
	var phone="<?=$phone?>";
	var h="<center> <h4><font color='#000'> RENT STATEMENT </font></h4><hr/></center><table style='padding:0px;border:0px solid #000' width='100%' border='0' id='table_header'><tr><td align='left'><ul style='font-size:14px; list-style-type:none'> <li> Tenant Name: "+tenant+" </li> <li>&nbsp; </li><li> Email: "+email+" </li><li>&nbsp; </li><li> Phone: "+phone+" </li> </ul></td><td align='right'> <ul style='font-size:14px; list-style-type:none'> <li> Landlord: "+l_name+" </li> <li>&nbsp; </li> <li> Property: "+property+" </li><li> &nbsp; </li> </ul> </td></tr></table><p>&nbsp;</p>"; 
 
 $("#btn_receipt").click( function () {

	var divContents = $("#prnt").html();
	var printWindow = window.open('', '', 'height=400,width=800');
	printWindow.document.write('<html><head><title> Rent Statement </title>');
	printWindow.document.write('<style>table, th, td { border-bottom: 1px solid #ddd;} </style></head><body>'+h);
	printWindow.document.write(divContents);
	printWindow.document.write('</body></html>');
	printWindow.document.close();
	printWindow.print();
        });
		 
        var email="<?=$email?>";  
        var fname="<?=$first_name?>";  
        var from="<?=$name?>"; 
		
		$("#btn_email").click( function ()
		{ 
			if(!email)
			{ 
					alert('Email address is empty! Please add tenant email first');
					return false;
			}
		    	$("#error").html("");
		$("#email_id").html(fname+" <u> "+email+" </u>");
		 document.getElementById('send_email').disabled = false;
			$("#sendMail").modal('show');
		});
		
		$("#send_email").click( function () {  
		var divContents = $("#prnt").html(); 
        document.getElementById('send_email').disabled = true;
		$("#error").html("<font color='#006699'> Sending email...please wait</font>");
				
		$.ajax({
			//'url':"<?=base_url();?>payment/sendRentStatement",
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Rent Statement", 
				'body':h+" "+divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{  
					$("#error").html("<font color='green'> Rent Statement sent   successfully</font>");
					 setTimeout(function()
						{ 
							$("#error").empty();
							$("#sendMail").modal('hide');
						}, 2500);
				}
				else
				{  
					$("#error").html("<font color='red'> Statement   not sent to   <u>"+email+"</u></font>");
					//$("#success").modal('show');
					 document.getElementById('send_email').disabled = false;
				}
				
			}
			
 })
 
        });
		
		
		$(document).ready(function () {
        var today = new Date();
        $('#date_picker').datepicker({
            format: 'mm/dd/yyyy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('#date_picker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
	
	$(function () {
    $('#datepicker').datepicker({
        format: 'mm/dd/yyyy',
        endDate: '+0d',
        autoclose: true
    });
});
</script>
