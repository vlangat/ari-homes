<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN HEADER -->      
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Tenants</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">All Tenant</a>
		<i class="fa fa-circle"></i>
	</li> 
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet light portlet-fit ">
	<div class="portlet-body">
	<div class="row"> 
 
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
		<font color="#ffffff"> All Tenants </font>
	</div>
 
<div class="col-md-12">  &nbsp;  </div>	
</div>
<form name="form1" method="post" action="<?=site_url();?>tenants/viewTenant" onSubmit="return validate();">
<input type="submit" class="btn green" id="view_tenant" value="View /Edit Tenants Details"> 
<hr/>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr>
	<th> # </th>
	<th> Tenant  </th>
	<th> Property </th>
	<th> Unit Category </th>
	<th> Room No </th>
	<th> Floor </th>
	<th> Mobile Number </th>
	<th> Rent Due </th>
	<th> Statement </th>
	</tr>
</thead>
<tbody>
<?php $bal=0; foreach($tenants->result() as $row):?>
	<tr>
		<td>  <input name="checkbox[]" type="checkbox" id="checkbox[]" value="<?=$row->id; ?>"></td>
		<td><?php if($row->tenant_type=="residential"){?> <?=$row->first_name?> &nbsp; <?=$row->middle_name?> &nbsp; <?=$row->last_name?> <?php }else {?><?=$row->company_name?><?php } ?></td>
		<td> <?=$row->property_name?>	</td>
		<td> <?=$row->category_name?>	</td>
		<td> <?=$row->house_no?> 			</td>
		<td class="center"> <?=$row->floor_no?></td> 
		<td> <?=$row->mobile_number?> </td>
		<td> <?php 
		foreach($current_acc->result() as $acc)
		{
			if($acc->tenant_id==$row->id)
			{
				 $bal=$acc->balance;
			}
		}
		echo $bal;
		$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
		$enct =base64_encode(do_hash($row->id . $salt,'sha512') . $salt);
			?> 
		</td>
		<td> 
			<a data-toggle="modal" id="payment_history"  href="<?=base_url();?>rent/statement/<?=$enct?>" class="btn green"> View Statement</a>
		</td>
	</tr> 
	 
<?php endforeach;?>
 
</tbody>
</table>
<!-- <a data-toggle="modal"    class="btn red"  id="collect" href="" > Collect Rent </a>-->
</form>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
</div>
<!-- END CONTAINER -->
 <!-- RESPONSIVE MODAL -->
<div id="rent_collection" class="modal fade" tabindex="-1" aria-hidden="true"  data-width="400">
  
	<div class="modal-header" >
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <font size="3" color="#006699"> <b> Rent Collection Details </b> </font></h4>
		  <span id="rent_for">  </font>
	</div>
<div class="modal-body">
<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
<div class="row">
	
<div class="col-md-12"> 
		<p>
			<label class="control-label"> Amount (KES) <font color='red'> * </font> </label> 
			<input   class="form-control"   type="text" id="amount" name="amount"> 
			<input   class="form-control"   type="hidden" id="id_no"> 
			<input   class="form-control"   type="hidden" id="name"> 
			<input   class="form-control"   type="hidden" id="property_name"> 
			<input   class="form-control"   type="hidden" id="floor_no"> 
			<input   class="form-control"   type="hidden" id="house_no">			
		</p>
	<p> 
		<label class="control-label">Payment Method  <font color='red'> * </font></label>
		<select class="form-control" name="mode" id="mode"> 
			<option> Cash </option>
			<option> Mpesa </option>
			<option> Cheque </option>
			<option> Bank Receipt </option>
		</select>
	</p>
  
		<p>
		<label class="control-label">Mpesa, Cheque, Bank receipt Number  <font color='red'> * </font> </label>
		<input   class="form-control" type="text" id="transaction_no" name="transaction_no"/> </p>
		<p>
 
		<p>
		<label class="control-label">Paid on</label>
		<input   class="form-control" type="text" id="date" placeholder="<?php echo date('Y-m-d');?>" value="<?php echo date('Y-m-d');?>" name="paid_on"> </p>
		<p>
 
		<p>
		<label class="control-label"> Description </label>
		 
		<textarea class="form-control" id="description" name="description" >  </textarea>
		</p>
</div>
</div>  <span id="status_msg">   </span>
</div>
<div class="modal-footer" >
		<input type="submit" class="btn green" id="save_data" value="Save">
		<button type="submit" class="btn red"> Save &amp; Print Receipt </button>
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div> 
</div>
</div>
 <!---END OF RESPONSIVE MODAL-->
 
 
<script language="javascript">
function validate()
{	
var formobj = document.forms[0];
var counter = 0;
for (var j = 0; j < formobj.elements.length; j++)
{
    if (formobj.elements[j].type == "checkbox")
    {
        if (formobj.elements[j].checked)
        {
            counter++;
        }
    }       
}
if(counter==0){  
    alert("Please select at least one.");
	return false;
 }
else if(counter>1)
{
 alert("Please select one tenant.");
	return false;	
}

	/*var chks = document.getElementsByName('checkbox[]');
	var hasChecked = false;
	for (var i = 0; i < chks.length; i++)
	{
		if (chks[i].checked)
		{
		hasChecked = true;
		break;
	}
	}
	if (hasChecked == false)
	{
	alert("Please select at least one.");
	return false;
	}*/
	 
	return true;
}


$(document).ready(function(){
 checkPrivilege();
/*var table = $('#sample_editable_1').DataTable();
table.destroy();
var table = $('#sample_editable_1').DataTable();
$('.dataTables_filter input').unbind().keyup(function(e) {
    var value = $(this).val();
	if(/^[a-zA-Z0-9- ]*$/.test(value) == false)
   {$(this).val('');
	  table.search('').draw();  
   }
   else
   {
	table.search(value).draw();   
   }
          
});*/
	
	$("#collect").click(function(){ 
	validate();

	var id = [];
	$.each($("input[name='checkbox[]']:checked"), function(){            
	id=($(this).val());
	}); 
	 
		$.ajax({
		"url":"<?=base_url();?>tenants/getTennantDetails/"+id,
        "type":"post",
		"success":function(data)
			{
				var obj = JSON.parse(data); 
				var data = obj.data;
				for(var i=0; i<data.length; i++)
				{
					var tennant = data[i]; 
					$("#id_no").val(id);
					var name=tennant['first_name']+" "+tennant['middle_name']+" "+tennant['last_name'];
					var property=tennant['property_name'];
					var house=tennant['house_no'];
					var floor=tennant['floor_no'];
					$("#property_name").val(property);   
					$("#house_no").val(house);   
					$("#floor_no").val(floor);
					$("#rent_for").html(name); 
                                        if(tennant['first_name']||tennant['last_name']||tennant['middle_name']){  }else{name=tennant['company_name'];   $("#rent_for").html(name);}  
					$("#name").val(name); 

 
				}
 
				 if(id==""){return false;}else{
				        $("#rent_collection").modal('show');
                                     }
			}
	});
		
 
}); 	

 $("#save_data").click(function(){  
	 var id=$("#id_no").val();
	 var name=$("#name").val(); 
	 var amount=$("#amount").val(); 
	 var mode=$("#mode").val(); 
	 var transaction_no=$("#transaction_no").val(); 
	 var property_name=$("#property_name").val(); 
	 var floor_no=$("#floor_no").val(); 
	 var house_no=$("#house_no").val();
	 var business_no=0; 
	 var paid_on=$("#date").val();
	 var description=$("#description").val();
	 if(! amount || ! transaction_no || ! mode){  $("#status_msg").html("<font color='red'> Fill in all the required inputs </font>");  return false;}
	 $("#status_msg").html("<font color='blue'> Updating....</font>");
	$.ajax(
	{
		url:"<?=base_url();?>rent/payment/",
		type:"POST",
		async:false,
		data:
		{
		'id':id,
		'name':name,
		'transaction_no':transaction_no,
		'business_no':business_no,
		'property_name':property_name,
		'house_no':house_no,
		'floor_no':floor_no,
		'amount':amount,
		'mode':mode,
		'paid_on':paid_on,
		'description':description
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 { 
                          $("#transaction_no").val('');
		           $("#amount").val('');
		           $("#description").val('');
					 $("#status_msg").html("<i class='fa fa-check'> </i><font color='green'> Changes Saved </font>");
					 setTimeout(function(){
								 $('#rent_collection').modal('hide');
                                                                  $("#status_msg").empty();
                      }, 2000); 
			 }
			 else
			 {
				 //$("#status_msg").html(" <font color='red'> Not updated. You have made no changes to save </font>");
			 }
		}
		
	})
 });
 
 
});
  
  
  function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>Auth/checkPrivilege/2",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			 
			if(obj.view==0){ 
				document.getElementById('view_tenant').disabled = true;  
			}
			 
		}
	 })
 } 
 
function getChecked(){ 
 var formobj = document.forms[0];

var counter = 0;
for (var j = 0; j < formobj.elements.length; j++)
{
    if (formobj.elements[j].type == "checkbox")
    {
        if (formobj.elements[j].checked)
        {
            counter++;
        }
    }       
}

alert('Total Checked = ' + counter);
}
</script> 