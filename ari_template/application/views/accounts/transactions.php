		<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
	   
		</div>
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE CONTENT BODY -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMBS -->
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="<?=base_url();?>"> Home </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						 Subscription 
						<i class="fa fa-circle"></i>
					</li>  				
					<li>
					My Transactions<span>  </span>
					</li> 
			</ul>
		
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="inbox">
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div> 
<div class="row">
	 
<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong> &nbsp; Recent Transactions  </strong> </font> 
	   </div>
	   <div class="col-md-12">  &nbsp;  </div>	
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit " style="min-height:400px;">
	<div class="portlet-body">
	 
	 <table   style="font-family:verdana" class="table table-striped table-hover table-bordered" id="sample_editable_1">
		<thead>
			<tr>  <th> # </th> <th> Date </th>  <th> Payment Method </th> <th> Transaction No </th> <th> Details </th>  <th> Amount  </th>  <th> Credit </th>  <th>View Receipt</th>  </tr>
		</thead>
	  <tbody>
	  <?php
	$amount=0; $total=0; $count=1; $invoice_no1=""; $package_type=""; $credit=0;
	  foreach($subscribers->result() as $rows)
		{  
             $package_id=$rows->package_id;
             $id=$rows->id;
             $receipt=$rows->payment_method_code;
             $payment_method=$rows->payment_method; $invoice_no=$rows->invoice_no; $amount=$rows->paid_amount;
             foreach($subscriber_info->result() as $r)
			 {
				 if($rows->user_info_id==$r->id)
					{ 
						$name=$r->first_name." ".$r->middle_name." ".$r->last_name; 
					}						
			 }	   

			foreach($packages->result() as $p)
				 {
					 if($package_id==$p->id)
						{ 
							$package_type=$p->package_type; 
						}						
				 }	
 
 //if($receipt==$invoice_no1){ continue; }	

				$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
				$enct =base64_encode(do_hash($id . $salt,'sha512') . $salt);			 
		 ?>
		 
	 <tr>
		<td><?=$count?></td> <td> <?=$rows->date_paid?></td>  <td> <?=$payment_method?> </td>   <td> <?=$receipt?> </td> <td> <?=$package_type?> </td>   <td>  <?=$amount=$rows->paid_amount?> </td> <td><?=$rows->credit?> </td> <td><a href="<?=base_url();?>payment/invoice/<?=$enct?>" style="text-decoration:none;"> <i class="fa fa-file-o"> </i> View Receipt </a></td> 
	 </tr>
	 
	 <?php  $count++; $total=$total+$amount;
	 $invoice_no1=$receipt;
			}?>
	 
	 </tbody>
	 </table>
	 <p> </p>
	 <table width="100%">
		 <tr> <td colspan="6"><td></tr>
		 <tr>
			<td>  <b> Current Balance: KES.  <font color="green"><?=$balance;?> </font></b> </td>
			<td colspan="5" align="right">
				<b> Total Amount: KES. <?php echo '<font color="blue">'.$total;?></font></b> <br/> 
			</td> 
		 </tr>
	 </table>
	  
</div> 
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
          
        </div>
        <!-- END CONTAINER -->
 
    </body>

</html>

<script language="javascript">
$(document).ready(function()
{ 
$("#save").click(function()
	{   
	 
		var fname=	$("#fname").val();
		var lname=	$("#lname").val();
		var email=	$("#email").val();
        var company=$("#company").val();	
        var phone=$("#phone").val();	
        var alt_phone=$("#alt_phone").val();	
        var national_id=$("#national_id").val();	
        var kra=$("#kra").val();	
        if(!(email)||!(phone)||!(kra)||!(national_id)){ $("#error_message").html("<font color='red'> Fill all the required inputs </font>"); return false; }		
	    $("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/addPartner/",
		 type:"POST",
		 async:false,
		 data:{
			 'fname':fname,
			 'lname':lname,
			 'company':company,
			 'email':email,
			 'phone':phone,
			 'alt_phone':alt_phone,
			 'kra':kra,
			 'id':national_id
		 },
		 success:function(data)
		 { 
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 //window.location.replace("<?=base_url()?>admin/home");
				$("#error_message").html(" <font color='green'> <i class='fa fa-check'></i> Partner registered successfully </font>");
				setTimeout(function(){
			  location.reload();
			 },3000);			  
			 }
			 else
			 {
				$("#error_message").empty();  
				$("#error_message").html(" <font color='red'><i class='fa fa-close'></i>  Partner Not registered successfully </font>");
				return false;
			 }
		 }
		  }
		 )	  
});

});


 
 $("#btn_receipt").click( function () {
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Receipt </title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }); 

</script>