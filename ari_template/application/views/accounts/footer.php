<script type="text/javascript">

$(document).ready (function(){  
	 
 $("#hide").removeClass("hidden");	
// $("#scroll").removeClass("hidden");
//scroll(); 
}); 
</script> 
<!-- BEGIN FOOTER -->
 <p>  &nbsp; </p>
 <link href="<?=base_url();?>css/chat2.css" rel="stylesheet" type="text/css" />
  
   <div class="col-md-5">
   
            <div id="hide" class="panel panel-primary" style="display:none;position:fixed;top:38%;z-index:5;height:600px;bottom:0%;left:70%;right:2%;">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> Chat
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle"  id="minimize" title="minimize" data-toggle="dropdown">
                            <span  class="glyphicon glyphicon-minus"></span>
                        </button> 
                    </div> 
                </div>
                      
                <div class="panel-body msg_container_base" style="background:#fff" id="received">
				
         
                </div>
              <div class="panel-footer">
                    <div class="input-group">
                        <textarea rows='2' cols='50' id="message"  class="form-control input-sm" placeholder="Type your message here..." /></textarea>
                         &nbsp;  <span class="input-group-btn">
                           <input type="hidden" class="btn btn-warning btn-large" value="Send" id="submit">
                                 
                        </span> 
						<input id="counter" value="0" type="hidden"/>
                    </div>
                </div> 
      </div>	
</div>
	 
	
	      
<div  class="float">
	<a href="javascript:;" style="font-family:Times New Roman;background-color:#0C9;" class="btn btn-circle btn-lg green"> 
	 Need help? Chat with us! 
	 <i class="fa fa-user"></i>
	 </a>	
</div>

<!-- BEGIN INNER FOOTER -->
<div class="page-footer">
<div class="container"> ARI Homes &copy;  <?php echo date("Y");?> 
<h5> Phone: +254 725 992 355 / +254 736 822 568 &nbsp;  Email: <a href="mailto:support@ari.co.ke"> support@ari.co.ke </a> </h5>
</div>
</div>
<div class="scroll-to-top">
<i class="icon-arrow-up"></i>
</div>
	<!-- END INNER FOOTER -->
	<!-- END FOOTER -->
    <!--[if lt IE 9]> 
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url();?>multifiles/prism.js"></script>
	<script src='<?=base_url();?>multifiles/jquery.form.js' type="text/javascript" language="javascript"></script>
	<script src='<?=base_url();?>multifiles/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
	<script src='<?=base_url();?>multifiles/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
     <script>
  /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1942730-1', 'fyneworks.com');
  ga('send', 'pageview');*/

</script> 
<script type="text/javascript">
/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-1942730-1']);
_gaq.push(['_setDomainName', 'fyneworks.com']);
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();*/
</script>
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
       <!-- END THEME LAYOUT SCRIPTS -->
		<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
		 
        <!-- BEGIN PAGE LEVEL SCRIPTS --> 
		<script src="<?=base_url();?>template/theme/assets/pages/scripts/table-datatables-editable.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>template/theme/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
         
		<script src="<?=base_url();?>template/js/dropzone.js" type="text/javascript"></script>
		<script src="<?=base_url();?>template/js/dropzone2.js" type="text/javascript"></script>
       	     <script src="<?=base_url();?>template/theme/assets/global/plugins/moment.min.js" type="text/javascript"></script>
		 <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
		
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url();?>template/theme/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
		<!--Begin Footer items for To do--> 
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript">
        </script>
		
	 
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->   
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
         <!-- BEGIN THEME LAYOUT SCRIPTS --> 
    </body>

</html>


<script type="text/javascript">
$('.datepicker').datepicker({
    startDate: '-3d'
});

var request_timestamp = 0;

var setCookie = function(key, value) {
	var expires = new Date();
	expires.setTime(expires.getTime() + (5 * 60 * 1000));
	document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

var getCookie = function(key) {
	var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
	return keyValue ? keyValue[2] : null;
}

var guid = function() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

if(getCookie('user_guid') == null || typeof(getCookie('user_guid')) == 'undefined'){
	var user_guid = guid();
	setCookie('user_guid', user_guid);
}


// https://gist.github.com/kmaida/6045266
var parseTimestamp = function(timestamp) {
	var d = new Date( timestamp * 1000 ), // milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		timeString;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}

	timeString = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
		
	return timeString;
}

var sendChat = function (message, callback) {
	$.getJSON('<?php echo base_url(); ?>api/send_message?message=' + message + '&nickname=' + $('#nickname').val() + '&guid=' + getCookie('user_guid'), function (data){
		callback();
	});
}

var append_chat_data = function (chat_data) {
	chat_data.forEach(function (data) {
		var is_me = "<?=$this->session->userdata('email');?>"; //data.guid == getCookie('user_guid');
		var photo="me.jpg";
		var conversation_time=parseTimestamp(data.timestamp);
		if(data.chatType ==1){
			var html = '<div class="row msg_container base_receive">'
                       // +'<div class="col-md-2 col-xs-2 avatar">'
					   //+'<img src="<?=base_url();?>images/me.jpg" class="img-responsive ">'
					   //+'</div>'
                       +' <div class="col-md-10 col-xs-10">'
                       +'  <div class="messages msg_receive" style="background:beige">'
                       +'<p> '+data.message+'</p>'
                        +'<time datetime="2009-11-13T20:00">'+conversation_time+'</time>'
                        +'</div> </div> </div>';
		}else{
		  photo="ARI_Homes_Logo.png";
			var html = '<div class="row msg_container base_sent">' 
                       +' <div class="col-xs-10 col-md-10">'
                       +' <div class="messages msg_sent" style="background:whitesmoke">'
                       +' '+data.message 
                       +'<time datetime="2009-11-13T20:00">'+conversation_time+'</time>'
                       +'</div>'
                       +'</div>'
					   +'<div   style="">'
                       +'<img src="<?=base_url();?>images/login/ARI_Homes_Logo.png" style="" height="40" width="40" class="img-circle">'
                       +' </div>'
					   +'</div>';
		}
		
		$("#received").html( $("#received").html() + html);
		$("#time").html(conversation_time);
	});
  
	//$('#received').animate({ scrollTop: $('#received').height()}, 1000);
}

	var update_chats = function (){
	if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0)
	{
		var offset = 60*15; // 15min 
		request_timestamp = parseInt( Date.now() / 1000 - offset );
	}
	var count =$("#counter").val(); 
	if(parseInt(count) ==0)
	{  

$.getJSON('<?php echo base_url(); ?>api/get_prev_messages?timestamp=' + request_timestamp, function (data){
		append_chat_data(data);	
			 
		var newIndex = data.length-1;
		if(typeof(data[newIndex]) != 'undefined'){
		request_timestamp = data[newIndex].timestamp;
			}
		}); 
	}
	else{  
		$.getJSON('<?php echo base_url(); ?>api/get_messages?timestamp=' + request_timestamp, function (data){
		append_chat_data(data);	
			 scroll();
		var newIndex = data.length-1;
		if(typeof(data[newIndex]) != 'undefined'){
		request_timestamp = data[newIndex].timestamp;
			}
		}); 
   }
	
}

$('#minimize').click(function (e) {
$("#scroll").hide();  
$("#hide").hide();
$(".float").show(); 	
});

$('.float').click(function (e){
	$(".float").hide(); 
	 
	$("#hide").show(); 
});

$('#submit').click(function (e) {
	e.preventDefault(); 
		var msg=$('#message').val();
		if(!msg){ $('#message').focus(); return false;}
	var $field = $('#message');
	var data = $field.val();

	$field.addClass('disabled').attr('disabled', 'disabled');
	sendChat(data, function (){
		$field.val('').removeClass('disabled').removeAttr('disabled');
	});
});

$('#message').keyup(function (e) {
	if (e.which == 13) {
		var msg=$('#message').val();
		//$("#counter").val(0);
		if(!msg){ $('#message').focus(); return false;}
		$('#submit').trigger('click');
		// $this.parents('.panel').find('.panel-body').slideDown();
		scroll();
	}
});


/*setInterval(function (){
	var tokenId = "<?=$this->session->userdata('tokenId');?>"; 
	update_chats(); 
//	if(!tokenId){ $("#counter").val(5);} else{ $("#counter").val(0); }  
 $("#counter").val(5);
	scroll();
}, 1500);
*/

setInterval(function (){
	  update_chats();
	  $("#counter").val(5);
	//scroll(); 
	 //$this.parents('.panel').find('.panel-body').slideDown(); 
}, 2000);

 function scroll()
  {
		var myDiv = document.getElementById("received");
		myDiv.scrollTop = myDiv.scrollHeight; 
		 //alternative you can use this to make scroll bar always at bottom
		 //$("#scroll").animate({ scrollTop: $(document).height() }, "fast");
  }
  
 
</script>