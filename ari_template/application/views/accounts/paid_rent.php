<?php 
	$paid=0;
	$expected_pay=0;
	//$expected_pay2=0;
	$arrears=0;
	//$arrears2=0;
	 $rent_not_paid=0;
 foreach($unpaid_rent->result() as $r){ $rent_not_paid=$rent_not_paid+$r->balance;}
	 foreach($paid_rent->result()  as $row)
	 { 
		 $month=date("m",strtotime($row->date_paid));
		 if($month==date("m"))
			{ 
				$paid=$paid+$row->amount;
			}
	 }
	foreach($expected_rent->result()  as $r)
	{
		foreach($tenant_details->result()  as $t)
			{
				 if($r->tenant_id==$t->id)
				 { 
					 $mth=date("m",strtotime($r->date_paid));
					 if($mth==date("m"))
						{ 
							$expected_pay=$expected_pay+$r->amount; 
						}
				 }
			}
	} 
	
	$rent_not_paid=$expected_pay-$paid;
?>
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#"> Rent </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Paid Rent</span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value=""> <?php echo $expected_pay;?> </span>
				<small class="font-green-sharp"> </small>
			</h3>
			<small> <?php echo date("M");?> Expected Rent </small>
		</div>
		<div class="icon">
			<i class="fa fa-money" style="color:#006699"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php $e=$expected_pay; if($expected_pay==0){$e=1;}  echo $pr=$paid/$e*100;?>%;" class="progress-bar progress-bar-success green-sharp">
				<!--<span class="sr-only">70% Occupation</span>-->
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Collection </div>
			<div class="status-number"> <?php $e=$expected_pay; if($expected_pay==0){$e=1; } echo $pr=round($paid/$e*100,0); if($pr>100){ echo '100';} ?> % </div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value=""> <?php echo $paid;?> </span>
				<small class="font-green-sharp"> </small>
			</h3>
			<small> <?php echo date("M");?> Rent Paid </small>
		</div>
		<div class="icon">
			<i class="fa fa-money" style="color:#006699"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php $e=$expected_pay; if($expected_pay==0){$e=1;}  echo $pr=$paid/$e*100;?>%;" class="progress-bar progress-bar-success green-sharp">
				<!--<span class="sr-only">70% Occupation</span>-->
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Collection </div>
			<div class="status-number"> <?php $e=$expected_pay; if($expected_pay==0){$e=1; } echo $pr=round($paid/$e*100,0); if($pr>100){ echo '100';} ?> % </div>
		</div>
	</div>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-red-haze">
				<span data-counter="counterup" data-value="1349"> <?php   $unpaid=$rent_not_paid; if($unpaid<0){ $pr=100; echo '0';} else{ echo $unpaid;} ?> </span>
			</h3>
			<small><?php echo date("M");?> Rent Unpaid</small>
		</div>
		<div class="icon">
			<i class="fa fa-money" style="color:red">     </i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php echo 100-$pr;?>%;" class="progress-bar progress-bar-success red-haze">
				<!--<span class="sr-only">30% change</span>-->
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Over due </div>
			<div class="status-number"> <?php echo round(100-$pr,0);?> %</div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-purple-soft" style="color:green">
				<span style="color:green" data-counter="counterup" data-value="276">  <?php   $overpaid=$paid-($expected_pay); if($overpaid>0){ if($expected_pay==0){$expected_pay=1;}  echo $overpaid;  $percent=round($overpaid/$expected_pay*100,0);}else{ echo '0';$percent=0;}?> </span>
			</h3>
			<small> <?php echo date("M");?> Rent Overpaid </small>
		</div>
		<div class="icon">
			<i class="fa fa-money" style="color:green">  </i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?php echo $percent; ?>%;" class="progress-bar progress-bar-success purple-soft">
				<!--<span class="sr-only">10% change </span>-->
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Prepaid </div>
			<div class="status-number"> <?php echo $percent; ?> % </div>
		</div>
	</div>
</div>
</div>
</div>
          <div class="row">
			<div class="col-md-12">
				<div class="col-md-12" style="background:#1bb968;padding:8px;">
						<font color="#ffffff"> Tenants </font>
				</div>
			
			</div>
		</div>
<div class="row">
	<div class="col-md-12">
 
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit ">
	<div class="portlet-body">
		<div class="table-toolbar">
		</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr>
		<th> &nbsp; </th> 
		<th> Date Paid </th>
		<th> Name </th>  
		<th> Payment Method </th>
		<th> Amount </th>	
		<th> Payment History </th>
	</tr>
</thead>
<tbody>
<?php $x=1; foreach($paid_rent->result() as $row):?>
	<tr>
		<td>  <?=$x; ?> </td>
				<td>
			<?=$row->date_paid?>
		</td>
		<td>  
		<?php  foreach($tenant_details->result()  as $t)
			{
				 if($row->tenant_id==$t->id){ 
				 ?>
				  <?php if($t->tenant_type=="residential"){?> <?=$t->first_name?> &nbsp; <?=$t->middle_name?> &nbsp; <?=$t->last_name?> <?php }else {?><?=$t->company_name?><?php } ?>
				<?php
				}
			 }	
			$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
			$enct =base64_encode(do_hash($row->tenant_id . $salt,'sha512') . $salt);
			 ?>
		</td>
		
		<td>
			<?=$row->payment_mode?>
		</td>
		<td> <?=$row->amount?> </td> 

		<td>
			<!--<a data-toggle="modal" id="payment_history"  href="<?=base_url();?>rent/payment_history/<?=$row->tenant_id?>" class="btn green"> View History</a>-->
			<a data-toggle="modal" id="payment_history"  href="<?=base_url();?>rent/statement/<?=$enct?>" class="btn green"> View Statement</a>
		</td>
	</tr> 	 
<?php $x++; endforeach;?>	
</tbody>
</table>
<!--
<a data-toggle="modal" id="payment_history" href="" class="btn red" > Payment History </a>-->
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
                    
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
<!-- responsive -->
    <div id="responsive" class="modal fade" tabindex="-1" data-width="600" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
           <h4 class="modal-title">	 Rent Payment History </h4> 
	     </div>
		<div class="modal-body" >
		<div class="col-md-12"> 
		<div class="col-md-6">
			<p id="tenant_name">    </p>
			<p id="phone">   </p>
			<p id="email">   </p>
		</div>
		<div class="col-md-6">
			<p id="property_name">    </p>
			<p id="floor">   </p>
			<p id="category">   </p>
		</div>
		
		</div>
		<div class="portlet light portlet-fit ">
		<div class="portlet-body">
			<div class="table-scrollable">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th> Date </th>
							<th> Amount </th>
							<th> Payment mode </th> 
						</tr>
					</thead>
					<tbody id="payment_details">
						 
						 
						 
					</tbody>
				</table>
			</div>
		</div>
	</div>

<div class="modal-footer" >
<!--<button type="button" class="btn red" onclick="window.print();"> Print </button>
<button type="button" class="btn green"> Email </button>-->
<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
</div>
  
</div>
</div> 
</div> 
</div> 


<script type="text/javascript">
 
 
 $(document).ready(function(){ 
	$("#payment_history1").click(function(){ 
	//validate();
	/*var id = [];
	$.each($("input[name='checkbox[]']:checked"), function(){            
	id=($(this).val()); 
	}); */ 
		$.ajax({
		"url":"<?=base_url();?>rent/payment_history/"+id,
        "type":"post",
		"success":function(data)
		{
			 
			var obj = JSON.parse(data); 
			 
			var data = obj.data;
			var content="";
            for(var i=0; i<data.length; i++)
			{
                    var property = data[i]; 
					$("#tenant_name").html((property['from']).toUpperCase());
					$("#property_name").html((property['property_allocated']).toUpperCase());
					$("#email").html(property['email']);
					$("#phone").html(property['mobile_phone']); 
					$("#house").html(property['house_no']);
					$("#floor").html(property['floor_no']);
					$("#date").html(property['paid_on']); 
					content = content+ ("<tr> <td> "+property['paid_on']+" </td> <td> "+property['amount']+"</td> <td> "+property['payment_mode']+" </td> </tr>");
				}
				$("#payment_details").append(content);
				getData(id);
if(id==""){return false;}else{
			 $("#responsive").modal('show');
                              }
		}
	});
		
 
}); 	
}); 

function payment_history(id)
{alert('hey');
	//validate();
	/*var id = [];
	$.each($("input[name='checkbox[]']:checked"), function(){            
	id=($(this).val()); 
	}); */ 
		$.ajax({
		"url":"<?=base_url();?>rent/payment_history/"+id,
        "type":"post",
		"success":function(data)
		{
			var obj = JSON.parse(data); 
			var data = obj.data;
			var content="";
            for(var i=0; i<data.length; i++)
			{
                    var property = data[i]; 
					$("#tenant_name").html((property['from']).toUpperCase());
					$("#property_name").html((property['property_allocated']).toUpperCase());
					$("#email").html(property['email']);
					$("#phone").html(property['mobile_phone']); 
					$("#house").html(property['house_no']);
					$("#floor").html(property['floor_no']);
					$("#date").html(property['paid_on']); 
					content = content+ ("<tr> <td> "+property['paid_on']+" </td> <td> "+property['amount']+"</td> <td> "+property['payment_mode']+" </td> </tr>");
				}
				$("#payment_details").append(content);
				getData(id);
				if(id==""){return false;}else{
			 $("#responsive").modal('show');
                              }
		}
	});
		
 

}	
function getData(id){	
$.ajax({
		"url":"<?=base_url();?>tenants/getData/"+id,
        "type":"post",
		"success":function(data)
		{
			 
			var obj = JSON.parse(data); 
			 
			var data = obj.data;
			var content="";
            for(var i=0; i<data.length; i++)
			{
                    var property = data[i];  
					$("#category").html((property['unit_category']).toUpperCase());
					$("#email").html(property['email']);
					$("#phone").html(property['mobile_no']); 
					$("#house").html(property['house_no']); 
				//	content = content+ ("<tr> <td> "+property['paid_on']+" </td> <td> "+property['amount']+"</td> <td> "+property['payment_mode']+" </td> </tr>");
			} 
		}
	});
}
</script>