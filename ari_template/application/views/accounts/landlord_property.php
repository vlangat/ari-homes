<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper"> 
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
	<div class="container">
		<!-- BEGIN PAGE BREADCRUMBS -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Property</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Landlord   </span>
			</li>
	</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row"> 
<div class="col-md-12">
<!-- BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
	<div class="row">
		<div class="col-md-12">
<div class="portlet light ">		 
<div class="portlet-body">
	<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
<div class="tab-pane active" name="tab_1_1">
<?php if($msg !="")
{
/*echo '<div class = "alert alert-success alert-dismissable" style="background:lightgreen">
   <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	  &times;
   </button>
	
   <font color="#006699">'. $msg. '</font>
</div>'; 	*/
}
?>
 
<div class="row"> 
	<div class="col-md-12" style="background:#006699;padding:6px;">
				<font color="#ffffff">  Landlord Properties </font>
	</div>		 
<div class="col-md-12">  &nbsp;   </div> 
<?php $name=""; $phone=""; $email="";
	foreach($landlords->result() as $l){   
	 
	 $landlord_id=$l->id; $phone=$l->phone; $email=$l->email; $name=strtolower($l->first_name).' '.strtolower($l->middle_name).' '.strtolower($l->last_name); 
	 
  }
   
?>
 	<table width="100%" id="" cellspacing="0" cellpadding="12" class="table table-striped table-hover table-bordered">
		<tr>
			<td colspan="2"> 
				<strong> Landlord Name :</strong>  <?=strtoupper($name)?>
			</td>
		</tr>
		<tr> 
			<td>
				<strong> Email: </strong> <?=$email?>	
			</td>
			<td>  <strong> Phone: </strong> <?=$phone?></td>
			 
		</tr>  
</table>
<div class="col-md-12"> &nbsp;  </div>
<table  class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>  
							<th> #  </th>
							<th> Property Name  </th> 
							<th> Type </th> 
							<th> Location </th> 
							<th> Street </th>  
							<th> Date Added </th> 
							<th> Remove </th>  
						</tr>
					</thead>
					<tbody>
					<?php $i=1;foreach($properties->result() as $row){
						//foreach($landlords->result() as $l){ if($row->landlord==$l->id){  $name=strtolower($l->first_name).' '.strtolower($l->middle_name).' '.strtolower($l->last_name);} }
						?>
						<tr> <td><?=$i?></td> <td> <?=ucfirst(strtolower($row->property_name))?> </td>  <td> <?=$row->property_type?> </td>  <td> <?=$row->location?> </td><td> <?=$row->street?> </td>  <td> <?=$row->property_added_date?> </td><td> <a href="javascript:;" onclick="delete_landlord('<?=$row->id?>','<?=$landlord_id?>')" ><i class="fa fa-trash"> </i> Remove </a> </td> </tr> 
					<?php  $i++; } 
					?>						
					</tbody>
				</table>
  
</div>
</div>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->

<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
</div>
</div>
<!-- END CONTAINER --> 
 

 <div id="delete_landlord" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Confirm Message </b></h5>
					<p>
					 Do you want to remove this property from this landlord?
					</p>
					<input type="hidden" id="property_id">
					<input type="hidden" id="landlord_id">
				</div>
			</div>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="confirm_del" class="btn green" <?=$disabled?>>&nbsp; Yes &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn red">&nbsp; No  &nbsp; </button> 
		</center>
	</div>
</div>

  <div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:20px;color:green">   Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				 
				<p id="">
				   <?php  echo $msg;?>
				</p>
				</div>
			</div>    
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
  

<script type="text/javascript">
 
function validateMail() {
     
    var x = $("#Email").val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
     $("#Email").val('');

        return false;
    }
}
function delete_landlord(property,landlord)
	{ 
	        $("#property_id").val(property);
	        $("#landlord_id").val(landlord);
			$("#delete_landlord").modal('show');
	 
	}
  
 
 $(document).ready(function(){   
  checkPrivilege();   
  var msg="<?php echo $msg;?>";   
  var saving_success="<?php echo $msg;?>";
 if(!saving_success)
 {

 }
else{
       $("#data_saving_success").modal('toggle'); 	
   }
 
});	 
 
 $("#confirm_del").click(function()
	{  
	//$("#delete_landlord").modal('hide');
	var property=$("#property_id").val();  
	var landlord=$("#landlord_id").val();   
	$.ajax({
		"url":"<?=base_url();?>tenants/dealocate_landlord/"+property+"/"+landlord,
        "type":"post",
		"success":function(data)
		{ 
			var obj=JSON.parse(data); 
			 if(obj.result=="ok")
			 {
				 location.reload(); 
			 }
			 else
			 {
				$("#message").html("<font color='red'> Error! Landlord Not removed. Try again later</font>");
				$("#success").modal('show');
			 }
		}
		
	})
	
 	});
function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>Auth/checkPrivilege/1",
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;  
			if(obj.add==0)
			{  
				 document.getElementById('save').disabled = true; 
		      //$("#save_btn").html("<font color='' onclick=\"alert('You have no privilege to add property')\"> Save Details </font>"); 
			} 
		 
		}
	 })
 } 
 		
</script>
