<body class="page-container-bg-solid page-boxed">
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->

<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
	<!-- BEGIN PAGE BREADCRUMBS -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="index.html">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="#">Property</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>	Edit property </span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMBS -->
	<!-- BEGIN PAGE CONTENT INNER -->
	<?php echo  form_open_multipart("property/edit_property");?>
	<div class="page-content-inner">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PROFILE SIDEBAR --> 
			<!-- BEGIN PROFILE CONTENT -->
			<div class="profile-content">
		<div class="row">
	<div class="col-md-12">
<div class="portlet light ">

<?php foreach($data->result() as $row){}?>

	<div class="portlet-title tabbable-line">
		<div class="caption caption-md">
			<i class="icon-globe theme-font hide"></i>
			<span class="caption-subject font-blue-madison bold uppercase"> Edit Property: <?=$row->property_name?></span>
	</div>  
</div>
		 
<div class="portlet-body">
	<div class="tab-content">
		<!-- PERSONAL INFO TAB -->
			<div class="tab-pane active" id="tab_1_1">
				<div class="row">
					<div class="col-md-12">
				<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"> Properties Basic details and Location </font>
		</div>
	</div>
<div class="col-md-12"> &nbsp; </div>	
</div>
<form role="form" action="#">
<div class="row">
	
		<div class="col-md-4">
			<div class="form-group"> 
				<label class="control-label"> Property Name </label>
				<input type="text"  class="form-control" name="property_name" onchange="validate()" value="<?=$row->property_name?>" />
				<input type="hidden"  name="property_id" class="form-control" value="<?=$row->id?>" />
				<input type="hidden"  name="enct_property_id" class="form-control" value="<?=$enct?>" />
			</div>
		</div>
		
		<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Property Type </label>
		   <select class="form-control" name="property_type">
				<option><?=$row->property_type?></option>
				<option>Bungalow</option>
				<option>Flat</option>
				<option>Commercial</option>
				<option>Office</option>
			</select>
		</div> 
		</div>
		<div class="col-md-4">  
<div class="form-group">
			<label class="control-label">LR/NO</label>
			<input type="text"   class="form-control" name="lr_no" value="<?=$row->lr_no?>" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="LR NO" onchange="checkChars('LR NO')"/> 
		</div>

</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> Total Floors </label>
			<input type="text"  class="form-control" name="floors" value="<?=$row->floors?>"/>
		</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">No of car Spaces</label>
				<input type="text"   name="car_spaces" class="form-control" value="<?=$row->car_spaces?>" /> 
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label"> Location Name </label>
				<input type="text"   class="form-control" name="location"  onchange="validate()" value="<?=$row->location?>" />
			</div>
		</div>
	</div>
<div class="row"> 
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Street</label>
			<input type="text"  name="street" class="form-control" onchange="validate()" value="<?=$row->street?>" /> 
		</div>
	</div> 
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label"> City/Town </label>
			<input type="text"   class="form-control" name="city_town"  onchange="validate()" value="<?=$row->town?>" />
		</div>
	</div>		
<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Country</label>
			<select class="form-control" name="country" onchange="validate()"/>
						<option value="<?=$row->country?>">  <?=$row->country?>  </option>
                        <option value="AF">Afghanistan</option>
                        <option value="AL">Albania</option>
                        <option value="DZ">Algeria</option>
                        <option value="AS">American Samoa</option>
                        <option value="AD">Andorra</option>
                        <option value="AO">Angola</option>
                        <option value="AI">Anguilla</option>
                        <option value="AR">Argentina</option>
                        <option value="AM">Armenia</option>
                        <option value="AW">Aruba</option>
                        <option value="AU">Australia</option>
                        <option value="AT">Austria</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="BS">Bahamas</option>
                        <option value="BH">Bahrain</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BB">Barbados</option>
                        <option value="BY">Belarus</option>
                        <option value="BE">Belgium</option>
                        <option value="BZ">Belize</option>
                        <option value="BJ">Benin</option>
                        <option value="BM">Bermuda</option>
                        <option value="BT">Bhutan</option>
                        <option value="BO">Bolivia</option>
                        <option value="BA">Bosnia and Herzegowina</option>
                        <option value="BW">Botswana</option>
                        <option value="BV">Bouvet Island</option>
                        <option value="BR">Brazil</option>
                        <option value="IO">British Indian Ocean Territory</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BI">Burundi</option>
                        <option value="KH">Cambodia</option>
                        <option value="CM">Cameroon</option>
                        <option value="CA">Canada</option>
                        <option value="CV">Cape Verde</option>
                        <option value="KY">Cayman Islands</option>
                        <option value="CF">Central African Republic</option>
                        <option value="TD">Chad</option>
                        <option value="CL">Chile</option>
                        <option value="CN">China</option>
                        <option value="CX">Christmas Island</option>
                        <option value="CC">Cocos (Keeling) Islands</option>
                        <option value="CO">Colombia</option>
                        <option value="KM">Comoros</option>
                        <option value="CG">Congo</option>
                        <option value="CD">Congo, the Democratic Republic of the</option>
                        <option value="CK">Cook Islands</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CI">Cote d'Ivoire</option>
                        <option value="HR">Croatia (Hrvatska)</option>
                        <option value="CU">Cuba</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="DK">Denmark</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="EC">Ecuador</option>
                        <option value="EG">Egypt</option>
                        <option value="SV">El Salvador</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="ER">Eritrea</option>
                        <option value="EE">Estonia</option>
                        <option value="ET">Ethiopia</option>
                        <option value="FK">Falkland Islands (Malvinas)</option>
                        <option value="FO">Faroe Islands</option>
                        <option value="FJ">Fiji</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GF">French Guiana</option>
                        <option value="PF">French Polynesia</option>
                        <option value="TF">French Southern Territories</option>
                        <option value="GA">Gabon</option>
                        <option value="GM">Gambia</option>
                        <option value="GE">Georgia</option>
                        <option value="DE">Germany</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GR">Greece</option>
                        <option value="GL">Greenland</option>
                        <option value="GD">Grenada</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GU">Guam</option>
                        <option value="GT">Guatemala</option>
                        <option value="GN">Guinea</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="HT">Haiti</option>
                        <option value="HM">Heard and Mc Donald Islands</option>
                        <option value="VA">Holy See (Vatican City State)</option>
                        <option value="HN">Honduras</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HU">Hungary</option>
                        <option value="IS">Iceland</option>
                        <option value="IN">India</option>
                        <option value="ID">Indonesia</option>
                        <option value="IR">Iran (Islamic Republic of)</option>
                        <option value="IQ">Iraq</option>
                        <option value="IE">Ireland</option>
                        <option value="IL">Israel</option>
                        <option value="IT">Italy</option>
                        <option value="JM">Jamaica</option>
                        <option value="JP">Japan</option>
                        <option value="JO">Jordan</option>
                        <option value="KZ">Kazakhstan</option>
                        <option value="KE">Kenya</option>
                        <option value="KI">Kiribati</option>
                        <option value="KP">Korea, Democratic People's Republic of</option>
                        <option value="KR">Korea, Republic of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LV">Latvia</option>
                        <option value="LB">Lebanon</option>
                        <option value="LS">Lesotho</option>
                        <option value="LR">Liberia</option>
                        <option value="LY">Libyan Arab Jamahiriya</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="MO">Macau</option>
                        <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                        <option value="MG">Madagascar</option>
                        <option value="MW">Malawi</option>
                        <option value="MY">Malaysia</option>
                        <option value="MV">Maldives</option>
                        <option value="ML">Mali</option>
                        <option value="MT">Malta</option>
                        <option value="MH">Marshall Islands</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MU">Mauritius</option>
                        <option value="YT">Mayotte</option>
                        <option value="MX">Mexico</option>
                        <option value="FM">Micronesia, Federated States of</option>
                        <option value="MD">Moldova, Republic of</option>
                        <option value="MC">Monaco</option>
                        <option value="MN">Mongolia</option>
                        <option value="MS">Montserrat</option>
                        <option value="MA">Morocco</option>
                        <option value="MZ">Mozambique</option>
                        <option value="MM">Myanmar</option>
                        <option value="NA">Namibia</option>
                        <option value="NR">Nauru</option>
                        <option value="NP">Nepal</option>
                        <option value="NL">Netherlands</option>
                        <option value="AN">Netherlands Antilles</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NZ">New Zealand</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NE">Niger</option>
                        <option value="NG">Nigeria</option>
                        <option value="NU">Niue</option>
                        <option value="NF">Norfolk Island</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="NO">Norway</option>
                        <option value="OM">Oman</option>
                        <option value="PK">Pakistan</option>
                        <option value="PW">Palau</option>
                        <option value="PA">Panama</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PY">Paraguay</option>
                        <option value="PE">Peru</option>
                        <option value="PH">Philippines</option>
                        <option value="PN">Pitcairn</option>
                        <option value="PL">Poland</option>
                        <option value="PT">Portugal</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="QA">Qatar</option>
                        <option value="RE">Reunion</option>
                        <option value="RO">Romania</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="KN">Saint Kitts and Nevis</option>
                        <option value="LC">Saint LUCIA</option>
                        <option value="VC">Saint Vincent and the Grenadines</option>
                        <option value="WS">Samoa</option>
                        <option value="SM">San Marino</option>
                        <option value="ST">Sao Tome and Principe</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SN">Senegal</option>
                        <option value="SC">Seychelles</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SG">Singapore</option>
                        <option value="SK">Slovakia (Slovak Republic)</option>
                        <option value="SI">Slovenia</option>
                        <option value="SB">Solomon Islands</option>
                        <option value="SO">Somalia</option>
                        <option value="ZA">South Africa</option>
                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                        <option value="ES">Spain</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="SH">St. Helena</option>
                        <option value="PM">St. Pierre and Miquelon</option>
                        <option value="SD">Sudan</option>
                        <option value="SR">Suriname</option>
                        <option value="SJ">Svalbard and Jan Mayen Islands</option>
                        <option value="SZ">Swaziland</option>
                        <option value="SE">Sweden</option>
                        <option value="CH">Switzerland</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="TW">Taiwan, Province of China</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TZ">Tanzania, United Republic of</option>
                        <option value="TH">Thailand</option>
                        <option value="TG">Togo</option>
                        <option value="TK">Tokelau</option>
                        <option value="TO">Tonga</option>
                        <option value="TT">Trinidad and Tobago</option>
                        <option value="TN">Tunisia</option>
                        <option value="TR">Turkey</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TC">Turks and Caicos Islands</option>
                        <option value="TV">Tuvalu</option>
                        <option value="UG">Uganda</option>
                        <option value="UA">Ukraine</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                        <option value="UM">United States Minor Outlying Islands</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="VU">Vanuatu</option>
                        <option value="VE">Venezuela</option>
                        <option value="VN">Viet Nam</option>
                        <option value="VG">Virgin Islands (British)</option>
                        <option value="VI">Virgin Islands (U.S.)</option>
                        <option value="WF">Wallis and Futuna Islands</option>
                        <option value="EH">Western Sahara</option>
                        <option value="YE">Yemen</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
</select>						
			</div>
		</div>
	</div>
</div>

  
 <div class="row">
 
<div class="col-md-6">
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Water </font>
	</div>		
   <div class="col-md-12">  &nbsp;  </div>	
 
<div class="form-group">
			<label class="control-label"> 	Price per unit</label>
			<input type="number"   class="form-control" min="0" name="water_unit_cost" value="<?=$row->water_unit_cost?>" /> 
		</div>
 
</div>

<div class="col-md-6">
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Management Fee </font>
	</div>		
 
   <div class="col-md-12">  &nbsp;  </div>	
 
<div class="form-group">
	<label class="control-label"> Agents Management Fee (in percentage)</label>
	<input type="number"  max="100" class="form-control" min="0" name="management_fee" value="<?=$row->management_fee?>" /> 
</div>
 
</div> 

</div>
<!-----------
<div class="row">
<div class="col-md-12">
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Indoor Property Features and Amenities </font>
	</div>		
</div>
<div class="col-md-12">  &nbsp;  </div>	
</div>

<div class="row"> 
<?php $checked=""; foreach($indoor_amenities->result() as $r){?>
		<div class="col-md-3">
			<div class="form-group">
			<?php foreach($checked_indoor->result() as $c){  if($c->amenity_id==($r->id)){ $checked="checked";} }?>
					<label>
					 
						<input type="checkbox"  <?php echo $checked;?>  value="<?=$r->id?>"   name='indoor[]'>  <?=$r->amenity?>
					 
					</label>
			</div>
		</div> 
<?php   $checked=""; }
if(!$indoor_amenities)
{
	echo '<p> In door amenities for the property are not available</p>';
}
?>

</div> 

<div class="row">
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Outdoor Property Features and Amenities </font>
		</div>		
</div>
<div class="col-md-12">  &nbsp;  </div>	
</div>

<div class="row"> 
		<?php $checked2=""; foreach($outdoor_amenities->result() as $q)
		{?>
		<div class="col-md-3">
			<div class="form-group">
			<?php foreach($checked_outdoor->result() as $c){if($c->amenity_id==($q->id)){ $checked2="checked";} }?>
				<label>
					<input type="checkbox" <?php echo $checked2;?> name="outdoor[]" value="<?=$q->id?>">  <?=$q->amenity?>
				</label>
			</div>
		</div> 
	<?php
		$checked2="";
	} 
	if(!$outdoor_amenities)
	{
		echo '<p> Out door amenities for the property are not available</p>';
	}
?> 
			 
</div> 
-->
<div class="row">
 
	<div class="col-md-12">  &nbsp;  </div>	 
 
 <div class="col-md-6">
	<div class="form-group">
			<input type="submit" value="Save Changes" class="btn green"  <?=$disabled?> />   
	 </div> 
 </div> 
<div class="col-md-6">
		<div class="form-group">
				&nbsp; 
		</div> </form>
<!-- END PROFILE CONTENT -->
</div>
 
</div> 
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END CONTAINER -->

<!-- responsive -->
     <div id="responsive" class="modal fade" tabindex="-1" data-width="400">
              <div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			<h5><b> Edit Units </b></h5>
			<div class="form-group">

		<p>	<label class="control-label">Category </label>
			<select class="form-control">
				<option>Single Room</option>
				<option>1 Bedroom</option>
				<option>2 Bedroom</option>
				<option>3 Bedroom</option>
				<option>4 Bedroom</option>
				<option>5 Bedroom</option>
			</select>
		</p>
	</div>
	<p>
		<label class="control-label">Total Units </label>
		<input placeholder="2" class="form-control" type="text">
	</p>
	<p>
		<label class="control-label">Number of Baths </label>
		<input placeholder="1" class="form-control" type="text"> </p>
	<p>
					
	<p>
		<label class="control-label">Full Furnished </label>
		<select class="form-control">
			<option>Yes</option>
			<option>No</option>
		</select>
	</p>
</div>
</div>
</div>
<div class="modal-footer" >
<button type="button" class="btn green"> OK </button>
<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div> 
<!-- responsive -->
  <div id="responsive2" class="modal fade" tabindex="-1" data-width="400">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b> Delete Confirmation </b></h5>
				<hr/>
				<p id="del_status"> Are you sure you want to remove this file? </p>
				
			</div>
		</div>
	</div>
<div class="modal-footer" >
	<center> <button type="button" class="btn green" id="confirm" <?=$disabled?>> Yes </button>
	<button type="button" data-dismiss="modal"  class="btn red"> No </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
    </center>
	</div>
</div>
 
 
<div id="msg_dialog" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Edit Property  </b></h5>
				<hr/>
				<p id="message">
				   
				</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<center> <button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button></center>
	</div>
</div>
<script language="javascript">
var id="<?php echo $this->session->userdata('property_id');?>";

$(function()
{ 
var msg="<?php echo $msg; ?>"; 
 if(msg==""){ 
   
	  //do nothing  
 }else{
	 //show modal
	 $('#message').html("<font> "+msg+"</font>");   
	 $('#msg_dialog').modal('toggle');   
 }
 
});

$("#confirm").click(function(){
	var property_id="<?php echo $this->session->userdata('property_id');?>";
	var id=$("#confirm").val();
	 $.ajax({
		url:"<?=base_url();?>property/deleteFile/"+id,
		type:"POST",
		async:false ,
		success:function(data)
		{ 
			var obj = JSON.parse(data); 
			if (obj.result=="ok")
			{
			 
			 window.location.replace("<?=base_url();?>property/edit/"+property_id);						
			//refreshData();
			}
			else{
				$("#del_status").html(" <font color='red'> Error! File was not removed </font> "); 
				setTimeout(function(){
					$('#responsive2').modal('hide')
				}, 2000);
				return false;
			}
			
		}
		})
	
});

function deleteFile(id)
{ 
		$("#responsive2").modal('toggle');
		$("#confirm").val(id);
		 
}
 
</script>