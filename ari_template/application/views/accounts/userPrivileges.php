 <?php foreach($data->result() as $row){			} ?>
 <?php foreach($property->result() as $p){			} ?>
 <?php foreach($tenants->result() as $t){   	 	} ?>
 <?php foreach($rent->result() as $r){  	}?>
 <?php foreach($suppliers->result() as $s){  	}?>
 <?php foreach($subscription->result() as $sub){  	} ?>
 <?php foreach($users->result() as $u){  	} ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
	<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="index.html">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#"> Users </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Permissions </span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		 <!-- PORTLET MAIN -->
		<?php //foreach($profile as $rows){} 
		?>
 
	</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
	 
<div class="portlet light ">
			<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Permissions </font>
		</div>	
 
<div class="portlet-body">

<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
 
	<!-- END PERSONAL INFO TAB --> 
<div   id="tab_1_4">
	<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit ">
			 <div class="portlet-body">
			 <form action="<?=base_url();?>auth/assign_privilege" method="post">
			 <input type="hidden" value="<?=$user_id?>" name="user_id">
			<table   class="table table-striped table-hover table-bordered" >
			<thead>
			<tr><th> &nbsp; </th>  <th> Add </th> <th> Edit  </th> <th> View </th> <th> Delete </th> </tr>
			</thead>
			<tbody>
			<tr>
				<td> 
					<strong>Property </strong>
					</td>  
					 
					  <td>  
					 <input type="checkbox"  name="p_addPrivilege"  <?php if($p->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="p_editPrivilege" <?php if($p->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="p_viewPrivilege" <?php if($p->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="p_deletePrivilege" <?php if($p->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Tenants </strong>
					</td> 
					 
					  <td>  
					 <input type="checkbox"  name="t_addPrivilege"  <?php if($t->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="t_editPrivilege" <?php if($t->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="t_viewPrivilege" <?php if($t->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="t_deletePrivilege" <?php if($t->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Rent </strong>
					</td> 
					 
					  <td>  
					 <input type="checkbox"  name="r_addPrivilege"  <?php if($r->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="r_editPrivilege" <?php if($r->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="r_viewPrivilege" <?php if($r->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="r_deletePrivilege" <?php if($r->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>			
			<tr>
					<td> 
						<strong>Suppliers </strong>
					</td> 
					 
					  <td>  
					 <input type="checkbox"  name="s_addPrivilege"  <?php if($s->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="s_editPrivilege" <?php if($s->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="s_viewPrivilege" <?php if($s->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="s_deletePrivilege" <?php if($s->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>			
			<tr>
				   <td> 
					<strong>Subscriptions </strong>
					</td> 
					  <td>  
					 <input type="checkbox"  name="sub_addPrivilege"  <?php if($sub->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="sub_editPrivilege" <?php if($sub->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="sub_viewPrivilege" <?php if($sub->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="sub_deletePrivilege" <?php if($sub->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>	
<tr>
				   <td> 
					<strong>Users </strong>
					</td> 
					  <td>  
					 <input type="checkbox"  name="users_addPrivilege"  <?php if($u->addPrivilege==1){ echo "checked";} ?> value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="users_editPrivilege" <?php if($u->editPrivilege==1){ echo "checked";} ?> value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"   name="users_viewPrivilege" <?php if($u->viewPrivilege==1){ echo "checked";} ?> value="1">    
					</td> 
					<td> 
					<input type="checkbox"   name="users_deletePrivilege" <?php if($u->deletePrivilege==1){ echo "checked";} ?> value="1" >    
					  
					</td>
			</tr>				
 </tbody>
</table>	 
<p>  </p>		
<div class="form-group">
		<input type="submit" <?=$disabled?> value="Save"  id="save_btn" class="btn green"  /> 
</div>
</form>
					</div>
				</div>
			</div>
			
			
</div>
</div> <p id="error">   </p>
</div>   
        <?php if($this->session->flashdata('temp')){
				$msg=$this->session->flashdata('temp');
				echo '<div class = "alert alert-success alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button> <font color="green">'. $msg. '</font> </div>'; 	
				} 	
		?>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div> 

<div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5> <b> Venit Message </b> </h5>
				<hr/>
					<p id="msg">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div> 
<!-- END CONTENT --> 
 
</body> 
</html>


<script type="text/javascript">
$(function()
{ 

 var msg="<?=$msg?>"; 
 if(!msg)
 {
 
 }else{
	 $("#msg").html("<font> "+msg+" </font>");
	 $('#success').modal('show');
 }
 
$("#confirm_subscription").click(function()
{ 
	var package_name=$("#package_type").val();
	window.location="<?=base_url();?>payment/package/";
});

$("#add_new").click(function()
{ 
	var c="<?=$disabled?>";
	if(c=="disabled")
	{
		
	}else{
	var users_limit=$("#total_users").val();
	var i=0;
	var users=0;
	$.ajax({
		url:"<?=base_url();?>auth/checkUsers",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
                                 
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					var property=data[i]; 
					var package_name=property['package_type']; 
					users=users+property['number_of_users']++;
				 } 
				 
				if(users_limit<users)
				{ 
					$('#responsive_2').modal('show');
				
					
				}else{
					$("#package_type").val(package_name);
						$('#subscribe_users').modal('show');
						
					return false;
				}
				
			}
			else{
				$('#subscribe_users').modal('show');
			}
		}
		
		
	})
	
	}
});

$("#add_new_user").click(function()
	{  
		var email=$("#email").val();
		var fname=$("#First_Name").val();
		var lname=$("#Last_Name").val();  	  	
		if(email&&fname&&lname){ } else{$("#add_user").html("<font color='red'> Empty fields not required </font>"); return false;}
		var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#add_user").html("<font color='red'> Email provided is not a valid e-mail address"); return false;}
		$("#add_user").html("<font color='blue'> Adding new user....</font>");
		$.ajax({
		 url:"<?=base_url();?>auth/addUser",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'lname':lname, 
		   'email':email 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){
				 $("#First Name").val('');$("#Last Name").val(''); $("#email").val('');
				  
				 $("#add_user").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>"+obj.msg+"  </font>"); 
                getAgents();	 
			 }
			 else{
				 $("#add_user").html("<font color='red'> "+obj.msg+"</font>");return false;
			 }
		 }
		 
		  }
		 )	  
});

$("#confirm").click(function(){ 
	var par1=$("#confirm").val();
	var par2=$("#status").val();
	$("#del_status").html("<font color='blue'>  Please wait....</font>");
	 $.ajax({
		url:"<?=base_url();?>auth/removeUser/"+par1+"/"+par2,
		type:"POST",
		async:false ,
		success:function(data)
		{ 
			var obj = JSON.parse(data); 
			if (obj.result=="ok")
			{
			   getAgents();
			   if(par2==1){ msg="de-activated";}else{msg="activated";}
			   $("#del_status").html(" <font color='green'> Agent "+msg+" successfully</font> ");
				setTimeout(function(){
					$('#del_status').empty()
					$('#del_user').modal('hide')
				}, 2000);
				return false;			   
			}
			else{
				$("#del_status").html(" <font color='red'> Error! Agent not removed </font> "); 
				return false;
			}
			
		}
		})
	
});

});

function getAgents()
{
	var i=0;
	var users=1;
	$.ajax({
		url:"<?=base_url();?>auth/loadAgents",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				 var content="";
				 $("#agents").empty();
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					var status="";
					var agent=data[i]; 
					 if(agent['user_enabled']==1){ status="<font color='green' title='Click to Deactivate'>Active</font>";} else{ status="<font color='red' title='Click to activate'> Deactivated </font>";}
					content=content+"<tr><td> "+agent['first_name']+"</td> <td> "+agent['last_name']+"</td><td> "+agent['mobile_number']+"</td><td> "+agent['email']+"</td><td><a   onclick=\"remove_user("+agent['id']+","+agent['user_enabled']+")\"> " +status+" </a></td><td><a   href='<?=base_url();?>auth/userPrivileges/"+agent['id']+"'> <i class='fa fa-edit'></i> Edit Privileges </a></td></tr>";
					users++;
				 }
				 $("#total_users").val(users); 
				 $("#agents").html(content); 
			}
else
{

$("#total_users").val(users); 
}
		}
		
		
	})
	
}

function remove_user(id,status)
{ 
	
		$("#del_user").modal('toggle');
		$("#confirm").val(id);
		$("#status").val(status); 
		 
}


$(document).ready(function(){
 
	 checkPrivilege(); 
}
);
function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/6",
		type:"POST", 
		async:false,
		success:function(data)
		{  
			var obj=JSON.parse(data);   
			var data = obj.data;   
			if(obj.add==0)
			{        $("#add_items").val(obj.add); 
				 document.getElementById('add_new_user').disabled = true; 
				 document.getElementById('add_new').disabled = true; 
			 } 
			 
		}
	 })
 } 
 
 
</script>