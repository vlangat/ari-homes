<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<?php foreach($property->result() as $row){ $property_id=$row->id;	}?>
<?php  $total_tenant=0; foreach($tenants->result() as $t){  $total_tenant=$total_tenant+1;	}?>
<?php  $expected_rent=0; foreach($tenants->result() as $ten){ 
		    foreach($rent->result() as $e){ if($ten->id==$e->tenant_id){ $expected_rent=$expected_rent+$e->payment_type_value;	} } 
			
			}
 if($expected_rent==0){ $expected_rent=1;}
  $total_rent=0; 
 
foreach($tenants->result() as $ten){
	foreach($paid_rent->result() as $row3){
		if($ten->id==$row3->tenant_id){
			$month=date("m",strtotime($row3->date_paid));
			if($month==date("m"))
			{ 
				$total_rent=$total_rent+$row3->amount;
			} 
		}
 }
}
 ?>
 <?php  $total_units=0;foreach($units->result() as $rows){  $total_units =$total_units + $rows->total_units;} ?>
 <?php  $media="property_img.jpg"; foreach($documents->result() as $docs){  $media =$docs->media;} ?>
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="index.html"> Property </a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span> <?=ucfirst(strtolower($row->property_name))?></span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="dashboard-stat2 ">
   <div class="row">
	<div class="col-md-4">  
		<img src="<?=base_url();?>media/<?=$media?>" width="300" height="150"> 
		 
		<p> <a href="javascript:;" id="upload_photo"> Change photo </a> </p>
	</div>
		<div class="col-md-8">
	 
				 
				<h4> <?=ucfirst(strtolower($row->property_name))?>  </h4>
				<h4><?=ucfirst(strtolower($row->location))?> , <?=ucfirst(strtolower($row->street))?> ,   <?=ucfirst(strtolower($row->town))?> , <?=$row->country?>   </h4>
				<h4> <?=ucfirst(strtolower($row->property_type)) ?> | <?=$row->floors?> floors |<?=$total_units?> units   </h4>
				 
			<!--	<h4> <b> Amenities </b> </h4>-->
		 
		 
		<?php
		//foreach($amenities->result() as $amenity){ 
		?>
		<!--		
			<div class="col-md-4">  
				<?php //echo $amenity->amenity;
				?>
			  <!--</div>-->
		<?php 
		//}
		?>
		 
		
		</div> 
	</div>
</div>
   
</div>
<div class="row">
<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
 <?php  $total_units=0; foreach($units->result() as $row2){  $total_units =$total_units + $row2->total_units; }?>
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value="7800"><?=$total_tenant?></span>
				<small class="font-green-sharp"> </small>
			</h3>
			<small> Occupied Units </small>
		</div>
		<div class="icon">
			<i class="icon-home"></i>
		</div>
	</div>
	<div class="progress-info"> <?php  $units=$total_units; if($total_units==0){ $units=1;} $per=($total_tenant /$units) * 100;?>
		<div class="progress">
			<span style="width:<?=$per?>%" class="progress-bar progress-bar-success green-sharp">
				<span class="sr-only">76% Occupation</span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Occupation </div>
			<div class="status-number"> <?=round($per) ?> % </div>
		</div>
	</div>
</div>
</div>
<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2">
	<div class="display">
		<div class="number">
			<h3 class="font-red-haze">
				<span data-counter="counterup" data-value="1349"><?=$total_units-($total_tenant)?> </span>
			</h3>
			<small> Vacant Units </small>
		</div>
		<div class="icon">
			<i class="icon-grid">	</i>   
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?=100-$per?>%;" class="progress-bar progress-bar-success red-haze">
				<span class="sr-only"> 100% change </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title">   Empty  </div>
			<div class="status-number">  <?=round(100-$per)?>%    </div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-purple-soft" style="color:green">
				<span style="color:green" data-counter="counterup" data-value="276"><?= $total_rent?></span>
			</h3>
			<small><?=date("M")?> Rent </small>
		</div>
		<div class="icon">
			<i class="fa fa-money"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?=$total_rent/$expected_rent*100?>%;" class="progress-bar progress-bar-success purple-soft">
				<span class="sr-only">56% change</span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Collection </div>
			<div class="status-number"> <?=round($total_rent/$expected_rent*100)?>% </div>
		</div>
	</div>
</div>
</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:8px;">
				<font color="#ffffff"> Tenants </font>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12">
			<div class="form-group">
					<div class="col-md-12">  &nbsp;  </div>
				<?php	$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
				$enct =base64_encode(do_hash($row->id . $salt,'sha512') . $salt);?>
					<a href="<?=base_url();?>tenants/individual/<?=$enct?>" class="btn red"  <?=$disabled?> >Add New Tenant <i class="glyphicon glyphicon-plus"></i></a> 
			</div>
		</div>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit ">
	<div class="portlet-body">
		<div class="table-toolbar">
		</div>
<form name="form1" method="post" action="<?=site_url();?>tenants/viewTenant" onSubmit="return validate();">
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
	<tr>
		<th> &nbsp; </th>
		<th> Name </th>
		<th> Mobile No </th>
		<th> Email </th>
		<th> House/Room </th>
		<th> Floor </th>
		<th> Category </th>
		<th> Rent </th>
		<!--<th> Status </th>-->
	</tr>
</thead>
<tbody>
<?php foreach($tenants->result() as $row):?>
	<tr>
		<td> <input name="checkbox[]" type="checkbox" id="checkbox[]" value="<?=$row->id; ?>"> </td>
		<td> <?php if($row->first_name !="" ||$row->middle_name!=""||$row->last_name!=""){?> <?=$row->first_name?> &nbsp; <?=$row->middle_name?> &nbsp; <?=$row->last_name?>   <?php }else {?><?=$row->company_name?> <?php } ?></td>
		<td> <?php if($row->company_mobile ==""){?> <?=$row->mobile_number?> <?php }else{?> <?= $row->company_mobile?>  <?php }?> </td>
		<td> <?=$row->tenant_email?> </td>
		<td class="center"> <?=$row->house_no?>  </td>
		<td>
			<?=$row->floor_no?> 
		</td>
		<td>
			<?=$row->category_name?>  
		</td>      
		<td>
			<?php $rent_paid=0; $rent_to_pay=0; 
				foreach($rent->result() as $r){
				if($r->tenant_id==$row->id){ $rent_to_pay=$rent_to_pay+$r->payment_type_value;}
				
			}
		
			echo $rent_to_pay;
			?> 
		</td>
		 
			  
	</tr>
	<?php endforeach;?>
</tbody>
</table>
<input type="submit" class="btn green"  <?=$disabled?> value="View Tenant ">
 
</form>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>              
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>

<div id="success" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Message </b></h5>
				<p id="success_msg">
				   
				</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
 
<div id="photo_modal" class="modal fade" tabindex="-1" data-width="400">
	  <div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Upload Photo </b></h5>
				<hr/>
				<p>
				<form action="<?=site_url('property/do_upload/'.$property_id);?>" method="post"  enctype="multipart/form-data">
				<img id="image_upload_preview" src="<?=base_url();?>media/<?=$media?>" width="300" height="150"> 
				   <input type="file" name="userfile[]" required  id="photo"  class="form-control">
				</p>
				</div>
			</div>
		</div>
	<div class="modal-footer" >  
		<button type="submit"   id="done_upload" class="btn green">Save changes</button>
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
	</div>
	</form>
</div>
 
<!-- END PAGE CONTENT BODY -->
<script language="javascript">
function validate()
{
var chks = document.getElementsByName('checkbox[]');
var hasChecked = false;
for (var i = 0; i < chks.length; i++)
{
if (chks[i].checked)
{
hasChecked = true;
break;
}
}
if (hasChecked == false)
{
alert("Please select at least one.");
return false;
}
return true;
}

$(function()
{

$("#upload_photo").click(function(){ 

	$("#photo_modal").modal('show');
});	
	 
	
}); 
 



function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#photo").change(function () {
        readURL(this);
    });
</script>

