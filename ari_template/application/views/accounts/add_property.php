<?php $properties=0; foreach($property->result() as $row){  $properties++; }?>
<?php $packages=0; foreach($package_account->result() as $r){   $packages++; }?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY --> 
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="<?=base_url();?>"> Home </a>
		<i class="fa fa-circle"> </i>
	</li>
	<li>
		<a href="#"> Property </a>
		<i class="fa fa-circle">    </i>
	</li>
	<li>
		<span> Add property </span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="row">
		<div class="col-md-12">
				<!-- BEGIN PROFILE SIDEBAR -->                         
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
					<div class="row">
					<div class="col-md-12">
					<div class="portlet light ">
					<div class="portlet-title tabbable-line">
					<div class="caption caption-md">
					<i class="icon-globe theme-font hide"></i>
					<span class="caption-subject font-blue-madison bold uppercase"> Property</span>
			</div>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab_1_1" data-toggle="tab"> Add Property </a>
		</li>
		<li>
			<a href="#tab_1_3" data-toggle="tab" id="refresh_items">Unit Details </a>
		</li>
		<li>
			<a href="#tab_1_4" data-toggle="tab"   class="tab_1_4_child">Payment Details </a>
		</li>		
	</ul>
</div>
		 
<div class="portlet-body"> 
<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
<div class="tab-pane active" id="tab_1_1">
<div class="row">
<div class="col-md-12">  
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Properties Basic details and Location </font>
	</div>
</div>
	<div class="col-md-12"> &nbsp; </div>	
</div>
<!--<form role="form" action="#">--->
 <form action="<?=site_url('property/add_property');?>" enctype="multipart/form-data" method="post" onsubmit="return input_validation()"> 
<?php //echo form_open('property/add_property');
?>
<div class="row">
	 
	<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Property Name </label> 
				<input type="text"   class="form-control" required onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" name="property_name" id="property name" onchange="checkChars('property name')" />
			</div>
	</div>
<div class="col-md-4">
	<div class="form-group">
			<label class="control-label">Property Type </label>
			   <select class="form-control" name="property_type" onchange="checkChars()">
					<option>Apartment</option>
					<option>Bungalow</option>
					<option>Flat</option>
					<option>Commercial</option>
					<option>Office</option>
				</select>
	</div> 
</div>

<div class="col-md-4">  
<div class="form-group">
			<label class="control-label">LR/NO</label>
			<input type="text"   class="form-control" name="lr_no" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="LR NO" onchange="checkChars('LR NO')"/> <!--onchange="checkChars('LR NO')"-->   
</div>

</div>
</div>

<div class="row"> 
<div class="col-md-4">
		 
		<div class="form-group">
			<label class="control-label">Total Floors </label>
			<input type="number"  min="0" max="120" class="form-control" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" name="floors"   id="floors" onkeypress="return checkIt(event)"/>
		</div>
	  
		<div class="form-group">
			<label class="control-label">Street</label>
			<input type="text"   class="form-control" name="street" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Street Name" onchange="checkChars('Street Name')"/> 
		</div>
</div>

<div class="col-md-4"> 
		<div class="form-group">
			<label class="control-label">Total parking spaces</label>
			<input  type="number" min="0" max="10000" value="0" class="form-control" name="car_spaces" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" onkeypress="return checkIt(event)" /> 
		</div>
		
		
			<div class="form-group">
				<label class="control-label">City/Town </label>
				<input type="text"  required  class="form-control" name="city_town"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="City Town" onchange="checkChars('City Town')"/>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Location Name</label>
				<input type="text"  required class="form-control" name="location" id="Location" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"  onchange="checkChars('Location')"/>
			</div>
			<div class="form-group">
			<label class="control-label"> Country </label>
			<select required class="form-control" name="country" id="Country"/> 
                        <option value="">Country</option>
                        <option value="AF">Afghanistan</option>
                        <option value="AL">Albania</option>
                        <option value="DZ">Algeria</option>
                        <option value="AS">American Samoa</option>
                        <option value="AD">Andorra</option>
                        <option value="AO">Angola</option>
                        <option value="AI">Anguilla</option>
                        <option value="AR">Argentina</option>
                        <option value="AM">Armenia</option>
                        <option value="AW">Aruba</option>
                        <option value="AU">Australia</option>
                        <option value="AT">Austria</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="BS">Bahamas</option>
                        <option value="BH">Bahrain</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BB">Barbados</option>
                        <option value="BY">Belarus</option>
                        <option value="BE">Belgium</option>
                        <option value="BZ">Belize</option>
                        <option value="BJ">Benin</option>
                        <option value="BM">Bermuda</option>
                        <option value="BT">Bhutan</option>
                        <option value="BO">Bolivia</option>
                        <option value="BA">Bosnia and Herzegowina</option>
                        <option value="BW">Botswana</option>
                        <option value="BV">Bouvet Island</option>
                        <option value="BR">Brazil</option>
                        <option value="IO">British Indian Ocean Territory</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BI">Burundi</option>
                        <option value="KH">Cambodia</option>
                        <option value="CM">Cameroon</option>
                        <option value="CA">Canada</option>
                        <option value="CV">Cape Verde</option>
                        <option value="KY">Cayman Islands</option>
                        <option value="CF">Central African Republic</option>
                        <option value="TD">Chad</option>
                        <option value="CL">Chile</option>
                        <option value="CN">China</option>
                        <option value="CX">Christmas Island</option>
                        <option value="CC">Cocos (Keeling) Islands</option>
                        <option value="CO">Colombia</option>
                        <option value="KM">Comoros</option>
                        <option value="CG">Congo</option>
                        <option value="CD">Congo, the Democratic Republic of the</option>
                        <option value="CK">Cook Islands</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CI">Cote d'Ivoire</option>
                        <option value="HR">Croatia (Hrvatska)</option>
                        <option value="CU">Cuba</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="DK">Denmark</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="EC">Ecuador</option>
                        <option value="EG">Egypt</option>
                        <option value="SV">El Salvador</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="ER">Eritrea</option>
                        <option value="EE">Estonia</option>
                        <option value="ET">Ethiopia</option>
                        <option value="FK">Falkland Islands (Malvinas)</option>
                        <option value="FO">Faroe Islands</option>
                        <option value="FJ">Fiji</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GF">French Guiana</option>
                        <option value="PF">French Polynesia</option>
                        <option value="TF">French Southern Territories</option>
                        <option value="GA">Gabon</option>
                        <option value="GM">Gambia</option>
                        <option value="GE">Georgia</option>
                        <option value="DE">Germany</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GR">Greece</option>
                        <option value="GL">Greenland</option>
                        <option value="GD">Grenada</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GU">Guam</option>
                        <option value="GT">Guatemala</option>
                        <option value="GN">Guinea</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="HT">Haiti</option>
                        <option value="HM">Heard and Mc Donald Islands</option>
                        <option value="VA">Holy See (Vatican City State)</option>
                        <option value="HN">Honduras</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HU">Hungary</option>
                        <option value="IS">Iceland</option>
                        <option value="IN">India</option>
                        <option value="ID">Indonesia</option>
                        <option value="IR">Iran (Islamic Republic of)</option>
                        <option value="IQ">Iraq</option>
                        <option value="IE">Ireland</option>
                        <option value="IL">Israel</option>
                        <option value="IT">Italy</option>
                        <option value="JM">Jamaica</option>
                        <option value="JP">Japan</option>
                        <option value="JO">Jordan</option>
                        <option value="KZ">Kazakhstan</option>
                        <option value="KE">Kenya</option>
                        <option value="KI">Kiribati</option>
                        <option value="KP">Korea, Democratic People's Republic of</option>
                        <option value="KR">Korea, Republic of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LV">Latvia</option>
                        <option value="LB">Lebanon</option>
                        <option value="LS">Lesotho</option>
                        <option value="LR">Liberia</option>
                        <option value="LY">Libyan Arab Jamahiriya</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="MO">Macau</option>
                        <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                        <option value="MG">Madagascar</option>
                        <option value="MW">Malawi</option>
                        <option value="MY">Malaysia</option>
                        <option value="MV">Maldives</option>
                        <option value="ML">Mali</option>
                        <option value="MT">Malta</option>
                        <option value="MH">Marshall Islands</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MU">Mauritius</option>
                        <option value="YT">Mayotte</option>
                        <option value="MX">Mexico</option>
                        <option value="FM">Micronesia, Federated States of</option>
                        <option value="MD">Moldova, Republic of</option>
                        <option value="MC">Monaco</option>
                        <option value="MN">Mongolia</option>
                        <option value="MS">Montserrat</option>
                        <option value="MA">Morocco</option>
                        <option value="MZ">Mozambique</option>
                        <option value="MM">Myanmar</option>
                        <option value="NA">Namibia</option>
                        <option value="NR">Nauru</option>
                        <option value="NP">Nepal</option>
                        <option value="NL">Netherlands</option>
                        <option value="AN">Netherlands Antilles</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NZ">New Zealand</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NE">Niger</option>
                        <option value="NG">Nigeria</option>
                        <option value="NU">Niue</option>
                        <option value="NF">Norfolk Island</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="NO">Norway</option>
                        <option value="OM">Oman</option>
                        <option value="PK">Pakistan</option>
                        <option value="PW">Palau</option>
                        <option value="PA">Panama</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PY">Paraguay</option>
                        <option value="PE">Peru</option>
                        <option value="PH">Philippines</option>
                        <option value="PN">Pitcairn</option>
                        <option value="PL">Poland</option>
                        <option value="PT">Portugal</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="QA">Qatar</option>
                        <option value="RE">Reunion</option>
                        <option value="RO">Romania</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="KN">Saint Kitts and Nevis</option>
                        <option value="LC">Saint LUCIA</option>
                        <option value="VC">Saint Vincent and the Grenadines</option>
                        <option value="WS">Samoa</option>
                        <option value="SM">San Marino</option>
                        <option value="ST">Sao Tome and Principe</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SN">Senegal</option>
                        <option value="SC">Seychelles</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SG">Singapore</option>
                        <option value="SK">Slovakia (Slovak Republic)</option>
                        <option value="SI">Slovenia</option>
                        <option value="SB">Solomon Islands</option>
                        <option value="SO">Somalia</option>
                        <option value="ZA">South Africa</option>
                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                        <option value="ES">Spain</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="SH">St. Helena</option>
                        <option value="PM">St. Pierre and Miquelon</option>
                        <option value="SD">Sudan</option>
                        <option value="SR">Suriname</option>
                        <option value="SJ">Svalbard and Jan Mayen Islands</option>
                        <option value="SZ">Swaziland</option>
                        <option value="SE">Sweden</option>
                        <option value="CH">Switzerland</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="TW">Taiwan, Province of China</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TZ">Tanzania, United Republic of</option>
                        <option value="TH">Thailand</option>
                        <option value="TG">Togo</option>
                        <option value="TK">Tokelau</option>
                        <option value="TO">Tonga</option>
                        <option value="TT">Trinidad and Tobago</option>
                        <option value="TN">Tunisia</option>
                        <option value="TR">Turkey</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TC">Turks and Caicos Islands</option>
                        <option value="TV">Tuvalu</option>
                        <option value="UG">Uganda</option>
                        <option value="UA">Ukraine</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                        <option value="UM">United States Minor Outlying Islands</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="VU">Vanuatu</option>
                        <option value="VE">Venezuela</option>
                        <option value="VN">Viet Nam</option>
                        <option value="VG">Virgin Islands (British)</option>
                        <option value="VI">Virgin Islands (U.S.)</option>
                        <option value="WF">Wallis and Futuna Islands</option>
                        <option value="EH">Western Sahara</option>
                        <option value="YE">Yemen</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
				</select>						
			</div> 
		</div>
		 
</div>

 
 
  
 <div class="row">
 
<div class="col-md-6">
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Water </font>
	</div>		
   <div class="col-md-12">  &nbsp;  </div>	
 
<div class="form-group">
			<label class="control-label"> 	Price per unit</label>
			<input type="number"   class="form-control" min="0" name="water_unit_cost" value="0" /> 
		</div>
 
</div>

<div class="col-md-6">
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Management Fee </font>
	</div>		
 
   <div class="col-md-12">  &nbsp;  </div>	
 
<div class="form-group">
	<label class="control-label"> Agents Management Fee (in percentage)</label>
	<input type="number"  max="100" class="form-control" min="0" name="management_fee" value="0" /> 
</div>
 
</div> 

</div>


<div class="row">  

  
<div class="col-md-12"> <hr/> <!--separator--->   </div>
 
<div class="col-md-3">
<div class="form-group">
		<input type="submit" <?=$disabled?> value="Save Details"  id="save_btn" class="btn green"  /> 
</div>
</div>	
<div class="col-md-3">
 
		
</div>
<div class="col-md-6">
&nbsp; <font color="blue" id="error"> </font><font color="green" id="suceess_msg"> </font>
</div>  
</form>
</div> 

</div>
<!-- END PERSONAL INFO TAB -->

<!-- CHANGE PASSWORD TAB -->
<div class="tab-pane" id="tab_1_3">
<div class="row" > 
<div class="col-md-12">
 <?php 
//echo form_open('property/add_units');
?> 
	<div class="form-group">
			<label class="control-label"> Choose Property </label>
			<select class="form-control" name="property" id="property_name1" onChange="getCategory(this.value,'','')"  required >
					<option value=""> Choose property  </option>
					<?php foreach($property->result() as $row):?> 
					<option value="<?=$row->id ?>"><?=$row->property_name?>  </option>
					<?php endforeach;?> 
			</select>
	</div>  
</div>
<div class="col-md-12">
&nbsp;
</div>
</div>


<div class="row">
<div class="col-md-12">
		<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff">Add  property's  Units <!-- Category Details--> </font>
				
		</div>
</div>
<div class="col-md-12">  &nbsp;  </div>	
</div>
 <!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit">
<div class="row"> 
<div class="col-md-6">
<div class="form-group">
		<label class="control-label">Unit Category </label>
		
		   <select class="form-control" name="category" id="category1" onchange="add_other(this.value)">
			<option>Select property first</option>
				<!--<option>Single Room</option>
				<!--<option>Single Room</option>
				<option>1 Bedroom</option>
				<option>2 Bedroom</option>
				<option>3 Bedroom</option>
				<option>4 Bedroom</option>
				<option>5 Bedroom</option>
				<option>1000 Sq ft </option> -->
			</select>
</div>
</div>	
<div class="col-md-6">
	<div class="form-group">
			<label class="control-label"> Total Units in this category </label>
			<input type="number" value="" min="1" max="10000" class="form-control" name="units" id="units1" /> 
	</div>
		
</div>

</div> 
<div class="row" id="hide_items"> 
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label"> Number of Baths </label>
			<input type="number" value="0" min="1" max="10000"  class="form-control" name="baths"  id="baths1"/> 
		</div> 
	</div>	
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label"> Fully Furnished </label>
				<select class="form-control" name="furnished" id="furnished1">
					 <option>No</option>
                                         <option>Yes</option>
				</select>
		</div> 
	</div>
</div>
<div class="row"> 
	<div class="col-md-4">
		<div class="form-group">
			<input type="submit"  <?=$disabled?> value="Save Category" id="add_category_details" class="btn green"  /> 
		</div>
	</div>	
	<div class="col-md-6"> 
	  <font id="messsage">   </font>
	</div>
<div class="col-md-2">
		&nbsp;
</div>
</div>
</div>
<!--</form>-->
<div class="row"> 
<div class="col-md-12">
	<div class="col-md-12" style="background:#1bb968;padding:6px;">
		<font color="#ffffff"> Current Units </font>
	</div>
</div>
</div>
 
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row"> 
<div class="portlet light portlet-fit ">
<div class="portlet-body">
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
	<thead>
		<tr> 
			<th> Property Name </th>
			<th> Category </th> 
			<th> Total Units </th>
			<th id="hide_col1"> Number of Baths </th>
			<th id="hide_col2"> Fully Furnished </th> 
			<th>  Edit </th> 
			<th>  Delete </th> 
		</tr>
	</thead>
	<tbody id="units_data">
		  
	</tbody>
</table>
</div> 
<!-- END EXAMPLE TABLE PORTLET-->			
 
</div>
</div>
</div>

 
 
<!-- CHANGE PASSWORD TAB -->
<div class="tab-pane" id="tab_1_4" style="min-height:500px">
<div class="row"> 
<div class="col-md-12">
	<div class="col-md-12" style="background:#1bb968;padding:6px;" >
		<font color="#ffffff"> Payment Details </font>
	</div>
</div>
<div class="col-md-12">  &nbsp;  </div>	
</div>
<div class="row"> 
<div class="col-md-6">
	<div class="form-group">
			<input type="hidden" id="pricing_category_title" value="">
			<label class="control-label"> Choose Property </label>
			<select class="form-control"  id="pricing_property_name" onChange="getPricingCategory(this.value)"  required >
					<option value=""> Choose property  </option>
					<?php foreach($property->result() as $row):?> 
					<option value="<?=$row->id ?>"><?=$row->property_name?>  </option>
					<?php endforeach;?> 
			</select>
	</div>  
</div>
<div class="col-md-6">
<div class="form-group">
		<label class="control-label">Category </label>
		   <select class="form-control" name="unit_category" id="unit_category" onchange="edit_pricing_details(this.value)"> 
		   
		   
			</select>
</div>
</div>
<div class="col-md-12">
&nbsp;
</div>
</div> 
  
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="row"> 
<div class="portlet light portlet-fit ">
<div class="portlet-body">
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
		<thead>
			<tr> 
				<th> Item </th>  
				<th> Payment  Amount </th>
				<th> Deposit </th> 
				<!--<th> Priority </th> --> 
				<th>Edit</th> 
				<th>Delete</th> 
			</tr>
		</thead>
		<tbody id="pricing_details">
			<td colspan="5" align="center">  No records found </td>  
		</tbody>
	</table>
	<p>	<font size="3"> Total Amount: Ksh.<b> <font id="pricing_total"> 0 </font> </b> </font> <font size="3"> Total Deposit: Ksh.<b> <font id="total_dep"> 0 </font> </b> </font> </p>
		<button  <?=$disabled?> class="btn green" id="add_new"> <i class="fa fa-plus"> </i> Add New </a> </button>
</div> 
<!-- END EXAMPLE TABLE PORTLET-->			
 
</div>
</div>
</div>

<!-- END  -->		
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->
 
<!-- END CONTAINER -->
<!-- pricing details modal -->
<div id="pricing_modal" class="modal fade" tabindex="-1" data-width="800">
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12"> 
					<h5><b> Payment Details  </b> <strong> &nbsp; &nbsp; <font id="pricing_category">  </font> </strong></h5> 
					
					 <input class="form-control" type="hidden" id="property_unit_id">  
					<table class="table" id="sample_editable_1">
					 <tr>
						<td> Payment Type:
							<select  class="form-control" id="item" onchange="add_other_payment(this.value)">
								<option value="">Select Item</option>
								<option>Rent</option>
								<option>Service charge</option> 
								<option>Gabage</option>
                                <option>Security</option>  
								<option>Internet</option>
								<option>Water Deposit</option>
								<option>Electricity Deposit</option>
								<option value="other">Other(specify your own)</option> 
							</select>
						</td>   
					<td> Amount: <input   class="form-control" type="number" min="0" max="1000000000" id="pricing_value" value="0" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" >  </td>
					<td> Deposit: <input   class="form-control" type="number" min="0" max="1000000000" id="pricing_deposit" value="0" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" >  </td>
					<!--<td> Priority:-->
						<input   class="form-control" type="hidden"  value="1" min="0" max="1000000000" id="priority" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" > 
					<!--</td> -->
					</tr>
					
					<tr>
					<td colspan="4">  <button class="btn green" id="save_pricing"> Save Changes </button>   
					</td>
					</tr>
					
					</table>
					<p id="status_msg">   </p>
				</div>
				</div>
				
<div class="row">			
<div class="col-md-12">			
	<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
		<thead>
			<tr> 
				<th> Item </th>  
				<th> Payment Amount  </th>
				<th> Deposit  </th> 
				<!--<th> Priority  </th> -->
				<th colspan="2"> Action  </th> 
			</tr>
		</thead>
		<tbody id="pricing_data">
			<td colspan="4" align="center">  No records found </td>  
		</tbody>
	</table>
		<p> Total Amount: <font id="total_price"> 0 </font>  Total Deposits: <font id="total_deposit"> 0 </font></p>
		 
			</div>
		</div>
	</div>
<div class="modal-footer" >  <span id="">    </span> &nbsp; 
 <button class="btn green" id="add_unit">  Add Unit </button>&nbsp; &nbsp; &nbsp; 
 <button class="btn green" id="add_tenant"> Add Tenant </button>&nbsp; &nbsp; &nbsp; &nbsp; 
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
	 
</div>
</div>
<!-- end  -->

<!-- responsive -->
<div id="responsive" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
			<!--<?php echo form_open('property/edit_units');?>-->
			<h5><b> Edit details for    <font id="category_title"> </font>  <font id="property_title"> </font></b></h5>
		<div class="form-group">
		<input   class="form-control" type="hidden" id="unit_id" value="">
		<input   class="form-control" type="hidden" id="property_id" value="">
		 
		<p>	<label class="control-label">Category </label>
			<select class="form-control" id="edit_category"> 
			
			
			</select>
		</p>
	</div>
	<p>
		<label class="control-label">Total Units </label>
		<input   class="form-control" type="text" id="total_units" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"  value="" >
	<span id="error1"> </span>
	</p>
	<p>
		<label class="control-label">Number of Baths </label>
		<input  class="form-control" type="text" id="no_of_baths" value="" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" > 
		<span id="error2"> </span>
		</p>
	<p>
		<p>
			<label class="control-label">Full Furnished </label>
			<select class="form-control" id="furnished">
				<option>Yes</option>
				<option>No</option>
			</select>
		</p>
		</div>
	</div>
</div>
<div class="modal-footer" > <span id="status_text">    </span> &nbsp;
	<button  class="btn green" id="done_edit"  <?=$disabled?>> OK </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>
 
<!-- responsive -->
<div id="add_new_category" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
			<h5> <b> Add Category </b> </h5>
			<p>
				<label class="control-label">Category Name</label>
				<input   class="form-control" required="required" type="text" id="category_name"> </p>
			<p>
			</div>
		</div>
	</div>
<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_category"> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</div>

 <div id="add_new_payment_modal" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
			<h5> <b> Add Payment Type </b> </h5><hr/>
			<p>
				<label class="control-label">Item Name </label>
				<input   class="form-control" required="required" placeholder="" type="text" id="new_payment_type"> </p>
			<p>
			</div>
		</div>
	</div>
<div class="modal-footer" > <span id="display_message" style="float:left"> </span>
	<button type="submit" class="btn green" id="add_pay"> Add </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
</div>
</div>
 
<div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5> <b> Success </b> </h5>
					<p>
						Data saved successfully  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>

<div class="modal fade draggable-modal" id="specify_category" tabindex="-1" role="basic" aria-hidden="true">
<div class="modal-header">
		<h4> <strong>  Property   saved Successfully </strong>  </h4>
</div>
<div class="modal-body" style="font-family:helvetica">
	<div class="row">
		<div class="col-md-12"> 
			 <h4> <strong>  Step 2: </strong> </h4> 
			 
				<p>   Add  the different units and categories  <img src="<?=base_url();?>images/Property-Icon.png"  width="30%" height="20%" align="right"> </p>
			    <p>e.g. 2 Bedroom, 500 sq feet  </p>
		</div>
	</div>
</div>
<div class="modal-footer" ><center>  
	<button type="button" data-dismiss="modal" id="got_it" class="btn green ">&nbsp;&nbsp;&nbsp; Got It  &nbsp;&nbsp;&nbsp; </button>
	</center>
</div>
<!-- /.modal-dialog -->
</div>


<div class="modal fade draggable-modal" id="units_saved_success" tabindex="-1" role="basic" aria-hidden="true">
<div class="modal-header">
		<h4>   Unit's   saved Successfully   </h4>
</div>
<div class="modal-body">
	<div class="row">
		<div class="col-md-12"> 
			 <h4> Step 3: </h4> 
			 
				<p>   Add  payment and deposit details  <img src="<?=base_url();?>images/cash.png"  width="30%" height="20%" align="right"> </p>
			 
		</div>
	</div>
</div>
<div class="modal-footer" ><center>  
	<button type="button" data-dismiss="modal" id="add_pricing" class="btn green ">&nbsp;&nbsp;&nbsp; Got It  &nbsp;&nbsp;&nbsp; </button>
	</center>
</div>
<!-- /.modal-dialog -->
</div>


<!-- responsive -->
<div id="edit_pricing" class="modal fade" tabindex="-1" data-width="600">
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
			<h5> <b> Edit payment details </b> </h5>
			<p>
				<label class="control-label"> Payment Type </label>
				<input   class="form-control"  type="text"  readonly id="payment_type_edit"> 
				<input   class="form-control"  type="hidden"   readonly id="id_edit"> 
				<input   class="form-control"  type="hidden"   readonly id="unit_id_edit"> 
				<input   class="form-control"  type="hidden"   readonly id="category_id_edit"> 
			</p>
			<p>
				<label class="control-label"> Amount </label>
				<input   class="form-control" required="required" type="text" id="amount_edit">
				<span id="error3"> 			</span>				
			</p>
			<p>
				<label class="control-label"> Deposit </label>
				<input   class="form-control" required="required" type="text" id="deposit_edit">
				<span id="error4"> 			</span>				
			</p>
			<!--<p>
				<label class="control-label"> Priority </label>-->
				<input type="hidden" min="1" value="1" max="1000" id="priority_edit" class="form-control" >
				 				
			<!--</p>-->
			</div>
		</div>
		 <p id="error_edit" style="float:left">  </p><br/><br/>
	</div>
	
<div class="modal-footer" >
	<button type="submit" class="btn green" id="done_pay_edit"  <?=$disabled?>> Save </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
</div>
</div>

 
  <div id="add_tenant_modal" class="modal fade" tabindex="-1" data-width="400">
	 <form action="<?=base_url();?>tenants/viewTenant" method="post">
	 <div class="modal-header">
 <p style="font-size:18px;color:green">   Add Tenant Message </p> 
		</div>
	 <div class="modal-body">
				<div class="row"> 
				 <div class="col-md-12">
					<p id="">
					   Add Either residential tenant or commercial tenant 
					</p>  
				</div>    
			</div>    
	</div>
	<div class="modal-footer">   
		<button type="submit" class="btn btn-success"><a style="color:#fff;text-decoration:none" href="<?=base_url()?>tenants/individual">Residential</a></button>
		<button type="submit" class="btn btn-success"><a style="color:#fff;text-decoration:none" href="<?=base_url()?>tenants/commercial">Commercial</a></button>
		 
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
	</div>
	</form>
</div>

<div id="alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	 
		<div class="modal-content">
			<div class="modal-header"> 
				<h4 class="modal-title">Alert  Message</h4>
			</div>
			<div class="modal-body">
				 <div style="line-height:25px">
				 <p> You have not  subscribed to any package. You can only add two properties and two tenants per property.   </p>   
				 <p> Please purchase a package to add more properties  and tenants </p>   
				 </div>
				<div class="modal-footer">
					<button class='btn green' onclick="window.location='<?=base_url();?>payment/package'" >
					           Subscribe Now
					</button> 
					<button class='btn red' onclick="window.location='<?=base_url();?>property'">
					           Subscribe Later
					</button> 
					 
				</div> 
		</div> 
</div> 
</div> 

<div id="warning" class="modal fade" tabindex="-1" data-width="400">
<div class="modal-body">
			<div class="row">
			<div class="col-md-12"> 
			<h5> <b  style="font-size:18px;color:brown">    Confirm Delete Action </b> </h5>
			<hr/>	
			<p>
				Are you sure you want to delete this unit?  This will also disassociate tenants with that unit
			</p> 
			<input type="hidden" id="del_id">  	<input type="hidden" id="del_unit_property_id">
			</div>
		</div>
</div>
<div class="modal-footer" >  
	<button type="button"  onclick="confirmDeleteUnit()"   <?=$disabled?> class="btn green">Yes</button>
	<button type="button" data-dismiss="modal" class="btn default">No</button>
</div>
</div>

<div id="deletePricing" class="modal fade" tabindex="-1" data-width="400">
<div class="modal-body">
			<div class="row">
			<div class="col-md-12"> 
			<h5> <b  style="font-size:18px;color:brown">    Confirm Delete Action </b> </h5>
			<hr/>	
			<p>
				Are you sure you want to remove this pricing item? 
			</p> 
			<input type="hidden" id="del_pricing_id">
			</div>
		</div>
</div>
<div class="modal-footer" >  
	<button type="button"  onclick="confirm_delete_pricing()"  <?=$disabled?> class="btn green">Yes</button>
	<button type="button" data-dismiss="modal" class="btn default">No</button>
</div>
</div>


 <div id="subscribe_users" class="modal fade" tabindex="-1" data-width="600">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b>  Add More Users </b></h5>
				<hr/>
				<p> You need to subscribe to be able to add more users to the system </p>
				<p> Do you want to start the subscription? </p>
				
			</div>
		</div>
</div>
<div class="modal-footer" >
<input type="hidden" id="status">
	<button type="button" class="btn green" id="confirm_subscription"> Yes </button>
	<button type="button" data-dismiss="modal"  class="btn red"> No </button> 
</div>
</div>

<input type="hidden" value="0" id="delete_items">


<script type="text/javascript"> 

function input_validation()
{
	var property_name=$("#property_name").val();
	var town=$("#town").val();
	var country=$("#Country").val();
	var street=$("#street").val();
	var floors=$("#floors").val();
	var location=$("#Location").val(); 
	// if(!property_name||!country||!street||!town||!floors||!location){  return false; }
	
}

function validate()
{
	var chks = document.getElementsByName('checkbox[]');
	var hasChecked = false;
	for (var i = 0; i < chks.length; i++)
	{
		if (chks[i].checked)
		{
			hasChecked = true;
			break;
		}
	}
	if (hasChecked == false)
	{
		alert("Please select at least one.");
		//jAlert('Please select at least one Unit to perform Edit action');
		return false;
	}
	return true;
}

$(function()
{   
	 
	// replaceCategory();
	$("#add_new").hide();
	
	var properties="<?=$properties?>";
	var packages="<?=$packages?>";  
	var info_msg="<?php echo $msg; ?>";
	var prop_id="<?php echo $id; ?>";
  if(properties>=2 && packages==0 && info_msg=="" && prop_id=="")
  {
	  $("#alert").modal({backdrop: "static"}); 
	  $('#alert').modal('show');
	  return false;
  }


$(".tab_1_4_child").click(function()
	{ 
		var id=("#unit_category").val(); 
		if(!id){
			clear_msg();
		}
		else{ 
				edit_pricing_details(id);
			} 
	}
	);
	
	$("#add_category").click(function()
	{ 
	var category=""; var id="";
	id=$("#property_name1").val(); category=$("#category_name").val();
	if(!id){ $("#status_message").html("<p style='color:red'> Please select property </p>"); return false;} else if(!category){ $("#status").html("<p style='color:red'> Empty field not required </p>"); $("#category_name").focus(); return false;}
	$("#status_message").html("<p> Adding... </p>");
   $.ajax(
   {
	   url:"<?=base_url();?>property/add_category/",
	   type:"POST",
	   async:false,
	   data:
	   {
		  'property_id':$("#property_name1").val(), 
		  'category_name':category 
	   },
	   success:function(data)
	   {
		   var obj=JSON.parse(data);
		   if(obj.result=="ok")
		   {
				$("#category_name").val('');
				$("#status_message").html("<font color='green'> <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> "+obj.msg+" </font>"); 
				getCategory(id,obj.id,obj.name);
				// 
				setTimeout(function()
				{
					$('#add_new_category').modal('hide');
					$("#status_message").empty();
				}, 2500); 	     
		   }
		   else
		   {
			   $("#category_name").val('');
			   $("#status_message").empty();
			  $("#status_message").html("<font color='red'>"+obj.msg+"</font>");  
		   }
	   }
   }
   )
	
}); 
 
$("#add_pay").click(function()
	{ 
		var pay=$("#new_payment_type").val();   
		if(!pay){ $("#new_payment_type").focus(); return false;} 
		$("#display_message").html("<p> Adding... </p>");
		$("#item").append("<option>"+pay+"</option>");
		$("#item").val(pay);
		$("#new_payment_type").val(''); 
		$("#display_message").html("<font color='green'> <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> Payment type Added </font>"); 
		 
		setTimeout(function()
				{
					$("#display_message").empty();
					$('#add_new_payment_modal').modal('hide'); 
				}, 1500); 
	}); 
  
});
  
 
 $(document).ready(function(){
	 
 checkPrivilege(); 
document.getElementById('unit_category').disabled = true;
 
 var msg="<?php echo $msg; ?>";
 var id="<?php echo $id; ?>";
 var name="<?php echo $property_name; ?>";
 if(msg==""&&id =="" && name =="")
 {  
	  //do nothing  
 }
else
 {
	 //show modal  
	 $('#specify_category').modal('toggle');  
 }
 
 

$("#got_it").click(function()
	{
			// onclick show category tab  
		//window.location.replace("<?=base_url();?>property/add#tab_1_3"); 
        $('.nav-tabs a[href="#tab_1_3"]').tab('show');
        //$("#property_name1").empty();
		//$("#property_name1").append("<option value='"+id+"'> "+name+" </option>");
		$("#property_name1").val(id);
		getCategory(id,'','');
		
});

	
$("#refresh_items").click(function()
{
	if(!id){
			
		}else{
		$("#property_name1").val(id);
		getCategory(id,'','');
		} 
});

 $("#done_edit").click(function(){   
    
	var unit_id=$("#unit_id").val();
	var property_id=$("#property_id").val();
	//var property_name=$("#property").val();
	var category=$("#edit_category").val();
	var total_units=$("#total_units").val();
	var no_of_baths=$("#no_of_baths").val();
	var furnished=$("#furnished").val(); 
	if(!total_units){ $("#total_units").focus(); return false;} 
	if(!no_of_baths){ $("#no_of_baths").focus(); return false;} 
	if(/^[a-zA-Z0-9- ]*$/.test(total_units) == false){ $("#total_units").focus(); $("#error1").html("<font color='red'>Total units value contains Illegal Characters </font>");  return false; }else{$("#error1").empty();}
	if(/^[a-zA-Z0-9- ]*$/.test(no_of_baths) == false){ $("#no_of_baths").focus(); $("#error2").html("<font color='red'>Number of Baths contains Illegal Characters </font>");  return false; } else{$("#error2").empty();} 
	$("#status_text").empty(); 
	$("#status_text").html("<font color='blue'> Updating....</font>");
	$.ajax(
	{
		url:"<?=base_url();?>property/doneEdit/",
		type:"POST",
		async:false,
		data:
		{
		'id':unit_id,
		'property_id':property_id,
		//'property_name':property_name,
		'category':category,
		'total_units':total_units,
		'baths':no_of_baths,
		'furnished':furnished
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
					$("#status_text").html("<i class='fa fa-check'> </i><font color='green'> Edit done successfully </font>");
					 showUnits(property_id);
					  setTimeout(function()
					  {
							$("#status_text").empty();
							$('#responsive').modal('hide')
                      }, 3000);
					
					  
			 }
			 else
			 {
				 $("#status_text").empty();
				 $("#status_text").html(" <font color='red'> Not updated. You have made no changes to save </font>");
			 }
		}
		
	}
	
	)
 });
 
 $("#add_category_details").click(function(){
	 var name=$("#property_name1").val();
	 var baths=$("#baths1").val();
	 var units=$("#units1").val();
	 var category=$("#category1").val();       
	 if(!name){ $("#messsage").html("<p style='color:red'> Please select property </p>"); return false;} else if(!units||!category){ $("#messsage").html("<p style='color:red'> Empty field not required </p>"); $("#units1").focus(); return false;}
	 
	$.ajax({
		url:"<?=base_url();?>property/add_units",
		type:"POST",
		data:
		{
			'property':$("#property_name1").val(),
			'units':$("#units1").val(),
			'baths':$("#baths1").val(),
			'furnished':$("#furnished1").val(),
			'category':$("#category1").val()
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok"){
			getCategory($("#property_name1").val());
			$("#baths1").val('');
			$("#units1").val('');
			$("#property_unit_id").val(obj.unit_id);
			$("#messsage").html("<font color='green'> <i class='glyphicon glyphicon-ok' style='font-size:18px;'>  </i>  "+obj.msg+"</font>");
			setTimeout(function()
			{
				$("#messsage").empty();
				showUnits(name);
			}, 2500); 
			$("#pricing_category").html(obj.property+ ", "+obj.category);  
			$("#pricing_category_title").val(obj.property+ ", "+obj.category);  
			$("#pricing_data").empty();	 
			$("#units_saved_success").modal('show');
			
		}
			else{
				$("#messsage").html("<font color='red'>" +obj.msg+"</font>");
				
			}
		}
		
		
	})
	 
 }); 
 
 
$("#add_pricing").click(function(){
    $("#total_price").html('0');	
    $("#total_deposit").html('0');	
    $("#status_msg").empty();	
    $("#priority").val(1);	
	$("#pricing_modal").modal('show'); 
 });
  
  $("#save_pricing").click(function(){  
  $("#status_msg").empty();
	var item=$("#item").val(); 
	var amount=$("#pricing_value").val();
	var deposit=$("#pricing_deposit").val();
	var priority=$("#priority").val();  
	var unit_id=$("#property_unit_id").val(); 	
	if(!item){ $("#item").focus(); return false;}	
	if(!amount){ $("#pricing_value").focus(); return false;}	
	if(amount<0){ $("#pricing_value").focus(); return false;}	
	if(!deposit){ $("#pricing_deposit").focus(); return false;}	
	if(deposit<0){ $("#pricing_deposit").focus(); return false;}	
	if(!priority){ $("#priority").focus(); return false;}	 
	$.ajax({
		url:"<?=base_url();?>property/insertPricing_details",
		type:"POST",
		data:
		{ 
			'unit_id':unit_id,
			'payment_type':item,
			'payment_type_value':amount,
			'pricing_deposit':deposit,
			'priority':priority 
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				$("#status_msg").html("<font color='green'> "+obj.msg+"</font>");
				$("#priority").val(parseInt(priority,10)+1);
				$("#item").val(''); 
				$("#pricing_value").val('0');
				$("#pricing_deposit").val('0');
				show_pricing_details(unit_id);
				getPricing_details(unit_id);
			}
			else
			{
				$("#status_msg").html("<font color='red'> "+obj.msg+"</font>");
				 
			}
		}
	})
	
  });
 
 $("#done_pay_edit").click(function(){
	$("#error_edit").empty();
	$("#pricing_data").empty();	
	$("#pricing_details").empty();	
	var payment_type=$("#payment_type_edit").val();
	var amount=$("#amount_edit").val(); 
	var deposit=$("#deposit_edit").val();  
	var id=$("#id_edit").val();	 
	var unit_id=$("#unit_id_edit").val();  	 
	var priority=$("#priority_edit").val(); 
	if(!item){ $("#payment_type_edit").focus(); return false;}	
	if(!amount){ $("#amount_edit").focus(); return false;}	
	if(amount<0){ $("#amount_edit").focus(); return false;}	
	if(!deposit){ $("#deposit_edit").focus(); return false;}	
	if(deposit<0){ $("#deposit_edit").focus(); return false;}	
	if(!priority){ $("#priority_edit").focus(); return false;}	
    if(/^[a-zA-Z0-9- ]*$/.test(amount) == false){ $("#amount_edit").focus(); $("#error3").html("<font color='red'>Amount contains illegal characters </font>");  return false; }else{$("#error3").empty();}
	if(/^[a-zA-Z0-9- ]*$/.test(deposit) == false){ $("#deposit_edit").focus(); $("#error4").html("<font color='red'>Deposit Value contains illegal characters</font>");  return false; } else{$("#error4").empty();} 
		
	var property_category_id=$("#category_id_edit").val(); 
document.getElementById('done_pay_edit').disabled = true;	
	$.ajax({
		url:"<?=base_url();?>property/updatePricing_details",
		type:"POST",
		data:
		{
			'id':id,
			'unit_id':unit_id,
			'payment_type':payment_type,
			'payment_type_value':amount,
			'deposit':deposit,
			//'property_category_id':property_category_id,
			'priority':priority 
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{  
		 
				$("#error_edit").html("<font color='green'> "+payment_type+" payment updated successfully</font>");
				getPricing_details(unit_id);
				show_pricing_details(unit_id);
                setTimeout(function()
				{
					document.getElementById('done_pay_edit').disabled = false;
				  $("#edit_pricing").modal('hide'); 
               },2000);
			}
			else
			{
				document.getElementById('done_pay_edit').disabled = false;
				$("#error_edit").html("<font color='red'> You have made no changes to save </font>"); 
			}
		}
	})
	
  });
 
	$("#add_unit").click(function()
	{ 
	     $("#pricing_modal").modal('hide');
	      $('.nav-tabs a[href="#tab_1_3"]').tab('show');
	});
	
	$("#add_tenant").click(function()
	{  
	 $("#pricing_modal").modal('hide');
	 $("#add_tenant_modal").modal('show');
	});
	
	$("#add_new").click(function()
	{ 
	     var id=$("#property_unit_id").val();  
		 show_pricing_details(id);
		// var category=$("#unit_category").val();
		//$("#pricing_category").html(category);
		$("#total_price").val('0');
		$("#pricing_modal").modal('show');
	});

});
 
 
function showUnits(id){
		var content="";	
		$.ajax({
		url:'<?=base_url();?>property/show_units/'+id, 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data); 
			var data = obj.data;
			$("#units_data").empty(); var y=0;
			for(var i=0; i<data.length; i++)
			{
				var unit = data[i]; 
				content=content+"<tr>"+
				 "<td> "+unit['property_name']+"</td> <td>"+unit['category_name']+"</td> <td>"+unit['total_units']+"</td> <td class='center'>"+unit['number_of_baths']+"</td>"+
				"<td>"+unit['fully_furnished']+"</td> <td class='center'><a data-toggle='modal' onclick=\"editUnit('"+unit['unit_id']+"','"+unit['property_name']+"')\" href='#' class='btn green'><i class='fa fa-edit'> </i> Edit </a></td><td class='center'><a data-toggle='modal' onclick=\"deleteUnit('"+unit['unit_id']+"','"+id+"')\" href='#' class='btn red'><i class='fa fa-trash'> </i> Delete </a></td>  </tr>";
			y++;}
			$("#total_units").val(y);
			$("#units_data").html(content);
		}
	});
	return false;
	}

function getCategory(id,id2,category){
		var prop=$("#property_name1").val();  
		if(!prop){	return false;	}  
		$("#status").empty();
		$("#category1").empty();  
		if(!id2||!category){ 
		$("#category1").append("<option value=''> Select category </option>");
        var x=replaceCategory(id);    if(x==true){   $("#property_name1").focus(); 	return false;	}  
		}
		else if(id2 !="" && category !=""){
			$("#category1").append("<option value='"+id2+"'> "+category+" </option>");
		}else{  
		$.ajax({
		url:'<?=base_url();?>property/property_Category/'+id, 
		type: 'POST', 
		success:function (data)
		{ 
			var obj = JSON.parse(data); 
			var data = obj.data;
			
			//$("#category1").append("<option value=''> Select category </option>");
			for(var i=0; i<data.length; i++)
			{
				var category = data[i]; 
				$("#category1").append("<option value='"+category['id']+"'> "+category['category_name']+" </option>");
			}
			$("#category1").append("<option value='other'> Other(specify your own) </option>");
			showUnits(id);
			
		}
	});
	
		}
	return false;
} 
function getPricingCategory(id){
		var prop=$("#pricing_property_name").val(); 
$("#pricing_details").empty(); 
        $("#total_price").html('0');
		$("#total_dep").html('0'); 
		$("#pricing_total").html('0');
		$("#unit_category").empty();		
		if(!prop){	
		$("#unit_category").append("<option value=''> Select property first </option>"); 
		return false;	
		}
		//$("#status").empty(); 
		
		$("#unit_category").append("<option value=''> Select category </option>");
		$.ajax({
		url:'<?=base_url();?>property/getCategory/'+id, 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data); 
			var data = obj.data;
			
			//$("#category1").append("<option value=''> Select category </option>");
			for(var i=0; i<data.length; i++)
			{
				var category = data[i]; 
				$("#unit_category").append("<option value='"+category['property_category_id']+"'> "+category['category_name']+" </option>");
			} 
			document.getElementById('unit_category').disabled = false;
			showUnits(id); 
		}
	});
	return false;
} 
	
function add_other(value)
  {
	  $("#pricing_data").empty();	
	  if(value=="other")
	  {
		$("#status_message").empty();
		$("#add_new_category").modal('show');
	  }
  }
 function add_other_payment(value)
  { 	
		$("#total_price").val('0');
		$("#total_deposit").val('0');
	  if(value=="other")
	  {
		$("#add_new_payment_modal").modal('show');
	  }
  }
 
 
  function editUnit(id,property_name){  

		$.ajax({
		"url":"<?=base_url();?>property/getUnits/"+id,
        "type":"post",
		"success":function(data)
		{
			var obj = JSON.parse(data);  
			var data = obj.data;
            for(var i=0; i<data.length; i++)
			{
                    var units = data[i]; 
					$("#unit_id").val(id);
					$("#property_id").val(units['property_id']); 
					$("#edit_category").empty();
					//$("#edit_category").append("<option>"+ units['property_category_id']+"</option>");
					$("#edit_category").append("<option>"+ units['category_name']+"</option>");
					$("#total_units").val(units['total_units']);
					$("#no_of_baths").val(units['number_of_baths']);
					$("#furnished").val(units['fully_furnished']);
					$("#property_title").html(property_name);
					//$("#category_title").html(units['category_name']); 
				}
			 $("#responsive").modal('show');
		}
	});
	
} 
	
function deleteUnit(id,property_id){  
       
	  var c='<?=$disabled?>';
	 if(c=="disabled"){ return false;}
	 var a=$("#delete_items").val();  
	 if(a==0){
		alert('You have no privilege to remove this unit!'); 
		return false;
	 }
	 $("#del_id").val(id); $("#del_unit_property_id").val(property_id);
	  //var c=confirm("Are you sure you want to delete this unit?"); 
	  
	  $('#warning').modal('show');
	
} 	

  function confirmDeleteUnit(){
	var id= $("#del_id").val(); var property_id=$("#del_unit_property_id").val();  
	 
		$.ajax({
		"url":"<?=base_url();?>property/deleteUnit/"+id,
        "type":"post",
		"success":function(data)
		{
			var obj=JSON.parse(data); 
			if(obj.result=="ok")
			{   
			     $('#warning').modal('hide');
				 showUnits(property_id);
			 }
			 else
			 {
				alert('Unit not deleted!'); 
			 }
		}
	});
}  


function show_pricing_details(id){
		var content="";	  
		$("#pricing_data").empty();
$("#pricing_data").html("<tr> <td colspan='5' align='center'> <b>  Loading...please wait </b> </td> </tr>");
		$.ajax({
		url:'<?=base_url();?>property/getPricing_details/'+id, 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data); 
			var data = obj.data;
			if(obj.result=="ok")
			{ 
				
				var total=0; var total_deposit=0;
				for(var i=0; i<data.length; i++)
				{
					var pricing = data[i]; 
					content=content+ "<tr>"+
						"<td>"+pricing['payment_type']+"</td>  <td> "+pricing['payment_type_value']+"</td> <td> "+pricing['deposit']+"</td>     <td class='center'> <a data-toggle='modal' href='javascript:;' onclick=\"edit_pricing('"+pricing['pricing_details_id']+"','"+pricing['unit_id']+"','"+pricing['payment_type']+"','"+pricing['payment_type_value']+"','"+pricing['deposit']+"','"+pricing['priority']+"')\" ><i class='fa fa-edit'></i> Edit </a></td> <td class='center'><a data-toggle='modal' onclick=\"delete_pricing('"+pricing['pricing_details_id']+"')\" href='#' class='btn red' > <i class='fa fa-trash-o'> </i> Remove </a></td>  </tr>";
						total=total+parseInt(pricing['payment_type_value']);
						total_deposit=total_deposit+parseInt(pricing['deposit']);
				}
				$("#pricing_data").empty();
                $("#pricing_data").html(content);
				$("#pricing_total").html(total); 
				$("#total_deposit").html(total_deposit);
				
									
			}
			else
			{
				$("#pricing_data").html("<tr><td colspan='5'> No payment details added</td>  </tr>");
			}
		}
	});
	return false;
}
 
function delete_pricing(id)
 {    
	 var c='<?=$disabled?>';
	 if(c=="disabled"){ return false;}
	 var a=$("#delete_items").val();
	 if(a==0){
		alert('You have no privilege to remove this item!'); 
		return false;
	 }
	 $("#del_pricing_id").val(id);
	 $("#deletePricing").modal('show');
	 
 }
 
 function confirm_delete_pricing()
 {
     var id=$("#del_pricing_id").val();
     $.ajax({
		url:"<?=base_url();?>property/remove_pricing_details/",
		type:"POST",  
		data:{ 'id':id},
		success:function(data)
		{ 
			var obj=JSON.parse(data); 
			var data = obj.data;
			if(obj.result=="ok")
			{   
			    $("#deletePricing").modal('hide');
				show_pricing_details(obj.unit_id); 
				getPricing_details(obj.unit_id);
			}
			else
			{
				alert('Not deleted Try again later');
				return false;
			}
		}
	 }) 
     
 }
 
 
function all_Units(id)
{
		var content="";	
		$.ajax({
		url:'<?=base_url();?>property/show_units/'+id, 
		type: 'POST', 
		success:function (data)
		{
			var obj = JSON.parse(data); 
			var data = obj.data;
			$("#units_data").empty();
			for(var i=0; i<data.length; i++)
			{
				var unit = data[i]; 
				content=content+"<tr> <td><input name='checkbox[]' type='checkbox' id='checkbox[]' value='"+unit['id']+"'>	</td>"+
				 "<td> "+unit['property_id']+"</td> <td>"+unit['property_category_id']+"</td> <td>"+unit['total_units']+"</td> <td class='center'>"+unit['number_of_baths']+"</td>"+
				"<td>"+unit['fully_furnished']+"</td> <td class='center'> <a data-toggle='modal' onclick=\"editUnit('"+unit['id']+"')\" href='#' class='btn green'><i class='fa fa-edit'> </i> Edit </a> </td>  </tr>";
			}
			$("#units_data").html(content);
		}
	});
	return false;
}
 
 
 
function edit_pricing_details(id){
		var content="";
		var total=0; var  deposit=0;	  
		$("#pricing_details").empty(); 
        $("#total_price").html('0');
		$("#total_dep").html('0'); 
		$("#pricing_total").html('0');		
		var prop_id=$("#pricing_property_name").val();  
        if(prop_id==""||prop_id==null||id==""||id==null){ return false;}		
		$.ajax({ 
		url:'<?=base_url();?>property/view_pricing_details/'+id+"/"+prop_id, 
		type: 'POST', 
		success:function (data)
		{ 
			var obj = JSON.parse(data); 
			var data = obj.data;
			$("#property_unit_id").val(obj.unit_id);
			if(obj.result=="ok")
			{  
				  
				for(var i=0; i<data.length; i++)
				{
					var pricing = data[i]; 
					content="<tr>"+
						"<td>"+pricing['payment_type']+"</td>  <td> "+pricing['payment_type_value']+"</td>  <td> "+pricing['deposit']+"</td>    <td class='center'><a href='javascript:;' onclick=\"edit_pricing('"+pricing['pricing_details_id']+"', '"+pricing['unit_id']+"','"+pricing['payment_type']+"','"+pricing['payment_type_value']+"','"+pricing['deposit']+"','"+pricing['priority']+"')\" ><i class='fa fa-edit'> </i> Edit </a></td> <td class='center'><a onclick=\"delete_pricing('"+pricing['pricing_details_id']+"')\" href='#'> <i class='fa fa-trash-o'> </i> Remove </a></td>  </tr>";
						total=total+parseInt(pricing['payment_type_value']);
						 deposit= deposit+parseInt(pricing['deposit']); 	
					$("#add_new").show();
					$("#total_items").val(i+1); 
					$("#pricing_details").append(content);
	               					
				} 
				$("#total_price").html(total);
				$("#total_dep").html(deposit); 
				$("#pricing_total").html(total);
			}
			else
			{
				$("#total_price").html(total);
				$("#total_dep").html(deposit); 
				$("#pricing_total").html(total);
				$("#add_new").show();
				$("#pricing_details").html("<tr><td colspan='5'> No payment details added</td>  </tr>");
			}
		}
	});
	 
}
 
function getPricing_details(id){
	$("#category_unit_id").val(id);
		var content="";	  
		$("#pricing_details").empty();
	
		$.ajax({
		url:'<?=base_url();?>property/getPricing_details/'+id, 
		type: 'POST', 
		success:function (data)
		{ 
			var obj = JSON.parse(data); 
			var data = obj.data;
			if(obj.result=="ok")
			{ 
				
				var total=0; var deposit=0;
				for(var i=0; i<data.length; i++)
				{
					var pricing = data[i]; 
					content="<tr>"+
						"<td> "+pricing['payment_type']+" </td>  <td> "+pricing['payment_type_value']+" </td> <td> "+pricing['deposit']+"</td>    <td class='center'><a href='javascript:;' onclick=\"edit_pricing('"+pricing['pricing_details_id']+"', '"+pricing['unit_id']+"','"+pricing['payment_type']+"','"+pricing['payment_type_value']+"','"+pricing['deposit']+"','"+pricing['priority']+"')\" ><i class='fa fa-edit'> </i> Edit </a></td> <td class='center'><a   onclick=\"delete_pricing('"+pricing['pricing_details_id']+"')\" href='#'  > <i class='fa fa-trash-o'> </i> Remove </a></td>  </tr>";
							total=total+parseInt(pricing['payment_type_value']);
							deposit= deposit+parseInt(pricing['deposit']);
							$("#pricing_details").append(content);	
				} 
				
				$("#total_price").html(total); 
				$("#total_deposit").html(deposit);
				$("#total_dep").html(deposit);
				$("#pricing_total").html(total);
				
				 
			}
			else
			{
                                $("#total_price").html(0);
				$("#total_deposit").html(0);
				$("#pricing_details").html("<tr><td colspan='5'> No payment details added</td>  </tr>");
			}
		}
	});
	 
	} 
 
 
function edit_pricing(id,unit_id,name,amount,deposit,priority)
 { 
	 $("#error_edit").empty();
	 $.ajax({
		url:"<?=base_url();?>property/edit_pricing_details/"+id+"/"+name+"/"+amount,
		type:"POST", 
		async:false,
		success:function(data)
		{  	var total=0; 
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				$("#payment_type_edit").val(name);
				$("#amount_edit").val(amount); 
				$("#unit_id_edit").val(unit_id); 
				$("#id_edit").val(id); 
				$("#priority_edit").val(priority); 
				$("#deposit_edit").val(deposit); 
				//$("#category_id_edit").val(category_id); 
				$("#edit_pricing").modal('show');
			}
			else
			{
				alert('Not payment records found for that unit!');
				//return false;
			}
		}
	 })
	 
	 } 
 
function replaceCategory(id)
{
	var x=0;
    var prop=$("#property_name1").val(); 
	$.ajax({
		url:"<?=base_url();?>property/getData/"+prop,
		type:"POST", 
		async:false,
		success:function(data)
		{ 
			var obj=JSON.parse(data);  
			var data = obj.data;  
			if(obj.result=="ok")
			{  
                            
				for(var i=0; i<data.length; i++)
				{
					var p= data[i];
                                        var property_type=p['property_type']; 
                                        if(property_type=="Office"||property_type=="Commercial"){
                                        $("#hide_items").hide();
										 
                                        getPropertyCategory(id,"Office");
                                        x=1;
                                        }else
										{ 
										var x="";
										$("#hide_items").show();
										getPropertyCategory(id,x);
										//x=0;
										x=1;
									}

                                 }
  
 
                        }

                 }

}); 
 
if(x==1){  return true;} if(x==0){   return false;}
}

function getPropertyCategory(id,c){
		var prop=$("#property_name1").val();  
		if(!prop){	return false;	}  
		$("#category1").empty();
        if(id==""){	}else{
		$("#status").empty(); 
		$.ajax({
		url:'<?=base_url();?>property/get_property_Category/'+c, 
		type: 'POST', 
		success:function (data)
		{  
			var obj = JSON.parse(data); 
			var data = obj.data;
			 
			//$("#category1").append("<option value=''> Select category </option>");
			for(var i=0; i<data.length; i++)
			{
				var category = data[i]; 
				$("#category1").append("<option value='"+category['id']+"'> "+category['category_name']+" </option>");
			}
			$("#category1").append("<option value='other'> Other(specify your own) </option>");
			showUnits(id);
		}
		 
	});

	 }
	 
} 


 function clear_msg()
 {
	 $("#total_deposit").empty();
	 $("#total_price").empty();
	 $("#pricing_total").empty();
	 $("#pricing_details").empty(); 
	 $("#total_dep").empty();
 }
 
function checkPrivilege()
 { 
	 
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/1",
		type:"POST",  
		success:function(data)
		{ 
			var obj=JSON.parse(data);
			var data = obj.data;  
			if(obj.add==0)
			{  
				 document.getElementById('save_btn').disabled = true;
				document.getElementById('add_category_details').disabled = true;
				document.getElementById('add_new').disabled = true;
				 
		      //$("#save_btn").html("<font color='' onclick=\"alert('You have no privilege to add property')\"> Save Details </font>"); 
			} 
			if(obj.view==0){ 
				//document.getElementById('pricinng_property_name').disabled = true;  
			}
			if(obj.edit==0){  
				 document.getElementById('done_edit').disabled = true;
				 document.getElementById('done_pay_edit').disabled = true;  
			}
		    
			 $("#delete_items").val(obj.delete);  
				//document.getElementById('delete_btn').disabled = true;   
		 
		}
	 })
 } 
 
 
 function validateMail() {
     
    var x = $("#Email").val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
	{
        alert("Not a valid e-mail address");
     $("#Email").val('');

        return false;
    }
 
}
</script>