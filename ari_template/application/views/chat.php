<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Chat-Example | CodeIgniter</title>
	 
	<!-- http://bootsnipp.com/snippets/4jXW -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chat.css" />
	
<style>
 *{padding:0;margin:0;}

body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px; 
}

.float{
	position:fixed;
	width:100px;
	height:50px;
	bottom:20px;
	right:30px;
	background-color:#0C9;
	color:#FFF;font-size:12px;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #000;
}
 #scroll{
	position:fixed; 
	bottom:0px;
	right:40px; 
	color:#000;font-size:12px;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}
 

.my-float{
	margin-top:22px;
}
#scroll{
	margin-top:22px;
}
</style>

<script type="text/javascript">	  
		$( document ).ready ( function () {
			$('#msg_block').show();
			//$('#success').modal('show');
			$('#nickname').keyup(function() {
				var nickname = $(this).val();
				
				if(nickname == ''){
					//$('#msg_block').hide();
				}else{
					$('#msg_block').show();
				}
			});
			
			// initial nickname check
			$('#nickname').trigger('keyup');
		});
		
</script>

</head>
<body>



<div class="container">

    <div class="row">
		<p>  </p>
		 <div  id="hide" style="position:fixed; top:38%; height:500px;bottom:0%;left:70%;right:2%;" >
		<div style="padding-bottom:8px;color:#fff;background:#006699;">
				<span class="glyphicon glyphicon-comment"> </span> Chat with us
				 <a href="javascript:;" style="float:right;color:#fff;font-size:20px" id="minimize" title="minimize"> &nbsp; - &nbsp; </a>
		</div> 
		
			<div class="panel panel-primary" id="scroll" style="position:fixed; top:38%; bottom:0%;left:70%;right:2%;overflow-y:scroll;min-height:200px">
			 <div class="panel-body">
				<ul class="chat" id="received">
					
					
				</ul>
			</div>
			<div class="panel-footer">
				<div class="clearfix">
					<!--<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">
								Nickname:
							</span>-->
							<input id="nickname" value="<?=$this->session->userdata('first_name')?>" type="hidden" class="form-control input-sm" placeholder="Nickname..." />
						<!--</div>
					</div>-->
					<div class="col-md-12" id="msg_block">
						<div class="input-group">
							<input id="message" type="text" class="form-control input-sm" placeholder="Type your message here..." />
							<span class="input-group-btn">
								<button class="btn btn-warning btn-sm" id="submit">Send</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    </div>
	
	 <div class="row">
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/>
		 Welcome<br/> 
    </div>
	
	
<a href="#" class="float">
	<i class="fa fa-wechat my-float" style="font-size:24px"></i> Support
</a>
</div>



</body>
</html>

<div class="modal fade" id="success" tabindex="-1" role="basic" aria-hidden="true">
	
	<div class="modal-body"><div class="modal-header" id="modal-header">  </div>
	<div class="row">
	<div class="col-md-12">  
		 
		<div class="panel-body">
				 
			</div>
		 
		</div>
	</div>
</div>
<div class="modal-footer" ><center>  
		<button type="button" data-dismiss="modal" id="got_it" class="btn green ">&nbsp;&nbsp;&nbsp; Got It  &nbsp;&nbsp;&nbsp; </button>
		</center>
</div>
<!-- /.modal-dialog -->
</div>
<script type="text/javascript">

var request_timestamp = 0;

var setCookie = function(key, value) {
	var expires = new Date();
	expires.setTime(expires.getTime() + (5 * 60 * 1000));
	document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

var getCookie = function(key) {
	var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
	return keyValue ? keyValue[2] : null;
}

var guid = function() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

if(getCookie('user_guid') == null || typeof(getCookie('user_guid')) == 'undefined'){
	var user_guid = guid();
	setCookie('user_guid', user_guid);
}


// https://gist.github.com/kmaida/6045266
var parseTimestamp = function(timestamp) {
	var d = new Date( timestamp * 1000 ), // milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		timeString;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}

	timeString = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
		
	return timeString;
}

var sendChat = function (message, callback) {
	$.getJSON('<?php echo base_url(); ?>api/send_message?message=' + message + '&nickname=' + $('#nickname').val() + '&guid=' + getCookie('user_guid'), function (data){
		callback();
	});
}

var append_chat_data = function (chat_data) {
	chat_data.forEach(function (data) {
		var is_me = "<?=$this->session->userdata('email');?>"; //data.guid == getCookie('user_guid');
		var photo=data.photo;
		if(!photo){ photo="me.jpg";}
		if(is_me=data.email){
			var html = '<li class="right clearfix" style="background:">';
			html += '	<span class="chat-img pull-right">';
			html += '		<img  height="60" width="60"  src="http://localhost/rental/images/' + photo + '" alt="User Avatar" class="img-circle" />';
			html += '	</span>';
			html += '	<div class="chat-body clearfix">';
			html += '		<div class="header">';
			html += '			<small class="text-muted"><span class="glyphicon glyphicon-time"></span>' + parseTimestamp(data.timestamp) + '</small>';
			html += '			&nbsp; <strong class="pull-right primary-font">' + data.nickname + '</strong>';
			html += '		</div> ';
			html += '		<p>&nbsp;&nbsp;' + data.message + '</p>';
			html += '	</div>';
			html += '</li>';
		}else{
		  
			var html = '<li class="left clearfix">';
			html += '	<span class="chat-img pull-left">';
			html += '		<img height="60" width="60" src="http://localhost/rental/images/' + photo+ '" alt="User Avatar" class="img-circle" />';
			html += '	</span>';
			html += '	<div class="chat-body clearfix">';
			html += '		<div class="header">';
			html += '			&nbsp; <strong class="primary-font"> ' + data.nickname + '</strong>';
			html += '			<small class="pull-right text-muted"> <span class="glyphicon glyphicon-time"></span> &nbsp;' + parseTimestamp(data.timestamp) + '</small>';
			
			html += '		</div>';
			html += '		<p>&nbsp;&nbsp;' + data.message + '</p>';
			html += '	</div>';
			html += '</li>';
		}
		$("#received").html( $("#received").html() + html);
	});
  
	$('#received').animate({ scrollTop: $('#received').height()}, 1000);
}

var update_chats = function () {
	if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
		var offset = 60*15; // 15min
		var tokenId="<?php echo $tokenId; ?>";
		request_timestamp = parseInt( Date.now() / 1000 - offset );
	}
	$.getJSON('<?php echo base_url(); ?>api/get_messages?timestamp=' + request_timestamp+'?tokenId='+tokenId, function (data){
		append_chat_data(data);	

		var newIndex = data.length-1;
		if(typeof(data[newIndex]) != 'undefined'){
			request_timestamp = data[newIndex].timestamp;
		}
	});      
}

$('#minimize').click(function (e) {
	$("#scroll").hide(); 
	$("#hide").hide();
$(".float").show(); 	
});

$('.float').click(function (e) {
	$(".float").hide(); 
	$("#scroll").show(); 
	$("#hide").show(); 
});

$('#submit').click(function (e) {
	e.preventDefault(); 
	var $field = $('#message');
	var data = $field.val();

	$field.addClass('disabled').attr('disabled', 'disabled');
	sendChat(data, function (){
		$field.val('').removeClass('disabled').removeAttr('disabled');
	});
});

$('#message').keyup(function (e) {
	if (e.which == 13) {
		$('#submit').trigger('click');
		scroll();
	}
});

setInterval(function (){
	update_chats();
	scroll();
}, 1500);



$(function(){
	$("#scroll").hide();
	$("#hide").hide();
	scroll();
 
});
 function scroll()
  {
		var myDiv = document.getElementById("scroll");
		myDiv.scrollTop = myDiv.scrollHeight; 
		 //alternative you can use this to make scroll bar always at bottom
		 //$("#scroll").animate({ scrollTop: $(document).height() }, "fast");
  }
</script>