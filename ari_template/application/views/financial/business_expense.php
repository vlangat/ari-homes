
		<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
	   
		</div>
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE CONTENT BODY -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMBS -->
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="<?=base_url();?>"> Home </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						 Financial Reports 
						<i class="fa fa-circle"></i>
					</li>  				
					<li>
					Business Expense  Report <span>  </span>
					</li> 
			</ul>
		
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="inbox">
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div  style="min-height:400px"> 
<div class="row">
	 
<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong> &nbsp; Business Expense Report  </strong> </font> 
	   </div>
	   <div class="col-md-12">  &nbsp;  </div>	    
<form action="<?=base_url();?>financial/business_expense" method="post" >
<div class="col-md-6">

<div class="form-group"> 
		   <label class="control-label">Year   </label>
		   <select class="form-control" required="required" name="year" id="year" required>  
				<option value=""> select Year  </option>
				<?php $i=0;
				$date=date('Y');
				while($i<=20){
					$d=$date-$i; ?> 
						<option value="<?=$d?>"> <?=$d?>  </option>
				<?php  $i++; } ?>
			</select>
			</div>
</div>

<div class="col-md-4">

<div class="form-group">
 <label class="control-label">  Month </label>
		   <select class="form-control" name="month" id="month"> 
		   <option value="01"> Jan  </option> 		   
		   <option value="02"> Feb  </option> 		   
		   <option value="03"> Mar  </option> 		   
		   <option value="04"> April  </option> 		   
		   <option value="05"> May  </option> 		   
		   <option value="06"> June  </option> 		   
		   <option value="07"> July  </option> 		   
		   <option value="08"> Aug  </option> 		   
		   <option value="09"> Sep  </option> 		   
		   <option value="10"> Oct  </option> 		   
		   <option value="11"> Nov  </option> 		   
		   <option value="12"> Dec  </option> 		   
			</select>
			</div>
</div>
<div class="col-md-2">
 <label class="control-label">&nbsp; </label>
   <br/> 
 <button type="submit" class="btn green" > Generate report </button>
 
 </div>
</form>
<div  class="portlet-body"  style="font-size:11px;"> 
<div class="col-md-12"  style="border:0px solid #bbb; font-size:11px;">
<p>   </p>
<?php $logo=""; $company_name=""; $location=""; $name=""; $email="";$company_location="";
foreach($user_info->result() as $u){ if($u->id==$this->session->userdata('id')){ $location=$u->town; $name=$u->first_name.' '. $u->middle_name.' '. $u->last_name; }} 
foreach($company_details->result() as $c){ if($c->company_code==$this->session->userdata('company_code')){ $company_name=$c->company_name; $company_location=$c->town; $logo=$c->logo;} 
}
if($company_name==""){
	$company_name=$name;
}
if($company_location==""){
	$company_location=$location;
}
$email=$this->session->userdata('email');
$first_name=$this->session->userdata('first_name');
?>

<table width="100%" id="" cellspacing="0" cellpadding="12" class="table table-striped table-hover table-bordered">
		<tr>
			<td colspan="3"> 
				<strong> <?=$company_name?> </strong>  
			</td>
		</tr>
		<tr> 
			<td>
				<strong>  Location: </strong>  <?=ucfirst(strtolower($company_location))?>
			</td>
			<td>  </td>
			<td>  &nbsp; </td>
		</tr>  
		<tr> 
			<td>
				<strong> Month: </strong> <?=$mnth=date('F', mktime(0, 0, 0, $month, 1));?>
			</td>
			<td>  <strong> Year: </strong>    <?=$year?> </td>
			<td> <strong> Date :</strong>  <?=date('d/m/Y')?> </td>
		</tr>
		 
</table>

<table class="table table-striped table-hover table-bordered" border="1" width="100%" cellspacing="0" cellpadding="4">
<thead>
	<tr>
		<th> Date </th> 
		<th> Supplier </th> 
		<th> Invoice No</th>
		<th> Payment Mode </th>
		<th> Mode No</th>
		<th> Description</th>
		<th> Amount   </th>   
	</tr>
</thead>
<tbody>
<?php 

$total=0;
foreach($expenses->result() as $e)
{
	$supplier_name=""; 
foreach($suppliers->result() as $s)
{ 
  if($e->supplier_id==$s->id){ $supplier_name=$s->company_name;}
}
$invoice=$e->supplier_invoice_no;
if($invoice==""){ $invoice=$e->invoice_no; }
 
if(($month==date("m",strtotime($e->date_paid))) && $year==date("Y",strtotime($e->date_paid)))
		{  
	?>
	<tr><td> <?=$e->date_paid?> </td> <td><?=$supplier_name?> </td><td> <?=$invoice?> </td><td> <?=$e->payment_mode?> </td><td> <?=$e->payment_method_code?> </td><td> <?=$e->description?> </td><td> <?=$e->amount?> </td> </tr>  
<?php 

$total=$total+$e->amount;
}
} 
if($total==0){
	echo "<tr><td colspan='7' align='center'> <font color='red'> No records found </font> </td> </tr> "; 

}
?>
 <tr><td> <strong> Total </strong> </td> <td>   </td><td>    </td><td>   </td><td>   </td><td>   </td><td>  <strong> <?=$total?>  </strong> </td> </tr>	
</table>
<p>   </p>
 

</div>
</div>
<div class="col-md-12">
 <a  href="javascript:;" class="btn green"  id="btn_receipt" >  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
<!-- <a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  
-->
<!--<a  href="javascript:;" class="btn grey"  id="" >  <i class="fa fa-file-excel-o" style="font-size:18px;"></i><b>  Export to Excel  </b> &nbsp;</a>  
-->
</div>  
	 	 
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
          
        </div>
        </div>
      </div>
      </div> 
        <!-- END CONTAINER -->
 <div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> ARI Homes Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
   
   

<script language="javascript">

$(document).ready(function()
{ 
  var y="<?php echo $year;?>";   
  $("#year").val(y); 
  var m="<?php echo $month;?>";   
  $("#month").val(m);  
});


 
 $("#btn_receipt").click( function () {
		 var y="<?php echo $year;?>";   var m="<?php echo strtoupper($mnth);?>";  
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Receipt </title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write('<h4>BUSINESS EXPENSE REPORT '+m+' '+y+'</h4><hr/>'+divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }); 

			
				
 $("#btn_email").click( function () {  
		var divContents = $(".portlet-body").html();   
        var email="<?=$email?>";  
        var fname="<?=$first_name?>";  
        var from="<?=$company_name?>"; 
		if(!email || email==""){ 
		$("#err").html("<font color='red'> Email not sent. Email address is empty. </font>");
		$("#success").modal('show');
		return false;}
		$("#err").html("<font color='#006699'> Sending email...please wait</font>");
		$("#success").modal('show');
		$.ajax({
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Business Expense Report", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
					$("#err").html("<font color='green'>Business expense report  sent to   <u>"+email+"</u></font>");
					$("#success").modal('show');
				}
				else
				{  
					$("#err").html("<font color='red'> Business expense report  not sent to   <u>"+email+"</u></font>");
					$("#success").modal('show');
				}
				
			}
			
 })
 
        });
</script>