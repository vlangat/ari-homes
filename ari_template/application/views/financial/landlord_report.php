
		<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
	   
		</div>
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE CONTENT BODY -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMBS -->
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="<?=base_url();?>"> Home </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						 Financial Reports 
					<i class="fa fa-circle"></i>
					</li>  				
					<li>
					Landlord Report <span>  </span>
					</li> 
			</ul>
		
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="inbox">
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div  style="min-height:400px"> 
<div class="row">
	 
<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong> &nbsp; Landlord Report  </strong> </font> 
	   </div>
	   <div class="col-md-12">  &nbsp;  </div>	    
<form action="<?=base_url();?>financial/landlord_report" method="post" >
<div class="col-md-6"> 
<div class="form-group">
 <label class="control-label">Select Landlord </label>
		   <select class="form-control" name="landlord" required id="landlord"> 
					<option value=""> None  </option>
					<?php foreach($landlords->result() as $row):?> 
					<option value="<?=$row->id ?>"><?=ucfirst(strtolower($row->first_name)).' '.ucfirst(strtolower($row->middle_name)).' '.ucfirst(strtolower($row->last_name))?>  </option>
					<?php endforeach;?>  
			</select>
	</div>
</div>

<div class="col-md-4">

<div class="form-group">
 <label class="control-label">Select Month</label>
		   <select class="form-control" required name="month" id="month"> 
		   <option value=""> None  </option> 		   
		   <option value="01"> Jan  </option> 		   
		   <option value="02"> Feb  </option> 		   
		   <option value="03"> Mar  </option> 		   
		   <option value="04"> April  </option> 		   
		   <option value="05"> May  </option> 		   
		   <option value="06"> June  </option> 		   
		   <option value="07"> July  </option> 		   
		   <option value="08"> Aug  </option> 		   
		   <option value="09"> Sep  </option> 		   
		   <option value="10"> Oct  </option> 		   
		   <option value="11"> Nov  </option> 		   
		   <option value="12"> Dec  </option> 		   
			</select>
			</div>
</div>
<div class="col-md-2">
 <label class="control-label">&nbsp; </label>
   <br/> 
 <button type="submit" class="btn green" > Generate report </button> 
 </div>
 
</form>
 <?php $logo=""; $company_name=""; foreach($company_details->result() as $c){ if($c->company_code==$this->session->userdata('company_code')){ $company_name=$c->company_name; $logo=$c->logo;} }?>
		
<?php  
$email=""; $i=0;
 if($landlord_details=="")
 {
	   
 }
 else{ 
	 $property_name=""; $landlord_name="-"; $bank="-"; $bank_branch="-"; $acc_no="-"; $landlord_name="-"; $management_fee=0;
	 
		foreach($landlord_details->result() as $l){  
			$landlord_name=$l->first_name.' '. $l->middle_name.' '. $l->last_name;
			$acc_no=$l->bank_acc; $bank=$l->bank_name; $email=$l->email;$bank_branch=$l->bank_branch;
		 $i++;
		} 
 }
?>
<div  class="portlet-body"> 
<div class="col-md-12"  style="border:0px solid #bbb;font-size:11px;">
<p>   </p>
<table  id="" cellspacing="0" cellpadding="12" class="table table-striped table-hover table-bordered">
		<tr>
			<td colspan="3"> 
				<strong> Landlord: </strong>  <?=$landlord_name?>
			</td>
		</tr> 
		<tr>  
			<td>  <strong> Account Number: </strong>  <?=$acc_no?> </td>
			<td> <strong> Bank :</strong> <?=$bank?> &nbsp; </td>
			<td> <strong>Branch: </strong> <?=$bank_branch?> </td>
		</tr>
		 
		<tr> 
			<td>
				<strong> Month: </strong>   <?=date('F', mktime(0, 0, 0, $month, 1));?>
			</td>
			<td>  <strong> Year: </strong>  <?=date('Y')?> </td>
			<td> <strong> Date :</strong>  <?=date('d/m/Y')?> </td>
		</tr>
		 
</table>

<table class="table table-striped table-hover table-bordered" border="1" width="100%" cellspacing="0" cellpadding="4">
<thead>
	<tr>
		<th> Property </th> 
		<th> Amount Received</th>
		<th> Deductions </th>
		<th> Amount Payable </th>   
	</tr>
</thead>
<tbody>
<?php  
 $obj2=new rent_class();
$total_received=0;   $total_deductions=0; $total_payable=0; $amtpayable=0; $ap=0; $management_fee=0; $d=0; $deducts=0; $reim=0; $ex=0; $m=0;
foreach($landlord_report->result() as $lr){ 
	$received=0; $deductions=0;
	foreach($tenants->result() as $t){
		if($lr->id==$t->property_id)
		{  
				foreach($tenants_payment->result() as $tp)
				{
					if(($tp->type=="c") && ($t->id==$tp->tenant_id) && ($month==date("m",strtotime($tp->date_paid))))
					{ 
						$received=$received+$tp->amount; 
						$total_received=$total_received+$tp->amount; 			
					}  			
				}
		}

	} 
  if($i>0){
  $management_fee=$lr->management_fee; 
 $m=$management_fee/100*$received;
 $ex=$obj2->property_expense($lr->id, $month); 
 $reim=$obj2->reimbursement($lr->id, $month); 
  }
  ?>
 
<tr><td> <?=$lr->property_name?> </td> <td> <?=$received?>  </td><td> <?=$deducts=$m+$ex+$reim?> </td><td> <?=$ap=$received-$deducts?> </td> </tr> 
<?php 
$d=$d+$deducts;
$amtpayable=$amtpayable+$ap; 
} ?>		
		<tr><td> <strong> Total </strong> </td> <td>  <strong><?=$total_received?>  </strong></td><td>  <strong> <?=$d?>  </strong> </td><td>  <strong> <?=$amtpayable?>  </strong> </td> </tr>	
</table>
<p>   </p>
 

</div>
</div>
<div class="col-md-12">
 <a  href="javascript:;" class="btn green"  id="btn_receipt" >  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
 <a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  
<!-- <a  href="javascript:;" class="btn grey"  id="" >  <i class="fa fa-file-excel-o" style="font-size:18px;"></i><b>  Export to Excel  </b> &nbsp;</a>  
-->
</div>  
	 	 
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
          
        </div>
        </div>
      </div>
      </div> 
        <!-- END CONTAINER -->
<div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4>   ARI Homes Message   </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
  
<div id="sendMail" class="modal fade" tabindex="-1" data-width="500">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> ARI Homes Message </b></h5>
				<hr/>
					<p>
					    This will Email this report to <font id="email_id"> </font>
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="send_email" class="btn green">&nbsp; Yes &nbsp; </button>
			 <button type="button" data-dismiss="modal" class="btn default">&nbsp; Cancel  &nbsp; </button> 
		</center>
	</div>
</div>
   
        

<script language="javascript">
$(document).ready(function()
{ 
  var l="<?php echo $selected_landlord;?>";   
  $("#landlord").val(l); 
  var m="<?php echo $month;?>";   
  $("#month").val(m); 
});


 
 $("#btn_receipt").click( function () {
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Receipt </title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }); 

	
var email="<?=$email?>";  
var fname="<?=$landlord_name?>";  
var from="<?=$company_name?>";
var email="<?=$email?>";	
 $("#btn_email").click( function () {  
   
		if(!email){ 
		$("#err").html("<font color='red'> Email not sent. Landlord email address is empty. </font>");
		$("#success").modal('show');
		return false;}		
		 $("#email_id").html(fname+" <u> "+email+" </u>");
		 document.getElementById('send_email').disabled = false;
		
		$("#sendMail").modal('show');
		 
 });
 
 $("#send_email").click(function(){  
		var divContents = $(".portlet-body").html(); 
		
		$("#error").html("<font color='#006699'> Sending email...please wait</font>");
		//$("#success").modal('show');
		$.ajax({
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Landlord Report", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
			         $("#error").html("");
					 $("#sendMail").modal('hide');
					$("#err").html("<font color='green'> Landlord Report  sent to <u>"+email+"</u></font>");
					$("#success").modal('show'); 
				}
				else
				{  
					 $("#error").html("");
					 $("#err").html("<font color='red'> Landlord Report not sent to  <u>"+email+"</u></font>");
					$("#sendMail").modal('hide');
					$("#success").modal('show');
				}
				
			}
			
 })

});
		
</script>