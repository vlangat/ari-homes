
		<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
	   
		</div>
		<!-- END PAGE HEAD-->
		<!-- BEGIN PAGE CONTENT BODY -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMBS -->
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="<?=base_url();?>"> Home </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						 Financial Reports 
						<i class="fa fa-circle"></i>
					</li>  				
					<li>
					 Agent Income Statement <span>  </span>
					</li> 
			</ul>
		
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
	<div class="inbox">
		<div class="row">
			<div class="col-md-12">
				<div class="inbox-body"> 
			<div  style="min-height:400px"> 
<div class="row">
	 
<div class="col-md-12" style="background:#1bb968;padding:6px;">
			<font color="#ffffff"><strong> &nbsp; Agent Income Statement  </strong> </font> 
	   </div>
	   <div class="col-md-12">  &nbsp;  </div>	    
 
<div  class="portlet-body" style="font-size:11px"> 
<div class="col-md-12"  style="border:0px solid #bbb; font-size:11px">
<p>   </p>
<?php $logo=""; $company_name=""; $location=""; $name=""; $email=""; $company_location="";
foreach($user_info->result() as $u){ if($u->id==$this->session->userdata('id')){ $location=$u->town; $name=$u->first_name.' '. $u->middle_name.' '. $u->last_name; }} 
foreach($company_details->result() as $c){ if($c->company_code==$this->session->userdata('company_code')){ $company_name=$c->company_name; $company_location=$c->town; $logo=$c->logo;} 
}
if($company_name==""){
	$company_name=$name;
}
if($company_location==""){
	$company_location=$location;
}
$email=$this->session->userdata('email');
$first_name=$this->session->userdata('first_name');

$obj2=new rent_class();
$total=0;
?>
<table width="100%" id="" cellspacing="0" cellpadding="12" class="table table-striped table-hover table-bordered">
		<tr>
			<td colspan="3"> 
				<strong>  <?=$company_name?> </strong>  
			</td>
		</tr>
		<tr> 
			<td>
				<strong>  Location: </strong>   <?=ucfirst(strtolower($company_location))?>	
			</td>
			<td>  </td>
			<td>  &nbsp; </td>
		</tr>  
		<tr> 
			<td>
				<strong> Year: </strong>   <?=$year?>  	
			</td>
			<td>  &nbsp;  </td>
			<td> <strong> Date :</strong>  <?=date('d/m/Y')?> </td>
		</tr>
		 
</table>

<table class="table table-striped table-hover table-bordered" border="1"  cellspacing="0" width="100%" >
<thead>
	<tr>
		<th> Item </th> 
		<th> Jan </th>  
		<th> Feb </th>  
		<th> Mar </th>  
		<th> Apr </th>  
		<th> May </th>  
		<th> Jun </th>  
		<th> Jul </th>  
		<th> Aug </th>  
		<th> Sep </th>  
		<th> Oct </th>  
		<th> Nov </th>  
		<th> Dec </th>     
	</tr>
</thead>
<tbody>
 
		<tr><td> <strong>  Income </strong> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td>  </tr>  
		<tr><td>  Management Fees </td> <td><?=$jan1=$obj2->management_fees($year,$month="01");?> </td><td><?=$feb1=$obj2->management_fees($year,$month="02");?>  </td><td><?=$mar1=$obj2->management_fees($year,$month="03");?>  </td><td> <?=$apr1=$obj2->management_fees($year,$month="04");?>  </td><td> <?=$may1=$obj2->management_fees($year,$month="05");?>  </td><td><?=$jun1=$obj2->management_fees($year,$month="06");?>  </td><td><?=$jul1=$obj2->management_fees($year,$month="07");?>  </td><td><?=$aug1=$obj2->management_fees($year,$month="08");?>  </td><td><?=$sep1=$obj2->management_fees($year,$month="09");?>  </td><td><?=$oct1=$obj2->management_fees($year,$month="10");?>  </td><td><?=$nov1=$obj2->management_fees($year,$month="11");?>  </td><td> <?=$dec1=$obj2->management_fees($year,$month="12");?>  </td> </tr>  
		<tr><td>  <strong> Expenses  </strong> </td><td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td>  </tr>    </tr> 
		<tr><td> Business Expenses </td> <td><?=$jan2=$obj2->business_expense($year,$month="01");?> </td><td><?=$feb2=$obj2->business_expense($year,$month="02");?>  </td><td><?=$mar2=$obj2->business_expense($year,$month="03");?>  </td><td> <?=$apr2=$obj2->business_expense($year,$month="04");?>  </td><td> <?=$may2=$obj2->business_expense($year,$month="05");?>  </td><td><?=$jun2=$obj2->business_expense($year,$month="06");?>  </td><td><?=$jul2=$obj2->business_expense($year,$month="07");?>  </td><td><?=$aug2=$obj2->business_expense($year,$month="08");?>  </td><td><?=$sep2=$obj2->business_expense($year,$month="09");?>  </td><td><?=$oct2=$obj2->business_expense($year,$month="10");?>  </td><td><?=$nov2=$obj2->business_expense($year,$month="11");?>  </td><td> <?=$dec2=$obj2->business_expense($year,$month="12");?>  </td>
		</tr><tr><td> <strong> Gross Income </strong> </td> <td><?=$jan=$jan1-$jan2?></td><td><?=$feb=$feb1-$feb2?> </td><td> <?=$mar=$mar1-$mar2?></td><td><?=$apr=$apr1-$apr2?> </td><td><?=$may=$may1-$may2?></td><td><?=$jun=$jun1-$jun2?> </td><td><?=$jul=$jul1-$jul2?> </td><td><?=$aug=$aug1-$aug2?> </td><td><?=$sep=$sep1-$sep2?> </td><td><?=$oct=$oct1-$oct2?> </td><td><?=$nov=$nov1-$nov2?> </td><td> <?=$dec=$dec1-$dec2?> </td> </tr>  
		<?php $total=$total+$jan+$feb+$mar+$apr+$may+$jun+$jul+$aug+$sep+$oct+$nov+$dec?>
		<tr> <td> &nbsp; </td> <td colspan="12"> </td> </tr> 
		<tr><td> <strong> Total <?=$year?></strong> </td> <td colspan="12">  <strong> <?=$total?>  </strong> </td> </tr>	

</tbody>		
</table>
 
<p>   </p>
 

</div>
</div>
<div class="col-md-12">
 <a  href="javascript:;" class="btn green"  id="btn_receipt" >  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
 <!--<a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  
-->
<!--
 <a  href="javascript:;" class="btn grey"  id="" >  <i class="fa fa-file-excel-o" style="font-size:18px;"></i><b>  Export to Excel  </b> &nbsp;</a>  
-->
</div>  
	 	 
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
		</div>
		<!-- END PAGE CONTENT BODY -->
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
  
</div>
</div>
</div>
</div> 
<!-- END CONTAINER -->
 <div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> ARI Homes  Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div> 
   

<script language="javascript">
$(document).ready(function()
{ 
  
});


 
 $("#btn_receipt").click( function () {
	 var y="<?php echo $year;?>";  
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>  Receipt </title>');
            printWindow.document.write('</head><body>');
            printWindow.document.write('<h4>AGENT INCOME STATEMENT YEAR '+y+'</h4><hr/>'+divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }); 
 		
 $("#btn_email").click( function () {  
		var divContents = $(".portlet-body").html();   
        var email="<?=$email?>";  
        var fname="<?=$first_name?>";  
        var from="<?=$company_name?>"; 
		if(!email || email==""){ 
		$("#err").html("<font color='red'> Email not sent. Email address is empty. </font>");
		$("#success").modal('show');
		return false;}
		$("#err").html("<font color='#006699'> Sending email...please wait</font>");
		$("#success").modal('show');
		$.ajax({
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Income Statement", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
					$("#err").html("<font color='green'>Income Statement sent to   <u>"+email+"</u></font>");
					$("#success").modal('show');
				}
				else
				{  
					$("#err").html("<font color='red'> Income Statement  not sent to   <u>"+email+"</u></font>");
					$("#success").modal('show');
				}
				
			}
			
 })
 
});
</script>