	 <div class="container"> 
	 <div class="row">
		
				<div class="pricing pricing--rabten">
				
				 
				<div class="col-md-12" id="heading">
				<div><font size="3">   About   </font></div>
				</div> 	 
					<div class="col-md-12">
						<p>  </p>
<p>ARI Homes is an Online Real Estate Management Solution, build for Real estate Agents and Landlords to automate
their day to day operations to enhance customer satisfaction and improve employee productivity. Being a cloud based solution it 
can be accessed from different offices and locations by logging into the system via www.ari.co.ke
</p>
<p>
As a Real Estate Agency or Landlord dealing with many properties and tenants, its important to have a tool that will
assist in record keeping, invoicing, receipting and performing rental income book keeping for your business, ensuring that
you focus on giving tenants and landlords the best customer service.
</p>
<p>
<p><h4> <strong> What makes your Real Estate Agency Different from the rest? </strong></h4> </p>
<p>There are many homes, apartments and commercial space for tenants to choose from, managed
by the many Real Estate Agencies available the question then is, what will make your properties and agency
different and most preferred by tenants and landlords?
</p>
<p>
ARI Homes gives your business the best tools to manage properties, tenants and landlords, ensuring you are the most
preferred agency and focused on giving the best service. Below is a list of modules available on the system  </p>				 
<p>                
<h4><strong> Modules /Features </strong></h4>
 
<div class="col-md-3">
 
<li>Add Properties</li>
<li>Add Tenants</li>
<li>Add Landlords</li>
<li>User permissions</li>
 
</div>
<div class="col-md-3">
 
<li>Recording rent received</li>
<li>Issue receipts</li>
<li>SMS tenants</li>
<li>Chat</li>
 
</div>

<div class="col-md-3">
 
<li>Recording business expenses</li>
<li>Recording property expenses</li>
<li>Lease Agreements</li>
<li>Accounting Reports</li>
 
</div>

<div class="col-md-3">
 
<li>Landlord income statement</li>
<li>Property income statement</li>
<li>Profit and loss statement</li>
 
</div>
</p>
<p> &nbsp; </p>
<div class="col-md-12">
<p> A Cloud service saves you time and money on constant maintenance, updates and the one off lump sum purchase of a
similar solution</p>
				</div>
</div>
	<div class="col-md-12">
<h4><strong>	ARI LIMITED	</strong></h4>
<p>ARI Limited is a Software Development Company, duly registered in Kenya (2012).</p>
<p> 

Our goal is to provide the best property management software solution throughout Kenya, Africa and Globally. With 5
years experience in software development, we have developed commercial software and mobile applications for corporate
clients, SME’s and Individuals.
</p>
<h4><strong> How ARI Homes Started </strong></h4>
<p> We used to rent office space in Westlands, Nairobi and had a bad experience, one day our office was broken into and
we lost all our laptops and other valuables, while looking for another office to let we discovered that there was no custom
software for real estate agents and after nearly 2 years of research and development ARI Homes was born.
</p>
<p> We are proud and thankful to have worked with many Real Estate agencies and Landlords to develop a solution that
meets industry needs and sets a higher standard for property management in Kenya.
</p>

<h4> <strong>Our Commitment to you </strong></h4>
<p> As ARI Limited we are committed to work with Real Estate Agencies to continuously Improve (Kaizen) the solution and
assure you 24/7 customer service support, 99.99% uptime and a 90 Days 100% no questions asked money back guarantee
a first in Kenya’s software development industry.
</p>

<h4><strong> The Future </strong></h4>
<p>
The future of technology is in cloud solutions i.e. software as a service (SAAS) and it’s already here, do not be left behind
because; “It is not the strongest species that survive, nor the most intelligent, but the ones most responsive to change” -
Charles Darwin
</p>
<p><font size="4"> Ndiang’ui Kinyagia ‘D’ </font></p>
<p>  </p>

<font size="4">MD - ARI Group</font>
</div>
	 
					<div class="col-md-4 animate-box">
                   
                  </div>
               </div>
		</div>
	</div>