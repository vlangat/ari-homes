  <div class="container"> 
			<div class="row row-padded-mb"> 
				
				<div class="col-md-6 animate-box">
				<div class="col-md-12" id="heading">
				<div> Online Real Estate Management System </div>
				</div> 
					<div class=""> 
					<span> &nbsp; </span>
						<p>Are you a Real Estate Agent or Landlord and having difficulties in.</p> 
						<ol style="list-style-type:square ;font-size:14px;line-height:25px;"> 
	 
							<li>   Calculating your agency fee  </li>
							 
							<li>  Managing your landlords  </li>
							 
							<li>  Viewing Monthly rentals income statements </li>
						 
							<li> Sending tenants Invoices, Receipts and SMS  </li>
							
							<li> Recording and Receiving Rental payments  </li>
						</ol>
						<p> Welcome to ARI Homes Real Estate Management System, an easy, efficient and paperless online solution that automates operations across all your branches. 

						</p>
						
						<p> <img src="<?=base_url();?>images/Guarantee.png" height="120" > &nbsp; &nbsp; &nbsp; &nbsp; <img src="<?=base_url();?>images/premium.png" height="120" ></p>
	
					</div>
				</div>
				
							
				<div class="col-md-6 animate-box" id="signup">
				<div class="col-md-12" id="heading">
				 <font size="3"> Create an Account </font> Start Your FREE 30 Days Trial
				</div> 
				<div class="">
				<p> &nbsp; </p>
				<p> <?php  echo form_open_multipart('auth/signUp/', 'onSubmit="return sign_up()"');?>
	   
	<div class="row">
	<div class="col-md-6">
	<p>
		<input placeholder="First Name" id="fname" name="first_name" class="form-control" type="text" onchange="checkChars('fname')">
		</p>
	</div>
	<div class="col-md-6">
		<p><input placeholder="Last Name" id="lname" name="last_name" class="form-control" type="text" onchange="checkChars('lname')"> </p>
	</div>
  
   
		<div class="col-md-6">
		<div class="form-group">
			<p>
				<select class="form-control"  id="type" name="user_type" placeholder="Register As"> 
					<option value=""> --Register As--  </option>
					<option value="1">Landlord</option>
					<option value="2">Business (Estate Agent)</option>
				<!--	<option value="3">Tenant </option>-->
				</select>
			</p>
			</div>
		</div>  
	<div class="col-md-6">
		<p><input placeholder="Company Name" id="company" name="company" class="form-control" type="text"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
  		
 	 
	<div class="col-md-6">
		<p><input placeholder="Email" id="email"  name="email" class="form-control" type="email"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
	</div>
	 <div class="col-md-6">
			<p><input type="number" autocomplete="off" name="phone"   id="phone" class="form-control" placeholder="Phone Number" value=""/></p>
	 </div> 
	<div class="col-md-12"> 
	 <p><input placeholder="Password" id="pass" name="password" class="form-control" type="password"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> </p>
		
	</div>
	<div class="col-md-6"> 	 					 
	 
		<font ><label for="captcha"  id="captcha_image">   <?=$this->session->userdata('image');?>  </label> </font>
		<font style="float:right"><a href='javascript:;' onclick="getCaptcha()"> <i class="fa fa-refresh"></i> Refresh image  </a> </font> 
	 
	</div> 
		
			<div class="col-md-6"> 
				<div class="form-group">	
			<input type="text" autocomplete="off" name="userCaptcha"  id="user_Captcha" class="form-control"   placeholder="Enter the text shown" value=""/>
				<input type="hidden"  name="captcha_name"  id="captcha_name" class="form-control" value='<?=$this->session->userdata('image');?>'/>
				<input type="hidden"  name="captcha_code" id="captcha_val" class="form-control" value="<?php echo $this->session->userdata('captchaWord');?>"/>
				</div> 
			</div> 
		<div class="col-md-12"> <font id="error">   </font> <hr/> </div>
		<div class="col-md-6"> 
				<div class="form-group">
				<input  id="status" name="status" value="0" class="form-control" type="hidden"/> 
				<label class="check">
				<input type="checkbox" id="tnc" name="tnc"/> Agree to the
				<a data-toggle="modal"  href="#terms_n_condition"> Terms & conditions </a>
				</label> 
				
				</div>
			</div> 
	<div class="col-md-6">
		 <div class="form-group">
            <button type="submit" id="register-submit-btn" class="btn btn-info uppercase"> Submit </button>
         </div>
	</div>
	<div class="col-md-6">
	  &nbsp;
	</div>
		<?php echo form_close(); ?>
	</div>
   
		</p>
	</div>
</div>
				
				<div class="col-md-6">
				
					<div class="">
					 <div class="col-md-12" id="heading">
				 Tutorials and Videos  
				</div> 
				<span> &nbsp; </span>
						<div>
					 <iframe width="550" height="310" src="https://www.youtube.com/embed/vHaxzdJbG8c" frameborder="0" allowfullscreen>
					 </iframe>
						</div>
						 <p>  </p>
					</div>
					
				</div>
	

				<div class="col-md-6 animate-box">
				<div class="col-md-12" id="heading">
				<font size="3"> Pricing   </font>
				</div> 
					<div class="">
					<span> &nbsp; </span>
					 <h4> FREE 30 Days Trial </h4>
					 <p> All features FREE for 30 Days </p>
					 
					<h4> Premium Package </h4>
					 <p> KES 30,000 for 10 Users, Annually</p>

					 <h4> Custom Package</h4>
					 <p> For more than 10 Users or Customization, get in <br/> touch with us </p>
					
					</div>
				</div>
				
			</div>
 
		</div>
		
		
		
<div id="terms_n_condition" class="modal fade" tabindex="-1" aria-hidden="true" data-width="1000" data-height="1200">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
     <h5 class="modal-title"> ARI Homes Terms and Conditions </h5>
	 </div>
      <div class="modal-body" style="height:450px;overflow-y:scroll;">
        <div class="row">
			<div class="col-md-12"> 	
                <p> These terms and conditions outline the rules and regulations for the use of ARI Homes Real Estate management Solution (“The Application”) www.arihomes.co.ke website. </p>
	
	<p>ARI Limited is located at:</p>
	<p> Suite 9, 1<sup> th </sup> Floor | AACC | Sir Francis Ibiam House | Waiyaki Way, Westlands | Nairobi, Kenya  </p>
	<p> By accessing this website we assume you accept these terms and conditions in full. Do not continue to use the application if you do not accept all of the terms and conditions.</p>
	<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: "Client", “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. "The Company", “Ourselves”, “We”, “Our” and "Us", refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves “The Application” refers to www.ari.co.ke All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services/products, in accordance with and subject to, prevailing law of Kenya. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same. </p>
	<p> <strong>Accounts</strong></p>
	<p>When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service. </p>
	<p> You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service. </p>
	<p> You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account. </p>
	
	<p> <strong> Termination </strong></p>
	<p> We may terminate or suspend access to the application and your access to the application immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. </p>
	<p> All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability. </p>
	<p> We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. </p>
	<p> Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service. </p>
	<p> All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability. </p>
	<p> <strong> Governing Law </strong> </p>
	<p> These Terms shall be governed and construed in accordance with the laws of KENYA, without regard to its conflict of law provisions. </p>
	<p>  Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>
	<p> <strong> Changes </strong>   </p>
	<p> We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days’ notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion. </p>
	<p> 
By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.
	</p>
	<p> <strong> Cookies </strong>  </p>
	<p>  We employ the use of cookies. By using The Application's website you consent to the use of cookies in accordance with ARI Limited’s privacy policy. </p>
	<p> Most of the modern day interactive web sites use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate / advertising partners may also use cookies. </p>
	<p> <strong> License </strong>  </p>
	<p>  Unless otherwise stated, ARI Limited and/or its licensors own the intellectual property rights for all material on The Application. All intellectual property rights are reserved. You may view and/or print pages from The Application for your own personal use subject to restrictions set in these terms and conditions.  </p>
	<p> You must not:  </p>
	<p> 
	<ol style="list-style-type:square;">
<li> Republish material from <a href="<?=base_url()?>" style="color:blue">www.ARI.co.ke</a> </li>
<li> Sell, rent or sub-license material from <a style="color:blue"href="<?=base_url()?>">www.ARI.co.ke</a>  </li>
<li>  Reproduce, duplicate or copy material from <a style="color:blue" href="<?=base_url()?>"> www.ARI.co.ke</a> </li>
<li>  Redistribute content from The Application (unless content is specifically made for redistribution) from <a href="<?=base_url()?>" style="color:blue">www.ARI.co.ke</a>  </li>
</ol>
	</p>
	<p> <strong> User Information </strong>  </p>
	<p> 
	<ol>
	<li> This Agreement shall begin on the date hereof 4th May 2016.</li> 
	<li> Certain parts of this website offer the opportunity for users to post and exchange opinions, information, material, data and comments in areas of the website. ARI Limited does not screen, edit, publish or review information prior to their appearance on the website and information do not reflect the views or opinions of ARI Limited, its agents or affiliates. Information reflect the view and opinion of the person who posts such view or opinion. To the extent permitted by applicable laws ARI Limited shall not be responsible or liable for the information or for any loss cost, liability, damages or expenses caused and or suffered as a result of any use of and/or posting of and/or appearance and loss of the information on this website.</li> 
	<li> ARI Limited reserves the right to monitor all information and to remove any information which it considers in its absolute discretion to be inappropriate, offensive or otherwise in breach of these Terms and Conditions.</li> 
	<li> You warrant and represent that:</li> 
	<li> You are entitled to post the information on our website and have all necessary licenses and consents to do so;</li> 
	<li> The Information do not infringe any intellectual property right, including without limitation copyright, patent or trademark, or other proprietary right of any third party;</li> 
	<li> The Information do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material or material which is an invasion of privacy</li> 
	<li>The information will not be used to solicit or promote business or custom or present commercial activities or unlawful activity. </li> 
	<li> You hereby grant to <strong> ARI Limited </strong> a non-exclusive royalty-free license to use, reproduce, edit and authorize others to use, reproduce and edit any of your information in any and all forms, formats or media.</li> 
	  
	</ol>
	</p>
	<p> <strong> Hyperlinking to our Content </strong>  </p>
	 
	<ol>
		<li> 
		We may consider and approve in our sole discretion other link requests from the following types of organizations:  </p>
	    <ol style="list-style-type:lower-alpha;">
		<li>Commonly-known consumer and/or business information sources, social media sites </li>
		<li> Community sites; </li>
		<li>Associations or other groups representing charities, including charity giving sites, </li>
		<li> Online directory distributors</li>
		<li> Educational institutions and trade associations</li>
		<li>Government agencies </li>
		<li> Search engines</li>
		<li>News organizations </li>
		 
		</ol>
		</li>
		<li> We will approve link requests from these organizations if we determine that: (a) the link would not reflect unfavorably on us or our accredited businesses (for example, trade associations or other organizations representing inherently suspect types of business, such as work-at-home opportunities, shall not be allowed to link); (b)the organization does not have an unsatisfactory record with us; (c) the benefit to us from the visibility associated with the hyperlink outweighs the absence of ARI Limited; and (d) where the link is in the context of general resource information or is otherwise consistent with editorial content in a newsletter or similar product furthering the mission of the organization. </li>
		<li> These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and it products or services; and (c) fits within the context of the linking party's site. </li>
		<li> If you are among the organizations listed in paragraph 2 above and are interested in linking to our website, you must notify us by sending an e-mail to support@ari.co.ke. Please include your name, your organization name, contact information (such as a phone number and/or e-mail address) as well as the URL of your site, a list of any URLs from which you intend to link to our Web site, and a list of the URL(s) on our site to which you would like to link. Allow 2-3 weeks for a response. </li>
		<li> Approved organizations may hyperlink to our Web site as follows:
		<ul>
		<li>By use of the uniform resource locator (Web address) being linked to</li>
		<li>No use of (our company name)’s logo or other artwork will be allowed for linking absent a trademark license agreement.</li>
		 
		</ul>
		</li>
		<li> Iframes<br/> <p>Without prior approval and express written permission, you may not create frames around our Web pages or use other techniques that alter in any way the visual presentation or appearance of our Web site. </li>
		<li> Content Liability<br/><p> We shall have no responsibility or liability for any content appearing on your Web site. You agree to indemnify and defend us against all claims arising out of or based upon your Website. No link(s) may appear on any page on your Web site or within any context containing content or materials that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p> </li>
		<li>   Reservation of Rights <br/> <p> We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Web site. You agree to immediately remove all links to our Web site upon such request. We also reserve the right to amend these terms and conditions and it’s linking policy at any time. By continuing to link to our Web site, you agree to be bound to and abide by these linking terms and conditions. </p></li>
		<li>   Removal of links from our website<br/><p> If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.</p>
		<p> Whilst we endeavor to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.</p>
		</li>
		<li> Links To Other Web Sites<br/><p> Our Service may contain links to third-party web sites or services that are not owned or controlled by ARI Limited.</p>
         <p> ARI Limited has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that ARI Limited shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or
		 services available on or through any such web sites or services.</p>
		 <p>We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.
		 </p>
		 </li> 
	</ol>
	</p>
	<p> <strong> Disclaimer </strong>  </p>
	<p> To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill). Nothing in this disclaimer will:
	<ol style="list-style-type:square;">
	<li> limit or exclude our or your liability for death or personal injury resulting from negligence; </li>
	<li> limit or exclude our or your liability for fraud or fraudulent misrepresentation; </li>
	<li>  limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
	<li> exclude any of our or your liabilities that may not be excluded under applicable law.  </li>
 
	</ol>
	</p>
	<p>The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.</p>
	<p>To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature. </p>
	<p> <strong> Contacting us </strong>  </p>
	<p>  If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at: </p>
	<p> <strong> ARI Limited</strong> </p>
	<p> <strong> www.arihomes.co.ke </strong> </p>
	<p> Suite 9, 1<sup>th</sup> Floor | AACC | Sir Francis Ibiam House | Waiyaki Way, Westlands | Nairobi, Kenya  </p>
	<p> +254 725 992355, +254 735 412002  </p>
	<p> support@ari.co.ke </p>
 
				
			</div>
		</div>	  
      </div>
      <div class="modal-footer"><font id="reset_status">   </font>
	    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		 
      </div>
	 
    </div>

  </div>
</div>		
		
		
 