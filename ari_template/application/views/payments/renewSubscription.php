<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
	<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="#">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#"> Users </a>
		<i class="fa fa-circle"> </i>
	</li>
	<li>
		<span> Subscriptions </span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		 <!-- PORTLET MAIN -->
		<?php //foreach($profile as $rows){}
		
		?>
 
	</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
	 
<div class="portlet light ">
			<div class="col-md-12" style="background:#1bb968;padding:6px;">
				<font color="#ffffff"> Renew Subscriptions </font>
		</div>	
 
<div class="portlet-body">

<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
 
	<!-- END PERSONAL INFO TAB -->
 <?php if($success==1){ ?>
<script type="text/javascript">
 	//$('#success').modal('show');
 setTimeout(function(){ 
			window.location="<?=base_url();?>auth/users/";		 
				}, 2000);
 </script>
 <?php
 
 } ?>
 
<div   id="tab_1_4">
	<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit">
			 <div class="portlet-body"> 
			<form action="<?=base_url();?>payment/user_renewal" method="post"> 
			 <input type="hidden" value='' id="package_type"> 
			 <input type="hidden" value='' id="total_users">
		<table class="table table-striped table-hover table-bordered"   id="sample_editable_1">
			<thead>
				<tr><th> # </th><th> UserName  </th> <th>  Email  </th> <th> Expiry Date  </th> <th> Days Remaining  </th><th> Status </th><th> Edit Permissions </th> </tr>
			</thead> 
			<tbody>
			<?php $x=0; $y=""; $addedBy="";  $sessionId="";
			foreach($users->result() as $row){  if($row->id==$this->session->userdata('id')){$sessionId=$this->session->userdata('id'); $addedBy=$row->user_added_by;} } 
			foreach($users->result() as $u){?>
			<tr> 
			<?php 	$now=time(); $status="<font color='green'> Active </font>";
							$diff_time=strtotime($u->acc_expiry_date)-$now;
							$days=floor($diff_time/(60*60*24));
							if($days<=0){ $status="<font color='red'> Expired </font>";}
							
							$salt = sha1('2ab'); $salt = substr($salt, 0, 10); 
										$enct =base64_encode(do_hash($u->id . $salt,'sha512') . $salt);
										if($disabled=="disabled"){
										$link="#"; 
										}
										else
										{
											$link=base_url().'auth/userPrivileges/'.$enct; 
										}
							?>
					<td> 
						 <input type="checkbox"  class="checkBoxClass" onmouseover="return boxChecked('<?=$x?>')" id="checkBoxClass<?=$x?>"  <?php if($days<=90){  echo 'checked'; } ?>   name="id[]" value="<?=$u->id?>"> 			
					</td> 
					<td>
                         <?=ucfirst(strtolower($u->first_name))?> <?=ucfirst(strtolower($u->middle_name))?>	<?=ucfirst(strtolower($u->last_name))?>						
					</td> 
					 <td>
						<?=$u->email?>	
					 </td> 
					 <td>
						<?=$u->acc_expiry_date?>	
					 </td>
					 <td>
						<?php if($days<0){ echo '0';} else{ echo $days+1;}?>	
					 </td>  
					 <td>  
					 <?php 
					 
					 if($u->user_enabled==1){ $status="<font color='green' title='Click to Deactivate'>Active</font>";} else{ $status="<font color='red' title='Click to activate'> Deactivated </font>";}?>
					<?php if($addedBy == 0 || $u->user_added_by==$sessionId){ ?>	 <a   onclick="remove_user('<?=$u->id?>','<?=$u->user_enabled?>')"><?php }else{?> <a   onclick="alert('You cannot perform the action!')"> <?php  }?> <?=$status?> </a>
					 </td> 
					<td id="edit_<?=$x?>"> 
						<?php if($addedBy == 0 || $u->user_added_by==$sessionId){ ?> <a href="<?=$link?>"> <i class='fa fa-edit'></i> Edit Permissions <?php } else{?> <a href="#" style="color:red" onclick="alert('Unable to edit  user permissions')" > <i class='fa fa-edit'></i>  Disabled <?php }?> </a>
					</td>
			</tr>	

			<?php $x++; }?>	
</tbody>			
		</table>	
 
<p>   </p>	

<?php if($addedBy == 0){?>
<table border="0" width="100%" id="order_summary" class="table" style="border:1px solid lightgrey"> 
					<tbody>
						 <tr>    
							<td> 
					<div class="form-group">
								<label> Package Type </label>
							<select class="form-control input-medium" readonly  name="package" id="package" onchange="getBillingCycle(this.value)">
								<option value="premium">Premium</option>
								<option value="custom">Custom</option> 
							</select>
							</div> 
							</td>
							<td> 
						<div class="form-group">
								<label> Billing Cycle </label>
								<select class="form-control input-medium" required name="pay" id="billing_cycle"    onchange="setData()">
								 <option value="250"> 1 Year @ KES 30,000  </option>
							 
								</select>
								<label id="error1"> </label>
						</div>
						
						</td> 	
						<td>  
						 
						<div class="form-group">
							<label>Total  
							<input type="hidden"  name="balance"   value="<?=$balance?>" >
							<input type="hidden"  name="vat"  id="vat_to_pay" value="" >
							<input type="hidden"  name="package_cost"  id="package_cost_to_pay" value="" >
							<input type="hidden"  name="total_cost"  id="amount_to_pay" value="" >
							
							<input type="hidden"  name="selected"    value="<?=$y?>" >
							<input type="hidden"  name="years"  id="years" value="" >
                            <input type="hidden"  name="package_name"  id="package_name" value="" >
							<input type="hidden"  name="users"  id="users" value="" >
							<input type="hidden"  name="mode" value="mpesa">
							<input type="number" readonly class="form-control"  name="amount"  id="amount" required value="" >
						
						 </div>
						</td>
							
						</tr>
						 
</table> 

<div class="form-group">
	<input type="submit"   value="Renew Subscription"  id="save_btn" onclick="return validate()" class="btn red"/>  &nbsp; &nbsp;	<font id="add_btn"> </font> 
</div>
<?php }?>
</form>


		</div>
	</div>
</div> 
</div>
</div>

 <p id="error">   </p>
</div>   
        <?php if($this->session->flashdata('temp')){
				$msg=$this->session->flashdata('temp');
				echo '<div class = "alert alert-success alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button> <font color="green">'. $msg. '</font> </div>'; 	
				} 	
		?>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div> 

<div id="responsive_2" class="modal fade" tabindex="-1" aria-hidden="true">
<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" id="task"> Add User </font></h4>
	</div>
<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div  class="form-horizontal">
				<div class="form-group">
					<div class="col-md-12">
					First Name:
					<input type="text"  class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off"required="required"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="First_Name"  />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					Second Name:
					<input type="text" id="Last_Name"  class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"   />
					</div>
				</div>
			<div class="form-group">
			<div class="col-md-12">
				Email:
				<input type="email" id="email"class="form-control todo-taskbody-tasktitle" required="required" autocomplete="off"     onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" />
			</div>
	</div>
	<div class="form-group">
			<div class="col-md-12">
				Phone Number:
				<input type="number" id="phone"class="form-control todo-taskbody-tasktitle" required="required" autocomplete="off"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" />
			</div>
	</div>
		<div class="form-group">
			<div class="col-md-12">    
				</div>
			<input type="hidden" id="added_by"class="form-control todo-taskbody-tasktitle"value="<?php echo $this->session->userdata('id');?>" placeholder="">
		</div>
	</div>
	</div>
</div>
</div><font id="add_user"> </font>
<div class="modal-footer"> 
<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
<input type="submit" id="add_new_user" class="btn green"value="Save Changes">
</div>   
</div>
</div> 
  
<!-- responsive -->
<div id="del_user" class="modal fade" tabindex="-1" data-width="400">
      <div class="modal-header">
	   <b> Confirm Action </b> 
	  </div>
	  <div class="modal-body">
		<div class="row">
		    
			<div class="col-md-12">
				
				<p id="del_status"> Are you sure you want to perform this Action? </p>
				
			</div>
		</div>
</div>
<div class="modal-footer" >
<input type="hidden" id="status">
	<button type="button" class="btn green" id="confirm" <?=$disabled?>> Yes </button>
	<button type="button" data-dismiss="modal"  class="btn red"> No </button> 
</div>
</div>
 
 
 <div id="subscribe_users" class="modal fade" tabindex="-1" data-width="600">
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b>  Add More Users </b></h5>
				<hr/>
				<p> You need to subscribe to be able to add more users to the system </p>
				<p> Do you want to start the subscription? </p>
				
			</div>
		</div>
</div>
<div class="modal-footer" > 
	<button type="button" class="btn green" id="confirm_subscription"> Yes </button>
	<button type="button" data-dismiss="modal"  class="btn red"> No </button> 
</div>
</div>


<div id="privileges" class="modal fade" tabindex="-1" data-width="600">
     <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3"> <b> Assign  Privileges to the user </b> </font></h4>
	</div>
	<div class="modal-body"> 
			 <form action="<?=base_url();?>auth/assign_privilege/userPrivileges/payments" method="post">
			 <input type="hidden" value="" name="user_id" id="user_id">
			<table   class="table table-striped table-hover table-bordered" id="sample_editable_1">
			<thead>
			<tr><th> &nbsp; </th> <th> Add </th> <th> Edit  </th> <th> View </th> <th> Delete </th> </tr>
			</thead>
			<tbody>
			<tr>
				<td> 
					<strong>Property </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="p_addPrivilege" checked    value="1">   
					  </td> 
					 <td>
					 <input type="checkbox"  name="p_editPrivilege"  checked  value="1"> 
					 </td> 
					 <td> 
					 <input type="checkbox"   name="p_viewPrivilege" checked  value="1">  
					</td> 
					<td> 
					<input type="checkbox"   name="p_deletePrivilege" checked  value="1" > 
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Tenants </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="t_addPrivilege" checked   value="1">  
					  </td> 
					 <td>
					 <input type="checkbox"  name="t_editPrivilege"  checked  value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"  name="t_viewPrivilege"  checked value="1">   
					</td> 
					<td> 
					<input type="checkbox"  name="t_deletePrivilege" checked  value="1">  
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Rent </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="r_addPrivilege"  checked  value="1">   
					  </td> 
					 <td>
					 <input type="checkbox"  name="r_editPrivilege"  checked value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"  name="r_viewPrivilege" checked  value="1">    
					</td> 
					<td> 
					<input type="checkbox"  name="r_deletePrivilege" checked  value="1">    
					  
					</td>
			</tr>			
			<tr>
					<td> 
						<strong>Suppliers </strong>
					</td>  
					<td>  
						<input type="checkbox"  name="s_addPrivilege"  checked  value="1">    
					  </td> 
					 <td>
						<input type="checkbox"  name="s_editPrivilege" checked  value="1">   
					 </td> 
					 <td> 
					 <input type="checkbox"  name="s_viewPrivilege"  checked value="1">  
					</td> 
					<td> 
					<input type="checkbox"  name="s_deletePrivilege" checked  value="1">    
					  
					</td>
			</tr>			
			<tr>
				<td> 
					<strong>Subscriptions </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="sub_addPrivilege" checked  value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="sub_editPrivilege" checked  value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"  name="sub_viewPrivilege" checked value="1">    
					</td> 
					<td> 
					<input type="checkbox"  name="sub_deletePrivilege"   checked  value="1">    
					  
					</td>
			</tr>			
 <tr>
				<td> 
					<strong>Users </strong>
					</td>  
					<td>  
					 <input type="checkbox"  name="users_addPrivilege"  checked value="1">    
					  </td> 
					 <td>
					 <input type="checkbox"  name="users_editPrivilege" checked  value="1">  
					 </td> 
					 <td> 
					 <input type="checkbox"  name="users_viewPrivilege" checked  value="1">    
					</td> 
					<td> 
					<input type="checkbox"  name="users_deletePrivilege"  checked value="1">    
					  
					</td>
			</tr>			
 </tbody>
</table>	
<p>  </p>		
  
</div>
<div class="modal-footer" >
 
	<button type="submit" class="btn green"   <?=$disabled?>> Save  </button>
	<button type="button" data-dismiss="modal"  class="btn red"> Close </button> 
</div>
</form>
</div>

<div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5> <b> ARI Homes Message </b> </h5>
				<hr/>
					<p id="msg">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
   
 <input type="hidden" value="1" id="delete_items"> 
 <input type="hidden" value="1" id="add_items"> 
<!-- END CONTENT -->
 
<!-- END CONTAINER -->
 
    </body>

</html>
<script type="text/javascript">
function validate()
{
		var users = $('input:checkbox:checked').length;     
		$("#error1").html(""); 
		if(users==0 || !users){ $("#error1").html("<font color='red'> No users selected to   re-new </font>"); return false;}
		 
}


function boxChecked(i) 
{ 
	//$("#checkBoxClass"+i).attr("checked",true);
	document.getElementById("checkBoxClass"+i).disabled = true;
	//return false;	 
}

function setData() 
	{   
		//var users=$("input[name=users]").val(); 
		var users = $('input:checkbox:checked').length; 
		var y=$("#billing_cycle option:selected").text(); 
		var p=$("#package").val(); 
		$("#package_name").val(p);
		$("#error1").html("");
		var years=y.trim().charAt(0);  
		$('#years').val(years);
		if(users==0 || !users){ $("#error1").html("<font color='red'> Select users  to   re-new </font>"); return false;}
		$('#users').val(users);  
		//document.getElementById("checkBoxClass"+i).disabled = true; 
		var bill=$("#billing_cycle").val();  
		//$("#billing_by_user").html(years+" Year(s),  &nbsp; "+users+" User(s)"); 
		var total_months=12;  
		var sub_total=0; 
		var sub_total=0;
        //var package_cost=bill*users*total_months*years;		
        var package_cost=30000;		
		var sub_total=30000;//(package_cost);   
		var vat=0;  
		var vat=Math.round(16/100*sub_total);		
		vat=Math.round(vat);
		var balance="<?=$balance?>"; 
		balance=Math.round(balance);
		$("#amount").val((sub_total+vat+balance));  
		$("#total_cost").html(sub_total+vat+balance); $("#amount_to_pay").val(sub_total+vat+balance);
		$("#package_cost").html(package_cost); $("#package_cost_to_pay").val(package_cost);
		$("#total_vat").html(vat); $("#vat_to_pay").val(vat);  
	}	 

	 
$(function()
{  

var balance="<?=$balance?>";
$("#balance").html("<font> +credit("+(balance)+")</font>");
var msg="<?=$msg?>";  
if(msg=="")
{
	
}
else{ 
	$("#message").html("<font>"+msg+"</font>");
	$("#success").modal('show');
}

var x="<?=$x?>";

var name="premium"; 
  if(x>10)
  {
	 name="custom"; 
  } 
   //getBillingCycle(name);
   $(".checkBoxClass").attr('checked', "checked");  
	
	setData();
	$("#package").val(name); 
	document.getElementById('package').disabled = true;

	var x=document.URL;
	//if (x=="https://ari.co.ke/rent/manager/#signup") { 
	if (x=="<?=base_url();?>auth/users/#add_user") { 
		 
		 $("#responsive_2").modal('show');  
	} 
		
});
 
 
function getBillingCycle(name){   
		$.ajax({
		"url":"<?=base_url();?>payment/getBillingCycle/"+name,
        "type":"post",
		"success":function(data)
		{ 
			var obj = JSON.parse(data);  
			var data = obj.data; 
				$("#billing_cycle").empty();
				//$("#billing_cycle").append("<option value=''> --Select--</option>");
            for(var i=0; i<data.length; i++)
			{
				var cycle = data[i]; 
				$("#billing_cycle").append("<option value='"+cycle['amount']+"'>"+ cycle['package_name']+"</option>"); 
			}
			setData();
		}
	});
	
} 	
 
 $(document).ready(function()
 { 
	 getAgents();
	 checkPrivilege(); 
 });
 
$(function()
{ 
	//$('#responsive_2').modal('hide');
	$("#add_btn").append("<a data-toggle='modal' href='javascript:;' id='add_new'    class='btn green'><i class='fa fa-plus'></i> Add New User</a>");  
	 
var msg="<?=$msg?>";
 if(!msg)
 {
	 
 }else{
	 $("#msg").html("<font> "+msg+" </font>");
	 $('#success').modal('show');
 }
 

$("#confirm_subscription").click(function()
{ 
	var package_name=$("#package_type").val();
	window.location="<?=base_url();?>payment/package/";
});

$("#add_btn").click(function()
{ 
      
	 var a=$("#add_items").val();
	  
	var c="<?=$disabled?>"; 
	if(c=="disabled")
	{
		  alert("You cannot add new user! Ensure your subscription is active");
	}else  if(a==0){
		alert('You have no privilege to add new user!'); 
		return false;
	 }
else{
	addNewUser();
	}
});

$("#add_new_user").click(function()
	{  
		var email=$("#email").val();
		var phone=$("#phone").val();
		var fname=$("#First_Name").val();
		var lname=$("#Last_Name").val();  	 
		if(!fname){   $("#First_Name").focus();  $("#add_user").html("<font color='red'> First Name required </font>"); return false;} 		
		if(!lname){   $("#Last_Name").focus();  $("#add_user").html("<font color='red'> Last Name required </font>"); return false;} 		
		if(!email){   $("#email").focus();  $("#add_user").html("<font color='red'> Email required </font>"); return false;} 		
		if(!phone){   $("#phone").focus();  $("#add_user").html("<font color='red'> Phone Number required </font>"); return false;} 		
		if(email&&fname&&lname){ } else{$("#add_user").html("<font color='red'> Empty fields not required </font>"); return false;}
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#add_user").html("<font color='red'> Email provided is not a valid e-mail address"); return false;}
		$("#add_user").html("<font color='blue'> Adding new user....</font>");
		$.ajax({
		 url:"<?=base_url();?>auth/addUser",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'lname':lname, 
		   'email':email, 
		   'phone':phone 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){
				 $("#First_Name").val('');$("#Last_Name").val(''); $("#phone").val('');$("#email").val('');
				  
				 $("#add_user").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>"+obj.msg+"  </font>"); 
                 getAgents(); 
                $("#user_id").val(obj.user_id); 
					setTimeout(function(){
					$('#add_user').empty();
					$('#responsive_2').modal('hide');
					$("#privileges").modal('show');
				}, 3000);				
					
			 }
			 else{
				 $("#add_user").html("<font color='red'> "+obj.msg+"</font>");return false;
			 }
		 }
		 
		  }
		 )	  
});

$("#confirm").click(function(){ 
	var par1=$("#confirm").val();
	var par2=$("#status").val();  
	$("#del_status").html("<font color='blue'>  Please wait....</font>");
	 $.ajax({
		url:"<?=base_url();?>auth/removeUser/"+par1+"/"+par2,
		type:"POST",
		async:false ,
		success:function(data)
		{ 
			var obj = JSON.parse(data); 
			if (obj.result=="ok")
			{
			   getAgents();
			   if(par2==1){ msg="de-activated";}else{msg="activated";}
			   $("#del_status").html(" <font color='green'> Agent "+msg+" successfully</font> ");
				setTimeout(function(){
					$('#del_status').empty();
					$('#del_user').modal('hide');
                    document.location.reload(true);
				}, 3000);
				return false;			   
			}
			else{
				$("#del_status").html(" <font color='red'> Action failed! Ensure the user activate account via email </font> "); 
				return false;
			}
			
		}
		})
	
});

checkPrivilege();

});

function getAgents()
{
	var i=0;
	var link="";
	var users=1;
	var c="<?=$disabled?>";
	
	$.ajax({
		url:"<?=base_url();?>auth/loadAgents",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				 var content="";
				 $("#agents").empty();
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					var status="";
					var agent=data[i]; 
					if(c=="disabled"){ link="#"; }else{ link="<?=base_url();?>auth/userPrivileges/"+agent['id']; }
					 if(agent['user_enabled']==1){ status="<font color='green' title='Click to Deactivate'>Active</font>";} else{ status="<font color='red' title='Click to activate'> Deactivated </font>";}
					content=content+"<tr><td> "+agent['first_name']+"</td> <td> "+agent['last_name']+"</td><td> "+agent['mobile_number']+"</td><td> "+agent['email']+"</td><td><a   onclick=\"remove_user("+agent['id']+","+agent['user_enabled']+")\"> " +status+" </a></td><td><a   href='"+link+"'> <i class='fa fa-edit'></i> Edit Privileges </a></td></tr>";
					users++;
				 }
				 $("#total_users").val(users); 
				 $("#agents").html(content); 
			}
else
{

$("#total_users").val(users); 
}
		}
		
		
	})
	
}


function remove_user(id,status)
{ 
	    $("#del_user").modal('toggle');
		$("#confirm").val(id);
		$("#status").val(status); 
		 
}

function checkPrivilege()
 { 
	 var i="<?=$x?>";
	$.ajax({
		url:"<?=base_url();?>auth/checkPrivilege/6",
		type:"POST", 
		async:false,
		success:function(data)
		{  
			var obj=JSON.parse(data);   
			var data = obj.data;   
			if(obj.add==0)
			{    $("#add_items").val(obj.add); 
				 document.getElementById('add_new_user').disabled = true; 
				 document.getElementById('add_new').disabled = true; 
			 } 
			if(obj.view==0){ 
				//document.getElementById('pricinng_property_name').disabled = true;  
			}
			if(obj.edit==0){  
				 //document.getElementById('edit').disabled = true;  
				 for(var x=0; x<i;x++){  $("#edit_"+x).html("<font color='' onclick=\"alert('You have no privilege to edit users')\"> <a href='javascript:;'><i class='fa fa-edit'></i>Edit Privileges </a></font>"); }
             
			}
			if(obj.delete==0){ 
			 $("#delete_items").val(obj.delete);  
				 document.getElementById('confirm').disabled = true;   
			}
		}
	 })
 } 
 
 
 function addNewUser()
 {
	$("#First_Name").val('');$("#Last_Name").val(''); $("#phone").val('');$("#email").val('');
	$("#add_user").empty();
	var users_limit=$("#total_users").val();
	var i=0;
	var users=0;
	$.ajax({
		url:"<?=base_url();?>auth/checkUsers",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
                                 
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					var property=data[i]; 
					var package_name=property['package_type']; 
					users=users+property['number_of_users']++;
				 } 
				  
				if(users_limit<users)
				{ 
					$('#responsive_2').modal('show');  
				}
				else{
					$("#package_type").val(package_name);
					$("#responsive_2").modal('show'); 
					$('#subscribe_users').modal('show'); 
					return false;
				}
				
			}
			else{
				$("#responsive_2").modal('hide'); 
				$('#subscribe_users').modal('show');
			}
		}
		
		
	}) 
 }

</script>