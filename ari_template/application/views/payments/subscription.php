<!-- BEGIN PAGE CONTENT BODY -->
<?php   
	$package_type="";  $package_title="";  $date_paid=""; 
	$x=0 ;
	foreach($subscription->result() as $row){
	$package_id=$row->package_id; $date_paid=$row->date_paid;   
	foreach($packages->result() as $pac){   if($package_id==$pac->id){    $package_type=$pac->package_type; $package_title=$package_type;  } }
	if($x==1){ break;} 
	 
	if($package_type == "sms"){ $package_type=""; continue; $x++; }else{ break;}
  }
  
	$remaining_sms=0; $total_days=0; $sent_sms=0; $sms=1; 
	foreach($sms_status->result() as $rows){ $sent_sms=$rows->sent_sms; $remaining_sms=$rows->remaining_sms;} 
	$expiry_date="";  
	foreach($expiring_date->result() as $e)
	{ 
			//$expiry_date=$e->expected_pay_date;  
			$expiry_date=$e->acc_expiry_date;
            $registration_date=$e->registration_date;  
            $subscription_date=$e->subscription_date;  
	} 
	$users=0; foreach($paid_users->result() as $u){   $users=$users+$u->number_of_users;	} 
	$agents=0; foreach($total_agents->result() as $a){ $agents=$agents+1;} 
	$days=0;
	if($package_type==""){    $package_type="free"; $package_title="FREE 30 Days Trial "; }
	date_default_timezone_set('Africa/Nairobi');
	 $date_paid=$subscription_date;
	if(!empty($date_paid)){
		$now=time();
		$total_days=strtotime($expiry_date)-strtotime($date_paid);
		$total_days=floor($total_days/(60*60*24));
		$diff_time=strtotime($expiry_date)-$now;
		$days=floor($diff_time/(60*60*24));
		$days=$days+1;
		}
		$p=0; foreach($package_current_account->result() as $row){ $p++;} 
		 
if($users==0){ $users=1;}
 	
?>
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Subscriptions </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  My Subscription  </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 
<div class="row"> 
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light "> 
		<div class="col-md-12"    style="background:#1bb968;padding:6px;">
				&nbsp; <font color="#ffffff"> Subscriptions </font>
		</div>		
 
 <div class="portlet-title">
 
				<div class="col-md-12">  
				<h3>  <font color="#006699"><strong>  Your Subscription </strong> </font> <br/> </h3>
				<h4>  <font color="#006699"><strong>  Review information relating to ARI  Subscription </strong> </font>   </h4>
				</div> 
	</div>  
<div class="row">
<div class="col-md-12 col-sm-6" >
   <div class="portlet light "> 
		<div class="portlet-body">
<table border="1" width="100%" id="subscription" style="border:1px solid lightgrey"> 
	<tbody>
		<tr height="50" > 
			<td  colspan="2"><h4>   <strong>  
			<?php if($package_type =="free" && $days<=0){ echo ' <font color="red"> &nbsp;&nbsp;  Your FREE 30 days Trial has expired</font>'; }else if($package_type =="free"){ echo ' <font color="red"> &nbsp;&nbsp;  You are currently on the FREE 30 days Trial</font>';}else{?> 
			
			<font color="green">&nbsp;&nbsp;  <i class="icon-check" style="font-size:20px;"></i> Congratulations you are Registered </font>  <?php } ?>

			</strong></h4> 
			</td>
		</tr>
		<tr height="30"> <td>  Package </td> <td> <?=ucfirst(strtolower($package_title))?> </td></tr>
		<tr height="30"> <td> Current Status </td> <td> <?php if($days>0){ echo 'Active';}else{ echo 'Expired';} ?> </td></tr>
		<tr height="30"> <td> Number of Users </td> <td><font> <?=$agents?>/ <?=$users?>   </font>
		</td>
		</tr>
		<tr> <td> <?php if($days<=0){ echo "<font color='red'>Current Subscription expired on </font>"; }else{ ?> Current Subscription will expire on <?php }?> </td>
		<td> <div class="row" >  
		<div class="col-md-3"> <?=$expiry_date?> </div>
		<div class="col-md-5">
			<?php if($expiry_date !=""){?>
			<div class="progress-info">
					<div class="progress">
					<?php  
					if($total_days==0){ $total_days=1;}  if($days >0){?>
						<span style="width:<?php echo ($days/$total_days)*100;?>%;" class="progress-bar progress-bar-success green-sharp"> 
						</span>
					<?php }?>
					</div> 
				</div>
			<?php } ?>
		</div>
			<div class="col-md-4">
				<?php  if($days!=""&&$days>=0){ echo $days; ?>   Days remaining  <?php } ?>
			</div>  
		</div> 
		</td>
		</tr>
		<tr height="30"> <td>  Subscription Date </td> <td>  <?=$date_paid?> <?php if(!$date_paid){ echo $registration_date ;}?> </td></tr>  
		
			<tr> 
				<td colspan="">  <a href="<?=base_url();?>auth/users/#add_user" class="btn green" >    ADD USERS  </a>  </td> 
				<td colspan=""> 
				
			<a href="<?=base_url();?>payment/package/" class="btn red">  BUY PACKAGE  </a> 
				&nbsp;&nbsp;
				<?php if($package_type !="free"){ ?>
				<a href="<?=base_url();?>auth/users/" class="btn green" id="button_nmedium">   RENEW SUBSCRIPTIONS  </a>    
				<?php } ?>
				</td> 
			</tr>
			
		<?php  if($remaining_sms>0){?>
		<tr  > <td colspan="2" >   <hr/> </td>  </tr>
		<tr height="30"> <td> <b> Bulk SMS  </b></td> <td>  </td></tr> 
		<tr  > <td><b> Remaining SMS </b></td> 
		<td>
			<div class="col-md-12">
				<div class="col-md-2"> 
					<b style=" "> <?=$remaining_sms?>    </b>
				</div>
				<div class="col-md-4">		 
						<div class="progress">
							<span style="width:<?php echo 50;?>%;" class="progress-bar progress-bar-success green-sharp"> 
							</span> 
					</div>   
				</div>   
			</div>   
		</td>
		
		</tr>
		
		<tr height="40"> <td> <b> Sent SMS </b></td> <td> <b><?=$sent_sms?></b></td></tr>
		<tr  > <td colspan="2"   > <font id="add"> <a  href="javascript:;" class="btn green" id="button_mediumn"> ADD SMS   </a> </font> </td>  </tr> 
		<?php } ?>
		 
		</tbody>
	</table> 
	
	 </div>
 </div>
</div>

	 </div>
	 </div>
	 </div> 
 
</div>



 </div>
		<!-- END EXAMPLE TABLE PORTLET-->
 
 

</div>
</div>
</div>   

<div id="add_sms" class="modal fade" tabindex="-1" data-width="600">
	<div class="modal-body">
			<div class="row">
			<div class="col-md-12">
			 
			<h4><b> Buy  Bulk SMS </b></h4>
			 <hr/>
			 <p>
			 <form action="<?=site_url('payment/buy_sms');?>"  method="post" onsubmit="return check_sms()"> 
			 <input  type="hidden" id="sms" name="sms" value="" >
			 <input  type="hidden" id="total_cost" name="total_cost" value="" > 
				<h5> <strong> Choose your desired plan </strong> </h5>
					  <p>   </p>
						<p> 
						<input  type="checkbox" id="checkbox1"   value="200"   onclick="return checkboxValue(this.value, 1)" > 200 SMS for KES 600 &nbsp; <input type="checkbox" id="checkbox2"   onchange="checkboxValue(this.value,2)" value="500"> 500 SMS for KES 1,500 &nbsp;  <input type="checkbox" id="checkbox3" value="1000" onclick="checkboxValue(this.value,3)" > 1,000 SMS for KES 3,000
				  <p>  </p>
				<p>  FAQ: Send SMS to all networks for KES 3 </p>
				 <font color="red" id="error"> </font>		   
			</div>
		</div>
	</div>
<div class="modal-footer" > <span id="status_message" style="float:left"> </span>
	<button type="submit" class="btn green" id=""> Add Now </button>
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
</div>
</form>
</div>   
<!-- END CONTAINER -->
<script language="javascript">
$(function()
{
	$("#add").click(function(){
		$("#add_sms").modal('show');
	});
	
	$("#buy_sms").click(function(){
		var sms=$('#sms').val(); 
		var code=$('#mpesa_code').val(); 
		if(!code){  $("#error").html("Enter Mpesa Transaction Code"); $('#mpesa_code').focus(); return false;}
		if(!sms){ sms=0; $('#checkbox1').focus();  return false; }
		var sms_cost=sms*3;
		 
		$.ajax({
		url:"<?=base_url();?>payment/add_sms",
		type:"POST", 
		data:{
			'sms':sms,
			'total_cost':sms_cost
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
		            $("#error").html("<font color='green'>You have successfully bought SMS </font>");
			}
			else
			{
				$("#error").html("<font color='red'>"+obj.msg+"</font>");
			}
		}
	})
	
	});
	
	loadUsers();
});

function loadUsers()
{
	var i=0;
	var users=1;
	$.ajax({
		url:"<?=base_url();?>auth/loadAgents",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					users=users+1;
				 }
				 $("#users").html(users); 
			}
		}
		
		
	})
	
}

function checkboxValue(value,id)
	{   
	var sms=$("#sms").val(value); 
	
	jQuery.uniform.update(jQuery("#checkbox"+id).attr('checked',true));
if(id==2){	
	jQuery.uniform.update(jQuery("#checkbox1").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox3").attr('checked',false)); 
	  
}else if(id==1){
	jQuery.uniform.update(jQuery("#checkbox2").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox3").attr('checked',false)); 
}else if(id==3){
	jQuery.uniform.update(jQuery("#checkbox1").attr('checked',false)); 
	jQuery.uniform.update(jQuery("#checkbox2").attr('checked',false)); 
}	
	if($("#checkbox1").prop('checked') == false && $("#checkbox2").prop('checked') == false && $("#checkbox3").prop('checked') == false) 
		{ 
			$("#sms").val(0);
		}else{
			$("#sms").val(value); 
	}	
    //setData();
  }
  
 function check_sms()
 {
	 var sms=0;
	    sms=$('#sms').val();   
		 if(!sms||sms==0){  $('#error').html("<font color='red'> You have not yet selected any  SMS plan. Select your desired plan to proceed</font>");$('#checkbox1').focus();  return false; }
		var sms_cost=sms*3; 
       $("#total_cost").val(sms_cost);	
return true;	   
 } 
 
</script>