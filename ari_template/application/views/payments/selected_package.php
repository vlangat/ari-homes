<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Packages </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Enterprise</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">  
			<div class="portlet">
			<form action="<?=base_url();?>payment/order_summary"  method="post">
				<h3>  <font color="#3bc71c"><strong> &nbsp;&nbsp; Congratulation for choosing <?php echo ucfirst(strtolower($selected_item));?> plan </strong> </font><br/><br/> </h3>
				</div>
				<input type="hidden" name="package" value="<?php echo $selected_item;?>">
				<div class="col-md-6">
					<label>Select Number of users </label>
					<input type="number" class="form-control" name="users" id="large_select" required min="1" max="15"> 
				</div>
				<div class="col-md-6">
					<label>Register for longer and save </label>
					 <select class="form-control" id="large_select" name="pay">
						<?php foreach($billing_cycle->result() as $row){?>
						<option value="<?=$row->years?>">  <?=$row->description?></option>
						<?php } ?> 
					 </select>
				</div>
				 
				
				<div class="col-md-12">
					<p>  &nbsp; </p>
					<!--<p > <b> <font size="3" color="#006699"> Other Value Add's you might be interested in </font></b></p>
						 <input  type="hidden"  id="sms" name="sms" value="" >
						 <h4> <strong>  Bulk SMS </strong> </h4>
					 <input  type="radio" id="radio1"   value="200"   onclick="return radioValue(this.value, 1)" > 200 SMS for KES 600 &nbsp;&nbsp; &nbsp;&nbsp; <input type="radio" id="radio2"   onchange="radioValue(this.value,2)" value="500"> 500 SMS for KES 1,500 &nbsp;&nbsp; &nbsp;&nbsp; <input type="radio" id="radio3" value="1000" onclick="radioValue(this.value,3)" > 1,000 SMS for KES 3,000
					 <p>  FAQ: Send SMS to all networks for KES 3 </p>
					 -->
					  <input type="submit"  class="btn green" id="button_medium" value=" CONTINUE "/> 
					</div>
		 <p>  &nbsp; </p>
		 </form>
		 </div>
		 </div>
		<!-- END EXAMPLE TABLE PORTLET-->
 
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
            <!-- END CONTENT -->
          
        
      
        <!-- END CONTAINER -->
<script language="javascript">

function radioValue(value,id)
	{  
	
	jQuery.uniform.update(jQuery("#radio"+id).attr('checked',true)); 
	$("#sms").val(value);  

  }

</script>