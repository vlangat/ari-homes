<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Packages </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Buy SMS </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
		<div style="background:#006699;padding:6px;">
			 <font color="#ffffff"><h4> <strong> Buy SMS via M-pesa <font id="method"> <?= strtoupper($this->session->userdata('mode'))?> </font> </strong> </h4> </font> </strong> </font>
		</div> 
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
	<div class="portlet light portlet-fit "> 
 
		 <div class="row">
	<div class="col-md-6 col-sm-6">
   <div class="portlet light "> 
			<div class="portlet-body">
			<p>   </p>
			<div id="content" style="width:100%">
<table border="0" width="100%"   id="chosen_payment"   style="border:1px solid lightgrey"> 
<tr>	<td colspan="2">
<p>  </p>		<?php $vat=0; ?>
			<ul style="padding:0px;list-style-type:none">
			<li>	<strong> Invoice No	# <?=$invoice_no?> </strong>	</li>
			<li>	<strong> <font color="red"> UNPAID	</font> </strong> </li>
			<li>	&nbsp;	</li>
			<li>	<strong> Invoiced To	</strong></li>
			<li>	&nbsp;	</li>
			<li>	<?=strtoupper($this->session->userdata('first_name'))?> <?=strtoupper($this->session->userdata('last_name'))?>	</li>
			<li>	<?=$this->session->userdata('email')?>	</li>
			<li>	<?=$this->session->userdata('company_name')?>	</li>
			<li> 	Nairobi, Kenya	</li>
			</ul>
		</td>
	</tr>	
		<tr   bgcolor="#32c5d2"> 
		<td colspan="2">  <strong id="strong">  Invoice Items </strong>   </td>
		</tr> 
		<tr > <td><?=$total_sms;?>  SMS </td> <td align="right">KES  <?=($total_sms_cost);?> </td></tr>
		<tr > <td><b> Sub Total </b></td> 
		<td align="right"><b>KES  <?=($sub_total=$total_sms_cost);?> </b> </td> </tr>   
		<tr > <td> VAT Tax   </td> <td align="right"> KES <?=16/100*$total_sms_cost;?> </td></tr>
		<tr > <td> Credit </td> <td align="right"> <?php  $credit=$package_balance; if($package_balance<0){ $credit=0-$package_balance;}?> KES <?=$credit?> </td></tr>
		<tr height="20"> <td><strong>  Total </strong> </td> <td align="right"><h4> <b>  KES <?=($vat+$sub_total-$credit);?> </h4>  </b>  </td></tr>
	</table>
	<?php   $total=$vat+$sub_total-$credit;?>
			 <p>     </p>
			 </div>
			 <div id="editor"></div>
			 <a  href="javascript:;"  class="btn grey" id="btn_invoice">   DOWNLOAD INVOICE  </a> 
		 </div>
		</div>
		 
		</div>
		
	<div class="col-md-6 col-sm-6"> 
		<div class="portlet light "> 
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12"> 
					<p>   </p>
					<?php if($total>0){?>
			<form action="#" method="post">
				<table border="0" width="100%" cellpadding="30" style="border:1px solid lightgrey"> 
					<tbody> 
					<tr align="center" id="mpesa_logo"> 
						<td> 
							  <img src="<?=base_url();?>images/mpesa.png" > 						
						</td>
					</tr> 
					 
					<tr height="40" id="mpesa_pay"> <td align="left" > 
						   <ul style="list-style-type:none">
								<li>	&nbsp;	</li>
								<li>	Due Date :<?php echo date("d-m-Y");?> </li>
								<li>	&nbsp;	<hr/></li>
								<li>	<strong> From your Mobile Phone </strong>	</li>
								<li>	   &nbsp;	  </li>
								<li>	  Go to <strong>  MPESA Menu  </strong>	</li>
								<li>	   &nbsp;	  </li> 
								<li>	  Select <strong> Lipa na Mpesa </strong> 	</li>
								<li>	   &nbsp;	  </li>
								<li>	  Select <strong> Pay Bill  </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	  Enter the Business No: <strong> 990850</strong> 	</li>
								<li>	   &nbsp;	  </li>
								<li>	 Enter Account number:  <strong>  <?=$invoice_no?>  </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	   Enter  Amount: <strong>KES  <?=$total?> </strong>	</li>
								<li>	   &nbsp;	  </li>  
								<li>	   Enter your MPESA PIN, Click Ok 	</li>
								<li>	   &nbsp;	  </li>	
								<li>	   Confirm by Clicking Ok   	</li>
								<li>	   &nbsp;	  </li>   
							</ul>
						</td>
						</tr> 
						 
			</tbody>						
					</table>
					
					<div class="row">
					<div class="col-md-12" id="mpesa_only">  
					 <p>  </p>
					<p> Wait for 3 Mins to allow API synchronization  </p>
					 <hr/>  
					 <p> Enter your M-pesa Transaction Code below</p>
						 
							<input type="text" value=""  id="mpesa_code" class="form-control input-medium">
						  <label id="error">  </label>
					  </div>
					  <div class="col-md-12"> 
					    
					</form>
					 <p>    </p>
					 
					<a  href="javascript:;" class="btn green"  id="button_large">   VERIFY & ACTIVATE </a> 
					 <?php
					  }else{
						  ?>
						  <div class="col-md-12" id="mpesa_only">  
					 <p> &nbsp; </p>
					<p> Your Account Balance is sufficient to purchase the selected package plan .  </p>
					  <p>  </p>
					 <p> Click on   "Confirm Transaction" button  below to accept the Transaction.</p>
					 <p>  </p> 
					 <p> <b> NB: </b> Once you click button below, transaction cannot be cancelled whatsoever </p>
					 <label id="error1">  </label>
					 <hr/> 
					  </div>
					  
						  <a  href="javascript:;" class="btn grey"  id="confirm_pay" >    CONFIRM TRANSACTION </a> 
					 <?php } ?> 
					<div class="margin-bottom-10 visible-sm"> </div>
					 
				</div>
				</div>
		</div>
	</div>
</div>
</div>
 

</div>

		 </div>
		<!-- END EXAMPLE TABLE PORTLET-->
 
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
            <!-- END CONTENT -->
 
      
        <!-- END CONTAINER -->
<script language="javascript">
 
	$(document).ready(function() { 
		getData();
	var selected="<?php echo $this->session->userdata('mode');?>";	 
		jQuery.uniform.update(jQuery("#"+selected+"").attr('checked',true)); 
	});
 
$("#button_large").click( function () {
		//var sms="<?=$this->session->userdata('total_sms');?>";
        var sms="<?=$total_sms?>";
		if(!sms){ sms=0;}
		var sms_cost=sms*3;  
        var invoice="<?=$invoice_no?>";
		var total="<?=$total?>";  
		var credit="<?=$credit?>";  
		var mpesa_code=$("#mpesa_code").val();
		var token=$("#token").val();
		if(!mpesa_code){ $("#error").html("<font color='red'> M-pesa Transaction code required </font>");return false;}
		
		$.ajax({ 
			url:"<?=base_url();?>payment/add_sms",
			type:"POST", 
			data:
			{
				'sms':sms,
				'mpesa_code':mpesa_code,
                'invoice_no':invoice,
				'total_cost':sms_cost,
				'credit':credit,
				'total_amount_to_pay':total
			},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{  
		        $("#error").html("<font color='green' size='3'> <i class='fa fa-check' style='font-size:20px;'> </i><b> You have successfully purchased "+sms+" SMS </b> </font>");
				setTimeout(function(){
						  $("#error").empty();
						  window.location.replace("<?=base_url();?>payment/package/success_payment");
                      }, 3000);
			}
			else
			{
				$("#error").html("<font color='red'>"+obj.msg+"</font>");
			}
		}
	})
		
		 
	
});
	
$("#confirm_pay").click( function () {
		//var sms="<?=$this->session->userdata('total_sms');?>"; 
         var sms="<?=$total_sms?>";
         var invoice="<?=$invoice_no?>";
		var overpay="<?=$package_balance?>";  
		 var r=confirm("Are you sure you want to proceed with transaction?");
	    if(r==true){ 
		 $("#error1").html("<font color='blue' size='3'> Processing...please wait</font>");	 
		 if(!sms){ sms=0;}
		var sms_cost=sms*3; 
		$.ajax({ 
			url:"<?=base_url();?>payment/add_sms_overpay",
			type:"POST", 
			data:
			{
				'sms':sms,
                'invoice_no':invoice, 
				'over_payment':overpay,
				'total_cost':sms_cost
			},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{  
		        $("#error").html("<font color='green' size='3'> <i class='fa fa-check' style='font-size:20px;'> </i><b> You have successfully purchased "+sms+" SMS </b> </font>");
				setTimeout(function(){
						  $("#error1").html("<font color='green' size='3'> Redirecting page...</font>");
						 window.location.replace("<?=base_url();?>payment/package/success_payment");
                      }, 2500);
			}
			else
			{
				$("#error").html("<font color='red'>"+obj.msg+"</font>");
			}
		}
	})
	
}	
		 
	
});
	
 
	$("#btn_invoice").click( function () {
            var divContents = $("#content").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Invoice</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
		
		$("#buy_sms").click(function(){
		var sms=$('#sms').val();  
		 if(!sms){ sms=0;}
		var sms_cost=sms*3; 
		$.ajax({
		url:"<?=base_url();?>payment/buy_sms",
		//url:"<?=base_url();?>payment/add_sms",
		type:"POST", 
		data:{
			'sms':sms,
			//'mpesa_code':code,
			'total_cost':sms_cost
		},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{ 
				window.location = "<?=base_url();?>payment/buy_sms";
		      // $("#error").html("<font color='green'>You have successfully bought SMS </font>");
			}
			else
			{
				$("#error").html("<font color='red'>"+obj.msg+"</font>");
			}
		}
	})
	
	});
</script>