<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Packages </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Payment Checkout</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 <?php if(!$credit){ $credit=0;}?>
<div class="page-content-inner">
  
<div class="row">
	<div class="col-md-12">
		<div style="background:#1bb968;padding:6px;">
		<font color="#ffffff"> <strong> &nbsp;&nbsp; Payment/Checkout </strong> </font>
							
		</div> 
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
<div class="portlet light ">
 <div class="portlet-title"> 
		 <div class="row">
			<div class="col-md-6 col-sm-6">
					<div class="portlet light "> 
						<div class="portlet-body"> <!--bgcolor="#DFDAD9"-->
							<table border="1" width="100%"  id="payment_checkout"   style="border:1px solid lightgrey"> 
								<tr   bgcolor="#32c5d2" > <td  colspan="2">  <strong> <font color="#fff" size="3" >  Price </font></strong>  </td></tr>
								<tr> <td> 
								<?=$years;?> Year(s), <?=$users;?> User(s)  &nbsp; </td> <td align="right"> KES <?=$total_package_bill;?>  <br/> 
								</div>
								</td></tr>
								<tr> <td><b> Sub Total </b></td> <td align="right"><b>KES  <?=$total_sub_total;?> </b></td></tr>
								<tr> <td> Set Up Fees </td> <td align="right"> KES 0</td></tr>
								<tr  > <td> <?=$total_sms;?>  SMS </td> <td align="right">KES  <?=$total_sms_cost;?> </td></tr>
								<tr> <td> <font color="green"> <b> Sum Total </b> </font> </td> <td align="right"> <font color="green"><b>KES  <?=($sum_total=$total_sum_total);?>  </b></font></td></tr>
								<tr> <td> VAT Tax @16%  </td> <td align="right"> KES <?=$vat=$total_vat;?> </td></tr>
								<tr> <td> Credit  </td> <td align="right"> KES <?=$credit;?> </td></tr>
								 
								<tr> <td colspan="2" align="right"> <h3> <b> KES <?=($vat+$sum_total-$credit);?>  <br/> </h3> Total Due Today </b> </td></tr>
							 </table>
							  
					</div>
				</div> 
		</div>
		
	<div class="col-md-6 col-sm-6"> 
		<div class="portlet light "> 
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12"> 
					<form action="<?=base_url();?>payment/preferred_pay" method="post">
					 <table border="0" width="100%"   style="border:1px solid lightgrey"> 
					 
						<tr height="30" bgcolor="#32c5d2">
							<td align="center">  <strong> <font color="#fff"size="3"> Choose your payment Method </font></strong> </h4> </td>
						</tr> 
					<tr height="40"> 
					<td> 
						<div class="radio-list">
						<label class="radio-inline">
								<input type="radio" name="mode" required value="mpesa">  &nbsp; &nbsp; <img src="<?=base_url();?>images/mpesa.png" height="75" width="150" > 
						 </label>
						</div>
						<br/>
					</td>
					</tr> 
					<tr height="40"> 
					<td> 
						 <div class="radio-list">
								<label class="radio-inline">
									<input type="radio" name="mode"  required value="gtbank"> &nbsp; &nbsp; <img src="<?=base_url();?>images/GTBank.png" height="75" width="150">
								</label> 
							 </div>
							 
						<br/>
					</td>
					</tr> 
 				
					</table>
					 
					 <p>    </p>
					 <input type="hidden"   name="credit" value="<?=$credit?>"/> 
					 <input type="submit"   class="btn green" id="button_large" value="PROCEED TO PAYMENT" > 
					 
					</div>
					<div class="margin-bottom-10 visible-sm"> </div>
					 
				</div>
			</div>
		</div>
	</div>
 </form>

</div>
</div>
<!-- END EXAMPLE TABLE PORTLET--> 
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>