<?php 
	$count1=0;$count2=0;$count3=0;$count4=0;
	$pending=0;$solved=0;$total=0; $sent=0; $trash=0;
	 foreach($inbox->result() as $r){ $total++;  if($r->status==1){$pending++; } else if($r->status==2){ $solved++;}}
	 foreach($admin_queries->result() as $r){     $sent++;  if($r->audit_number==2){ $trash++;}  }
	 foreach($trashed->result() as $t){      $trash++;   }
?>
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
   
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE CONTENT BODY -->
	<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="index.html">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Support</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Queries</span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER --> 
 <div class="row">
<div class="col-lg-3">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value="<?=$total?>"><?=$total?></span>
				<small class="font-green-sharp"> </small>
			</h3>
			<small> Total Queries </small>
		</div>
		<div class="icon">
			<i class="glyphicon glyphicon-question-sign"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:100%;" class="progress-bar progress-bar-success green-sharp">
				<span class="sr-only"> </span>
			</span>
		</div>
			<div class="status">
				<div class="status-title"> Total Queries </div>
			 <div class="status-number"> 100% </div> 
		</div>
	</div>
</div>
</div>
<div class="col-lg-5">
<div class="dashboard-stat2">
	<div class="display">
		<div class="number">
			<h3 class="font-red-haze">
				<span data-counter="counterup" data-value="<?=$pending?>"> 0 </span>
			</h3>
			<small> Pending </small>
		</div>
		<div class="icon">
			<i class='glyphicon glyphicon-warning-sign' style="color:brown">	</i>   
		</div>
	</div>
	<div class="progress-info">
		<div class="progress"> <?php $s=$total;if($total==0){ $s=1;} $per=$pending/$s*100;?>
			<span style="width:<?=$per;?>%" class="progress-bar progress-bar-success red-haze">
				<span class="sr-only">   </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title">  Pending Queries </div>
			<div class="status-number">  <?=round($per)?>%    </div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-4 ">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-purple-soft" style="color:green">
				<span style="color:green" data-counter="counterup" data-value="<?=$solved?>"><?=0?></span>
			</h3>
			<small>   Solved   </small>
		</div>
		<div class="icon">
			<i class="glyphicon glyphicon-ok-sign" style="color:green"> </i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress"> <?php $t=$total;if($total==0){ $t=1;} $sol=$solved/$t*100;  ?>
			<span style="width: <?=$sol?>%;" class="progress-bar progress-bar-success purple-soft">
				<span class="sr-only"> </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Solved Queries </div>
			<div class="status-number"> <?=round($sol)?>% </div>
		</div>
	</div>
</div>
</div>
</div>
 
<div class="page-content-inner">
<div class="inbox">
<div class="row">
<div class="col-md-3">
	<div class="inbox-sidebar"><!--onclick="compose()"-->
		<a href="javascript:;" id="compose_msg"  data-title="Compose" class="btn red compose-btn btn-block">
			<i class="fa fa-plus"> </i>   New Query</a>
		<ul class="inbox-nav">  		
			<li class="divider"> </li>	
			<li>
				<a href="<?=base_url();?>support/admin_view"  data-type="sent" data-title="Sent"> Inbox 
				<span class="badge badge-success"><?=$total?></span>
				</a>
				
			</li>
			<li class="divider"> </li>	
			<li>
				<a href="<?=base_url();?>support/sent"  data-type="sent" data-title="Sent"> Sent 
				<span class="badge badge-warning"><?=$sent?></span>
				</a>
				
			</li>
			<!--<li class="divider"> </li>
			<li>
				<a href="javascript:;" onclick="loadMessages('draft','','')" data-type="draft" data-title="Draft"> Draft
					<span class="badge badge-danger"> <?=$count3?> </span>
				</a>
			</li>-->
			<li class="divider"></li>
			<li>
				<a href="<?=base_url();?>support/trash"  class="sbold uppercase" data-title="Trash"> Trash
					<span class="badge badge-danger">  <?=$trash++?> </span>
				</a>
			</li>
		<li class="divider"></li> 
	</ul> 
</div>
</div>
<div class="col-md-9">
<div class="inbox-body">
<select id="delete" onchange="validate()"> <option value=''> Action </option> <option value='2'> Trash Message </option> <option value='1'> Undo Trash </option> </select>
   <hr/> 
<div class="inbox-header">
	<h1 class="pull-left"> <font id="smss_title" size="3"> <font> </h1>
	 <font color="#006699"><strong> Recent Messages &amp;  Queries </strong> </font>  
	
 
</div>
	<div class="inbox-content">  
  
	<table  style="font-family:verdana" class="table table-striped table-hover table-bordered" id="sample_editable_1">
		<thead>
			<tr>  <th> # </th> <th> </th> <th> Date </th> <th> From </th> <th> Subject </th> <th> Description  </th>  <th> Status </th>      </tr>
		</thead>
	  <tbody>
	  <form action="" onsubmit="validate()">
			<?php $x=1;
			foreach($queries->result() as $r){?>
			<tr > <td> <?=$x?></td> <td> <input type="checkbox" name="checkbox[]" value="<?=$r->id?>"></td> <td onclick="response(<?=$r->id?>)"> <?=$r->date_sent?> </td> <td onclick="response(<?=$r->id?>)"> <?=substr($r->email,0,8)?>   </td> <td onclick="response(<?=$r->id?>)"> <?=$r->subject?> </td>  <td onclick="response(<?=$r->id?>)"> <?=substr($r->description,0,18).'...'?>  </td> <td>  <?php $msg="Pending"; $t_color="#fff"; $color="red"; $status=$r->status; if($status=="2"){ $msg="Solved"; $color="green"; }if($status=="3"){ $msg="support"; $color="yellow"; $t_color="red"; }?> <font style="background:<?=$color?>;color:<?=$t_color?>"> <?=$msg?> </font> </td>   </tr>
	  <?php 
		  $x++;
		  } 
	  ?>
	  </form>
	  </tbody>
	</table> 
	
	
		</div>
	</div>
</div>
</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>

 <div id="confirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
			<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"> Confirmation Message </h4>
			</div>
			<div class="modal-body">
				<p>  Do you want to perform action on the selected Messages? </p>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" id="confirmDelete" class="btn blue">Yes</button> 
				<button class="btn default" data-dismiss="modal" aria-hidden="true">No</button>
			</div>
			</div>
		</div>
		 
</div>


<div id="compose_sms" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Compose New Query </h4>
			</div>
			<div class="modal-body">
				<input type="hidden" value="" id="msg_id">
				<div>
				 <label > To: </label> 
					<div> 
				 <select  class='selectpicker' multiple data-live-search='true' id='to' name='to'   title='Select Receipient...' onchange="check_if_all(this.value)">
					  <option value="all"> All </option> 
					   <?php foreach($receipients->result() as $row){	?>
						 <option value="<?=$row->email?>"> 
							<?=$row->email?>
						 </option>
					 <?php }?>
				 </select>		
					</div>
				</div>   
				<p>   </p>
				<div>
			 <label > Subject: </label> 
				<div> 
					<input type="text" class="form-control" id="subject" name="subject">
				</div>
			</div>
			<p>   </p>
		<div class='inbox-form-group'> <label class='control-label'> Description   </label>
			<textarea class='inbox-editor inbox-wysihtml5 form-control' id='message' rows='6'></textarea>
		</div>
			<div>
			<p id="counter"> </p> <p id="error"> </p>
			</div> 
		<div class="modal-footer">
				<button class='btn green' onclick='send()'>
			<i class='fa fa-check'></i> Send Query </button> 
			<button class="btn default" data-dismiss="modal" aria-hidden="true"> Close </button>
			 
		</div>
</div> 
		</div>
	</div>
</div>
<!-- END CONTAINER -->
<script type="text/javascript">

function validate()
{
var value=$("#delete").val();
if(!value){ return false;}	
var chks = document.getElementsByName('checkbox[]');
var hasChecked = false;
for (var i = 0; i < chks.length; i++)
{
if (chks[i].checked)
{
hasChecked = true;
break;
}
}
if (hasChecked == false)
{
alert("Please select at least one message.");
//jAlert('Please select at least one message to perform the action');
return false;
}
$("#confirmModal").modal('show');
return true;
}
  
 
 
$(function(){
	
 $("#compose_msg").click(function(){
	  $("#compose_sms").modal('show');
  });
  $("#confirmDelete").click(function(){
		var favorite = [];
		$.each($("input[name='checkbox[]']:checked"), function(){            
			favorite=($(this).val()); 
			deleteMessage(favorite);
		});
	   location.reload();
 });

 }); 
 
 function deleteMessage(id)
 {
	 
	 $.ajax({
			url:"<?=base_url();?>support/removeMessages/"+id,
			type:"post",
			success: function(data){ 
			var obj = JSON.parse(data);
			if (obj.result=="ok"){ 	 
                //jAlert('There was an error in deleting the messages');
			}
			else
			{ 
				jAlert('There was an error in performing the action');
			}
	}	
}	
)	
}
 
function send(){  	
	 
	var to=$("#to").val();	 
	var body=$("#message").val(); 
	var subject=$("#subject").val();  
	if(!subject){ $("#error").html("<font color='red'> Subject Required  </font>"); $("#subject").focus(); return false;}
	if(!to){ $("#error").html("<font color='red'>Select Receipient  </font>"); $("#to").focus(); return false;}
	if(!body){ $("#error").html("<font color='red'>Message is empty.  </font>"); $("#message").focus(); return false;}
	//if(/^[a-zA-Z0-9- ]*$/.test(body) == false){ $("#message").focus(); $("#error").html("<font color='red'>Message content   have illegal characters </font>");  return false; } else{$("#error").empty();} 
    $("#error").html("<font color='#006699'> Sending message waiting...</font>"); 
	$.ajax({ 
			url:"<?=base_url();?>support/compose_message",
			type:"POST",
			async:false,
			data:
			{   
				'to':to,   
				'subject':subject,
				'body':body
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{
					$("#to").val('');
					$("#message").val(''); 
					$("#subject").val('');
					$("#error").html("<font color='#006699'>  Message sent successfully </font>");
					setTimeout(function()
						{
							location.reload(); 
						}, 2500); 
				}
				else{
					$("#error").html("<font color='red'> Message not sent! Try again later  </font>");
				}
					
			} 
		}) 
}
 
 
function response(id)
{  	 
	var id2="<?=substr(md5(date("d")),0,16)?>";
	window.location="<?=base_url();?>support/admin_response/"+id+"/"+id2; 
}
function check_if_all(value)
{  	 
     if(value=="all")
	 { 
		  $("#to").val('all');
		 document.getElementById('to').disabled = true;
		 
	 }
 
}

</script>

