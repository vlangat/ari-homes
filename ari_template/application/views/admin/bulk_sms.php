<?php  $count1=0;$count2=0;$count3=0;$count4=0; $z=0; $sent_sms=1; $sms=1;?>
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="index.html">Home</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="#">Pages</a>
		<i class="fa fa-circle"></i>
	</li>
	<li>
		<span>Messages</span>
	</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER --> 
<div class="row">
 
<div class="col-lg-3">
<div class="dashboard-stat2 "> 
	<div class="display">
		<div class="number">
			<h3 class="font-green-sharp">
				<span data-counter="counterup" data-value="<?php echo $z=substr($sms_balance, 4)+$total_sms;?>"><?php echo $z;?></span>
				<small class="font-green-sharp">		</small>
			</h3>
			<small> SMS Subscribed   </small>
		</div>
		<div class="icon">
			<i class="fa fa-envelope-o"></i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:100%;" class="progress-bar progress-bar-success green-sharp">
				<span class="sr-only"> </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Subscribed SMS </div>
			 <div class="status-number"> 100% </div> 
		</div>
	</div>
</div>
</div>
<div class="col-lg-5">
<div class="dashboard-stat2">
	<div class="display">
		<div class="number">
			<h3 class="font-red-haze">
				<span data-counter="counterup" data-value="<?=$total_sms?>"> <?=$total_sms?> </span>
			</h3>
			<small> Sent SMS </small>
		</div>
		<div class="icon">
			<i class="fa fa-comment">	</i>   
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width:<?=$x=$sent_sms/$sms*100?>%;" class="progress-bar progress-bar-success red-haze">
				<span class="sr-only">   </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title">  No of  Sent SMS </div>
			<div class="status-number">  <?=$x?>%    </div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-4 ">
<div class="dashboard-stat2 ">
	<div class="display">
		<div class="number">
			<h3 class="font-purple-soft" style="color:green">
				<span style="color:green" data-counter="counterup" ><?=substr($sms_balance, 4)+0?></span>
			</h3>
			<small>   Remaining SMS </small>
		</div>
		<div class="icon">
			<i class="fa fa-envelope"> </i>
		</div>
	</div>
	<div class="progress-info">
		<div class="progress">
			<span style="width: 57%;" class="progress-bar progress-bar-success purple-soft">
				<span class="sr-only"> </span>
			</span>
		</div>
		<div class="status">
			<div class="status-title"> Remaining SMS </div>
			<div class="status-number"> <?=100-$x?>% </div>
		</div>
	</div>
</div>
</div>
</div>
 
	
<div class="page-content-inner">
<div class="inbox">
<div class="row">
 
<div class="col-md-12">
<div class="inbox-body"> <span style="float:right"><a href="javascript:;" class="btn green" id="new_sms"> Compose New SMS </a> </span><br/><br/>
<hr/>
<div class="inbox-content">  
  
	<table   class="table table-striped table-hover table-bordered" id="sample_editable_1">
    <thead>
        <tr> 
            <th> Date </th>
            <th> Receipient </th>
            <th> Message Body</th>
            <th> Status</th>
            <th> Action</th>
        </tr>
    </thead>
    <tbody> 
	<?php $x=1; foreach($messages->result() as $row){	?>
	<tr>   
			<td> <?=$row->date_sent?>  </td>
			<td> <?=$row->to?> </td>
			<td> <?=$row->message?> </td>
			<td> <?=$row->message_type?>  </td>
			<td> <a href="<?=base_url();?>admin/deleteAdminMsg/<?=$row->id?>" onclick="return confirm('Delete this message?')"><i class="fa fa-trash-o"> </i> Delete </a>    </td>
	</tr>
	
	<?php $x++; } ?> 
	
	</tbody>
	</table>
   
   </div> 
   
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>  

 <div id="compose_sms" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Compose New Message</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" value="" id="msg_id">
				<div>
				 <label > To: </label> 
					<div> 
				 <select  class='selectpicker' multiple data-live-search='true' id='to'   title='Select Receipient...'>
					 <?php foreach($receipients->result() as $row){	?>
					 <option value="<?=$row->mobile_number?>"> 
					 <?php $name=$row->first_name." ".$row->last_name; if($name !=""){ echo $name.","."(".$row->mobile_number.")"; }else{ echo $row->company_name."(".$row->mobile_number.")";}?>
					 </option>
					 <?php }?>
				 </select>		
					</div>
				</div>   
				<p>   </p>
		<div class='inbox-form-group'> <label class='control-label'> Message   </label>
			<textarea class='inbox-editor inbox-wysihtml5 form-control' id='message' rows='12'></textarea>
		</div>
			<div>
			<p id="counter"> </p> <p id="error"> </p>
			</div> 
		<div class="modal-footer">
				<button class='btn green' onclick='send()'>
			<i class='fa fa-check'></i> Send </button> 
			<button class="btn default" data-dismiss="modal" aria-hidden="true"> Close </button>
			 
		</div>
</div> 
		</div>
	</div>
</div>

 
<!-- END CONTAINER -->
<script type="text/javascript">
 
$(function(){ 
//loadMessages('sent');

$("#new_sms").click(function(){  

 $("#compose_sms").modal('show');
 
 });
 
$("#message").keydown(function(){ 
	var str = $("#message").val();
	var char_length = str.length; 
	var pages=1;
	var display_value=0;
	display_value=(160-char_length);
	if(char_length>160){pages=char_length/160;}
	pages=Math.ceil(pages);
	var x=pages-2;
	while(x>=0){ if(display_value <= 0){display_value = display_value+160;} x--;  } 
	$("#counter").html(display_value+"/"+pages);
	$("#total_pages").val(pages)
}); 
 
 });
 

function send(status){  	
	if(!status){ status="sent";}
	var to=$("#to").val();
	var body=$("#message").val();
	if(!to){ $("#error").html("<font color='red'>Receipient Number is Required</font>");return false;}
	$("#error").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>admin/sms",
			type:"POST",
			async:false,
			data:
			{
				'to':to,  
				'message_type':status,
				'body':body
			},
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				if(status=="inbox"){
					$("#success").html("<font color='green'> Message sent successfully </font>");
				}else{
					$("#success").html("<font color='green'> Message successfully saved as draft  </font>");
				}
				$("#to").val('');
				$("#message").val('');
				$("#cc").val('');
				$("#subject").val('');
				location.reload();
			}
			else
			{
				$("#success").html("<font color='red'> Error occurred! Please contact your administrator  </font>");
			}
				
		}
	
}
)
		
}
</script>