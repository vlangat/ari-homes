<!DOCTYPE html> 
<html lang="en"> 
<head profile="http://www.w3.org/2005/10/profile">
<link rel="icon" 
      type="image/png" 
      href="<?=base_url();?>images/favicon.png">
<meta charset="utf-8" />
<title>ARI Homes | Real Estate Management</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- Dependencies -->
<script src="<?=base_url();?>template/popup_js/jquery.js" type="text/javascript"></script>
<script src="<?=base_url();?>template/popup_js/jquery.ui.draggable.js" type="text/javascript"></script>
<!-- core files -->
<!-- core files -->
<script src="<?=base_url();?>template/popup_js/jquery.alerts.js" type="text/javascript"></script>
<link href="<?=base_url();?>template/popup_css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
 <!--END OF POP-UPS------->
<!-- BEGIN PAGE LEVEL STYLES -->    
<link href="<?=base_url();?>template/theme/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
 
  <link rel="stylesheet" href="<?=base_url();?>css/bootstrap-select.css">
  <script src="<?=base_url();?>js/bootstrap-select.js"></script> 
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

 <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
 
	
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid page-boxed">
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="<?=base_url();?>admin/home">
					<img src="<?=base_url();?>images/login/ARI_Homes_Logo.png" width="90" height="45" alt="logo">
				</a>
			</div>

<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
			 
					<li class="droddown dropdown-separator">
						<span class="separator"></span>
					</li>
		 
<!-- BEGIN USER LOGIN DROPDOWN -->
<li class="dropdown dropdown-user dropdown-dark">
	<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
		<img alt="" class="img-circle" src="<?=base_url();?>media/<?php echo $this->session->userdata('photo');?>">
		<span class="username username-hide-mobile"><?php echo $this->session->userdata('first_name');?></span>
	</a>
	<ul class="dropdown-menu dropdown-menu-default">
		<li>
			<a href="<?=base_url();?>admin/account">
				<i class="icon-user"> </i> My Profile </a>
		</li> 
		<li>
			<a href="<?=base_url();?>admin/messages">
				<i class="icon-envelope-open"> </i> My Inbox
				<!--<span class="badge badge-danger"> 3 </span>--->
			</a>
		</li> 
		<li>
			<a href="<?=base_url();?>admin/logout">
				<i class="icon-key"></i> Log Out </a>
		</li>
	</ul>
</li>
<!-- END USER LOGIN DROPDOWN -->
<!-- BEGIN QUICK SIDEBAR TOGGLER -->
<li class="dropdown dropdown-extended quick-sidebar-toggler">
	<span class="sr-only">Toggle Quick Sidebar</span>
	<!--<i class="icon-logout"></i>-->
</li>
<!-- END QUICK SIDEBAR TOGGLER -->
</ul>
</div>
<?php

$id=$this->session->userdata('admin_id');  
	  if($this->session->userdata('username') !=""){
			//////	 
	  }
		else
		{
			redirect(base_url()."admin/logout");
		}  
		
		 if($id ==""){
		     
		     	redirect(base_url()."admin/logout");
			//////	 
	  }
	?>

<!-- END TOP NAVIGATION MENU -->
</div>
</div>
<!-- END HEADER TOP -->
<!-- BEGIN HEADER MENU -->
<div class="page-header-menu">
<div class="container">
 
<!-- BEGIN MEGA MENU -->
<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
<div class="hor-menu  ">
<ul class="nav navbar-nav">
<li class="menu-dropdown classic-menu-dropdown active">
	<a href="<?=base_url();?>admin/home"> Home
		<span class="arrow"></span>
	</a>

</li>

<li class="menu-dropdown classic-menu-dropdown ">
	<a href="javascript:;"> Users
		<span class="arrow"></span>
	</a>
	<ul class="dropdown-menu pull-left">
		<li class="">
			<a href="<?=base_url();?>admin/users/paid" class="nav-link  "> Paid </a>
		</li>
		<li class="">
			<a href="<?=base_url();?>admin/users/free" class="nav-link  "> Free </a>
		</li> 
		 <li class="">
			<a href="<?=base_url();?>admin/administrators/" class="nav-link"> Administrators </a>
		</li>
		<li class="">
			<a href="<?=base_url();?>admin/signUpTenants/" class="nav-link"> Tenants </a>
		</li> 
		 <li class="">
			<a href="<?=base_url();?>admin/referredAgents/" class="nav-link"> Referred Agents </a>
		</li> 
		 
	</ul>
</li>

<li class="menu-dropdown classic-menu-dropdown ">
	<a href="javascript:;"> Properties
		<span class="arrow"></span>
	</a>
	<ul class="dropdown-menu pull-left">
		<li class=" ">
			<a href="<?=base_url();?>admin/properties/" class="nav-link"> Live </a>
		</li>
		<li class=" ">
			<a href="<?=base_url();?>admin/properties/removed" class="nav-link"> Removed </a>
		</li> 
		 
	</ul>
</li>

<li class="menu-dropdown classic-menu-dropdown ">
	<a href="javascript:;"> Tenants
		<span class="arrow"></span>
	</a>
	<ul class="dropdown-menu pull-left">
		<li class=" ">
			<a href="<?=base_url();?>admin/tenants/" class="nav-link  "> Live </a>
		</li>
		<li class=" ">
			<a href="<?=base_url();?>admin/tenants/removed" class="nav-link  "> Removed </a>
		</li> 
		 
	</ul>
</li>

<li class="menu-dropdown classic-menu-dropdown">
	<a href="<?=base_url();?>support/chat">  Support
		<span class="arrow"> </span>
	</a> 
</li> 
  
<li class="menu-dropdown classic-menu-dropdown ">
	<a href="javascript:;"> Customers
		<span class="arrow"></span>
	</a>
	<ul class="dropdown-menu pull-left">
		<li class=" ">
			<a href="<?=base_url();?>customers/" class="nav-link"> Add Customer </a>
		</li>
		<li class=" ">
			<a href="<?=base_url();?>customers/calls/" class="nav-link"> Calls </a>
		</li>
		<li class=" ">
			<a href="<?=base_url();?>customers/demo" class="nav-link"> Demo </a>
		</li> 
		 
	</ul>
</li>
 
 
<li class="menu-dropdown classic-menu-dropdown">
	<a href="<?=base_url();?>admin/partners">  Partners
		<span class="arrow"> </span>
	</a>
 
</li> 
 
<li class="menu-dropdown classic-menu-dropdown ">
	<a href="javascript:;"> Packages
		<span class="arrow"></span>
	</a>
	<ul class="dropdown-menu pull-left">
		<li class=" ">
			<a href="<?=base_url();?>admin/paid_packages/mpesa" class="nav-link  "> Mpesa  Payment</a>
		</li>
		<li class=" ">
			<a href="<?=base_url();?>admin/paid_packages/GTBank" class="nav-link"> GTBank Payment</a>
		</li>
		<li class=" ">
			<a href="<?=base_url();?>admin/package/" class="nav-link"> Payment Statement </a>
		</li>
		 
	</ul>
</li>


<li class="menu-dropdown classic-menu-dropdown ">
	<a href="<?=base_url();?>admin/messages"> Messages
		<span class="arrow"> </span>
	</a>
 
</li>

<li class="menu-dropdown classic-menu-dropdown ">
	<a href="<?=base_url();?>admin/logs"> Audit Trail
		<span class="arrow"> </span>
	</a> 
</li>

<li class="menu-dropdown classic-menu-dropdown ">
	<a href="<?=base_url();?>admin/ip_history"> Login History
		<span class="arrow"> </span>
	</a> 
</li>

</ul>
</div>
<!-- END MEGA MENU -->
</div>
</div>
<!-- END HEADER MENU -->
</div>

<script type='text/javascript'>
function checkChars(id)
{
   var TCode = document.getElementById(id).value;
   if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false)
   {
		//alert(id+' contains illegal characters');
		$("#error_msg").html("<font color='red'> "+id+" field contains illegal characters.Only [Aa-Zz] or [0-9] required</font>"); 
		 document.getElementById(id).value="";
		//$("#text_field_error").modal('toggle');
alert(''+id+' field contains illegal characters.Only [Aa-Zz] or [0-9] required');		
        return false;
   }
    return true;     
}
function checkIt(evt)
{  
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        status = "This field accepts numbers only."
        return false
    }
    status = ""
    return true
}
</script>
 
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->