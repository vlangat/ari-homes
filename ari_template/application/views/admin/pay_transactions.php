<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Paid Packages </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar">
 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th>
							<th> Name  </th>
							<th> Phone Number</th> 
							<th> Payment Mode</th> 
							<th> Transaction Code</th> 
							<th> Amount </th>
							<th> Date  </th> 
							<th> Description </th> 
							<th> View </th> 
						</tr>
					</thead>
					<tbody>
						 <?php $x=1; $total=0;
						 foreach($payment_info->result() as $p){  ?>
						<tr>
							<td><?=$x?> </td>
							<td><?=$p->name?> </td>
							<td> 
							<?=$p->phonenumber?>
							</td>
							<td> <?=$p->payment_mode?> </td>
							<td> <?=$p->receipt?> </td>
							<td> <?=$amount=($p->amount)/100?> </td>
							<td> <?=$p->time?>  </td>
							<td> <?=$p->note?> </td> 				 
							<td>  
							<?php 
							
						$note=str_replace(",","",$p->note);
					    $note=str_replace("'","",$note);
						$name=str_replace("'","",$p->name);
						 
						?>
								<a   href="javascript:;" onclick="edit('<?=$p->id?>','<?=$name?>','<?=$p->phonenumber?>','<?=$p->payment_mode?>','<?=$p->receipt?>','<?=$note?>','<?=$amount?>')"> <i class="fa fa-edit"> </i>  Edit </a>
							</td> 
						</tr>
						 <?php $x++; $total=$total+$amount;
						 }
						 ?>  
						 
					</tbody>
				</table>
				<button class='btn green' id="add_new">
			<i class='fa fa-plus'> </i> Add New </button> 
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div> 
    
<div id="add_payment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Add New Payment</h4>
			</div>
			<div class="modal-body">
	<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
	<div class="row">
	<div class="col-md-12">  
				 <p>
			<label class="control-label">Name (Optional)</label>
				<input   class="form-control" type="text" id="name"  name="name" onchange="validate_char('name')"> 
				<input   class="form-control" type="hidden" value="" id="edit_id"> </p>
				
			<p>
			<label class="control-label">Phone No (Optional)</label>
				<input   class="form-control" type="text" id="phone" maxlength="10" name="phone"> </p>
			 <p>
			<label class="control-label">Mode</label>
				<select  class="form-control"  id="mode" name="mode"> 
				<option value="mpesa">MPESA</option>
				<option value="gtbank">GTBANK</option>
				</select>
				</p>
			 <p>
			
			<label class="control-label">Transaction Code </label>
				<input   class="form-control" type="text" id="code"  name="code" onchange="validate_char('code')"> </p>
			 <p>
			<label class="control-label">Amount (Ksh.) </label>
				<input   class="form-control" type="number" id="amount" name="amount" > </p>
		
			 <p>
			 <label class="control-label"> Description </label>
			
			<textarea class="form-control" id="description" name="description" onchange="validate_char('add_description')"></textarea>
			</p>
		</div>
		</div>  <span id="add_msg">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="submit" value="Submit"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>
        <!-- END CONTAINER -->
<script language="javascript">
    $(function(){ 
//loadMessages('sent');

$("#add_new").click(function(){  
$("#code").val(''); $("#amount").val(''); $("#name").val(''); $("#phone").val(''); $("#description").val('');
 $("#add_payment").modal('show');
 
 });
 
 
 $("#submit").click(function(){   
	var code=$("#code").val();
	var amount=$("#amount").val();
	var name=$("#name").val();
	var phone=$("#phone").val();
	var mode=$("#mode").val();
	var id=$("#edit_id").val() 
	var description=$("#description").val();
	if(!name){ $("#add_msg").html("<font color='red'>Name  is Required</font>");  $("#name").focus(); return false;}
	if(!phone){ $("#add_msg").html("<font color='red'>Mobile Number is Required</font>"); $("#phone").focus(); return false;}
	if(!code){ $("#add_msg").html("<font color='red'>Transaction Code is Required</font>");  $("#code").focus(); return false;}
	if(!amount){ $("#add_msg").html("<font color='red'>Amount Paid is Required</font>"); $("#amount").focus(); return false;}
	$("#add_msg").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>admin/add_payment",
			type:"POST",
			async:false,
			data:
			{
				'edit_id':id,  
				'mode':mode,  
				'code':code,  
				'amount':amount,
				'name':name,
				'phone':phone,
				'description':description
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{
					if(id){ }else{
						$("#code").val(''); $("#amount").val(''); $("#name").val(''); $("#phone").val(''); $("#description").val('');
					}
					$("#add_msg").html("<font color='green'> Payment updated successfully </font>");
						setTimeout(function(){
						  $("#add_msg").empty();
						  $("#add_new").modal('hide');
						 window.location.replace("<?=base_url();?>admin/package");
                      }, 2500);
				}
				else
				{
					$("#add_msg").html("<font color='red'> Payment <b> Not </b> added. Check Transaction code if correct </font>"); 
				} 
			} 
	})
 
 });
  
  
  
 });
 
 
 function edit(id,name,phone,mode,code,description,amount){  
 
   $("#title").html('<font> Edit Payment Details </font>');
  $("#edit_id").val(id);$("#code").val(code); $("#amount").val(amount); $("#name").val(name); $("#phone").val(phone); $("#mode").val(mode); $("#description").val(description);
 $("#add_payment").modal('show');
 
 } 
 
</script>