 <?php $i=0; foreach($customer_info->result() as $p){ $i++; }?>
 <!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Customers </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Demo </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER --> 
<div class="page-content-inner"> 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar">
 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th> 
							<th> Company  </th>
							<th> Contact Person</th> 
							<th> Email</th> 
							<th> Phone No</th>  
							<th> Demo Date  </th>		 
							<th> Demo By  </th>		 
							<th> Comment  </th>		 
							<th> Remarks after</th>		 
							<th> Action </th>  
						</tr>
					</thead>
					<tbody>
						 <?php $x=1;
						 foreach($customer_info->result() as $p){  $total=0;?>
						<tr>
							<td><?=$x?> </td>
							<td><?=$p->company?></td>
							<td> <?=$p->contact_person?> </td> 
							<td><?=$p->email?> </td>
							<td><?=$p->phone?> </td>  
							<td> <?=$p->demo_date?> </td> 
							<td> <?=$p->demo_by?> </td> 
							<td> <?=$p->additional_comment?> </td> 
							<td  title="click to edit" onclick="edit_remarks('<?=$p->id?>','<?=$p->remarks?>')"> <?=$p->remarks?> </td> 
							<td>
							 <a href="javascript:;" onclick="deleteCustomer('<?=$p->id?>')"><i class="fa fa-edit"></i> Delete</a>	 
							</td> 
						</tr>
						 <?php $x++; //$total=$total+$amount;
						 }
						 ?>  
						 
					</tbody>
				</table>
				  
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
  

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div> 
    
<div id="add_action" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Schedule  Demo  </h4>
				<input type="hidden" class="form-control" id="id" value="" > 
			</div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				<div class="col-md-12">  
				 <p>
				<div class="form-group"> 
					<label class="control-label"> Demo Date </label>
					<input type="text" required class="form-control  input-xxlarge date-picker" value="<?=date('m/d/Y')?>" data-date-format="dd/mm/yyyy"  onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" id="Demodate" name="Demodate" />
				</div> 
			 
			 <p>
			 <label class="control-label"> Assign To </label> 
			<input type="text" class="form-control" id="assign_to" value="" > 
			
			</p>
			 <p>
			 <label class="control-label"> Your Comments (Optional)</label> 
			<textarea type="text" class="form-control" id="remarks" value="" ></textarea>
			</p>
		</div>
		</div>  <span id="add_msg">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="submit" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>


<div id="edit_remarks" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title"> Remarks after Demo  </h4>
				 
			</div>
	<div class="modal-body">
		<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
				<div class="col-md-12"> 
				
				 <p>
			 <label class="control-label"> Your Remarks </label> 
			<textarea type="text" class="form-control" id="e_remarks" value="" ></textarea>
			</p>
		</div>
		</div> 
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="done_edit" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>
<!-- END CONTAINER -->
<script language="javascript">
$(function(){ 
//loadMessages('sent');
var c="<?=$x?>";
$("#total_customers").val(c);
$("#add_new").click(function()
{  
	$("#email").val(''); $("#contact_person").val(''); $("#name").val(''); $("#phone").val(''); $("#remarks").val('');
	$("#add_msg").empty();
	$("#add_customer").modal('show'); 
});
 


 $("#submit").click(function(){    
	var assign_to=$("#assign_to").val();
	var date=$("#Demodate").val(); 
	var id=$("#id").val();
	var remarks=$("#remarks").val(); 
	if(!date){ $("#add_msg").html("<font color='red'> Demo date Required</font>");  $("#name").focus(); return false;}
	if(!assign_to){ $("#add_msg").html("<font color='red'> Person Assigned  is Required</font>");  $("#name").focus(); return false;}
	 
	$("#add_msg").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/add_demo_request",
			type:"POST",
			async:false,
			data:
			{
				'id':id,   
				'demo_date':date,
				'assign_to':assign_to,
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					$("#assign_to").val(''); $("#Demodate").val('');  $("#remarks").val('');
					$("#add_msg").html("<font color='green'> Data saved successfully </font>");
					setTimeout(function(){
					  $("#add_msg").empty();
					  $("#add_new").modal('hide');
					 window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
					$("#add_msg").html("<font color='red'> Record  <b> Not </b> saved. </font>"); 
				} 
			} 
	})
 
 });
 
 $("#done_edit").click(function(){    
	var id=$("#id").val(); 
	var remarks=$("#e_remarks").val(); 
	 if(!remarks){ $("#e_remarks").focus(); return false;}
	  
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/remarksAfterDemo",
			type:"POST",
			async:false,
			data:
			{
				'id':id,    
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					setTimeout(function(){ 
					  $("#edit_remarks").modal('hide');
					 window.location.replace("<?=base_url();?>customers/demo");
					}, 2500);
				}
				else
				{
					alert('Record Not saved '); 
				} 
			} 
	})
 
 });
  
  
  
 });
 
 function edit_remarks(id,c)
{  
    $("#id").val(id);
    $("#e_remarks").val(c);
	$("#edit_remarks").modal('show'); 
}
 function updateStatus(id)
 { 
    $("#id").val(id);
   var v=$("#action").val();
   if(v=="Call Again")
   {
	   
   }else if(v=="No Response")
   {
	   
   }
   else if(v=="Not Interested")
   {
	   
   }
   else
   {
	   $("#add_action").modal("show");
   }
 }
 
 function deleteCustomer(id)
 {
	 var c=confirm("Delete this record?");
	 if(c==true)
	 {
		  $("#err").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/deleteCustomer",
			type:"POST",
			async:false,
			data:
			{
				'id':id 
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					 $("#err").html("<font color='green'> Record removed successfully </font>");
					setTimeout(function(){
					  $("#err").empty(); 
					  window.location.reload();
					 //window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
					alert(" Record   Not   removed. </font>");  
				} 
			} 
	})
	 }
	 else
	 {
		return c;  
	 }
 }
 
 function edit(id,name,phone,contact_person,email,remarks)
 {  
	$("#title").html('<font> Edit Customer Details </font>');
	$("#edit_id").val(id);$("#contact_person").val(contact_person); $("#email").val(email); $("#name").val(name); $("#phone").val(phone); $("#remarks").val(remarks);
	$("#add_customer").modal('show'); 
 } 
 
</script>