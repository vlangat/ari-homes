 <?php $i=0;$converted=0;
      foreach($customer_info->result() as $p){ 
      foreach($users->result() as $u){ if($p->email==$u->email){$converted++; }}
     $i++; } 
        $d=0; foreach($demos->result() as $dem){ $d++; }
        
  ?>
 <!-- BEGIN PAGE CONTENT BODY -->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> Customers </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 <div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-green-sharp">
						<span data-counter="counterup"  data-value="<?=$i;?>"> 0 </span>
						<small class="font-green-sharp"> </small>
					</h3>
					<small> POSSIBLE CUSTOMERS</small>
				</div>
				<div class="icon">
					<i class="fa fa-users"> </i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
						<span class="sr-only">76% progress</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> progress </div>
					<div class="status-number"> 76% </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-red-haze">
						<span data-counter="counterup" data-value="<?php echo $i;?>">0</span>
					</h3>
					<small> TOTAL CALLS </small>
				</div>
				<div class="icon">
					<i class="icon-home"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
						<span class="sr-only">85% change</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> change </div>
					<div class="status-number"> 85% </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-blue-sharp">
						<span data-counter="counterup" data-value="<?php echo $d;?>"> 0 </span>
					</h3>
					<small> DEMO  </small>
				</div>
				<div class="icon">
					<i class="icon-users"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 45%;" class="progress-bar progress-bar-success blue-sharp">
						<span class="sr-only">45% grow</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> grow </div>
					<div class="status-number"> 45% </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 ">
			<div class="display">
				<div class="number">
					<h3 class="font-purple-soft">
						<span data-counter="counterup" data-value="<?php echo $converted;?>"> 0</span>
					</h3>
					<small>CONVERTED</small>
				</div>
				<div class="icon">
					<i class="icon-user"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: 57%;" class="progress-bar progress-bar-success purple-soft">
						<span class="sr-only">56% change</span>
					</span>
				</div>
				<div class="status">
					<div class="status-title"> change </div>
					<div class="status-number"> 57% </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar">
 
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th> 
							<th> Company </th>
							<th> Contact Person</th> 
							<th> Email</th> 
							<th> Phone No</th>  
							<th> Date Added </th>							
							<!--<th> Remarks </th> -->
							<th> Status </th> 
							<th> Edit </th> 
							<th> Delete </th> 
						</tr>
					</thead>
					<tbody>
						 <?php $x=1;
						 foreach($customer_info->result() as $p){  $total=0;?>
						<tr>
							<td><?=$x?> </td> 
							<td><?=$p->company?></td>
							<td> <?=$p->contact_person?> </td> 
							<td><?=$p->email?> </td>
							<td><?=$p->phone?> </td>  
							<td> <?=$p->date_added?> </td>
							<!--<td> <?php
							//=$p->remarks
							?> </td>-->
							<td> <?=$p->status?> </td>
							<td>
								<a   href="javascript:;" onclick="edit('<?=$p->id?>','<?=$p->company?>','<?=$p->phone?>','<?=$p->contact_person?>','<?=$p->email?>','<?=$p->remarks?>')"> <i class="fa fa-edit"> </i>  Edit </a>
							</td>
							<td>
								<a   href="javascript:;" onclick="deleteCustomer('<?=$p->id?>')"> <i class="fa fa-trash"> </i>  Delete </a>
							</td> 
						</tr>
						 <?php $x++; //$total=$total+$amount;
						 }
						 ?>  
						 
					</tbody>
				</table>
				<button class='btn green' id="add_new">
			<i class='fa fa-plus'> </i> Add New </button> 
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div> 
    
<div id="add_customer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" id="title">Add Possible Customers</h4>
			</div>
			<div class="modal-body">
	<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
	<div class="row">
	<div class="col-md-12">  
				 <p>
			<label class="control-label">Company </label>
				<input   class="form-control" type="text" id="name"  name="name" onchange="checkChars('name')" autocomplete="on" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"> 
				<input   class="form-control" type="hidden" value="" id="edit_id"> </p>
				 
			<p>
			<label class="control-label">Contact Person</label>
				<input   class="form-control" type="text" onchange="checkChars('contact_person')" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);"  id="contact_person"  name="contact_person"> </p>
			  
			 <p>
			<label class="control-label">Phone No</label>
				<input   class="form-control" type="text" id="phone" onPaste="var e=this; setTimeout(function(){ e.value='';}, 1);" onkeypress="return checkIt(event)" maxlength="10" name="phone"> </p>
			 <p>
			<label class="control-label">Email</label>
				<input   class="form-control" type="email" id="email"   name="email"> </p>
			 
			 <p>
			 <label class="control-label"> Email to Send </label>
			<select  class="form-control" id="email_type" name="email_type"> 
				<option value="email_1">Online Real Estate Management Solution </option>
				<!--<option value="email_2">Email 1</option>
				<option value="email_3">Email 1</option>
				<option value="email_4">Email 1</option>-->
			</select>
			<input type="hidden" class="form-control" id="remarks" value="" name="remarks" ></textarea>
			</p>
		</div>
		</div> 
		<span id="add_msg">   </span>
		</div>
		<div class="modal-footer">
				<input type="submit" class="btn green"  id="submit" value="Save"> 
				<button type="button" data-dismiss="modal" class="btn btn-outline dark"> Cancel </button>
		</div> 
		</div>
		</div>
	</div>
</div>
        <!-- END CONTAINER -->
<script language="javascript">
$(function(){ 
//loadMessages('sent');
var c="<?=$x?>";
$("#total_customers").val(c);
$("#add_new").click(function()
{  
	$("#email").val(''); $("#contact_person").val(''); $("#name").val(''); $("#phone").val(''); $("#remarks").val('');
	$("#add_msg").empty();
	$("#add_customer").modal('show'); 
});
 
 
 $("#submit").click(function(){   
	var contact_person=$("#contact_person").val();
	var amount=$("#amount").val();
	var name=$("#name").val();
	var phone=$("#phone").val(); 
	var id=$("#edit_id").val() 
	var email=$("#email").val() 
	var remarks=$("#remarks").val();
	var email_type=$("#email_type").val();
	if(!name){ $("#add_msg").html("<font color='red'> Company Name  is Required</font>");  $("#name").focus(); return false;}
	if(!contact_person){ $("#add_msg").html("<font color='red'>Contact Person is Required</font>");  $("#contact_person").focus(); return false;}
	if(!email){ $("#add_msg").html("<font color='red'>Email is Required</font>"); $("#email").focus(); return false;}
	if(! email){ }else
	 {
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { alert("Email provided is invalid");   $("#email").focus(); return false;}
	 }
	 if(!phone){ $("#add_msg").html("<font color='red'>Mobile Number is Required</font>"); $("#phone").focus(); return false;}
	
	$("#add_msg").html("<font color='#006699'> Please waiting...</font>"); 
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/add_customer",
			type:"POST",
			async:false,
			data:
			{
				'edit_id':id,  
				'contact_person':contact_person, 
				'company':name,
				'phone':phone,
				'email':email,
				'email_type':email_type,
				'remarks':remarks
			},
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{
					if(id){ }else{
						$("#code").val(''); $("#amount").val(''); $("#name").val(''); $("#phone").val(''); $("#remarks").val('');
					}
					$("#add_msg").html("<font color='green'> Data saved successfully </font>");
						setTimeout(function(){
						  $("#add_msg").empty();
						  $("#add_new").modal('hide');
						 window.location.replace("<?=base_url();?>customers/");
                      }, 2500);
				}
				else
				{
					$("#add_msg").html("<font color='red'> Customer details <b> Not </b> added. Check if customer with the same details exists </font>"); 
				} 
			} 
	})
 
 });
  
  
  
 });
 
 function deleteCustomer(id)
 {
	 var c=confirm("Delete this customer?");
	 if(c==true)
	 {
		  $("#err").html("<font color='#006699'> Please waiting...</font>"); 
		  
	$.ajax(
	{ 
			url:"<?=base_url();?>customers/deleteCustomer/"+id,
			type:"POST",
			async:false, 
			success:function(data)
			{
				var obj=JSON.parse(data);
				if(obj.result=="ok")
				{ 
					 $("#err").html("<font color='green'> Record removed successfully </font>");
					setTimeout(function(){
					  $("#err").empty(); 
					  window.location.reload();
					 //window.location.replace("<?=base_url();?>customers/calls");
					}, 2500);
				}
				else
				{
					alert(" Record Not removed. </font>"); 
				} 
			} 
	})
	 }
	 else
	 {
		return c;  
	 }
 }
 
 function edit(id,name,phone,contact_person,email,remarks)
 {  
	$("#title").html('<font> Edit Customer Details </font>');
	$("#edit_id").val(id);$("#contact_person").val(contact_person); $("#email").val(email); $("#name").val(name); $("#phone").val(phone); $("#remarks").val(remarks);
	$("#add_customer").modal('show'); 
 } 
 
	$(function (){
		$('#name').autocomplete({
			source: '<?=base_url();?>customers/getCustomers'
		});
	});
 
</script>