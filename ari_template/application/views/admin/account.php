 <?php foreach($data->result() as $row) {} ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
	<!-- BEGIN PAGE BREADCRUMBS -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="index.html">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="#"> Account</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Profile</span>
		</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
 <div class="row">
<div class="col-md-12">
<!-- END BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
<div class="col-md-12">
<div class="portlet light ">
<div class="portlet-title tabbable-line">
<div class="caption caption-md">
	<i class="icon-globe theme-font hide"></i>
	<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
</div>
<ul class="nav nav-tabs">
	<li class="active">
		<a href="#tab_1_1" data-toggle="tab" onclick="clearErrors()">Personal Info</a>
	</li>
	<li>
		<a href="#tab_1_2" data-toggle="tab" onclick="clearErrors()">Change Pic</a>
	</li>
	<li>
		<a href="#tab_1_3" data-toggle="tab" onclick="clearErrors()">Change Password</a>
	</li>
	<!-- <li>
	<a href="#tab_1_4" data-toggle="tab" onclick="clearErrors()"> Users</a>
	</li>-->
</ul>
</div>
<div class="portlet-body">
<div class="tab-content">
	<!-- PERSONAL INFO TAB -->
	<div class="tab-pane active" id="tab_1_1">
		<form role="form" action="#">
			<div class="form-group">
				<label class="control-label">First Name</label>
				<input type="text" id="first_name" value="<?php echo $this->session->userdata('first_name');?>" class="form-control" /> </div>
			<div class="form-group">
				<label class="control-label">Last Name</label>
				<input type="text" id="last_name" value="<?php echo $this->session->userdata('last_name');?>" class="form-control" /> </div>
			<div class="form-group">
				<label class="control-label"> Mobile Number </label>
				<input type="text"  id="phone" value="<?php echo $this->session->userdata('phone');?>" class="form-control" /> </div>
			<div class="form-group">
				<label class="control-label">Company</label>
				<input type="text" id="company" value="Ari Limited" class="form-control" readonly > </div>
			 
			 <div class="margiv-top-10">
				<a href="javascript:;" id="update_details" class="btn green"> Save Changes </a>
				<a href="javascript:;" class="btn default"> Cancel </a>
			</div>
		</form>
</div> 
	<!-- END PERSONAL INFO TAB -->
	<!-- CHANGE AVATAR TAB -->
	<div class="tab-pane" id="tab_1_2"> 
		<!--<form action="changePhoto" role="form" enctype="form-multipart" >-->
		<?php echo form_open_multipart('admin/changePhoto');?>
			<div class="form-group">
				<div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						<img src="<?=base_url();?>media/<?php echo $this->session->userdata('photo');?>" alt="" /> </div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
					<div>
						<span class="btn default btn-file">
							<span class="fileinput-new"> Select image </span>
							<span class="fileinput-exists"> Change </span>
							<input type="file" name="userfile[]"  accept="gif|jpg|png"> </span>
						<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
					</div>
				</div>
				<div class="clearfix margin-top-10">
					 </div>
			</div>
			<div class="margin-top-10">
				<button href="javascript:;" type="submit" class="btn green"> Save changes </button>
				<button href="javascript:;" type="reset" class="btn red"> Cancel </button>
				 
			</div>
		<!--</form>-->
		<?php echo form_close();?>
</div>
<!-- END CHANGE AVATAR TAB -->
<!-- CHANGE PASSWORD TAB -->
<div class="tab-pane" id="tab_1_3">
	<form action="#">
		<div class="form-group">
			<label class="control-label">Current Password</label>
			<input type="password" id="curr_password" class="form-control" /> </div>
		<div class="form-group">
			<label class="control-label">New Password</label>
			<input type="password" id="new_pass" class="form-control" /> </div>
		<div class="form-group">
			<label class="control-label">Re-type New Password</label>
			<input type="password" id="confirm_pass" class="form-control" /> </div>
		<div class="margin-top-10">
			<a href="javascript:;" class="btn green" id="change_pass"> Change Password </a>
			<a href="javascript:;" class="btn default"> Cancel </a>
		</div>
	</form>
</div>
<!-- END CHANGE PASSWORD TAB -->
<div class="tab-pane" id="tab_1_4">
										   <!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet light portlet-fit ">
					
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="btn-group">
									   
										<a data-toggle="modal" href="#responsive_2" class="btn green"> Add New User
											<i class="fa fa-plus"></i>
											</a>
									</div>
								</div>
							</div>
						</div>
						<!--<table class="table table-striped table-hover table-bordered" id="sample_editable_1">-->
				
						<table class="table table-striped table-hover table-bordered"  >
							<thead>
								<tr>
									<th> First Name </th>
									<th> Last Name </th>
									<th> Email </th>
									<th> Last Login </th>
									<th> Status </th>
								</tr>
							</thead>
								<tbody  id="agents"> 
								
								</tbody>
						</table>
					</div>
				</div>
			</div>
			
			
</div>
</div> <p id="error">   </p>
</div>   
       <p id="dismissable"> <?php if($this->session->flashdata('temp')){
				$msg=$this->session->flashdata('temp');
				echo '<div class = "alert alert-success alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button> <font color="green">'. $msg. '</font> </div>'; 	
				} 	
		?>
		</p>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>  
</div>  
</div>    
     
 
<!-- END CONTENT -->
 
<!-- END CONTAINER -->
<div id="responsive_2"   class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"> 
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" id="task">Add User </font></h4>
	</div>
<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div  class="form-horizontal">
				<div class="form-group">
					<div class="col-md-12">
					First Name:
					<input type="text" id="fname"class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off"required="required" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					Second Name:
					<input type="text" id="lname"class="form-control todo-taskbody-tasktitle"required="required"autocomplete="off"required="required" />
					</div>
				</div>
			<div class="form-group">
			<div class="col-md-12">
				Email:
				<input type="email" id="email"class="form-control todo-taskbody-tasktitle" required="required" autocomplete="off"required="required" placeholder="Email"/>
			</div>
	</div>
		<div class="form-group">
			<div class="col-md-12">    
				</div>
			<input type="hidden" id="added_by"class="form-control todo-taskbody-tasktitle"value="<?php echo $this->session->userdata('id');?>" placeholder="">
		</div>
	</div>
	</div>
</div>
</div><font id="add_user"> </font>
<div class="modal-footer"> 
<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
<input type="submit" id="add_new_user" class="btn green"value="Save Changes">
</div>   
</div>
</div> 
</div> 
</div> 
  
<!-- responsive -->
<div id="del_user" class="modal fade" tabindex="-1" data-width="400">
      <div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5><b>  Confirm Action </b></h5>
				<p id="del_status"> Are you sure you want to perform this Action? </p>
				
			</div>
			</div>
				</div>
					<div class="modal-footer" >
					<input type="hidden" id="status">
					<input type="hidden" id="agent_id">
					<button type="button" class="btn green" id="confirm"> Yes </button>
				<button type="button" data-dismiss="modal"  class="btn red"> No </button>
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
		</div>
	</div>
</div>
</div>

</body> 
</html>
<script type="text/javascript">
$(function()
{ 
	getAgents();
	$("#change_pass").click(function()
	{  
	 
		var curr_pass=$("#curr_password").val();
		var new_pass=$("#new_pass").val();
		var confirm_pass=$("#confirm_pass").val();  	
	   if(curr_pass||new_pass||confirm_pass){ } else{$("#error").html("<font color='red'> Empty fields not required </font>"); return false;}
		if(new_pass == confirm_pass){ }else{  $("#error").empty();$("#error").html("<font color='red'> Password does not match. Try again </font>");  $("#pass2").val('');$("#pass2").focus(); return false;}
		 
		  $.ajax({
		 url:"<?=base_url();?>admin/updatePassword/",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'curr_password':curr_pass,
		   'new_pass':new_pass 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){
				 $("#confirm_pass").val('');$("#new_pass").val(''); $("#curr_password").val('');
				 $("#error").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'> Password changed successfully  </font>"); 
                	 
			 }
			 else{
				 $("#error").html("<font color='red'>Password not changed. Check if current password is correct</font>");return false;
			 }
		 }
		 
		  }
		 )	  
});

$("#update_details").click(function()
	{  
		var fname=$("#first_name").val();
		var lname=$("#last_name").val();
		var company=$("#company").val();  	  	
		var phone=$("#phone").val();    	
		if(fname&&phone&&lname){ } else{$("#error").html("<font color='red'> Empty fields not required </font>"); return false;}
		$("#error").html("<font color='blue'> Saving please wait...</font>");
		$.ajax({
		 url:"<?=base_url();?>admin/updateProfile",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'lname':lname, 
		   'phone':phone
		   //'company':company, 
		   //'role':role 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){ 
			     $("#phone").val(phone);
			     $("#first_name").val(fname);
			     $("#last_name").val(lname); 
				 $("#error").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>Profile details updated successfully  </font>"); 
                getAgents();	 
			 }
		else{
				$("#error").html("<font color='red'> Error occured. Check if details provided are correct </font>");return false;
			}
		 }
		 
		  }
		 )	  
});

$("#add_new_user").click(function()
	{  
		var email=$("#email").val();
		var fname=$("#fname").val();
		var lname=$("#lname").val();  	  	
		if(email&&fname&&lname){ } else{$("#add_user").html("<font color='red'> Empty fields not required </font>"); return false;}
		var atpos = email.indexOf("@");
       var dotpos = email.lastIndexOf(".");
       if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#add_user").html("<font color='red'> Email provided is not a valid e-mail address"); return false;}
		$("#add_user").html("<font color='blue'> Adding new user....</font>");
		$.ajax({
		 url:"<?=base_url();?>admin/addUser",
		 type:"POST",
		 async:false,
		 data:
		 {
		   'fname':fname,
		   'lname':lname, 
		   'email':email 
		 },
		 success:function(data)
		 {
			 var obj=JSON.parse(data);
			 if(obj.result=="ok"){
				 $("#fname").val('');$("#lname").val(''); $("#email").val('');
				  
				 $("#add_user").html(" <i class='glyphicon glyphicon-ok' style='font-size:20px;color:green;'>  </i> <font color='green'>"+obj.msg+"  </font>"); 
                getAgents();	 
			 }
			 else{
				 $("#add_user").html("<font color='red'> "+obj.msg+"</font>");return false;
			 }
		 }
		 
		  }
		 )	  
});

$("#confirm").click(function(){ 
	var par1=$("#agent_id").val();
	var par2=$("#status").val();  
	$("#del_status").html("<font color='blue'>  Please wait....</font>");
	 $.ajax({
		url:"<?=base_url();?>admin/removeUser/"+par1+"/"+par2,
		type:"POST",
		async:false ,
		success:function(data)
		{ 
			var obj = JSON.parse(data); 
			if (obj.result=="ok")
			{
			   getAgents();
			   if(par2==1){ msg="de-activated";}else{msg="activated";}
			   $("#del_status").html(" <font color='green'> Agent "+msg+" successfully</font> ");
				setTimeout(function(){
					$('#del_status').empty()
					$('#del_user').modal('hide')
				}, 2000);
				return false;			   
			}
			else{
				$("#del_status").html(" <font color='red'>  Changes not made. Try again later </font> "); 
				return false;
			}
			
		}
		})
	
});

});

function getAgents() 
{
	var i=0;
	$.ajax({
		url:"<?=base_url();?>admin/loadUsers",
		type:"POST", 
		success:function(data)
		{
			var obj=JSON.parse(data);
			if(obj.result=="ok")
			{
				
				 var content="";
				 $("#agents").empty();
				 var data=obj.data;
				 for(i=0; i<data.length;i++)
				 {
					var status="";
					var agent=data[i]; 
					 if(agent['user_enabled']==1){ status="<font color='green' title='Click to Deactivate'>Active</font>";} else{ status="<font color='red' title='Click to activate'> Deactivated </font>";}
					content=content+"<tr><td> "+agent['first_name']+"</td> <td> "+agent['last_name']+"</td><td> "+agent['email']+"</td> <td> "+agent['last_login']+"</td> <td><a   onclick=\"remove_user("+agent['id']+","+agent['user_enabled']+")\"> " +status+" </a></td></tr>";
					
				 }
				 $("#agents").html(content); 
			}
		}
		
		
	})
	
}

function remove_user(id,status)
{ 

		$("#agent_id").val(id);
		$("#status").val(status);
		$("#del_user").modal('toggle');
		 
}

function clearErrors()
{
	$("#error").empty();
	$("#dismissable").empty();
	
}
</script>