<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="index.html">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Users </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   Log Activity </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
 
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="page-content-inner">
<div class="row">
	
	<div class="col-md-12">
	<div class="col-md-12" style="background:#006699;padding:6px;">
				<font color="#ffffff"> Audit Trail </font>
		</div>
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet-body">
				<div class="table-toolbar" style="min-height:400px">
  
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
					<thead>
						<tr>
							<th> #  </th>      
							<th> Date Created </th> 
							<th> User Name  </th> 
							<th> Email </th> 
							<th> Message </th> 
							<th> Status </th>  
						</tr>
					</thead>
					<tbody>
						 <?php $x=1; foreach($log_activity->result() as $row){ ?>
							  	 
						<tr>
							<td> <?=$x?> </td>
							 
							<td><?=$row->create_date?>  </td>
							<td> 
							<?php 
							foreach($user_info->result() as $r){
								if($row->user_id==$r->email)
								{ 
								   echo ucfirst(strtolower($r->first_name)).' '.ucfirst(strtolower($r->middle_name)).' '.ucfirst(strtolower($r->last_name));
									
								} 
							}
							?>  
							</td>
							<td> 
								<?=$row->user_id?>
							</td>
							<td> <?=$row->message?> </td>
							<td> <?=$row->status?> </td>
							 <!--<td>  
								<a href="javascript:;"  onclick="delete_rec('<?=$row->id?>')" > <i class="fa fa-trash"> </i> Remove </a>
							</td> --> 
						</tr>
						 <?php $x++; 
						 
						 }?>  
						 
					</tbody>
				</table>
				 
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
</div>
</div>
 
</div>
</div>
 

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
	<i class="icon-login"></i>
</a>

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

<script language="javascript">
function validate()
{	
var formobj = document.forms[0];
var counter = 0;
for (var j = 0; j < formobj.elements.length; j++)
{
    if (formobj.elements[j].type == "checkbox")
    {
        if (formobj.elements[j].checked)
        {
            counter++;
        }
    }       
}
if(counter==0){  
    alert("Please select at least one.");
	return false;
 }
 

	/*var chks = document.getElementsByName('checkbox[]');
	var hasChecked = false;
	for (var i = 0; i < chks.length; i++)
	{
		if (chks[i].checked)
		{
		hasChecked = true;
		break;
	}
	}
	if (hasChecked == false)
	{
	alert("Please select at least one.");
	return false;
	}*/
	 var x=confirm("Are you you to remove this  audit trail?");
	if(x==true){  
	  	 	
	return true;
	}
	else{
		return false;
	}
}

 
function delete_rec(id)
	{ 
	  
	var x=confirm("Are you you to remove this  audit trail?");
	if(x==true){  
	  	 	
	    //$("#error_message").html(" <font color='#006699'> Saving changes... </font>");
		$.ajax({
		 url:"<?=base_url();?>admin/remove_sessions",
		 type:"POST",
		 async:false,
		 data:{'id':id},
		 success:function(data)
		 {  
			var obj=JSON.parse(data);
			 if(obj.result=="ok"){  
				 setTimeout(function(){
				location.reload();
			 },2000);			  
			 }
			 else
			 {
				//$("#error_message").empty();  
				alert(" Audit Trail not removed! Try again later");
				return false;
			 }
		 }
		  }
		 )	
	}
else{ return false;}	
	}
</script>