 <?php 
 $first_name=""; $house_no=""; $email=""; $property_name=""; $floor_no=""; $lease_period="";$category_name=""; $tenant_name=""; $phone=""; $property_unit_id="";
 if($statement==""){ }else{
 foreach($statement->result() as $rows)
 { 
	 $tenant_type=$rows->tenant_type; $property_unit_id=$rows->property_unit_id;
	 if($tenant_type=="residential"){ $tenant_name=$rows->last_name." ".$rows->middle_name." ".$first_name=$rows->first_name;}else{ $tenant_name=$rows->company_name;} 
	 $house_no=$rows->house_no; $floor_no=$rows->floor_no; $lease_period=$rows->lease_period; $phone=$rows->mobile_number;  
     $email=$rows->tenant_email;
}
 }
foreach($units_details->result() as $u)
{ 
	 if($property_unit_id==$u->unit_id)
	 { 
			 $property_name=$u->property_name;$category_name=$u->category_name;
			 
	 }
}
if($first_name==""){ $first_name=$tenant_name;}
$name=""; $company_phone_number="";
 foreach($users->result() as $r)
 { 
	 if($r->id==$this->session->userdata('id'))
	 {
	 $phone=$r->mobile_number;
	 $name=ucfirst(strtolower($r->first_name))." ".ucfirst(strtolower($r->last_name)); 
	 if($name !=""){

					}
					else
					{
						foreach($company_info->result() as $c)
						{
							 $name=$c->company_name; $company_phone_number=$c->mobile_number;
						} 
					} 
	 }
}  
if($tenant_name=="")
{
	$tenant_name=$name;
}
if($email=="")
{
	$email=$this->session->userdata('email');
}
?>

 <div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>tenantSignUp/statement/home"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>  Rent </span>
	<i class="fa fa-circle"> </i>
</li>
<li>
	<span> Payment Statement</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  <div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="color:#ffffff;background:#32c5d2;height:150px">
			<div class="display">
				<div class="number">
				<h4>  <?=$name=ucfirst(strtolower($this->session->userdata('first_name'))).' '.ucfirst(strtolower($this->session->userdata('middle_name'))).' '  
				.ucfirst(strtolower($this->session->userdata('last_name')));?> </h4>	 
				<span> <?=$email?> </span>
				<h6> 
					<?php echo $this->session->userdata('phone');
					//if($phone==""||$phone=="0"){ $phone=$company_phone_number;}  echo $phone;
					?> 
				</h6>
				<h5> <?php //$property_name
				?>  </h5>
				</div> 
			</div>
			 
		</div>
	</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:150px">
			<div class="display">
				<div class="number">
				<!---	<h3 class="font-red-haze">	-->
					<h4 class="font-red-haze">
						<span data-counter="counterup" data-value="1349"> <?=$house_no?> </span>
					</h4>
					<small>House Number</small>
				</div>
				<div class="icon">
					<i class="icon-home"> </i>
				</div>
			</div>
			
			<div class="progress-info">
				<div class="progress">
					<span style="width: 100%;" class=""> 
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Tenant for  </div>
					<div class="status-number"> <?=$lease_period?> years </div>
				</div>
			</div>
		</div>
</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:150px">
			<div class="display">
				<div class="number">
					<!--<h4 class="font-blue-sharp">-->
					 <h4 class="font-blue-sharp"> 
						<span data-counter="counterup" data-value="567"> <?=$category_name?> </span>
					</h4>
					<small>   Category </small>
				</div>
				<div class="icon">
					<i class="icon-layers"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width:100%" class="">
					 
					</span>
				</div>
				<div class="status">
					<div class="status-title"> Floor </div>
					<div class="status-number"> <?=$floor_no?> </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2 " style="height:150px">
			<div class="display">
				<div class="number">
					<h4 class="font-purple-soft">
						<span data-counter="counterup" data-value="276">  
	<?php
			$rent_paid=0; $rent_to_pay=0; 
				//foreach($rent->result() as $r){
				//if($r->tenant_id==$rows->id){ $rent_to_pay=$rent_to_pay+$r->payment_type_value;}
				// }
				
			?>  
			<?php 
				//foreach($paid_rent->result() as $pr){
			 	//if($pr->tenant_id==$rows->id){ $rent_paid=$rent_paid+$pr->amount;}
				
				// }
			$rent=0;
			  foreach($rent_due->result() as $rt){
			 	 $rent=$rt->balance;
				
			 }
			  //($rent_to_pay-$rent_paid);
						
						?> 
						<font id="rent_due">0</font>
					</span>
				</h4>
				<small>  Rent Due </small>
			</div>
			<div class="icon">
				<i class="fa fa-money"></i>
			</div>
		</div>
		<div class="progress-info">
			<div class="progress">
				<span style="width:100%;" class="">
					 
				</span>
			</div>
			<div class="status">
				<div class="status-title"> Days delayed </div>
				<div class="status-number"> 0 days </div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title">
		 
<form action="<?=base_url();?>tenantSignUp/statement/<?=$id?>" method="post" onsubmit="return validate()">	 
<div class="row">
	<div class="col-md-4">  
		<div class="form-group">    
				<label class="control-label col-md-4">From </label>
				 <input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value="<?=date("m/d/Y")?>"  placeholder=""  name="from" />
			
					<!-- /input-group -->
				</div> 
			<label id="error3">					</label>  
	</div> 
	<div class="col-md-4">  
		<div class="form-group">    
				<label class="control-label col-md-4">To </label>
				 <input type="text" class="form-control  input-xxlarge date-picker"  data-date-format="dd/mm/yyyy" value="<?=date("m/d/Y")?>"  placeholder=""  name="to" />
		</div> 
			<label id="error3">					</label>  
	</div> 	 		 
	<div class="col-md-4"> 
	<label class="control-label col-md-12"> &nbsp;&nbsp; </label>
		<div class="form-group">   
				 <button type="submit" class="btn green"> Search</button>
		</div>
	</div> 
</div> 
</form>
  
<hr/>
 <!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
	
<div id="prnt">
<table    class="table" style="font-size:12px" width="100%" >
<thead>
	<tr> 
		<th align="left"> Date </th> 
		<th align="left"> Receipt No </th>  
		<th align="left"> Payment Mode </th>  
		<th align="left"> Mode No </th>  
		<th align="left"> Description </th>  
		<th align="left"> Debit </th>  
		<th align="left"> Credit </th>  
		<th align="left"> Balance </th> 
	</tr>
</thead>
<tbody> 
<?php 
		$credit=0; $debit=0; $curr_bal=0; $total=0;
		if($statement==""){}
		else{
			foreach($statement->result() as $row){
		if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
		if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
		 	   
	?>

	<tr>
		  <td> <?=$row->date_paid?>    </td>  <td> <?=$row->receipt_no?> </td> <td> <?=$row->payment_mode?> </td> <td> <?=$row->payment_mode_code?> </td> <td> <?=$row->description?>  </td> <td> <?php if($row->type=="d"){echo $row->amount;}else{ echo '-';}?> </td>  <td> <?php if($row->type=="c"){echo $row->amount;}else{ echo '-';}?> </td>  <td> <?php   if(($debit-$credit)<0){echo 0-$curr_bal;}else { echo $curr_bal; }?> </td> 
	</tr>
	
		<?php   }}?>	
</tbody>
</table>
<hr/>
<center> <p style="font-size:12px"> <b>  Closing Balance  </b>  Ksh: <?=$closing_bal=$debit-$credit; ?> </p> </center>
</div>

 <a  href="javascript:;" class="btn green"  id="btn_receipt">  <i class="fa fa-print" style="font-size:18px;"></i><b>  Print </b> &nbsp;</a> 
 
		 
</div> 
<!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>
</div> 

<div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> Venit Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
  
<!-- END CONTAINER -->

 <div id="sendMail" class="modal fade" tabindex="-1" data-width="600">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Email Tenant Message </b></h5>
				<hr/>
					<p>
					    This Statement shall be sent as an Email to <font id="email_id"> </font>
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="send_email" class="btn green">&nbsp; Yes &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn red">&nbsp; No  &nbsp; </button> 
			<button type="button" data-dismiss="modal" class="btn default">&nbsp; Close  &nbsp; </button> 
		</center>
	</div>
</div>


<div id="responsive_2" class="modal fade" tabindex="-1" aria-hidden="true">
<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" id="task">Provide your National ID or Passport for us to match you with your agent/landlord </font></h4>
	</div>
<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
			<div class="col-md-12">
			 <p>	
			 <label> National Id/Passport </label> 
				<input   class="form-control"  value="" type="number" id="national_id"/>
			</p>
			 	 
			</div>
		</div>
</div><font id="error1">     </font>
<div class="modal-footer"> 
<input type="submit" id="save_changes" class="btn green"value="Match">
<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
</div>   
</div>
</div> 
  
<div id="referAgent" class="modal fade" tabindex="-1" aria-hidden="true">
<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"> <font size="3" id="task"> Oops! </font></h4>
</div>
<div class="modal-body">
			<div class="scroller" style="height:100%" data-always-visible="1" data-rail-visible1="1">
			<div class="row">
			<div class="col-md-12">
			<p> Your agent/landlord is not currently using ARI. Refer him/her for better services  </p>
			 <p>	
				<label class="control-label">Agent Name </label> 
				<input   class="form-control" type="text"   required id="agent_name" name="agent_name" value=""> 
			</p>
			<p>
				<label class="control-label">Email </label>
				<input   class="form-control" type="text"  id="email" name="email" value="">
			</p>			
			<p>
				<label class="control-label">Phone </label>
				<input   class="form-control" type="number"  id="phone" name="phone" value="">
			</p>			
			<p> 
				 
			</p> 
			</div>
		</div>
</div><font id="error2">     </font>
<div class="modal-footer"> 
<input type="submit"   id="save"  class="btn green"value="Send">
<button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
</div>   
</div>
</div> 
  
<div id="data_saving_success" class="modal fade" tabindex="-1" data-width="400">
	 <div class="modal-header">
 <b style="font-size:18px;color:green">Success Message </b> 
		</div>
	 <div class="modal-body">
				<div class="row">
				 <div class="col-md-12">
				  <p> Your Agent is using ARI and you are listed under : </p>
					<ul id="content">
					  
					</ul>
				 </div>
				</div>
			</div>   
	<div class="modal-footer" >  
		<center> <button type="button" data-dismiss="modal" class="btn btn-outline dark" >OK</button> </center>
		 
	</div> 
</div>
 
 
<script language="javascript">
 
 $("#btn_receipt").click( function () {
	var divContents = $("#prnt").html();
	var printWindow = window.open('', '', 'height=400,width=800');
	printWindow.document.write('<html><head><title>  Statement </title>');
	printWindow.document.write('</head><body >');
	printWindow.document.write(divContents);
	printWindow.document.write('</body></html>');
	printWindow.document.close();
	printWindow.print();
        });
var national_id="<?=$national_id?>"; 
$(function ()
{     
    var bal="<?php echo $closing_bal;?>";
    $("#rent_due").html(bal);
	if(national_id==""){ $("#responsive_2").modal('show'); }
});
		
        var email="<?=$email?>";  
        var fname="<?=$first_name?>";  
        var from="<?=$name?>"; 
      
		$("#btn_email").click( function ()
		{ 
		$("#email_id").html(fname+" <u> "+email+" </u>");
			$("#sendMail").modal('show');
		});
		
		$("#send_email").click( function () {  
		var divContents = $("#prnt").html();   
		$.ajax({
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Rent Statement", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{  
					$("#error").html("<font color='green'> Rent Statement sent   successfully</font>");
					//$("#success").modal('show');
				}
				else
				{  
					$("#error").html("<font color='red'> Statement   not sent to   <u>"+email+"</u></font>");
					//$("#success").modal('show');
				}
				
			}
			
 })
 
        });
		
	
$("#save_changes").click(function(){     
	var  id= $("#national_id").val();  
	if(!id){ $("#error1").html("<font color='red'>  National ID required</font>"); $("#national_id").focus(); return false;}
	if(id.length <8){ $("#error1").html("<font color='red'>  National ID Must be more than 7 characters</font>"); $("#national_id").focus(); return false;}
 $("#error1").html("<font color='blue'> Matching...</font>");
 $.ajax(
  {
		url:"<?=base_url();?>tenantSignUp/getId",
		type:"POST",  
		data:
		{
			'id':id 
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				$("#national_id").val(''); 
				$("#responsive_2").modal('hide');
				checkAgent(id);  
			 }
			 else
			 {
				 $("#responsive_2").modal('hide');
				 checkAgent(id);  
			 }
		}
  })
 });
 
 function checkAgent(id)
  { 
	$.ajax({
		url:"<?=base_url();?>tenantSignUp/getAgent/"+id,
		type:"POST", 
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {   
				var data=obj.data;
				var content="";
				for(var i=0; i<=data.length; i++)
				{
					var p=data[i];  
					var property_name=p['property_name'];   
					content="<li><h4>"+property_name+"</h4></li>";
					$("#content").append(content);  
					$("#responsive_2").modal('hide');   	 
					$("#data_saving_success").modal('show');
            setTimeout(function()
				{ 
					window.location.reload(); 
				}, 2000); 					
				}  
 				
			 }
			 else
			 {
				 $("#referAgent").modal('show');
				 
			 }
		}
  })
}

 $("#save").click(function(){     
	var email=$("input[name='email']").val();  
	var phone=$("input[name='phone']").val();
	var name=$("input[name='agent_name']").val(); 
	if(!name){ $("#error2").html("<font color='red'>  Enter agent name </font>"); $("#agent_name").focus(); return false;}
	if(!email){ }
	else{ 
			var atpos = email.indexOf("@");
			var dotpos = email.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) { $("#error").html("<font color='red'> Email provided is not a valid e-mail address"); $("#email").focus(); return false;}
		}
	if(!phone){ $("#error2").html("<font color='red'>  Enter phone number </font>");
	$("#phone").focus(); return false;}
	$("#error2").html("<font color='green'>Saving...please wait</font> ");
	$.ajax(
	  {
		url:"<?=base_url();?>Auth/referAgent",
		type:"POST",  
		data:{
			'phone':phone,
			'email':email,
			'name':name 
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				$("#phone").val(''); $("#email").val(''); $("#agent_name").val('');
				$("#error2").html("<font color='green'>   Thank you for referring "+name+" to ARI. We shall contact and sign him up for the service </font>");
				setTimeout(function()
				{ 
					window.location.reload(); 
				}, 2000);  
			 }
			 else
			 {
				$("#error2").html("<font color='red'>   It seems your Agent/Landlord already exists </font>");
				//window.location="<?=base_url();?>auth/tenant";				
			 }
		}
  })
 });
 
 
</script>
