<!DOCTYPE html> 
<html lang="en"> 
<head profile="http://www.w3.org/2005/10/profile">
<link rel="icon" 
      type="image/png" 
      href="<?=base_url();?>images/favicon.png">
<meta charset="utf-8" />
<title>Ari Limited | Property Management</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

<link href="<?=base_url();?>template/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	   
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
 <link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/css/dropzone.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?=base_url();?>multifiles/prism.css">
  <link href="<?=base_url();?>template/theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />

<!-- END THEME GLOBAL STYLES -->
 <!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url();?>template/theme/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>css/mycss.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
 <link href="<?=base_url();?>template/theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url();?>template/theme/assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>template/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END THEME LAYOUT STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>template/theme/assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS --> 

        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>template/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
 <!-- HTML TO PDF PLUGIN --> 
<script src="<?=base_url();?>js/jspdf.min.js" type="text/javascript"></script>
<!-- END OF HTML TO PDF PLUGIN -->   
  <link rel="stylesheet" href="<?=base_url();?>css/bootstrap-select.css">
  <script src="<?=base_url();?>js/bootstrap-select.js"></script> 
<!--Auto complete plugins--> 
<!--<link rel="shortcut icon" href="<?=base_url();?>images/favicon.png" />--> 
 <link rel="stylesheet" href="<?=base_url();?>css/jquery-ui.css">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">




</head>
<!-- END HEAD -->
<body class="page-container-bg-solid page-boxed">
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="<?=base_url();?>tenantSignUp/statement/home">
					<img src="<?=base_url();?>images/login/ARI-Logo.png" alt="logo"  height="60">
				</a>
			</div>

<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
			 
					<li class="droddown dropdown-separator">
						<span class="separator"></span>
					</li>
		 
<!-- BEGIN USER LOGIN DROPDOWN -->
<li class="dropdown dropdown-user dropdown-dark">
	<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
		<img alt="" class="img-circle" src="<?=base_url();?>images/<?php echo $this->session->userdata('photo');?>">
		<span class="username username-hide-mobile"><?php echo $this->session->userdata('first_name');?></span>
	</a>
	<ul class="dropdown-menu dropdown-menu-default">
		<li>
			<a href="<?=base_url();?>tenantSignUp/profile">
				<i class="icon-user"></i> My Profile 
			</a>
		</li> 
		
		<li>
			<a href="<?=base_url();?>Auth/logout/">
				<i class="icon-key"></i> Log Out 
			</a>
		</li>
	</ul>
</li>
<!-- END USER LOGIN DROPDOWN -->
<!-- BEGIN QUICK SIDEBAR TOGGLER -->
<li class="dropdown dropdown-extended quick-sidebar-toggler">
	<span class="sr-only">Toggle Quick Sidebar</span>
	<!--<i class="icon-logout"></i>-->
</li>
<!-- END QUICK SIDEBAR TOGGLER -->
</ul>
</div>
<?php
	/*  if($this->session->userdata('id')=="")
	  { 
			redirect(base_url()."Auth/logout");
	  }
	 else
	 {
		 
	 } */ 
?>

<!-- END TOP NAVIGATION MENU -->
</div>
</div>
<!-- END HEADER TOP -->
<!-- BEGIN HEADER MENU -->
<div class="page-header-menu">
<div class="container"> 
<!-- BEGIN MEGA MENU -->
<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
<div class="hor-menu  ">
<ul class="nav navbar-nav">

<li class="menu-dropdown classic-menu-dropdown active">
	<a href="<?=base_url();?>tenantSignUp/statement"> Home </a> 
</li>

<li class="menu-dropdown classic-menu-dropdown ">
	<a href="<?=base_url();?>tenantSignUp/tenant_receipts"> Receipts </a> 
</li>
 
 
<li class="menu-dropdown classic-menu-dropdown ">
	<a href="<?=base_url();?>tenantSignUp/referredAgents"> Agents/Landlords </a> 
</li>

<li class="menu-dropdown classic-menu-dropdown ">
	<a href="<?=base_url();?>tenantSignUp/profile"> My Profile </a>
	 
</li>
</ul>

</div>
<!-- END MEGA MENU -->
</div>
</div>
<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">

<!-- END PAGE HEAD-->