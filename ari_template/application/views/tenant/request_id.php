<!-- BEGIN PAGE CONTENT BODY -->
<script>
function validate()
	{   
		var tenant=$("#tenants").val();
		var pay_mode=$("#pay_mode").val();
		var receipt_no=$("#receipt_no").val();
		var amount=$("#amount").val();
		var pay_date=$("#pay_date").val(); 
		if(tenant==""||tenant==null){ $("#error1").html("<font color='red'> Please enter at least 1 Tenant  </font>");return false;}
		if(pay_mode==""||pay_mode==null){ $("#error3").html("<font color='red'> Please select payment mode </font>");return false;}
		if(amount==""||amount==null){ $("#error2").html("<font color='red'> Amount field is empty </font>");return false;}
	//	if(pay_date==""||pay_date==null){ $("#error4").html("<font color='red'> Date field is empty</font>");return false;}
		 if(receipt_no==""||receipt_no==null){ $("#error5").html("<font color='red'> Receipt/Cheque or Receipt No  empty </font>");return false;}
		 
	}
	 
</script>

<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>   User  </span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Register</span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
  
<div class="row">
	
	<div class="col-md-12"   style="min-height:400px">  
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit "> 
			<div class="portlet light ">
				<div class="portlet-title">
				
		<div class="col-md-12" style="background:#006699;padding:6px;">
			<font color="#ffffff"><strong> &nbsp;  Provide your National ID or Passport </strong> </font> 
	   </div>
	   <div class="col-md-12">  &nbsp;  </div>	
	 
 
				<div class="col-md-6">
				<div class="form-group"> 
						 <label> National Id/Passport </label> 
								 <input   class="form-control"  value="" type="number" id="national_id"/>
								 <br/>
								<label id="error1"> </label>  
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group"> 
					<label> &nbsp; </label><br/>
					<button type="submit" class="btn green" id="save_changes"> Submit  </button>	
					</div>
				</div>	  
  
</div>
<p>  				</p>
<!-- END PAGE CONTENT INNER -->
</div>
</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
</div>
</div>
</div>     
   
 
<!-- END CONTENT --> 
<!-- END CONTAINER -->
<script language="javascript">
 
function validate_char(id)
{
    var TCode = document.getElementById(id).value;
       if(/^[a-zA-Z0-9- ]*$/.test(TCode) == false) 
	   {
		$("#success_msg").html("<font color='brown'> Input you have provided contains illegal characters </font>"); 
		document.getElementById(id).value="";
		$("#success").modal('toggle');  
        return false;
		}
    
} 
 
 
 $("#save_changes").click(function(){     
	var  id= $("#national_id").val();  
	if(!id){ $("#error").html("<font color='red'>  National ID required</font>"); $("#national_id").focus(); return false;}
 $("#error").html("<font color='blue'> Matching...</font>");
 $.ajax(
  {
		url:"<?=base_url();?>Auth/getId",
		type:"POST",  
		data:
		{
			'id':id 
		},
		success:function(data)
		{
			 var obj=JSON.parse(data);
			 if(obj.result=="ok")
			 {
				$("#national_id").val(''); 
				setTimeout(function()
				{ 
					window.location="<?=base_url();?>tenantSignUp/getAgent/"+id;
                },2500); 
			 }
			 else
			 {
				setTimeout(function()
				{ 
					window.location="<?=base_url();?>tenantSignUp/getAgent/"+id;
                },2000);  
			 }
		}
  })
 });
  
</script> 