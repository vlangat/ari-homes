<!-- BEGIN PAGE CONTENT BODY -->

<?php 
		$credit=0; $debit=0; $curr_bal=0; $total=0; $receipt="";
		 foreach($statement->result() as $row){
	     if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
		 if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
		
		 }
		   	  $company_code="";  $company_address="";
		   foreach($company_details->result() as $c){ $company_code=$c->company_code; $company_email=$c->company_email; $company_address=$c->address; $company_mobile=$c->mobile_number; }
		   
	?>
<div class="page-content">
<div class="container">
<!-- BEGIN PAGE BREADCRUMBS -->
<ul class="page-breadcrumb breadcrumb">
<li>
	<a href="<?=base_url();?>"> Home </a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span> <a href="<?=base_url();?>payment/packages"> Payment </a></span>
	<i class="fa fa-circle"></i>
</li>
<li>
	<span>Receipt   </span>
</li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
 
<div class="page-content-inner">
  
<div class="row">
	
	<div class="col-md-12"    style="background:#006699;padding:6px;">
				<font color="#ffffff"> Receipt </font> 
	</div>		 
  
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit "> 
<div class="portlet light ">  
<div class="row">
 
<div class="col-md-8">

   <div class="portlet light" > 

		<div class="portlet-body" style="font-size:12px">
			 <?php $logo=""; $company_name=""; foreach($company_details->result() as $c){ if($c->company_code==$company_code){ $company_name=$c->company_name; $logo=$c->logo;} }?>
			 <?php foreach($tenant_details->result() as $t){  }?>
			 <?php if($logo==""||$logo==0){ $logo="ari_company_logo.png"; }?> 
		<div>
			<img id="my_file" src="<?=base_url();?>media/<?=$logo?>" height="75" width="150" style="display:block;max-width:230px;max-height:95px;width: auto;height: auto;" alt="<?=strtoupper($company_name)?>">
		</div>
		<p>  </p>
		 
	<table border="0"  class="table table-striped table-hover" style="font-size:12px;border:1px solid lightgrey" width="80%"> 
	
				<tr> 
					<td align="left" colspan="2">
						<font>  <strong> Receipt Number:  </strong> <?=$receipt_no?> </font> 
					</td> 
					  
					<td align="right">
						<strong>   Date: </strong>  <?=$date_paid;?> 
					</td>
				 </tr>
		<tr>
		<td align="left">  	
			<p style="font-size:14px;padding-left:0px;list-style-type:none">
			<p>	<strong> Paid By:	</strong></p>  
			<p>	<?php $tenant_type=$t->tenant_type; if($tenant_type=="residential"){ echo $tenant_name=($t->last_name." ".$t->middle_name." ".$t->first_name);}else{ echo   $tenant_name=$t->company_name;}?>	</p>
			<p>	<?=$email=$t->tenant_email?>	</p>
			<p>	<?=$t->company_name?>	</p>  
			 
		</td>
		<td>   &nbsp; &nbsp; </td>
		 
		<td align="right">
			<ul style="font-size:15px; list-style-type:none">
				<li>	<strong> Paid to:	</strong></li>  
				<li>  <?php if($company_name==""){ 
				 foreach($agent_details->result() as $a){ if($a->company_code=$company_code){ $company_name=$a->first_name.' '.$a->middle_name.' '.$a->last_name; $company_email=$a->email; $company_address=$a->address; $company_mobile=$a->mobile_number; }}
				}
				 ?> 
				 <?=$company_name?>
				 </li>
				<li> <?=$company_email;?> </li>
				<li> <?=$company_mobile;?> </li>
				<li> <?=$company_address;?> </li> 
			</ul>
		</td>
	</tr>	
	<tr>
			<td> 
				 <strong> Amount Received:</strong> KES <?=$amount?>  
			</td> 
			<td> 
				<strong> Payment Method:</strong>   <?=$payment_mode?>  
			</td> 
			<td>
				<strong> Payment No: </strong>  <?php if($payment_mode_code=="" || $payment_mode_code==0){ $payment_mode_code=$receipt_no;}?> <?=$payment_mode_code;?> 	
			</td> 
	</tr>
		<tr> <td>   </td><td>  </td><td>  </td> </tr> 
		<tr  bgcolor="#DFDAD9"> 
			<td>   <strong>  Particulars </strong>    </td> <td>  </td> <td align="right">  <strong>   Amount &nbsp;&nbsp;  </strong>  </td>
		</tr>
	 
		 <?php $item_cost=0;
		foreach($receipt_items->result() as $r) {?>
		<tr height="20"> <td> <?=$r->payment_type?>  </td> <td>  </td> <td align="right">KES  <?=$r->paid_amount?></td></tr>
		<?php 
		 $item_cost=$item_cost+$r->paid_amount;
		} ?>
		<?php
		$vat=(16/100)*$amount;
		$sub_total=$amount-$vat; 
		?>
		 <tr height="20"> <td> Water  <?=$previous_reading?>-<?=$current_reading?> </td> <td>  </td> <td align="right">KES  <?=$water_cost?></td></tr> 
		 <?php if(($water_cost+$item_cost)  < $amount)
		 { 
		 ?>
		<tr height="20"> <td> Other Items   </td> <td>  </td> <td align="right">KES  <?=$sub_total-$item_cost?></td></tr> 
		 <?php }?>
		<tr> <td> </td> <td>  </td><td align="right"> <b> Sub Total </b> &nbsp; KES  <?=$sub_total;?>  </td></tr> 
		<tr > <td>  </td> <td>  </td> <td align="right"> <b> 16.00%  VAT TAX </b> &nbsp;  KES  <?=$vat;?>   </td></tr>
		<tr> <td> </td> <td>  </td> <td align="right">   <strong> Total </strong>    &nbsp;  KES <?=$amount;?>   </b>  </td></tr>
		
		 <tr><td>  </td><td colspan="2">  </td>  </tr>
		 <tr> <td>  </td> <td>  </td> <td align="right">  <strong> Balance </strong>   &nbsp;  KES <?=$balance; ?>     </td></tr>
	</table>
  
<p> <strong> Served by: <?=ucfirst(strtolower($this->session->userdata('first_name')));?>  </strong>  <h6> Record as at <?php echo date('d-m-Y H:i');?></h6>  </p>
</div>
 
<p>  <hr/> </p>
 
<a  href="javascript:;" class="btn grey"  id="btn_receipt" > &nbsp;&nbsp;&nbsp; <i class="fa fa-print" style="font-size:18px;color:#006699"></i><b>  Print  </b>&nbsp;&nbsp;&nbsp;</a> 
&nbsp;
<a  href="javascript:;" class="btn red"  id="btn_email" >  <i class="fa fa-envelope" style="font-size:18px;"></i><b>  Email  </b> &nbsp;</a>  

		 </div>
		</div>
 
</div>
</div>
</div> 
</div> 
</div>
<!-- END EXAMPLE TABLE PORTLET--> 

</div>
<!-- END PAGE CONTENT INNER -->
</div> 
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
  <div id="success" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h4> <b> Venit Message </b> </h4>
					<hr/>
					<p id="err">
						  
					</p>
				</div>
			</div>
	</div>
	<div class="modal-footer" >  
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">OK</button>
	</div>
</div>
   


 <div id="sendMail" class="modal fade" tabindex="-1" data-width="600">
	<div class="modal-body">
				<div class="row">
				<div class="col-md-12"> 
				<h5><b> Email Tenant Message </b></h5>
				<hr/>
					<p>
					    This Receipt will be sent as an Email to <font id="email_id"> </font>
					</p>
				</div>
			</div>
			<label id="error">  </label>
	</div>
	<div class="modal-footer" > 
		<center>  
			<button type="button" id="send_email" class="btn green">&nbsp; Yes &nbsp; </button>
			<button type="button" data-dismiss="modal" class="btn red">&nbsp; No  &nbsp; </button> 
			<button type="button" data-dismiss="modal" class="btn default">&nbsp; Close  &nbsp; </button> 
		</center>
	</div>
</div>
   
      
        <!-- END CONTAINER -->
<script language="javascript">
 
 $("#btn_receipt").click( function () {
            var divContents = $(".portlet-body").html();
            var printWindow = window.open('', '', 'height=400,width=1000');
            printWindow.document.write('<html><head><title>  Receipt </title>');
            printWindow.document.write('</head><body style="margin-left: auto;margin-right: auto; width:80%">');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
		
	
		
  
 
   var email="<?=$email?>";  
        var fname="<?=$tenant_name?>";  
        var from="<?=$company_name?>"; 
      
		
 $("#btn_email").click( function ()
		{ 
		   $("#email_id").html(fname+" <u> "+email+" </u>");
			$("#sendMail").modal('show');
		});
		
   $("#send_email").click( function () {  
		var divContents = $(".portlet-body").html();   
       
		$("#error").html("<font color='#006699'> Sending email...please wait</font>");
		//$("#success").modal('show');
		$.ajax({
			'url':"<?=base_url();?>payment/sendRentStatement",
			'type':"POST",
			async:false,
			data:
			{
				'email':email, 
				'first_name':fname, 
				'from':from, 
				'title':"Rent Receipt", 
				'body':divContents 
			},
			success:function(data)
			{  
				var obj=JSON.parse(data);  
				if(obj.result=="ok")
				{   
					$("#error").html("<font color='green'> Receipt   sent   successfully</font>");
					//$("#success").modal('show');
				}
				else
				{  
					$("#error").html("<font color='red'> Receipt   not sent to   <u>"+email+"</u></font>");
					//$("#success").modal('show');
				}
				
			}
			
 })
 
});
		
$(function () {
$.fn.vAlign = function(){
    return this.each(function(i){
    var ah = $(this).height();
    var ph = $(this).parent().height();
    var mh = Math.ceil((ph-ah) / 2);
    $(this).css('margin-top', mh);
    });
};

$('#my_file').vAlign();
		});
</script>
