<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Encrypte extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();  
		$this->load->model("Authentication_model"); 
		date_default_timezone_set('Africa/Nairobi'); 
	}
 
public function hashId($enct="", $table="",  $condition="",$searchItem="")
	{
		if(empty($condition)  || $table=="" ||  $enct=="" ||  $searchItem=="")
				{ 
					 $id=""; 
				}
				else{
				$q=$this->Authentication_model->get_data($table, $condition); 
                if($q->num_rows()>0)
                 {
                   foreach($q->result() as $r)
				   {
					$id=$r->$searchItem; 
					$s = sha1('2ab'); $s = substr($s, 0, 10); 
					$e =base64_encode(do_hash($id . $s,'sha512') . $s);
					if($e==$enct)
					{  
						$id=$r->$searchItem;   break;
					} 
					else{ $id=""; }
				   } 
				  
                 }
				} 
				
				return  $id; 
    }

}