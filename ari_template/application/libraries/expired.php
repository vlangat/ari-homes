<?php
class Expired extends CI_Controller{
	     public function __construct(){
		 parent::__construct(); 
		$this->load->model("Authentication_model");
		$this->load->model("Payment_model");
		$this->load->model("Admin_model"); 
        $this->load->library('session');		
		date_default_timezone_set('Africa/Nairobi'); 
}

public function check_subscription_expiry(){ 
	date_default_timezone_set('Africa/Nairobi'); 
		$disabled=""; $expiry_date=""; 
		$subscription=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_payment", $order_by="id");
		$p=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1), $table="package_current_account");
		$expiring_date=$this->Payment_model->get_data(array('company_code'=>$this->session->userdata('company_code'),'audit_number'=>1,'id'=>$this->session->userdata('id')), $table="user_info");
		$days=0;		
		
		if($expiring_date->num_rows()<=0)
		{
			$disabled="";  
		}
		else{
			
			foreach($expiring_date->result() as $e){ $expiry_date=$e->acc_expiry_date; $date_paid=$e->subscription_date;  }
		   
		//$now=time(); 
		$now=time();
		$total_days=strtotime($expiry_date)-strtotime($date_paid);
		$total_days=floor($total_days/(60*60*24));
		$diff_time=strtotime($expiry_date)-$now;
		$days=floor($diff_time/(60*60*24));
		$days=$days+1;	 	
		   
	} 
			if($days<=0)
			{
				$disabled="disabled";
			}
		 
		return $disabled;
 }
 
}
 ?>