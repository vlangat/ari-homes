<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rent_class extends CI_Controller {
	var $disabled="";
	public function __construct()
	{
		parent::__construct();  
		$this->load->model("Authentication_model");
		$this->load->model("Property_model"); 
		$this->load->model("Tenant_model"); 
		$this->load->model("Rent_model"); 
		$this->load->library('upload');
		$this->load->library('Expired'); 
		date_default_timezone_set('Africa/Nairobi');
		$obj=new Expired();
		$this->disabled=$obj->check_subscription_expiry();
	}
public function expected_pay_day($tenant_id)
	{
		$day=""; $days_of_year=date("z", mktime(0,0,0,12,31,date('Y'))) + 1;
		$month = date("m");	 $year = date("Y"); $days_of_month=date("t",mktime(0,0,0,$month,1,$year)); 
		$expiry_date=date_create(date("Y-m-d"));
		//$day=""; $rent_frequency="Monthly"; 
		$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
		if($q->num_rows()>0)
		{
			$q=$this->Tenant_model->get_data($table="tenant", array('id'=>$tenant_id));
			foreach($q->result() as $r){ $date_added=$r->date_added; $rent_frequency=$r->rent_frequency; $expiry_date=$r->expected_pay_day;}
			$now=date("d");
			if($rent_frequency=="Monthly")
			{
				
				$date_added=date_create($date_added);
		        date_add($date_added,date_interval_create_from_date_string($days_of_month-$now+$expiry_date  ."days"));
		        $day=date_format($date_added,"Y-m-d");  
			}
			else if($rent_frequency=="Daily"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(1 ."days"));
				$day=date_format($date,"Y-m-d"); 
			} 
			else if($rent_frequency=="Quarterly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(90-$now+$expiry_date ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Yearly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string($days_of_year-$now+$expiry_date."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
			else if($rent_frequency=="Weekly"){
				$date=date_create($date_added);
		        date_add($date,date_interval_create_from_date_string(7 ."days"));
		        $day=date_format($date,"Y-m-d"); 
			}
		}
		 return  $day; 
		
}

public function expected_pay($id="", $month="")
{
			$debit=0;  
			if($month==""){ $month=date('m'); } 
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id));
			if($id=="")
			{
				echo json_encode(array('result'=>"false"));
			}
			else
			{
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						if($row->type=="d" && ($month==date("m",strtotime($row->date_paid)))){ $debit=$debit+$row->amount; }  
						  
					} 
				}
			}
		return $debit; 
}

public function property_expense($id="", $month="")
{
	$expenses=0;
	$q=$this->Rent_model->getData($table="expenses", $condition=array('audit_number'=>1,'property_id'=>$id,'expense_type'=>1));
			if($id=="")
			{
				$expenses=0;
			}
			else
			{
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						if($month==date("m",strtotime($row->date_paid))){ $expenses=$expenses+$row->amount; }  
						
					}
				}
			}
	return $expenses;
}

public function reimbursement($id="", $month="")
{
	$deposits=0;
	$q=$this->Rent_model->getData($table="refunded_deposits", $condition=array('audit_number'=>1,'property_id'=>$id));
			if($id=="")
			{
				$deposits=0;
			}
			else
			{
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						if($month==date("m",strtotime($row->date_refunded))){ $deposits=$deposits+$row->amount; }  
						
					}
				}
			}
	return $deposits;
}


public function balance_forwarded($id="", $month="")
{
			$debit=0;   $balance=0; $credit=0;
			if($month==""){ $month=date('m'); } 
			if($month==1){ $month=12; } else { $month=$month-1;} 
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id));
			if($id=="")
			{
				$balance=0;
			}
			else
			{
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						if($row->type=="d" && ($month==date("m",strtotime($row->date_paid)))){ $debit=$debit+$row->amount; }  
						if($row->type=="c" && ($month==date("m",strtotime($row->date_paid)))){$credit=$credit+$row->amount;} 
						if($credit>=$debit){ $balance=($credit-$debit);} else if($credit<$debit) { $balance= $debit-$credit; } 
						 
					} 
				}
			}
		return $balance; 
}

public function getPrevBalance($id="", $par2="")
	{
		    $credit=0; $debit=0; $prev_bal=0; $rent=0;
			$q=$this->Rent_model->get_statement($condition=array('tenant_id'=>$id));
			if($id=="")
			{
				echo json_encode(array('result'=>"false"));
			}
			else
			{
			if($q->num_rows()>0)
				{
					$count=$q->num_rows();
					foreach($q->result() as $row)
					{
						if($row->type=="c"){$credit=$credit+$row->amount;}else if($row->type=="d")  { $debit=$debit+$row->amount; }  
						if($credit>=$debit){ $curr_bal=($credit-$debit);} else if($credit<$debit) { $curr_bal= $debit-$credit; } 
						//if($count==2){  $prev_bal=$debit-$credit; }	
						if($row->type=='d'){  $rent=$row->amount; }
						$count--;
					} 
				}
			}
		
		//$return_val= $prev_bal;
		$return_val= $rent;
		if($par2==""){ } else{  $return_val=$rent; }
		
		 
		return $return_val;
	}
	
	
	public function paid_rent($id="", $month="", $year="")
	{
		if($month==""){ $month=date('m'); }
		if($year==""){ $year=date('Y'); }
		$paid=0;
		$q=$this->Rent_model->get_paid_rent($table="tenant_transaction",  array('tenant_id'=>$id,'tenant_property_units.company_code'=>$this->session->userdata('company_code'),'tenant_transaction.type'=>'c','tenant_transaction.audit_number'=>1)); 
		 
		 foreach($q->result()  as $row)
			{ 
				if($month==date("m",strtotime($row->date_paid)) && $year==date("Y",strtotime($row->date_paid)))
				{ 
					$paid=$paid+$row->amount;
				}
			}
		return $paid;
	}
	
public function business_expense($year="", $month="")
{
	$expenses=0;
	if($year==""){  $year=date("Y");}
	$q=$this->Rent_model->getData($table="expenses", $condition=array('audit_number'=>1,'company_code'=>$this->session->userdata('company_code'),'expense_type'=>2));
	 
			if($q->num_rows()>0)
				{ 
					foreach($q->result() as $row)
					{
						if($month==date("m",strtotime($row->date_paid)) && $year==date("Y",strtotime($row->date_paid))){ $expenses=$expenses+$row->amount; }  
						
					}
				} 
	return $expenses;
}


public function management_fees($year="", $month="")
{
	$paid=0;
	$q=$this->Property_model->get_data($table="tenant_property_units" ,array('company_code'=>$this->session->userdata('company_code'))); 
	foreach($q->result() as $t)
	{  
			$tenant_id=$t->id; 
			$management_fee=$t->management_fee; 
			$e=$this->expected_pay($tenant_id, $month);
			if($t->tenant_deleted==2 && $e==0){
				 continue;
			}
			else{
			$paid=$paid+($management_fee/100*$this->paid_rent($tenant_id, $month ,$year));
			}
	}
	return $paid;
}

}