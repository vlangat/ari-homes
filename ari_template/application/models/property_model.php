<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Property_model extends CI_Model {
 
	public function update_property($condition,$data)
	{
	  	  $this->db->where($condition);
		return  $this->db->update('property',$data);

	} 	

	public function update_data($table, $condition,$data)
	{
		if(!empty($condition)){
				$this->db->where($condition);
				return  $this->db->update($table,$data);
			}

	} 
	
	public function insert_documents($data)
	{
	  	 
		return  $this->db->insert('media',$data);

	}
	public function insert_property_units($data)
	{
		$this->db->where('no',$data['property_id']);
		$q=$this->db->get("property_details");
		foreach($q->result() as $row){ $id=$row->no;$property_name=$row->property_name;}
		$this->db->insert('property_units',$data);
		$this->db->where('property_id',$id);
		return  $this->db->update('property_units',array('property_name'=>$property_name));

	}
	public function insert_data($table,$data)
	{ 
		$this->db->where($data);
		$this->db->get($table);
		 if($this->db->affected_rows()>0)
		 {
			 $this->db->where($data);
			return $this->db->update($table,$data);
		 }
		 else{
				return $this->db->insert($table,$data);
		 } 

	}  

 
	public function get_data($table,$condition="", $order_by="")
	{ 
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
             if(!empty($order_by))
		{
		$this->db->order_by($order_by,"desc");
               } 
		return $this->db->get($table);
	}
	
	public function get_tenant($table,$condition="")
	{ 
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		
		return $this->db->get($table);
	}
	
	public function delete_data($table,$condition="")
	{ 
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->delete($table);
	} 
	
public function update_amenity($table,$data="")
	{ 
		 
			$this->db->where($data);
			$this->db->delete($table);
			if($this->db->affected_rows()>0)
			{
				return $this->db->insert($table, $data);	 	
			}
			else
			{
				return $this->db->insert($table, $data);
			}
	}
	
public function showAmenities($condition="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->get('amenities');
	}
public function property_Category($condition="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
			//->or_where(array('property_id'=>0,'is_user_defined'=>2));
		} 
		$this->db->distinct();
		$this->db->order_by("id","asc"); 
		return $this->db->get('property_category');
	}
	
public function getCategory($condition="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
			//->or_where('property_id',1);
		} 
		$this->db->distinct();
		$this->db->order_by("unit_id","asc"); 
		return $this->db->get('unit_property_details');
	}

public function checkIfCategoryExist($condition="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition) ;	
		}
     	
	return $this->db->get('property_category');
	}
	
	public function doneEdit($condition="",$data)
	{
		 
		$this->db->where($condition);   
		return $this->db->update('property_units',$data);
	}
	public function deleteFile($condition="")
	{
		 
		$this->db->where($condition);   
		return $this->db->delete('media');
	}
	
	public function check_file($condition="")
	{
		 
		$this->db->where($condition);   
		return $this->db->get('media');
	}
	
}