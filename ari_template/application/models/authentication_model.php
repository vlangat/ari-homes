<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication_model extends CI_Model {
 
   public function login($condition)
   {
	   
	   $this->db->where($condition);
	   return $this->db->get('account');
   }
   public function signUp($data)
	{
	  	
		return  $this->db->insert('user_info',$data);

	}
 public function insert_data($table="", $data)
	{
	  	
		return  $this->db->insert($table,$data);

	}
	
public function update_data($table, $condition="", $data)
	{
	  	if($condition){
			$this->db->where($condition);
		}
		return  $this->db->update($table,$data);

	}
 public function get_data($table, $condition,$order_by="",$asc_desc="DESC",$limit="")
	{
	  	if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		if(!empty($order_by))
		{
			$this->db->order_by($order_by, $asc_desc);
		} 
		if(!empty($limit))
		{
			$this->db->limit($limit,0);
		} 
		return  $this->db->get($table);

	}

 public function insertCompany($data)
	{
	  	
		return  $this->db->insert('company_details',$data);

	}
	
	public function proof_user($condition)
	{
		$this->db->where($condition);
		return $this->db->get("account");	
	}
	
	public function checkCompany($condition)
	{
		$this->db->where($condition);
		return $this->db->get("account");	
	}
	
	public function get($condition="")
	{	
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->get('account');

	}
	public function removeUser($condition="",$data="")
	{	
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->update('enter',$data);

	}

	 public function activation($code)
		{
			$this->db->where('activation',$code);
			return $this->db->update('enter',$data=array('user_enabled'=>1,'activation'=>1));
		}	

	public function getPassword($id,$pass,$reset_ref_no)
	{
		$this->db->where(array('user_info_id'=>$id));
		$this->db->update('enter',$data=array('reset_password_code'=>$pass,'user_enabled'=>2,'reset_ref_no'=>$reset_ref_no));
		if($this->db->affected_rows()>0)
		{
			$this->db->where(array('id'=>$id,'deactivated'=>1));
			return $this->db->get('account');
		}
		else
		{
				return false;
		}
	}
	
  public function reset($code,$pass="")
	{
		$this->db->where(array('reset_password_code'=>$code));

		return $this->db->update('enter',$data=array('reset_password_code'=>$code,'user_enabled'=>1));		
	}
	
  public function changePass($condition,$new_pass,$ref_time)
	{
		$data=array('verify'=>$new_pass,'ref_time'=>$ref_time);
		$this->db->where($condition);
		return $this->db->update('enter',$data);
	}
	
  public function updateProfile($condition,$data)
	{ 
		$this->db->where($condition);
		return $this->db->update('user_info',$data);
	}
	
  public function getCompanyDetails($condition)
	{ 
		$this->db->where($condition);
		return $this->db->get('company_info');
	}
	
  public function updateCompanyProfile($condition,$data)
	{ 
		$this->db->where($condition);
		return $this->db->update('company_info',$data);
	}

	public function addUser($data)
		{
		$this->db->where('email',$data['email']); 
		$q = $this->db->get('account'); 
	
		if ($q->num_rows() >0) 
		{ 
			
			//$this->db->where('email',$email); 
			return ;	//$this->db->update('user_info',$data);
		} 
		else
		{
			//insert into enter
			return $this->db->insert('user_info',$data); 
		}
	}
	
		
	public function userActivation($id,$added_by,$account_id)
	{
		$this->db->where(array('user_info_id'=>$id));
		return $this->db->update('enter',$data=array('deactivated'=>1,'user_enabled'=>1));
	}
	
	public function acceptAccount($email,$pass, $ref_time)
	{
	    $this->db->where('email',$email);
		$query=$this->db->get("account");
		foreach($query->result() as $row)
			{
			$id=$row->id;
			$account_id=$row->company_code;
			}
        $this->db->where('user_info_id',$id); 
		return $this->db->update('enter',$data=array('user_enabled'=>1,'deactivated'=>1,'activation'=>1,'verify'=>$pass,'ref_time'=>$ref_time)); 
	}
	
	public function uploadPhoto($file_name,$condition,$table)
	{
		$data=array('logo'=>$file_name);
		$this->db->where($condition);
		$this->db->update($table,$data);		
		return true;
	}
	
	public function log_activity($user="", $ip="", $message="",$status="")
    {  
		$this->db->insert('logs', array('user_id'=>$user,'ip_address'=>$ip,'message'=>$message,'status'=>$status));
    }
	
}