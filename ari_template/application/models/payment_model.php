<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment_model extends CI_Model {
	 
 
	public function billing_cycle($condition="")
	{ 
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->get('package');
	}
 
	public function proof_payment($condition="", $table)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->get($table);
	}

	public function get_data($condition="", $table="",$order="",$limit="")
	{ 
		  
		 if(!empty($condition))
		 {
			 $this->db->where($condition);
		 }
		if(!empty($order))
		 {
			 $this->db->order_by($order, 'DESC');
		 }
		 if(!empty($limit))
		 {
			 $this->db->limit($limit,0);
		 }
		return $this->db->get($table); 
	}
	
	public function insert_data($data, $table)
	{ 
		  $this->db->where($data);
		  $this->db->get($table);
		 if($this->db->affected_rows()>0)
		 {
			 $this->db->where($data);
			return $this->db->update($table,$data);
		 }
		 else
		 {
				return $this->db->insert($table,$data);
		 }
	}

	public function expected_pay_date($table="",$condition,$data)
	{ 
		  $this->db->where($condition);
		  $this->db->get($table);
		 if($this->db->affected_rows()>0)
		 {
			$this->db->where($condition);
			return $this->db->update($table,$data);
		 }
		 else
		 {
			return $this->db->insert($table,$data);
		 }
	}
	
	public function update_data($condition, $table, $data)
	{ 
	  if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		return $this->db->update($table, $data);
	}
	
	public function add_sms($table,$condition, $data)
	{  
	     $this->db->where($condition); 
		  
		return $this->db->update($table, $data);
	}

	public function save_payment($condition="",$data, $table="")
	{  
		  if(!empty($condition))
		  {
			   $this->db->where($condition);
			   $this->db->get($table);	
		  }		  
		 if($this->db->affected_rows()>0)
		 {
			//return $this->db->update($table,$data);
		 }
		 else
		 {
				return $this->db->insert($table,$data);
		 }
	}
	}