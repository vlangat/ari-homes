<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rent_model extends CI_Model {
  
  public function payment($data)
	{
	  	 
		return  $this->db->insert('rent',$data);

	} 
	
	public function insert_data($table, $data)
	{
		return  $this->db->insert($table,$data);
	}
	
	public function update_data($table, $data, $condition)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return  $this->db->update($table,$data);
	}
	public function delete_data($table, $condition)
	{
		$this->db->where($condition);
		return  $this->db->delete($table);
	}
	
	public function getData($table="", $condition="",$order_by="", $asc_desc="asc")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		if(!empty($order_by))
		{
			$this->db->order_by($order_by,$asc_desc);
		} 
		return  $this->db->get($table);
	}
 
 public function get_paid_rent($table="", $condition)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		$this->db->order_by("id","DESC");
		$this->db->select("tenant_transaction.id,tenant_transaction.tenant_id,tenant_transaction.date_paid,tenant_transaction.payment_mode,tenant_transaction.amount,tenant_transaction.type,tenant_property_units.company_code");
		$this->db->from($table);
		$this->db->join('tenant_property_units',$table.'.tenant_id=tenant_property_units.id','left'); 
		return  $this->db->get();
	}
 	
	public function get_statement($condition="",$from="",$to="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		if(!empty($to))
		{
			$this->db->where("date_paid BETWEEN '".$from."' AND '".$to."'");
		}
		//$this->db->join('tenant','tenant.id=tenant_transaction_view.tenant_id','left');
		$this->db->join('unit_property_details','unit_property_details.unit_id=tenant_transaction_view.property_unit_id','left');
		
		$this->db->order_by("transaction_id","ASC");
		return  $this->db->get('tenant_transaction_view');
	}

	public function get_expense_statement($condition="",$from="",$to="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		if(!empty($to))
		{
			$this->db->where("expenses_view.date_paid BETWEEN '".$from."' AND '".$to."'");
		} 
		return  $this->db->get('expenses_view ');
	}

	public function getExpenses($condition="")
	{
		$this->db->select("suppliers.company_name, suppliers.product_description, suppliers.company_code, expenses.id, expenses.property_id,expenses.supplier_invoice_no,expenses.invoice_no, expenses.expense_type_id,expenses.amount, expenses.payment_mode, expenses.date_paid, expenses.description");
		$this->db->from("expenses");
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		$this->db->order_by("expenses.id","desc");
		$this->db->join('suppliers','expenses.supplier_id=suppliers.id','left');
		return  $this->db->get();
	}
	
	public function get_receipt($id="")
	{
		if(!empty($id))
		{
			$this->db->where('tenant_payment.tenant_transaction_id',$id);
		} 
		$this->db->join('pricing_details','pricing_details.id=tenant_payment.pricing_details_id','left'); 
		return  $this->db->get('tenant_payment');
	}
 	
	public function received_pay($condition="")
	{ 
		$this->db->select('`tenant_transaction`.`id` AS `id`,tenant_transaction.amount,tenant_transaction.date_paid, tenant_transaction.payment_mode,tenant_property_units.first_name,tenant_property_units.tenant_type,tenant_property_units.middle_name,tenant_property_units.last_name,tenant_property_units.company_name');
		$this->db->from('tenant_transaction');
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		$this->db->order_by("id","desc");
		$this->db->join('tenant_property_units','tenant_property_units.id=tenant_transaction.tenant_id','left'); 
		return  $this->db->get();
	}
 
	public function payment_for()
	{ 
		$this->db->distinct(); 
		$this->db->select("payment_type"); 
		return  $this->db->get("pricing_details");
	}
	
	public function checkArrears($id="",$condition="")
	{ 
		if(!empty($id))
			{
				$this->db->where(array('no'=>$id));
			}
		else if(!empty($condition)){$this->db->where($condition);}
		return $this->db->get('tenant');  
	} 
	
	public function updateArrears($id="",$new_arrears)
	{ 
		 $this->db->where(array('no'=>$id));
		 return $this->db->update('tenant',array('current_arrears'=>$new_arrears));
	} 
 
   public function paidRent($condition="")
	{
	  	if(!empty($condition))
		{
			$this->db->where($condition);
		}
		//return  $this->db->get('rent');
		return  $this->db->get('rent_received');

	}
	
	public function expectedRent($condition="")
	{
	  	 
            if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return  $this->db->get('tenant');

	} 
	public function unpaid_rent($condition="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		$this->db->select("*");
		$this->db->from("tenant_property_units");
		$this->db->where("current_account.balance >",0);
		$this->db->where("current_account.audit_number ",1);
		$this->db->join("current_account","current_account.tenant_id=tenant_property_units.id","left");
		return  $this->db->get();  
	} 

	/*public function expectedRentCommercial($condition="")
	{
         if(!empty($condition))
		{
			$this->db->where($condition);
		}
	  	 
		return  $this->db->get('commercial_tenant');

	} */
 
}