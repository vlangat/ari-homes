<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tenant_model extends CI_Model {
  
 
public function get_data($table,$condition="",$order_by="",$asc_desc="DESC")
	{ 
		if(!empty($condition))
		{
			$this->db->where($condition);
		} 
		if(!empty($order_by))
		{
			$this->db->order_by($order_by,$asc_desc);
        } 
		return $this->db->get($table);
	}

public function insert_data($table, $data)
	{ 
	 
		return $this->db->insert($table, $data);
	}
	
public function update_data($table="",$condition="",$data)
{	
	if(!empty($condition))
	{
		$this->db->where($condition);
	}
	return $this->db->update($table,$data);  

}
	
public function insert_singleTenant($data)
	{
	  	$this->db->where(array('tenant_email'=>$data['tenant_email']));
		$q= $this->db->get('tenant');
		if($q->num_rows()>0)
		{
			 $this->db->where('tenant_email',$data['tenant_email']);
			 return $this->db->update('tenant',$data);
		}
		else
		{
			return $this->db->insert('tenant',$data);
		}
		 
	} 
	
	
public function insert_commercialTenant($data)
	{
	  	 $this->db->where('company_email',$data['company_email']);
	  	 $this->db->or_where('tenant_email',$data['tenant_email']);
		$q= $this->db->get('tenant');
		if($q->num_rows()>0)
		{
			 $this->db->where('company_email',$data['company_email']); 
			 return $this->db->update('tenant',$data);
		}
		else
		{
			return $this->db->insert('tenant',$data);
		} 
	} 

	public function showTenants($condition="")
	{	
		if(!empty($condition))
		{
			
			$this->db->select('*');
			$this->db->from('tenant'); 
			$this->db->join('unit_property_details', 'unit_property_details.unit_id = tenant.property_unit_id','left');
			 $this->db->where($condition);  
			 
			//$this->db->where(array('tenant.audit_number'=>1));
		}
		return $query = $this->db->get();  
	}
	
	public function tenant_details($condition="")
	{	
		
		$this->db->select('*');
		$this->db->from('tenant'); 
		//$this->db->join('unit_property_details', 'unit_property_details.unit_id = tenant.property_unit_id','left');
		$this->db->where($condition);
		return $this->db->get(); 

	}

	public function updateTenant($condition="",$data)
	{	
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return $this->db->update('tenant',$data); 
		//return $this->db->update('single_tenant',$data);

	}
	public function update_supplier($data,$condition)
	{	
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return $this->db->update('suppliers',$data);  

	}

	function lookup_supplier($search_id)
	{   
		$this->db->where('company_code',$this->session->userdata('company_code')); 
		$this->db->like('company_name',$search_id,'both');  
       // $this->db->or_like('contact_person',$search_id);       
        return $this->db->get('suppliers'); 
    }
	
	public function updateCommercialTenant($condition="",$data)
	{	
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return $this->db->update('tenant',$data);  

	}
	
	public function allocatedProperty($condition="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
	return $this->db->get('property');	
		
	}
	public function documents($condition="")
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
	return $this->db->get('media');		
	}
	public function search($search_id="")
	{
		if(!empty($search_id))
		{
			$this->db->or_like($search_id);
		}
		else
		{
			$this->db->or_like('account_id',$this->session->userdata('account_id'));
		}
		return $this->db->get('project');
	} 
	
	public function insert_documents($data)
	{
	  	 
		return  $this->db->insert('media',$data);

	}
	public function removeTenant($condition)
	{
	  	 if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return  $this->db->update('tenant',array('tenant_deleted'=>2,'audit_number'=>2,'tenant_status'=>2));

	}
	
	public function add_supplier($data)
	{
	  	 
		return  $this->db->insert('supplier',$data);

	}
	
	public function show_supplier($condition)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
		}
		return  $this->db->get('suppliers');

	}
}